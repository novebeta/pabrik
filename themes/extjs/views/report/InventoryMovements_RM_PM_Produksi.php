<h1>Inventory Movements Material (Produksi)</h1>

<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Item Code',
            'name' => 'kode'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama'
        ),
        array(
            'header' => 'Exp. Date',
            'name' => 'tgl_expired'
        ),
        array(
            'header' => 'Saldo Awal',
            'name' => 'before',
            'value' => function ($data) {
                return number_format($data['before'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Terima dari PPIC',
            'name' => 'received',
            'value' => function ($data) {
                return number_format($data['received'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Pemakaian',
            'name' => 'consumed',
            'value' => function ($data) {
                return number_format($data['consumed'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Retur ke PPIC',
            'name' => 'returned',
            'value' => function ($data) {
                return number_format($data['returned'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Saldo Akhir',
            'name' => 'after',
            'value' => function ($data) {
                return number_format($data['after'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
    )
));
?>
