<h1>Inventory Card Material</h1>
<h2>ITEM : <?= $kode ?> - <?= $nama ?></h2>
<?php if ($exp !== '') { ?>
    <h3>TGL. EXP. : <?= $exp ?></h3>
    <h3>No. Lot : <?= $no_lot ?></h3>
    <h3>No. QC : <?= $no_qc ?></h3>
    <?php ;
} ?>
<?php if ($supplier_name != NULL) { ?>
    <h3>Supplier : <?= $supplier_name ?></h3>
    <?php ;
} ?>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Tanggal',
            'name' => 'tgl'
        ),
        array(
            'header' => 'No. Referensi',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Keterangan',
            'name' => 'mvt'
        ),
        array(
            'header' => 'Satuan',
            'name' => 'sat',
            'htmlOptions' => array('style' => 'text-align: center;')
        ),
        array(
            'header' => 'Saldo Awal',
            'name' => 'before',
            'value' => function ($data) {
                return number_format($data['before'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Masuk',
            'name' => 'in',
            'value' => function ($data) {
                return number_format($data['in'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Keluar',
            'name' => 'out',
            'value' => function ($data) {
                return number_format($data['out'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Saldo Akhir',
            'name' => 'after',
            'value' => function ($data) {
                return number_format($data['after'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>