<h1>General Journal</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
//$this->pageTitle = 'General Journal';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Transaction Tipe',
            'name' => 'tipe_name'
        ),
        array(
            'header' => 'Reference',
            'name' => 'reference'
        ),
        array(
            'header' => 'Account Code',
            'name' => 'account_code'
        ),
        array(
            'header' => 'Account Name',
            'name' => 'account_name'
        ),
        array(
            'header' => 'Description',
            'name' => 'memo_'
        ),
        array(
            'header' => 'Debit',
            'name' => 'Debit',
            'value' => function ($data) {
                return number_format($data['Debit'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Credit',
            'name' => 'Credit',
            'value' => function ($data) {
                return number_format($data['Credit'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>