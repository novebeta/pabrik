<h1>Barang Keluar Detail</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Tanggal',
            'name' => 'tgl'
        ),
        array(
            'header' => 'No. TB',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'TUJUAN( SJ&RB)',
            'name' => 'kode_customer'
        ),
        array(
            'header' => 'Batch',
            'name' => 'batch'
        ),
        array(
            'header' => 'EXP',
            'name' => 'tgl_exp'
        ),
        array(
            'header' => 'Qty',
            'name' => 'qty',
            'value' => function ($data) {
                return number_format($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>