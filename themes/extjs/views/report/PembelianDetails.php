<h1>Rincian Pembelian</h1>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Supplier</td>
        <td> : <?= $supplier ?></td>
    </tr>
    </tbody>
</table>
<?
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'TGL PO',
            'name' => 'tgl_po',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'TGL SJ',
            'name' => 'tgl_sj',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'TGL INV',
            'name' => 'tgl_inv',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'KODE SUPPLIER',
            'name' => 'supplier_code',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'NAMA SUPPLIER',
            'name' => 'supplier_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'NO. PO',
            'name' => 'doc_ref_po',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO. SJ',
            'name' => 'tanda_terima',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO. INV',
            'name' => 'no_invoice',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL INVOICE',
            'name' => 'tgl_inv',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'KODE MATERIAL',
            'name' => 'kode_material',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NAMA MATERIAL',
            'name' => 'nama_material',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'QTY',
            'value' => function ($data) {
                return number_format($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'SATUAN',
            'name' => 'sat',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'HARGA',
            'value' => function ($data) {
                return number_format($data['price']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'TOTAL',
            'value' => function ($data) {
                return number_format($data['bruto']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'DISC',
            'value' => function ($data) {
                return number_format($data['disc_rp']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'PPN',
            'value' => function ($data) {
                return number_format($data['vatrp']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'TOTAL',
            'value' => function ($data) {
                return number_format($data['total_invd']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>