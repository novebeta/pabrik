<h1>Rekap FPT</h1>
<?
switch ($status) {
    case "All":
        break;
    case FPT_OPEN :
        $status = "OPEN";
        break;
    case FPT_PAID :
        $status = "PAID";
        break;
    default :
        $status = "All";
}
?>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Supplier</td>
        <td> : <?= $supplier ?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td> : <?= $status ?></td>
    </tr>
    </tbody>
</table>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'TGL',
            'name' => 'tgl',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'NO. FPT',
            'name' => 'doc_ref',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'SUPPLIER',
            'name' => 'supplier_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL JATUH TEMPO',
            'name' => 'tgl_jatuh_tempo',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL BAYAR',
            'name' => 'tgl_bayar',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TOTAL BAYAR',
            'value' => function ($data) {
                return number_format($data['total_bayar']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'STATUS',
            'value' => function ($data) {
                $status = "";
                switch ($data['status']) {
                    case FPT_OPEN :
                        $status = "OPEN";
                        break;
                    case FPT_PAID :
                        $status = "PAID";
                        break;
                }
                return $status;
            },
            'htmlOptions' => array('style' => 'text-align: center;')
        )
    )
));
?>