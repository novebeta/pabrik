<h1><?= $title ?></h1>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <b><?= $from ?></b> s/d <b><?= $to ?></b></td>
    </tr>
    <? if ($_POST['pengirim']) { ?>
        <tr>
            <td>Pengirim</td>
            <td> : <b><?= $pengirim ?></b></td>
        </tr>
    <? } ?>
    <? if ($_POST['penerima']) { ?>
        <tr>
            <td>Penerima</td>
            <td> : <b><?= $penerima ?></b></td>
        </tr>
    <? } ?>
    <? if ($_POST['status'] != "") { ?>
        <tr>
            <td>Status</td>
            <td> : <b><?= ($status == 0 ? 'Belum diterima' : 'Diterima') ?></b></td>
        </tr>
    <? } ?>
    </tbody>
</table>
<?
$gridColums = array();
array_push($gridColums,
    array(
        'header' => 'Tanggal',
        'name' => 'tgl'
    ),
    array(
        'header' => 'No. Tanda Terima',
        'name' => 'doc_ref'
    ),
    array(
        'header' => 'Note',
        'name' => 'note'
    )
);
if (!$_POST['pengirim']) {
    array_push($gridColums,
        array(
            'header' => 'Pengirim',
            'name' => 'pengirim',
            'value' => function ($data) {
                return $GLOBALS['storLoc'][$data['pengirim']]['name'];
            }
        )
    );
}
if (!$_POST['penerima']) {
    array_push($gridColums,
        array(
            'header' => 'Penerima',
            'name' => 'penerima',
            'value' => function ($data) {
                return $GLOBALS['storLoc'][$data['penerima']]['name'];
            }
        )
    );
}
if ($_POST['status'] == "") {
    array_push($gridColums,
        array(
            'header' => 'Status',
            'name' => 'status',
            'value' => function ($data) {
                return $data['status'] == 0 ? 'Belum diterima' : 'Diterima';
            }
        )
    );
}
if (isset($_POST['show_detail'])) {
    array_push($gridColums,
        array(
            'header' => 'Kode',
            'name' => 'kode'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama'
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                return number_format($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Satuan',
            'name' => 'sat'
        )
    );
}
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));
?>