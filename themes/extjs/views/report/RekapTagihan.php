<h1>Rekap Tagihan</h1>

<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'NO. INV PAJAK',
            'name' => 'inv',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
            'footer' => 'Total'
        ),
//        array(
//            'header' => 'BADAN HUKUM',
//            'name' => 'nama_badan_hukum',
//            'htmlOptions' => array('style' => 'white-space: nowrap;')
//        ),
//        array(
//            'header' => 'NPWP',
//            'name' => 'npwp',
//            'htmlOptions' => array('style' => 'white-space: nowrap;')
//        ),
        array(
            'header' => 'CUSTOMERS',
            'name' => 'cabang',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL',
            'name' => 'tgl',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO FAKTUR PAJAK',
            'name' => 'no_faktur_pajak',
            'value' => function ($data) {
                $number = '00000000';
                if (preg_match("/\d+$/", $data['no_faktur_pajak'], $result) == 1)
                    list($number) = $result;
                return $number;
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'DPP',
            'name' => 'dpp',
            'value' => function ($data) {
                return number_format($data['dpp']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => number_format($total_dpp)
        ),
        array(
            'header' => 'PPN',
            'name' => 'ppn',
            'value' => function ($data) {
                return number_format($data['ppn']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => number_format($total_ppn)
        ),
        array(
            'header' => 'PAJAK',
            'name' => 'pajak',
            'value' => function ($data) {
                return number_format($data['pajak']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => number_format($total_pajak)
        )
    )
));
?>
