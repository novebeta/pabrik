<h1>Rekap Pembelian</h1>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Supplier</td>
        <td> : <?= $supplier ?></td>
    </tr>
    </tbody>
</table>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'TGL PO',
            'name' => 'tgl',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'KODE PRINCIPLE',
            'name' => 'supplier_code',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'NAMA PRINCIPLE',
            'name' => 'supplier_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'JENIS BAHAN',
            'name' => 'nama_tipe_material',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'NO. PO',
            'name' => 'doc_ref',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO. SJ',
            'name' => 'no_sj_supplier',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO. INV',
            'name' => 'no_inv_supplier',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL INVOICE',
            'name' => 'tgl_inv_supplier',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO. FAKTUR',
            'name' => 'no_faktur_pajak',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL FAKTUR',
            'name' => 'tgl_no_faktur_pajak',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL BRG DATANG',
            'name' => 'tgl_terima',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TOP',
            'name' => 'term_of_payment',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL JATUH TEMPO',
            'name' => 'tgl_jatuh_tempo',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL DIBAYAR',
            'name' => 'tgl_bayar',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'KODE BAHAN',
            'name' => 'kode_material',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NAMA BAHAN',
            'name' => 'nama_material',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'QTY',
            'value' => function ($data) {
                return number_format($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'SATUAN',
            'name' => 'sat',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'HARGA',
            'value' => function ($data) {
                return number_format($data['price']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'MU',
            'name' => 'currency',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TOTAL',
            'value' => function ($data) {
                return number_format($data['total']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'NILAI KURS',
            'value' => function ($data) {
                return number_format($data['IDR_rate']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'DPP',
            'value' => function ($data) {
                return number_format($data['dpp']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'PPN',
            'value' => function ($data) {
                return number_format($data['ppn']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'PPH',
            'value' => function ($data) {
                return number_format($data['pph']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'TOTAL',
            'value' => function ($data) {
                return number_format($data['total_bayar']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>