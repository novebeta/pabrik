<h1>Giro</h1>
<?
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Kode Customer',
            'name' => 'kode_customer'
        ),
        array(
            'header' => 'Nama Customer',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'No Giro',
            'name' => 'no_giro'
        ),
        array(
            'header' => 'Tgl Kliring',
            'name' => 'tgl_klr'
        ),
        array(
            'header' => 'Tgl Jatuh Tempo',
            'name' => 'tgl_jt'
        ),
        array(
            'header' => 'Bank Giro',
            'name' => 'bank_giro'
        ),
        array(
            'header' => 'Saldo',
            'name' => 'saldo',
            'value' => function ($data) {
                return number_format($data['saldo']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>