<?
/** @var Sj $surat_jalan */
//$detail_count = 1;
//$konsumen = Konsumen::model()->findByPk($jual->konsumen_id);
$nama = $alamat = $phone = '';
$nama = $surat_jalan->customer->nama_customer;
$alamat = $surat_jalan->customer->address;

//if ($konsumen == null) {
//    $nama = $jual->nama;
//    $alamat = $jual->alamat;
//} else {
//    $nama = $konsumen->nama;
//    $alamat = $konsumen->alamat;
//    $phone = $konsumen->phone;
//}
$almt_arr = explode('<br>', wordwrap(str_replace(array("\r", "\n"), " ", $alamat), 29, '<br>', true));
$almt1 = array_key_exists(0, $almt_arr) ? $almt_arr[0] : "";
$almt2 = array_key_exists(1, $almt_arr) ? $almt_arr[1] : "";
$almt3 = array_key_exists(2, $almt_arr) ? $almt_arr[2] : "";
$p = new BasePrint();
$format =
    chr(27) . chr(64) . //init
    chr(27) . chr(67) . chr(33) . // page lenght 33 lines
    chr(27) . chr(80) . //10cpi
    chr(27) . chr(33) . chr(16) . //double strike more bold
    $nama_faktur . "\r\n" .
    chr(27) . chr(33) . chr(32) . //double width
    app()->params['corp']['nama'] . "\r\n" .
    chr(27) . chr(33) . chr(0) . // reset font style
    app()->params['corp']['address'][0] . "\r\n" .
    app()->params['corp']['address'][1] . "\r\n" .
    $p->addLeftRight("No. Faktur", $surat_jalan->doc_ref_inv, 34) . "     " . $p->addLeftLabel("Nama", $nama, 10, 29, " ") . "\r\n" .
    $p->addLeftRight("Tgl. Faktur", sql2date($surat_jalan->tgl_inv), 34) . "     " . $p->addLeftLabel("Alamat", $almt1, 10, 29, " ") . "\r\n" .
    $p->addLeftRight("No. SO", $surat_jalan->sales->doc_ref, 34) . "     " . $p->addLeftLabel("", $almt2, 10, 29, " ") . "\r\n" .
    $p->addLeftRight("Salesman", $surat_jalan->sales->salesman->salesman_code, 34) . "     " . $p->addLeftLabel("Telp", $phone, 10, 29, " ") . "\r\n" .
    chr(27) . chr(77) . //12cpi
    "================================================================================================\r\n" .
    " NAMA BARANG                                    SAT      JML       HARGA    POTONGAN     NOMINAL\r\n" .
    "================================================================================================\r\n";
$no = 1;
foreach ($items as $item) {
    $format .= $p->addDetailsItemSales(
            $item['nama_barang'],$item['sat'], $item['qty'], $item['price'], $item['totalpot'], $item['price_jual']) . "\r\n";
    $no++;
}
for ($no; $no < 12; $no++) {
    $format .= "                                                                                                \r\n";
}
$user = Users::model()->findByPk($surat_jalan->inv_id_user);
$format .=
    "================================================================================================\r\n" .
    $p->addLeftRightWithMax("# ".spellNumber($surat_jalan->total)." #", "Jumlah", 69, 14, " ") . " " . str_pad(number_format($surat_jalan->tot_bruto, 0), 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax("Keterangan :                                               Hormat Kami,", "Disc", 74, 9, " ") . " " . str_pad(number_format(($surat_jalan->tot_pot1 + $surat_jalan->tot_pot2 + $surat_jalan->tot_pot3), 0), 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax("- Pembayaran dgn cek/giro dianggap lunas jika sudah cair", "Total", 69, 14, " ") . " " . str_pad(number_format($surat_jalan->total, 0), 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax("- Pembayaran : ", "", 69, 14, " ") . " " . str_pad("", 11, " ", STR_PAD_LEFT) . "\r\n" .
    $p->addLeftRightWithMax("", "", 20, 41, " ") . " " . "" . "\r\n" .
    "                                                                                                \r\n" .
    "                                                                                                \r\n" .
    "                                                                                                \r\n" .
    chr(13) . chr(10) . chr(12) . chr(27) . chr(64); // cr lf ff init
echo base64_encode($format);
//echo "<pre>" . $format . "</pre>";