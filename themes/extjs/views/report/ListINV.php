<h1>List Invoice</h1>

<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'NO. SJ',
            'name' => 'sj',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'NO. INV PAJAK',
            'name' => 'inv',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL',
            'name' => 'tgl',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'CABANG',
            'name' => 'cabang',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'BADAN HUKUM',
            'name' => 'nama_badan_hukum',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'ALAMAT BADAN HUKUM',
            'name' => 'alamat_badan_hukum',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'ALAMAT CABANG',
            'name' => 'alamat',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'KODE',
            'name' => 'kode',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'KETERANGAN',
            'name' => 'keterangan',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'BATCH',
            'name' => 'batch',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'JML',
            'value' => function ($data) {
                return number_format($data['jml']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'SATUAN',
            'name' => 'satuan'
        ),
        array(
            'header' => 'EXP',
            'value' => function ($data) {
                return $data['tgl_exp'] . ($this->is_excel ? "." : "");
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'HARGA PAJAK+PPN',
            'value' => function ($data) {
                return number_format($data['dpp_ppn']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'HARGA DPP',
            'value' => function ($data) {
                return number_format($data['dpp']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'TOTAL ALL PAJAK+DPP',
            'value' => function ($data) {
                return number_format($data['total_dpp_ppn']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => number_format($total_dpp_ppn)
        ),
        array(
            'header' => 'TOTAL DPP PAJAK',
            'value' => function ($data) {
                return number_format($data['total_dpp']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => number_format($total_dpp)
        ),
        array(
            'header' => 'PPN',
            'value' => function ($data) {
                return number_format($data['total_ppn']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => number_format($total_ppn)
        )
    )
));
?>