<h1>EFaktur</h1>

<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'FK<hr>LT<hr>OF',
            'name' => 'col1'
        ),
        array(
            'header' => 'KD_JENIS_TRANSAKSI<hr>NPWP<hr>KODE_OBJEK',
            'name' => 'col2'
        ),
        array(
            'header' => 'FG_PENGGANTI<hr>NAMA<hr>NAMA',
            'name' => 'col3'
        ),
        array(
            'header' => 'NOMOR_FAKTUR<hr>JALAN<hr>HARGA_SATUAN',
            'name' => 'col4'
        ),
        array(
            'header' => 'MASA_PAJAK<hr>BLOK<hr>JUMLAH_BARANG',
            'name' => 'col5'
        ),
        array(
            'header' => 'TAHUN_PAJAK<hr>NOMOR<hr>HARGA_TOTAL',
            'name' => 'col6'
        ),
        array(
            'header' => 'TANGGAL_FAKTUR<hr>RT<hr>DISKON',
            'name' => 'col7'
        ),
        array(
            'header' => 'NPWP<hr>RW<hr>DPP',
            'name' => 'col8'
        ),
        array(
            'header' => 'NAMA<hr>KECAMATAN<hr>PPN',
            'name' => 'col9'
        ),
        array(
            'header' => 'ALAMAT_LENGKAP<hr>KELURAHAN<hr>TARIF_PPNBM',
            'name' => 'col10'
        ),
        array(
            'header' => 'JUMLAH_DPP<hr>KABUPATEN<hr>PPNBM',
            'name' => 'col11'
        ),
        array(
            'header' => 'JUMLAH_PPN<hr>PROPINSI<hr>_',
            'name' => 'col12'
        ),
        array(
            'header' => 'JUMLAH_PPNBM<hr>KODE_POS<hr>_',
            'name' => 'col13'
        ),
        array(
            'header' => 'ID_KETERANGAN_TAMBAHAN<hr>NOMOR_TELEPON<hr>_',
            'name' => 'col14'
        ),
        array(
            'header' => 'FG_UANG_MUKA<hr>_<hr>_',
            'name' => 'col15'
        ),
        array(
            'header' => 'UANG_MUKA_DPP<hr>_<hr>_',
            'name' => 'col16'
        ),
        array(
            'header' => 'UANG_MUKA_PPN<hr>_<hr>_',
            'name' => 'col17'
        ),
        array(
            'header' => 'UANG_MUKA_PPNBM<hr>_<hr>_',
            'name' => 'col18'
        ),
        array(
            'header' => 'REFERENSI<hr>_<hr>_',
            'name' => 'col19'
        )
    )
));
?>
