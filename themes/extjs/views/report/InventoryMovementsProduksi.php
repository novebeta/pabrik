<h1>Inventory Movements (Produksi)</h1>

<h3>FROM : <?= $from ?></h3>

<h3>TO : <?= $to ?></h3>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Batch',
            'name' => 'batch'
        ),
        array(
            'header' => 'Saldo Awal',
            'name' => 'before',
            'value' => function ($data) {
                return number_format($data['before'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Produksi',
            'name' => 'produksi',
            'value' => function ($data) {
                return number_format($data['produksi'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Kirim ke GPJ',
            'name' => 'kirimGPJ',
            'value' => function ($data) {
                return number_format($data['kirimGPJ'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Saldo Akhir',
            'name' => 'after',
            'value' => function ($data) {
                return number_format($data['after'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
    )
));
?>