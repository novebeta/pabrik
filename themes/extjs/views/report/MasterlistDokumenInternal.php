<h1>MASTERLIST DOKUMEN INTERNAL</h1>

<h3>Bagian : <?= $activity ?></h3>
<h3>Jenis Dokumen : <?= $tipe ?></h3>
<?
$gridColums = array();
array_push($gridColums,
    array(
        'header' => 'No. Dokumen',
        'name' => 'nomor'
    ),
    array(
        'header' => 'Judul Dokumen',
        'name' => 'judul'
    ),
    array(
        'header' => 'Rev 00',
        'name' => 'tgl_rev0',
        'htmlOptions' => array('style' => 'text-align: center;')
    ),
    array(
        'header' => 'Rev 01',
        'name' => 'tgl_rev1',
        'htmlOptions' => array('style' => 'text-align: center;')
    ),
    array(
        'header' => 'Rev 02',
        'name' => 'tgl_rev2',
        'htmlOptions' => array('style' => 'text-align: center;')
    ),
    array(
        'header' => 'Rev 03',
        'name' => 'tgl_rev3',
        'htmlOptions' => array('style' => 'text-align: center;')
    ),
    array(
        'header' => 'Rev 04',
        'name' => 'tgl_rev4',
        'htmlOptions' => array('style' => 'text-align: center;')
    ),
    array(
        'header' => 'Rev 05',
        'name' => 'tgl_rev5',
        'htmlOptions' => array('style' => 'text-align: center;')
    )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));
?>
