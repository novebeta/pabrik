<h1>Rekap SJ Supplier</h1>
<?
switch ($status) {
    case "All":
        break;
    case SJ_SUPPLIER_OPEN :
        $status = "OPEN";
        break;
    case SJ_SUPPLIER_INVOICED :
        $status = "INVOICED";
        break;
    case SJ_SUPPLIER_FPT :
        $status = "FPT";
        break;
    case SJ_SUPPLIER_PAID :
        $status = "PAID";
        break;
    default :
        $status = "All";
}
?>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Supplier</td>
        <td> : <?= $supplier ?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td> : <?= $status ?></td>
    </tr>
    </tbody>
</table>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'NO. SJ',
            'name' => 'no_sj_supplier',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'TGL SJ',
            'name' => 'tgl_sj_supplier',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'SUPPLIER',
            'name' => 'supplier_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL TERIMA',
            'name' => 'tgl_terima',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'STATUS',
            'value' => function ($data) {
                $status = "";
                switch ($data['status']) {
                    case SJ_SUPPLIER_OPEN :
                        $status = "OPEN";
                        break;
                    case SJ_SUPPLIER_INVOICED :
                        $status = "INVOICED";
                        break;
                    case SJ_SUPPLIER_FPT :
                        $status = "FPT";
                        break;
                    case SJ_SUPPLIER_PAID :
                        $status = "PAID";
                        break;
                }
                return $status;
            },
            'htmlOptions' => array('style' => 'text-align: center;')
        )
    )
));
?>