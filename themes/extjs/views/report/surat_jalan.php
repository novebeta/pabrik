<?
/** @var Sj $surat_jalan */
//$detail_count = 1;
//$konsumen = Konsumen::model()->findByPk($jual->konsumen_id);
$nama = $alamat = $phone = '';
$nama = $surat_jalan->customer->nama_customer;
$alamat = $surat_jalan->customer->address;

//if ($konsumen == null) {
//    $nama = $jual->nama;
//    $alamat = $jual->alamat;
//} else {
//    $nama = $konsumen->nama;
//    $alamat = $konsumen->alamat;
//    $phone = $konsumen->phone;
//}
$almt_arr = explode('<br>', wordwrap(str_replace(array("\r", "\n"), " ", $alamat), 29, '<br>', true));
$almt1 = array_key_exists(0, $almt_arr) ? $almt_arr[0] : "";
$almt2 = array_key_exists(1, $almt_arr) ? $almt_arr[1] : "";
$almt3 = array_key_exists(2, $almt_arr) ? $almt_arr[2] : "";
$p = new BasePrint();
$format =
    chr(27) . chr(64) . //init
    chr(27) . chr(67) . chr(33) . // page lenght 33 lines
    chr(27) . chr(80) . //10cpi
    chr(27) . chr(33) . chr(16) . //double strike more bold
    $nama_faktur . "\r\n" .
    chr(27) . chr(33) . chr(32) . //double width
    app()->params['corp']['nama'] . "\r\n" .
    chr(27) . chr(33) . chr(0) . // reset font style
    app()->params['corp']['address'][0] . "\r\n" .
    app()->params['corp']['address'][1] . "\r\n" .
    $p->addLeftRight("No. SJ", $surat_jalan->doc_ref, 34) . "     " . $p->addLeftLabel("Nama", $nama, 7, 32, " ") . "\r\n" .
    $p->addLeftRight("Tgl. ", sql2date($surat_jalan->tgl), 34) . "     " . $p->addLeftLabel("Alamat", $almt1, 7, 32, " ") . "\r\n" .
    $p->addLeftRight("No. SO", $surat_jalan->sales->doc_ref, 34) . "     " . $p->addLeftLabel("", $almt2, 7, 32, " ") . "\r\n" .
    $p->addLeftRight("", "", 34) . "     " . $p->addLeftLabel("Telp", $phone, 7, 32, " ") . "\r\n" .
    chr(27) . chr(77) . //12cpi
    "================================================================================================\r\n" .
    " NAMA BARANG                                    SAT      JML NOTE                               \r\n" .
    "================================================================================================\r\n";
$no = 1;
foreach ($items as $item) {
    $format .= $p->addDetailsItemDO(
            $item['nama_barang'],$item['sat'], $item['qty'], $item['ket']) . "\r\n";
    $no++;
}
for ($no; $no < 12; $no++) {
    $format .= "                                                                                                \r\n";
}
$format .=
    "================================================================================================\r\n" .
    " Barang-barang diatas telah diperiksa dan diterima dengan baik.                                 \r\n" .
    "     Disetujui,         Gudang,         Supir,         Yang Menerima,       Hormat Kami,        \r\n" .
    "                                                                                                \r\n" .
    "                                                                                                \r\n" .
    "                                                                                                \r\n" .
    " NB : Claim berlaku hanya dalam waktu 3 hari terhitung dari tanggal terima barang tsb.          \r\n" .
    chr(13) . chr(10) . chr(12) . chr(27) . chr(64); // cr lf ff init
echo base64_encode($format);
//echo "<pre>" . $format . "</pre>";