<h1>Rekap Invoice Supplier</h1>
<?
if ($_POST['show_belum_dibayar']) {
    ?>
    <h3>BELUM DIBAYAR</h3>
    <?
}
if ($_POST['show_lewat_jatuh_tempo']) {
    ?>
    <h3>BELUM DIBAYAR DAN LEWAT JATUH TEMPO</h3>
    <?
}
switch ($status) {
    case "All":
        break;
    case SJ_SUPPLIER_INVOICED :
        $status = "OPEN";
        break;
    case SJ_SUPPLIER_FPT :
        $status = "FPT";
        break;
    case SJ_SUPPLIER_PAID :
        $status = "PAID";
        break;
    default :
        $status = "All";
}
?>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Supplier</td>
        <td> : <?= $supplier ?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td> : <?= $status ?></td>
    </tr>
    </tbody>
</table>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'TGL TERIMA',
            'name' => 'tgl_terima_inv',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO. INVOICE',
            'name' => 'no_sj_supplier',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'TGL INVOICE',
            'name' => 'tgl_sj_supplier',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'SUPPLIER',
            'name' => 'supplier_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'NO. FAKTUR PAJAK',
            'name' => 'no_faktur_pajak',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'TGL FAKTUR PAJAK',
            'name' => 'tgl_no_faktur_pajak',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'MU',
            'name' => 'currency',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TOTAL BAYAR',
            'value' => function ($data) {
                return number_format($data['total_bayar']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'T.O.P.',
            'name' => 'term_of_payment',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL JATUH TEMPO',
            'name' => 'tgl_jatuh_tempo',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'STATUS',
            'value' => function ($data) {
                $status = "";
                switch ($data['status']) {
                    case SJ_SUPPLIER_INVOICED :
                        $status = "OPEN";
                        break;
                    case SJ_SUPPLIER_FPT :
                        $status = "FPT";
                        break;
                    case SJ_SUPPLIER_PAID :
                        $status = "PAID";
                        break;
                }
                return $status;
            },
            'htmlOptions' => array('style' => 'text-align: center;')
        )
    )
));
?>