<h1>Kartu Persediaan Produk Jadi</h1>
<h2><?= $kode ?> - <?= $nama ?></h2>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <b><?= $from ?></b> s/d <b><?= $to ?></b></td>
    </tr>
    <? if ($no_batch) { ?>
        <tr>
            <td>BATCH</td>
            <td> : <b><?= $no_batch ?></b></td>
        </tr>
    <? } ?>
    </tbody>
</table>
<?
$gridColums = array();
array_push($gridColums,
    array(
        'header' => 'TANGGAL MASUK',
        'name' => 'tgl_masuk'
    )
);
if (!$no_batch) {
    array_push($gridColums,
        array(
            'header' => 'NO. BATCH',
            'name' => 'batch_masuk'
        )
    );
}
array_push($gridColums,
    array(
        'header' => 'QTY MASUK',
        'name' => 'qty_masuk',
        'value' => function ($data) {
            return $data['qty_masuk'] == 0 ? "" : number_format($data['qty_masuk']);
        },
        'htmlOptions' => array('style' => 'text-align: right; background-color: #D0FBD3')
    ),
    array(
        'header' => 'TANGGAL KELUAR',
        'name' => 'tgl_keluar'
    )
);
if (!$no_batch) {
    array_push($gridColums,
        array(
            'header' => 'NO. BATCH',
            'name' => 'batch_keluar'
        )
    );
}
array_push($gridColums,
    array(
        'header' => 'QTY KELUAR',
        'name' => 'qty_keluar',
        'value' => function ($data) {
            return $data['qty_keluar'] == 0 ? "" : number_format($data['qty_keluar']);
        },
        'htmlOptions' => array('style' => 'text-align: right; background-color: #FBD0D0')
    ),
    array(
        'header' => 'TUJUAN',
        'name' => 'tujuan'
    ),
    array(
        'header' => 'SISA',
        'name' => 'after',
        'value' => function ($data) {
            return number_format($data['after']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));
?>