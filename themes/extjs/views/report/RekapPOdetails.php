<h1>Rekap Purchase Order Details</h1>
<?
switch ($status) {
    case "All":
        break;
    case P0_OPEN                :
        $status = "OPEN";
        break;
    case P0_RELEASED            :
        $status = "RELEASED";
        break;
    case PO_PARTIALLY_RECEIVED  :
        $status = "PARTIALLY RECEIVED";
        break;
    case PO_RECEIVED            :
        $status = "RECEIVED";
        break;
    case PO_CANCELED            :
        $status = "CANCELED";
        break;
    case PO_CLOSED              :
        $status = "CLOSED";
        break;
    default                     :
        $status = "All";
}
switch ($tipe_material) {
    case "All":
        break;
    case MAT_RAW_MATERIAL :
        $tipe_material = "RAW MATERIAL";
        break;
    case MAT_PACKAGING :
        $tipe_material = "PACKAGING";
        break;
    case MAT_GENERAL :
        $tipe_material = "GENERAL";
        break;
    default :
        $tipe_material = "All";
}
?>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Supplier</td>
        <td> : <?= $supplier ?></td>
    </tr>
    <tr>
        <td>Tipe Barang</td>
        <td> : <?= $tipe_material ?></td>
    </tr>
    <tr>
        <td>Status</td>
        <td> : <?= $status ?></td>
    </tr>
    </tbody>
</table>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'NO. PO',
            'name' => 'doc_ref',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
        ),
        array(
            'header' => 'TGL',
            'name' => 'tgl',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'SUPPLIER',
            'name' => 'supplier_name',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'STATUS',
            'value' => function ($data) {
                $status = "";
                switch ($data['status']) {
                    case P0_OPEN                :
                        $status = "OPEN";
                        break;
                    case P0_RELEASED            :
                        $status = "RELEASED";
                        break;
                    case PO_PARTIALLY_RECEIVED  :
                        $status = "PARTIALLY RECEIVED";
                        break;
                    case PO_RECEIVED            :
                        $status = "RECEIVED";
                        break;
                    case PO_CANCELED            :
                        $status = "CANCELED";
                        break;
                    case PO_CLOSED              :
                        $status = "CLOSED";
                        break;
                }
                return $status;
            },
            'htmlOptions' => array('style' => 'text-align: center;')
        ),
        array(
            'header' => 'KODE MATERIAL',
            'name' => 'kode_material',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'QTY',
            'value' => function ($data) {
                return number_format($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'QTY TERIMA',
            'value' => function ($data) {
                return number_format($data['qty_terima']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'SATUAN',
            'name' => 'sat',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
    )
));
?>