<h1>Rekap Invoice Hutang</h1>

<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'NO. INV PAJAK',
            'name' => 'inv',
            'htmlOptions' => array('style' => 'white-space: nowrap;'),
            'footer' => 'Total'
        ),
//        array(
//            'header' => 'BADAN HUKUM',
//            'name' => 'nama_badan_hukum',
//            'htmlOptions' => array('style' => 'white-space: nowrap;')
//        ),
//        array(
//            'header' => 'NPWP',
//            'name' => 'npwp',
//            'htmlOptions' => array('style' => 'white-space: nowrap;')
//        ),
        array(
            'header' => 'SUPPLIER',
            'name' => 'cabang',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'TGL',
            'name' => 'tgl',
            'htmlOptions' => array('style' => 'white-space: nowrap;')
        ),
        array(
            'header' => 'Total',
            'name' => 'grand_total',
            'value' => function ($data) {
                return number_format($data['grand_total']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => number_format($grand_total)
        )
    )
));
?>
