<h1>Inventory Movements</h1>

<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$gridColums = array();
array_push($gridColums,
    array(
        'header' => 'Kode Barang',
        'name' => 'kode'
    ),
    array(
        'header' => 'Nama Barang',
        'name' => 'nama'
    )
);
if (isset($_POST['show_expired_date'])) {
    array_push($gridColums,
        array(
            'header' => 'Exp. Date',
            'name' => 'tgl_expired'
        )
    );
}
if (isset($_POST['show_no_batch'])) {
    array_push($gridColums,
        array(
            'header' => 'No. Batch',
            'name' => 'no_batch'
        )
    );
}
array_push($gridColums,
    array(
        'header' => 'Saldo Awal',
        'name' => 'before',
        'value' => function ($data) {
            return number_format($data['before']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Masuk',
        'name' => 'suppin',
        'value' => function ($data) {
            return number_format($data['suppin']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Keluar',
        'name' => 'suppout',
        'value' => function ($data) {
            return number_format($data['suppout']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Saldo Akhir',
        'name' => 'after',
        'value' => function ($data) {
            return number_format($data['after']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));
?>