<h1>Laporan Produksi</h1>

<h3>FROM : <?= $from ?></h3>

<h3>TO : <?= $to ?></h3>
<?
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'mergeColumns' => $format == 'excel' ? array('qty_mixing') : array('no_bom', 'kode_barang', 'nama_barang', 'kode_formula', 'batch_size', 'qty_per_pot', 'batch', 'nama_grup', 'tgl_prepare', 'tgl_mixing', 'qty_mixing'),
    'mergeType' => 'nested',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'NO. BOM',
            'name' => 'no_bom'
        ),
        array(
            'header' => 'KODE',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'NAMA',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'KODE FORMULA',
            'name' => 'kode_formula'
        ),
        array(
            'header' => 'BATCH SIZE (gr)',
            'name' => 'batch_size',
            'value' => function ($data) {
                return number_format($data['batch_size'], 0, ",", ".");
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'ISI KEMASAN (gr)',
            'name' => 'qty_per_pot',
            'value' => function ($data) {
                return number_format($data['qty_per_pot'], 0, ",", ".");
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'NO. BATCH',
            'name' => 'batch'
        ),
        array(
            'header' => 'PRODUK',
            'name' => 'nama_grup'
        ),
        array(
            'header' => 'TGL. TERIMA BB',
            'name' => 'tgl_prepare'
        ),
        array(
            'header' => 'TGL. MIXING',
            'name' => 'tgl_mixing'
        ),
        array(
            'header' => 'HASIL MIXING (gr)',
            'name' => 'qty_mixing',
            'value' => function ($data) {
                return number_format($data['qty_mixing'], 0, ",", ".");
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'TGL. KIRIM',
            'name' => 'tgl_kirim'
        ),
        array(
            'header' => 'NO. LOT',
            'name' => 'no_lot'
        ),
        array(
            'header' => 'QTY',
            'name' => 'qty_batch',
            'value' => function ($data) {
                return number_format($data['qty_batch'], 0, ",", ".");
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
    )
));
if ($format == 'excel') {
    ?>
    <br>
    <table>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="4" style="text-align: center;"><? echo $now; ?><br><br><br>(...........................)</td>
        </tr>
    </table>
    <?
}
?>