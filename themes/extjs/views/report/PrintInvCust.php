<? /** @var Sj $surat_jalan */ ?>
<html>
<head>

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/normalize.css">
    <style>
        @page {
            margin: 0
        }

        body {
            margin: 0
        }

        table {
            border-collapse: collapse;
        }

        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            page-break-after: always;
        }

        /** Paper sizes **/

        body.fo .sheet {
            width: 279mm;
            height: 228mm
        }

        body.A3 .sheet {
            width: 297mm;
            height: 419mm
        }

        body.A3.landscape .sheet {
            width: 420mm;
            height: 296mm
        }

        body.A4 .sheet {
            width: 210mm;
            height: 296mm
        }

        body.A4.landscape .sheet {
            width: 297mm;
            height: 209mm
        }

        body.A5 .sheet {
            width: 148mm;
            height: 209mm
        }

        body.A5.landscape .sheet {
            width: 210mm;
            height: 147mm
        }

        /** Padding area **/
        .sheet.padding-10mm {
            padding: 10mm
        }

        .sheet.padding-15mm {
            padding: 15mm
        }

        .sheet.padding-20mm {
            padding: 20mm
        }

        .sheet.padding-25mm {
            padding: 25mm
        }

        /** For screen preview **/
        @media screen {
            body {
                background: #e0e0e0
            }

            .sheet {
                background: white;
                box-shadow: 0 .5mm 2mm rgba(0, 0, 0, .3);
                margin: 5mm;
            }
        }

        /** Fix for Chrome issue #273306 **/
        @media print {
            body.A3.landscape {
                width: 420mm
            }

            body.A3, body.A4.landscape {
                width: 297mm
            }

            body.A4, body.A5.landscape {
                width: 210mm
            }

            body.A5 {
                width: 148mm
            }

            body.fo {
                width: 279mm
            }
        }
    </style>
</head>
<body class="fo">
<section class="sheet padding-10mm">
    <table style="height: 93px; width: 100%;">
        <tbody>
        <tr>
            <td style="width: 25%; text-align: left; vertical-align: top;" rowspan="3">
                <h4><strong>CV. JOGJA FOAMINDO</strong></h4>
            </td>
            <td style="width: 50%;" colspan="2">
                JOGJA&nbsp;<?= date_format(date_create($surat_jalan->tgl_inv), 'd/m/Y'); ?></td>
            <td style="width: 25%;  text-align: center;" rowspan="3">
                <h2><strong>FAKTUR</strong></h2>
            </td>
        </tr>
        <tr>
            <td style="">Kepada Yth :</td>
            <td style="">
            </td>
        </tr>
        <tr>
            <td style="width: 339px;" colspan="2"><?= $surat_jalan->sales->customer->nama_customer; ?><br>
                <?= $surat_jalan->sales->customer->alamat_badan_hukum; ?></td>
        </tr>
        </tbody>
    </table>
    <table style="height: 48px; width: 719px;">
        <tbody>
        <tr>
            <td style="width: 90px;">Bukti</td>
            <td style="width: 621px;">:<?= $surat_jalan->doc_ref_inv; ?></td>
        </tr>
        <tr>
            <td style="width: 90px;">Ref</td>
            <td style="width: 621px;">:<?= $surat_jalan->doc_ref ?></td>
        </tr>
        </tbody>
    </table>
    <table style="height: 136px; width: 100%;">
        <tbody>
        <tr>
            <td style="border: 1px solid #000;width: 50%;" colspan="2">NAMA BARANG</td>
            <td style="border: 1px solid #000;width: 5%; text-align: center;">QTY</td>
            <td style="border: 1px solid #000;width: 15%; text-align: center;">HARGA</td>
            <td style="border: 1px solid #000;width: 15%; text-align: center;">TOTAL</td>
            <td style="border: 1px solid #000;width: 15%; text-align: center;">DISCOUNT</td>
        </tr>
        <? foreach ($items as $item) : ?>
            <tr>
                <td style="font-size: small;border-left: 1px solid #000;border-right: 1px solid #000;width: 30%; text-align: left;"
                    colspan="2">
                    &nbsp;<?= $item['nama_barang']; ?>
                </td>
                <td style="font-size: small;border-left: 1px solid #000;border-right: 1px solid #000;text-align: right;">
                    &nbsp;<?= $item['qty'] . ' ' . $item['sat']; ?>
                </td>
                <td style="font-size: small;border-left: 1px solid #000;border-right: 1px solid #000;text-align: right;">
                    &nbsp;<?= number_format($item['price'], 2); ?>
                </td>
                <td style="font-size: small;border-left: 1px solid #000;border-right: 1px solid #000;text-align: right;">
                    &nbsp;<?= number_format($item['price_jual'], 2); ?>
                </td>
                <td style="font-size: small;border-left: 1px solid #000;border-right: 1px solid #000;text-align: right;">
                    &nbsp;<?= number_format($item['totalpot'], 2); ?>
                </td>
            </tr>
        <? endforeach; ?>
        <? for ($i = count($items); $i < 11; $i++) : ?>
            <tr>
                <td style="border-left: 1px solid #000;border-right: 1px solid #000;text-align: left;" colspan="2">
                    &nbsp;
                </td>
                <td style="border-left: 1px solid #000;border-right: 1px solid #000;">&nbsp;</td>
                <td style="border-left: 1px solid #000;border-right: 1px solid #000;">&nbsp;</td>
                <td style="border-left: 1px solid #000;border-right: 1px solid #000;text-align: right;">&nbsp;</td>
                <td style="border-left: 1px solid #000;border-right: 1px solid #000;text-align: right;">&nbsp;</td>
            </tr>
        <? endfor; ?>
        <tr>
            <td style="font-size: small;border-top: 1px solid #000;" colspan="4" rowspan="2">Terbilang
                :&nbsp;<?= spellNumber($surat_jalan->total); ?>
            </td>
            <td style="border-left: 1px solid #000;border-top: 1px solid #000;">Jumlah&nbsp;:</td>
            <td style="border-top: 1px solid #000;border-right: 1px solid #000;text-align: right;">
                &nbsp;<?= number_format($surat_jalan->tot_bruto, 2); ?></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #000;">Disc :</td>
            <td style="border-right: 1px solid #000;text-align: right;">
                &nbsp;<?= number_format(($surat_jalan->tot_pot1 + $surat_jalan->tot_pot2 + $surat_jalan->tot_pot3), 2); ?>
            </td>
        </tr>
        <tr>
            <td style="font-size: small;text-align: left; vertical-align: top;" colspan="3" rowspan="2">Keterangan :</td>
            <td style="text-align: center;">Hormat&nbsp;Kami</td>
            <td style="border-left: 1px solid #000;">&nbsp;</td>
            <td style="border-right: 1px solid #000;text-align: right;">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 137px;">&nbsp;</td>
            <td style="border-left: 1px solid #000;border-bottom: 1px solid #000;width: 132px;">Total :</td>
            <td style="border-right: 1px solid #000;border-bottom: 1px solid #000;width: 142px; text-align: right;">
                &nbsp;<?= number_format($surat_jalan->total, 2); ?></td>
        </tr>
        <tr>
            <td style="">&nbsp;</td>
            <td style="">&nbsp;</td>
            <td style="">&nbsp;</td>
            <td style="">&nbsp;</td>
            <td style="">&nbsp;</td>
            <td style="">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 76px; font-size: small; text-align: left; vertical-align: top;">Catatan :</td>
            <td style="width: 1217px; font-size: small;" colspan="2">-&nbsp;Pembayarandengan cekqu/giro diamnggap lunas
                jika
                sudah cair
            </td>
            <td style="width: 110px;">&nbsp;</td>
            <td style="width: 132px;">&nbsp;</td>
            <td style="width: 142px;">&nbsp;</td>
        </tr>
        <tr>
            <td style="">&nbsp;</td>
            <td style=" font-size: small;" colspan="2">-&nbsp;Pembayaran : 45 Hari.</td>
            <td style="text-align: center;">&nbsp;(..................)</td>
            <td style="">&nbsp;</td>
            <td style="">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</section>
</body>
</html>