<h1>Inventory Card</h1>
<h2><?= $kode ?> - <?= $nama ?></h2>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <b><?= $from ?></b> s/d <b><?= $to ?></b></td>
    </tr>
    <? if ($exp) { ?>
        <tr>
            <td>TGL. EXP.</td>
            <td> : <b><?= $exp ?></b></td>
        </tr>
    <? } ?>
    <? if ($no_batch) { ?>
        <tr>
            <td>BATCH</td>
            <td> : <b><?= $no_batch ?></b></td>
        </tr>
    <? } ?>
    </tbody>
</table>
<?
$gridColums = array();
array_push($gridColums,
    array(
        'header' => 'Tanggal',
        'name' => 'tgl'
    ),
    array(
        'header' => 'No. Referensi',
        'name' => 'doc_ref'
    ),
    array(
        'header' => 'Tipe',
        'name' => 'mvt'
    )
);
if (!$exp) {
    array_push($gridColums,
        array(
            'header' => 'Tgl. Expired',
            'name' => 'tgl_exp'
        )
    );
}
if (!$no_batch) {
    array_push($gridColums,
        array(
            'header' => 'No. Batch',
            'name' => 'batch'
        )
    );
}
array_push($gridColums,
    array(
        'header' => 'Saldo Awal',
        'name' => 'before',
        'value' => function ($data) {
            return number_format($data['before']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Masuk',
        'name' => 'in',
        'value' => function ($data) {
            return number_format($data['in']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Keluar',
        'name' => 'out',
        'value' => function ($data) {
            return number_format($data['out']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Saldo Akhir',
        'name' => 'after',
        'value' => function ($data) {
            return number_format($data['after']);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));
?>