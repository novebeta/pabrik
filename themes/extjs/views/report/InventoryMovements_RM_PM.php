<h1>Inventory Movements Material</h1>

<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$gridColums = array();
array_push($gridColums,
    array(
        'header' => 'Item Code',
        'name' => 'kode'
    ),
    array(
        'header' => 'Item Name',
        'name' => 'nama'
    )
);
if (isset($_POST['show_expired_date'])) {
    array_push($gridColums,
        array(
            'header' => 'Exp. Date',
            'name' => 'tgl_expired',
            'htmlOptions' => array('style' => 'text-align: center;')
        )
    );
}
if (isset($_POST['show_no_lot'])) {
    array_push($gridColums,
        array(
            'header' => 'No. Lot',
            'name' => 'no_lot'
        )
    );
}
if (isset($_POST['show_no_qc'])) {
    array_push($gridColums,
        array(
            'header' => 'No. QC',
            'name' => 'no_qc'
        )
    );
}
if (isset($_POST['show_supplier'])) {
    array_push($gridColums,
        array(
            'header' => 'Supplier',
            'name' => 'supplier_name'
        )
    );
}
array_push($gridColums,
    array(
        'header' => 'Satuan',
        'name' => 'sat',
        'htmlOptions' => array('style' => 'text-align: center;')
    ),
    array(
        'header' => 'Saldo Awal',
        'name' => 'before',
        'value' => function ($data) {
            return number_format($data['before'], 2);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Masuk',
        'name' => 'suppin',
        'value' => function ($data) {
            return number_format($data['suppin'], 2);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Keluar',
        'name' => 'suppout',
        'value' => function ($data) {
            return number_format($data['suppout'], 2);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    ),
    array(
        'header' => 'Saldo Akhir',
        'name' => 'after',
        'value' => function ($data) {
            return number_format($data['after'], 2);
        },
        'htmlOptions' => array('style' => 'text-align: right;')
    )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));
?>
