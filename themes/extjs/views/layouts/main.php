<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <link rel="shortcut icon" href="<?php echo bu(); ?>/images/logo-small.png"/>
    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/default.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/GroupSummary.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 12px;
            font-family: Candara;
        }

        .container {
            display: table;
        }

        .search-item-table {
            display: table-row;
            color: #8B1D51;
        }

        .cell4 {
            display: table-cell;
            border: solid;
            border-width: thin;
            padding-left: 5px;
            padding-right: 5px;
        }

        .custom-sales-details .x-grid-row-selected .x-grid-cell-first {
            padding-left: 5px;
        }

        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .x-grid3-cell, /* Normal grid cell */
            .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                box-sizing: border-box;
            }
        }

        #mainpanel .x-tab-panel-body {
            background-image: url(images/logo-large.png);
            background-repeat: no-repeat;
            background-position: center center;
            background-size: 1000px auto;
        }

        .mfcombobox {
            width: 100%;
            border: 1px solid #bbb;
            border-collapse: collapse;
        }
        .mfcombobox th{
            font-weight: bold;
        }
        .mfcombobox td, .mfcombobox th {
            border: 1px solid #ccc;
            border-collapse: collapse;
            padding: 5px;
        }
    </style>
</head>
<body>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGrid.js"></script>
<script>
    Ext.namespace('jun');
    var DEFAULT_JATUH_TEMPO_PURCHASE = 45;
    var DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    var SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    var SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    var SYSTEM_LOGO = '<img src="<?= bu(); ?>/images/logo-small.png" alt="logo PBU"/>';
    var SYSTEM_ICON = '<img src="<?= bu(); ?>/images/logo-small.png" alt="icon PBU" height="32" style="margins:5;"/>';
    //id tipe barang
    var BRG_PRODUK_LOKAL = <?= BRG_PRODUK_LOKAL ?>;
    var BRG_PRODUK_EXPORT = <?= BRG_PRODUK_EXPORT ?>;
    var BRG_EXPEDISI = <?= BRG_EXPEDISI ?>;
    var BRG_BARANGRONGSOKAN = <?= BRG_BARANGRONGSOKAN ?>;
    //id tipe material
    var MAT_RAW_MATERIAL = <?= MAT_RAW_MATERIAL ?>;
    var MAT_PACKAGING = <?= MAT_PACKAGING ?>;
    var MAT_GENERAL = <?= MAT_GENERAL ?>;
    var MAT_SPAREPART = <?= MAT_SPAREPART ?>;
    var MAT_SPAREPART_BEKAS = <?= MAT_SPAREPART_BEKAS ?>;
    //max jumlah item SJ
    var MAX_ITEM_SJ_PRODUK_LOKAL = <?= MAX_ITEM_SJ_PRODUK_LOKAL ?>;
    var MAX_ITEM_SJ_PRODUK_EXPORT = <?= MAX_ITEM_SJ_PRODUK_EXPORT ?>;
    var MAX_ITEM_SJ_EXPEDISI = <?= MAX_ITEM_SJ_EXPEDISI ?>;
    var MAX_ITEM_SJ_BARANGRONGSOKAN = <?= MAX_ITEM_SJ_BARANGRONGSOKAN ?>;
    //status
    var OPEN = <?= OPEN ?>;
    var CLOSE = <?= CLOSE ?>;
    //info PO
    var MAX_ITEM_PO = <?= MAX_ITEM_PO ?>;
    //status PR
    var PR_OPEN = <?= PR_OPEN ?>;
    var PR_PROCESS = <?= PR_PROCESS ?>;
    var PR_REJECTED = <?= PR_REJECTED ?>;
    var PR_CLOSED = <?= PR_CLOSED ?>;
    //status PO
    var P0_OPEN = <?= P0_OPEN ?>;
    var P0_RELEASED = <?= P0_RELEASED ?>;
    var PO_PARTIALLY_RECEIVED = <?= PO_PARTIALLY_RECEIVED ?>;
    var PO_RECEIVED = <?= PO_RECEIVED ?>;
    var PO_INVOICED = <?= PO_INVOICED ?>;
    var PO_CANCELED = <?= PO_CANCELED ?>;
    var PO_CLOSED = <?= PO_CLOSED ?>;
    //status SJ supplier
    var SJ_SUPPLIER_OPEN = <?= SJ_SUPPLIER_OPEN ?>;
    var SJ_SUPPLIER_INVOICED = <?= SJ_SUPPLIER_INVOICED ?>;
    var SJ_SUPPLIER_FPT = <?= SJ_SUPPLIER_FPT ?>;
    var SJ_SUPPLIER_PAID = <?= SJ_SUPPLIER_PAID ?>;
    //TIPE INVOICE
    var TIPE_INV_DEFAULT = <?= TIPE_INV_DEFAULT ?>;
    var TIPE_INV_DOWNPAYMENT = <?= TIPE_INV_DOWNPAYMENT ?>;
    var TIPE_INV_TANPA_SJ = <?= TIPE_INV_TANPA_SJ ?>;
    //status FPT
    var FPT_OPEN = <?= FPT_OPEN ?>;
    var FPT_PAID = <?= FPT_PAID ?>;
    //status Return Pembelian
    var RETURN_PEMBELIAN_OPEN = <?= RETURN_PEMBELIAN_OPEN ?>;
    var RETURN_PEMBELIAN_DELIVERED = <?= RETURN_PEMBELIAN_DELIVERED ?>;
    var RETURN_PEMBELIAN_RECEIVED = <?= RETURN_PEMBELIAN_RECEIVED ?>;
    var RETURN_PEMBELIAN_NOTA_RETURN = <?= RETURN_PEMBELIAN_NOTA_RETURN ?>;
    //status Nota Return
    var NOTA_RETURN_OPEN = <?= NOTA_RETURN_OPEN ?>;
    var NOTA_RETURN_USED = <?= NOTA_RETURN_USED ?>;
    var NOTA_RETURN_CANCELED = <?= NOTA_RETURN_CANCELED ?>;
    //status Nota Return
    var BATCH_OPEN = <?= BATCH_OPEN ?>;
    var BATCH_PREPARED = <?= BATCH_PREPARED ?>;
    var BATCH_MIXED = <?= BATCH_MIXED ?>;
    var BATCH_FINISHED = <?= BATCH_FINISHED ?>;
    //prepare dan return bahan baku
    var SUPPIN = <?= SUPPIN ?>;
    var SUPPOUT = <?= SUPPOUT ?>;
    //User Role
    var USER_ADMIN = <?= USER_ADMIN ?>;
    var USER_GPJ = <?= USER_GPJ ?>;
    var USER_PPIC = <?= USER_PPIC ?>;
    var USER_GA = <?= USER_GA ?>;
    var USER_PURCHASING = <?= USER_PURCHASING ?>;
    var USER_PEMBELIAN = <?= USER_PEMBELIAN ?>;
    var USER_RND = <?= USER_RND ?>;
    var USER_PRODUKSI = <?= USER_PRODUKSI ?>;
    var USER_QA = <?= USER_QA ?>;
    var USER_UMUM = <?= USER_UMUM ?>;
    var USER_QC = <?= USER_QC ?>;
    var USER_EKSTERNAL = <?= USER_EKSTERNAL ?>;
    //info user logged in
    var UID = '<?= User()->getId() ?>';
    var UROLE = <?= U::get_user_role() ?>;
    //storage location
    var storloc = <?= json_encode($GLOBALS['storLoc'], JSON_FORCE_OBJECT) ?>;
    var storloc_arraystore = [];
    for (x in storloc)
        storloc_arraystore.push(new Array(parseInt(x), storloc[x].name, storloc[x].havestock));
    var storloc_nostock_arraystore = [];
    for (x in storloc)
        if (!storloc[x].havestock) storloc_nostock_arraystore.push(new Array(parseInt(x), storloc[x].name,
            storloc[x].havestock));
</script>
<?php echo $content; ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl . "/js/main.js?v=" .
            md5_file(dirname(Yii::app()->getBasePath()) . "/js/main.js"); ?>">
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/pdfobject.min.js"></script>
</body>
</html>
