<?php
return array(
    'basePath' => dirname(__file__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'CV. JOGJA FOAMINDO',
    'theme' => 'extjs',
    'preload' => array('log'),
    'sourceLanguage' => 'xx',
    'language' => 'en',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'application.vendors.*'
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'admin',
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array('ext.giix-core',
            ),
        ),
    ),
    'components' => array(
        'CGridViewPlus' => array(
            'class' => 'components.CGridViewPlus',
        ),
        'user' => array(
            'loginUrl' => array('site/login'),
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
//                '/' => 'site/index',
//                '<action:(contact|login|logout)>/*' => 'site/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
//        'excel' => array(
//            'class' => 'application.extensions.PHPExcel',
//        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=pabrik_new;port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'pbu_',
            'username' => 'rCpp9uvJpztw',
            'password' => 'feZHrlnzcx+l7E0M',
            'charset' => 'utf8',
        ),
        'session' => array(
            'sessionName' => 'pabrik',
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable' => false,
            'connectionID' => 'db',
            'sessionTableName' => 'pbu_session',
            'useTransparentSessionID' => isset($_POST['PHPSESSID']) ? true : false,
            'autoStart' => 'false',
            'cookieMode' => 'only',
            'timeout' => 108000
        ),
        'errorHandler' => array(
            'errorAction' => '',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                    'filter' => 'CLogFilter',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 10
                ),
            ),
        ),
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendors.mpdf.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                        '_MPDF_TTFONTDATAPATH' => Yii::getPathOfAlias('application.runtime')
                    ),
                    'class' => 'mpdf',
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendors.html2pdf.*',
                    'classFile' => 'html2pdf.class.php',
                )
            ),
        ),
    ),
    'params' => array(
        'adminEmail' => 'webmaster@example.com',
        'system_title' => 'CV. JOGJA FOAMINDO',
        'system_subtitle' => 'Jl. Parangtritis Km.4 Sewon, Yogyakarta',
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
        'corp' => array(
            'nama' => 'CV. JOGJA FOAMINDO',
            'address' => array(
                'Jl. Parangtritis Km.4 Sewon,',
                'Yogyakarta'
            ),
            'faktur_footer' => array(
                'Barang yang sudah dibeli tidak dapat ditukar atau dikembalikan',
                'Pembayaran selain tunai dianggap lunas pada saat dana cair.'
            ),
        )
    ),
//    'behaviors' => array(
//        'onBeginRequest' => array('class' => 'application.components.RequireLogin'
//        )
//    ),
);
