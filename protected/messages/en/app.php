<?php
return array(
    "pass.success" => "Password successfully changed.",
    "pass.fail" => "Changing password failed.",
    "pass.wrong.old" => "Invalid old password.",
    "save.success" => "Data successfully saved.",
    "save.fail" => "Saving Data failed.",
    "save.model.success" => "{model} successfully saved.",
    "save.model.fail" => "Saving {model} failed.",
    "save.success.id" => "Successfully saved with id {id}",
    "saldo.item.fail" => "{item} balance is not enough. On Hand {h} Required {r}",
    "coa.fail.use.gl" => "COA {coa} already used in GL transaction",
    "coa.fail.use.model" => "COA {coa} already used in other {model} account",
    "bank.fail.cash" => "Bank Cash can't found",
);