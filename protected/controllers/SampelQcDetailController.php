<?php
class SampelQcDetailController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('sampel_id = :sampel_id');
        $criteria->params = array(':sampel_id' => $_POST['sampel_id']);
        $model = SampelQcDetail::model()->findAll($criteria);
        $total = SampelQcDetail::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}