<?php
class SparepartMasukDetailController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('sparepart_masuk_id = :sparepart_masuk_id');
        $criteria->params = array(':sparepart_masuk_id' => $_POST['sparepart_masuk_id']);
        $model = SparepartMasukDetil::model()->findAll($criteria);
        $total = SparepartMasukDetil::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}