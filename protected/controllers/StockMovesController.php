<?php
class StockMovesController extends GxController
{
    public function actionCreate()
    {
        $model = new StockMoves;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StockMoves'][$k] = $v;
            }
            $model->attributes = $_POST['StockMoves'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->stock_moves_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'StockMoves');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StockMoves'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['StockMoves'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->stock_moves_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->stock_moves_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'StockMoves')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = StockMoves::model()->findAll($criteria);
        $total = StockMoves::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionQtyStock()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['material_id'];
            $loc_code = null;
            if (isset($_POST['loc_code'])) {
                $loc_code = $_POST['loc_code'];
            }
            $material = Material::model()->findByPk($id);
            if ($material == null) {
                $status = false;
                $msg = 'Material tidak ditemukan';
            } else {
                $status = true;
                $msg = $material->get_saldo($loc_code);
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionCreateKelolaStok()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $msg = 'Pengelolaan persediaan berhasil disimpan.';
            $trans_date = $_POST['trans_date'];
            $ket = $_POST['note'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $trans_no = $this->generate_uuid();
//                $trans_no++;
//                $ref = new Reference();
//                $docref = $ref->get_next_reference(KELOLASTOK);
                $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => KELOLASTOK));
                $docref = $systype->next_reference;
                if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $docref, $result) == 1) {
                    list($all, $prefix, $year, $number) = $result;
                    $tgl = strtotime($_POST['trans_date']);
                    $year_new = date("dmy", $tgl);
                    if ($number == '9999999999') {
                        $nextval = '0000000001';
                    } else {
                        $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                        $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                        $val = intval($number + 1);
                        $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
                    }
                    $systype->next_reference = "$prefix/$year_new/$nextval";
                    $docref = $systype->next_reference;
                }
                if (!$systype->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                        CHtml::errorSummary($systype));
                }
//                $total_minus = 0;
//                $barang_id_plus = '';
//                $qty_plus = '';
//                foreach ($detils as $detil) {
//                    if ($detil['qty'] < 0) {
//                        /** @var Barang $barang */
//                        $barang = Material::model()->findByPk($detil['barang_id']);
//                        if ($barang != null) {
//                            $total_item = $barang->price_buy * abs($detil['qty']);
//                            $total_minus += $total_item;
//                        }
//                    } else {
//                        $barang_id_plus = $detil['barang_id'];
//                        $qty_plus = $detil['qty'];
//                    }
//                }
//                if($barang_id_plus == ''){
//                    throw new Exception('Pengelolaan persediaan harus ada barang qty plus.');
//                }
//                $harga_pcs = round($total_minus / $qty_plus,2);
//                Cogs::count_biaya_beli($tgl, $trans_no, $barang_id_plus, $qty_plus, $harga_pcs);
                foreach ($detils as $detil) {
                    U::add_stock_moves_material(KELOLASTOK,
                        $trans_no, $trans_date, $detil['material_id'], $detil['qty'], null, null, null,
                        $docref, $detil['storage_location'], null);
                }
                U::add_comments(KELOLASTOK, $trans_no, $trans_date, $ket);
//                $ref->save(KELOLASTOK, $trans_no, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionkelolaStok()
    {
//        $kelolaStok = U::get_kelola_stok($_POST['tgl']);
//        $total = count($kelolaStok);
//        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($kelolaStok) . '}';
//        Yii::app()->end($jsonresult);
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("trans_date = :trans_date");
        $param[':trans_date'] = $_POST['tgl'];
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = KelolaStok::model()->findAll($criteria);
        $total = KelolaStok::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionkelolaStokDetil()
    {
        $trans_no = $_POST['trans_no'];
        $criteria = new CDbCriteria();
        $param = array();
//        $criteria->params = $param;
        $criteria->addCondition("type_no = :type_no");
        $param[':type_no'] = KELOLASTOK;
        $criteria->addCondition("trans_no = :trans_no");
        $param[':trans_no'] = $trans_no;
        $criteria->params = $param;
        $model = StockMovesMaterial::model()->findAll($criteria);
        $total = StockMovesMaterial::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}