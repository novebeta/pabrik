<?php
class MaterialController extends GxController
{
    public function actionCreate()
    {
        $model = new Material;
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Material'][$k] = $v;
            }
            $model->attributes = $_POST['Material'];
            $msg = "";
            if ($model->save()) {
                $status = true;
                $msg = "Data Material berhasil di simpan dengan id " . $model->material_id;
            } else {
                $msg = "Data Material gagal disimpan. " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Material');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Material'][$k] = $v;
            }
            $model->attributes = $_POST['Material'];
            $msg = "";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->material_id;
            } else {
                $msg = "Data gagal disimpan " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->material_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = array();
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
            $criteria->addCondition($_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'");
        }
        if (isset($_POST['tipe_material_id']) && $_POST['tipe_material_id'] != '' && $_POST['tipe_material_id'] != 0) {
            $criteria->addCondition("tipe_material_id = :tipe_material_id");
            $criteria->params[':tipe_material_id'] = $_POST['tipe_material_id'];
        }
        if (isset($_POST['perbaikan_mesin']) && $_POST['perbaikan_mesin'] == '1') {
            $criteria->addCondition("tipe_material_id IN (" . MAT_SPAREPART . "," . MAT_SPAREPART_BEKAS . ")");
        }
        if (!isset($_POST['tipe_material_id']) || $_POST['tipe_material_id'] == '' || $_POST['tipe_material_id'] == 0) {
            $user = Users::model()->findByPk(user()->getId());
            switch ($user->security_roles_id) {
                case USER_PPIC:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_PPIC]);
                    break;
                case USER_GA:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_GA]);
                    break;
            }
        }
        if (isset($_POST['po_id']) && $_POST['po_id'] != '') {
            $data = Yii::app()->db->createCommand('select material_id  FROM pbu_purchase_order_details where po_id="' . $_POST['po_id'] . '"')->queryAll();
            foreach ($data as $value) {
                $in[] = $value['material_id'];
            }
            $criteria->addInCondition("material_id", $in);
        }
        if (isset($_POST['bom_id']) && $_POST['bom_id'] != '') {
            $data = Yii::app()->db->createCommand('select material_id  FROM pbu_bom_detail where bom_id="' . $_POST['bom_id'] . '"')->queryAll();
            foreach ($data as $value) {
                $in[] = $value['material_id'];
            }
            $criteria->addInCondition("material_id", $in);
        }
        if (isset($_POST['terima_material_id']) && $_POST['terima_material_id'] != '') {
            $data = Yii::app()->db->createCommand('select material_id  FROM pbu_terima_material_details where terima_material_id="' . $_POST['terima_material_id'] . '"')->queryAll();
            foreach ($data as $value) {
                $in[] = $value['material_id'];
            }
            $criteria->addInCondition("material_id", $in);
        }
        if (isset($_POST['materialforbom']) && $_POST['materialforbom'] == 1) {
            $criteria->addInCondition('tipe_material_id', array(MAT_RAW_MATERIAL, MAT_PACKAGING));
        }
        if (isset($_POST['urole'])) {
            switch ($_POST['urole']) {
                case USER_PPIC:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_PPIC]);
                    break;
                case USER_GA:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_GA]);
                    break;
            }
        }
        $model = Material::model()->findAll($criteria);
        $total = Material::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Material')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionExpiredDate()
    {
        //yang pakai fungsi ini : Transfer Material, Inventory Card (Material)
        $material_id = $_POST['material_id'];
        $noZeroStock = isset($_POST['noZeroStock']) ? 1 : 0;
        $trans_no = isset($_POST['trans_no']) ? $_POST['trans_no'] : NULL;
        $storloc = isset($_POST['storloc']) ? $GLOBALS['storLoc'][get_number($_POST['storloc'])]['name'] : NULL;
        //$transOut = isset($_POST['trans_out']) ? $_POST['trans_out'] : NULL;
        //$storloc = isset($_POST['urole_storloc'])?$GLOBALS['storLoc'][get_number($_POST['urole_storloc'])]['name']:$GLOBALS['storLoc'][U::get_user_role()]['name'];
        //$storloc_havestock = $GLOBALS['storLoc'][get_number($_POST['storloc'])]['havestock'];
        if ($trans_no !== NULL) {
            //edit Transfer Material khusus auto accept
            $arr = Material::get_availableStock_before($material_id, $trans_no, $noZeroStock, $storloc);
        } else {
            //ketika create/edit Transfer Material / lihat kartu stock
            $arr = Material::get_availableStock($material_id, $noZeroStock, $storloc);
        }
        foreach ($arr as $key => $row) {
            $row['qty'] = UnitOfMeasure::get_qtySatuan($row['qty'], $row['sat']);
            $arr[$key] = $row;
        }
        $this->renderJsonArr($arr);
    }
    public function actionExpiredDateReturnBahanBaku()
    {
        $material_id = isset($_POST['material_id']) ? $_POST['material_id'] : NULL;
        $batch_id = isset($_POST['batch_id']) ? $_POST['batch_id'] : NULL;
        $noZeroStock = isset($_POST['noZeroStock']) ? $_POST['noZeroStock'] : 0;
        $trans_no = isset($_POST['trans_no']) ? $_POST['trans_no'] : NULL;
//        if($material_id !== NULL && $trans_no !== NULL){
//            $arr = Material::get_availableStock_ReturnBahanBaku_before($material_id, $batch_id, $trans_no, $noZeroStock);
//        }else{
        $arr = Material::get_availableStock_ReturnBahanBaku($material_id, $batch_id, $noZeroStock);
//        }
        foreach ($arr as $key => $row) {
            $row['qty'] = UnitOfMeasure::get_qtySatuan($row['qty'], $row['sat']);
            $arr[$key] = $row;
        }
        $this->renderJsonArr($arr);
    }
}