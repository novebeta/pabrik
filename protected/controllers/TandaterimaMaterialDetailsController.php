<?php
class TandaterimaMaterialDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('tandaterima_id = :tandaterima_id');
        $criteria->params = array(':tandaterima_id' => $_POST['tandaterima_id']);
        $model = TandaterimaMaterialDetails::model()->findAll($criteria);
        $total = TandaterimaMaterialDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}