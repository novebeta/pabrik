<?php
class GiroController extends GxController
{
    public function actionCreate()
    {
        $model = new Giro;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Giro'][$k] = $v;
            }
            $model->attributes = $_POST['Giro'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->giro_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Giro');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Giro'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Giro'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->giro_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->giro_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Giro')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionCair()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Giro berhasil dicairkan.';
            $status = false;
            $id = $_POST['id'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var Giro $g */
                $g = $this->loadModel($id, 'Giro');
                $g->tgl_cair = $_POST['tgl_cair'];
                $g->bank_id = $_POST['bank_id'];
                if (isset($_POST['notep_'])) {
                    $g->notep_ = $_POST['notep_'];
                }
                if (!$g->save()) {
                    throw new Exception(CHtml::errorSummary($g));
                }
                $gl = new GL();
                # Bank - Piutang Giro
                $gl->add_bank(GIRO_CAIR, $g->giro_id, $g->tgl_cair, $g->bank_id, $g->no_giro,
                    "Pencairan Giro " . $g->no_giro, '', $g->saldo, 0);
                $gl->add_gl(GIRO_CAIR, $g->giro_id, $g->tgl_cair, $g->no_giro, COA_PIUTANG_GIRO,
                    "Pencairan Giro " . $g->no_giro, "Pencairan Giro " . $g->no_giro, -$g->saldo, 0);
                $gl->validate();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionTolak($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Giro berhasil ditolak.';
            $status = true;
            try {
                /** @var Giro $g */
                $g = $this->loadModel($id, 'Giro');
                $g->tolak = 1;
                $g->used = 1;
                if (isset($_POST['notep_'])) {
                    $g->notep_ = $_POST['notep_'];
                }
                if (!$g->save()) {
                    throw new Exception(CHtml::errorSummary($g));
                }
                /** @var CustomerPayment[] $custPay */
                $custPay = CustomerPayment::model()->findAllByAttributes([
                    'no_bg_cek' => $g->giro_id,
                    'void' => 0
                ]);
                foreach ($custPay as $c) {
                    $c->void__();
                }
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $params = [];
//        if (isset($_POST['used'])) {
//            $criteria->addCondition('used = :used');
//            $params[':used'] = 0;
//        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
//        $criteria->params = $params;
        $model = Giro::model()->findAll($criteria);
        $total = Giro::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionSisa()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $params = [];
//        if (isset($_POST['used'])) {
//            $criteria->addCondition('used = :used');
//            $params[':used'] = 0;
//        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
//        $criteria->params = $params;
        $model = GiroSisa::model()->findAll($criteria);
        $total = GiroSisa::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}