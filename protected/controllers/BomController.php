<?php
class BomController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $bom_id = $_POST['id'];
            $detail = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Bom() : $this->loadModel($bom_id, "Bom");
                //$doc_ref = $model->doc_ref;
                if ($is_new) {
                    //$doc_ref = Reference::createDocumentReference_TandaTerima($_POST['tgl']);
                } else {
                    BomDetail::model()->deleteAll("bom_id = :bom_id", array(':bom_id' => $model->bom_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Bom'][$k] = $v;
                }
                //$_POST['Bom']['doc_ref'] = $doc_ref;
                $_POST['Bom']['enable'] = isset($_POST['enable']) ? 1 : 0;
                $model->attributes = $_POST['Bom'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Bill Of Material')) . CHtml::errorSummary($model));
                }
                foreach ($detail as $detil) {
                    $item_details = new BomDetail();
                    $_POST['BomDetail']['material_id'] = $detil['material_id'];
                    $_POST['BomDetail']['qty'] = get_number($detil['qty']);
                    $_POST['BomDetail']['sat'] = get_number($detil['sat']);
                    $_POST['BomDetail']['loc_code'] = get_number($detil['loc_code']);
                    $_POST['BomDetail']['bom_id'] = $model->bom_id;
                    $item_details->attributes = $_POST['BomDetail'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Bom Detail')) . CHtml::errorSummary($item_details));
                    }
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                //'id' => $doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Bom')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionBomProduk()
    {
        $enable = isset($_POST['enable']) ? get_number(isset($_POST['enable'])) : NULL;
        $this->renderJsonArr(Bom::getBomProduk($enable));
    }
    public function actionIndex()
    {
        $cmd = Yii::app()->db->createCommand();
        $ttl = Yii::app()->db->createCommand();
        $params = array();
        $where = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $cmd->limit = $limit;
            $cmd->offset = $start;
        }
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "" && isset ($_POST['mode']) && $_POST['mode'] == 'grid') {
            $where[] = $_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'";
        }
        if (isset($_POST['grup_id']) && $_POST['grup_id'] != '' && $_POST['grup_id'] != '0') {
            $where[] = "grup_id = :grup_id";
            $params[':grup_id'] = $_POST['grup_id'];
        }
        $cmd->where = implode(" AND ", $where);
        $cmd->select = '*';
        $cmd->from = '{{bom}} t';
        $ttl->where = implode(" AND ", $where);
        $ttl->select = 'count(*)';
        $ttl->from = '{{bom}} t';
        if (isset ($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->select .= ', b.kode_material, b.nama_material';
            $cmd->join = 'LEFT JOIN {{material}} b ON t.material_id = b.material_id';
            $ttl->join = 'LEFT JOIN {{material}} b ON t.material_id = b.material_id';
        }
        $cmd->order = 't.no_bom ASC';
        $model = $cmd->queryAll(true, $params);
        $total = $ttl->queryScalar($params);
        $this->renderJsonArrWithTotal($model, $total);
    }
}