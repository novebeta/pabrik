<?php
class SalesmanController extends GxController
{
    public function actionCreate()
    {
        $model = new Salesman;
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['Salesman'][$k] = $v;
            }
            $model->attributes = $_POST['Salesman'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->salesman_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Salesman');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                $_POST['Salesman'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Salesman'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->salesman_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->salesman_id));
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['salesman_code'])) {
            $criteria->addCondition("salesman_code like :salesman_code");
            $param[':salesman_code'] = "%" . $_POST['salesman_code'] . "%";
        }
        if (isset($_POST['salesman_name'])) {
            $criteria->addCondition("salesman_name like :salesman_name");
            $param[':salesman_name'] = "%" . $_POST['salesman_name'] . "%";
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition("status = :status");
            $param[':status'] = $_POST['status'];
        }
        $criteria->params = $param;
        if (isset($_POST['mode']) && $_POST['mode'] == 'active') {
            $criteria->addColumnCondition(array('status' => 1));
        } elseif ((isset($_POST['mode']) && $_POST['mode'] == 'grid')
            ||
            (isset($_POST['limit']) || isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        } elseif (isset($_POST['bagian'])) {
            $bagian_arr = explode(',', $_POST['bagian']);
            $criteria->addInCondition("bagian", $bagian_arr, 'OR');
//            $param[':bagian'] = $bagian_arr;
        }
        $model = Salesman::model()->findAll($criteria);
        $total = Salesman::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}