<?php
class CreditNoteController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $tb_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new CreditNote : $this->loadModel($tb_id, "CreditNote");
                $docref = $model->doc_ref;
                if ($is_new) {
                    $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => CNOTE));
                    $docref = $systype->next_reference;
                    if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $docref, $result) == 1) {
                        list($all, $prefix, $year, $number) = $result;
                        $tgl = strtotime($_POST['tgl']);
                        $year_new = date("dmy", $tgl);
                        if ($number == '9999999999') {
                            $nextval = '0000000001';
                        } else {
                            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                            $val = intval($number + 1);
                            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
                        }
                        $systype->next_reference = "$prefix/$year_new/$nextval";
                        $docref = $systype->next_reference;
                    }
                    if (!$systype->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                            CHtml::errorSummary($systype));
                    }
                } else {
                    CreditNoteDetails::model()->deleteAll("credit_note_id = :credit_note_id",
                        array(':credit_note_id' => $tb_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['CreditNote'][$k] = $v;
                }
                $_POST['CreditNote']['doc_ref'] = $docref;
                $model->attributes = $_POST['CreditNote'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) .
                        CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $item_details = new CreditNoteDetails;
                    $_POST['CreditNoteDetails']['barang_id'] = $detil['barang_id'];
//                    $_POST['CreditNoteDetails']['batch'] = $detil['batch'];
//                    $_POST['CreditNoteDetails']['tgl_exp'] = $detil['tgl_exp'];
                    $_POST['CreditNoteDetails']['qty'] = get_number($detil['qty']);
                    $_POST['CreditNoteDetails']['price'] = get_number($detil['price']);
                    $_POST['CreditNoteDetails']['total'] = get_number($detil['total']);
                    $_POST['CreditNoteDetails']['credit_note_id'] = $model->credit_note_id;
                    $item_details->attributes = $_POST['CreditNoteDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'CreditNote');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['CreditNote'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['CreditNote'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->credit_note_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->credit_note_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'CreditNote')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('tgl = :tgl');
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = CreditNote::model()->findAll($criteria);
        $total = CreditNote::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}