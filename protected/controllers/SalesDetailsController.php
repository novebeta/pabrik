<?php
class SalesDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new DetilPenjualan;
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['DetilPenjualan'][$k] = $v;
            }
            $model->attributes = $_POST['DetilPenjualan'];
            $msg = "Data gagal disimpan";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->detil_penjualan;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'DetilPenjualan');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['DetilPenjualan'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['DetilPenjualan'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->detil_penjualan;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->detil_penjualan));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'DetilPenjualan')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app',
                    'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("sales_id = :sales_id");
        $criteria->params = array(':sales_id' => $_POST['sales_id']);
        $model = SalesDetails::model()->findAll($criteria);
        $total = SalesDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}