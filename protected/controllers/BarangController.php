<?php
class BarangController extends GxController
{
    public function actionCreate()
    {
        $model = new Barang; //buat data barang baru
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Barang'][$k] = $v;
            }
            //menentukan kode UUID untuk field barang_id
            $query = Barang::model()->dbConnection->createCommand("SELECT UUID();");
            $uuid = $query->queryScalar();
            $model->barang_id = $uuid; //set nilai barang_id dengan hasil UUID
            $model->attributes = $_POST['Barang']; //set nilai field lainnya sesuai dengan userinput
            $msg = "";
            if ($model->save()) { //menyimpan data baru
                $status = true;
                $msg = "Data Barang berhasil di simpan dengan id " . $model->barang_id;
            } else {
                $msg = "Data Barang gagal disimpan. " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Barang'); //mengambil data berdasarkan barang_id yang diberikan
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Barang'][$k] = $v;
            }
            $model->attributes = $_POST['Barang']; //Ubah nilai field sesuai dengan userinput
            $msg = "";
            if ($model->save()) { //menyimpan perubahan data
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->barang_id;
            } else {
                $msg = "Data gagal disimpan " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->barang_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
            $criteria->addCondition($_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'");
        }
        if (isset($_POST['grup_id']) && $_POST['grup_id'] != '') {
            $criteria->addCondition("grup_id = :grup_id");
            $criteria->params[':grup_id'] = $_POST['grup_id'];
        }
        if (isset($_POST['tipe_barang_id']) && $_POST['tipe_barang_id'] != '' && $_POST['tipe_barang_id'] != 0) {
            $criteria->addCondition("tipe_barang_id = :tipe_barang_id");
            $criteria->params[':tipe_barang_id'] = $_POST['tipe_barang_id'];
        }
        $model = Barang::model()->findAll($criteria);
        $total = Barang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionWithBatch()
    {
        $grup_id = isset($_POST['grup_id']) ? $_POST['grup_id'] : null;
        $arr = Barang::get_barang($grup_id);
        $this->renderJsonArr($arr);
    }
    public function actionBatch()
    {
        $barang_id = isset($_POST['barang_id']) ? $_POST['barang_id'] : null;
        $storloc = isset($_POST['urole_storloc']) ? $GLOBALS['storLoc'][get_number($_POST['urole_storloc'])]['name'] : $GLOBALS['storLoc'][U::get_user_role()]['name'];
        $arr = Barang::get_batch($barang_id, $storloc);
        $this->renderJsonArr($arr);
    }
    public function actionBatchTransfer()
    {
        $barang_id = isset($_POST['barang_id']) ? $_POST['barang_id'] : null;
        $trans_no = isset($_POST['trans_no']) ? $_POST['trans_no'] : NULL;
        $storloc = isset($_POST['urole_storloc']) ? $GLOBALS['storLoc'][get_number($_POST['urole_storloc'])]['name'] : $GLOBALS['storLoc'][U::get_user_role()]['name'];
        if ($trans_no !== NULL) {
            $arr = Barang::get_availabel_barang_before($barang_id, $trans_no, $storloc);
        } else {
            $arr = Barang::get_availabel_barang($barang_id, $storloc);
        }
        $this->renderJsonArr($arr);
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Barang')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionSales()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        $criteria->addCondition('');
        $model = BarangSales::model()->findAll($criteria);
        $total = BarangSales::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}