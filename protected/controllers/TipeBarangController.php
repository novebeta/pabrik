<?php
class TipeBarangController extends GxController
{
    public function actionIndex()
    {
        $model = TipeBarang::model()->findAll();
        $total = TipeBarang::model()->count();
        $this->renderJson($model, $total);
    }
}