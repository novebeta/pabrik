<?php
class KasDetailController extends GxController
{
    public function actionCreate()
    {
        $model = new KasDetail;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['KasDetail'][$k] = $v;
            }
            $model->attributes = $_POST['KasDetail'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_detail_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'KasDetail');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['KasDetail'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['KasDetail'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_detail_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kas_detail_id));
            }
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->select = "kas_detail_id,item_name,IF(total>=0,total,-total) total,kas_id,account_code";
        $criteria->addCondition('kas_id = :kas_id');
        $criteria->params = array(":kas_id" => $_POST['kas_id']);
        $model = KasDetail::model()->findAll($criteria);
        $total = KasDetail::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}