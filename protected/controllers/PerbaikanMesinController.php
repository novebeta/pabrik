<?php
class PerbaikanMesinController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $id = $_POST['perbaikan_mesin_id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new PerbaikanMesin : $this->loadModel($id, "PerbaikanMesin");
                $doc_ref = $model->doc_ref;
                if ($is_new) {
                    $doc_ref = Reference::createDocumentReference_PerbaikanMesin();
                } else {
                    StockMovesMaterial::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                        array(':type_no' => SUPPOUT, ':trans_no' => $id));
                    PerbaikanMesinDetail::model()->deleteAll("perbaikan_mesin_id = :perbaikan_mesin_id",
                        array(':perbaikan_mesin_id' => $id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if ($k == 'mutu') $v = $v == 'Memuaskan' ? 1 : -1;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['PerbaikanMesin'][$k] = $v;
                }
                $_POST['PerbaikanMesin']['doc_ref'] = $doc_ref;
                if (isset($_POST['PerbaikanMesin']['mutu']) && $_POST['PerbaikanMesin']['mutu'] > 0 && isset($_POST['PerbaikanMesin']['tgl_serahterima']))
                    $_POST['PerbaikanMesin']['status'] = CLOSE; //status Finished/Closed
                $model->attributes = $_POST['PerbaikanMesin'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Perbaikan Mesin')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $item_details = new PerbaikanMesinDetail;
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['PerbaikanMesinDetail'][$k] = $v;
                    }
                    $_POST['PerbaikanMesinDetail']['perbaikan_mesin_id'] = $model->perbaikan_mesin_id;
                    $item_details->attributes = $_POST['PerbaikanMesinDetail'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    $user_role = U::get_user_role();
                    U::add_stock_moves_material(
                        SUPPOUT,
                        $model->perbaikan_mesin_id,
                        $item_details->tgl,
                        $item_details->material_id,
                        -$item_details->qty,
                        NULL,
                        NULL,
                        NULL,
                        $model->doc_ref,
                        $GLOBALS['storLoc'][$user_role]['name'],
                        $item_details->supplier_id
                    );
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $doc_ref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl_lapor'])) {
            $criteria->addCondition('tgl_lapor = :tgl_lapor');
            $criteria->params[':tgl_lapor'] = $_POST['tgl_lapor'];
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            $criteria->addCondition('status = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
        $criteria->order = "tgl_lapor DESC";
        $model = PerbaikanMesin::model()->findAll($criteria);
        $total = PerbaikanMesin::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}