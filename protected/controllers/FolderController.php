<?php
class FolderController extends GxController
{
    public function actionCreate()
    {
        $model = new Folder;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Folder'][$k] = $v;
            }
            $model->attributes = $_POST['Folder'];
            $model->createDocRef();
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->folder_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Folder');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Folder'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Folder'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->folder_id;
            } else {
                $msg .= " " . Chtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->folder_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Folder')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionBreakdown()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = [];
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->order = 'tgl ASC';
        $criteria->params = $param;
        $model = FolderBreakdown::model()->findAll($criteria);
        $total = FolderBreakdown::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionAssembly()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = [];
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->order = 'tgl ASC';
        $criteria->params = $param;
        $model = FolderAssembly::model()->findAll($criteria);
        $total = FolderAssembly::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}