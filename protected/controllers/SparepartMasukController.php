<?php
class SparepartMasukController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $id = $_POST['sparepart_masuk_id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new SparepartMasuk : $this->loadModel($id, "SparepartMasuk");
                $doc_ref = $model->doc_ref;
                if ($is_new) {
                    $doc_ref = Reference::createDocumentReference_TandaterimaSparepartBekas();
                } else {
                    StockMovesMaterial::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                        array(':type_no' => SUPPIN, ':trans_no' => $id));
                    SparepartMasukDetil::model()->deleteAll("sparepart_masuk_id = :sparepart_masuk_id",
                        array(':sparepart_masuk_id' => $id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['SparepartMasuk'][$k] = $v;
                }
                $_POST['SparepartMasuk']['doc_ref'] = $doc_ref;
                $model->attributes = $_POST['SparepartMasuk'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Tanda Terima Sparepart')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $item_details = new SparepartMasukDetil;
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['SparepartMasukDetil'][$k] = $v;
                    }
                    $_POST['SparepartMasukDetil']['sparepart_masuk_id'] = $model->sparepart_masuk_id;
                    $item_details->attributes = $_POST['SparepartMasukDetil'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    $user_role = U::get_user_role();
                    U::add_stock_moves_material(
                        SUPPIN,
                        $model->sparepart_masuk_id,
                        $model->tgl,
                        $item_details->material_id,
                        $item_details->qty,
                        NULL,
                        NULL,
                        NULL,
                        $model->doc_ref,
                        $GLOBALS['storLoc'][$user_role]['name'],
                        $item_details->supplier_id == '' ? NULL : $item_details->supplier_id
                    );
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $doc_ref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        $criteria->order = "tgl DESC";
        $model = SparepartMasuk::model()->findAll($criteria);
        $total = SparepartMasuk::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}