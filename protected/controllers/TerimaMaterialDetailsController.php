<?php
class TerimaMaterialDetailsController extends GxController
{
    public function actionIndex()
    {
//        $criteria = new CDbCriteria();
//        $criteria->params = array();
//
//        if (isset($_POST['terima_material_id'])) {
//            $criteria->addCondition('terima_material_id = :terima_material_id');
//            $criteria->params[':terima_material_id'] = $_POST['terima_material_id'];
//        }
//        if (isset($_POST['material_id'])) {
//            $criteria->addCondition('material_id = :material_id');
//            $criteria->params[':material_id'] = $_POST['material_id'];
//        }
//
//        $model = TerimaMaterialDetails::model()->findAll($criteria);
//        $total = TerimaMaterialDetails::model()->count($criteria);
//        $this->renderJson($model, $total);
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['po_id'])) {
            $criteria->addCondition('po_id = :po_id');
            $criteria->params = array(':po_id' => $_POST['po_id']);
            $model = RemainReceive::model()->findAll($criteria);
            $total = RemainReceive::model()->count($criteria);
            $this->renderJson($model, $total);
            Yii::app()->end();
        }
//        if (isset($_POST['terima_barang_id']) && isset($_POST['arus']) && $_POST['arus'] == 1) {
//            $criteria->addCondition('terima_barang_id = :terima_barang_id');
//            $criteria->params = array(':terima_barang_id' => $_POST['terima_barang_id']);
//            $model = SuppCnoteTerima::model()->findAll($criteria);
//            $total = SuppCnoteTerima::model()->count($criteria);
//            $this->renderJson($model, $total);
//            Yii::app()->end();
//        }
        if (isset($_POST['terima_barang_id'])) {
            $criteria->addCondition('terima_barang_id = :terima_barang_id');
            $param[':terima_barang_id'] = $_POST['terima_barang_id'];
        }
        $criteria->params = $param;
        $model = TerimaBarangDetails::model()->findAll($criteria);
        $total = TerimaBarangDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}