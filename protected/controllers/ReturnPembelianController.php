<?php
class ReturnPembelianController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $return_pembelian_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new ReturnPembelian() : $this->loadModel($return_pembelian_id, "ReturnPembelian");
                $docref = $model->doc_ref;
                if ($is_new) {
                    $docref = Reference::createDocumentReference_ReturnPembelian($_POST['tgl']);
                } else {
                    StockMovesMaterial::model()->deleteAll("trans_no = :trans_no",
                        array(':trans_no' => $return_pembelian_id));
                    ReturnPembelianDetails::model()->deleteAll("return_pembelian_id = :return_pembelian_id",
                        array(':return_pembelian_id' => $return_pembelian_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['ReturnPembelian'][$k] = $v;
                }
                $_POST['ReturnPembelian']['doc_ref'] = $docref;
                /** @var TerimaBarang $terima_barang */
                $terima_barang = TerimaBarang::model()->findByPk($_POST['ReturnPembelian']['terima_barang_id']);
                if ($terima_barang == null) {
                    throw new Exception("Surat jalan tidak ditemukan!");
                }
                $_POST['ReturnPembelian']['supplier_id'] = $terima_barang->po->supplier_id;
                $model->attributes = $_POST['ReturnPembelian'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Pembelian')) . CHtml::errorSummary($model));
                $model->nota_return_bruto = 0;
                $model->nota_return_dpp = 0;
                $model->nota_return_ppn = 0;
                $model->nota_return_total = 0;
                $model->nota_return_discount = 0;
                foreach ($detils as $detil) {
                    $item_details = new ReturnPembelianDetails();
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['ReturnPembelianDetails'][$k] = $v;
                    }
                    $_POST['ReturnPembelianDetails']['return_pembelian_id'] = $model->return_pembelian_id;
                    $_POST['ReturnPembelianDetails']['material_id'] = $detil['material_id'];
                    $_POST['ReturnPembelianDetails']['qty'] = get_number($detil['qty']);
//                    $_POST['ReturnPembelianDetails']['note_return'] = $detil['note_return'];
//                    $_POST['ReturnPembelianDetails']['sat'] = $detil['sat'];
                    $_POST['ReturnPembelianDetails']['price'] = get_number($detil['price']);
//                    $_POST['ReturnPembelianDetails']['tgl_expired'] = $detil['tgl_expired'];
                    $item_details->attributes = $_POST['ReturnPembelianDetails'];
                    $terimad = TerimaBarangDetails::model()->findAllByAttributes(array(
                        'terima_barang_id' => $model->terima_barang_id,
                        'barang_id' => $detil['material_id']
                    ));
                    if (count($terimad) > 1) {
                        throw new Exception('Detail ambigu.');
                    }
                    if (count($terimad) == 0) {
                        throw new Exception('Tidak ditemukan referensi terima barang details.');
                    }
                    $item_details->price = $terimad[0]->price;
                    $item_details->disc = $terimad[0]->disc;
                    $item_details->vat = $terimad[0]->vat;
                    $item_details->bruto = $item_details->qty * $item_details->price;
                    $item_details->disc_rp = $item_details->bruto * $item_details->disc * 0.01;
                    $item_details->vatrp = ($item_details->bruto - $item_details->disc_rp) * $item_details->vat * 0.01;
                    $item_details->total = $item_details->bruto - $item_details->disc_rp + $item_details->vatrp;
                    $model->nota_return_bruto += $item_details->bruto;
                    $model->nota_return_dpp += ($item_details->bruto - $item_details->disc_rp);
                    $model->nota_return_ppn += $item_details->vatrp;
                    $model->nota_return_discount += $item_details->disc_rp;
                    $model->nota_return_total += $item_details->total;
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Retur Pembelian detil')) . CHtml::errorSummary($item_details));
                    U::add_stock_moves(
                        RETURN_PEMBELIAN,
                        $model->return_pembelian_id,
                        $model->tgl,
                        $item_details->material_id,
                        -$item_details->qty,
//                            $item_details->tgl_expired,
//                            $item_details->no_lot,
//                            $item_details->no_qc,
                        $model->doc_ref,
                        $item_details->price,
                        $item_details->material->avg,
                        $model->loc_code,
                        $model->supplier_id
//                            $GLOBALS['storLoc'][U::get_user_role()]['name'],
//                            $model->supplier_id
                    );
//                    $total += ($item_details->qty * $item_details->price);
                }
//                $model->total = $total;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Pembelian')) . CHtml::errorSummary($model));
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
//                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionDeliver($id)
    {
        $msg = "Set status Kirim tidak berhasil";
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = $this->loadModel($id, "ReturnPembelian");
            $model->status = RETURN_PEMBELIAN_DELIVERED;
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Return Pembelian')) . CHtml::errorSummary($model));
            }
            $transaction->commit();
            $msg = "Set status Kirim berhasil.<br> No. Return : " . $model->doc_ref;
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionGetItems()
    {
        $arr = ReturnPembelian::getItems($_POST['terima_material_id']);
        $this->renderJsonArr($arr);
    }
    public function actionCreateNotaReturn()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $return_pembelian_id = $_POST['id'];
            //$detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var ReturnPembelian $model */
                $model = $this->loadModel($return_pembelian_id, "ReturnPembelian");
                $docref = $model->nota_return_doc_ref;
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['ReturnPembelian'][$k] = $v;
                }
                if ($is_new) {
                    $docref = Reference::createDocumentReference_NotaReturn($_POST['nota_return_tgl']);
                    $_POST['ReturnPembelian']['nota_return_status'] = NOTA_RETURN_OPEN;
                }
                $_POST['ReturnPembelian']['nota_return_doc_ref'] = $docref;
                $model->attributes = $_POST['ReturnPembelian'];
//                foreach ($model->returnPembelianDetails as $row) {
//
//                }
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Nota Return')) . CHtml::errorSummary($model));
                ReturnPembelian::setStatus(RETURN_PEMBELIAN_NOTA_RETURN, $return_pembelian_id);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionCancelNotaReturn($id)
    {
        $msg = "Proses pembatalan Nota Return tidak berhasil";
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = $this->loadModel($id, "ReturnPembelian");
            $model->nota_return_status = NOTA_RETURN_CANCELED;
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Nota Return')) . CHtml::errorSummary($model));
            }
            ReturnPembelian::setStatus(RETURN_PEMBELIAN_DELIVERED, $id);
            $transaction->commit();
            $msg = "Nota Return berhasil dibatalkan.<br> No. Nota Return : " . $model->nota_return_doc_ref;
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
//        if (isset($_POST['tgl'])) {
//            $criteria->addCondition('tgl = :tgl');
//            $criteria->params[':tgl'] = $_POST['tgl'];
//        }
//
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            $criteria->addCondition('status = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
//
//        if (isset($_POST['uid']) && $_POST['uid'] !== 'all') {
//            $criteria->addCondition('id_user = :uid');
//            $criteria->params[':uid'] = $_POST['uid'];
//        }
//        if (isset($_POST['urole']) && $_POST['urole'] !== 'all') {
//            $criteria->addCondition('urole = :urole');
//            $criteria->params[':urole'] = $_POST['urole'];
//        }
//
        if (isset($_POST['filter']) && $_POST['filter'] == 'nota_return') {
            $criteria->addCondition('nota_return_doc_ref IS NOT NULL');
        }
        if (isset($_POST['nota_return_tgl'])) {
            $criteria->addCondition('nota_return_tgl = :nota_return_tgl');
            $criteria->params[':nota_return_tgl'] = $_POST['nota_return_tgl'];
        }
        if (isset($_POST['nota_return_status']) && $_POST['nota_return_status'] !== 'all') {
            $criteria->addCondition('nota_return_status = :nota_return_status');
            $criteria->params[':nota_return_status'] = $_POST['nota_return_status'];
        }
        $criteria->order = "tgl DESC";
        $model = ReturnPembelian::model()->findAll($criteria);
        $total = ReturnPembelian::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}