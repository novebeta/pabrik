<?php
class BreakdownController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Breakdown;
//                $docref = Reference::createDocumentReference(BREAKDOWN, $_POST['tgl'], "dmy");
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Breakdown'][$k] = $v;
                }
                /** Material $mate */
                $mate = Material::model()->findByPk($_POST['Breakdown']['material_id']);
                $_POST['Breakdown']['price'] = $mate->avg;
                $_POST['Breakdown']['total'] =
                    round(floatval($_POST['Breakdown']['price']) * floatval($_POST['Breakdown']['qty']), 2);
                $_POST['Breakdown']['avg_price'] =
                    round(floatval($_POST['Breakdown']['total']) / $_POST['Breakdown']['total_qty'], 2);
                $model->attributes = $_POST['Breakdown'];
                $model->createDocRef();
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Breakdown')) .
                        CHtml::errorSummary($model));
                $gl = new GL();
                foreach ($detils as $detil) {
                    $breakdownDetails = new BreakdownDetails;
                    $_POST['BreakdownDetails']['price_std'] = $model->avg_price;
                    $_POST['BreakdownDetails']['qty'] = get_number($detil['qty']);
                    $_POST['BreakdownDetails']['material_id'] = $detil['material_id'];
                    $_POST['BreakdownDetails']['loc_code'] = $detil['loc_code'];
                    $_POST['BreakdownDetails']['breakdown_id'] = $model->breakdown_id;
                    $breakdownDetails->attributes = $_POST['BreakdownDetails'];
                    if (!$breakdownDetails->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detail Cash')) .
                            CHtml::errorSummary($breakdownDetails));
                    $mateD = Material::model()->findByPk($breakdownDetails->material_id);
                    $mateD->count_price_std($breakdownDetails->qty, $model->avg_price, $model->tgl,
                        BREAKDOWN, $model->breakdown_id);
                    U::add_stock_moves_material(
                        BREAKDOWN,
                        $model->breakdown_id,
                        $model->tgl,
                        $mateD->material_id,
                        $breakdownDetails->qty,
                        null, null, null,
                        $model->doc_ref, //$model->doc_ref,
                        $breakdownDetails->loc_code
                    );
                    //GL Persediaan WIP
                    //          Persediaan bahan baku
                    $nominal = round($breakdownDetails->price_std / $breakdownDetails->qty, 2);
                    $gl->add_gl(BREAKDOWN, $model->breakdown_id, $model->tgl, $model->doc_ref,
                        $breakdownDetails->material->tipeMaterial->coa_persediaan,
                        "Breakdown $model->doc_ref", "Breakdown $model->doc_ref", $nominal, 0);
                    $gl->add_gl(BREAKDOWN, $model->breakdown_id, $model->tgl, $model->doc_ref,
                        $model->material->tipeMaterial->coa_persediaan,
                        "Breakdown $model->doc_ref", "Breakdown $model->doc_ref", -$nominal, 0);
                }
                U::add_stock_moves_material(
                    BREAKDOWN,
                    $model->breakdown_id,
                    $model->tgl,
                    $model->material_id,
                    -$model->qty,
                    null, null, null,
                    $model->doc_ref, //$model->doc_ref,
                    $model->loc_code
                );
                $gl->validate();
//                $gl->update_reference(BREAKDOWN, $model->breakdown_id, $docref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg,
//                'print' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Breakdown');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Breakdown'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Breakdown'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->breakdown_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->breakdown_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil divoid.';
            $status = false;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $gl = new GL();
                /** @var Breakdown $model */
                $model = $this->loadModel($id, 'Breakdown');
                U::add_stock_moves_material(
                    BREAKDOWN_VOID,
                    $model->breakdown_id,
                    $model->tgl,
                    $model->material_id,
                    $model->qty,
                    null, null, null,
                    $model->doc_ref, //$model->doc_ref,
                    $model->loc_code
                );
                $gl->add_gl(BREAKDOWN_VOID, $model->breakdown_id, $model->tgl, $model->doc_ref,
                    $model->material->tipeMaterial->coa_persediaan,
                    "Breakdown $model->doc_ref",
                    "Breakdown $model->doc_ref", -$model->price, 0);
                foreach ($model->breakdownDetails as $breakdownDetail) {
                    U::add_stock_moves_material(
                        BREAKDOWN_VOID,
                        $model->breakdown_id,
                        $model->tgl,
                        $breakdownDetail->material_id,
                        -$breakdownDetail->qty,
                        null, null, null,
                        $model->doc_ref, //$model->doc_ref,
                        $breakdownDetail->loc_code
                    );
                    $gl->add_gl(BREAKDOWN_VOID, $model->breakdown_id, $model->tgl, $model->doc_ref,
                        $breakdownDetail->material->tipeMaterial->coa_persediaan,
                        "Breakdown $model->doc_ref", "Breakdown $model->doc_ref",
                        $breakdownDetail->price_std, 0);
                }
                $gl->validate();
                if (!$model->saveAttributes(['void' => 1])) {
                    throw new Exception('Breakdown gagal di void');
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg,
            ));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $params = [];
        if (isset($_POST['folder_id'])) {
            $criteria->addCondition('folder_id = :folder_id');
            $params[':folder_id'] = $_POST['folder_id'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $params;
        $model = Breakdown::model()->findAll($criteria);
        $total = Breakdown::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}