<?php
class BankTransController extends GxController
{
    public function actionCreate()
    {
        $model = new BankTrans;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['BankTrans'][$k] = $v;
            }
            $model->attributes = $_POST['BankTrans'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->bank_trans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'BankTrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['BankTrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['BankTrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->bank_trans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->bank_trans_id));
            }
        }
    }
    public function actionIndex()
    {
        $this->renderJsonArr(BankTrans::get_list_bank_transfer($_POST['tgl']));
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
//        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
//        $model = BankTrans::model()->findAll($criteria);
//        $total = BankTrans::model()->count($criteria);
//        $this->renderJson($model, $total);
    }
    public function actionGetBalance()
    {
        $sum = BankTrans::get_balance($_POST['id']);
        echo CJSON::encode(array(
            'success' => true,
            'id' => $sum));
        Yii::app()->end();
    }
    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $prt = new PrintTransfer($_POST['trans_no']);
            echo CJSON::encode(array(
                'success' => $prt != null,
                'msg' => $prt->buildTxt()));
            Yii::app()->end();
        }
    }
}