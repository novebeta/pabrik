<?php
class TandaTerimaMaterialController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $tandaterima_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new TandaterimaMaterial : $this->loadModel($tandaterima_id, "TandaterimaMaterial");
                $doc_ref = $model->doc_ref;
                if ($is_new) {
                    $doc_ref = Reference::createDocumentReference_TandaTerima($_POST['tgl']);
                } else {
                    StockMovesMaterial::model()->deleteAll("trans_no = :trans_no",
                        array(':trans_no' => $tandaterima_id));
                    TandaterimaMaterialDetails::model()->deleteAll("tandaterima_id = :tandaterima_id",
                        array(':tandaterima_id' => $tandaterima_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Terima'][$k] = $v;
                }
                $_POST['Terima']['doc_ref'] = $doc_ref;
                $model->attributes = $_POST['Terima'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $item_details = new TandaterimaMaterialDetails;
                    $_POST['TerimaDetails']['tandaterima_id'] = $model->tandaterima_id;
                    $_POST['TerimaDetails']['material_id'] = $detil['material_id'];
                    $_POST['TerimaDetails']['qty'] = get_number($detil['qty']);
//                    $_POST['TerimaDetails']['sat'] = $detil['sat'];
//                    $_POST['TerimaDetails']['tgl_expired'] = $detil['tgl_expired'];
                    $_POST['TerimaDetails']['note'] = $detil['note'];
                    $item_details->attributes = $_POST['TerimaDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    U::add_stock_moves_material(
                        TANDA_TERIMA,
                        $model->tandaterima_id,
                        $model->tgl,
                        $item_details->material_id,
                        $item_details->qty,
                        null, null, null,
                        $model->doc_ref,
                        $model->loc_code,
                        $model->supplier_id
                    );
                }
                //update status return pembelian
                ReturnPembelian::setStatus(RETURN_PEMBELIAN_RECEIVED, $model->return_pembelian_id);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $doc_ref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionGetReturnedItems()
    {
        $return_pembelian_id = isset($_POST['return_pembelian_id']) ? $_POST['return_pembelian_id'] : null;
        $arr = TandaterimaMaterial::GetReturnedItems($return_pembelian_id);
        $this->renderJsonArr($arr);
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->addCondition('tgl = :tgl');
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = TandaterimaMaterial::model()->findAll($criteria);
        $total = TandaterimaMaterial::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}