<?php
class TransferBarangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
//            $is_new = $_POST['mode'] == 0;
            $is_new = true;
            $trans_id = $_POST['trans_id'];
            $detils = CJSON::decode($_POST['detil']);
//            $storloc_pengirim = $GLOBALS['storLoc'][get_number($_POST['pengirim'])]['name'];
//            $storloc_penerima = $GLOBALS['storLoc'][get_number($_POST['penerima'])]['name'];
//            $storloc_penerima_havestock = $GLOBALS['storLoc'][get_number($_POST['penerima'])]['havestock'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new TransferBarang : $this->loadModel($trans_id, "TransferBarang");
                $doc_ref = $model->doc_ref;
                if ($is_new) {
                    $storloc_pengirim = 'TRF';
                    $storloc_penerima = 'BRG';
                    $doc_ref = Reference::createDocumentReference_Transfer($_POST['tgl'], $storloc_pengirim, $storloc_penerima);
                } else {
                    StockMoves::model()->deleteAll("trans_no = :trans_no",
                        array(':trans_no' => $trans_id));
                    TransferBarangDetail::model()->deleteAll("transfer_barang_id = :transfer_barang_id",
                        array(':transfer_barang_id' => $trans_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransferBarang'][$k] = $v;
                }
                $_POST['TransferBarang']['doc_ref'] = $doc_ref;
                $model->attributes = $_POST['TransferBarang'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $item_details = new TransferBarangDetail;
//                    $_POST['TBD']['barang_id'] = $detil['barang_id'];
//                    $_POST['TBD']['batch'] = $detil['batch'];
//                    $_POST['TBD']['tgl_exp'] = $detil['tgl_exp'];
//                    $_POST['TBD']['qty'] = get_number($detil['qty']);
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['TBD'][$k] = $v;
                    }
                    $_POST['TBD']['transfer_barang_id'] = $model->transfer_barang_id;
                    $item_details->attributes = $_POST['TBD'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                }
                //.. jika storeloc tidak maintain stock atau transIn, maka auto accept
//                if ($storloc_penerima_havestock == 0 || !$model->trans_out)
//                    $this->acceptTransfer($model->transfer_barang_id);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $doc_ref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    private function acceptTransfer($id)
    {
        $model = $this->loadModel($id, "TransferBarang");
        $model->accepted = 1;
        if (!$model->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Batch')) . CHtml::errorSummary($model));
        }
        $storloc_pengirim = $GLOBALS['storLoc'][$model->pengirim]['name'];
        $storloc_penerima = $GLOBALS['storLoc'][$model->penerima]['name'];
        $detils = TransferBarangDetail::getitems2($id);
        foreach ($detils as $detil) {
            //catat di stockmoves barang PENGIRIM
            U::add_stock_moves(
                SUPPOUT,
                $model->transfer_barang_id,
                $model->tgl,
                $detil['barang_id'],
                $detil['batch'],
                $detil['tgl_exp'],
                -$detil['qty'],
                $model->doc_ref,
                0,
                $storloc_pengirim
            );
            //catat di stockmoves barang PENERIMA
            U::add_stock_moves(
                SUPPIN,
                $model->transfer_barang_id,
                $model->tgl,
                $detil['barang_id'],
                $detil['batch'],
                $detil['tgl_exp'],
                $detil['qty'],
                $model->doc_ref,
                0,
                $storloc_penerima
            );
        }
    }
    public function actionGetItems()
    {
        $transfer_barang_id = isset($_POST['transfer_barang_id']) ? $_POST['transfer_barang_id'] : null;
        $arr = TransferBarangDetail::getitems($transfer_barang_id);
        Yii::app()->end(json_encode($arr));
    }

//    public function actionBatchInProduction()
//    {
//        $barang_id = isset($_POST['barang_id']) ? $_POST['barang_id'] : null;
//        $arr = TransferBarang::get_batch_in_production($barang_id);
//        $this->renderJsonArr($arr);
//    }
    public function actionAccept($id)
    {
        $msg = "Proses gagal.";
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $this->acceptTransfer($id);
            $transaction->commit();
            $msg = "Pengiriman Barang telah diterima.";
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            $criteria->addCondition('accepted = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
        if (isset($_POST['urole']) && $_POST['urole'] !== 'all') {
            $criteria->addCondition('pengirim = :urole OR penerima = :urole');
            $criteria->params[':urole'] = $_POST['urole'];
        }
        if (isset($_POST['uid']) && $_POST['uid'] !== 'all') {
            $criteria->addCondition('id_user = :uid');
            $criteria->params[':uid'] = $_POST['uid'];
        }
        $criteria->order = "doc_ref DESC, tgl DESC";
        $model = TransferBarang::model()->findAll($criteria);
        $total = TransferBarang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}