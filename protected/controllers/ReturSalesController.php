<?php
class ReturSalesController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $is_new = $_POST['mode'] == 0;
            $Sales_id = $_POST['id'];
            $msg = 'Retur Sales berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Yii::import('application.components.U');
                /** @var Sales $Sales */
                $Sales = $is_new ? new Sales : $this->loadModel($Sales_id,
                    "Sales");
//                $docref = $Sales->doc_ref;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Sales'][$k] = $v;
                }
//                $_POST['Sales']['doc_ref'] = $docref;
                $_POST['Sales']['arus'] = -1;
                $_POST['Sales']['id_user'] = Yii::app()->user->getId();
                $_POST['Sales']['final'] = 1;
                $_POST['Sales']['lunas'] = 0;
                $Sales->attributes = $_POST['Sales'];
                if ($is_new) {
//                    $ref = new Reference($_POST['tgl']);
//                    $docref = $ref->get_next_reference(RETURJUAL);
                    $Sales->createDocRefRetur();
                } else {
                    SalesDetails::model()->deleteAll('Sales_id  = :Sales_id', array(':Sales_id' => $Sales_id));
                }
                $Sales->sub_total = -$Sales->sub_total;
                $Sales->total = -$Sales->total;
                $Sales->bruto = -$Sales->bruto;
                $Sales->totalpot = -$Sales->totalpot;
                $Sales->tot_pot1 = -$Sales->tot_pot1;
                $Sales->tot_pot2 = -$Sales->tot_pot2;
                $Sales->tot_pot3 = -$Sales->tot_pot3;
                $Sales->vatrp = -$Sales->vatrp;
                if (!$Sales->save())
                    throw new Exception("Gagal menyimpan Retur Sales. " . CHtml::errorSummary($Sales));
                $ppn = 0;
                $hpp = 0;
                foreach ($detils as $detil) {
                    $Sales_detil = new SalesDetails;
                    $std = Material::getAvg($detil['barang_id']);
                    $_POST['SalesDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['SalesDetails']['jml'] = -get_number($detil['jml']);
                    $_POST['SalesDetails']['price'] = get_number($detil['price']);
                    $_POST['SalesDetails']['disc1'] = -get_number($detil['disc1']);
                    $_POST['SalesDetails']['disc2'] = -get_number($detil['disc2']);
                    $_POST['SalesDetails']['disc3'] = -get_number($detil['disc3']);
                    $_POST['SalesDetails']['pot1'] = -get_number($detil['pot1']);
                    $_POST['SalesDetails']['pot2'] = -get_number($detil['pot2']);
                    $_POST['SalesDetails']['pot3'] = -get_number($detil['pot3']);
                    $_POST['SalesDetails']['totalpot'] = -get_number($detil['totalpot']);
                    $_POST['SalesDetails']['bruto'] = -get_number($detil['bruto']);
                    $_POST['SalesDetails']['nominal'] = -get_number($detil['nominal']);
                    $_POST['SalesDetails']['ketpot'] = $detil['ketpot'];
                    $_POST['SalesDetails']['vat'] = -get_number($detil['vat']);
                    $_POST['SalesDetails']['vatrp'] = -get_number($detil['vatrp']);
                    $_POST['SalesDetails']['loc_code'] = $detil['loc_code'];
                    $_POST['SalesDetails']['hpp'] = (get_number($detil['jml']) * $std);
                    $_POST['SalesDetails']['sales_id'] = $Sales->sales_id;
                    $Sales_detil->attributes = $_POST['SalesDetails'];
                    if (!$Sales_detil->save())
                        throw new Exception("Gagal menyimpan Retur Sales detil. " .
                            CHtml::errorSummary($Sales_detil));
                    $hpp += $Sales_detil->hpp;
                    U::add_stock_moves(RETURJUAL, $Sales->sales_id, $Sales->tgl,
                        $Sales_detil->barang_id, abs($Sales_detil->jml), $Sales->doc_ref,
                        $Sales_detil->price, $std, $Sales_detil->loc_code, null);
                }
                $hpp = round($hpp);
//                if ($is_new) {
//                    $ref->save(PENJUALAN, $Sales->sales_id, $docref);
//                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
//                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ReturSales');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['ReturSales'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ReturSales'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sales_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sales_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ReturSales')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = [];
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->params = $param;
        $model = ReturSales::model()->findAll($criteria);
        $total = ReturSales::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}