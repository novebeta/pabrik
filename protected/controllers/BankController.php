<?php
class BankController extends GxController
{
    public function actionCreate()
    {
        $model = new Bank;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
            //$msg = "Akun bank berhasil dibuat";
            $model = new Bank;
//            if (U::account_in_gl_trans($id)) {
//                $status = false;
//                $msg = "COA $id already used in GL transaction";
//            }
//            elseif (U::account_used_bank($id)) {
//                $status = false;
//                $msg = t('coa.fail.use.model', 'app', array('{coa}' => $id, '{model}' => 'Bank'));
//            }
//            if (!$status) {
//                echo CJSON::encode(array(
//                    'success' => $status,
//                    'msg' => $msg
//                ));
//                Yii::app()->end();
//            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Bank'][$k] = $v;
            }
            $model->attributes = $_POST['Bank'];
            $msg = t('save.fail', 'app');
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->bank_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Bank');
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
            //$msg = "Akun bank berhasil dibuat";
//            if (U::account_in_gl_trans($id)) {
//                $status = false;
//                $msg = "COA $id already used in GL transaction";
//            }
//            elseif (U::account_used_bank($id)) {
//                $status = false;
//                $msg = t('coa.fail.use.model', 'app', array('{coa}' => $id, '{model}' => 'Bank'));
//            }
//            if (!$status) {
//                echo CJSON::encode(array(
//                    'success' => $status,
//                    'msg' => $msg
//                ));
//                Yii::app()->end();
//            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Bank'][$k] = $v;
            }
            $msg = t('save.fail', 'app');
            $model->attributes = $_POST['Bank'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->bank_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->bank_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
//        $arr = explode(',', SysPrefs::get_val('filter_payment'));
//        if (isset ($_POST['mode']) && $_POST['mode'] == 'bank_cabang') {
//            $criteria->addCondition('store = :store');// = "bank_id not in (:cash_ho,:cash_while) AND store = :store";
//            $criteria->addNotInCondition('bank_id', explode(',', SysPrefs::get_val('filter_payment')));// = "bank_id not in (:cash_ho,:cash_while) AND store = :store";
//            $criteria->params[':store'] = STOREID;
//        } elseif (isset ($_POST['mode']) && $_POST['mode'] == 'bank_trans_cabang') {
//            $criteria->alias = "nb";
//            $criteria->join = "inner join nscc_chart_master ncm on (nb.account_code = ncm.account_code)";
//            $criteria->condition = "ncm.kategori in (:bank,:kas) AND store = :store";
//            $criteria->params = array(
//                ':bank' => COA_GRUP_BANK,
//                ':kas' => COA_GRUP_KAS,
//                ':store' => STOREID
//            );
//        } elseif (isset ($_POST['mode']) && $_POST['mode'] == 'bank_trans_pusat') {
//            $criteria->alias = "nb";
//            $criteria->join = "inner join nscc_chart_master ncm on (nb.account_code = ncm.account_code)";
//            $criteria->condition = "ncm.kategori in (:bank,:kas)";
//            $criteria->params = array(
//                ':bank' => COA_GRUP_BANK,
//                ':kas' => COA_GRUP_KAS
//            );
//        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Bank::model()->findAll($criteria);
        $total = Bank::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Bank')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}