<?php
class BatchHasilController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            //$is_new = $_POST['mode'] == 0;
            $batch_id = $_POST['batch_id'];
            $barang_id = $_POST['barang_id'];
            $final = isset($_POST['final']) ? true : false;
            $detail = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                BatchHasil::model()->deleteAll("batch_id = :batch_id", array(':batch_id' => $batch_id));
                StockMovesMaterial::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                    array(':type_no' => STANDAR_KONSUMSI, ':trans_no' => $batch_id));
                StockMovesMaterial::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                    array(':type_no' => ADDITIONAL_KONSUMSI, ':trans_no' => $batch_id));
                StockMovesMaterial::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                    array(':type_no' => REDUCTION_KONSUMSI, ':trans_no' => $batch_id));
                StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                    array(':type_no' => HASIL_PRODUKSI, ':trans_no' => $batch_id));
                $qty_hasil = 0;
                $total_consume = array();
                $modelBatch = $this->loadModel($batch_id, "Batch");
                $t = $modelBatch->get_qty_bom_and_prepared();
                foreach ($detail as $detil) {
                    $item_details = new BatchHasil();
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['BatchHasil'][$k] = $v;
                    }
                    $_POST['BatchHasil']['batch_id'] = $batch_id;
                    $_POST['BatchHasil']['barang_id'] = $barang_id;
                    $item_details->attributes = $_POST['BatchHasil'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Batch Hasil')) . CHtml::errorSummary($item_details));
                    }
                    $qty_hasil += get_number($detil['qty']);
                    //catat konsumsi di stockmoves_material
//                    foreach($t as $r) {
//                        $qty_std_consume = $r['qty_bom']*(($item_details->qty*$modelBatch->qty_per_pot)/$modelBatch->qty);
//                        $qty = $qty_std_consume*($r['qty']/$r['qty_prepared']);
//                        
//                        U::add_stock_moves_material(
//                            STANDAR_KONSUMSI,
//                            $batch_id,
//                            $item_details->tgl,
//                            $r['material_id'],
//                            -$qty,
//                            $r['tgl_expired'],
//                            $r['no_lot'],
//                            $r['no_qc'],
//                            $modelBatch->no_batch,
//                            $GLOBALS['storLoc'][USER_PRODUKSI]['name']
//                        );
//                        if(!array_key_exists($r['material_id'].$r['tgl_expired'].$r['no_lot'].$r['no_qc'], $total_consume))
//                                $total_consume[$r['material_id'].$r['tgl_expired'].$r['no_lot'].$r['no_qc']] = 0;
//                        $total_consume[$r['material_id'].$r['tgl_expired'].$r['no_lot'].$r['no_qc']] += $qty;
//                    }
                    //catat penambahan stock barang di produksi
                    U::add_stock_moves(
                        HASIL_PRODUKSI,
                        $item_details->batch_id,
                        $item_details->tgl,
                        $item_details->barang_id,
                        $item_details->no_batch,
                        $item_details->tgl_exp,
                        $item_details->qty,
                        $modelBatch->no_batch,
                        0,
                        $GLOBALS['storLoc'][USER_PRODUKSI]['name']
                    );
                }
                if ($final) {
                    //jika konfirmasi hasil produksi terakhir/final
                    //maka sisa bahan baku belum di-consume maka dikonsumkan semua
                    foreach ($t as $r) {
                        U::add_stock_moves_material(
                            STANDAR_KONSUMSI,
                            $batch_id,
                            date("Y-m-d"),
                            $r['material_id'],
                            -$r['qty'],
                            $r['tgl_expired'],
                            $r['no_lot'],
                            $r['no_qc'],
                            $modelBatch->no_batch,
                            $GLOBALS['storLoc'][USER_PRODUKSI]['name'],
                            $r['supplier_id']
                        );
                    }
                }
//                if($final){
//                    //jika konfirmasi hasil produksi terakhir/final
//                    //maka sisa bahan baku belum di-consume maka dikonsumkan semua
//                    foreach($t as $r) {
//                        $qty_consumed = $total_consume[$r['material_id'].$r['tgl_expired'].$r['no_lot'].$r['no_qc']];
//                        if($qty_consumed<$r['qty']){
//                            $qty = $r['qty']-$qty_consumed; //ADDITIONAL_KONSUMSI
//                        }else if($qty_consumed>$r['qty']){
//                            $qty = $qty_consumed-$r['qty']; //REDUCTION_KONSUMSI
//                        }
//
//                        U::add_stock_moves_material(
//                            ($qty_consumed<$r['qty'])?ADDITIONAL_KONSUMSI:REDUCTION_KONSUMSI,
//                            $batch_id,
//                            date("Y-m-d"),
//                            $r['material_id'],
//                            -$qty,
//                            $r['tgl_expired'],
//                            $r['no_lot'],
//                            $r['no_qc'],
//                            $modelBatch->no_batch,
//                            $GLOBALS['storLoc'][USER_PRODUKSI]['name']
//                        );
//                    }
//                }
                $modelBatch->qty_hasil = $qty_hasil;
                if ($final) $modelBatch->status = BATCH_FINISHED;
                if (!$modelBatch->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Batch')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                //'id' => $doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('batch_id = :batch_id');
        $criteria->params = array(':batch_id' => $_POST['batch_id']);
        $criteria->order = "tgl";
        $model = BatchHasil::model()->findAll($criteria);
        $total = BatchHasil::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}