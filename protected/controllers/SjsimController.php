<?php
class SjsimController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $sjsim_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Sjsim() : $this->loadModel($sjsim_id, "Sjsim");
                $docref = $model->doc_ref;
                if ($is_new) {
                    /*
//                    $ref = new Reference($_POST['tgl']);
//                    $docref = $ref->get_next_reference(RETURBELI);
                    $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => RETURBELI));
                    $docref = $systype->next_reference;
                    if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $docref, $result) == 1) {
                        list($all, $prefix, $year, $number) = $result;
                        $tgl = strtotime($_POST['tgl']);
                        $year_new = date("dmy", $tgl);
                        $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                        $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                        $val = intval($number +  1);
                        $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
                        $systype->next_reference = "$prefix/$year_new/$nextval";
                        $docref = $systype->next_reference;
                    }
                    if (!$systype->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                            CHtml::errorSummary($systype));
                    }*/
                    $docref = Reference::createDocumentReference(RETURBELI, $_POST['tgl'], "dmy");
                } else {
                    StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                        array(':type_no' => RETURBELI, ':trans_no' => $sjsim_id));
                    SjsimDetails::model()->deleteAll("sjsim_id = :sjsim_id",
                        array(':sjsim_id' => $sjsim_id));
                }
//                $model->doc_ref_inv = str_replace('SJ', 'INV', $docref);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Sjsim'][$k] = $v;
                }
                if ($is_new) {
                    $query = Sjsim::model()->dbConnection->createCommand("SELECT UUID();");
                    $uuid = $query->queryScalar();
                    $model->sjsim_id = $uuid;
                }
                $_POST['Sjsim']['doc_ref'] = $docref;
                $model->attributes = $_POST['Sjsim'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan')) . CHtml::errorSummary($model));
                $total = $total_tax = 0;
                foreach ($detils as $detil) {
                    $item_details = new SjsimDetails();
                    $query = SjsimDetails::model()->dbConnection->createCommand("SELECT UUID();");
                    $uuid = $query->queryScalar();
                    $item_details->sjsim_details_id = $uuid;
                    $_POST['SjsimDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['SjsimDetails']['batch'] = $detil['batch'];
                    $_POST['SjsimDetails']['tgl_exp'] = $detil['tgl_exp'];
                    $_POST['SjsimDetails']['ket'] = $detil['ket'];
                    $_POST['SjsimDetails']['qty'] = get_number($detil['qty']);
                    $_POST['SjsimDetails']['sjsim_id'] = $model->sjsim_id;
                    $item_details->attributes = $_POST['SjsimDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan detil')) . CHtml::errorSummary($item_details));
                    U::add_stock_moves(
                        RETURBELI,
                        $model->sjsim_id,
                        $model->tgl,
                        $item_details->barang_id,
                        $item_details->batch,
                        $item_details->tgl_exp,
                        -$item_details->qty,
                        $model->doc_ref,
                        0,
                        $GLOBALS['storLoc'][USER_GPJ]['name']
                    );
                }
                $model->total = $total;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan')) . CHtml::errorSummary($model));
//                if ($is_new) {
//                    $ref->save(SJ, $model->sjsim_id, $docref);
//                }
//                $model->create_simulation();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Sjsim');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Sjsim'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Sjsim'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sjsim_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sjsim_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Sjsim')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('tgl = :tgl');
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = Sjsim::model()->findAll($criteria);
        $total = Sjsim::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionCreateAudit()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        $sjsims = CJSON::decode($_POST['sjsim_id']);
        $total = get_number($_POST['total']);
        $total_input = get_number($_POST['total_input']);
        $persen_tax = $total_input / $total;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($sjsims as $sjsim) {
                $loadsj = $this->loadModel($sjsim, "Sjsim");
//                $loadsj = new Sjsim;
                $loadsj->persen_tax = $persen_tax;
                $loadsj->total_input = $total_input;
                if (!$loadsj->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Simulasi')) .
                        CHtml::errorSummary($loadsj));
                $sj = $loadsj->sj;
                $sj->adt = 1;
                if (!$sj->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan')) .
                        CHtml::errorSummary($sj));
                $sj->create_IntTax($persen_tax);
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }
}