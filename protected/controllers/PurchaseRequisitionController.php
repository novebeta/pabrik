<?php
class PurchaseRequisitionController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $pr_id = $_POST['pr_id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new PurchaseRequisition : $this->loadModel($pr_id, "PurchaseRequisition");
                $docref = $model->doc_ref;
                //jika edit PR
                if (!$is_new) {
                    if (!$model->isNewRecord && $model->adt != 0) {
                        throw new Exception("Purchase Requisition tidak bisa diedit karena sudah di audit.");
                    }
                    PurchaseRequisitionDetails::model()->deleteAll("pr_id= :pr_id", array(':pr_id' => $pr_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['PurchaseRequisition'][$k] = $v;
                }
                //jika buat PR baru
                if ($is_new) {
                    $docref = Reference::createDocumentReference_PR($_POST['PurchaseRequisition']['tipe_material_id'], $_POST['tgl']); //====== Doc.Ref Baru
                    $_POST['PurchaseRequisition']['doc_ref'] = $docref;
                    $_POST['PurchaseRequisition']['id_user'] = User()->getId();
                }
                $model->attributes = $_POST['PurchaseRequisition'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Surat Jalan')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $item_details = new PurchaseRequisitionDetails;
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) {
                            $v = get_number($v);
                        }
                        $_POST['PurchaseRequisitionDetails'][$k] = $v;
                    }
                    $_POST['PurchaseRequisitionDetails']['pr_detail_id'] = null;
                    $_POST['PurchaseRequisitionDetails']['pr_id'] = $model->pr_id;
                    $item_details->attributes = $_POST['PurchaseRequisitionDetails'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Purchase Requisition detil')) . CHtml::errorSummary($item_details));
                    }
                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Purchase Requisition')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $msg = t('save.success', 'app') . "<br>No. PR : " . $docref;
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionCancel($id) //DESPERATED
    {
        $model = $this->loadModel($id, "PurchaseRequisition");
        //$model->status = PR_CANCELED;
        $msg = "";
        if ($model->save()) {
            $status = true;
            $msg = "PR berhasil dibatalkan.<br>No. PR : " . $model->doc_ref;
        } else {
            $msg = "Proses pembatalan PR tidak berhasil.<br>" . CHtml::errorSummary($model);
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionReject()
    {
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "failed";
            $pr_id = $_POST['pr_id'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($pr_id, "PurchaseRequisition");
                $model->status = PR_REJECTED;
                $model->reject_tgl = $_POST['tgl_reject'];
                $model->reject_no_ba = $_POST['no_ba'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Purchase Requisition')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $msg = "Proses reject PR berhasil.<br> No. PR : " . $model->doc_ref;
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = "Proses reject PR tidak berhasil.<br>" . $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                //'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionGetAudit()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST['tglfrom']) && isset($_POST['tglto'])) {
            $audit = U::get_list_audit($_POST['tglfrom'], $_POST['tglto']);
            $this->renderJsonArr($audit);
        } else {
            $this->redirect(url('/'));
        }
    }
    public function actionCreateAudit()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        $sjsims = CJSON::decode($_POST['pr_id']);
        $total = get_number($_POST['total']);
        $total_input = get_number($_POST['total_input']);
        $persen_tax = $total_input / $total;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($sjsims as $sjsim) {
                $sj = $this->loadModel($sjsim, "PurchaseRequisition");
//                $loadsj = new PurchaseRequisitionsim;
//                $loadsj->persen_tax = $persen_tax;
//                $loadsj->total_input = $total_input;
//                if (!$loadsj->save())
//                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Simulasi')) .
//                        CHtml::errorSummary($loadsj));
//                $sj = $loadsj->sj;
                $sj->adt = 1;
                if (!$sj->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan')) .
                        CHtml::errorSummary($sj));
                }
                self::create_Tax($sj);
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['supplier_id'])) {
            $criteria->addCondition('supplier_id = :supplier_id');
            $criteria->params[':supplier_id'] = $_POST['supplier_id'];
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            $criteria->addCondition('status = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
        if (isset($_POST['uid']) && $_POST['uid'] !== 'all') {
            $criteria->addCondition('id_user = :uid');
            $criteria->params[':uid'] = $_POST['uid'];
        }
        if (isset($_POST['urole'])) {
            switch ($_POST['urole']) {
                case USER_PPIC:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_PPIC]);
                    break;
                case USER_GA:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_GA]);
                    break;
            }
        }
        $criteria->order = "tgl DESC";
        $model = PurchaseRequisition::model()->findAll($criteria);
        $total = PurchaseRequisition::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}