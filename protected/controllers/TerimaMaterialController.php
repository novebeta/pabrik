<?php
class TerimaMaterialController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $tr_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new TerimaMaterial : $this->loadModel($tr_id, "TerimaMaterial");
                $docref = $model->doc_ref;
                //...jika edit
                if (!$is_new) {
                    TerimaMaterialDetails::model()->deleteAll("terima_material_id = :terima_material_id",
                        array(':terima_material_id' => $tr_id));
                    StockMovesMaterial::model()->deleteAll("trans_no = :trans_no",
                        array(':trans_no' => $tr_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TerimaMaterial'][$k] = $v;
                }
                //...jika buat baru
                if ($is_new) {
                    $docref = Reference::createDocumentReference_SJsupplier(SJ_SUPPLIER, $_POST['tgl_terima']); //====== Doc.Ref Baru
                    $_POST['TerimaMaterial']['doc_ref'] = $docref;
                    //$_POST['TerimaMaterial']['id_user'] = User()->getId();
                    //$_POST['TerimaMaterial']['urole'] = U::get_user_role();
                    $modelSupp = $this->loadModel($_POST['TerimaMaterial']['supplier_id'], "Supplier");
                    $model->term_of_payment = $modelSupp->term_of_payment;
                }
                $model->attributes = $_POST['TerimaMaterial'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Penerimaan Material')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $item_details = new TerimaMaterialDetails;
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['TerimaMaterialDetails'][$k] = $v;
                    }
                    $_POST['TerimaMaterialDetails']['terima_material_id'] = $model->terima_material_id;
                    $item_details->attributes = $_POST['TerimaMaterialDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    //catat di stockmoves_material
                    U::add_stock_moves_material(
                        SJ_SUPPLIER,
                        $model->terima_material_id,
                        $model->tgl_terima,
                        $item_details->material_id,
                        $item_details->qty,
                        $item_details->tgl_expired,
                        $item_details->no_lot,
                        $item_details->no_qc,
                        $model->no_sj_supplier, //$model->doc_ref,
                        'PUSAT',
                        $model->supplier_id
                    );
                    //.. update status PO
                    $statusPO = PurchaseOrder::delivery_is_completed($item_details->po_id) ? PO_RECEIVED : PO_PARTIALLY_RECEIVED;
                    PurchaseOrder::setStatus($statusPO, $item_details->po_id);
                    //.. update status PO untuk PR bersangkutan
//                    $modelPO = $this->loadModel($item_details->po_id, "PurchaseOrder");
//                    PurchaseRequisition::setStatus_PO($statusPO, $modelPO->pr_id);
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionCreateInvoice()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $tr_id = $_POST['id'];
            $tipeInvoice = get_number($_POST['tipeInvoice']);
            $createNew =
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = ($is_new && $tipeInvoice != TIPE_INV_DEFAULT) ? new TerimaMaterial : $this->loadModel($tr_id, "TerimaMaterial");
                $docref = $model->doc_ref_inv;
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TerimaMaterial'][$k] = $v;
                }
                //...jika buat baru
                if ($is_new) {
                    $_POST['TerimaMaterial']['tipe_invoice'] = $tipeInvoice;
                    if ($tipeInvoice == TIPE_INV_DEFAULT)
                        $_POST['TerimaMaterial']['doc_ref_inv'] = str_replace('SJ', 'INV', $model->doc_ref);
                }
                $model->attributes = $_POST['TerimaMaterial'];
                $model->status = SJ_SUPPLIER_INVOICED;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Penerimaan Material')) . CHtml::errorSummary($model));
                if (!$is_new && $tipeInvoice != TIPE_INV_DEFAULT) {
                    TerimaMaterialDetails::model()->deleteAll("terima_material_id= :terima_material_id",
                        array(':terima_material_id' => $model->terima_material_id));
                }
                foreach ($detils as $detil) {
                    if ($tipeInvoice != TIPE_INV_DEFAULT) {
                        $item_details = new TerimaMaterialDetails;
                        foreach ($detil as $k => $v) {
                            if (is_angka($v)) $v = get_number($v);
                            $_POST['InvoiceDetails'][$k] = $v;
                        }
                        $_POST['InvoiceDetails']['terima_material_id'] = $model->terima_material_id;
                        $item_details->attributes = $_POST['InvoiceDetails'];
                    } else {
                        $item_details = $this->loadModel($detil['terima_material_detail_id'], "TerimaMaterialDetails");
                        $item_details->price = $detil['price'];
                    }
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionGetPoItems()
    {
        $po_id = isset($_POST['po_id']) ? $_POST['po_id'] : null;
        $arr = TerimaMaterial::getPOitems($po_id);
        $this->renderJsonArr($arr);
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl_terima = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            //untuk query Grid Surat Jalan Supplier
            $criteria->addCondition('status = :status');
            $criteria->params[':status'] = $_POST['status'];
            if (isset($_POST['pembayaran_supplier_id'])) {
                //dipanggil ketika Edit FPT/PembayaranSupplier
                $criteria->addCondition('pembayaran_supplier_id = :pembayaran_supplier_id', 'OR');
                $criteria->params[':pembayaran_supplier_id'] = $_POST['pembayaran_supplier_id'];
            }
        } else if (isset($_POST['status']) && $_POST['status'] == 'all') {
            //untuk query Grid Invoice
            //jika status adalah 'all' maka
            //menampilkan semua data status SJ_SUPPLIER_INVOICED, SJ_SUPPLIER_FPT, SJ_SUPPLIER_PAID
            if (isset($_POST['filter']) && $_POST['filter'] == 'invoice') {
                $criteria->addCondition('status >= :status');
                $criteria->params[':status'] = SJ_SUPPLIER_INVOICED;
            }
        }
        if (isset($_POST['uid']) && $_POST['uid'] !== 'all') {
            $criteria->addCondition('id_user = :uid');
            $criteria->params[':uid'] = $_POST['uid'];
        }
        if (isset($_POST['urole']) && $_POST['urole'] !== 'all') {
            $criteria->addCondition('urole = :urole');
            $criteria->params[':urole'] = $_POST['urole'];
        }
        if (isset($_POST['supplier_id'])) {
            $criteria->addCondition('supplier_id = :supplier_id');
            $criteria->params[':supplier_id'] = $_POST['supplier_id'];
        }
        if (isset($_POST['terima_material_id'])) {
            $criteria->addCondition('terima_material_id = :terima_material_id');
            $criteria->params[':terima_material_id'] = $_POST['terima_material_id'];
        }
        if (isset($_POST['sj_only'])) {
            $criteria->addCondition('tipe_invoice = ' . TIPE_INV_DEFAULT);
        }
        $criteria->order = "tgl_terima DESC";
        $model = TerimaMaterial::model()->findAll($criteria);
        $total = TerimaMaterial::model()->count($criteria);
        $this->renderJson($model, $total);
    }

}