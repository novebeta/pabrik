<?php
class SupplierPaymentController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $tb_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new SupplierPayment : $this->loadModel($tb_id, "SupplierPayment");
                $docref = $model->doc_ref;
                if ($is_new) {
                    $model->createDocRef();
                    $docref = $model->doc_ref;
//                    $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => SUPP_PAYMENT));
//                    $docref = $systype->next_reference;
//                    if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $docref, $result) == 1) {
//                        list($all, $prefix, $year, $number) = $result;
//                        $tgl = strtotime($_POST['tgl']);
//                        $year_new = date("dmy", $tgl);
//                        if ($number == '9999999999') {
//                            $nextval = '0000000001';
//                        } else {
//                            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
//                            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
//                            $val = intval($number + 1);
//                            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
//                        }
//                        $systype->next_reference = "$prefix/$year_new/$nextval";
//                        $docref = $systype->next_reference;
//                    }
//                    if (!$systype->save()) {
//                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
//                            CHtml::errorSummary($systype));
//                    }
                } else {
                    SupplierPaymentDetil::model()->deleteAll("supplier_payment_id = :supplier_payment_id",
                        array(':supplier_payment_id' => $tb_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['SupplierPayment'][$k] = $v;
                }
//                $_POST['SupplierPayment']['doc_ref'] = $docref;
                $model->attributes = $_POST['SupplierPayment'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Supplier Payment')) .
                        CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $item_details = new SupplierPaymentDetil;
                    $_POST['SupplierPaymentDetil']['reference_item_id'] = $detil['reference_item_id'];
                    $_POST['SupplierPaymentDetil']['doc_ref'] = $detil['doc_ref'];
                    $_POST['SupplierPaymentDetil']['no_faktur'] = $detil['no_faktur'];
                    $_POST['SupplierPaymentDetil']['tgl'] = $detil['tgl'];
                    $_POST['SupplierPaymentDetil']['payment_tipe'] = $detil['payment_tipe'];
                    $_POST['SupplierPaymentDetil']['kas_dibayar'] = get_number($detil['kas_dibayar']);
                    $_POST['SupplierPaymentDetil']['sisa'] = get_number($detil['sisa']);
                    $_POST['SupplierPaymentDetil']['supplier_payment_id'] = $model->supplier_payment_id;
                    $item_details->attributes = $_POST['SupplierPaymentDetil'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Supplier Payment item detail')) . CHtml::errorSummary($item_details));
                    }
                    if ($item_details->sisa == 0) {
                        if ($item_details->payment_tipe == 0) {
                            /** @var $terima TerimaBarang */
                            $terima = TerimaBarang::model()->findByPk($item_details->reference_item_id);
                            if ($terima == null) {
                                throw new Exception('Fatal error, Goods Receival Notes tidak ditemukan!!!');
                            }
                            $terima->lunas = 1;
                            if (!$terima->save()) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => 'Goods Receival Notes')) . CHtml::errorSummary($terima));
                            }
                        } elseif ($item_details->payment_tipe == 1) {
                            /** @var $terima ReturnPembelian */
                            $terima = ReturnPembelian::model()->findByPk($item_details->reference_item_id);
                            if ($terima == null) {
                                throw new Exception('Fatal error, Nota Return tidak ditemukan!!!');
                            }
                            $terima->nota_return_status = NOTA_RETURN_USED;
                            if (!$terima->save()) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => 'Goods Receival Notes')) . CHtml::errorSummary($terima));
                            }
                        }
//                        elseif($item_details->payment_tipe == 2){
//                            /** @var $terima SuppCreditNote*/
//                            $terima = SuppCreditNote::model()->findByPk($item_details->reference_item_id);
//                            if($terima == null){
//                                throw new Exception('Fatal error, Supplier Credit Note tidak ditemukan!!!');
//                            }
//                            $terima->lunas = 1;
//                            if (!$terima->save()) {
//                                throw new Exception(t('save.model.fail', 'app',
//                                        array('{model}' => 'Supplier Credit Note')) . CHtml::errorSummary($terima));
//                            }
//                        }elseif($item_details->payment_tipe == 3){
//                            /** @var $terima PerlengkapanItem */
//                            $terima = PerlengkapanItem::model()->findByPk($item_details->reference_item_id);
//                            if($terima == null){
//                                throw new Exception('Fatal error, Pembelian perlengkapan tidak ditemukan!!!');
//                            }
//                            $terima->lunas = 1;
//                            if (!$terima->save()) {
//                                throw new Exception(t('save.model.fail', 'app',
//                                        array('{model}' => 'Perlengkapan Item')) . CHtml::errorSummary($terima));
//                            }
//                        }elseif($item_details->payment_tipe == 4){
//                            /** @var $terima PembelianBarangasset */
//                            $terima = PembelianBarangasset::model()->findByPk($item_details->reference_item_id);
//                            if($terima == null){
//                                throw new Exception('Fatal error, Pembelian aset tidak ditemukan!!!');
//                            }
//                            $terima->lunas = 1;
//                            if (!$terima->save()) {
//                                throw new Exception(t('save.model.fail', 'app',
//                                        array('{model}' => 'Pembelian assets')) . CHtml::errorSummary($terima));
//                            }
//                        }
                    }
                    Apar::addHutang(SUPP_PAYMENT, $model->supplier_payment_id, $model->tgl, $model->supplier_id,
                        $model->doc_ref, -$item_details->kas_dibayar, $item_details->reference_item_id);
                }
                //GL Hutang
                //          kas
                $gl = new GL();
                $gl->add_gl(SUPP_PAYMENT, $model->supplier_payment_id, $model->tgl, $model->doc_ref, $model->supplier->account_code,
                    "Supplier Payment " . $model->doc_ref, "Supplier Payment " . $model->doc_ref, $model->total, 1);
                $gl->add_gl(SUPP_PAYMENT, $model->supplier_payment_id, $model->tgl, $model->doc_ref, $model->bank->account_code,
                    "Supplier Payment " . $model->doc_ref, "Supplier Payment " . $model->doc_ref, -$model->total, 0);
                $gl->validate();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SupplierPayment');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['SupplierPayment'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SupplierPayment'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_payment_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->supplier_payment_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SupplierPayment')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->order = "tgl DESC, doc_ref DESC";
        $criteria->params = $param;
        $model = SupplierPayment::model()->findAll($criteria);
        $total = SupplierPayment::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}