<?php
class SupplierPaymentDetilController extends GxController
{
    public function actionCreate()
    {
        $model = new SupplierPaymentDetil;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['SupplierPaymentDetil'][$k] = $v;
            }
            $model->attributes = $_POST['SupplierPaymentDetil'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_payment_detil_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SupplierPaymentDetil');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['SupplierPaymentDetil'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SupplierPaymentDetil'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_payment_detil_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->supplier_payment_detil_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SupplierPaymentDetil')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['supplier_id'])) {
            $criteria->addCondition('supplier_id = :supplier_id');
            $criteria->params = array(':supplier_id' => $_POST['supplier_id']);
            $model = SuppOutstanding::model()->findAll($criteria);
            $total = SuppOutstanding::model()->count($criteria);
            $this->renderJson($model, $total);
            Yii::app()->end();
        }
        if (isset($_POST['supplier_payment_id'])) {
            $criteria->addCondition('supplier_payment_id = :supplier_payment_id');
            $param[':supplier_payment_id'] = $_POST['supplier_payment_id'];
        }
        $criteria->params = $param;
        $model = SupplierPaymentDetil::model()->findAll($criteria);
        $total = SupplierPaymentDetil::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}