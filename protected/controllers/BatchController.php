<?php
class BatchController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $batch_id = $_POST['batch_id'];
            $detail = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Batch() : $this->loadModel($batch_id, "Batch");
                if (!$is_new) {
                    BatchDetail::model()->deleteAll("batch_id = :batch_id", array(':batch_id' => $model->batch_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Batch'][$k] = $v;
                }
                $model->attributes = $_POST['Batch'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Bill Of Material')) . CHtml::errorSummary($model));
                }
                $doc_ref = $model->no_batch;
                foreach ($detail as $detil) {
                    $item_details = new BatchDetail();
                    $_POST['BatchDetail']['batch_id'] = $model->batch_id;
                    $_POST['BatchDetail']['qty'] = get_number($detil['qty']);
                    $_POST['BatchDetail']['sat'] = $detil['sat'];
                    $_POST['BatchDetail']['material_id'] = $detil['material_id'];
                    $item_details->attributes = $_POST['BatchDetail'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Batch Detail')) . CHtml::errorSummary($item_details));
                    }
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
            $criteria->addCondition($_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'");
            //$criteria->join = "LEFT JOIN pbu_bom ON pbu_bom.bom_id = t.bom_id ";
            //$criteria->join .= "LEFT JOIN pbu_barang ON pbu_bom.barang_id = t.barang_id ";
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['status'])) {
            if ($_POST['status'] !== 'all') {
                $criteria->addCondition('status = :status');
                $criteria->params[':status'] = $_POST['status'];
            } else if ($_POST['status'] == 'all') {
                $criteria->addCondition('status <> :status');
                $criteria->params[':status'] = BATCH_FINISHED;
            }
        }
        if (isset($_POST['batch_id'])) {
            $criteria->addCondition('batch_id = :batch_id');
            $criteria->params[':batch_id'] = $_POST['batch_id'];
        }
        $criteria->order = "tgl_produksi";
        $model = Batch::model()->findAll($criteria);
        $total = Batch::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionInputHasilMixing($id)
    {
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            $msg = "";
            try {
                $model = $this->loadModel($id, 'Batch');
                $qty_mixing = get_number($_POST['qty_mixing']);
                $model->qty_mixing = $qty_mixing;
                $model->tgl_mixing = $_POST['tgl_mixing'];
                $model->tgl_expired = $_POST['tgl_expired'];
                if ($model->status == BATCH_PREPARED) $model->status = BATCH_MIXED;
                if ($model->save()) {
                    $status = true;
                    $msg = "Konfirmasi hasil mixing jadi berhasil. No Batch : " . $model->no_batch;
                } else {
                    $msg = "Konfirmasi hasil mixing tidak berhasil " . CHtml::errorSummary($model);
                    $status = false;
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionInputHasil($id)
    {
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            $msg = "";
            try {
                $model = $this->loadModel($id, 'Batch');
                $qty_hasil = get_number($_POST['qty_hasil']);
                $model->qty_hasil += $qty_hasil;
                $model->status = $model->qty_hasil < $model->qty ? BATCH_MIXED : BATCH_FINISHED;
                if ($model->save()) {
                    $status = true;
                    $msg = "Konfirmasi barang jadi berhasil. No Batch : " . $model->no_batch;
                } else {
                    $msg = "Konfirmasi barang jadi tidak berhasil " . CHtml::errorSummary($model);
                    $status = false;
                }
                $bom_model = Bom::model()->find("bom_id = :bom_id", array(':bom_id' => $model->bom_id));
                U::add_stock_moves(
                    HASIL_PRODUKSI,
                    $model->batch_id,
                    $_POST['tgl'],
                    $bom_model->barang_id,
                    $model->no_batch,
                    $_POST['tgl_exp'],
                    $model->qty_hasil,
                    $model->no_batch, 0);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

//    public function actionTransferGudang($id)
//    {
//        if (isset($_POST) && !empty($_POST)) {
////            app()->db->autoCommit = false;
////            $transaction = Yii::app()->db->beginTransaction();
////            $msg = "";
////            try{
//                $model = $this->loadModel($id, 'Batch');
////                $qty_hasil = get_number($_POST['tgl_transfer_gudang']);
////                
////                if($model->qty_hasil != $qty_hasil){
//                    $model->tgl_transfer_gudang = $_POST['tgl_transfer_gudang'];
//                    if ($model->save()) {
//                        $status = true;
//                        $msg = "Data berhasil di simpan dengan id " . $model->batch_id;
//                    } else {
//                        $msg = "Data gagal disimpan " . CHtml::errorSummary($model);
//                        $status = false;
//                    }
//                    
////                    $this->_clearStockMovesProduk($id);
////                    $this->_stockMovesProduk($model);
////                }
////                
////                $transaction->commit();
////                $msg = t('save.success', 'app');
////                $status = true;
////            } catch (Exception $ex) {
////                $transaction->rollback();
////                $status = false;
////                $msg = $ex->getMessage();
////            }
//            
//            echo CJSON::encode(array(
//                'success' => $status,
//                'msg' => $msg));
//            Yii::app()->end();
//        }
//    }
}