<?php
class ChartClassController extends GxController
{
    public function actionCreate()
    {
        $model = new ChartClass;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['ChartClass'][$k] = $v;
            }
            $model->attributes = $_POST['ChartClass'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->cid;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ChartClass');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['ChartClass'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ChartClass'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->cid;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->cid));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ChartClass')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = ChartClass::model()->findAll($criteria);
        $total = ChartClass::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionType()
    {
        global $class_types;
//        $argh[] = array();
        foreach ($class_types as $key => $row) {
            $argh[] = array(
                'ctype' => "$key",
                'class_type_name' => $row
            );
        }
        $jsonresult = '{"total":"' . count($argh) . '","results":' . json_encode($argh) . '}';
        Yii::app()->end($jsonresult);
    }
    public function actionLabaRugi()
    {
//        $arr = array();
        switch ($_POST['tipe']) {
            case 'A':
                $arr = $this->generate_chart(array(CL_CURRENT_ASSETS, CL_FIXED_ASSETS));
                break;
            case 'P':
                $arr = $this->generate_chart(array(CL_CURRENT_LIABILITIES, CL_LONGTERM_LIABILITIES, CL_EQUITY));
                break;
            default:
                $arr = $this->generate_chart(array(CL_INCOME, CL_COGS, CL_EXPENSE, CL_OTHER_INCOME));
                break;
        }
//        $arr = $this->generate_chart(array(CL_INCOME, CL_COGS, CL_EXPENSE, CL_OTHER_INCOME));
        Yii::app()->end(json_encode($arr));
    }
    private function generate_chart($class_type_arr = array())
    {
        $arr = array();
        foreach ($class_type_arr as $class) {
            $chart = ChartClass::model()->findByAttributes(array('ctype' => $class));
            $criteria = new CDbCriteria();
            $criteria->addCondition('class_id = :class_id');
            $criteria->addCondition('LENGTH(parent) = 0');
            $criteria->order = 'seq';
            $criteria->params = array(':class_id' => $chart->cid);
            $chartType = ChartTypes::model()->findAll($criteria);
            $arrChild = array();
            foreach ($chartType as $key => $item) {
                $arrChild[] = array(
                    'id' => $item->id,
                    'account_code' => '',
                    'account_name' => $item->name,
                    'node_type' => 'T',
                    'node_name' => 'Chart Types',
                    'hide' => $item->hide,
                    'parent' => $item->parent,
                    'class_id' => $item->class_id,
                    'seq' => $item->seq,
                    'children' => ChartTypes::get_arr_all_child($item->id)
                );
            }
            $arr[] = array(
                'id' => $chart->cid,
                'account_code' => '',
                'account_name' => $chart->class_name,
                'node_type' => 'C',
                'node_name' => 'Chart Class',
                'expanded' => true,
                'children' => $arrChild
            );
        }
        return $arr;
    }
//    public function actionNeracaActiva()
//    {
//        $arr = $this->generate_chart(array(CL_CURRENT_ASSETS, CL_FIXED_ASSETS));
//        Yii::app()->end(json_encode($arr));
//    }
//    public function actionNeracaPasiva()
//    {
//        $arr = $this->generate_chart(array(CL_CURRENT_LIABILITIES, CL_LONGTERM_LIABILITIES, CL_EQUITY));
//        Yii::app()->end(json_encode($arr));
//    }
}