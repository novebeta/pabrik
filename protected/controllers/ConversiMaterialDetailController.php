<?php
class ConversiMaterialDetailController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('conversi_material_barkas_id = :conversi_material_barkas_id');
        $criteria->params = array(':conversi_material_barkas_id' => $_POST['conversi_material_barkas_id']);
        $model = ConversiMaterialDetail::model()->findAll($criteria);
        $total = ConversiMaterialDetail::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}