<?php
class CustomerPaymentDetilController extends GxController
{
    public function actionCreate()
    {
        $model = new CustomerPaymentDetil;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['CustomerPaymentDetil'][$k] = $v;
            }
            $model->attributes = $_POST['CustomerPaymentDetil'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->customer_payment_detil_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'CustomerPaymentDetil');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['CustomerPaymentDetil'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['CustomerPaymentDetil'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->customer_payment_detil_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->customer_payment_detil_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'CustomerPaymentDetil')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['customer_id'])) {
            if ($_POST['customer_id'] != null) {
                $criteria->addCondition('customer_id = :customer_id');
                $criteria->params = array(':customer_id' => $_POST['customer_id']);
            }
            $model = CustOutstanding::model()->findAll($criteria);
            $total = CustOutstanding::model()->count($criteria);
//            $model = CustOutstanding::model()->findAll();
//            $total = CustOutstanding::model()->count();
            $this->renderJson($model, $total);
            Yii::app()->end();
        }
        if (isset($_POST['customer_payment_id'])) {
            $criteria->addCondition('customer_payment_id = :customer_payment_id');
            $param[':customer_payment_id'] = $_POST['customer_payment_id'];
        }
        $criteria->params = $param;
        $model = CustomerPaymentDetil::model()->findAll($criteria);
        $total = CustomerPaymentDetil::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}