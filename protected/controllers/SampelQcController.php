<?php
class SampelQcController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $sampel_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new SampelQc : $this->loadModel($sampel_id, "SampelQc");
                $docref = $model->doc_ref;
                if ($is_new) {
                    if (isset($_POST['doc_ref']) && strlen(trim($_POST['doc_ref'])) > 0) {
                        $docref = $_POST['doc_ref'];
                    } else {
                        /*
                        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => SAMPEL_QC));
                        $docref =  $systype->next_reference;
                        if (preg_match('/^(\D*?)\/\d{6}\/(\d+)/', $docref, $result) == 1) {
                            list($all, $prefix, $number) = $result;
                            $tgl = strtotime($_POST['tgl']);
                            $year_new = date("01my", $tgl);
                            if ($number == '9999999999') {
                                $nextval = '0000000001';
                            } else {
                                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                                $val = intval($number + 1);
                                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
                            }
                            $systype->next_reference = "$prefix/$year_new/$nextval";
                            $docref = $systype->next_reference;
                        }
                        if (!$systype->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                                CHtml::errorSummary($systype));
                        }*/
                        $docref = Reference::createDocumentReference(SAMPEL_QC, $_POST['tgl'], "dmy");
                    }
                } else {
                    if (!$model->isNewRecord && $model->adt != 0) {
                        throw new Exception("Data Sampel QC tidak bisa diedit karena sudah di audit.");
                    }
                    StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                        array(':type_no' => SAMPEL_QC, ':trans_no' => $sampel_id));
                    SampelQcDetail::model()->deleteAll("sampel_id = :sampel_id",
                        array(':sampel_id' => $sampel_id));
                }
                $model->doc_ref_inv = str_replace('QC', 'INV', $docref);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['SampelQc'][$k] = $v;
                }
                if ($is_new) {
                    $query = SampelQc::model()->dbConnection->createCommand("SELECT UUID();");
                    $uuid = $query->queryScalar();
                    $model->sampel_id = $uuid;
                }
                $_POST['SampelQc']['doc_ref'] = $docref;
                $model->attributes = $_POST['SampelQc'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sampel QC')) . CHtml::errorSummary($model));
                }
                $total = $total_tax = 0;
                foreach ($detils as $detil) {
                    $item_details = new SampelQcDetail();
                    $query = SampelQcDetail::model()->dbConnection->createCommand("SELECT UUID();");
                    $uuid = $query->queryScalar();
                    $item_details->sampel_detail_id = $uuid;
                    $_POST['SampelQcDetail']['barang_id'] = $detil['barang_id'];
                    $_POST['SampelQcDetail']['batch'] = $detil['batch'];
                    $_POST['SampelQcDetail']['tgl_exp'] = $detil['tgl_exp'];
                    $_POST['SampelQcDetail']['ket'] = $detil['ket'];
                    $_POST['SampelQcDetail']['qty'] = get_number($detil['qty']);
                    $_POST['SampelQcDetail']['sampel_id'] = $model->sampel_id;
                    /*
//                    $price = PriceInt::get_price($_POST['SampelQcDetail']['barang_id'], $model->customer->wil_int_id);
//                    if ($price == null) {
//                        throw new Exception("Harga internal belum dibuat");
//                    }
//                    $_POST['SampelQcDetail']['price'] = $price->amount;
//                    $_POST['SampelQcDetail']['total'] = $price->amount * $_POST['SampelQcDetail']['qty'];
//                    $total += $_POST['SampelQcDetail']['total'];
//                    $price_tax = PriceTax::get_price($_POST['SampelQcDetail']['barang_id'], $model->customer->wil_tax_id);
//                    if ($price_tax == null) {
//                        throw new Exception("Harga pajak belum dibuat");
//                    }
//                    $_POST['SampelQcDetail']['price_tax'] = $price_tax->amount;
//                    $_POST['SampelQcDetail']['total_tax'] = $price_tax->amount * $_POST['SampelQcDetail']['qty'];
//                    $total_tax += $_POST['SampelQcDetail']['total_tax'];
                    */
                    $item_details->attributes = $_POST['SampelQcDetail'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Sampel QC detil')) . CHtml::errorSummary($item_details));
                    }
                    U::add_stock_moves(SAMPEL_QC, $model->sampel_id, $model->tgl,
                        $item_details->barang_id, $item_details->batch, $item_details->tgl_exp,
                        -$item_details->qty, $model->doc_ref, 0);
                }
                $model->total = $total;
                $model->total_tax = $total_tax;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sampel QC')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('tgl = :tgl');
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = SampelQc::model()->findAll($criteria);
        $total = SampelQc::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}