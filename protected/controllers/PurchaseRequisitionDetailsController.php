<?php
class PurchaseRequisitionDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('pr_id = :pr_id');
        $criteria->params = array(':pr_id' => $_POST['pr_id']);
        $model = PurchaseRequisitionDetails::model()->findAll($criteria);
        $total = PurchaseRequisitionDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionGetMaterialForPO()
    {
        $this->renderJsonArr(PurchaseRequisitionDetails::get_material_create_PO($_POST['pr_id']));
    }
    public function actionGetItemsForPO()
    {
        $this->renderJsonArr(PurchaseRequisitionDetails::get_items_create_PO($_POST['pr_id']));
    }
}