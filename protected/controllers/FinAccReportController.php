<?php
class FinAccReportController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            if ($is_new && FinAccReport::isExist($_POST['start_date'], $_POST['end_date'])) {
                echo CJSON::encode(array('success' => false, 'msg' => 'Laporan untuk tanggal <b>' . $_POST['start_date'] . '</b> sampai <b>' . $_POST['end_date'] . '</b> telah dibuat.'));
                Yii::app()->end();
            }
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                //Create/Edit Data Laporan
                $model = $is_new ? new FinAccReport : $this->loadModel($_POST['id_fin_acc_report'], "FinAccReport");
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['FinAccReport'][$k] = $v;
                }
                $model->attributes = $_POST['FinAccReport'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung HPP')) . CHtml::errorSummary($model));
                //Mencari fin_acc_report_id sebelumnya
                $_POST['fin_acc_report_id_before'] = $model->get_report_id_before();
                //Menghitung HPP material
                if (!$is_new) {
                    FaHppMaterial::model()->deleteAll("fin_acc_report_id = :fin_acc_report_id",
                        array(':fin_acc_report_id' => $model->fin_acc_report_id));
                }
                $materials = FaHppMaterial::get_materials_n_hpp_before();
                foreach ($materials as $m) {
                    $hppMaterial = new FaHppMaterial();
                    $hppMaterial->fin_acc_report_id = $model->fin_acc_report_id;
                    $hppMaterial->material_id = $m['material_id'];
                    $hppMaterial->qty_before = $m['qty_after'];
                    $hppMaterial->hpp_before = $m['hpp_after'];
                    $hppMaterial->hitungHPP($model->start_date, $model->end_date);
                    if (!$hppMaterial->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung HPP Material')) . CHtml::errorSummary($hppMaterial));
                }
                //Menghitung nilai material yang digunakan
                if (!$is_new) {
                    FaStockMaterial::model()->deleteAll("fin_acc_report_id = :fin_acc_report_id",
                        array(':fin_acc_report_id' => $model->fin_acc_report_id));
                }
                $stoks = FaStockMaterial::get_stock_material_before($model->fin_acc_report_id);
                foreach ($stoks as $s) {
                    $stockMaterial = new FaStockMaterial();
                    $stockMaterial->fin_acc_report_id = $model->fin_acc_report_id;
                    $stockMaterial->material_id = $s['material_id'];
                    $stockMaterial->qty_before = $s['qty_before'];
                    $stockMaterial->hpp_before = $s['hpp_before'];
                    $stockMaterial->hpp = $s['hpp'];
                    $stockMaterial->hitungStock($model->start_date, $model->end_date);
                    if (!$stockMaterial->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung Pemakaian Material')) . CHtml::errorSummary($stockMaterial));
                }
                //Menghitung BOP
                $bop_per_pce = 0;
                if (!$is_new) {
                    FaBop::model()->deleteAll("fin_acc_report_id = :fin_acc_report_id",
                        array(':fin_acc_report_id' => $model->fin_acc_report_id));
                }
                $pengiriman = FaBop::get_pengiriman_produksi($model->start_date, $model->end_date);
                $totalKirim = FaBop::get_total_pengiriman_produksi($model->start_date, $model->end_date);
                foreach ($pengiriman as $p) {
                    $bop = new FaBop();
                    $bop->fin_acc_report_id = $model->fin_acc_report_id;
                    $bop->barang_id = $p['barang_id'];
                    $bop->qty = $p['qty'];
                    $bop->hitungBOP(
                        $totalKirim,
                        $model->biaya_tenagakerja_tidak_langsung,
                        $model->biaya_listrik,
                        $model->biaya_penyusutan_mesin
                    );
                    if (!$bop->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung BOP')) . CHtml::errorSummary($bop));
                    if (!$bop_per_pce) $bop_per_pce = (float)$bop->bop;
                }
                //Menghitung nilai BOM untuk HPP BJ dan BDP
                if (!$is_new) {
                    FaBomHppBj::model()->deleteAll("fin_acc_report_id = :fin_acc_report_id",
                        array(':fin_acc_report_id' => $model->fin_acc_report_id));
                }
                $bomHPP = FaBomHppBj::report_laporan_produksi($model->start_date, $model->end_date);
                foreach ($bomHPP as $b) {
                    $modelBomHPP = new FaBomHppBj();
                    $modelBomHPP->fin_acc_report_id = $model->fin_acc_report_id;
                    $modelBomHPP->bom_id = $b['bom_id'];
                    $modelBomHPP->batch_id = $b['batch_id'];
                    $modelBomHPP->batch_size = $b['batch_size'];
                    $modelBomHPP->barang_id = $b['barang_id'];
                    $modelBomHPP->total_bj = $b['total_bj'];
                    $modelBomHPP->hitung_nilai_RM_PM_berdasarkan_BOM();
                    if (!$modelBomHPP->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung Nilai BDP')) . CHtml::errorSummary($modelBomHPP));
                }
                //Menghitung nilai BDP (Barang Dalam Proses)
                if (!$is_new) {
                    FaStockBdp::model()->deleteAll("fin_acc_report_id = :fin_acc_report_id",
                        array(':fin_acc_report_id' => $model->fin_acc_report_id));
                }
                $StokBDP = FaStockBdp::get_BDP_produksi($model->start_date, $model->end_date);
                foreach ($StokBDP as $bdp) {
                    $modelBDP = new FaStockBdp();
                    $modelBDP->fin_acc_report_id = $model->fin_acc_report_id;
                    $modelBDP->barang_id = $bdp['barang_id'];
                    $modelBDP->bom_id = $bdp['bom_id'];
                    $modelBDP->batch_id = $b['batch_id'];
                    $modelBDP->batch_size = $bdp['batch_size'];
                    $modelBDP->no_batch = $bdp['no_batch'];
                    $modelBDP->qty = $bdp['qty'];
                    $modelBDP->set_nilai_RM_PM_berdasarkan_BOM();
                    if (!$modelBDP->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung Nilai BDP')) . CHtml::errorSummary($modelBDP));
                }
                //Menghitung HPP Barang Jadi
                if (!$is_new) {
                    FaHppBarangjadi::model()->deleteAll("fin_acc_report_id = :fin_acc_report_id",
                        array(':fin_acc_report_id' => $model->fin_acc_report_id));
                }
                $hasilProduksi = FaHppBarangjadi::get_total_hasil_produksi($model->start_date, $model->end_date);
                foreach ($hasilProduksi as $hp) {
                    $hpp_bj = new FaHppBarangjadi();
                    $hpp_bj->fin_acc_report_id = $model->fin_acc_report_id;
                    $hpp_bj->barang_id = $hp['barang_id'];
                    $hpp_bj->bom_id = $hp['bom_id'];
                    $hpp_bj->x_produksi = $hp['x_produksi'];
                    $hpp_bj->batch_size = $hp['batch_size'];
                    $hpp_bj->qty = $hp['qty'];
                    $hpp_bj->hitungHPP_BarangJadi($bop_per_pce);
                    if (!$hpp_bj->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung HPP Barang Jadi')) . CHtml::errorSummary($hpp_bj));
                }
                //Menghitung nilai rupiah Barang Jadi di GPJ
                if (!$is_new) {
                    FaStockGpj::model()->deleteAll("fin_acc_report_id = :fin_acc_report_id",
                        array(':fin_acc_report_id' => $model->fin_acc_report_id));
                }
                $barangs = FaStockGpj::get_barangjadi_n_hpp_before();
                foreach ($barangs as $b) {
                    $stockBarang = new FaStockGpj();
                    $stockBarang->fin_acc_report_id = $model->fin_acc_report_id;
                    $stockBarang->barang_id = $b['barang_id'];
                    $stockBarang->qty_before = (float)$b['qty_after'];
                    $stockBarang->hpp_before = (float)$b['hpp_after'];
                    $stockBarang->hitungHPP($model->start_date, $model->end_date);
                    if (!$stockBarang->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung HPP Material')) . CHtml::errorSummary($stockBarang));
                }
                //Hitung HPP
                $model->HitungHPP_produksi();
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Hitung HPP')) . CHtml::errorSummary($model));
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'FinAccReport')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
            if ($_POST['fieldsearch'] == "periode") {
                $criteria->addCondition("'" . $_POST['valuesearch'] . "' BETWEEN start_date AND end_date");
            }
        }
        $criteria->order = 'start_date DESC';
        $model = FinAccReport::model()->findAll($criteria);
        $total = FinAccReport::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}