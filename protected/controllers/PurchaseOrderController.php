<?php
class PurchaseOrderController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $po_id = $_POST['po_id'];
//            $pr_id = $_POST['pr_id'];
            $statusPR = get_number($_POST['status']);
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new PurchaseOrder : $this->loadModel($po_id, "PurchaseOrder");
                $docref = $model->doc_ref;
                //jika edit PO
                if (!$is_new) {
                    if (!$model->isNewRecord && $model->adt != 0) {
                        throw new Exception("Purchase Order tidak bisa diedit karena sudah di audit.");
                    }
                    PurchaseOrderDetails::model()->deleteAll("po_id= :po_id",
                        array(':po_id' => $po_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['PurchaseOrder'][$k] = $v;
                }
                $_POST['PurchaseOrder']['tipe_material_id'] = MAT_RAW_MATERIAL;
                //jika Buat PO baru (dari PR yang berstatus OPEN)
                $model->attributes = $_POST['PurchaseOrder'];
                if ($is_new && ($statusPR == PR_OPEN || $statusPR == PR_PROCESS)) {
                    //jika status PR = OPEN maka dibuat POnya
//                    $docref = Reference::createDocumentReference_PO($_POST['PurchaseOrder']['tipe_material_id'], $_POST['tgl']); //====== Doc.Ref Baru
//                    $_POST['PurchaseOrder']['doc_ref'] = $docref;
                    $model->createDocRef();
                    $docref = $model->doc_ref;
                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
                }
                //menyimpan detail item PO
                foreach ($detils as $detil) {
                    $item_details = new PurchaseOrderDetails;
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) {
                            $v = get_number($v);
                        }
                        $_POST['PurchaseOrderDetails'][$k] = $v;
                    }
                    $_POST['PurchaseOrderDetails']['po_id'] = $model->po_id;
                    $_POST['PurchaseOrderDetails']['po_detail_id'] = NULL;
                    $item_details->attributes = $_POST['PurchaseOrderDetails'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Purchase Order detil')) . CHtml::errorSummary($item_details));
                    }
                }
                //Update status PR jika buat PO baru
//                if ($is_new) {
//                    $modelPR = $this->loadModel($pr_id, "PurchaseRequisition");
//                    $modelPR->setStatusCLOSED(); //jika belum dibuat PO semua, mak status PROCESS
//                    if ($modelPR->status_po < P0_OPEN) $modelPR->status_po = P0_OPEN;  //status PO untuk PR ini adalah P0_OPEN
//                    if (!$modelPR->save()) {
//                        throw new Exception(t('save.model.fail', 'app',
//                                array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
//                    }
//                }
                $transaction->commit();
                $msg = t('save.success', 'app') . "<br>No. PO : " . $docref;
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionCancel($id)
    {
        $msg = "Proses pembatalan PO tidak berhasil";
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var PurchaseOrder $model */
            $model = $this->loadModel($id, "PurchaseOrder");
            $model->status = PO_CANCELED;
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
            }
            //update status PO untuk PR bersangkutan
            if ($model->pr_id != null) {
                $modelPR = $this->loadModel($model->pr_id, "PurchaseRequisition");
                $modelPR->status_po = PO_CANCELED;
                if (!$modelPR->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
                }
            }
            $transaction->commit();
            $msg = "Proses pembatalan PO berhasil.<br> No. PO : " . $model->doc_ref;
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionClose($id)
    {
        $msg = "Proses closing PO tidak berhasil";
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var PurchaseOrder $model */
            $model = $this->loadModel($id, "PurchaseOrder");
//            $model->status = PO_CLOSED;
            if (!$model->saveAttributes(['status' => PO_CLOSED])) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
            }
            //update status PO untuk PR bersangkutan
            if ($model->pr_id != null) {
                $modelPR = $this->loadModel($model->pr_id, "PurchaseRequisition");
//            $modelPR->status_po = PO_CANCELED;
                if (!$modelPR->saveAttributes(['status_po' => PO_CLOSED])) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
                }
            }
            $transaction->commit();
            $msg = "Proses closing PO berhasil.<br> No. PO : " . $model->doc_ref;
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionRelease($id)
    {
        $msg = "Proses release PO tidak berhasil";
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var PurchaseOrder $model */
            $model = $this->loadModel($id, "PurchaseOrder");
            $model->status = P0_RELEASED;
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
            }
            if ($model->pr_id != null) {
                //update status PO untuk PR bersangkutan
                $modelPR = $this->loadModel($model->pr_id, "PurchaseRequisition");
                $modelPR->status_po = P0_RELEASED;
                if (!$modelPR->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Purchase Order')) . CHtml::errorSummary($model));
                }
            }
            $transaction->commit();
            $msg = "Proses release PO berhasil.<br> No. PO : " . $model->doc_ref;
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionGetAudit()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST['tglfrom']) && isset($_POST['tglto'])) {
            $audit = U::get_list_audit($_POST['tglfrom'], $_POST['tglto']);
            $this->renderJsonArr($audit);
        } else {
            $this->redirect(url('/'));
        }
    }
    public function actionCreateAudit()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        $sjsims = CJSON::decode($_POST['po_id']);
        $total = get_number($_POST['total']);
        $total_input = get_number($_POST['total_input']);
        $persen_tax = $total_input / $total;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($sjsims as $sjsim) {
                $sj = $this->loadModel($sjsim, "PurchaseOrder");
//                $loadsj = new PurchaseOrdersim;
//                $loadsj->persen_tax = $persen_tax;
//                $loadsj->total_input = $total_input;
//                if (!$loadsj->save())
//                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Simulasi')) .
//                        CHtml::errorSummary($loadsj));
//                $sj = $loadsj->sj;
                $sj->adt = 1;
                if (!$sj->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan')) .
                        CHtml::errorSummary($sj));
                }
                self::create_Tax($sj);
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'po';
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
//        if (isset($_POST['tgl'])) {
//            $criteria->addCondition('po.tgl = :tgl');
//            $criteria->params[':tgl'] = $_POST['tgl'];
//        }
        if (isset($_POST['supplier_id'])) {
            //condition untuk menampilkan daftar PO saat penerimaan barang dari supplier
            //status PO yang telah direlease dan belum diclose adalah
            //PO yang dimungkinkan adanya penerimaan barang
            $criteria->addCondition('po.supplier_id = :supplier_id');
            $criteria->addInCondition('po.status', array(P0_RELEASED, PO_PARTIALLY_RECEIVED, PO_RECEIVED));
            $criteria->params[':supplier_id'] = $_POST['supplier_id'];
        }
        if (isset($_POST['status']) && $_POST['status'] != 'all') {
            $criteria->addCondition('po.status = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(po.tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['uid']) && $_POST['uid'] !== 'all') {
            $criteria->join = 'INNER JOIN pbu_purchase_requisition ON pbu_purchase_requisition.pr_id=po.pr_id AND pbu_purchase_requisition.id_user=:uid';
            $criteria->params[':uid'] = $_POST['uid'];
        }
        if (isset($_POST['urole'])) {
            switch ($_POST['urole']) {
                case USER_PPIC:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_PPIC]);
                    break;
                case USER_GA:
                    $criteria->addInCondition('tipe_material_id', $GLOBALS['tipeMaterialRestiction'][USER_GA]);
                    break;
            }
        }
//        if (isset($_POST['viewprice']) && $_POST['viewprice'] == 0) {
//            $criteria->select = "po_id, doc_ref, tdate, tgl, note, tgl_delivery, supplier_id, supplier_cp, term_of_payment, attn_name, 0 total, 0 discount, 0 dpp, 0 tax, 0 grand_total, currency, 0 IDR_rate, tipe_material_id, pr_id, pr_doc_ref, status, adt";
//        }
        $criteria->order = "tgl DESC";
        $model = PurchaseOrder::model()->findAll($criteria);
        $total = PurchaseOrder::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}