<?php
class ReturnPembelianDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset($_POST['po_id'])) {
            $criteria->addCondition('po_id = :po_id');
            $criteria->params = array(':po_id' => $_POST['po_id']);
            $model = PurchaseOrderDetails::model()->findAll($criteria);
            $total = PurchaseOrderDetails::model()->count($criteria);
            $this->renderJson($model, $total);
        }
        if (isset($_POST['return_pembelian_id'])) {
            $criteria->addCondition('return_pembelian_id = :return_pembelian_id');
            $criteria->params = array(':return_pembelian_id' => $_POST['return_pembelian_id']);
            $model = ReturnPembelianDetails::model()->findAll($criteria);
            $total = ReturnPembelianDetails::model()->count($criteria);
            $this->renderJson($model, $total);
        }
    }
}