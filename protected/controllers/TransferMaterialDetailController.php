<?php
class TransferMaterialDetailController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();

        if (isset($_POST['terima_barang_id'])) {
            $criteria->addCondition('transfer_material_id = :transfer_material_id');
            $param[':transfer_material_id'] = $_POST['transfer_material_id'];
        }
        $model = TransferMaterialDetail::model()->findAll($criteria);
        $total = TransferMaterialDetail::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}