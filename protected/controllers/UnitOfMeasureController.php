<?php
class UnitOfMeasureController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['dimension'])) {
            $criteria->addCondition("dimension = :dimension");
            $criteria->params[':dimension'] = $_POST['dimension'];
        }
        $model = UnitOfMeasure::model()->findAll($criteria);
        $total = UnitOfMeasure::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}