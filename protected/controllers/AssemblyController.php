<?php
class AssemblyController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Assembly;
//                $docref = Reference::createDocumentReference(ASSEMBLY, $_POST['tgl'], "dmy");
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Assembly'][$k] = $v;
                }
//                $_POST['Assembly']['doc_ref'] = $docref;
                $model->attributes = $_POST['Assembly'];
                $model->createDocRef();
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Assembly')) .
                        CHtml::errorSummary($model));
                $gl = new GL();
                $total = 0;
                foreach ($detils as $detil) {
                    $AssemblyDetails = new AssemblyDetails;
                    /** @var Material $mat */
                    $mat = Material::model()->findByPk($detil['material_id']);
                    if ($mat == null) {
                        throw new Exception('Material tidak ditemukan.');
                    }
                    $_POST['AssemblyDetails']['qty'] = get_number($detil['qty']);
                    $_POST['AssemblyDetails']['material_id'] = $detil['material_id'];
                    $_POST['AssemblyDetails']['price_std'] = $mat->avg;
                    $_POST['AssemblyDetails']['total_std'] = $mat->avg * $_POST['AssemblyDetails']['qty'];
                    $_POST['AssemblyDetails']['assembly_id'] = $model->assembly_id;
                    $_POST['AssemblyDetails']['loc_code'] = $detil['loc_code'];
                    $AssemblyDetails->attributes = $_POST['AssemblyDetails'];
                    if (!$AssemblyDetails->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detail Cash')) .
                            CHtml::errorSummary($AssemblyDetails));
                    U::add_stock_moves_material(
                        ASSEMBLY,
                        $model->assembly_id,
                        $model->tgl,
                        $AssemblyDetails->material_id,
                        -$AssemblyDetails->qty,
                        null, null, null,
                        $model->doc_ref, //$model->doc_ref,
                        $AssemblyDetails->loc_code
                    );
                    $total += floatval($AssemblyDetails->total_std);
                    //GL Persediaan WIP
                    //          Persediaan bahan baku
//                    $gl = new GL();
                    $gl->add_gl(ASSEMBLY, $model->assembly_id, $model->tgl, $model->doc_ref,
                        $AssemblyDetails->material->tipeMaterial->coa_persediaan,
                        "Assembly $model->doc_ref", "Assembly $model->doc_ref", -$AssemblyDetails->total_std, 0);
                }
                $model->price_total = $total;
                /** @var Material $material */
                $material = Material::model()->findByPk($model->material_id);
                $model->price_std_old = $material->avg;
                $model->price_std_new = round($model->price_total / $model->qty, 2);
                $material->count_price_std($model->qty, $model->price_std_new, $model->tgl, ASSEMBLY, $model->assembly_id);
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Assembly')) .
                        CHtml::errorSummary($model));
                U::add_stock_moves_material(
                    ASSEMBLY,
                    $model->assembly_id,
                    $model->tgl,
                    $model->material_id,
                    $model->qty,
                    null, null, null,
                    $model->doc_ref, //$model->doc_ref,
                    $model->loc_code
                );
                $gl->add_gl(ASSEMBLY, $model->assembly_id, $model->tgl, $model->doc_ref,
                    $model->material->tipeMaterial->coa_persediaan,
                    "Assembly $model->doc_ref", "Assembly $model->doc_ref", $model->price_total, 0);
                $gl->validate();
//                $gl->update_reference(ASSEMBLY, $model->assembly_id, $docref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg,
//                'print' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Assembly');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Assembly'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Assembly'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->assembly_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->assembly_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil divoid.';
            $status = false;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $gl = new GL();
                /** @var Assembly $model */
                $model = $this->loadModel($id, 'Assembly');
                U::add_stock_moves_material(
                    ASSEMBLY_VOID,
                    $model->assembly_id,
                    $model->tgl,
                    $model->material_id,
                    -$model->qty,
                    null, null, null,
                    $model->doc_ref, //$model->doc_ref,
                    $model->loc_code
                );
                $gl->add_gl(ASSEMBLY_VOID, $model->assembly_id, $model->tgl, $model->doc_ref,
                    $model->material->tipeMaterial->coa_persediaan,
                    "Assembly $model->doc_ref", "Assembly $model->doc_ref", -$model->price_total, 0);
                foreach ($model->assemblyDetails as $AssemblyDetails) {
                    U::add_stock_moves_material(
                        ASSEMBLY_VOID,
                        $model->assembly_id,
                        $model->tgl,
                        $AssemblyDetails->material_id,
                        $AssemblyDetails->qty,
                        null, null, null,
                        $model->doc_ref, //$model->doc_ref,
                        $AssemblyDetails->loc_code
                    );
                    $gl->add_gl(ASSEMBLY_VOID, $model->assembly_id, $model->tgl, $model->doc_ref,
                        $AssemblyDetails->material->tipeMaterial->coa_persediaan,
                        "Assembly $model->doc_ref", "Assembly $model->doc_ref", $AssemblyDetails->total_std, 0);
                }
                $gl->validate();
                if (!$model->saveAttributes(['void' => 1])) {
                    throw new Exception('Assembly gagal di void');
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg,
            ));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $params = [];
        if (isset($_POST['folder_id'])) {
            $criteria->addCondition('folder_id = :folder_id');
            $params[':folder_id'] = $_POST['folder_id'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $params;
        $model = Assembly::model()->findAll($criteria);
        $total = Assembly::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}