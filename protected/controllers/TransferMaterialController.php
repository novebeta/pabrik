<?php
class TransferMaterialController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
//            $is_new = $_POST['mode'] == 0;
            $is_new = true;
            $trans_id = $_POST['trans_id'];
            $details = CJSON::decode($_POST['detil']);
//            $storloc_pengirim = $GLOBALS['storLoc'][get_number($_POST['pengirim'])]['name'];
//            $storloc_penerima = $GLOBALS['storLoc'][get_number($_POST['penerima'])]['name'];
//            $storloc_penerima_havestock = $GLOBALS['storLoc'][get_number($_POST['penerima'])]['havestock'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new TransferMaterial : $this->loadModel($trans_id, "TransferMaterial");
                $doc_ref = $model->doc_ref;
                //...jika edit
                if (!$is_new) {
                    TransferMaterialDetail::model()->deleteAll("transfer_material_id = :transfer_material_id",
                        array(':transfer_material_id' => $trans_id));
                    StockMovesMaterial::model()->deleteAll("trans_no = :trans_no",
                        array(':trans_no' => $trans_id));
                }
                $data_header = [];
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $data_header[$k] = $v;
                }
                //...jika buat baru
                if ($is_new) {
                    $storloc_pengirim = 'TRF';
                    $storloc_penerima = 'BRG';
                    $doc_ref = Reference::createDocumentReference_TransferMaterial($_POST['tgl'], $storloc_pengirim, $storloc_penerima); //====== Doc.Ref Baru
                    $data_header['doc_ref'] = $doc_ref;
                }
                $model->attributes = $data_header;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Penerimaan Material')) . CHtml::errorSummary($model));
                foreach ($details as $detil) {
                    $item_details = new TransferMaterialDetail;
                    $data_detail = [];
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $data_detail[$k] = $v;
                    }
                    $data_detail['transfer_material_id'] = $model->transfer_material_id;
                    $item_details->attributes = $data_detail;
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                }
                //.. update status Batch
//                if ($is_new && $model->batch_id) {
//                    $modelBatch = $this->loadModel($model->batch_id, "Batch");
//                    if ($modelBatch->status == BATCH_OPEN) Batch::setStatus(BATCH_PREPARED, $model->batch_id);
//                }
                //.. jika storeloc tidak maintain stock atau transIn, maka auto accept
//                if ($storloc_penerima_havestock == 0 || !$model->trans_out)
//                    $this->acceptTransfer($model->transfer_material_id);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $doc_ref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    private function acceptTransfer($id)
    {
        $model = $this->loadModel($id, "TransferMaterial");
        $model->accepted = 1;
        if (!$model->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Batch')) . CHtml::errorSummary($model));
        }
        $storloc_pengirim = $GLOBALS['storLoc'][$model->pengirim]['name'];
        $storloc_penerima = $GLOBALS['storLoc'][$model->penerima]['name'];
        $detils = TransferMaterialDetail::getitems2($id);
        foreach ($detils as $detil) {
            //catat di stockmoves_material PENGIRIM
            U::add_stock_moves_material(
                SUPPOUT,
                $model->transfer_material_id,
                $model->tgl,
                $detil['material_id'],
                -$detil['qty'],
                $detil['tgl_expired'],
                $detil['no_lot'],
                $detil['no_qc'],
                $model->doc_ref,
                $storloc_pengirim,
                $detil['supplier_id']
            );
            //catat di stockmoves_material PENERIMA
            U::add_stock_moves_material(
                SUPPIN,
                $model->transfer_material_id,
                $model->tgl,
                $detil['material_id'],
                $detil['qty'],
                $detil['tgl_expired'],
                $detil['no_lot'],
                $detil['no_qc'],
                $model->doc_ref,
                $storloc_penerima,
                $detil['supplier_id']
            );
        }
    }
    public function actionGetItems()
    {
        $transfer_material_id = isset($_POST['transfer_material_id']) ? $_POST['transfer_material_id'] : null;
        $arr = TransferMaterialDetail::getitems($transfer_material_id);
        Yii::app()->end(json_encode($arr));
    }
    public function actionAccept($id)
    {
        $msg = "Proses gagal.";
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $this->acceptTransfer($id);
            $transaction->commit();
            $msg = "Transfer barang telah diterima.";
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionGetTransferedItems()
    {
        if (isset($_POST['batch_id'])) {
            $batch_id = isset($_POST['batch_id']) ? $_POST['batch_id'] : null;
            $arr = TransferMaterial::getTransferedItems($batch_id);
            Yii::app()->end(json_encode($arr));
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            $criteria->addCondition('accepted = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
//        if (isset($_POST['urole']) && $_POST['urole'] !== 'all') {
//            $criteria->addCondition('pengirim = :urole OR penerima = :urole');
//            $criteria->params[':urole'] = $_POST['urole'];
//        }
//        if (isset($_POST['uid']) && $_POST['uid'] !== 'all') {
//            $criteria->addCondition('id_user = :uid');
//            $criteria->params[':uid'] = $_POST['uid'];
//        }
        $criteria->order = "tgl DESC";
        $model = TransferMaterial::model()->findAll($criteria);
        $total = TransferMaterial::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}