<?php
class AssemblyDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new AssemblyDetails;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssemblyDetails'][$k] = $v;
            }
            $model->attributes = $_POST['AssemblyDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->assembly_detail_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'AssemblyDetails');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssemblyDetails'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['AssemblyDetails'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->assembly_detail_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->assembly_detail_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'AssemblyDetails')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
        $criteria = new CDbCriteria();
        $param = array();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        if (isset($_POST['assembly_id'])) {
            $criteria->addCondition('assembly_id = :assembly_id');
            $param[':assembly_id'] = $_POST['assembly_id'];
            $criteria->params = $param;
            $model = AssemblyDetails::model()->findAll($criteria);
            $total = AssemblyDetails::model()->count($criteria);
            $this->renderJson($model, $total);
        } else if (isset($_POST['material_id'])) {
            /** @var Bom $bom */
            $bom = Bom::model()->findByAttributes(array(
                'material_id' => $_POST['material_id']
            ));
            $model = [];
            $total = 0;
            if ($bom != null) {
                $qty = get_number($_POST['qty']);
                $f = round($qty / $bom->qty, 2);
                $criteria->addCondition('bom_id = :bom_id');
                $criteria->params = array(':bom_id' => $bom->bom_id);
                $criteria->select = "material_id,(qty*$f) qty,sat,loc_code";
                $model = BomDetail::model()->findAll($criteria);
                $total = BomDetail::model()->count($criteria);
            }
            $this->renderJson($model, $total);
        }
    }
}