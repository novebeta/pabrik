<?php
class SupplierController extends GxController
{
    public function actionCreate()
    {
        $model = new Supplier;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $account_code = $_POST['account_code'];
            $rekening = CJSON::decode($_POST['rekening']);
            $status = true;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
//                if (U::account_in_gl_trans($account_code)) {
//                    $status = false;
//                    $msg = t('coa.fail.use.gl', 'app', array('{coa}' => $account_code));
//                } elseif (U::account_used_supplier($account_code, NULL)) {
//                    $status = false;
//                    $msg = t('coa.fail.use.model', 'app', array('{coa}' => $account_code, '{model}' => 'Supplier'));
//                }
//                if (!$status) {
//                    echo CJSON::encode(array(
//                        'success' => $status,
//                        'msg' => $msg
//                    ));
//                    Yii::app()->end();
//                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Supplier'][$k] = $v;
                }
                $model->attributes = $_POST['Supplier'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Supplier')) . CHtml::errorSummary($model));
                }
                foreach ($rekening as $rek) {
                    $item_rekening = new SupplierBank;
                    $item_rekening->attributes = $rek;
                    $item_rekening->supplier_id = $model->supplier_id;
                    if (!$item_rekening->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Supplier Bank detil')) . CHtml::errorSummary($item_rekening));
                    }
                }
                $transaction->commit();
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_id;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Supplier');
        if (isset($_POST) && !empty($_POST)) {
            $account_code = $_POST['account_code'];
            $status = true;
            $rekening = CJSON::decode($_POST['rekening']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
//                if (U::account_in_gl_trans($account_code)) {
//                    $status = false;
//                    $msg = t('coa.fail.use.gl', 'app', array('{coa}' => $account_code));
//                } elseif (U::account_used_supplier($account_code, $id)) {
//                    $status = false;
//                    $msg = t('coa.fail.use.model', 'app', array('{coa}' => $account_code, '{model}' => 'Supplier'));
//                }
//                if (!$status) {
//                    echo CJSON::encode(array(
//                        'success' => $status,
//                        'msg' => $msg
//                    ));
//                    Yii::app()->end();
//                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Supplier'][$k] = $v;
                }
                $model->attributes = $_POST['Supplier'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Supplier')) . CHtml::errorSummary($model));
                }
                foreach ($rekening as $rek) {
                    if (!isset($rek['supplier_bank_id']) || empty($rek['supplier_bank_id'])) {
                        $item_rekening = new SupplierBank;
                    } else {
                        $item_rekening = $this->loadModel($rek['supplier_bank_id'], 'SupplierBank');
                    }
                    $item_rekening->attributes = $rek;
                    $item_rekening->supplier_id = $model->supplier_id;
                    if (!$item_rekening->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Supplier Bank detil')) . CHtml::errorSummary($item_rekening));
                    }
                }
                $transaction->commit();
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_id;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
            $criteria->addCondition($_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'");
        }
        $model = Supplier::model()->findAll($criteria);
        $total = Supplier::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Supplier')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}