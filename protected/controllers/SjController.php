<?php
class SjController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $sj_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var Sj $model */
                $gl = new GL();
                $model = $is_new ? new Sj : $this->loadModel($sj_id, "Sj");
                $docref = $model->doc_ref;
                //...jika edit
                if (!$is_new) {
                    if (!$model->isNewRecord && $model->adt != 0) {
                        throw new Exception("Surat jalan tidak bisa diedit karena sudah di audit.");
                    }
                    StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                        array(':type_no' => SURAT_JALAN, ':trans_no' => $sj_id));
                    GlTrans::model()->deleteAll("type = :type_no AND type_no = :trans_no",
                        array(':type_no' => SURAT_JALAN, ':trans_no' => $sj_id));
                    SjDetails::model()->deleteAll("sj_id = :sj_id",
                        array(':sj_id' => $sj_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Sj'][$k] = $v;
                }
                $model->attributes = $_POST['Sj'];
//                $model->createDocRef();
                if ($is_new) {
//                    $docref = Reference::createDocumentReference(SURAT_JALAN, $_POST['tgl'], "01my");
//                    $_POST['Sj']['doc_ref'] = $docref;
                    $model->createDocRef();
                    $model->doc_ref_inv = str_replace('SJ', 'INV/FOA', $model->doc_ref);
                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Surat Jalan')) . CHtml::errorSummary($model));
                }
                $total = $tot_bruto = $tot_pot1 = $tot_pot2 = $tot_pot3 = $tot_vatrp = 0;
                foreach ($detils as $detil) {
                    $item_details = new SjDetails;
                    $query = SjDetails::model()->dbConnection->createCommand("SELECT UUID();");
                    $uuid = $query->queryScalar();
                    $item_details->sj_details_id = $uuid;
                    $_POST['SjDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['SjDetails']['loc_code'] = $detil['loc_code'];
//                    $_POST['SjDetails']['tgl_exp'] = $detil['tgl_exp'];
                    $_POST['SjDetails']['ket'] = $detil['ket'];
                    $_POST['SjDetails']['qty'] = get_number($detil['qty']);
                    $_POST['SjDetails']['price'] = get_number($detil['price']);
                    $_POST['SjDetails']['bruto'] = get_number($detil['price']) * get_number($detil['qty']);
                    $tot_bruto += $_POST['SjDetails']['bruto'];
                    $_POST['SjDetails']['disc1'] = get_number($detil['disc1']);
                    $_POST['SjDetails']['pot1'] =
                        round((get_number($detil['disc1']) / 100) * $_POST['SjDetails']['bruto'], 2);
                    $tot_pot1 += $_POST['SjDetails']['pot1'];
                    $_POST['SjDetails']['disc2'] = get_number($detil['disc2']);
                    $_POST['SjDetails']['pot2'] =
                        round((get_number($detil['disc2']) / 100) *
                            ($_POST['SjDetails']['bruto'] - $_POST['SjDetails']['pot1']), 2);
                    $tot_pot2 += $_POST['SjDetails']['pot2'];
                    $_POST['SjDetails']['disc3'] = get_number($detil['disc3']);
                    $_POST['SjDetails']['pot3'] =
                        round((get_number($detil['disc3']) / 100) *
                            ($_POST['SjDetails']['bruto'] - $_POST['SjDetails']['pot1'] - $_POST['SjDetails']['pot2']), 2);
                    $tot_pot3 += $_POST['SjDetails']['pot3'];
                    $_POST['SjDetails']['vat'] = get_number($detil['vat']);
                    $_POST['SjDetails']['vatrp'] = round((get_number($detil['vat']) / 100) *
                        ($_POST['SjDetails']['bruto'] - $_POST['SjDetails']['pot1'] -
                            $_POST['SjDetails']['pot2'] - $_POST['SjDetails']['pot3']), 2);
                    $tot_vatrp += $_POST['SjDetails']['vatrp'];
                    $_POST['SjDetails']['total'] =
                        ($_POST['SjDetails']['bruto'] - $_POST['SjDetails']['pot1'] -
                            $_POST['SjDetails']['pot2'] - $_POST['SjDetails']['pot3']) + $_POST['SjDetails']['vatrp'];
                    $_POST['SjDetails']['sj_id'] = $model->sj_id;
                    $total += $_POST['SjDetails']['total'];
                    $item_details->attributes = $_POST['SjDetails'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Surat Jalan detil')) . CHtml::errorSummary($item_details));
                    }
                    U::add_stock_moves_material(
                        SURAT_JALAN, $model->sj_id, $model->tgl, $item_details->barang_id,
                        -$item_details->qty, null, null, null, $model->doc_ref,
                        $item_details->loc_code, null
                    );
                    //GL Hpp
                    //          persediaan
                    $gl = new GL();
                    if ($item_details->total != 0) {
                        /** @var Material $matHpp */
                        $matHpp = Material::model()->findByPk($item_details->barang_id);
                        $gl->add_gl(SURAT_JALAN, $model->sj_id, $model->tgl, $model->doc_ref,
                            $matHpp->tipeMaterial->coa_hpp,
                            "Sj $model->doc_ref", "",
                            round($matHpp->avg * $item_details->qty, 2), 0);
                        $gl->add_gl(SURAT_JALAN, $model->sj_id, $model->tgl, $model->doc_ref,
                            $matHpp->tipeMaterial->coa_persediaan,
                            "Sj $model->doc_ref", "Sj $model->doc_ref",
                            -(round($matHpp->avg * $item_details->qty, 2)), 0);
                    }
                }
                $count = RemainDeliver::model()->count('sales_id = :sales_id',
                    array(':sales_id' => $model->sales_id));
                /** @var Sales $sales */
                $sales = Sales::model()->findByPk($model->sales_id);
                if ($sales == null) {
                    throw new Exception('Sales tidak ditemukan');
                }
                if ($count == 0) {
                    $sales->received = SJ_DELIVERED;
                } else {
                    $sales->received = SJ_PARTIAL;
                }
                if (!$sales->update()) {
                    throw new Exception(CHtml::errorSummary($sales));
                }
                $model->total = $total;
                $model->tot_vatrp = $tot_vatrp;
                $model->tot_pot1 = $tot_pot1;
                $model->tot_pot2 = $tot_pot2;
                $model->tot_pot3 = $tot_pot3;
                $model->tot_bruto = $tot_bruto;
                if (!$model->update()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Surat Jalan')) . CHtml::errorSummary($model));
                }
                $piutang = $model->customer->getOutStanding('1970-01-01', $model->tgl);
                if ($piutang > $model->customer->limit_) {
                    throw new Exception("Kredit untuk customer ini sudah melebihi limit.");
                }
//                if ($is_new) {
//                    $ref->save(SJ, $model->sj_id, $docref);
//                }
//                $model->create_simulation();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $msg = '';
                try {
                    $transaction->rollback();
                } catch (Exception $e) {
                    $msg = $e->getMessage();
                }
                $status = false;
                $msg = $msg . ' ' . $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate()
    {
//        $id = $_POST['sj_id'];
        $id = $_POST['id'];
        /** @var Sj $model */
//        $model = $this->loadModel($id, 'Sj');
        $model = Sj::model()->findByPk($id);
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Sj'][$k] = $v;
                }
                $msg = "Data gagal disimpan";
                $model->attributes = $_POST['Sj'];
                if ($model->update()) {
                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->sj_id;
                } else {
                    $msg .= " " . CHtml::errorSummary($model);
                    $status = false;
                }
                $criteria = new CDbCriteria();
                $criteria->addCondition('status = :status');
                $criteria->addCondition('sales_id = :sales_id');
                $criteria->params = array(':status' => 0, ':sales_id' => $model->sales_id);
                $total = Sj::model()->count($criteria);
                /** @var Sales $so */
                $so = $this->loadModel($model->sales_id, "Sales");
                if ($total == 0) {
                    $so->status = SJ_INVOICED;
                    if (!$so->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Order')) . CHtml::errorSummary($so));
                    }
                }
//            $criteria = new CDbCriteria();
//            $criteria->addCondition('status = :status');
//            $criteria->addCondition('sales_id = :sales_id');
//            $criteria->params = array(':status' => 0, ':sales_id' => $model->sales_id);
//            $total = Sj::model()->count($criteria);
//            if ($total == 0) {
//                if (!$so->saveAttributes(array('status' => SJ_SUPPLIER_INVOICED))) {
//                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Order')) . CHtml::errorSummary($so));
//                }
//            }
                Apar::addPiutang(PENJUALAN, $model->sj_id, $model->tgl_inv,
                    $so->customer_id, $model->doc_ref_inv, $model->total, $model->sj_id);
                //GL BANK (sesuai cara bayar)
                //        VAT
                //        Diskon Faktur
                $gl = new GL();
                if ($model->total != 0) {
                    $gl->add_gl(PENJUALAN, $model->sales_id, $model->tgl, $model->doc_ref_inv,
                        COA_PIUTANG, "Sales $model->doc_ref_inv", "",
                        $model->total, 0);
                }
                if ($model->tot_bruto != 0) {
                    $gl->add_gl(PENJUALAN, $model->sales_id, $model->tgl, $model->doc_ref_inv,
                        COA_SALES, "Sales $model->doc_ref_inv", "",
                        -$model->tot_bruto, 0);
                }
                if ($model->tot_pot1 != 0 || $model->tot_pot2 != 0 || $model->tot_pot3 != 0) {
                    $gl->add_gl(PENJUALAN, $model->sales_id, $model->tgl, $model->doc_ref_inv,
//                        COA_DISKJUAL, "Sales $model->doc_ref_inv", "",
                        COA_SALES, "Sales $model->doc_ref_inv", "",
                        ($model->tot_pot1 + $model->tot_pot2 + $model->tot_pot3), 0);
                }
                if ($model->tot_vatrp != 0) {
                    $gl->add_gl(PENJUALAN, $model->sales_id, $model->tgl, $model->doc_ref_inv,
                        COA_VAT, "Sales $model->doc_ref_inv", "Sales $model->doc_ref_inv",
                        -$model->tot_vatrp, 0);
                }
                //GL Hpp
                //          persediaan
//            $gl = new GL();
//            if ($model->total != 0) {
//                $gl->add_gl(CUST_INV, $model->sj_id, $model->tgl_inv, $model->doc_ref_inv,
//                    COA_PIUTANG, "Cust Invoice $model->doc_ref_inv", "Cust Invoice $model->doc_ref_inv",
//                    $model->total, 0);
//                $gl->add_gl(CUST_INV, $model->sj_id, $model->tgl_inv, $model->doc_ref_inv,
//                    COA_SALES, "Cust Invoice $model->doc_ref_inv", "Cust Invoice $model->doc_ref_inv",
//                    -($model->total - $model->total_tax), 0);
//                $gl->add_gl(CUST_INV, $model->sj_id, $model->tgl_inv, $model->doc_ref_inv,
//                    COA_VAT, "Cust Invoice $model->doc_ref_inv", "Cust Invoice $model->doc_ref_inv",
//                    -($model->total_tax), 0);
//            }
//            foreach ($model->sjDetails as $item_details) {
//                //GL Hpp
//                //          persediaan
//                if ($item_details->total != 0) {
//                    $gl->add_gl(SURAT_JALAN, $model->sj_id, $model->tgl, $model->doc_ref,
//                        COA_PIUTANG, "Sj $model->doc_ref", "Sj $model->doc_ref",
//                        $item_details->total, 0);
//                    $gl->add_gl(SURAT_JALAN, $model->sj_id, $model->tgl, $model->doc_ref,
//                        $item_details->barang->tipeMaterial->coa_jual,
//                        "Sj $model->doc_ref", "Sj $model->doc_ref",
//                        -($item_details->total), 0);
//                }
//            }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $msg = '';
                try {
                    $transaction->rollback();
                } catch (Exception $e) {
                    $msg = $e->getMessage();
                }
                $status = false;
                $msg = $msg . ' ' . $ex->getMessage();
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sj_id));
            }
        }
    }
    public function actionUpdateTgl()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['id'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var Sj $model */
                $model = Sj::model()->findByPk($id);
                if ($_POST['tgl'] != $model->tgl) {
                    $tgl_old = sql2date($model->tgl, 'MMyy');
                    $tgl_new = sql2date($_POST['tgl'], 'MMyy');
                    $ganti_bulan = $tgl_old != $tgl_new;
                    $model->updateTgl($_POST['tgl'], $ganti_bulan);
                    if ($ganti_bulan) {
                        /** @var Sj[] $sj_ */
                        $sj_ = Sj::model()->findAll("DATE_FORMAT(tgl,'%m%y') = :tgl", [':tgl' => $tgl_old]);
                        foreach ($sj_ as $s) {
                            $s->updateTgl($s->tgl, true);
                        }
                    }
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl_sj,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition('status_sj = :status');
            $param[':status'] = $_POST['status'];
        }
        $criteria->params = $param;
        $model = SlsSjInvHeader::model()->findAll($criteria);
        $total = SlsSjInvHeader::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}