<?php
class ServiceDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('service_id = :service_id');
        $criteria->params = array(':service_id' => $_POST['service_id']);
        $model = ServiceDetails::model()->findAll($criteria);
        $total = ServiceDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}