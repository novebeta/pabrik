<?php
class ConversiMaterialBarkasController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $id = $_POST['conversi_material_barkas_id'];
            $materialdetils = CJSON::decode($_POST['materialdetil']);
            $barkasdetils = CJSON::decode($_POST['barkasdetil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new ConversiMaterialBarkas : $this->loadModel($id, "ConversiMaterialBarkas");
                $doc_ref = $model->doc_ref;
                if ($is_new) {
                    $doc_ref = Reference::createDocumentReference_KonversiSparepartBekas();
                } else {
                    StockMovesMaterial::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                        array(':type_no' => SUPPOUT, ':trans_no' => $id));
                    ConversiMaterialDetail::model()->deleteAll("conversi_material_barkas_id = :conversi_material_barkas_id",
                        array(':conversi_material_barkas_id' => $id));
//                    StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
//                        array(':type_no' => SUPPIN, ':trans_no' => $id));
                    ConversiBarkasDetail::model()->deleteAll("conversi_material_barkas_id = :conversi_material_barkas_id",
                        array(':conversi_material_barkas_id' => $id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['ConversiMaterialBarkas'][$k] = $v;
                }
                $_POST['ConversiMaterialBarkas']['doc_ref'] = $doc_ref;
                $model->attributes = $_POST['ConversiMaterialBarkas'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Tanda Terima Sparepart')) . CHtml::errorSummary($model));
                foreach ($materialdetils as $detil) {
                    $material_details = new ConversiMaterialDetail;
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['ConversiMaterialDetil'][$k] = $v;
                    }
                    $_POST['ConversiMaterialDetil']['conversi_material_barkas_id'] = $model->conversi_material_barkas_id;
                    $material_details->attributes = $_POST['ConversiMaterialDetil'];
                    if (!$material_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Material Item detail')) . CHtml::errorSummary($material_details));
                    $user_role = U::get_user_role();
                    U::add_stock_moves_material(
                        SUPPOUT,
                        $model->conversi_material_barkas_id,
                        $model->tgl,
                        $material_details->material_id,
                        -$material_details->qty,
                        NULL,
                        NULL,
                        NULL,
                        $model->doc_ref,
                        $GLOBALS['storLoc'][$user_role]['name']
                    );
                }
                foreach ($barkasdetils as $detil) {
                    $barkas_details = new ConversiBarkasDetail;
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['ConversiBarkasDetil'][$k] = $v;
                    }
                    $_POST['ConversiBarkasDetil']['conversi_material_barkas_id'] = $model->conversi_material_barkas_id;
                    $barkas_details->attributes = $_POST['ConversiBarkasDetil'];
                    if (!$barkas_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Barang Bekas item detail')) . CHtml::errorSummary($barkas_details));
//                    $user_role = U::get_user_role();
//                    U::add_stock_moves(
//                        SUPPIN,
//                        $model->sampel_id,
//                        $model->tgl,
//                        $item_details->barang_id,
//                        $item_details->batch, $item_details->tgl_exp,
//                        $item_details->qty, $model->doc_ref, 0
//                    );
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $doc_ref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        $criteria->order = "tgl DESC";
        $model = ConversiMaterialBarkas::model()->findAll($criteria);
        $total = ConversiMaterialBarkas::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionGetQtySparepart()
    {
        $material_id = $_POST['material_id'];
        $storloc = isset($_POST['urole_storloc']) ? $GLOBALS['storLoc'][get_number($_POST['urole_storloc'])]['name'] : $GLOBALS['storLoc'][U::get_user_role()]['name'];
        $qty = ConversiMaterialBarkas::get_availableStock_sparepart($material_id, $storloc);
        $qty2 = UnitOfMeasure::get_qtySatuan($qty, $_POST['sat']);
        echo json_encode($qty2);
        Yii::app()->end();
    }
}