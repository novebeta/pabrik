<?php
class ServiceController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $service_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var Service $model */
                $model = $is_new ? new Service : $this->loadModel($service_id, "Service");
                $docref = $model->doc_ref;
                if (!$is_new) {
//                    StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
//                        array(':type_no' => SERVICE_IN, ':trans_no' => $service_id));
//                    GlTrans::model()->deleteAll("type = :type_no AND type_no = :trans_no",
//                        array(':type_no' => SERVICE_IN, ':trans_no' => $service_id));
                    ServiceDetails::model()->deleteAll("service_id = :service_id",
                        array(':service_id' => $service_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Service'][$k] = $v;
                }
                if ($is_new) {
                    $docref = Reference::createDocumentReference(SERVICE_IN, $_POST['tgl'], "01my");
                    $_POST['Service']['doc_ref'] = $docref;
//                    $_POST['Sj']['doc_ref_inv'] = str_replace('SJ', 'INV/PBU', $docref);
                }
                $model->attributes = $_POST['Service'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Surat Jalan')) . CHtml::errorSummary($model));
                }
//                $total = $total_tax = 0;
                foreach ($detils as $detil) {
                    $item_details = new ServiceDetails;
                    $query = ServiceDetails::model()->dbConnection->createCommand("SELECT UUID();");
                    $uuid = $query->queryScalar();
                    $item_details->service_detail_id = $uuid;
                    $_POST['ServiceDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['ServiceDetails']['jml'] = get_number($detil['jml']);
                    $_POST['ServiceDetails']['service_id'] = $model->service_id;
                    $item_details->attributes = $_POST['ServiceDetails'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Service detil')) . CHtml::errorSummary($item_details));
                    }
//                    U::add_stock_moves_material(
//                        SERVICE_IN,
//                        $model->service_id,
//                        $model->tgl,
//                        $item_details->barang_id,
//                        $item_details->jml,
//                        null, null, null,
//                        $model->doc_ref,
//                        $model->loc_code,
//                        null
//                    );
                    //GL Hpp
                    //          persediaan
//                    $gl = new GL();
//                    if ($item_details->total != 0) {
//                        $gl->add_gl(SERVICE_IN, $model->sj_id, $model->tgl, $model->doc_ref,
//                            $item_details->barang->tipeMaterial->coa_hpp,
//                            "Sj $model->doc_ref", "Sj $model->doc_ref",
//                            round($item_details->barang->avg * $item_details->qty, 2), 0);
//                        $gl->add_gl(SERVICE_IN, $model->sj_id, $model->tgl, $model->doc_ref,
//                            $item_details->barang->tipeMaterial->coa_hpp,
//                            "Sj $model->doc_ref", "Sj $model->doc_ref",
//                            -(round($item_details->barang->avg * $item_details->qty, 2)), 0);
//                    }
                }
//                $model->total = $total;
//                $model->total_tax = 0;
//                if (!$model->save()) {
//                    throw new Exception(t('save.model.fail', 'app',
//                            array('{model}' => 'Surat Jalan')) . CHtml::errorSummary($model));
//                }
//                if ($is_new) {
//                    $ref->save(SJ, $model->service_id, $docref);
//                }
//                $model->create_simulation();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Service');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Service'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Service'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->service_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->service_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Service')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Service::model()->findAll($criteria);
        $total = Service::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}