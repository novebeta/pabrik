<?php
class TerimaBarangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $tb_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var TerimaBarang $model */
                $model = $is_new ? new TerimaBarang : $this->loadModel($tb_id, "TerimaBarang");
                if ($is_new) {
                    $model->createDocRef();
//                    $ref = new Reference($_POST['tgl']);
//                    $docref = $ref->get_next_reference(SUPPIN);
//                    $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => SUPPIN));
//                    $doc_ref = $systype->next_reference;
//                    $sjtax->doc_ref_inv = str_replace('SJ', 'INV', $sjtax->doc_ref);
//                    if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $doc_ref, $result) == 1) {
//                        list($all, $prefix, $year, $number) = $result;
//                        $tgl = strtotime($_POST['tgl']);
//                        $year_new = date("dmy", $tgl);
//                        if ($number == '9999999999') {
//                            $nextval = '0000000001';
//                        } else {
//                            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
//                            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
//                            $val = intval($number + 1);
//                            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
//                        }
//                        $systype->next_reference = "$prefix/$year_new/$nextval";
//                        $doc_ref = $systype->next_reference;
//                    }
//                    if (!$systype->save()) {
//                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
//                            CHtml::errorSummary($systype));
//                    }
                } else {
                    StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                        array(':type_no' => SUPPIN, ':trans_no' => $tb_id));
                    TerimaBarangDetails::model()->deleteAll("terima_barang_id = :terima_barang_id",
                        array(':terima_barang_id' => $tb_id));
                }
                $doc_ref = $model->doc_ref;
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['TerimaBarang'][$k] = $v;
                }
                $_POST['TerimaBarang']['arus'] = 0;
                $_POST['TerimaBarang']['doc_ref'] = $doc_ref;
                $model->attributes = $_POST['TerimaBarang'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
                }
                $total_dpp = $total_tax = $total_discount = $grand_total = 0;
                $gl = new GL();
                foreach ($detils as $detil) {
                    $item_details = new TerimaBarangDetails;
                    $_POST['TerimaBarangDetails']['barang_id'] = $detil['barang_id'];
//                    $_POST['TerimaBarangDetails']['batch'] = $detil['batch'];
//                    $_POST['TerimaBarangDetails']['tgl_exp'] = $detil['tgl_exp'];
                    $_POST['TerimaBarangDetails']['qty'] = get_number($detil['qty']);
                    $_POST['TerimaBarangDetails']['price'] = get_number($detil['price']);
//                    $_POST['TerimaBarangDetails']['total'] = $_POST['TerimaBarangDetails']['qty'] * $_POST['TerimaBarangDetails']['price'];
                    $_POST['TerimaBarangDetails']['total'] = get_number($detil['total']);
                    $_POST['TerimaBarangDetails']['disc'] = get_number($detil['disc']);
                    $_POST['TerimaBarangDetails']['disc_rp'] = get_number($detil['disc_rp']);
                    $_POST['TerimaBarangDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['TerimaBarangDetails']['vat'] = get_number($detil['vat']);
                    $_POST['TerimaBarangDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['TerimaBarangDetails']['ket'] = $detil['ket'];
                    $_POST['TerimaBarangDetails']['terima_barang_id'] = $model->terima_barang_id;
                    $item_details->attributes = $_POST['TerimaBarangDetails'];
                    $total_dpp += $_POST['TerimaBarangDetails']['bruto'];
                    $total_discount += $_POST['TerimaBarangDetails']['disc_rp'];
                    $total_tax += $_POST['TerimaBarangDetails']['vatrp'];
                    $grand_total += $_POST['TerimaBarangDetails']['total'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    }
                    $brg = Material::model()->findByPk($item_details->barang_id);
                    if ($brg == null) {
                        throw new Exception("Barang tidak ditemukan!");
                    }
                    $std_cost = $brg->count_biaya_beli($item_details->qty, $item_details->price, $model->tgl,
                        SUPPIN, $model->terima_barang_id);
                    U::add_stock_moves(SUPPIN, $model->terima_barang_id, $model->tgl, $item_details->barang_id, $item_details->qty,
                        $model->doc_ref, $item_details->price, $std_cost, $model->loc_code, $model->po->supplier_id);
                    //GL Persediaan
                    //          Hutang / Kas
                    if ($item_details->total != 0) {
                        $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl, $model->doc_ref,
                            $item_details->barang->tipeMaterial->coa_persediaan,
                            "Purchase $model->doc_ref", "Purchase $model->doc_ref",
                            ($item_details->total), 0);
//                        $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl, $model->doc_ref,
//                            COA_VAT_BELI, "Purchase $model->doc_ref", "Purchase $model->doc_ref",
//                            ($item_details->vatrp), 0);
                        $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl, $model->doc_ref,
                            $model->po->supplier->account_code, "Purchase $model->doc_ref",
                            "Purchase $model->doc_ref",
                            -$item_details->total, 0);
//                        $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl, $model->doc_ref,
//                            COA_HPP, "Purchase $model->doc_ref", "Purchase $model->doc_ref",
//                            -$item_details->total, 0);
                    }
                }
                $model->total = $total_dpp;
                $model->discount = $total_discount;
                $model->tax = $total_tax;
                $model->grand_total = $grand_total;
                /** @var $po PurchaseOrder */
                $po = $this->loadModel($model->po_id, "PurchaseOrder");
                $model->top = $po->term_of_payment;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
                }
//                Apar::addHutang(SUPPIN, $model->terima_barang_id, $model->tgl, $model->po->supplier_id,
//                    $model->doc_ref, $model->grand_total, $model->terima_barang_id);
                $criteria = new CDbCriteria();
                $criteria->addCondition('po_id = :po_id');
                $criteria->params = array(':po_id' => $model->po_id);
                $total = RemainReceive::model()->count($criteria);
                if ($total == 0) {
                    $po->status = PO_RECEIVED;
                    if (!$po->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Purchase Order')) . CHtml::errorSummary($po));
                    }
                }
                $gl->validate();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $id = $_POST['terima_barang_id'];
                /** @var $model TerimaBarang */
                $model = $this->loadModel($id, 'TerimaBarang');
                if ($model == null) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'GRN tidak ditemukan')));
                }
                $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => SUPP_INV));
                $doc_ref = $systype->next_reference;
//                    $sjtax->doc_ref_inv = str_replace('SJ', 'INV', $sjtax->doc_ref);
                if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $doc_ref, $result) == 1) {
                    list($all, $prefix, $year, $number) = $result;
                    $tgl = strtotime($_POST['tgl_inv']);
                    $year_new = date("dmy", $tgl);
                    if ($number == '9999999999') {
                        $nextval = '0000000001';
                    } else {
                        $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                        $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                        $val = intval($number + 1);
                        $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
                    }
                    $systype->next_reference = "$prefix/$year_new/$nextval";
                    $doc_ref = $systype->next_reference;
                }
                if (!$systype->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                        CHtml::errorSummary($systype));
                }
                $model->doc_ref_inv = $doc_ref;
                $model->status = $_POST['status'];
                $model->tgl_inv = $_POST['tgl_inv'];
                $model->no_faktur_tax = $_POST['no_faktur_tax'];
                $model->no_invoice = $_POST['no_invoice'];
                $model->tgl_tempo = $_POST['tgl_tempo'];
                $model->inv_id_user = Yii::app()->user->getId();
                $model->inv_tdate = new CDbExpression('NOW()');
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Supplier Invoice')) .
                        CHtml::errorSummary($model));
                }
                $criteria = new CDbCriteria();
                $criteria->addCondition('status = :status');
                $criteria->addCondition('po_id = :po_id');
                $criteria->params = array(':status' => 0, ':po_id' => $model->po_id);
                $total = TerimaBarang::model()->count($criteria);
                /** @var $po PurchaseOrder */
                $po = $this->loadModel($model->po_id, "PurchaseOrder");
                if ($total == 0) {
                    if ($po->status == PO_RECEIVED) {
                        $po->status = PO_INVOICED;
                        if (!$po->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => 'Purchase Order')) . CHtml::errorSummary($po));
                        }
                    }
                }
                Apar::addHutang(SUPPIN, $model->terima_barang_id, $model->tgl_inv, $po->supplier_id,
                    $model->doc_ref_inv, $model->grand_total, $model->terima_barang_id);
//                $gl = new GL();
//                $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl_inv, $model->doc_ref_inv, COA_PERSEDIAAN,
//                    "Supplier Invoice " . $model->doc_ref_inv, "Supplier Invoice " . $model->doc_ref_inv, $model->total, 0);
//                $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl_inv, $model->doc_ref_inv, COA_HUTANG,
//                    "Supplier Invoice " . $model->doc_ref_inv, "Supplier Invoice " . $model->doc_ref_inv, -$model->total, 0);
//                $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl_inv, $model->doc_ref_inv,
//                    COA_P, "Purchase $model->doc_ref_inv", "Purchase $model->doc_ref_inv",
//                    $model->grand_total, 0);
//                $gl->add_gl(SUPPIN, $model->terima_barang_id, $model->tgl_inv, $model->doc_ref_inv,
//                    $model->po->supplier->account_code, "Purchase $model->doc_ref_inv", "Purchase $model->doc_ref_inv",
//                    -$model->grand_total, 0);
//                $gl->validate();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionRevisiInvoice()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $id = $_POST['terima_barang_id'];
                /** @var $model TerimaBarang */
                $model = $this->loadModel($id, 'TerimaBarang');
                if ($model == null) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'GRN tidak ditemukan')));
                }
                //$model->doc_ref_inv = $doc_ref;
                $model->tgl_inv = $_POST['tgl_inv'];
                $model->no_faktur_tax = $_POST['no_faktur_tax'];
                $model->no_invoice = $_POST['no_invoice'];
                $model->tgl_tempo = $_POST['tgl_tempo'];
                //$model->inv_id_user = Yii::app()->user->getId();
                //$model->inv_tdate = new CDbExpression('NOW()');
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Supplier Invoice')) .
                        CHtml::errorSummary($model));
                }
//                $criteria = new CDbCriteria();
//                $criteria->addCondition('status = :status');
//                $criteria->addCondition('po_in_id = :po_in_id');
//                $criteria->params = array(':status' => 0, ':po_in_id' => $model->po_in_id);
//                $total = TerimaBarang::model()->count($criteria);
//                /** @var $po PurchaseOrder */
//                $po = $this->loadModel($model->po_in_id, "PurchaseOrder");
//                if ($total == 0) {
//                    if ($po->status == PO_RECEIVED) {
//                        $po->status = PO_INVOICED;
//                        if (!$po->save()) {
//                            throw new Exception(t('save.model.fail', 'app',
//                                    array('{model}' => 'Purchase Order')) . CHtml::errorSummary($po));
//                        }
//                    }
//                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdateHarga($id)
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (!Yii::app()->request->isPostRequest) {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
//        if (isset($_POST) && !empty($_POST)) {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var $model TerimaBarang */
            $model = $this->loadModel($id, 'TerimaBarang');
            if ($model == null) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'GRN tidak ditemukan')));
            }
            if ($model->status == 1) {
                throw new Exception('Tidak bisa update harga karena invoice sudah terbentuk.');
            }
            StockMoves::model()->deleteAll("type_no = :type_no AND trans_no = :trans_no",
                array(':type_no' => SUPPIN, ':trans_no' => $id));
            $total_price = 0;
            foreach ($model->terimaBarangDetails as $detil) {
                $beli = Beli::model()->find('barang_id = :barang_id', array(':barang_id' => $detil->barang_id));
                $price = 0;
                if ($beli != null) {
                    $price = $beli->price;
                }
                //history detail function!!!!!!!
                $detil->price = $price;
                $detil->total = round($detil->qty * $price, 2);
                $total_price += $detil->total;
                if (!$detil->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Receive item detail')) . CHtml::errorSummary($detil));
                }
                $brg = Material::model()->findByPk($detil->barang_id);
                if ($brg == null) {
                    throw new Exception("Barang tidak ditemukan!");
                }
                $std_cost = $brg->count_biaya_beli($detil->qty, $detil->price, $model->tgl, SUPPIN, $model->terima_barang_id);
                U::add_stock_moves(SUPPIN, $model->terima_barang_id, $model->tgl, $detil->barang_id, $detil->qty,
                    $model->doc_ref, $detil->price, $std_cost, $model->loc_code, $model->po->supplier_id);
            }
            $model->total = $total_price;
            //history master function!!!!!!!
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
//        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['tgl_inv']) && $_POST['tgl_inv'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl_inv,"%m%Y") = DATE_FORMAT(Date(:tgl_inv),"%m%Y")');
            $param[':tgl_inv'] = $_POST['tgl_inv'];
        }
        if (isset($_POST['status'])) {
            $criteria->addCondition('status = :status');
            $param[':status'] = $_POST['status'];
            $criteria->order = "tgl DESC";
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->addCondition('arus = :arus');
        $param[':arus'] = 0;
        if (isset($_POST['status']) && $_POST['status'] != null && $_POST['status'] == 1) {
            $criteria->order = 'tgl_inv DESC, doc_ref DESC';
        } else {
            $criteria->order = 'tgl DESC, doc_ref DESC';
        }
        $criteria->params = $param;
        $model = TerimaBarang::model()->findAll($criteria);
        $total = TerimaBarang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionUpdateHistoryTerimaBarang1($model)
    {
        $model1 = $this->loadModel('TerimaBarangHistory');
        $x['terima_barang_id'] = $model['terima_barang_id'];
        $x['total'] = $model['total'];
        $model1->attribute = $x;
    }
    public function actionUpdateHistoryTerimaBarang2($model)
    {
    }
}