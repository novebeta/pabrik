<?php
class BomDetailController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('"bom_id" = :"bom_id"');
        $criteria->params = array(':"bom_id"' => $_POST['"bom_id"']);
        $model = BomDetail::model()->findAll($criteria);
        $total = BomDetail::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionGetItems()
    {
        $bom_id = isset($_POST['bom_id']) ? $_POST['bom_id'] : null;
        $arr = BomDetail::get_items($bom_id);
        Yii::app()->end(json_encode($arr));
    }
}