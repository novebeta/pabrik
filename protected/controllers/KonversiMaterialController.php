<?php
class KonversiMaterialController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new KonversiMaterial;
//                $docref = Reference::createDocumentReference(KONVERSI_MATERIAL, $_POST['tgl'], "dmy");
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['KonversiMaterial'][$k] = $v;
                }
//                $_POST['KonversiMaterial']['doc_ref'] = $docref;
                $model->attributes = $_POST['KonversiMaterial'];
                $model->createDocRef();
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Konversi Material')) .
                        CHtml::errorSummary($model));
                $gl = new GL();
                foreach ($detils as $detil) {
                    $konversiMaterialDetails = new KonversiMaterialDetails;
                    /** @var Material $dari_mat */
                    $dari_mat = Material::model()->findByPk($detil['dari_material_id']);
                    /** @var Material $ke_mat */
                    $ke_mat = Material::model()->findByPk($detil['ke_material_id']);
                    $_POST['KonversiMaterialDetails']['dari_material_id'] = $detil['dari_material_id'];
                    $_POST['KonversiMaterialDetails']['dari_price_std'] = $dari_mat->avg;
                    $_POST['KonversiMaterialDetails']['dari_qty'] = get_number($detil['dari_qty']);
                    $_POST['KonversiMaterialDetails']['loc_code_old'] = $detil['loc_code_old'];
                    $_POST['KonversiMaterialDetails']['total_std'] =
                        round(floatval($_POST['KonversiMaterialDetails']['dari_qty']) *
                            floatval($_POST['KonversiMaterialDetails']['dari_price_std']), 2);
                    $_POST['KonversiMaterialDetails']['ke_material_id'] = $detil['ke_material_id'];
                    $_POST['KonversiMaterialDetails']['ke_qty'] = get_number($detil['ke_qty']);
                    $_POST['KonversiMaterialDetails']['loc_code_new'] = $detil['loc_code_new'];
                    $_POST['KonversiMaterialDetails']['ke_price_std'] =
                        round(floatval($_POST['KonversiMaterialDetails']['total_std']) /
                            floatval($_POST['KonversiMaterialDetails']['ke_qty']), 2);
                    $_POST['KonversiMaterialDetails']['konversi_material_id'] = $model->konversi_material_id;
                    $konversiMaterialDetails->attributes = $_POST['KonversiMaterialDetails'];
                    if (!$konversiMaterialDetails->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Konversi Material Details')) .
                            CHtml::errorSummary($konversiMaterialDetails));
                    $ke_mat->count_price_std($konversiMaterialDetails->ke_qty, $konversiMaterialDetails->ke_price_std, $model->tgl,
                        KONVERSI_MATERIAL, $model->konversi_material_id);
                    U::add_stock_moves_material(
                        KONVERSI_MATERIAL,
                        $model->konversi_material_id,
                        $model->tgl,
                        $konversiMaterialDetails->dari_material_id,
                        -$konversiMaterialDetails->dari_qty,
                        null, null, null,
                        $model->doc_ref, //$model->doc_ref,
                        $konversiMaterialDetails->loc_code_old
                    );
                    U::add_stock_moves_material(
                        KONVERSI_MATERIAL,
                        $model->konversi_material_id,
                        $model->tgl,
                        $konversiMaterialDetails->ke_material_id,
                        $konversiMaterialDetails->ke_qty,
                        null, null, null,
                        $model->doc_ref, //$model->doc_ref,
                        $konversiMaterialDetails->loc_code_new
                    );
                    //GL Persediaan
                    //          Persediaan
                    $gl->add_gl(KONVERSI_MATERIAL, $model->konversi_material_id, $model->tgl, $model->doc_ref,
                        $konversiMaterialDetails->keMaterial->tipeMaterial->coa_persediaan,
                        "Breakdown $model->doc_ref", "Breakdown $model->doc_ref",
                        $konversiMaterialDetails->total_std, 0);
                    $gl->add_gl(KONVERSI_MATERIAL, $model->konversi_material_id, $model->tgl, $model->doc_ref,
                        $konversiMaterialDetails->dariMaterial->tipeMaterial->coa_persediaan,
                        "Breakdown $model->doc_ref", "Breakdown $model->doc_ref",
                        -($konversiMaterialDetails->total_std), 0);
                }
                $gl->validate();
                $gl->update_reference(KONVERSI_MATERIAL, $model->konversi_material_id, $model->doc_ref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg,
//                'print' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'KonversiMaterial');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['KonversiMaterial'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['KonversiMaterial'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->konversi_material_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->konversi_material_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'KonversiMaterial')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = [];
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->params = $param;
        $model = KonversiMaterial::model()->findAll($criteria);
        $total = KonversiMaterial::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}