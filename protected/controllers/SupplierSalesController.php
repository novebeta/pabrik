<?php
class SupplierSalesController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset($_POST['supplier_id'])) {
            $criteria->addCondition('supplier_id = :supplier_id');
            $criteria->params = array(':supplier_id' => $_POST['supplier_id']);
        }
        $model = SupplierSales::model()->findAll($criteria);
        $total = SupplierSales::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}