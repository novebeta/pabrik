<?php
class BatchDetailController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('"batch_id" = :"batch_id"');
        $criteria->params = array(':"batch_id"' => $_POST['"batch_id"']);
        $model = BatchDetail::model()->findAll($criteria);
        $total = BatchDetail::model()->count($criteria);
        $this->renderJson($model, $total);
    }
//    public function actionGetBOMItems()
//    {
//        $bom_id= isset($_POST['bom_id']) ? $_POST['bom_id'] : null;
//        $arr = BatchDetail::getBOMitems($bom_id);
//        Yii::app()->end(json_encode($arr));
//    }
    public function actionGetItems()
    {
        $batch_id = isset($_POST['batch_id']) ? $_POST['batch_id'] : null;
        $bom_id = isset($_POST['bom_id']) ? $_POST['bom_id'] : null;
        if ($batch_id == NULL) {
            $arr = BatchDetail::getBOMitems($bom_id);
        } else {
            $arr = BatchDetail::getitems($batch_id);
        }
        Yii::app()->end(json_encode($arr));
    }
}