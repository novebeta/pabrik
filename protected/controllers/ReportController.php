<?php
Yii::import('application.components.U');
Yii::import("application.components.tbs_class_php5", true);
Yii::import("application.components.tbs_plugin_excel", true);
Yii::import("application.components.tbs_plugin_opentbs", true);
Yii::import("application.components.tbs_plugin_aggregate", true);
Yii::import('application.components.phpExcelReportHelper', true);
class ReportController extends GxController
{
    public $is_excel;
    private $TBS;
    private $logo;
    private $format;
    public function init()
    {
        parent::init();
//        if (!isset($_POST) || empty($_POST)) {
//            $this->redirect(url('/'));
//        }
        $this->layout = "report";
        $this->logo = bu() . "/images/logo_pbu.png";
        $this->TBS = new clsTinyButStrong;
        if (isset($_POST['format'])) {
            $this->format = $_POST['format'];
        }
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
            $this->is_excel = true;
        } elseif ($this->format == 'openOffice') {
            $this->TBS->PlugIn(TBS_INSTALL, OPENTBS_PLUGIN);
        }
        error_reporting(E_ERROR);
    }
    public function actionPrintSuratJalan()
    {
        $this->layout = 'plain';
        if (isset($_POST) && !empty($_POST)) {
//            $sj = $this->loadModel($_POST['sj_id'], 'Sj');
            // $sj = Sj::model()->findByPk($_POST['sj_id']);
            /** @var Sj $surat_jalan */
            $surat_jalan = Sj::model()->findByAttributes(['sj_id' => $_POST['sj_id']]);
            if ($surat_jalan != null) {
//                if ($this->format == 'excel') {
//                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sj_jual.xml');
//                }
//                $this->TBS->SetOption('noerr', true);
                $items = $surat_jalan->report_surat_jalan();
//                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8', array(279.4,228.6));
                $h = $this->render('surat_jalan', [
                    'surat_jalan' => $surat_jalan,
                    'items' => $items
                ], true);
//                echo $h;
//                $mPDF1->WriteHTML($this->render('PrintInvCust', [
//                    'surat_jalan' => $surat_jalan,
//                    'items' => $items
//                ], true));
//                $mPDF1->Output();

                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => $h
                ));
                Yii::app()->end();


                for ($i = count($items); $i < 11; $i++) { //50
                    $items[] = array(
                        'num' => null,
                        'kode_barang' => '',
                        'nama_barang' => '',
                        'loc_code' => '',
                        'qty' => null,
                        'sat' => ''
                    );
                }
                $columnData = array(
                    array(
                        'A' => '#',
                        'C' => 'nama_barang',
                        'G' => 'qty',
                        'H' => 'sat'
                    )
                );
                $field = array(
                    'F1' => date_format(date_create($surat_jalan->tgl), 'd/m/Y'),
                    'C5' => $surat_jalan->doc_ref,
                    'C6' => $surat_jalan->sales->doc_ref,
                    'D3' => $surat_jalan->sales->customer->nama_customer,
                    'D4' => $surat_jalan->sales->customer->alamat_badan_hukum,
                    'C21' => ''
                );
                $report = new PhpExcelReportHelper();
                $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') .
                    DIRECTORY_SEPARATOR . 'sjjual.xls')
                    ->setActiveSheetIndex(0)
                    ->mergeBlock($items, $columnData, 9, 1)
                    ->mergeField($field)
                    //->protectSheet()
                    ->downloadExcel('SJ ' . $surat_jalan->doc_ref);
//                $this->TBS->MergeField('header', $header_data);
//                $this->TBS->MergeBlock('0', $items);
//                if ($this->format == 'excel') {
//                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$surat_jalan->doc_ref.xls");
//                } else {
//                    $this->TBS->Show();
//                }
            }
        }
    }
    public function actionPrintInvCust()
    {
        $this->layout = 'plain';
        if (isset($_POST) && !empty($_POST)) {
//            $sj = $this->loadModel($_POST['sj_id'], 'Sj');
            // $sj = Sj::model()->findByPk($_POST['sj_id']);
            /** @var Sj $surat_jalan */
            $surat_jalan = Sj::model()->findByAttributes(['sj_id' => $_POST['sj_id']]);
            if ($surat_jalan != null) {
//                if ($this->format == 'excel') {
//                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'inv_jual.xml');
//                }
//                $this->TBS->SetOption('noerr', true);
                $items = $surat_jalan->report_invoice_int();
//                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8', array(279.4,228.6));

                $h = $this->render('sales', [
                    'surat_jalan' => $surat_jalan,
                    'items' => $items
                ], true);
//                echo $h;

//                $mPDF1->WriteHTML($this->render('PrintInvCust', [
//                    'surat_jalan' => $surat_jalan,
//                    'items' => $items
//                ], true));
//                $mPDF1->Output();

                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => $h
                ));
                Yii::app()->end();
                for ($i = count($items); $i < 11; $i++) { //50
                    $items[] = array(
//                        'num' => null,
                        'nama_barang' => '',
                        'qty' => null,
                        'price' => null,
                        'price_jual' => null,
                        'totalpot' => null,
                        'sat' => ''
                    );
                }
                $columnData = array(
                    array(
//                        'A' => '#',
                        'A' => 'nama_barang',
                        'E' => 'qty',
                        'F' => 'sat',
                        'G' => 'price',
                        'H' => 'price_jual',
                        'I' => 'totalpot'
                    )
                );
                $field = [
                    'F1' => date_format(date_create($surat_jalan->tgl_inv), 'd/m/Y'),
                    'C5' => $surat_jalan->doc_ref_inv,
                    'C6' => $surat_jalan->doc_ref,
                    'C23' => '',
                    'D3' => $surat_jalan->sales->customer->nama_customer,
                    'D4' => $surat_jalan->sales->customer->alamat_badan_hukum,
                    'C21' => spellNumber($surat_jalan->total),
                    'I21' => $surat_jalan->tot_bruto,
                    'I22' => ($surat_jalan->tot_pot1 + $surat_jalan->tot_pot2 + $surat_jalan->tot_pot3),
                    'I24' => $surat_jalan->total
                ];
                $report = new PhpExcelReportHelper();
                $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') .
                    DIRECTORY_SEPARATOR . 'fakturjual.xls')
                    ->setActiveSheetIndex(0)
                    ->mergeBlock($items, $columnData, 9, 1)
                    ->mergeField($field)
                    //->protectSheet()
//                    ->downloadExcel('Faktur ' . $surat_jalan->doc_ref_inv);
                    ->downloadPDF('Faktur ');
//                    ->showHTML('TestFaktur');
//                $this->TBS->MergeField('header', $header_data);
//                $this->TBS->MergeBlock('0', $items);
//                if ($this->format == 'excel') {
//                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$surat_jalan->doc_ref_inv.xls");
//                } else {
//                    $this->TBS->Show();
//                }
            }
        }
    }
//    public function actionPrintSuratJalan()
//    {
//        if (Yii::app()->request->isAjaxRequest) {
//            return;
//        }
//        if (isset($_POST) && !empty($_POST)) {
//            $sj = $this->loadModel($_POST['sj_id'], 'Sj');
//            switch ($_POST['type']) {
//                case 'SJ_barang_rongsokan':
//                case 'SJ_expedisi':
//                case 'SJ_produk_lokal':
//                    //            $sj = new Sj;
//                    if ($this->format == 'excel') {
//                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sjtax.xml');
//                    }
//                    $this->TBS->SetOption('noerr', true);
//                    $sjd = $sj->report_surat_jalan();
//                    for ($i = count($sjd); $i < MAX_ITEM_SJ_PRODUK_LOKAL; $i++) { //50
//                        $sjd[] = array(
//                            'kode_barang' => '',
//                            'nama_barang' => '',
//                            'batch' => '',
//                            'qty' => '',
//                            'sat' => '',
//                            'tgl_exp' => '',
//                            'ket' => '',
//                            'price' => ''
//                        );
//                    }
//                    $this->TBS->MergeField('header', array(
//                        'doc_ref' => $sj->doc_ref,
//                        'tgl' => sql2date($sj->tgl),
//                        'nama_customer' => $sj->customer->nama_customer,
//                        'address' => $sj->customer->address
//                    ));
//                    $this->TBS->MergeBlock('0', $sjd);
////                    for($i = count($sjd);$i<=22;$i++){
////                        $sjd[] = array('kode_barang'=>null,'nama_barang'=>null,'batch'=>null,'qty'=>null,
////                            'sat'=>null,'tgl_exp'=>null,'ket'=>null);
////                    }
////                    foreach ($sjd as $key => $row) {
////                        $this->TBS->MergeField("$key", $row);
////                    }
//                    if ($this->format == 'excel') {
//                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SJ $sj->doc_ref.xls");
//                    } else {
//                        $this->TBS->Show();
//                    }
//                    break;
//                case 'SJ_produk_export':
//                    //            $sj = new Sj;
//                    if ($this->format == 'excel') {
//                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sj_malaysia.xml');
//                    }
//                    $this->TBS->SetOption('noerr', true);
//                    $sjd = $sj->report_surat_jalan();
//                    for ($i = count($sjd); $i < MAX_ITEM_SJ_PRODUK_EXPORT; $i++) { //30
//                        $sjd[] = array(
//                            'kode_barang' => '',
//                            'nama_barang' => '',
//                            'batch' => '',
//                            'qty' => '',
//                            'sat' => '',
//                            'tgl_exp' => '',
//                            'ket' => ''
//                        );
//                    }
//                    $this->TBS->MergeField('header', array(
//                        'doc_ref' => $sj->doc_ref,
//                        'tgl' => date_format(date_create($sj->tgl), 'd-M-Y'),
//                        'kode_customer' => $sj->customer->kode_customer,
//                        'nama_customer' => $sj->customer->nama_customer,
//                        'address' => $sj->customer->address
//                    ));
//                    $this->TBS->MergeBlock('0', $sjd);
//                    if ($this->format == 'excel') {
//                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SJ EXPORT $sj->doc_ref.xls");
//                    } else {
//                        $this->TBS->Show();
//                    }
//                    break;
//                case 'INV_produk_lokal':
//                    //            $sj = new Sj;
//                    if ($this->format == 'excel') {
//                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'invtax.xml');
//                    }
//                    $this->TBS->SetOption('noerr', true);
//                    $sjd = $sj->report_invoice_int();
//                    for ($i = count($sjd); $i < MAX_ITEM_SJ_PRODUK_LOKAL; $i++) { //50
//                        $sjd[] = array(
//                            'kode_barang' => '',
//                            'nama_barang' => '',
//                            'batch' => '',
//                            'qty' => '',
//                            'sat' => '',
//                            'tgl_exp' => '',
//                            'ket' => '',
//                            'price' => ''
//                        );
//                    }
//                    $subtotal = $sj->total;
//                    $ppn = $subtotal * 0.1;
//                    //$ppn = 0;
//                    $total = $subtotal + $ppn;
//                    $tgl = strtotime($sj->tgl);
//                    $tgl_jatuh_tempo = strtotime($sj->tgl_jatuh_tempo);
//                    $this->TBS->MergeField('header', array(
//                        'doc_ref' => $sj->doc_ref_inv,
//                        'tgl' => date('d', $tgl) . ' ' . U::num_to_month(date('n', $tgl)) . ' ' . date('Y', $tgl),
//                        'nama_customer' => $sj->customer->nama_customer,
//                        'nama_badanhukum' => $sj->customer->nama_badan_hukum,
//                        'alamat_badanhukum' => $sj->customer->alamat_badan_hukum,
//                        'npwp' => $sj->customer->npwp,
//                        'tgl_cetak' => date('d/m/Y'),
//                        'tgl_jatuh_tempo' => date('d', $tgl_jatuh_tempo) . ' ' . U::num_to_month(date('n', $tgl_jatuh_tempo)) . ' ' . date('Y', $tgl_jatuh_tempo),
//                        'subtotal' => $subtotal,
//                        'dpp' => $subtotal,
//                        'ppn' => $ppn,
//                        'total' => $total,
//                        'no_faktur_pajak' => $sj->no_faktur_pajak
//                    ));
//                    $this->TBS->MergeBlock('0', $sjd);
//                    if ($this->format == 'excel') {
//                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InvoiceInt $sj->doc_ref.xls");
//                    } else {
//                        $this->TBS->Show();
//                    }
//                    break;
//                case 'INV_produk_export':
//                    if ($this->format == 'excel') {
//                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'inv_malaysia.xml');
//                    }
//                    $this->TBS->SetOption('noerr', true);
//                    $sjd = $sj->report_invoice_int();
//                    for ($i = count($sjd); $i < MAX_ITEM_SJ_PRODUK_EXPORT; $i++) { //47
//                        $sjd[] = array(
//                            'kode_barang' => '',
//                            'nama_barang' => '',
//                            'batch' => '',
//                            'qty' => '',
//                            'sat' => '',
//                            'tgl_exp' => '',
//                            'ket' => '',
//                            'price' => '',
//                            'price_jual' => ''
//                        );
//                    }
//                    //$total = $sj->total;
//                    //$ppn = $subtotal * 0.1;
//                    //$ppn = 0;
//                    //$total = $subtotal + $ppn;
//                    //$tgl = strtotime($sj->tgl);
//                    //$tgl_jatuh_tempo = strtotime($sj->tgl_jatuh_tempo);
//                    $this->TBS->MergeField('header', array(
//                        'doc_ref' => $sj->doc_ref_inv,
//                        'tgl' => date_format(date_create($sj->tgl), 'd-M-y'),
//                        'tgl2' => date_format(date_create($sj->tgl), 'd F Y'),
//                        //'nama_customer' => $sj->customer->nama_customer,
//                        //'nama_badanhukum' => $sj->customer->nama_badan_hukum,
//                        //'alamat_badanhukum' => $sj->customer->alamat_badan_hukum,
//                        //'npwp' => $sj->customer->npwp,
//                        //'tgl_cetak' => date('d/m/Y'),
//                        //'tgl_jatuh_tempo' => date('d',$tgl_jatuh_tempo).' '.U::num_to_month(date('n',$tgl_jatuh_tempo)).' '.date('Y',$tgl_jatuh_tempo),
//                        //'subtotal' => $subtotal,
//                        //'dpp' => $subtotal,
//                        //'ppn' => $ppn,
//                        'total' => $sj->total
//                        //'no_faktur_pajak' => $sj->no_faktur_pajak
//                    ));
//                    $this->TBS->MergeBlock('0', $sjd);
//                    if ($this->format == 'excel') {
//                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Commercial Invoice $sj->doc_ref.xls");
//                    } else {
//                        $this->TBS->Show();
//                    }
//                    break;
//                case 'INV_expedisi':
//                    if ($this->format == 'excel') {
//                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'inv_ekspedisi_malaysia.xml');
//                    }
//                    $this->TBS->SetOption('noerr', true);
//                    $sjd = $sj->report_invoice_int();
//                    for ($i = count($sjd); $i < MAX_ITEM_SJ_EXPEDISI; $i++) { //30
//                        $sjd[] = array(
//                            'kode_barang' => '',
//                            'nama_barang' => '',
//                            'batch' => '',
//                            'qty' => '',
//                            'sat' => '',
//                            'tgl_exp' => '',
//                            'ket' => '',
//                            'price' => '',
//                            'price_jual' => ''
//                        );
//                    }
//                    $subtotal = $sj->total;
//                    $ppn = $subtotal * 0.1;
//                    $total = $subtotal + $ppn;
//                    //$tgl = strtotime($sj->tgl);
//                    //$tgl_jatuh_tempo = strtotime($sj->tgl_jatuh_tempo);
//                    $this->TBS->MergeField('header', array(
//                        'doc_ref' => $sj->doc_ref_inv,
//                        'tgl' => date_format(date_create($sj->tgl), 'd F Y'),
//                        'nama_customer' => $sj->customer->nama_customer,
//                        'alamat' => $sj->customer->address,
//                        'npwp' => ($sj->customer->npwp == "" ? "00.000.000.0-000.000" : $sj->customer->npwp),
//                        //'tgl_cetak' => date('d/m/Y'),
//                        'tgl_jatuh_tempo' => date_format(date_create($tgl_jatuh_tempo), 'd F Y'),
//                        'subtotal' => $subtotal,
//                        'dpp' => $subtotal,
//                        'ppn' => $ppn,
//                        'total' => $total,
//                        'no_faktur_pajak' => $sj->no_faktur_pajak
//                    ));
//                    $this->TBS->MergeBlock('0', $sjd);
//                    if ($this->format == 'excel') {
//                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Invoice Biaya Expedisi $sj->doc_ref.xls");
//                    } else {
//                        $this->TBS->Show();
//                    }
//                    break;
//                case 'INV_barang_rongsokan':
//                    if ($this->format == 'excel') {
//                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'inv_barangbekas.xml');
//                    }
//                    $this->TBS->SetOption('noerr', true);
//                    $sjd = $sj->report_invoice_int();
//                    for ($i = count($sjd); $i < MAX_ITEM_SJ_BARANGRONGSOKAN; $i++) { //30
//                        $sjd[] = array(
//                            'kode_barang' => '',
//                            'nama_barang' => '',
//                            'batch' => '',
//                            'qty' => '',
//                            'sat' => '',
//                            'tgl_exp' => '',
//                            'ket' => '',
//                            'price' => '',
//                            'price_jual' => ''
//                        );
//                    }
//                    $subtotal = $sj->total;
//                    $ppn = $subtotal * 0.1;
//                    $total = $subtotal + $ppn;
//                    $this->TBS->MergeField('header', array(
//                        'doc_ref' => $sj->doc_ref_inv,
//                        'tgl' => date_format(date_create($sj->tgl), 'd F Y'),
//                        'nama_customer' => $sj->customer->nama_customer,
//                        'alamat' => $sj->customer->address,
//                        'npwp' => ($sj->customer->npwp == "" ? "00.000.000.0-000.000" : $sj->customer->npwp),
//                        'tgl_jatuh_tempo' => date_format(date_create($tgl_jatuh_tempo), 'd F Y'),
//                        'subtotal' => $subtotal,
//                        'dpp' => $subtotal,
//                        'ppn' => $ppn,
//                        'total' => $total,
//                        'no_faktur_pajak' => $sj->no_faktur_pajak
//                    ));
//                    $this->TBS->MergeBlock('0', $sjd);
//                    if ($this->format == 'excel') {
//                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Invoice BarangRongsokan $sj->doc_ref.xls");
//                    } else {
//                        $this->TBS->Show();
//                    }
//                    break;
//            }
//        }
//    }
    public function actionPrintPurchaseOrder()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            /** @var PurchaseOrder $po */
            $po = $this->loadModel($_POST['po_id'], 'PurchaseOrder');
            /** @var Supplier $sup */
            $sup = $this->loadModel($po->supplier_id, 'Supplier');
            $purchaser = $this->loadModel($po->id_user, 'Users');
            $field = array(
                'J4' => $po->doc_ref,
                'J5' => date_format(date_create($po->tgl), 'F d, Y'),
                'J6' => $po->pr_doc_ref,
                'J9' => $po->attn_name,
                'D9' => $po->supplier_cp,
                'D10' => $sup->supplier_name,
                'D11' => $sup->alamat,
                'D13' => $sup->kota,
                'D14' => $sup->negara,
                'D15' => $sup->telepon,
                'D16' => $sup->fax,
                'D17' => $sup->email,
                'E20' => date_format(date_create($po->tgl_delivery), 'd F Y'),
                'J20' => $po->term_of_payment,
                'I24' => $po->currency,
                'K24' => $po->total,
                'I25' => $po->currency,
                'K25' => $po->discount,
                'I26' => $po->currency,
                'K26' => $po->dpp,
                'I27' => $po->currency,
                'K27' => $po->tax,
                'I28' => $po->currency,
                'K28' => $po->grand_total,
                'A26' => $po->note,
                'J35' => $purchaser->name_,
                'J37' => "Date  : " . date_format(date_create($po->tgl), 'd-m-Y') //date("d-m-Y")
            );
            $pod = $po->report();
            foreach ($pod as $k => $p) {
                $pod[$k]['qty'] = $p['qty'];//UnitOfMeasure::get_qtySatuan($p['qty'], $p['sat']);
                $pod[$k]['total'] = (float)$pod[$k]['qty'] * (float)$p['price'];
            }
            $columnData = array(
                array(
                    'A' => '#',
                    'B' => 'nama_material',
                    'E' => 'kode_material',
                    'F' => 'qty',
                    'G' => 'sat',
                    'H' => 'price',
                    'I' => '=' . $po->currency,
                    'K' => 'total'
                )
            );
            $mergeCol = array('B:D', 'I:J');
            $report = new PhpExcelReportHelper();
            $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'po.xls')
                ->setActiveSheetIndex(0)
                ->mergeField($field)
                ->mergeBlock($pod, $columnData, 23, 1, $mergeCol)
                //->protectSheet()
                ->downloadExcel('Purchase Order ' . $po->doc_ref);
//                    ->downloadPDF('Purchase Order '.$po->doc_ref);
        }
    }
    public function actionPrintPurchaseRequisition()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $pr = $this->loadModel($_POST['pr_id'], 'PurchaseRequisition');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'pr.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $prd = $pr->report();
            $jml = count($prd);
            for ($i = 0; $i < 15; $i++) {
                if ($i < $jml) {
                    $prd[$i]['qty'] = UnitOfMeasure::get_qtySatuan($prd[$i]['qty'], $prd[$i]['sat']);
                } else {
                    $prd[] = array(
                        'nama_material' => '',
                        'qty' => '',
                        'sat' => '',
                        'note' => '',
                        'vendor_name' => '',
                        'tgl_required' => ''
                    );
                }
            }
            $header_data = array(
                'no_pr' => $pr->doc_ref,
                'tgl' => date_format(date_create($pr->tgl), 'd F Y'),
                'divisi' => ($pr->tipe_material_id == MAT_GENERAL ? 'GA' : 'PPIC')
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('0', $prd);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "PurchaseRequisition $pr->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintSuratJalanSupp()
    {
        if (isset($_POST) && !empty($_POST)) {
            $tb_id = $_POST['terima_barang_id'];
            /** @var TerimaBarang $surat_jalan */
            $surat_jalan = TerimaBarang::model()->findByPk($tb_id);
            if ($surat_jalan != null) {
                if ($this->format == 'excel') {
                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') .
                        DIRECTORY_SEPARATOR . 'sj_supp.xml');
                }
                $this->TBS->SetOption('noerr', true);
                $items = $surat_jalan->getItems();
                for ($i = count($items); $i < 5; $i++) { //50
                    $items[] = array(
                        'num' => null,
                        'kode_material' => '',
                        'nama_material' => '',
                        'loc_code' => '',
                        'qty' => null,
                        'sat' => ''
                    );
                }
                $header_data = array(
                    'tgl' => date_format(date_create($surat_jalan->tgl), 'd / m / Y'),
                    'doc_ref' => $surat_jalan->doc_ref,
                    'doc_ref_po' => $surat_jalan->po->doc_ref,
                    'supplier_name' => $surat_jalan->po->supplier->supplier_name,
                    'alamat' => $surat_jalan->po->supplier->alamat
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('r', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$surat_jalan->doc_ref.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionPrintInvSupp()
    {
        if (isset($_POST) && !empty($_POST)) {
            $tb_id = $_POST['terima_barang_id'];
            /** @var TerimaBarang $surat_jalan */
            $surat_jalan = TerimaBarang::model()->findByPk($tb_id);
            if ($surat_jalan != null) {
                if ($this->format == 'excel') {
                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'pembelian.xml');
                }
                $this->TBS->SetOption('noerr', true);
                $items = $surat_jalan->getItems();
                for ($i = count($items); $i < 5; $i++) { //50
                    $items[] = array(
                        'num' => null,
                        'kode_material' => '',
                        'nama_material' => '',
                        'loc_code' => '',
                        'qty' => null,
                        'sat' => '',
                        'price' => null,
                        'bruto' => null
                    );
                }
                $header_data = array(
                    'tgl' => date_format(date_create($surat_jalan->tgl_inv), 'd / m / Y'),
                    'tgl_sj' => date_format(date_create($surat_jalan->tgl), 'd / m / Y'),
                    'tgl_tempo' => date_format(date_create($surat_jalan->tgl_tempo), 'd / m / Y'),
                    'doc_ref' => $surat_jalan->doc_ref_inv,
                    'doc_ref_sj' => $surat_jalan->doc_ref,
                    'note' => $surat_jalan->po->note,
                    'kode_supp' => $surat_jalan->po->supplier->supplier_code,
                    'supplier_name' => $surat_jalan->po->supplier->supplier_name,
                    'bruto' => $surat_jalan->total,
                    'disc' => $surat_jalan->discount,
                    'tax' => $surat_jalan->tax,
                    'total' => $surat_jalan->grand_total
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('r', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$surat_jalan->doc_ref_inv.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionPrintBreakDown()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['breakdown_id'];
            /** @var Breakdown $brk */
            $brk = Breakdown::model()->findByPk($id);
            if ($brk != null) {
                if ($this->format == 'excel') {
                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') .
                        DIRECTORY_SEPARATOR . 'breakdown.xml');
                }
//                $this->TBS->SetOption('noerr', true);
                $items = $brk->getItems();
                for ($i = count($items); $i < 15; $i++) { //50
                    $items[] = array();
                }
                $header_data = array(
                    'tgl' => date_format(date_create($brk->tgl), 'd / m / Y'),
                    'doc_ref' => $brk->doc_ref,
                    'note_' => $brk->note_,
                    'kode_material' => $brk->material->kode_material,
                    'nama_material' => $brk->material->nama_material,
                    'qty' => $brk->qty,
                    'sat' => $brk->material->sat
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('0', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$brk->doc_ref.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionPrintAssembly()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['assembly_id'];
            /** @var Assembly $brk */
            $brk = Assembly::model()->findByPk($id);
            if ($brk != null) {
                if ($this->format == 'excel') {
                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') .
                        DIRECTORY_SEPARATOR . 'assembly.xml');
                }
//                $this->TBS->SetOption('noerr', true);
                $items = $brk->getItems();
                for ($i = count($items); $i < 15; $i++) { //50
                    $items[] = array();
                }
                $header_data = array(
                    'tgl' => date_format(date_create($brk->tgl), 'd / m / Y'),
                    'doc_ref' => $brk->doc_ref,
                    'note_' => $brk->note_,
                    'kode_material' => $brk->material->kode_material,
                    'nama_material' => $brk->material->nama_material,
                    'qty' => $brk->qty,
                    'sat' => $brk->material->sat
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('0', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$brk->doc_ref.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionReportHistoryPembelian()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $template = 'InventoryMovements';
            $mutasi = U::report_history_pembelian($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovements$from-$to.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionReportGiro()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
//            $from = $_POST['tglfrom'];
//            $to = $_POST['tglto'];
            $template = 'Giro';
            $mutasi = U::report_giro();
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'Giro',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Giro.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider
                ));
            }
        }
    }
    public function actionPrintKas()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['kas_id'];
            /** @var Kas $kas */
            $kas = Kas::model()->findByPk($id);
            if ($kas != null) {
                if ($this->format == 'excel') {
                    if ($kas->arus == -1) {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') .
                            DIRECTORY_SEPARATOR . 'kaskeluar.xml');
                    } else {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') .
                            DIRECTORY_SEPARATOR . 'kasmasuk.xml');
                    }
                }
//                $this->TBS->SetOption('noerr', true);
                $items = $kas->getItems();
                for ($i = count($items); $i < 5; $i++) { //50
                    $items[] = ['item_name' => '', 'total' => '', 'account_code' => ''];
                }
                /** @var Users $u */
                $u = Users::model()->findByPk($kas->user_id);
                $header_data = array(
                    'tgl' => date_format(date_create($kas->tgl), 'd / m / Y'),
                    'doc_ref' => $kas->doc_ref,
                    'user_id' => $u->user_id,
                    'no_kwitansi' => $kas->no_kwitansi,
                    'account_code' => $kas->bank->account_code,
                    'account_name' => $kas->bank->accountCode->account_name,
                    'print_date' => get_date_today("dd/MM/yyyy hh:mm"),
                    'total' => abs($kas->total),
                    'terbilang' => spellNumber(abs($kas->total))
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('0', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$kas->doc_ref.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionPrintCustPay()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['customer_payment_id'];
            /** @var CustomerPayment $custpay */
            $custpay = CustomerPayment::model()->findByPk($id);
            if ($custpay != null) {
                if ($this->format == 'excel') {
                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') .
                        DIRECTORY_SEPARATOR . 'custpay.xml');
                }
//                $this->TBS->SetOption('noerr', true);
                $items = $custpay->getItems();
                for ($i = count($items); $i < 5; $i++) { //50
                    $items[] = ['doc_ref' => '', 'kas_diterima' => '', 'remark' => ''];
                }
                /** @var Users $u */
                $u = Users::model()->findByPk($custpay->id_user);
                $header_data = array(
                    'tgl' => date_format(date_create($custpay->tgl), 'd / m / Y'),
                    'no_kwitansi' => $custpay->doc_ref,
                    'user_id' => $u->user_id,
                    'account_code' => $custpay->bank->account_code,
                    'account_name' => $custpay->bank->accountCode->account_name,
                    'print_date' => get_date_today("dd/MM/yyyy hh:mm"),
                    'total' => abs($custpay->total),
                    'terbilang' => spellNumber(abs($custpay->total))
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('0', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$custpay->doc_ref.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionPembayaranSupp()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['supplier_payment_id'];
            /** @var SupplierPayment $kas */
            $kas = SupplierPayment::model()->findByPk($id);
            if ($kas != null) {
                if ($this->format == 'excel') {
                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') .
                        DIRECTORY_SEPARATOR . 'supp_bayar.xml');
                }
                /** @var GlTrans[] $gl_items */
                $gl_items = GlTrans::model()->findAllByAttributes(
                    [
                        'type' => SUPP_PAYMENT,
                        'type_no' => $kas->supplier_payment_id
                    ], 'amount > 0'
                );
//                $this->TBS->SetOption('noerr', true);
//                $items = $kas->getItems();
                $items = [];
                foreach ($gl_items as $row) {
                    $items[] = ['item_name' => $row['memo_'], 'total' => $row['amount'], 'account_code' => $row['account_code']];
                }
                for ($i = count($items); $i < 5; $i++) { //50
                    $items[] = ['item_name' => '', 'total' => '', 'account_code' => ''];
                }
                /** @var Users $u */
                $u = Users::model()->findByPk($kas->id_user);
                $header_data = array(
                    'tgl' => date_format(date_create($kas->tgl), 'd / m / Y'),
                    'doc_ref' => $kas->doc_ref,
                    'user_id' => $u->user_id,
                    'no_kwitansi' => $kas->no_bukti,
                    'account_code' => $kas->bank->account_code,
                    'account_name' => $kas->bank->accountCode->account_name,
                    'print_date' => get_date_today("dd/MM/yyyy hh:mm"),
                    'total' => abs($kas->total),
                    'terbilang' => spellNumber(abs($kas->total))
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('0', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$kas->doc_ref.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionPrintTerimaMaterial()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $tb_id = $_POST['id'];
            $surat_jalan = TerimaMaterial::model()->findByPk($tb_id);
            if ($surat_jalan != null) {
                if ($this->format == 'excel') {
                    $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sj_supp.xml');
                }
                $this->TBS->SetOption('noerr', true);
                $items = $surat_jalan->getItems();
                for ($i = count($items); $i < 15; $i++) { //50
                    $items[] = array();
                }
                $header_data = array(
                    'tgl' => date_format(date_create($surat_jalan->tgl), 'd / m / Y'),
                    'doc_ref' => $surat_jalan->doc_ref,
                    'nama_customer' => $surat_jalan->customer->nama_customer,
                    'address' => $surat_jalan->customer->address
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('item', $items);
                if ($this->format == 'excel') {
                    $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "FPT $fpt->doc_ref.xls");
                } else {
                    $this->TBS->Show();
                }
            }
        }
    }
    public function actionPrintFPT()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $fpt = $this->loadModel($_POST['pembayaran_supplier_id'], 'PembayaranSupplier');
            $sup = $this->loadModel($fpt->supplier_id, 'Supplier');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'fpt.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $items = $fpt->getItems();
            for ($i = count($items); $i < 5; $i++) { //50
                $items[] = array(
                    'dpp' => 0,
                    'ppn' => 0,
                    'materai' => 0,
                    'total' => 0,
                    'pph' => 0,
                    'total_bayar' => 0,
                    'no_inv_supplier' => '',
                    'tgl_inv_supplier' => ''
                );
            }
            $header_data = array(
                'tgl' => date_format(date_create($fpt->tgl), 'd / m / Y'),
                'doc_ref' => $fpt->doc_ref,
                'nama_rekening' => $fpt->nama_rekening,
                'no_rekening' => $fpt->no_rekening,
                'bank' => $fpt->bank,
                'supp_nama_rekening' => $fpt->supp_nama_rekening,
                'supp_no_rekening' => $fpt->supp_no_rekening,
                'supp_bank' => $fpt->supp_bank,
                'supp_alamat_bank' => $fpt->supp_alamat_bank,
                'supp_rekening_currency' => $fpt->supp_rekening_currency,
                'tgl_jatuh_tempo' => date_format(date_create($fpt->tgl_jatuh_tempo), 'd F Y'),
                'tujuan_pembayaran' => $fpt->tujuan_pembayaran,
                'cara_pembayaran' => $fpt->cara_pembayaran,
                'dpp' => $fpt->dpp,
                'ppn' => $fpt->ppn,
                'materai' => $fpt->materai,
                'total' => $fpt->total,
                'pph' => $fpt->pph,
                'total_bayar' => $fpt->total_bayar,
                'total_bayar_terbilang' => spellNumber($fpt->total_bayar)
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $items);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "FPT $fpt->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintBatch()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST['batch_id']) && !empty($_POST['batch_id'])) {
            $b = $this->loadModel($_POST['batch_id'], 'Batch');
            $bom = $this->loadModel($b->bom_id, 'Bom');
            $brg = $this->loadModel($bom->barang_id, 'Barang');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'batch.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $bd_rm = $b->get_prepared_items(MAT_RAW_MATERIAL);
            $bd_pm = $b->get_prepared_items(MAT_PACKAGING);
            foreach ($bd_rm as $k => $p) {
                $bd_rm[$k]['qty'] = UnitOfMeasure::get_qtySatuan($p['qty'], $p['sat']);
            }
            foreach ($bd_pm as $k => $p) {
                $bd_pm[$k]['qty'] = UnitOfMeasure::get_qtySatuan($p['qty'], $p['sat']);
            }
            $total_rm = 0;
            for ($i = 0; $i < count($bd_rm); $i++) {
                $total_rm += $bd_rm[$i]['qty'];
            }
            $header_data = array(
                'no_bom' => $bom->no_bom,
                'tgl_bom' => date_format(date_create($bom->tgl), 'd-F-Y'),
                'tgl_produksi' => '',
                'bentuk_sediaan' => $bom->bentuk_sediaan,
                'qty' => UnitOfMeasure::get_qtySatuan($b->qty, $b->sat),
                'sat' => $b->sat,
                'kode_produk' => $brg->kode_barang_rnd,
                'nama_produk' => $brg->nama_barang,
                'kode_formula' => $bom->kode_formula,
                'no_batch' => $b->no_batch,
                'total_rm' => $total_rm
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('rm', $bd_rm);
            $this->TBS->MergeBlock('pm', $bd_pm);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "$brg->kode_barang no_batch $b->no_batch.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintTransferMaterial()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST['transfer_material_id']) && !empty($_POST['transfer_material_id'])) {
            $t = $this->loadModel($_POST['transfer_material_id'], 'TransferMaterial');
            if ($t->batch_id) $b = $this->loadModel($t->batch_id, 'Batch');
            $template = ($t->batch_id && $t->pengirim == USER_PPIC) ? 'tandaterima2prepare.xml' : 'tandaterima2.xml';
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . $template);
            }
            $this->TBS->SetOption('noerr', true);
            $td = $t->report();
            foreach ($td as $k => $p) {
                $td[$k]['qty'] = UnitOfMeasure::get_qtySatuan($p['qty'], $p['sat']);
            }
            $header = array(
                'doc_ref' => $t->doc_ref,
                'no_batch' => (!$t->batch_id) ? "" : $b->no_batch,
                'tgl' => date_format(date_create($t->tgl), 'd F Y'),
                'pengirim' => $GLOBALS['storLoc'][$t->pengirim]['name'],
                'penerima' => $GLOBALS['storLoc'][$t->penerima]['name']
            );
            $this->TBS->MergeField('header', $header);
            $this->TBS->MergeBlock('item', $td);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "TANDA TERIMA MATERIAL $t->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintTransferBarang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST['transfer_barang_id']) && !empty($_POST['transfer_barang_id'])) {
            $t = $this->loadModel($_POST['transfer_barang_id'], 'TransferBarang');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tandaterima3.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $td = $t->report();
            $header = array(
                'doc_ref' => $t->doc_ref,
                'tgl' => date_format(date_create($t->tgl), 'd F Y'),
                'pengirim' => $GLOBALS['storLoc'][$t->pengirim]['name'],
                'penerima' => $GLOBALS['storLoc'][$t->penerima]['name']
            );
            $this->TBS->MergeField('header', $header);
            $this->TBS->MergeBlock('item', $td);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "TANDA TERIMA BARANG $t->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintSuratJalanTax()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $sjtax = $this->loadModel($_POST['sjtax_id'], 'Sjtax');
            switch ($_POST['type']) {
                case 'btnPrintSJTax':
                    //            $sj = new Sj;
                    if ($this->format == 'excel') {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sjtax.xml');
                    }
                    $this->TBS->SetOption('noerr', true);
                    $sjd = $sjtax->report_surat_jalan_tax();
                    for ($i = count($sjd); $i < 50; $i++) {
                        $sjd[] = array(
                            'kode_barang' => '',
                            'nama_barang' => '',
                            'batch' => '',
                            'qty' => '',
                            'sat' => '',
                            'tgl_exp' => '',
                            'ket' => '',
                            'price' => ''
                        );
                    }
                    $this->TBS->MergeField('header', array(
                        'doc_ref' => $sjtax->doc_ref,
                        'tgl' => sql2date($sjtax->tgl),
                        'nama_customer' => $sjtax->customer->nama_customer,
                        'address' => $sjtax->customer->address
                    ));
                    $this->TBS->MergeBlock('0', $sjd);
//                    for($i = count($sjd);$i<=22;$i++){
//                        $sjd[] = array('kode_barang'=>null,'nama_barang'=>null,'batch'=>null,'qty'=>null,
//                            'sat'=>null,'tgl_exp'=>null,'ket'=>null);
//                    }
//                    foreach ($sjd as $key => $row) {
//                        $this->TBS->MergeField("$key", $row);
//                    }
                    if ($this->format == 'excel') {
                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SJTAX$sjtax->doc_ref.xls");
                    } else {
                        $this->TBS->Show();
                    }
                    break;
                case 'btnPrintINVTax':
                    //            $sj = new Sj;
                    if ($this->format == 'excel') {
                        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'invtax.xml');
                    }
                    $this->TBS->SetOption('noerr', true);
                    $sjd = $sjtax->report_invoice_tax();
                    for ($i = count($sjd); $i < 50; $i++) {
                        $sjd[] = array(
                            'kode_barang' => '',
                            'nama_barang' => '',
                            'batch' => '',
                            'qty' => '',
                            'sat' => '',
                            'tgl_exp' => '',
                            'ket' => '',
                            'price' => ''
                        );
                    }
                    $subtotal = $sjtax->total;
                    $ppn = $subtotal * 0.1;
                    $total = $subtotal + $ppn;
                    $this->TBS->MergeField('header', array(
                        'doc_ref' => $sjtax->doc_ref_inv,
                        'tgl' => sql2date($sjtax->tgl),
                        'nama_customer' => $sjtax->customer->nama_customer,
                        'address' => $sjtax->customer->address,
                        'tgl_cetak' => date('d/m/Y'),
                        'tgl_jatuh_tempo' => sql2date($sjtax->tgl_jatuh_tempo),
                        'subtotal' => $subtotal,
                        'dpp' => $subtotal,
                        'ppn' => $ppn,
                        'total' => $total,
                        'npwp' => $sjtax->customer->npwp
                    ));
                    $this->TBS->MergeBlock('0', $sjd);
                    if ($this->format == 'excel') {
                        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InvoiceTax$sjtax->doc_ref.xls");
                    } else {
                        $this->TBS->Show();
                    }
                    break;
            }
        }
    }
    public function actionInventoryMovements()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $showExp = isset($_POST['show_expired_date']) ? 1 : 0;
            $showNoBatch = isset($_POST['show_no_batch']) ? 1 : 0;
            $storloc = $GLOBALS['storLoc'][get_number($_POST['storloc'])]['name'];
            $template = 'InventoryMovements';
            $mutasi = U::report_mutasi_stok($from, $to, $showExp, $showNoBatch, $storloc);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovements$from-$to.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionInventoryCard()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $batch = $_POST['batch'];
            $tgl_exp = $_POST['tgl_exp'];
            $storloc = $GLOBALS['storLoc'][get_number($_POST['storloc'])]['name'];
            $saldo_awal = StockMoves::get_saldo_item_batch_before($barang_id, $tgl_exp, $batch, $from, $storloc);
            $barang = Barang::model()->findByPk($barang_id);
            $row = U::report_kartu_stok($barang_id, $tgl_exp, $batch, $from, $to, $storloc);
            $stock_card = array();
            foreach ($row as $newrow) {
                $newrow['before'] = $saldo_awal;
                $newrow['after'] += $newrow['before'] + $newrow['qty'];
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($stock_card, array(
                'id' => 'ListPembelian',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryCard$from-$to.xls");
                echo $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'exp' => $tgl_exp !== '' ? formatDatefromString($tgl_exp, 'M Y') : '',
                    'no_batch' => $batch,
                    'from' => formatDatefromString($from, 'd M Y'),
                    'to' => formatDatefromString($to, 'd M Y'),
                    'kode' => $barang->kode_barang,
                    'nama' => $barang->nama_barang
                ), true);
            } else {
                $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'exp' => $tgl_exp !== '' ? formatDatefromString($tgl_exp, 'M Y') : '',
                    'no_batch' => $batch,
                    'from' => formatDatefromString($from, 'd M Y'),
                    'to' => formatDatefromString($to, 'd M Y'),
                    'kode' => $barang->kode_barang,
                    'nama' => $barang->nama_barang
                ));
            }
            /*if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.htm');
            }
            $this->TBS->MergeBlock('header',
                array(
                    array(
                        'logo' => $this->logo,
                        'start' => $from,
                        'to' => $to,
                        'item' => $barang->nama_barang,
                        'batch' => $batch
                    )
                ));
            $this->TBS->MergeBlock('mutasi', $stock_card);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryCard$from-$to.xls");
            } else {
                $this->TBS->Show();
            }*/
        }
    }
    public function actionKartuPersediaanProdukJadi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $batch = $_POST['batch'];
            $storloc = $GLOBALS['storLoc'][get_number($_POST['storloc'])]['name'];
            $saldo_awal = StockMoves::get_saldo_item_batch_before($barang_id, NULL, $batch, $from, $storloc);
            $barang = Barang::model()->findByPk($barang_id);
            $row = U::report_KartuPersediaanProdukJadi($barang_id, NULL, $batch, $from, $to, $storloc);
            $stock_card = array();
            $stock_card[] = array(
                'tgl_masuk' => date_format(date_create($from), "d M Y"),
                'batch_masuk' => "",
                'qty_masuk' => 0,
                'tgl_keluar' => "",
                'batch_keluar' => "",
                'tujuan' => "",
                'qty_keluar' => 0,
                'after' => $saldo_awal
            );
            foreach ($row as $newrow) {
                $newrow['before'] = $saldo_awal;
                $newrow['after'] += $newrow['before'] + $newrow['qty'];
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($stock_card, array(
                'id' => 'KartuStock',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_persedian_produk_jadi.xml');
                $header_data = array(
                    'kode' => $barang->kode_barang,
                    'nama' => $barang->nama_barang
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('data', $stock_card);
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Kartu Persediaan $barang->kode_barang $from-$to.xls");
            } else {
                $this->render('KartuPersediaanProdukJadi', array(
                    'dp' => $dataProvider,
                    'exp' => $tgl_exp !== '' ? formatDatefromString($tgl_exp, 'M Y') : '',
                    'no_batch' => $batch,
                    'from' => formatDatefromString($from, 'd M Y'),
                    'to' => formatDatefromString($to, 'd M Y'),
                    'kode' => $barang->kode_barang,
                    'nama' => $barang->nama_barang
                ));
            }
            /*if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.htm');
            }
            $this->TBS->MergeBlock('header',
                array(
                    array(
                        'logo' => $this->logo,
                        'start' => $from,
                        'to' => $to,
                        'item' => $barang->nama_barang,
                        'batch' => $batch
                    )
                ));
            $this->TBS->MergeBlock('mutasi', $stock_card);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryCard$from-$to.xls");
            } else {
                $this->TBS->Show();
            }*/
        }
    }
    public function actionBarangKeluar()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $batch = $_POST['batch'];
            $saldo_awal = StockMoves::get_saldo_item_batch_before($barang_id, $batch, $from);
            $barang = Barang::model()->findByPk($barang_id);
            $row = U::report_barang_keluar($barang_id, $batch, $from, $to);
            $stock_card = array();
            $stock_card[] = array(
                'nama_customer' => 'SALDO AWAL',
                'awal' => $saldo_awal,
                'out' => '',
                'in' => '',
                'tgl_exp' => '',
                'batch' => '',
                'nama_barang' => '',
                'kode_barang' => '',
                'doc_ref' => '',
                'tgl' => '',
                'akhir' => $saldo_awal
            );
            foreach ($row as $newrow) {
                $newrow['awal'] = $saldo_awal;
                $newrow['akhir'] += $newrow['awal'] + $newrow['qty'];
                $saldo_awal = $newrow['akhir'];
                $stock_card[] = $newrow;
            }
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'barang_keluar.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'barang_keluar.htm');
            }
            $this->TBS->MergeBlock('header',
                array(
                    array(
                        'logo' => $this->logo,
                        'start' => $from,
                        'to' => $to,
                        'item' => $barang->nama_barang,
                        'batch' => $batch
                    )
                ));
            $this->TBS->MergeBlock('mutasi', $stock_card);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BarangKeluar$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionTerimaBarangDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_terima_barang_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'TerimaBarangDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=TerimaBarangDetails$from-$to.xls");
                echo $this->render('TerimaBarangDetails', array(
                    'dp' => $dataProvider,
                    'from' => formatDatefromString($from, 'd M Y'),
                    'to' => formatDatefromString($to, 'd M Y')
                ), true);
            } else {
                $this->render('TerimaBarangDetails', array(
                    'dp' => $dataProvider,
                    'from' => formatDatefromString($from, 'd M Y'),
                    'to' => formatDatefromString($to, 'd M Y')
                ));
            }
        }
    }
    public function actionReturBeliDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_retur_beli_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'ReturBeliDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ReturBeliDetails$from-$to.xls");
                echo $this->render('ReturBeliDetails', array(
                    'dp' => $dataProvider,
                    'from' => formatDatefromString($from, 'd M Y'),
                    'to' => formatDatefromString($to, 'd M Y')
                ), true);
            } else {
                $this->render('ReturBeliDetails', array(
                    'dp' => $dataProvider,
                    'from' => formatDatefromString($from, 'd M Y'),
                    'to' => formatDatefromString($to, 'd M Y')
                ));
            }
        }
    }
    public function actionSJDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_int_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'SJDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SJDetails$from-$to.xls");
                echo $this->render('SJDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('SJDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionINVDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_int_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'INVDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=INVDetails$from-$to.xls");
                echo $this->render('INVDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('INVDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionSJTAXDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_tax_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'SJTAXDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SJTAXDetails$from-$to.xls");
                echo $this->render('SJTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('SJTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionINVTAXDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_tax_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'INVTAXDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=INVTAXDetails$from-$to.xls");
                echo $this->render('INVTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('INVTAXDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionCreditNoteDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_credit_note_details($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'CreditNoteDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=CreditNoteDetails$from-$to.xls");
                echo $this->render('CreditNote', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('CreditNote', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionExportETax()
    {
        Yii::import('ext.ECSVExport');
        $data = array(
            array('key1' => 'value1', 'key2' => 'value2')
        );
        $csv = new ECSVExport($data);
        $output = $csv->toCSV(); // returns string by default
        echo $output;
//        if (Yii::app()->request->isAjaxRequest) {
//            return;
//        }
//        if (isset($_POST) && !empty($_POST)) {
//        }
    }
    public function actionListINV()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_list_invoice($from, $to, $_POST['barang_id']);
            $total_dpp_ppn = array_sum(array_column($mutasi, 'total_dpp_ppn'));
            $total_dpp = array_sum(array_column($mutasi, 'total_dpp'));
            $total_ppn = array_sum(array_column($mutasi, 'total_ppn'));
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'ListINV',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ListINV$from-$to.xls");
                echo $this->render('ListINV', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_dpp_ppn' => $total_dpp_ppn,
                    'total_dpp' => $total_dpp,
                    'total_ppn' => $total_ppn
                ), true);
            } else {
                $this->render('ListINV', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_dpp_ppn' => $total_dpp_ppn,
                    'total_dpp' => $total_dpp,
                    'total_ppn' => $total_ppn
                ));
            }
        }
    }
    public function actionRekapTagihan()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_rekap_tagihan($from, $to);
            $total_pajak = array_sum(array_column($mutasi, 'pajak'));
            $total_dpp = array_sum(array_column($mutasi, 'dpp'));
            $total_ppn = array_sum(array_column($mutasi, 'ppn'));
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'RekapTagihan',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapTagihan$from-$to.xls");
                echo $this->render('RekapTagihan', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_pajak' => $total_pajak,
                    'total_dpp' => $total_dpp,
                    'total_ppn' => $total_ppn
                ), true);
            } else {
                $this->render('RekapTagihan', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_pajak' => $total_pajak,
                    'total_dpp' => $total_dpp,
                    'total_ppn' => $total_ppn
                ));
            }
        }
    }
    public function actionRekapPenjualan()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $customer_id = $_POST['customer_id'];
            $customer_name = 'All Customers';
            $mutasi = U::report_rekap_penjualan($from, $to, $customer_id);
            if ($customer_id != null) {
                /** @var Customers $cust */
                $cust = Customers::model()->findByPk($customer_id);
                if ($cust != null) {
                    $customer_name = $cust->nama_customer;
                }
            }
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'RekapTagihan',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapTagihan$from-$to.xls");
                echo $this->render('RekapTagihan', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'customer_name' => $customer_name
                ), true);
            } else {
                $this->render('RekapTagihan', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'customer_name' => $customer_name
                ));
            }
        }
    }
    public function actionReportRincianPembelian()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'];
            $supplier_name = 'All Supplier';
//            $criteria = new CDbCriteria;
//            $params = [':from' => $from, ':to' => $to];
//            $criteria->addCondition('tgl_inv >= :from');
//            $criteria->addCondition('tgl_inv <= :to');
            if ($supplier_id != null) {
//                $criteria->addCondition('supplier_id = :supplier_id');
//                $params[':supplier_id'] = $supplier_id;
                $supp = Supplier::model()->findByPk($supplier_id);
                if ($supp != null) {
                    $supplier_name = $supp->supplier_name;
                }
            }
//            $criteria->params = $params;
//            $dataProvider = new CActiveDataProvider('PoSjInv', array(
//                'criteria' => $criteria
//            ));
            $mutasi = U::report_rincian_pembelian($from, $to, $supplier_id);
//            $total_pajak = array_sum(array_column($mutasi, 'pajak'));
//            $total_dpp = array_sum(array_column($mutasi, 'dpp'));
//            $total_ppn = array_sum(array_column($mutasi, 'ppn'));
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'RincianPembelian',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapTagihan$from-$to.xls");
                echo $this->render('PembelianDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_name
                ), true);
            } else {
                $this->render('PembelianDetails', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_name
                ));
            }
        }
    }
    public function actionRekapTagihanHutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_rekap_tagihan_hutang($from, $to);
            $total_pajak = array_sum(array_column($mutasi, 'grand_total'));
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'RekapTagihan',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapInvoiceHutang$from-$to.xls");
                echo $this->render('RekapTagihanHutang', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grand_total' => $total_pajak
                ), true);
            } else {
                $this->render('RekapTagihanHutang', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grand_total' => $total_pajak
                ));
            }
        }
    }
//--------------------------- MATERIAL -------------------------------------
    public function actionInventoryMovementsMaterial()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
//            $showExp = isset($_POST['show_expired_date']) ? 1 : 0;
//            $showNoLot = isset($_POST['show_no_lot']) ? 1 : 0;
//            $showNoQc = isset($_POST['show_no_qc']) ? 1 : 0;
//            $showSupplier = isset($_POST['show_supplier']) ? 1 : 0;
            $storloc = $_POST['storloc'];
            $template = 'InventoryMovements_RM_PM';
            $mutasi = U::report_mutasi_stok_Material($from, $to, $storloc);
//            foreach ($mutasi as $key => $row) {
//                $row['before'] = UnitOfMeasure::get_qtySatuan($row['before'], $row['sat']);
//                $row['suppin'] = UnitOfMeasure::get_qtySatuan($row['suppin'], $row['sat']);
//                $row['suppout'] = UnitOfMeasure::get_qtySatuan($row['suppout'], $row['sat']);
//                $row['after'] = UnitOfMeasure::get_qtySatuan($row['after'], $row['sat']);
//                $mutasi[$key] = $row;
//            }
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovements$from-$to.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionInventoryCardMaterial()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $material_id = $_POST['material_id'];
//            $tgl_expired = !$_POST['tgl_expired'] ? NULL : $_POST['tgl_expired'];
//            $no_lot = !$_POST['no_lot'] ? NULL : $_POST['no_lot'];
//            $no_qc = !$_POST['no_qc'] ? NULL : $_POST['no_qc'];
//            $supplier_id = !$_POST['supplier_id'] ? NULL : $_POST['supplier_id'];
            $storloc = $_POST['storloc']; //$GLOBALS['storLoc'][get_number($_POST['storloc'])]['name'];
//            $supplier = Supplier::model()->findByPk($supplier_id);
            $material = Material::model()->findByPk($material_id);
            if ($this->format == 'excel' && ($material->tipe_material_id == MAT_SPAREPART || $material->tipe_material_id == MAT_SPAREPART_BEKAS)) {
                $this->inventoryCardSparepart();
                return;
            }
            $saldo_awal_result = StockMovesMaterial::get_saldo_item_before($material_id, $from, null, null, null, null, $storloc);
            $saldo_awal = $saldo_awal_result;//UnitOfMeasure::get_qtySatuan($saldo_awal_result, $material->sat);
            $row = U::report_kartu_stok_Material($material_id, $from, $to, null, null, null, null, $storloc);
            $stock_card = array();
            foreach ($row as $newrow) {
//                $newrow['qty'] = UnitOfMeasure::get_qtySatuan($newrow['qty'], $newrow['sat']);
//                $newrow['in'] = UnitOfMeasure::get_qtySatuan($newrow['in'], $newrow['sat']);
//                $newrow['out'] = UnitOfMeasure::get_qtySatuan($newrow['out'], $newrow['sat']);
                $newrow['before'] = $saldo_awal;
                $newrow['after'] += $newrow['before'] + $newrow['qty'];
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            if (count($stock_card) == 0) {
                $newrow = array();
                $newrow['sat'] = $material->sat;
                $newrow['qty'] = 0;
                $newrow['in'] = 0;
                $newrow['out'] = 0;
                $newrow['before'] = $saldo_awal;
                $newrow['after'] = $saldo_awal;
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($stock_card, array(
                'id' => 'ListPembelian',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryCardMaterial$from-$to.xls");
                echo $this->render('InventoryCard_Material', array(
                    'dp' => $dataProvider,
                    'exp' => '', //$tgl_expired ? sql2date($tgl_expired, 'dd MMM yyyy') : '',
//                    'no_lot' => $no_lot,
//                    'no_qc' => $no_qc,
                    'supplier_name' => null, //$supplier ? $supplier->supplier_name : NULL,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'kode' => $material->kode_material,
                    'nama' => $material->nama_material
                ), true);
            } else {
                $this->render('InventoryCard_Material', array(
                    'dp' => $dataProvider,
                    'exp' => '',//$tgl_expired ? sql2date($tgl_expired, 'dd MMM yyyy') : '',
//                    'no_lot' => $no_lot,
//                    'no_qc' => $no_qc,
                    'supplier_name' => null, //$supplier ? $supplier->supplier_name : NULL,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'kode' => $material->kode_material,
                    'nama' => $material->nama_material
                ));
            }
        }
    }
    private function inventoryCardSparepart()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $material_id = $_POST['material_id'];
            $supplier_id = !$_POST['supplier_id'] ? NULL : $_POST['supplier_id'];
            $storloc = $GLOBALS['storLoc'][get_number($_POST['storloc'])]['name'];
            $supplier = Supplier::model()->findByPk($supplier_id);
            $material = Material::model()->findByPk($material_id);
            $field = array(
                'D6' => ': ' . $material->nama_material,
                'D8' => ': ' . $material->kode_material,
                'J8' => ' : ' . $supplier->supplier_name
            );
            $saldo_awal_result = StockMovesMaterial::get_saldo_item_before($material_id, $from, NULL, NULL, NULL, $supplier_id, $storloc);
            $saldo_awal = UnitOfMeasure::get_qtySatuan($saldo_awal_result, $material->sat);
            $data = U::report_kartu_stok_Sparepart($material_id, $from, $to, $supplier_id, $storloc);
            foreach ($data as $k => $row) {
                $data[$k]['qty'] = UnitOfMeasure::get_qtySatuan($row['qty'], $row['sat']);
                $data[$k]['in'] = UnitOfMeasure::get_qtySatuan($row['in'], $row['sat']);
                $data[$k]['out'] = UnitOfMeasure::get_qtySatuan($row['out'], $row['sat']);
                $data[$k]['before'] = $saldo_awal;
                $data[$k]['after'] += $saldo_awal + $row['qty'];
                $saldo_awal = $data[$k]['after'];
            }
            $columnData = array(
                array(
                    'A' => '#',
                    'B' => 'tgl_in',
                    'D' => 'in',
                    'E' => 'tgl_out',
                    'F' => 'out',
                    'G' => 'after',
                    'L' => 'petugas',
                    'M' => 'spv'
                )
            );
            $report = new PhpExcelReportHelper();
            $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'formKartuStockSparepart.xls')
                ->setActiveSheetIndex(0)
                ->mergeField($field)
                ->mergeBlock($data, $columnData, 12, 1)
                //->protectSheet()
                ->downloadExcel('Kartu Stock ' . $material->kode_material);
        }
    }
    public function actionStokProduksi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $showBatch = isset($_POST['show_batch']) ? 1 : 0;
            switch ($_POST['jenis_stok']) {
                case '1':
                    $jenisStok = 'Barang';
                    $data = Batch::get_stok_Barang_in_production($showBatch);
                    break;
                case '2':
                    $jenisStok = 'Raw Material';
                    $data = Batch::get_stok_RM_in_production($showBatch);
                    break;
                case '3':
                    $jenisStok = 'Packaging';
                    $data = Batch::get_stok_PM_in_production($showBatch);
                    break;
            }
            $template = 'stok_produksi' . ($showBatch ? '_batch' : '');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . $template . '.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . $template . '.html');
            }
            $this->TBS->MergeBlock('header',
                array(
                    array(
                        'logo' => $this->logo,
                        'date' => date("d-m-Y"),
                        'jenis_stok' => $jenisStok
                    )
                ));
            $this->TBS->MergeBlock('data', $data);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryCard" . date("d-m-Y") . ".xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionGenerateLabaRugi()
    {
        $month = $_POST['month'];
        $year = $_POST['year'];
        $d = new DateTime("$year-$month-01");
        $from = $d->format('Y-m-d');
        $to = $d->format('Y-m-t');
        try {
            $result = self::create_laba_rugi_new($from, $to);
            $amount = $result['header'][0]['laba_bersih'];
            $amount = -$amount;
            $gl = GlTrans::model()->find('type = :type AND tran_date >= :from AND tran_date <= :to',
                array(':type' => LABARUGI, ':from' => $from, ':to' => $to));
            if ($gl == null) {
                $id = $this->generate_uuid();//U::get_next_trans_no_bank_trans(LABARUGI, $store);
                Yii::import('application.components.GL');
                $gl = new GL();
                $gl->add_gl(LABARUGI, $id, $to, null, COA_LABA_RUGI, 'PROFIT LOST', '', $amount, 0);
                $gl->validate();
            } else {
                $gl->amount = $amount;
                if (!$gl->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'GL Trans')) . CHtml::errorSummary($gl));
                }
            }
            $msg = 'Profit Lost succesfully generated.';
            $status = true;
        } catch (Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            )
        );
        Yii::app()->end();
    }
    private function create_laba_rugi_new($from, $to)
    {
        $lr = ChartMaster::get_laba_rugi_new($from, $to);
        $income = ChartTypes::get_chart_types_by_class(CL_INCOME);
        $income_arr = array();
        foreach ($income as $row) {
            $income_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $income_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => -$item['total']
                    );
                    unset($lr[$key]);
                }
            }
            $income_arr = array_merge($income_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_income = array_sum(array_column($income_arr, 'total'));
        $hpp = ChartTypes::get_chart_types_by_class(CL_COGS);
        $hpp_arr = array();
        foreach ($hpp as $row) {
            $hpp_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $hpp_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => $item['total']
                    );
                    unset($lr[$key]);
                }
            }
            $hpp_arr = array_merge($hpp_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_hpp = array_sum(array_column($hpp_arr, 'total'));
//        var_dump($hpp_arr);
        $cost = ChartTypes::get_chart_types_by_class(CL_EXPENSE);
        $cost_arr = array();
        foreach ($cost as $row) {
            $cost_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $cost_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => $item['total']
                    );
                    unset($lr[$key]);
                }
            }
            $cost_arr = array_merge($cost_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_biaya = array_sum(array_column($cost_arr, 'total'));
        $other_income = ChartTypes::get_chart_types_by_class(CL_OTHER_INCOME);
        $other_income_arr = array();
        foreach ($other_income as $row) {
            $other_income_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $other_income_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => $item['total']
                    );
                    unset($lr[$key]);
                }
            }
            $other_income_arr = array_merge($other_income_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_other_income = array_sum(array_column($other_income_arr, 'total'));
        $laba_kotor = $total_income - $total_hpp;
        $total_laba_bersih = $laba_kotor - $total_biaya;
        $laba_bersih_sebelum_pajak = $total_laba_bersih - $total_other_income;
        return array(
            'header' => array(
                array(
                    'from' => $from,
                    'to' => $to,
                    'income' => $total_income,
                    'hpp' => $total_hpp,
                    'laba_kotor' => $laba_kotor,
                    'biaya' => $total_biaya,
                    'laba_bersih' => $total_laba_bersih,
                    'other_income' => $total_other_income,
                    'laba_bersih_sebelum_pajak' => $laba_bersih_sebelum_pajak,
                    'income_label' => 'Income',
                    'hpp_label' => 'HPP'
                )
            ),
            'income' => $income_arr,
            'hpp' => $hpp_arr,
            'biaya' => $cost_arr,
            'other_income' => $other_income_arr
        );
    }
    public function actionGeneralLedger()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $coa = $_POST['account_code'];
//            $store = $_POST['store'];
            $chart_account = ChartMaster::model()->findByPk($coa);
            $result = U::get_general_ledger($coa, $from, $to);
            $begin = U::get_gl_before($coa, $from);
            $begin_arr = array(
                'tgl' => '',
                'memo_' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin
            );
            $gl[] = $begin_arr;
            foreach ($result as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = $newrow['amount'] + $begin;
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'GeneralLedger',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=GeneralLedger$from-$to.xls");
                echo $this->render('GeneralLedger',
                    array(
                        'dp' => $dataProvider,
                        'from' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'account_code' => $chart_account->account_code,
                        'account_name' => $chart_account->account_name
                    ), true);
            } else {
                $this->render('GeneralLedger', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'account_code' => $chart_account->account_code,
                    'account_name' => $chart_account->account_name
                ));
            }
        }
    }
    public function actionGeneralJournal()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $result = U::get_general_journal($from, $to);
            $dataProvider = new CArrayDataProvider($result, array(
                'id' => 'GeneralJournal',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=GeneralJournal$from-$to.xls");
                echo $this->render('GeneralJournal',
                    array(
                        'dp' => $dataProvider,
                        'from' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy')
                    ), true);
            } else {
                $this->render('GeneralJournal', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionLabaRugi()
    {
        $from = $_POST['from_date'];
        $to = $_POST['to_date'];
        $result = self::create_laba_rugi_new($from, $to);
        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'lr.xml');
        $this->TBS->MergeBlock('header', $result['header']);
        $this->TBS->MergeBlock('income', $result['income']);
        $this->TBS->MergeBlock('hpp', $result['hpp']);
        $this->TBS->MergeBlock('biaya', $result['biaya']);
        $this->TBS->MergeBlock('other_income', $result['other_income']);
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "LabaRugi" . $from . $to . ".xls");
    }
    public function actionNeraca()
    {
        $to = $_POST['to_date'];
        $result = self::create_neraca_new($to);
        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'nr.xlsx');
        $this->TBS->MergeBlock('header', $result['header']);
        $this->TBS->MergeBlock('current', $result['current']);
        $this->TBS->MergeBlock('fixed', $result['fixed']);
        $this->TBS->MergeBlock('equity', $result['equity']);
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "NERACA" . $to . ".xlsx");
    }
    private function create_neraca_new($to)
    {
        $lr = ChartMaster::get_neraca_new($to);
        $current_assets = ChartTypes::get_chart_types_by_class(CL_CURRENT_ASSETS);
        $curent_assets_arr = array();
        foreach ($current_assets as $row) {
            $curent_assets_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $curent_assets_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $curent_assets_arr = array_merge($curent_assets_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_current_assets = array_sum(array_column($curent_assets_arr, 'total'));
        $fixed_assets = ChartTypes::get_chart_types_by_class(CL_FIXED_ASSETS);
        $fixed_assets_arr = array();
        foreach ($fixed_assets as $row) {
            $fixed_assets_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $fixed_assets_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $fixed_assets_arr = array_merge($fixed_assets_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_fixed_assets = array_sum(array_column($fixed_assets_arr, 'total'));
//        var_dump($hpp_arr);
        $current_liabilities = ChartTypes::get_chart_types_by_class(CL_CURRENT_LIABILITIES);
        $current_liabilities_arr = array();
        foreach ($current_liabilities as $row) {
            $current_liabilities_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $current_liabilities_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $current_liabilities_arr = array_merge($current_liabilities_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_current_liabilities = array_sum(array_column($current_liabilities_arr, 'total'));
        $fixed_liabilities = ChartTypes::get_chart_types_by_class(CL_LONGTERM_LIABILITIES);
        $fixed_liabilities_arr = array();
        foreach ($fixed_liabilities as $row) {
            $fixed_liabilities_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $fixed_liabilities_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $fixed_liabilities_arr = array_merge($fixed_liabilities_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_fixed_liabilities = array_sum(array_column($fixed_liabilities_arr, 'total'));
        $equiti = ChartTypes::get_chart_types_by_class(CL_EQUITY);
        $equiti_arr = array();
        foreach ($equiti as $row) {
            $equiti_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $equiti_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $equiti_arr = array_merge($equiti_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_equiti = array_sum(array_column($equiti_arr, 'total'));
        $laba_kotor = $total_current_assets - $total_fixed_assets;
        $total_laba_bersih = $laba_kotor - $total_current_liabilities;
        $laba_bersih_sebelum_pajak = $total_laba_bersih - $total_fixed_liabilities;
        $count_assets = count($curent_assets_arr);
        $count_current_liability = count($current_liabilities_arr);
        $count_fixed_assets = count($fixed_assets_arr);
        $count_longterm_liability = count($fixed_liabilities_arr);
        if ($count_assets >= $count_current_liability) {
            for ($i = 0; $i <= $count_assets; $i++) {
                $arr_current_assets[$i]['account_code_assets'] = $curent_assets_arr[$i]['account_code'];
                $arr_current_assets[$i]['account_assets'] = $curent_assets_arr[$i]['account_name'];
                $arr_current_assets[$i]['assets'] = $curent_assets_arr[$i]['total'];
                if ($i > $count_current_liability) {
                    $arr_current_assets[$i]['account_code_liabilities'] = null;
                    $arr_current_assets[$i]['account_liabilities'] = null;
                    $arr_current_assets[$i]['liabilities'] = null;
                } else {
                    $arr_current_assets[$i]['account_code_liabilities'] = $current_liabilities_arr[$i]['account_code'];
                    $arr_current_assets[$i]['account_liabilities'] = $current_liabilities_arr[$i]['account_name'];
                    $arr_current_assets[$i]['liabilities'] = $current_liabilities_arr[$i]['total'];
                }
            }
        } else {
            for ($i = 0; $i <= $count_current_liability; $i++) {
                $arr_current_assets[$i]['account_code_liabilities'] = $current_liabilities_arr[$i]['account_code'];
                $arr_current_assets[$i]['account_liabilities'] = $current_liabilities_arr[$i]['account_name'];
                $arr_current_assets[$i]['liabilities'] = $current_liabilities_arr[$i]['total'];
                if ($i > $count_assets) {
                    $arr_current_assets[$i]['account_code_assets'] = null;
                    $arr_current_assets[$i]['account_assets'] = null;
                    $arr_current_assets[$i]['assets'] = null;
                } else {
                    $arr_current_assets[$i]['account_code_assets'] = $curent_assets_arr[$i]['account_code'];
                    $arr_current_assets[$i]['account_assets'] = $curent_assets_arr[$i]['account_name'];
                    $arr_current_assets[$i]['assets'] = $curent_assets_arr[$i]['total'];
                }
            }
        }
        if ($count_fixed_assets >= $count_longterm_liability) {
            for ($i = 0; $i <= $count_fixed_assets; $i++) {
                $arr_fixed_assets[$i]['account_code_assets'] = $fixed_assets_arr[$i]['account_code'];
                $arr_fixed_assets[$i]['account_assets'] = $fixed_assets_arr[$i]['account_name'];
                $arr_fixed_assets[$i]['assets'] = $fixed_assets_arr[$i]['total'];
                if ($i > $count_longterm_liability) {
                    $arr_fixed_assets[$i]['account_code'] = null;
                    $arr_fixed_assets[$i]['account_liabilities'] = null;
                    $arr_fixed_assets[$i]['liabilities'] = null;
                } else {
                    $arr_fixed_assets[$i]['account_code'] = $fixed_liabilities_arr[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_liabilities'] = $fixed_liabilities_arr[$i]['account_name'];
                    $arr_fixed_assets[$i]['liabilities'] = $fixed_liabilities_arr[$i]['total'];
                }
            }
        } else {
            for ($i = 0; $i <= $count_longterm_liability; $i++) {
                $arr_fixed_assets[$i]['account_code'] = $fixed_liabilities_arr[$i]['account_code'];
                $arr_fixed_assets[$i]['account_liabilities'] = $fixed_liabilities_arr[$i]['account_name'];
                $arr_fixed_assets[$i]['liabilities'] = $fixed_liabilities_arr[$i]['after'];
                if ($i > $count_fixed_assets) {
                    $arr_fixed_assets[$i]['account_code_assets'] = null;
                    $arr_fixed_assets[$i]['account_assets'] = null;
                    $arr_fixed_assets[$i]['assets'] = null;
                } else {
                    $arr_fixed_assets[$i]['account_code_assets'] = $fixed_assets_arr[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_assets'] = $fixed_assets_arr[$i]['account_name'];
                    $arr_fixed_assets[$i]['assets'] = $fixed_assets_arr[$i]['after'];
                }
            }
        }
        return array(
            'header' => array(
                array(
                    'to' => $to,
                    'total_current_assets' => $total_current_assets,
                    'total_fixed_assets' => $total_fixed_assets,
                    'total_current_liabilities' => $total_current_liabilities,
                    'total_longterm_liabilities' => $total_fixed_liabilities,
                    'total_equity' => $total_equiti,
                    'activa' => $total_current_assets + $total_fixed_assets,
                    'passiva' => $total_current_liabilities + $total_fixed_liabilities + $total_equiti
                )
            ),
            'current' => $arr_current_assets,
            'fixed' => $arr_fixed_assets,
            'equity' => $equiti_arr
        );
    }
    public function actionRekeningKoran()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $bank_id = $_POST['bank_id'];
            $summary = U::get_bank_trans($from, $to, $bank_id);
            $bank = Bank::model()->findByPk($bank_id);
            $begin = U::get_balance_before_for_bank_account($from, $bank_id);
            $begin_arr = array(
                'tgl' => '',
                'ref' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin
            );
            $gl[] = $begin_arr;
            foreach ($summary as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = $newrow['amount'] + $begin;
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekeningKoran$from-$to.xls");
                echo $this->render('RekeningKoran',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ), true);
            } else {
                $this->render('RekeningKoran',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ));
//                $this->renderJsonArr($summary);
            }
        }
    }
    public function actionEFaktur()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $efaktur = U::report_E_Faktur($from, $to);
            foreach ($efaktur as $key => $row) {
                if ($efaktur[$key]['col1'] != 'FK') {
                    continue;
                }
                $efaktur[$key]['col4'] = preg_replace('/[^\da-z]/i', '', preg_replace("/^\d+./", '', $row['col4'])); //No. Faktur Pajak
                $efaktur[$key]['col8'] = preg_replace('/[^\da-z]/i', '', $row['col8']); //NPWP
            }
            $dataProvider = new CArrayDataProvider($efaktur, array(
                'id' => 'EFaktur',
                'pagination' => false
            ));
            if ($this->format == 'csv') {
                header('Content-type: text/csv');
                header("Content-Disposition: attachment; filename=EFaktur$from-$to.csv");
                $fp = fopen('php://output', 'w');
                $header[] = array("FK", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP", "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNBM", "ID_KETERANGAN_TAMBAHAN", "FG_UANG_MUKA", "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM", "REFERENSI");
                $header[] = array("LT", "NPWP", "NAMA", "JALAN", "BLOK", "NOMOR", "RT", "RW", "KECAMATAN", "KELURAHAN", "KABUPATEN", "PROPINSI", "KODE_POS", "NOMOR_TELEPON", "", "", "", "", "");
                $header[] = array("OF", "KODE_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP", "PPN", "TARIF_PPNBM", "PPNBM", "", "", "", "", "", "", "", "");
                foreach ($header as $row) {
                    fputcsv($fp, $row, ";");
                }
                foreach ($efaktur as $row) {
                    fputcsv($fp, $row, ";");
                }
                fclose($fp);
            } elseif ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=EFaktur$from-$to.xls");
                echo $this->render('EFaktur', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ), true);
            } else {
                $this->render('EFaktur', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
            }
        }
    }
    public function actionRekapPurchaseOrder()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'] == '' ? 'All' : $_POST['supplier_id'];
            $tipe_material_id = $_POST['tipe_material_id'] == '' ? 'All' : $_POST['tipe_material_id'];
            $status = $_POST['status'] == '' ? 'All' : $_POST['status'];
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $data = $show_detail ?
                U::report_rekap_PO_details($from, $to, $supplier_id, $tipe_material_id, $status)
                :
                U::report_rekap_PO($from, $to, $supplier_id, $tipe_material_id, $status);
            if ($show_detail) {
                foreach ($data as $key => $row) {
                    $row['qty'] = UnitOfMeasure::get_qtySatuan($row['qty'], $row['sat']);
                    $row['qty_terima'] = UnitOfMeasure::get_qtySatuan($row['qty_terima'], $row['sat']);
                    $data[$key] = $row;
                }
            }
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ListPO',
                'pagination' => false
            ));
            if ($supplier_id != 'All') {
                $modelSupplier = $this->loadModel($supplier_id, "Supplier");
                $supplier_id = $modelSupplier->supplier_name;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapPO$from-$to.xls");
                echo $this->render($show_detail ? 'RekapPOdetails' : 'RekapPO', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'tipe_material' => $tipe_material_id,
                    'status' => $status
                ), true);
            } else {
                $this->render($show_detail ? 'RekapPOdetails' : 'RekapPO', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'tipe_material' => $tipe_material_id,
                    'status' => $status
                ));
            }
        }
    }
    public function actionRekapTerimaMaterial()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'] == '' ? 'All' : $_POST['supplier_id'];
            $status = $_POST['status'] == '' ? 'All' : $_POST['status'];
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $data = $show_detail ?
                U::report_rekap_Sj_Supplier_details($from, $to, $supplier_id, $status)
                :
                U::report_rekap_Sj_Supplier($from, $to, $supplier_id, $status);
            if ($show_detail) {
                foreach ($data as $key => $row) {
                    $row['qty'] = UnitOfMeasure::get_qtySatuan($row['qty'], $row['sat']);
                    $data[$key] = $row;
                }
            }
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ListSJ',
                'pagination' => false
            ));
            if ($supplier_id != 'All') {
                $modelSupplier = $this->loadModel($supplier_id, "Supplier");
                $supplier_id = $modelSupplier->supplier_name;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapSuratJalanSupplier$from-$to.xls");
                echo $this->render($show_detail ? 'RekapSuratJalanSupplierDetails' : 'RekapSuratJalanSupplier', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'status' => $status
                ), true);
            } else {
                $this->render($show_detail ? 'RekapSuratJalanSupplierDetails' : 'RekapSuratJalanSupplier', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'status' => $status
                ));
            }
        }
    }
    public function actionRekapSupplierInvoice()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'] == '' ? 'All' : $_POST['supplier_id'];
            $status = $_POST['status'] == '' ? 'All' : $_POST['status'];
            $_POST['show_belum_dibayar'] = isset($_POST['show_belum_dibayar']) ? TRUE : FALSE;
            $_POST['show_lewat_jatuh_tempo'] = isset($_POST['show_lewat_jatuh_tempo']) ? TRUE : FALSE;
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $data = U::report_rekap_INV_Supplier($from, $to, $supplier_id, $status);
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ListInvSupplier',
                'pagination' => false
            ));
            if ($supplier_id != 'All') {
                $modelSupplier = $this->loadModel($supplier_id, "Supplier");
                $supplier_id = $modelSupplier->supplier_name;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapInvoiceSupplier$from-$to.xls");
                echo $this->render($show_detail ? 'RekapInvoiceSupplierDetails' : 'RekapInvoiceSupplier', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'status' => $status
                ), true);
            } else {
                $this->render($show_detail ? 'RekapInvoiceSupplierDetails' : 'RekapInvoiceSupplier', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'status' => $status
                ));
            }
        }
    }
    public function actionRekapPembayaranSupplier()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'] == '' ? 'All' : $_POST['supplier_id'];
            $status = $_POST['status'] == '' ? 'All' : $_POST['status'];
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $data = $show_detail ?
                U::report_rekap_Pembayaran_Supplier_details($from, $to, $supplier_id, $status)
                :
                U::report_rekap_Pembayaran_Supplier($from, $to, $supplier_id, $status);
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ListFPT',
                'pagination' => false
            ));
            if ($supplier_id != 'All') {
                $modelSupplier = $this->loadModel($supplier_id, "Supplier");
                $supplier_id = $modelSupplier->supplier_name;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapFPTSupplier$from-$to.xls");
                echo $this->render($show_detail ? 'RekapPembayaranSupplierDetails' : 'RekapPembayaranSupplier', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'status' => $status
                ), true);
            } else {
                $this->render($show_detail ? 'RekapPembayaranSupplierDetails' : 'RekapPembayaranSupplier', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'status' => $status
                ));
            }
        }
    }
    public function actionRekapPembelian()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'] == '' ? 'All' : $_POST['supplier_id'];
            $data = U::report_rekap_Pembelian($from, $to, $supplier_id);
            foreach ($data as $key => $row) {
                $row['qty'] = UnitOfMeasure::get_qtySatuan($row['qty'], $row['sat']);
                $row['total'] = UnitOfMeasure::get_qtySatuan($row['total'], $row['sat']);
                $row['dpp'] = UnitOfMeasure::get_qtySatuan($row['dpp'], $row['sat']);
                $row['ppn'] = UnitOfMeasure::get_qtySatuan($row['ppn'], $row['sat']);
                $row['pph'] = UnitOfMeasure::get_qtySatuan($row['pph'], $row['sat']);
                $row['total_bayar'] = UnitOfMeasure::get_qtySatuan($row['total_bayar'], $row['sat']);
                $data[$key] = $row;
            }
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ListPembelian',
                'pagination' => false
            ));
            if ($supplier_id != 'All') {
                $modelSupplier = $this->loadModel($supplier_id, "Supplier");
                $supplier_id = $modelSupplier->supplier_name;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapFPTSupplier$from-$to.xls");
                echo $this->render('RekapPembelian', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id
                ), true);
            } else {
                $this->render('RekapPembelian', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id
                ));
            }
        }
    }
    public function actionPrintNotaReturn()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            /** @var ReturnPembelian $nr */
            $nr = $this->loadModel($_POST['return_pembelian_id'], 'ReturnPembelian');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'nota_return.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $items = $nr->getItemsNotaReturn();
            $jml = count($items);
            for ($i = 0; $i < 10; $i++) {
                if ($i < $jml) {
//                    $items[$i]['qty'] = UnitOfMeasure::get_qtySatuan($items[$i]['qty'], $items[$i]['sat']);
                } else {
                    $items[] = array(
                        'nama_material' => '',
                        'qty' => '',
                        'price' => '',
                        'harga_jual' => ''
                    );
                }
            }
            $header_data = array(
                'tgl' => date_format(date_create($nr->nota_return_tgl), 'd F Y'),
                'doc_ref' => $nr->nota_return_doc_ref,
                'dpp' => $nr->nota_return_dpp,
                'ppn' => $nr->nota_return_ppn,
                'ppnbm' => $nr->nota_return_ppnbm,
                'no_faktur_pajak' => $nr->nota_return_no_faktur_pajak,
                'tgl_faktur_pajak' => date_format(date_create($nr->nota_return_tgl_faktur_pajak), 'd F Y'),
                'nama_pembeli' => $nr->nota_return_nama_pembeli,
                'pembeli' => $nr->nota_return_pembeli,
                'alamat' => $nr->nota_return_alamat,
                'npwp' => $nr->nota_return_npwp,
                'penjual' => $nr->nota_return_penjual,
                'alamat_penjual' => $nr->nota_return_alamat_penjual,
                'npwp_penjual' => $nr->nota_return_npwp_penjual
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $items);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Nota Return $nr->nota_return_doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintTandaTerima()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $nr = $this->loadModel($_POST['return_pembelian_id'], 'ReturnPembelian');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tandaterima.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $items = $nr->getItemsTandaTerima();
            foreach ($items as $key => $row) {
                //$row['qty'] = $row['qty']//UnitOfMeasure::get_qtySatuan($row['qty'], $row['sat']);
                $items[$key] = $row;
            }
            $modelSupplier = $this->loadModel($nr->supplier_id, "Supplier");
            $header_data = array(
                'tgl' => date_format(date_create($nr->tgl), 'd F Y'),
                'doc_ref' => $nr->doc_ref,
                'kepada' => $modelSupplier->supplier_name
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $items);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "TANDA TERIMA $nr->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionLaporanProduksi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $user = Users::model()->findByPk(user()->getId());
            $_POST['USER_PRODUKSI'] = $user->security_roles_id == USER_PRODUKSI;
            $template = 'LaporanProduksi';
            $mutasi = U::report_laporan_produksi($from, $to);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'LaporanProduksi',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=LaporanProduksi$from-$to.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => DateTime::createFromFormat('Y-m-d', $from)->format('j M Y'),
                    'to' => DateTime::createFromFormat('Y-m-d', $to)->format('j M Y'),
                    'now' => date("j F Y"),
                    'format' => $this->format
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'from' => DateTime::createFromFormat('Y-m-d', $from)->format('j M Y'),
                    'to' => DateTime::createFromFormat('Y-m-d', $to)->format('j M Y'),
                    'format' => $this->format
                ));
            }
        }
    }
    public function actionRekapTransferMaterial()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $pengirim = !$_POST['pengirim'] ? 'All' : $_POST['pengirim'];
            $penerima = !$_POST['penerima'] ? 'All' : $_POST['penerima'];
            $status = $_POST['status'] == "" ? 'All' : $_POST['status'];
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $data = U::report_rekap_Transfer_Material($from, $to, $pengirim, $penerima, $status, $show_detail ? TRUE : FALSE);
            foreach ($data as $k => $p) {
                $data[$k]['qty'] = UnitOfMeasure::get_qtySatuan($p['qty'], $p['sat']);
            }
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ListTandaTerima',
                'pagination' => false
            ));
            $title = "Rekap Transfer Material";
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapPO$from-$to.xls");
                echo $this->render('RekapTandaTerima', array(
                    'dp' => $dataProvider,
                    'title' => $title,
                    'from' => date_format(date_create($from), 'd M Y'),
                    'to' => date_format(date_create($to), 'd M Y'),
                    'pengirim' => $GLOBALS['storLoc'][$pengirim]['name'],
                    'penerima' => $GLOBALS['storLoc'][$penerima]['name'],
                    'status' => $status
                ), true);
            } else {
                $this->render('RekapTandaTerima', array(
                    'dp' => $dataProvider,
                    'title' => $title,
                    'from' => date_format(date_create($from), 'd M Y'),
                    'to' => date_format(date_create($to), 'd M Y'),
                    'pengirim' => $GLOBALS['storLoc'][$pengirim]['name'],
                    'penerima' => $GLOBALS['storLoc'][$penerima]['name'],
                    'status' => $status
                ));
            }
        }
    }
    public function actionRekapTransferBarang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $pengirim = !$_POST['pengirim'] ? 'All' : $_POST['pengirim'];
            $penerima = !$_POST['penerima'] ? 'All' : $_POST['penerima'];
            $status = $_POST['status'] == "" ? 'All' : $_POST['status'];
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $data = U::report_rekap_Transfer_Barang($from, $to, $pengirim, $penerima, $status, $show_detail ? TRUE : FALSE);
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ListTandaTerima',
                'pagination' => false
            ));
            $title = "Rekap Transfer Barang Jadi";
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapPO$from-$to.xls");
                echo $this->render('RekapTandaTerima', array(
                    'dp' => $dataProvider,
                    'title' => $title,
                    'from' => date_format(date_create($from), 'd M Y'),
                    'to' => date_format(date_create($to), 'd M Y'),
                    'pengirim' => $GLOBALS['storLoc'][$pengirim]['name'],
                    'penerima' => $GLOBALS['storLoc'][$penerima]['name'],
                    'status' => $status
                ), true);
            } else {
                $this->render('RekapTandaTerima', array(
                    'dp' => $dataProvider,
                    'title' => $title,
                    'from' => date_format(date_create($from), 'd M Y'),
                    'to' => date_format(date_create($to), 'd M Y'),
                    'pengirim' => $GLOBALS['storLoc'][$pengirim]['name'],
                    'penerima' => $GLOBALS['storLoc'][$penerima]['name'],
                    'status' => $status
                ));
            }
        }
    }
    public function actionMasterlistDokumenInternal()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $activity = $_POST['pustaka_qa_activity_id'];
            $tipe = $_POST['pustaka_qa_tipe_id'];
            $aktif = isset($_POST['aktif']) ? 1 : 0;
            $tidakaktif = isset($_POST['tidakaktif']) ? 1 : 0;
            $template = 'MasterlistDokumenInternal';
            $modelTipe = $this->loadModel($tipe, 'PustakaQaTipe');
            $modelActivity = $this->loadModel($activity, 'PustakaQaActivity');
            $docs = PustakaQa::masterlist_dokumen_internal($activity, $tipe, $aktif, $tidakaktif);
            $lists = array();
            $masterlist = array();
            foreach ($docs as $doc) {
                if (!array_key_exists($doc['pustaka_qa_id'], $lists)) {
                    $lists[$doc['pustaka_qa_id']] = $doc;
                }
                $number = $doc['revisi_ke'];
                $lists[$doc['pustaka_qa_id']]['tgl_rev' . (int)$number] = $doc['tgl_efektif'];
            }
            foreach ($lists as $list) $masterlist[] = $list;
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'MasterlistDokumenInternal.xml');
                $this->TBS->SetOption('noerr', true);
                $header_data = array(
                    'tipe' => $modelTipe->tipe_name,
                    'activity' => $modelActivity->activity_name
                );
                $this->TBS->MergeField('header', $header_data);
                $this->TBS->MergeBlock('doc', $masterlist);
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "MASTERLIST.xls");
            } else {
                $dataProvider = new CArrayDataProvider($masterlist, array(
                    'id' => 'MasterlistDokumenInternal',
                    'pagination' => false
                ));
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'tipe' => $modelTipe->tipe_name,
                    'activity' => $modelActivity->activity_name
                ));
            }
        }
    }
    public function actionPrintFinAccReport()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $fa = $this->loadModel($_POST['fin_acc_report_id'], 'FinAccReport');
            $report = new PhpExcelReportHelper();
            $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'hpp.xls');
            //Laporan Produksi
            $laporanProduksi = U::report_laporan_produksi($fa->start_date, $fa->end_date, TRUE);
            $field = array(
                'A2' => $fa->start_date . " - " . $fa->end_date
            );
            $columnData = array(
                array(
                    'A' => 'no_bom',
                    'B' => 'nama_barang',
                    'C' => 'kode_formula',
                    'D' => 'batch',
                    'E' => 'batch_size',
                    'F' => 'qty_per_pot',
                    'G' => '',
                    'H' => 'nama_grup',
                    'I' => 'tgl_prepare',
                    'J' => 'tgl_mixing',
                    'K' => 'qty_mixing',
                    'L' => 'tgl_kirim',
                    'M' => 'no_lot',
                    'N' => 'qty_batch',
                    'O' => 'status_bdp',
                )
            );
            $report->setActiveSheetIndex(0)
                ->mergeField($field)
                ->mergeBlock($laporanProduksi, $columnData, 6);
            //Rangkuman Laporan Produksi
            $pengiriman = FaBop::get_pengiriman_produksi($fa->start_date, $fa->end_date, TRUE);
            $pengirimanTotal = 0;
            foreach ($pengiriman as $k => $v) {
                $pengirimanTotal += (float)$pengiriman[$k]['qty'];
            }
            $columnData_pengiriman = array(
                array(
                    'A' => 'kode_barang',
                    'B' => 'nama_barang',
                    'C' => 'qty'
                )
            );
            $hasilProduksi = FaHppBarangjadi::get_total_hasil_produksi($fa->start_date, $fa->end_date, TRUE);
            $hasilProduksiTotal = 0;
            foreach ($hasilProduksi as $k => $v) {
                $hasilProduksiTotal += (float)$hasilProduksi[$k]['qty'];
            }
            $columnData_hasilProduksi = array(
                array(
                    'A' => 'kode_barang',
                    'B' => 'nama_barang',
                    'C' => 'no_bom',
                    'D' => 'batch_size',
                    'E' => 'qty',
                    'F' => 'x_produksi',
                )
            );
            $field = array(
                'A2' => $fa->start_date . " - " . $fa->end_date,
                'C6' => $pengirimanTotal,
                'E12' => $hasilProduksiTotal
            );
            $report->setActiveSheetIndex(1)
                ->mergeField($field)
                ->mergeBlock($pengiriman, $columnData_pengiriman, 19)
                ->mergeBlock($hasilProduksi, $columnData_hasilProduksi, 11)
                ->mergeBlock($pengiriman, $columnData_pengiriman, 5);
            //Biaya Overhead Produksi (BOP)
            $bop = FaBop::report_BiayaOverheadProduksi($fa->fin_acc_report_id);
            $bopTotal = FaBop::report_BiayaOverheadProduksi($fa->fin_acc_report_id, TRUE);
            $field = array(
                'C4' => $fa->biaya_tenagakerja_tidak_langsung,
                'D4' => $fa->biaya_listrik,
                'E4' => $fa->biaya_penyusutan_mesin,
                'B6' => $bopTotal['qty'],
                'C6' => $bopTotal['biaya_tenagakerja_tidak_langsung'],
                'D6' => $bopTotal['biaya_listrik'],
                'E6' => $bopTotal['biaya_penyusutan_mesin'],
                'F6' => $bopTotal['total_bop']
            );
            $columnData = array(
                array(
                    'A' => 'kode_barang',
                    'B' => 'qty',
                    'C' => 'biaya_tenagakerja_tidak_langsung',
                    'D' => 'biaya_listrik',
                    'E' => 'biaya_penyusutan_mesin',
                    'F' => 'total_bop',
                    'G' => 'bop_per_pcs'
                )
            );
            $report->setActiveSheetIndex(2)
                ->mergeField($field)
                ->mergeBlock($bop, $columnData, 5);
            //HPP Barang Jadi
            $hppBJ = FaHppBarangjadi::report_HPP_BJ($fa->fin_acc_report_id);
            $field = array(
                'A2' => $fa->start_date . " - " . $fa->end_date
            );
            $columnData = array(
                array(
                    'A' => 'kode_barang',
                    'B' => 'nama_barang',
                    'C' => 'no_bom',
                    'D' => 'batch_size',
                    'E' => 'qty',
                    'F' => 'x_produksi',
                    'G' => 'nilai_rm_per_batch',
                    'H' => 'nilai_rm_total',
                    'I' => 'nilai_rm_per_pce',
                    'J' => 'nilai_pm_per_pce',
                    'L' => 'bop',
                    'M' => 'hpp_per_pce',
                    'N' => 'price',
                    'O' => 'profit'
                )
            );
            $report->setActiveSheetIndex(3)
                ->mergeField($field)
                ->mergeBlock($hppBJ, $columnData, 4);
            //Stock GPJ (Nasional)
            $stockGPJ = FaStockGpj::report_Stock_BJ($fa->fin_acc_report_id);
            $sumStockGPJ = FaStockGpj::report_Stock_BJ($fa->fin_acc_report_id, TRUE);
            $field = array(
                'A3' => $fa->start_date . " - " . $fa->end_date,
                'E8' => $sumStockGPJ['qty_before'],
                'F8' => $sumStockGPJ['hpp_before'],
                'G8' => $sumStockGPJ['nominal_before'],
                'H8' => $sumStockGPJ['qty_in'],
                'I8' => $sumStockGPJ['hpp_per_pce'],
                'J8' => $sumStockGPJ['nominal_in'],
                'K8' => $sumStockGPJ['qty_out'],
                'L8' => $sumStockGPJ['hpp_out'],
                'M8' => $sumStockGPJ['nominal_out'],
                'N8' => $sumStockGPJ['qty_after'],
                'O8' => $sumStockGPJ['hpp_after'],
                'P8' => $sumStockGPJ['nominal_after']
            );
            $columnData = array(
                array(
                    'A' => '#',
                    'B' => 'kode_barang',
                    'C' => 'nama_barang',
                    'D' => 'sat',
                    'E' => 'qty_before',
                    'F' => 'hpp_before',
                    'G' => 'nominal_before',
                    'H' => 'qty_in',
                    'I' => 'hpp_per_pce',
                    'J' => 'nominal_in',
                    'K' => 'qty_out',
                    'L' => 'hpp_out',
                    'M' => 'nominal_out',
                    'N' => 'qty_after',
                    'O' => 'hpp_after',
                    'P' => 'nominal_after'
                )
            );
            $report->setActiveSheetIndex(4)
                ->mergeField($field)
                ->mergeBlock($stockGPJ, $columnData, 7);
            //HPP Bahanbaku dan Kemas
            $hppMat = FaHppMaterial::report_hpp_material($fa->fin_acc_report_id);
            $field = array(
                'A2' => $fa->start_date . " - " . $fa->end_date
            );
            $columnData = array(
                array(
                    'A' => '#',
                    'B' => 'kode_material',
                    'C' => 'nama_material',
                    'D' => '', //sat
                    'E' => 'qty_before',
                    'F' => 'hpp_before',
                    'G' => 'nominal_before',
                    'H' => 'qty',
                    'I' => 'price',
                    'J' => 'nominal',
                    'K' => 'qty_after',
                    'L' => 'hpp_after',
                    'M' => 'nominal_after'
                )
            );
            $report->setActiveSheetIndex(5)
                ->mergeField($field)
                ->mergeBlock($hppMat, $columnData, 5);
            //Barang Dalam Proses (BDP)
            $stockBDP = FaStockBdp::report_stock_BDP($fa->fin_acc_report_id);
            $sumStockBDP = FaStockBdp::report_stock_BDP($fa->fin_acc_report_id, TRUE);
            $field = array(
                'A2' => $fa->start_date . " - " . $fa->end_date,
                'F6' => $sumStockBDP['qty'],
                'G6' => $sumStockBDP['total_bj'],
                'L6' => $sumStockBDP['nilai_rm_pm']
            );
            $columnData = array(
                array(
                    'A' => 'kode_barang',
                    'B' => 'nama_barang',
                    'C' => 'kode_formula',
                    'D' => 'batch_size_kg',
                    'E' => 'no_batch',
                    'F' => 'qty',
                    'G' => 'total_bj',
                    'H' => 'nilai_rm_per_batch',
                    'I' => 'nilai_pm_per_pce',
                    'J' => 'nilai_rm',
                    'K' => 'nilai_pm',
                    'L' => 'nilai_rm_pm'
                )
            );
            $report->setActiveSheetIndex(6)
                ->mergeField($field)
                ->mergeBlock($stockBDP, $columnData, 5);
            //HPP
            $hpp = FinAccReport::model()->find("fin_acc_report_id = :fin_acc_report_id", array(':fin_acc_report_id' => $fa->fin_acc_report_id));
            $field = array(
                'A2' => $fa->start_date . " - " . $fa->end_date,
                'C4' => $hpp['value_stock_material_before'],
                'C5' => $hpp['value_pembelian_material'],
                'C6' => $hpp['value_stock_material_after'],
                'C10' => 0,
                'C11' => $hpp['value_bop'],
                'C15' => $hpp['value_stock_bdp_before'],
                'C16' => $hpp['value_stock_bdp_after'],
                'C20' => $hpp['value_stock_barang_before'],
                'C24' => $hpp['value_stock_barang_after']
            );
            $report->setActiveSheetIndex(7)
                ->mergeField($field);
            //download report
            $report->downloadExcel('Laporan FA');
        }
    }
    public function actionPrintPerbaikanMesin()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $pm = $this->loadModel($_POST['perbaikan_mesin_id'], 'PerbaikanMesin');
            $field = array(
                'C5' => $pm->departemen,
                'C7' => $pm->lokasi_kerja,
                'A9' => '  Tanggal Laporan Kerusakan : ' . date_format(date_create($pm->tgl_lapor), 'd-m-Y'),
                'H6' => $pm->pelapor,
                'A13' => $pm->permintaan_perbaikan,
                'A29' => '  TGL : ' . date_format(date_create($pm->tgl_terima), 'd-m-Y'),
                'A31' => '  JAM : ' . $pm->jam_terima,
                'A34' => $pm->nomor_pp,
                'C28' => $pm->laporan_pekerjaan,
                'C44' => '  PEKERJAAN SELESAI TGL : ' . date_format(date_create($pm->tgl_selesai), 'd-m-Y'),
                'G44' => '  JAM : ' . $pm->jam_selesai,
                'E46' => '  TGL : ' . date_format(date_create($pm->tgl_serahterima), 'd-m-Y'),
                'H48' => $pm->mutu ? 'X' : '',
                'H50' => $pm->mutu ? '' : 'X',
                'A40' => '(' . $pm->adm_engineering . ')',
                'A46' => '(' . $pm->manager_spv . ')',
                'D46' => '(' . $pm->pelaksana . ')'
            );
            $field2 = array(
                'C3' => ': ' . $pm->doc_ref,
                'C4' => ': ' . $pm->nomor_pp
            );
            $pmd = $pm->reportPemakaianSparepart();
            foreach ($pmd as $k => $p) {
                $pmd[$k]['qty'] = UnitOfMeasure::get_qtySatuan($p['qty'], $p['sat']);
            }
            $columnData = array(
                array(
                    'A' => '#',
                    'B' => 'kode_material',
                    'D' => 'supplier_name',
                    'F' => 'qty',
                    'G' => 'sat'
                ),
                array(
                    'B' => 'nama_material',
                    'F' => 'tgl'
                ),
                array(
                    'B' => 'ket : [ket]'
                )
            );
            $report = new PhpExcelReportHelper();
            $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'formPerbaikanMesin.xls')
                ->setActiveSheetIndex(1)
                ->mergeField($field2)
                ->mergeBlock($pmd, $columnData, 8, 4)
                ->protectSheet()
                ->setActiveSheetIndex(0)
                ->mergeField($field)
                ->protectSheet()
                ->downloadExcel('Laporan Perbaikan Mesin ' . $pm->nomor_pp);
        }
    }
    public function actionPrintTandaTerimaSparepartMasuk()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST['sparepart_masuk_id']) && !empty($_POST['sparepart_masuk_id'])) {
            $t = $this->loadModel($_POST['sparepart_masuk_id'], 'SparepartMasuk');
            $template = 'tandaterimaSparepartBekas.xml';
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . $template);
            }
            $this->TBS->SetOption('noerr', true);
            $td = $t->report();
            $header = array(
                'doc_ref' => $t->doc_ref,
                'tgl' => date_format(date_create($t->tgl), 'd F Y'),
                'note' => $t->note,
                'pengirim' => $t->nama,
                'penerima' => Users::model()->findByPk(user()->getId())->name_
            );
            $this->TBS->MergeField('header', $header);
            $this->TBS->MergeBlock('item', $td);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Tanda Terima Sparepart Second $t->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionBukuPiutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $row = U::piutang_due();
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'R_piutang_due.xml');
            $this->TBS->MergeBlock('header', array(
                array(
                    'title_pt' => app()->params['report_title_pt'],
                    'tgl_now' => get_date_today('dd MMM yyyy')
                )
            ));
            $this->TBS->MergeBlock('0', $row);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "piutang_due.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionBukuHutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $row = U::hutang_due();
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'hutang_due.xml');
            $this->TBS->MergeBlock('header', array(
                array(
                    'title_pt' => app()->params['report_title_pt'],
                    'tgl_now' => get_date_today('dd MMM yyyy')
                )
            ));
            $this->TBS->MergeBlock('0', $row);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "hutang_due.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
}