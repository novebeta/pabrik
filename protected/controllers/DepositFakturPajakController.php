<?php
class DepositFakturPajakController extends GxController
{
    public function actionCreate()
    {
        $model = new DepositFakturPajak;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['DepositFakturPajak'][$k] = $v;
            }
            $query = DepositFakturPajak::model()->dbConnection->createCommand("SELECT UUID();");
            $uuid = $query->queryScalar();
            $model->fp_id = $uuid;
            $query = DepositFakturPajak::model()->dbConnection->createCommand("SELECT CURRENT_TIMESTAMP();");
            $tgl = $query->queryScalar();
            $model->tgl = $tgl;
            $model->attributes = $_POST['DepositFakturPajak'];
            $msg = "";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->fp_id;
            } else {
                $msg = "Data gagal disimpan. " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'DepositFakturPajak');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['DepositFakturPajak'][$k] = $v;
            }
            $msg = "";
            $model->attributes = $_POST['DepositFakturPajak'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->fp_id;
            } else {
                $msg = "Data gagal disimpan " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->fp_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = DepositFakturPajak::model()->findAll($criteria);
        $total = DepositFakturPajak::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'DepositFakturPajak')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}