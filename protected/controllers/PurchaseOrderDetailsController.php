<?php
class PurchaseOrderDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('po_id = :po_id');
        $criteria->params = array(':po_id' => $_POST['po_id']);
//        if (isset($_POST['viewprice']) && $_POST['viewprice'] == 0) {
//            $criteria->select = "po_detail_id, po_id, material_id, qty, sat, 0 price, note";
//        }
        $model = PurchaseOrderDetails::model()->findAll($criteria);
        $total = PurchaseOrderDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionGetItems()
    {
        $po_id = isset($_POST['po_id']) ? $_POST['po_id'] : null;
        $arr = PurchaseOrderDetails::get_items($po_id);
        $this->renderJsonArr($arr);
    }
}