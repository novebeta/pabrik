<?php
class TerimaBarangDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new TerimaBarangDetails;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['TerimaBarangDetails'][$k] = $v;
            }
            $model->attributes = $_POST['TerimaBarangDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->terima_barang_details_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'TerimaBarangDetails');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['TerimaBarangDetails'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['TerimaBarangDetails'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->terima_barang_details_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->terima_barang_details_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'TerimaBarangDetails')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['po_id'])) {
            $criteria->addCondition('po_id = :po_id');
            $criteria->params = array(':po_id' => $_POST['po_id']);
            $model = RemainReceive::model()->findAll($criteria);
            $total = RemainReceive::model()->count($criteria);
            $this->renderJson($model, $total);
            Yii::app()->end();
        }
        if (isset($_POST['terima_barang_id']) && isset($_POST['arus']) && $_POST['arus'] == 1) {
            $criteria->addCondition('terima_barang_id = :terima_barang_id');
            $criteria->params = array(':terima_barang_id' => $_POST['terima_barang_id']);
            $model = SuppCnoteTerima::model()->findAll($criteria);
            $total = SuppCnoteTerima::model()->count($criteria);
            $this->renderJson($model, $total);
            Yii::app()->end();
        }
        if (isset($_POST['terima_barang_id'])) {
            $criteria->addCondition('terima_barang_id = :terima_barang_id');
            $param[':terima_barang_id'] = $_POST['terima_barang_id'];
        }
        $criteria->params = $param;
        $model = TerimaBarangDetails::model()->findAll($criteria);
        $total = TerimaBarangDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}