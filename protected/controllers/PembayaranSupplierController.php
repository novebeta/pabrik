<?php
class PembayaranSupplierController extends GxController
{
    public function actionCreate() //Create FPT (Form Pengajuan Transfer)
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new PembayaranSupplier : $this->loadModel($id, "PembayaranSupplier");
                $docref = $model->doc_ref;
                //...jika edit
                if (!$is_new) {
                    //kembalikan status SJ/Invoice Supplier
                    TerimaMaterial::resetStatusFPT_to_INV($id);
                    //kembalikan status Nota return
                    ReturnPembelian::resetStatusUSED_to_OPEN($id);
                    //hapus semua details
                    PembayaranSupplierDetails::model()->deleteAll("pembayaran_supplier_id = :pembayaran_supplier_id",
                        array(':pembayaran_supplier_id' => $id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TerimaRawMaterial'][$k] = $v;
                }
                //...jika buat baru
                if ($is_new) {
                    $docref = Reference::createDocumentReference_FPT(($_POST['cara_pembayaran'] == 'TUNAI' ? FPT_TUNAI : FPT), $_POST['tgl']);
                    $_POST['TerimaRawMaterial']['doc_ref'] = $docref;
                }
                $model->attributes = $_POST['TerimaRawMaterial'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Penerimaan Material')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $item_details = new PembayaranSupplierDetails;
                    $_POST['PembayaranSupplierDetails'] = [];
                    foreach ($detil as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $_POST['PembayaranSupplierDetails'][$k] = $v;
                    }
                    $_POST['PembayaranSupplierDetails']['pembayaran_supplier_id'] = $model->pembayaran_supplier_id;
                    $item_details->attributes = $_POST['PembayaranSupplierDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    if ($item_details->return_pembelian_id === NULL) {
                        //.. update status SJ/Invoice Supplier
                        TerimaMaterial::setStatus(
                            SJ_SUPPLIER_FPT,
                            $item_details->terima_material_id,
                            $model->pembayaran_supplier_id
                        );
                    } else {
                        //.. update status Nota Return
                        ReturnPembelian::setStatusNotaReturn(
                            NOTA_RETURN_USED,
                            $item_details->return_pembelian_id,
                            $model->pembayaran_supplier_id
                        );
                    }
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionKonfirmasiBayar()
    {
        $pembayaran_supplier_id = $_POST['id'];
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = $this->loadModel($pembayaran_supplier_id, "PembayaranSupplier");
            $model->tgl_bayar = $_POST['tgl_bayar'];
            $model->IDR_rate = get_number($_POST['IDR_rate']);
            $model->status = FPT_PAID;
            if (!$model->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Penerimaan Material')) . CHtml::errorSummary($model));
            //.. update status SJ/Invoice Supplier
            TerimaMaterial::setStatus_PAID($pembayaran_supplier_id);
            /*
             * Suatu PO, jika semua barang telah diterima dan telah dibayar
             * maka status PO menjadi CLOSED
             */
            $listTM = $model->getList_terima_material_id();
            foreach ($listTM as $tm) {
                $listPO = TerimaMaterial::getListPO($tm['terima_material_id']);
                foreach ($listPO as $v) {
                    if (PurchaseOrder::getStatus($v['po_id']) == PO_RECEIVED &&
                        PurchaseOrder::deliveries_are_paid($v['po_id'])
                    ) {
                        //.. update status PO
                        PurchaseOrder::setStatus(PO_CLOSED, $v['po_id']);
                        //.. update status PO untuk PR bersangkutan
                        $modelPO = $this->loadModel($v['po_id'], "PurchaseOrder");
                        PurchaseRequisition::setStatus_PO(PO_CLOSED, $modelPO->pr_id);
                    }
                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }
    public function actionGetInvoiceAndNotaReturn()
    {
        if (isset($_POST['supplier_id'])) {
            $arr = PembayaranSupplier::get_InvoiceAndNotaReturn();
            $this->renderJsonArr($arr);
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->params = array();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl_terima = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            $criteria->addCondition('status = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
        $criteria->order = "tgl DESC";
        $model = PembayaranSupplier::model()->findAll($criteria);
        $total = PembayaranSupplier::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}