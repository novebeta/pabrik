<?php
class SjDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new SjDetails;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['SjDetails'][$k] = $v;
            }
            $model->attributes = $_POST['SjDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sj_details_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SjDetails');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['SjDetails'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SjDetails'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sj_details_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sj_details_id));
            }
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset($_POST['sj_id'])) {
            $criteria->addCondition('sj_id = :sj_id');
            $criteria->params = array(':sj_id' => $_POST['sj_id']);
            $model = SjDetails::model()->findAll($criteria);
            $total = SjDetails::model()->count($criteria);
        } else {
            $criteria->addCondition('sales_id = :sales_id');
            $criteria->params = array(':sales_id' => $_POST['sales_id']);
            $model = RemainDeliver::model()->findAll($criteria);
            $total = RemainDeliver::model()->count($criteria);
        }
        $this->renderJson($model, $total);
    }
}