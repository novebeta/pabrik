<?php
Yii::import('application.components.Reference');
class SalesController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $is_new = $_POST['mode'] == 0;
            $Sales_id = $_POST['id'];
            $msg = 'Sales berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Yii::import('application.components.U');
                /** @var Sales $Sales */
                $Sales = $is_new ? new Sales : $this->loadModel($Sales_id,
                    "Sales");
//                $docref = $Sales->doc_ref;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Sales'][$k] = $v;
                }
//                $_POST['Sales']['doc_ref'] = $docref;
                $_POST['Sales']['arus'] = 1;
                $_POST['Sales']['id_user'] = Yii::app()->user->getId();
                $_POST['Sales']['final'] = 1;
                $_POST['Sales']['lunas'] = 0;
                $Sales->attributes = $_POST['Sales'];
                if ($is_new) {
//                    $ref = new Reference($_POST['tgl']);
//                    $docref = $ref->get_next_reference(PENJUALAN);
                    $Sales->createDocRef();
                } else {
                    SalesDetails::model()->deleteAll('Sales_id  = :Sales_id', array(':Sales_id' => $Sales_id));
                }
                if (!$Sales->save())
                    throw new Exception("Gagal menyimpan Sales. " . CHtml::errorSummary($Sales));
                $ppn = 0;
                $hpp = 0;
                foreach ($detils as $detil) {
                    $Sales_detil = new SalesDetails;
                    $_POST['SalesDetails']['barang_id'] = $detil['barang_id'];
//                    $_POST['SalesDetails']['sat'] = $detil['sat'];
                    $_POST['SalesDetails']['jml'] = get_number($detil['jml']);
                    $_POST['SalesDetails']['price'] = get_number($detil['price']);
                    $_POST['SalesDetails']['disc1'] = get_number($detil['disc1']);
                    $_POST['SalesDetails']['disc2'] = get_number($detil['disc2']);
                    $_POST['SalesDetails']['disc3'] = get_number($detil['disc3']);
                    $_POST['SalesDetails']['pot1'] = get_number($detil['pot1']);
                    $_POST['SalesDetails']['pot2'] = get_number($detil['pot2']);
                    $_POST['SalesDetails']['pot3'] = get_number($detil['pot3']);
                    $_POST['SalesDetails']['totalpot'] = get_number($detil['totalpot']);
                    $_POST['SalesDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['SalesDetails']['nominal'] = get_number($detil['nominal']);
                    $_POST['SalesDetails']['ketpot'] = $detil['ketpot'];
                    $_POST['SalesDetails']['vat'] = get_number($detil['vat']);
                    $_POST['SalesDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['SalesDetails']['hpp'] = get_number($detil['jml']) * Material::getAvg($detil['barang_id']);
                    $_POST['SalesDetails']['sales_id'] = $Sales->sales_id;
                    $Sales_detil->attributes = $_POST['SalesDetails'];
                    if (!$Sales_detil->save())
                        throw new Exception("Gagal menyimpan Sales detil. " . CHtml::errorSummary($Sales_detil));
                    $hpp += $Sales_detil->hpp;
                    //                        U::add_stock_moves(Sales, $Sales->Sales_id, $Sales->tgl,
//                            $Sales_detil->barang_id, GUDANG_SMT, -$Sales_detil->pcs, $Sales->doc_ref,
//                            $Sales_detil->price);
                }
                $hpp = round($hpp);
//                if ($is_new) {
//                    $ref->save(PENJUALAN, $Sales->sales_id, $docref);
//                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
//                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate()
    {
        $msg = '';
        $status = false;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['sales_id'];
            $model = $this->loadModel($id, 'Sales');
            try {
                if ($model == null) {
                    throw new Exception('Penjualan tidak ditemukan');
                }
                if (!$model->saveAttributes(array('received' => SJ_DELIVERED))) {
                    throw new Exception(CHtml::errorSummary($model));
                }
                $status = true;
                $msg = 'Penjualan berhasil diclose.';
            } catch (Exception $e) {
                $msg = $e->getMessage();
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
//            foreach ($_POST as $k => $v) {
//                if (is_angka($v)) $v = get_number($v);
//                $_POST['Sales'][$k] = $v;
//            }
//            $msg = "Data gagal disimpan";
//            $model->attributes = $_POST['Sales'];
//            if ($model->update()) {
//                $status = true;
//                $msg = "Data berhasil di simpan dengan id " . $model->Sales_id;
//            } else {
//                $msg .= " " . CHtml::errorSummary($model);
//                $status = false;
//            }
//            if (Yii::app()->request->isAjaxRequest) {
//
//            } else {
//                $this->redirect(array('view', 'id' => $model->Sales_id));
//            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Sales')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app',
                    'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = [];
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }

        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset ($_POST['havestock'])) {
            $criteria->addCondition('nama_grup <> "NON STOCK"');
        }
        $criteria->addCondition('arus = 1');
        $criteria->params = $param;
        $model = Sales::model()->findAll($criteria);
        $total = Sales::model()->count($criteria);
        $this->renderJson($model, $total);
//        $criteria = new CDbCriteria();
//        $criteria->addCondition("salesman_id = :salesman_id");
//        $criteria->addCondition("tgl = :tgl");
//        $criteria->addCondition("total >= 0");
//        $criteria->params = array(':salesman_id' => $_POST['sales'], ':tgl' => $_POST['tgl']);
//        $model = Sales::model()->findAll($criteria);
//        $total = Sales::model()->count($criteria);
//        $this->renderJson($model, $total);
    }
    public function actionRetur()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = ReturSales::model()->findAll($criteria);
        $total = ReturSales::model()->count($criteria);
        $this->renderJson($model, $total);
//        $criteria = new CDbCriteria();
//        $criteria->addCondition("salesman_id = :salesman_id");
//        $criteria->addCondition("tgl = :tgl");
//        $criteria->addCondition("total >= 0");
//        $criteria->params = array(':salesman_id' => $_POST['sales'], ':tgl' => $_POST['tgl']);
//        $model = Sales::model()->findAll($criteria);
//        $total = Sales::model()->count($criteria);
//        $this->renderJson($model, $total);
    }
}