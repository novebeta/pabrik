<?php
class PerbaikanMesinDetailController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('perbaikan_mesin_id = :perbaikan_mesin_id');
        $criteria->params = array(':perbaikan_mesin_id' => $_POST['perbaikan_mesin_id']);
        $model = PerbaikanMesinDetail::model()->findAll($criteria);
        $total = PerbaikanMesinDetail::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}