<?php
class CustomerPaymentController extends GxController
{
    public function actionVoid($id)
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (Yii::app()->request->isPostRequest) {
            /** @var  CustomerPayment $custPay */
            $custPay = CustomerPayment::model()->findByPk($id);
            $res = $custPay->void__();
            echo CJSON::encode(array(
                'success' => $res['status'],
                'msg' => $res['msg']
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $tb_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            if (!empty($detils)) {
                app()->db->autoCommit = false;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $model = $is_new ? new CustomerPayment : $this->loadModel($tb_id, "CustomerPayment");
                    $docref = $model->doc_ref;
                    if (!$is_new) {
//                        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => CUST_PAYMENT));
//                        $docref = $systype->next_reference;
//                        if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $docref, $result) == 1) {
//                            list($all, $prefix, $year, $number) = $result;
//                            $tgl = strtotime($_POST['tgl']);
//                            $year_new = date("dmy", $tgl);
//                            if ($number == '9999999999') {
//                                $nextval = '0000000001';
//                            } else {
//                                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
//                                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
//                                $val = intval($number + 1);
//                                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
//                            }
//                            $systype->next_reference = "$prefix/$year_new/$nextval";
//                            $docref = $systype->next_reference;
//                        }
//                        if (!$systype->save()) {
//                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
//                                CHtml::errorSummary($systype));
//                        }
//                    } else {
                        foreach ($model->customerPaymentDetils as $row) {
                            if ($row->payment_tipe == 0 || $row->payment_tipe == 1) {
                                /** @var $sj Sj */
                                $sj = Sj::model()->findByPk($row->reference_item_id);
                                if ($sj == null) {
                                    throw new Exception('Fatal error, Delivery tidak ditemukan!!!');
                                }
                                if ($sj->lunas == 1) {
                                    $sj->lunas = 0;
                                    if (!$sj->save()) {
                                        throw new Exception(t('save.model.fail', 'app',
                                                array('{model}' => 'Delivery')) . CHtml::errorSummary($sj));
                                    }
                                }
                            }
                        }
                        CustomerPaymentDetil::model()->deleteAll("customer_payment_id = :customer_payment_id",
                            array(':customer_payment_id' => $tb_id));
                        GlTrans::model()->deleteAll('type = :type AND type_no = :type_no',
                            [':type' => CUST_PAYMENT, ':type_no' => $tb_id]);
                        BankTrans::model()->deleteAll('type_ = :type_ AND trans_no = :trans_no',
                            [':type_' => CUST_PAYMENT, ':trans_no' => $tb_id]);
                        Apar::model()->deleteAll('type_no = :type_no AND trans_no = :trans_no',
                            [':type_no' => CUST_PAYMENT, ':trans_no' => $tb_id]);
                    }
                    foreach ($_POST as $k => $v) {
                        if ($k == 'detil') {
                            continue;
                        }
                        if (is_angka($v)) {
                            $v = get_number($v);
                        }
                        $_POST['CustomerPayment'][$k] = $v;
                    }
                    $_POST['CustomerPayment']['doc_ref'] = $docref;
                    $model->attributes = $_POST['CustomerPayment'];
                    if ($is_new) {
                        $model->createDocRef();
                    }
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Customer Payment')) .
                            CHtml::errorSummary($model));
                    }
                    $gl = new GL();
//                     BANK - PIUTANG
                    if ($model->bank_id != null) {
                        $gl->add_gl(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $model->doc_ref, $model->bank->account_code,
                            "Customer Payment " . $model->doc_ref, "Customer Payment " . $model->doc_ref, $model->total, 0);
                    } else {
                        $gl->add_gl(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $model->doc_ref, COA_PIUTANG_GIRO,
                            "Customer Payment " . $model->doc_ref, "Customer Payment " . $model->doc_ref, $model->total, 0);
                        /** @var Giro $giro */
                        $giro = Giro::model()->findByPk($model->no_bg_cek);
                        if ($giro == null) {
                            throw new Exception('Giro tidak ditemukan.');
                        }
                        /** @var GiroSisa $giroSisa */
                        $giroSisa = GiroSisa::model()->findByAttributes([
                            'giro_id' => $giro->giro_id
                        ]);
                        if ($giroSisa == null) {
                            throw new Exception('Giro sudah habis digunakan.');
                        }
                        if ((floatval($giroSisa->total) + floatval($model->total)) > $giroSisa->saldo) {
                            throw new Exception('Giro tidak cukup.');
                        }
                        if ($giroSisa->saldo - (floatval($giroSisa->total) + floatval($model->total)) <= 0) {
//                            $giro->used = $giro->isUsed() ? 0 : 1;
                            $giro->used = 1;
                        }
                        if (!$giro->save()) {
                            throw new Exception(CHtml::errorSummary($giro));
                        }
//                        $sisa_giro = $giro->saldo - $model->total;
//                        if ($sisa_giro > 0) {
//                            $gl->add_gl(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $model->doc_ref, COA_PENDAPATAANLAIN,
//                                "Sisa Giro Customer Payment " . $model->doc_ref, "Sisa Giro Customer Payment " . $model->doc_ref, -$sisa_giro, 0);
//                        }
                    }
                    foreach ($detils as $detil) {
                        $item_details = new CustomerPaymentDetil;
                        $_POST['CustomerPaymentDetil']['reference_item_id'] = $detil['reference_item_id'];
                        $_POST['CustomerPaymentDetil']['doc_ref'] = $detil['doc_ref'];
                        $_POST['CustomerPaymentDetil']['customer_id'] = $detil['customer_id'];
                        $_POST['CustomerPaymentDetil']['remark'] = $detil['remark'];
                        $_POST['CustomerPaymentDetil']['tgl'] = $detil['tgl'];
                        $_POST['CustomerPaymentDetil']['payment_tipe'] = $detil['payment_tipe'];
                        $_POST['CustomerPaymentDetil']['kas_diterima'] = get_number($detil['kas_diterima']);
                        $_POST['CustomerPaymentDetil']['sisa'] = get_number($detil['sisa']);
                        $_POST['CustomerPaymentDetil']['uang_diterima'] = 0;//get_number($detil['uang_diterima']);
                        $_POST['CustomerPaymentDetil']['selisih_uang'] = 0;
                        $_POST['CustomerPaymentDetil']['disc'] = get_number($detil['disc']);;
                        $_POST['CustomerPaymentDetil']['lebih_bayar'] = get_number($detil['lebih_bayar']);;
                        $_POST['CustomerPaymentDetil']['uang_muka'] = get_number($detil['uang_muka']);;
                        //get_number($detil['uang_diterima'] - $detil['kas_diterima']);
                        //$_POST['CustomerPaymentDetil']['uang_diterima']- $_POST['CustomerPaymentDetil']['kas_diterima'];
                        $_POST['CustomerPaymentDetil']['customer_payment_id'] = $model->customer_payment_id;
                        $item_details->attributes = $_POST['CustomerPaymentDetil'];
                        if (!$item_details->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => 'Customer Payment item detail')) . CHtml::errorSummary($item_details));
                        }
                        if ($item_details->sisa == 0) {
                            if ($item_details->payment_tipe == 0 || $item_details->payment_tipe == 1) {
                                /** @var $sj Sj */
                                $sj = Sj::model()->findByPk($item_details->reference_item_id);
                                if ($sj == null) {
                                    throw new Exception('Fatal error, Delivery tidak ditemukan!!!');
                                }
                                $sj->lunas = 1;
                                if (!$sj->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => 'Delivery')) . CHtml::errorSummary($sj));
                                }
                            }
//                            elseif ($item_details->payment_tipe == 2) {
//                                /** @var $sj CreditNote */
//                                $sj = CreditNote::model()->findByPk($item_details->reference_item_id);
//                                if ($sj == null) {
//                                    throw new Exception('Fatal error, Customer Credit Note tidak ditemukan!!!');
//                                }
//                                $sj->lunas = 1;
//                                if (!$sj->save()) {
//                                    throw new Exception(t('save.model.fail', 'app',
//                                            array('{model}' => 'Customer Credit Note')) . CHtml::errorSummary($sj));
//                                }
//                            }
//                            elseif ($item_details->payment_tipe == 3) {
//                                /** @var $sj Claim */
//                                $sj = Claim::model()->findByPk($item_details->reference_item_id);
//                                if ($sj == null) {
//                                    throw new Exception('Fatal error, Customer Claim tidak ditemukan!!!');
//                                }
//                                $sj->lunas = 1;
//                                if (!$sj->save()) {
//                                    throw new Exception(t('save.model.fail', 'app',
//                                            array('{model}' => 'Customer Credit Note')) . CHtml::errorSummary($sj));
//                                }
//                            }
                            else {
                                throw new Exception('Bugs found!!!');
                            }
                        }
                        Apar::addPiutang(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $item_details->customer_id,
                            $model->doc_ref, -($item_details->kas_diterima), $item_details->reference_item_id);
                        $gl->add_gl(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $model->doc_ref, COA_UANGMUKA,
                            "Uang Muka " . $model->doc_ref, "Customer Payment " . $model->doc_ref,
                            $item_details->uang_muka, 1);
                        $gl->add_gl(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $model->doc_ref, COA_DISKJUAL,
                            "Disk " . $model->doc_ref, "Customer Payment " . $model->doc_ref,
                            $item_details->disc, 1);
                        $gl->add_gl(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $model->doc_ref, COA_PIUTANG,
                            "Piutang " . $model->doc_ref, "Customer Payment " . $model->doc_ref,
                            -$item_details->uang_diterima, 1);
                        $gl->add_gl(CUST_PAYMENT, $model->customer_payment_id, $model->tgl, $model->doc_ref, COA_PENDAPATAANLAIN,
                            "Pendapatan Lain " . $model->doc_ref, "Customer Payment " . $model->doc_ref,
                            -$item_details->lebih_bayar, 1);
                    }
                    $gl->validate();
                    $transaction->commit();
                    $msg = t('save.success', 'app');
                    $status = true;
                } catch
                (Exception $ex) {
                    $transaction->rollback();
                    $status = false;
                    $msg = $ex->getMessage();
                }
            } else {
                $msg = "Data Belum Lengkap";
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdateTgl()
    {
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['id'];
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var CustomerPayment $model */
                $model = CustomerPayment::model()->findByPk($id);
                if ($_POST['tgl'] != $model->tgl) {
                    $tgl_old = sql2date($model->tgl, 'MMyy');
                    $tgl_new = sql2date($_POST['tgl'], 'MMyy');
                    $ganti_bulan = $tgl_old != $tgl_new;
                    $model->updateTgl($_POST['tgl'], $ganti_bulan);
                    if ($ganti_bulan) {
                        /** @var CustomerPayment[] $customerPayments */
                        $customerPayments = CustomerPayment::model()->findAll("DATE_FORMAT(tgl,'%m%y') = :tgl",
                            [':tgl' => $tgl_old]);
                        foreach ($customerPayments as $s) {
                            $s->updateTgl($s->tgl, true);
                        }
                    }
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        /** @var CustomerPayment $model */
        $model = $this->loadModel($id, 'CustomerPayment');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['CustomerPayment'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['CustomerPayment'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->customer_payment_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->customer_payment_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'CustomerPayment')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
//    if (isset($_POST['limit'])) {
//        $limit = $_POST['limit'];
//    } else {
//        $limit = 20;
//    }
//    if (isset($_POST['start'])) {
//        $start = $_POST['start'];
//    } else {
//        $start = 0;
//    }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
            $criteria->addCondition('DATE_FORMAT(tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
            $param[':tgl'] = $_POST['tgl'];
        }
//    if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//        (isset($_POST['limit']) && isset($_POST['start']))
//    ) {
//        $criteria->limit = $limit;
//        $criteria->offset = $start;
//
//    }
        $criteria->order = 'tgl DESC, doc_ref DESC';
        $criteria->params = $param;
//        $model = Customerpaymentext::model()->findAll($criteria);
//        $total = Customerpaymentext::model()->count($criteria);
//        $model = CustomerPayment::model()->findAll($criteria);
//        $total = CustomerPayment::model()->count($criteria);
        $model = CustomerPaymentView::model()->findAll($criteria);
        $total = CustomerPaymentView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}