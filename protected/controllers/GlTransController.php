<?php
Yii::import('application.components.GL');
class GlTransController extends GxController
{
    public function actionCreate()
    {
        if (!app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $msg = t('save.success', 'app');
            $detils = CJSON::decode($_POST['detil']);
            $is_new = $_POST['mode'] == 0;
            app()->db->autoCommit = false;
            $transaction = app()->db->beginTransaction();
            try {
                $gl = new GL();
                if ($is_new) {
                    /*
//                    $ref = new Reference;
//                    $docref = $ref->get_next_reference(JURNAL_UMUM);
                    $jurnal_umum_id = GlTrans::generete_primary_key(JURNAL_UMUM);
                    
                    $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => JURNAL_UMUM));
                    $docref = $systype->next_reference;
                    if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $docref, $result) == 1) {
                        list($all, $prefix, $year, $number) = $result;
                        $tgl = strtotime($_POST['tran_date']);
                        $year_new = date("dmy", $tgl);
                        if ($number == '9999999999') {
                            $nextval = '0000000001';
                        } else {
                            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                            $val = intval($number + 1);
                            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
                        }
                        $systype->next_reference = "$prefix/$year_new/$nextval";
                        $docref = $systype->next_reference;
                    }
                    if (!$systype->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                            CHtml::errorSummary($systype));
                    }*/
                    $query = GlTrans::model()->dbConnection->createCommand("SELECT UUID();");
                    $jurnal_umum_id = $query->queryScalar();
                    $docref = Reference::createDocumentReference(JURNAL_UMUM, $_POST['tran_date'], "dmy");
                } else {
                    $docref = GlTrans::get_reference(JURNAL_UMUM, $_POST['id']);
                    $this->delete_gl_trans(JURNAL_UMUM, $_POST['id']);
                    $jurnal_umum_id = $_POST['id'];
                }
                foreach ($detils as $detil) {
                    $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                    $gl->add_gl(
                        JURNAL_UMUM,
                        $jurnal_umum_id,
                        $_POST['tran_date'],
                        $docref,
                        $detil['account_code'],
                        $detil["memo_"],
                        '',
                        $amount,
                        0
                    );
                }
                $gl->validate();
//                if ($is_new) {
//                    $ref->save(JURNAL_UMUM, $jurnal_umum_id, $docref);
//                }
                $gl->update_reference(JURNAL_UMUM, $jurnal_umum_id, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            app()->end();
        }
    }
    public function actionIndex()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $this->renderJsonArr(GlTrans::jurnal_umum_details($_POST['type_no']));
        }
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
//        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
//        $model = GlTrans::model()->findAll($criteria);
//        $total = GlTrans::model()->count($criteria);
//        $this->renderJson($model, $total);
    }
    public function actionListJurnalUmum()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $this->renderJsonArr(GlTrans::jurnal_umum_index($limit, $start, $_POST['tgl']));
    }
    public function actionDelete()
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $this->delete_gl_trans(JURNAL_UMUM, $_POST['type_no']);
//                $this->delete_refs(JURNAL_UMUM, $_POST['type_no']);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg)
            );
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}