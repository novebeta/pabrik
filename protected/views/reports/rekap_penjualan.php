<h1>Rekap Penjualan</h1>
<table>
    <tbody>
    <tr>
        <td>Date</td>
        <td> : <?= $from ?> to <?= $to ?></td>
    </tr>
    <tr>
        <td>Customers</td>
        <td> : <?= $customer_name ?></td>
    </tr>
    </tbody>
</table>
<?
$this->pageTitle = 'Rekap Penjualan';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Kode Material',
            'name' => 'kode_material'
        ),
        array(
            'header' => 'Nama Material',
            'name' => 'nama_material'
        ),
        array(
            'header' => 'Qty',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Nilai',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    ),
));