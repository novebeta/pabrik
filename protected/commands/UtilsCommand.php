<?php
class UtilsCommand extends CConsoleCommand
{
    public function generate_uuid()
    {
        $command = Yii::app()->db->createCommand("SELECT UUID();");
        return $command->queryScalar();
    }
    public function actionSentEmail($to, $from, $fromAlias, $subject, $message, $html = false)
    {
//        file_put_contents('sentMail', $message);
        $path = base64_decode($message);
        $message = file_get_contents($path);
        try {
            /** @var PHPMailer $mail */
            $mail = Yii::app()->Smtpmail;
            $mail->SetFrom($from, ($fromAlias == null ? $from : $fromAlias));
            $mail->Subject = $subject;
            if ($html) {
                $mail->MsgHTML($message);
            } else {
                $mail->Body = $message;
            }
            $mail->ClearAddresses();
            $mail->ClearAttachments();
            $mail->AddAddress($to);
            if (!$mail->Send()) {
                return var_export($mail->ErrorInfo, true);
            } else {
                return 'OK';
            }
        } catch (Exception $ex) {
            var_export($ex->getMessage(), true);
        }
        unlink($path);
    }
    public function actionReorderAssembly($from, $to, $year)
    {
        for ($i = $from; $i <= $to; $i++) {
            echo ">> proses bulan $i \n";
            Assembly::urutDocRef($i, $year);
        }
        echo ">> Selesai.. \n";
    }
    public function actionReorderKas($arus, $from, $to, $year)
    {
        for ($i = $from; $i <= $to; $i++) {
            echo ">> proses bulan $i \n";
            Kas::urutDocRef($arus,$i, $year);
        }
        echo ">> Selesai.. \n";
    }
    public function actionReorderSj($from, $to, $year)
    {
        for ($i = $from; $i <= $to; $i++) {
            echo ">> proses bulan $i \n";
            Sj::urutDocRef($i, $year);
        }
        echo ">> Selesai.. \n";
    }
    public function actionReorderCustPay($from, $to, $year)
    {
        for ($i = $from; $i <= $to; $i++) {
            echo ">> proses bulan $i \n";
            CustomerPayment::urutDocRef($i, $year);
        }
        echo ">> Selesai.. \n";
    }
    public function actionReorderSales($arus, $from, $to, $year)
    {
        if($arus > 0){
            for ($i = $from; $i <= $to; $i++) {
                echo ">> proses bulan $i \n";

                Sales::urutDocRef($i, $year);
            }
        }else{
            for ($i = $from; $i <= $to; $i++) {
                echo ">> proses bulan $i \n";

                Sales::urutReturDocRef($i, $year);
            }
        }

        echo ">> Selesai.. \n";
    }
}