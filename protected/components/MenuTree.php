<?php
class MenuTree
{
    var $security_role;
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
    }
    Public static function is_purchasing()
    {
        $user = Users::model()->findByPk(user()->getId());
        $role = SecurityRoles::model()->findByPk($user->security_roles_id);
        return strpos($role->sections, '222') !== FALSE ? 1 : 0;
    }
    function getPustakaQA($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Dokumen QA',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildPustakaQA()
    {
        $child = "";
        $child .= in_array(500, $this->security_role) ? "{
                            text: 'Dokumen',
                            id: 'jun.PustakaQA',
                            leaf: true
                        }," : '';
        $child .= in_array(501, $this->security_role) ? "{
                            text: 'Jenis Dokumen',
                            id: 'jun.PustakaQAtipeGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(502, $this->security_role) ? "{
                            text: 'Kegiatan',
                            id: 'jun.PustakaQAactivityGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(503, $this->security_role) ? "{
                            text: 'Kategori',
                            id: 'jun.PustakaQAcategoryGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(504, $this->security_role) ? "{
                            text: 'Level',
                            id: 'jun.PustakaQAlevelGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(505, $this->security_role) ? "{
                            text: 'Masterlist Dokumen Internal',
                            id: 'jun.ReportMasterListPustakaQA',
                            leaf: true
                        }," : '';
        return $child;
    }
    public function get_menu()
    {
        $data = array();
        $master = self::getMaster(self::getChildMaster());
        if (!empty($master)) {
            $data[] = $master;
        }
        $trans = self::getTransaction(self::getChildTransaction());
        if (!empty($trans)) {
            $data[] = $trans;
        }
        $report = self::getReport(self::getChildReport());
        if (!empty($report)) {
            $data[] = $report;
        }
        $adm = self::getAdministration(self::getChildAdministration());
        if (!empty($adm)) {
            $data[] = $adm;
        }
        $username = Yii::app()->user->name;
//        if (in_array(000, $this->security_role)) {
        $data[] = array(
            'text' => 'Change Password',
            'id' => 'jun.PasswordWin',
            'leaf' => true
        );
//        }
//        if (in_array(001, $this->security_role)) {
        $data[] = array(
            'text' => "Logout ($username)",
            'id' => 'logout',
            'leaf' => true
        );
//        }
        return CJSON::encode($data);
//        $username = Yii::app()->user->name;
//        $data = "[";
//        $data .= self::getMaster(self::getChildMaster());
//        $data .= self::getTransaction(self::getChildTransaction());
//        $data .= self::getReport(self::getChildReport());
//        $data .= self::getPustakaQA(self::getChildPustakaQA());
//        $data .= self::getAdministration(self::getChildAdministration());
//        $data .= self::getGeneral();
//        $data .= "]";
//        return $data;
    }
    function getMaster($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Master',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildMaster()
    {
        $child = array();
        if (in_array(101, $this->security_role)) {
            $child[] = array(
                'text' => 'Group',
                'id' => 'jun.GrupGrid',
                'leaf' => true
            );
        }
        if (in_array(102, $this->security_role)) {
            $child[] = array(
                'text' => 'Barang',
                'id' => 'jun.BarangGrid',
                'leaf' => true
            );
        }
        if (in_array(115, $this->security_role)) {
            $child[] = array(
                'text' => 'Tipe Material',
                'id' => 'jun.TipeMaterialGrid',
                'leaf' => true
            );
        }
        if (in_array(103, $this->security_role)) {
            $child[] = array(
                'text' => 'Material',
                'id' => 'jun.MaterialGrid',
                'leaf' => true
            );
        }
//        $child .= in_array(104, $this->security_role) ? "{
//                            text: 'Packaging Material',
//                            id: 'jun.PackMaterialGrid',
//                            leaf: true
//                        }," : '';
        if (in_array(105, $this->security_role)) {
            $child[] = array(
                'text' => 'Bill Of Material',
                'id' => 'jun.BomGrid',
                'leaf' => true
            );
        }
        if (in_array(106, $this->security_role)) {
            $child[] = array(
                'text' => 'Kas/Bank',
                'id' => 'jun.BankGrid',
                'leaf' => true
            );
        }
//        $child .= in_array(107, $this->security_role) ? "{
//                            text: 'Chart Of Account Class',
//                            id: 'jun.ChartClassGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(107, $this->security_role) ? "{
//                            text: 'Chart Of Account Type',
//                            id: 'jun.ChartTypesGrid',
//                            leaf: true
//                        }," : '';
        if (in_array(107, $this->security_role)) {
            $child[] = array(
                'text' => 'Chart Of Account',
                'id' => 'jun.DesignLabaRugiGrid',
                'leaf' => true
            );
        }
        if (in_array(108, $this->security_role)) {
            $child[] = array(
                'text' => 'Wilayah',
                'id' => 'jun.WilIntGrid',
                'leaf' => true
            );
        }
//        if (in_array(109, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Wilayah Pajak',
//                'id' => 'jun.WilTaxGrid',
//                'leaf' => true
//            );
//        }
        if (in_array(110, $this->security_role)) {
            $child[] = array(
                'text' => 'Harga Jual',
                'id' => 'jun.PriceIntGrid',
                'leaf' => true
            );
        }
//        if (in_array(111, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Harga Pajak',
//                'id' => 'jun.PriceTaxGrid',
//                'leaf' => true
//            );
//        }
        if (in_array(112, $this->security_role)) {
            $child[] = array(
                'text' => 'Customer',
                'id' => 'jun.CustomersGrid',
                'leaf' => true
            );
        }
        if (in_array(113, $this->security_role)) {
            $child[] = array(
                'text' => 'Supplier',
                'id' => 'jun.SupplierGrid',
                'leaf' => true
            );
        }
        if (in_array(114, $this->security_role)) {
            $child[] = array(
                'text' => 'Deposit Faktur Pajak',
                'id' => 'jun.DepositFakturPajakGrid',
                'leaf' => true
            );
        }
        if (in_array(116, $this->security_role)) {
            $child[] = array(
                'text' => 'Salesman',
                'id' => 'jun.SalesmanGrid',
                'leaf' => true
            );
        }
        if (in_array(117, $this->security_role)) {
            $child[] = array(
                'text' => 'Gudang',
                'id' => 'jun.StorageLocationGrid',
                'leaf' => true
            );
        }
        if (in_array(118, $this->security_role)) {
            $child[] = array(
                'text' => 'Merk',
                'id' => 'jun.MerkGrid',
                'leaf' => true
            );
        }
        return $child;
    }
    function getTransaction($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Transaction',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildTransaction()
    {
        $child = $sales = $purchase = $finance = $manufaktur = $stok = array();
        if (in_array(201, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Purchase Request',
                'id' => 'jun.PurchaseRequisitionGrid',
                'leaf' => true
            );
        }
        if (in_array(202, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Purchase Order',
                'id' => 'jun.PurchaseOrderGrid',
                'leaf' => true
            );
        }
//        if (in_array(204, $this->security_role)) {
//            $purchase[] = array(
//                'text' => 'Penerimaan Barang',
//                'id' => 'jun.TerimaBarangGrid',
//                'leaf' => true
//            );
//        }
        if (in_array(203, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Surat Jalan Supplier',
//                'id' => 'jun.TerimaMaterialGrid',
                'id' => 'jun.TerimaBarangGrid',
                'leaf' => true
            );
        }
        if (in_array(204, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Supplier Invoices',
                'id' => 'jun.SupplierInvoiceGrid',
                'leaf' => true
            );
        }
        if (in_array(205, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Pembayaran Supplier',
//                'id' => 'jun.PembayaranSupplierGrid',
                'id' => 'jun.SupplierPaymentGrid',
                'leaf' => true
            );
        }
        if (in_array(206, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Return Pembelian',
                'id' => 'jun.ReturnPembelianGrid',
                'leaf' => true
            );
        }
        if (in_array(207, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Nota Return',
                'id' => 'jun.NotaReturnGrid',
                'leaf' => true
            );
        }
        if (in_array(208, $this->security_role)) {
            $purchase[] = array(
                'text' => 'Tanda Terima Material',
                'id' => 'jun.TandaTerimaMaterialGrid',
                'leaf' => true
            );
        }
//        if (in_array(207, $this->security_role)) {
//            $purchase[] = array(
//                'text' => 'Retur Beli',
//                'id' => 'jun.SjsimGrid',
//                'leaf' => true
//            );
//        }
        if (!empty($purchase)) {
            $child[] = array(
                'text' => 'Pembelian',
                'expanded' => false,
                'children' => $purchase
            );
        }
        if (in_array(209, $this->security_role)) {
            $manufaktur[] = array(
                'text' => 'Production Order',
                'id' => 'jun.BatchGrid',
                'leaf' => true
            );
        }
        if (in_array(233, $this->security_role)) {
            $manufaktur[] = array(
                'text' => 'Assembly',
                'id' => 'jun.FolderAssemblyGrid',
                'leaf' => true
            );
        }
        if (in_array(234, $this->security_role)) {
            $manufaktur[] = array(
                'text' => 'Breakdown',
                'id' => 'jun.FolderBreakdownGrid',
                'leaf' => true
            );
        }
        if (in_array(235, $this->security_role)) {
            $manufaktur[] = array(
                'text' => 'Konversi Material',
                'id' => 'jun.KonversiMaterialGrid',
                'leaf' => true
            );
        }
        if (in_array(210, $this->security_role)) {
            $manufaktur[] = array(
                'text' => 'Transfer Barang',
                'id' => 'jun.TransferMaterialGrid',
                'leaf' => true
            );
        }
        if (in_array(239, $this->security_role)) {
            $manufaktur[] = array(
                'text' => 'Kelola Persediaan',
                'id' => 'jun.KelolaStokGrid',
                'leaf' => true
            );
        }
        if (!empty($manufaktur)) {
            $child[] = array(
                'text' => 'Produksi',
                'expanded' => false,
                'children' => $manufaktur
            );
        }
//        $child .= in_array(205, $this->security_role) ? "{
//                            text: 'Surat Jalan Supplier',
//                            id: 'jun.TerimaMaterialGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(208, $this->security_role) ? "{
//                            text: 'Retur Beli Raw Material',
//                            id: 'jun.ReturnBeliRawGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(209, $this->security_role) ? "{
//                            text: 'Retur Beli Packaging Material',
//                            id: 'jun.ReturnBeliPackGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(210, $this->security_role) ? "{
//                            text: 'Tanda Terima Raw Material',
//                            id: 'jun.TandaTerimaRawGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(211, $this->security_role) ? "{
//                            text: 'Tanda Terima Packaging Material',
//                            id: 'jun.TandaTerimaPackGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(212, $this->security_role) ? "{
//                            text: 'Barang Keluar Raw Material',
//                            id: 'jun.BarangKeluarRawGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(213, $this->security_role) ? "{
//                            text: 'Barang Keluar Packaging Material',
//                            id: 'jun.BarangKeluarPackGrid',
//                            leaf: true
//                        }," : '';
        if (in_array(220, $this->security_role)) {
            $sales[] = array(
                'text' => 'Penjualan',
                'id' => 'jun.PenjualanKreditGrid',
                'leaf' => true
            );
        }
        if (in_array(240, $this->security_role)) {
            $sales[] = array(
                'text' => 'Retur Penjualan',
                'id' => 'jun.ReturPenjualanKreditGrid',
                'leaf' => true
            );
        }
        if (in_array(211, $this->security_role)) {
            $sales[] = array(
                'text' => 'Surat Jalan',
                'id' => 'jun.SjGrid',
                'leaf' => true
            );
        }
        if (in_array(212, $this->security_role)) {
            $sales[] = array(
                'text' => 'Cetak Invoice',
                'id' => 'jun.SjCetakInvGrid',
                'leaf' => true
            );
        }
//        $child .= in_array(102, $this->security_role) ? "{
//                            text: 'Surat Jalan Pajak',
//                            id: 'jun.SjtaxGrid',
//                            leaf: true
//                        }," : '';
        if (in_array(213, $this->security_role)) {
            $sales[] = array(
                'text' => 'Credit Note',
                'id' => 'jun.CreditNoteGrid',
                'leaf' => true
            );
        }
        if (in_array(241, $this->security_role)) {
            $sales[] = array(
                'text' => 'Giro',
                'id' => 'jun.GiroGrid',
                'leaf' => true
            );
        }
        if (in_array(236, $this->security_role)) {
            $sales[] = array(
                'text' => 'Pembayaran Customers',
                'id' => 'jun.CustomerPaymentGrid',
                'leaf' => true
            );
        }
        if (in_array(237, $this->security_role)) {
            $sales[] = array(
                'text' => 'Service Masuk',
                'id' => 'jun.ServiceGrid',
                'leaf' => true
            );
        }
        if (in_array(238, $this->security_role)) {
            $sales[] = array(
                'text' => 'Service Keluar',
                'id' => 'jun.ServiceOutGrid',
                'leaf' => true
            );
        }
        if (!empty($sales)) {
            $child[] = array(
                'text' => 'Penjualan',
                'expanded' => false,
                'children' => $sales
            );
        }
        if (in_array(214, $this->security_role)) {
            $stok[] = array(
                'text' => 'Transfer Barang',
                'id' => 'jun.TransferBarangGrid',
                'leaf' => true
            );
        }
        if (!empty($stok)) {
            $child[] = array(
                'text' => 'Stok Manajemen',
                'expanded' => false,
                'children' => $stok
            );
        }
//        $child .= in_array(102, $this->security_role) ? "{
//                            text: 'Audit',
//                            id: 'jun.Sales2AuditWin',
//                            leaf: true
//                        }," : '';
        if (in_array(215, $this->security_role)) {
            $finance[] = array(
                'text' => 'Kas/Bank Masuk',
                'id' => 'jun.KasGridPusat',
                'leaf' => true
            );
        }
        if (in_array(216, $this->security_role)) {
            $finance[] = array(
                'text' => 'Kas/Bank Keluar',
                'id' => 'jun.KasGridPusatOut',
                'leaf' => true
            );
        }
        if (in_array(217, $this->security_role)) {
            $finance[] = array(
                'text' => 'Kas/Bank Transfer',
                'id' => 'jun.BankTransGrid',
                'leaf' => true
            );
        }
        if (in_array(218, $this->security_role)) {
            $finance[] = array(
                'text' => 'Generate Profit Lost',
                'id' => 'jun.GenerateLabaRugi',
                'leaf' => true
            );
        }
        if (in_array(219, $this->security_role)) {
            $finance[] = array(
                'text' => 'General Journal',
                'id' => 'jun.JurnalUmum',
                'leaf' => true
            );
        }
        if (!empty($finance)) {
            $child[] = array(
                'text' => 'Akuntansi',
                'expanded' => false,
                'children' => $finance
            );
        }
//        if (in_array(223, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Sampel QC',
//                'id' => 'jun.SampelQcGrid',
//                'leaf' => true
//            );
//        }
//        $child .= in_array(229, $this->security_role) ? "{
//                            text: 'Laporan FA',
//                            id: 'jun.FinAccReportGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(230, $this->security_role) ? "{
//                            text: 'Perbaikan Mesin/Utility',
//                            id: 'jun.PerbaikanMesinGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(231, $this->security_role) ? "{
//                            text: 'Tanda Terima Sparepart',
//                            id: 'jun.SparepartMasukGrid',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(232, $this->security_role) ? "{
//                            text: 'Konversi Sparepart > Barang Bekas',
//                            id: 'jun.ConversiMaterialBarkasGrid',
//                            leaf: true
//                        }," : '';
        return $child;
    }
    function getReport($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Report',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildReport()
    {
        $child = array();
//        if (in_array(301, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Mutasi Stok',
//                'id' => 'jun.ReportInventoryMovements',
//                'leaf' => true
//            );
//        }
        if (in_array(302, $this->security_role)) {
            $child[] = array(
                'text' => 'Mutasi Stok',
                'id' => 'jun.ReportInventoryMovementsMaterial',
                'leaf' => true
            );
        }
//        if (in_array(304, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Kartu Stok',
//                'id' => 'jun.ReportInventoryCard',
//                'leaf' => true
//            );
//        }
//        $child .= in_array(303, $this->security_role) ? "{
//                            text: 'Mutasi Stok (Packaging)',
//                            id: 'jun.ReportInventoryMovementsPackMaterial',
//                            leaf: true
//                        }," : '';
        if (in_array(305, $this->security_role)) {
            $child[] = array(
                'text' => 'Kartu Stok',
                'id' => 'jun.ReportInventoryCardMaterial',
                'leaf' => true
            );
        }
        if (in_array(306, $this->security_role)) {
            $child[] = array(
                'text' => 'Laporan Produksi',
                'id' => 'jun.ReportProduksi',
                'leaf' => true
            );
        }
        if (in_array(307, $this->security_role)) {
            $child[] = array(
                'text' => 'Barang Keluar',
                'id' => 'jun.ReportBarangKeluar',
                'leaf' => true
            );
        }
        if (in_array(308, $this->security_role)) {
            $child[] = array(
                'text' => 'Penerimaan Barang Detail',
                'id' => 'jun.ReportTerimaBarangDetails',
                'leaf' => true
            );
        }
        if (in_array(309, $this->security_role)) {
            $child[] = array(
                'text' => 'Retur Beli Detail',
                'id' => 'jun.ReportReturBeliDetails',
                'leaf' => true
            );
        }
        if (in_array(310, $this->security_role)) {
            $child[] = array(
                'text' => 'Surat Jalan Internal Detail',
                'id' => 'jun.ReportSJDetails',
                'leaf' => true
            );
        }
        if (in_array(311, $this->security_role)) {
            $child[] = array(
                'text' => 'Invoice Internal Detail',
                'id' => 'jun.ReportINVDetails',
                'leaf' => true
            );
        }
        if (in_array(312, $this->security_role)) {
            $child[] = array(
                'text' => 'Surat Jalan Pajak Detail',
                'id' => 'jun.ReportSJTAXDetails',
                'leaf' => true
            );
        }
        if (in_array(313, $this->security_role)) {
            $child[] = array(
                'text' => 'Invoice Pajak Detail',
                'id' => 'jun.ReportINVTAXDetails',
                'leaf' => true
            );
        }
        if (in_array(314, $this->security_role)) {
            $child[] = array(
                'text' => 'Credit Note Detail',
                'id' => 'jun.ReportCreditNoteDetails',
                'leaf' => true
            );
        }
        if (in_array(315, $this->security_role)) {
            $child[] = array(
                'text' => 'Stok Produksi',
                'id' => 'jun.ReportStokProduksi',
                'leaf' => true
            );
        }
        if (in_array(316, $this->security_role)) {
            $child[] = array(
                'text' => 'General Ledger',
                'id' => 'jun.ReportGeneralLedger',
                'leaf' => true
            );
        }
        if (in_array(317, $this->security_role)) {
            $child[] = array(
                'text' => 'General Journal',
                'id' => 'jun.ReportGeneralJournal',
                'leaf' => true
            );
        }
        if (in_array(318, $this->security_role)) {
            $child[] = array(
                'text' => 'Profit Lost',
                'id' => 'jun.ReportLabaRugi',
                'leaf' => true
            );
        }
        if (in_array(319, $this->security_role)) {
            $child[] = array(
                'text' => 'Neraca',
                'id' => 'jun.ReportNeraca',
                'leaf' => true
            );
        }
        if (in_array(320, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekening Koran',
                'id' => 'jun.ReportRekeningKoran',
                'leaf' => true
            );
        }
        if (in_array(321, $this->security_role)) {
            $child[] = array(
                'text' => 'E-Faktur',
                'id' => 'jun.ReportEFaktur',
                'leaf' => true
            );
        }
        if (in_array(322, $this->security_role)) {
            $child[] = array(
                'text' => 'List Invoice',
                'id' => 'jun.ReportListInvoice',
                'leaf' => true
            );
        }
        if (in_array(331, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekap Invoice Hutang',
                'id' => 'jun.ReportRekapTagihanHutang',
                'leaf' => true
            );
        }
        if (in_array(332, $this->security_role)) {
            $child[] = array(
                'text' => 'Rincian Pembelian',
                'id' => 'jun.ReportRincianPembelian',
                'leaf' => true
            );
        }
        if (in_array(323, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekap Invoice Piutang',
                'id' => 'jun.ReportRekapTagihan',
                'leaf' => true
            );
        }
        if (in_array(324, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekap Purchase Order',
                'id' => 'jun.ReportPurchaseOrder',
                'leaf' => true
            );
        }
        if (in_array(325, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekap Surat Jalan Supplier',
                'id' => 'jun.ReportTerimaMaterial',
                'leaf' => true
            );
        }
        if (in_array(326, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekap Invoice Supplier',
                'id' => 'jun.ReportSupplierInvoice',
                'leaf' => true
            );
        }
        if (in_array(327, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekap FPT',
                'id' => 'jun.ReportPembayaranSupplier',
                'leaf' => true
            );
        }
        if (in_array(328, $this->security_role)) {
            $child[] = array(
                'text' => 'Rekap Pembelian',
                'id' => 'jun.ReportPembelian',
                'leaf' => true
            );
        }
        if (in_array(329, $this->security_role)) {
            $child[] = array(
                'text' => 'Transfer Material',
                'id' => 'jun.ReportTransferMaterial',
                'leaf' => true
            );
        }
        if (in_array(330, $this->security_role)) {
            $child[] = array(
                'text' => 'Transfer Barang',
                'id' => 'jun.ReportTransferBarang',
                'leaf' => true
            );
        }
        if (in_array(333, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Giro',
                'id' => 'jun.ReportGiro',
                'leaf' => true
            );
        }
        if (in_array(334, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Buku Piutang',
                'id' => 'jun.ReportBukuPiutang',
                'leaf' => true
            );
        }
        if (in_array(335, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Buku Hutang',
                'id' => 'jun.ReportBukuHutang',
                'leaf' => true
            );
        }
        /* * 
         */
        /*
        $child = "{
                    text: 'Mutasi Stok',
                    id: 'jun.ReportInventoryMovements',
                    leaf: true
                },
                    {
                        text: 'Mutasi Stok (Raw Material)',
                        id: 'jun.ReportInventoryMovementsRawMaterial',
                        leaf: true
                    },
                    {
                        text: 'Mutasi Stok (Packaging)',
                        id: 'jun.ReportInventoryMovementsPackMaterial',
                        leaf: true
                    },
                {
                    text: 'Kartu Stok',
                    id: 'jun.ReportInventoryCard',
                    leaf: true
                },
                    {
                        text: 'Kartu Stok (Raw Material)',
                        id: 'jun.ReportInventoryCardRawMaterial',
                        leaf: true
                    },
                    {
                        text: 'Kartu Stok (Packaging)',
                        id: 'jun.ReportInventoryCardPackMaterial',
                        leaf: true
                    },
                {
                    text: 'Barang Keluar',
                    id: 'jun.ReportBarangKeluar',
                    leaf: true
                },
                {
                    text: 'Penerimaan Barang Detail',
                    id: 'jun.ReportTerimaBarangDetails',
                    leaf: true
                },
                {
                    text: 'Retur Beli Detail',
                    id: 'jun.ReportReturBeliDetails',
                    leaf: true
                },
                {
                    text: 'Surat Jalan Internal Detail',
                    id: 'jun.ReportSJDetails',
                    leaf: true
                },
                {
                    text: 'Invoice Internal Detail',
                    id: 'jun.ReportINVDetails',
                    leaf: true
                },
                {
                    text: 'Surat Jalan Pajak Detail',
                    id: 'jun.ReportSJTAXDetails',
                    leaf: true
                },
                {
                    text: 'Invoice Pajak Detail',
                    id: 'jun.ReportINVTAXDetails',
                    leaf: true
                },
                {
                    text: 'Credit Note Detail',
                    id: 'jun.ReportCreditNoteDetails',
                    leaf: true
                },";
         * 
         */
        return $child;
    }
//    function getGeneral()
//    {
//        $username = Yii::app()->user->name;
//        $child = "";
//        $child .= in_array(001, $this->security_role) ? "{
//                            text: 'Change Password',
//                            id: 'jun.PasswordWin',
//                            leaf: true
//                        }," : '';
//        $child .= in_array(001, $this->security_role) ? "{
//                            text: 'Logout ($username)',
//                            id: 'logout',
//                            leaf: true
//                        }," : '';
//        return $child;
//    }
    function getAdministration($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Administration',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildAdministration()
    {
        $child = array();
        if (in_array(401, $this->security_role)) {
            $child[] = array(
                'text' => 'User Management',
                'id' => 'jun.UsersGrid',
                'leaf' => true
            );
        }
        if (in_array(402, $this->security_role)) {
            $child[] = array(
                'text' => 'Security Roles',
                'id' => 'jun.SecurityRolesGrid',
                'leaf' => true
            );
        }
//        if (in_array(402, $this->security_role) && !Users::is_audit()) {
//            $child[] = array(
//                'text' => 'Backup / Restore',
//                'id' => 'jun.BackupRestoreWin',
//                'leaf' => true
//            );
//        }
//        if (in_array(403, $this->security_role) && !Users::is_audit()) {
//            $child[] = array(
//                'text' => 'Import',
//                'id' => 'jun.ImportXlsx',
//                'leaf' => true
//            );
//        }
//        if (in_array(404, $this->security_role) && !Users::is_audit()) {
//            $child[] = array(
//                'text' => 'Setting Client',
//                'id' => 'jun.SettingsClientsGrid',
//                'leaf' => true
//            );
//        }
        return $child;
    }
}
