<?php
/**
 * Created by novebeta.
 * Date: 9/23/12
 * Time: 12:03 PM
 */
class Reference
{
    private $tgl;
    function __construct($tgl)
    {
        $this->tgl = $tgl;
    }
    static function createDocumentReference($type, $date, $format)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $type));
        $docref = $systype->next_reference;
        if (preg_match('/^(\D*?)\/\d{4}(\d{2})\/(\d+)/', $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date($format, $tgl);
            if ($year != date("y", $tgl) || $number == '9999999999') {
                $nextval = '0000000001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $systype->next_reference = "$prefix/$year_new/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReferenceNonProduk($type, $date)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $type));
        $docref = $systype->next_reference;
        if (preg_match('/^(\D*)\/(\D*)\/(\D*)\/\d+(\d{2})\/(\d+)$/', $docref, $result) == 1) {
            list($all, $prefix, $pbu, $infix, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("01my", $tgl);
            if ($year != date("y", $tgl) || $number == '999') {
                $nextval = '001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $systype->next_reference = "$prefix/$pbu/$infix/$year_new/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_PO($tipe_material, $date)
    {
        switch ($tipe_material) {
            case MAT_RAW_MATERIAL:
                $po = PO_RAW_MATERIAL;
                break;
            case MAT_PACKAGING:
                $po = PO_PACKAGING;
                break;
            case MAT_GENERAL:
            case MAT_SPAREPART:
                $po = PO_GENERAL;
                break;
        }
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $po));
        $docref = $systype->next_reference;
        if (preg_match('/^(\D*-)(\D*)\/(\d{3})\/(\d{2})/', $docref, $result) == 1) {
            list($all, $prefix, $infix, $number, $year) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '999') {
                $nextval = '001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $systype->next_reference = "$prefix$infix/$nextval/$year_new";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_PR($tipe_material, $date)
    {
        switch ($tipe_material) {
            case MAT_RAW_MATERIAL:
                $pr = PR_RAW_MATERIAL;
                break;
            case MAT_PACKAGING:
                $pr = PR_PACKAGING;
                break;
            case MAT_GENERAL:
            case MAT_SPAREPART:
                $pr = PR_GENERAL;
                break;
        }
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $pr));
        $docref = $systype->next_reference;
        if (preg_match('/(\D+-)(\D+)\/(\d{3})\/(\d{2})/', $docref, $result) == 1) {
            list($all, $prefix, $infix, $number, $year) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '999') {
                $nextval = '001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $systype->next_reference = "$prefix$infix/$nextval/$year_new";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_SJsupplier($type, $date)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $type));
        $docref = $systype->next_reference;
        if (preg_match("/(\D*)\/(\d{2})\/(\d+)/", $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            //$date_new = date("dmy", $tgl);
            $systype->next_reference = "$prefix/$year_new/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_FPT($type, $date)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $type));
        $docref = $systype->next_reference;
        if (preg_match("/(\D*)\/(\d{2})\/(\d+)/", $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $date_new = date("dmy", $tgl);
            $systype->next_reference = "$prefix$date_new/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_ReturnPembelian($date)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => RETURN_PEMBELIAN));
        $docref = $systype->next_reference;
        if (preg_match("/(\D*)\/(\d{2})\/(\d+)/", $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            //$date_new = date("dmy", $tgl);
            $systype->next_reference = "$prefix/$year_new/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_NotaReturn($date)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => NOTA_RETURN));
        $docref = $systype->next_reference;
        if (preg_match("/(\D*)\/\d{4}(\d{2})\/(\d+)/", $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '999') {
                $nextval = '001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $date_new = date("dmy", $tgl);
            $systype->next_reference = "$prefix/$date_new/$nextval";
            $docref = "$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $docref;
        }
    }
    static function createDocumentReference_TandaTerima($date)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => TANDA_TERIMA));
        $docref = $systype->next_reference;
        if (preg_match("/(\D*)\/\d{4}(\d{2})\/(\d+)/", $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $date_new = date("dmy", $tgl);
            $systype->next_reference = "$prefix/$date_new/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_Transfer($date, $pengirim, $penerima)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => TRANSFER_BARANG));
        $docref_all = $systype->next_reference;
        $no_match = FALSE;
        if (preg_match("/$pengirim-$penerima\/\d{2}\/\d+/", $docref_all, $docref_arr) == 1) {
            list($docref) = $docref_arr;
        } else {
            $no_match = TRUE;
            $docref = "$pengirim-$penerima/00/0000";
        }
        if (preg_match("/\D*\/(\d{2})\/(\d+)/", $docref, $result) == 1) {
            list($all, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $next_docref = "$pengirim-$penerima/$year_new/$nextval";
            if ($no_match) {
                $systype->next_reference .= "$next_docref;";
            } else {
                $systype->next_reference = preg_replace("/$pengirim-$penerima\/\d{2}\/\d+/", $next_docref, $systype->next_reference);
            }
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $next_docref;
        }
    }
    static function createDocumentReference_TransferMaterial($date, $pengirim, $penerima)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => TRANSFER_MATERIAL));
        $docref_all = $systype->next_reference;
        $no_match = FALSE;
        if (preg_match("/$pengirim-$penerima\/\d{2}\/\d+/", $docref_all, $docref_arr) == 1) {
            list($docref) = $docref_arr;
        } else {
            $no_match = TRUE;
            $docref = "$pengirim-$penerima/00/0000";
        }
        if (preg_match("/\D*\/(\d{2})\/(\d+)/", $docref, $result) == 1) {
            list($all, $year, $number) = $result;
            $tgl = strtotime($date);
            $year_new = date("y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $next_docref = "$pengirim-$penerima/$year_new/$nextval";
            if ($no_match) {
                $systype->next_reference .= "$next_docref;";
            } else {
                $systype->next_reference = preg_replace("/$pengirim-$penerima\/\d{2}\/\d+/", $next_docref, $systype->next_reference);
            }
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $next_docref;
        }
    }
    static function createDocumentReference_PerbaikanMesin()
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => PERBAIKAN_MESIN));
        $docref = $systype->next_reference;
        if (preg_match('/^(\D*?)\/\d{2}\/(\d{4})\/(\d+)/', $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime(date("Y-m-d"));
            $year_new = date("Y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $month_year = date("m/Y", $tgl);
            $systype->next_reference = "$prefix/$month_year/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_TandaterimaSparepartBekas()
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => TANDATERIMA_SPAREPART_BEKAS));
        $docref = $systype->next_reference;
        if (preg_match('/^(\D*?)\/\d{2}\/(\d{4})\/(\d+)/', $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime(date("Y-m-d"));
            $year_new = date("Y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $month_year = date("m/Y", $tgl);
            $systype->next_reference = "$prefix/$month_year/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    static function createDocumentReference_KonversiSparepartBekas()
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => KONVERSI_SPAREPART_BARKAS));
        $docref = $systype->next_reference;
        if (preg_match('/^(\D*?)\/\d{2}\/(\d{4})\/(\d+)/', $docref, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime(date("Y-m-d"));
            $year_new = date("Y", $tgl);
            if ($year != $year_new || $number == '9999') {
                $nextval = '0001';
            } else {
                $dig_count = strlen($number); // How many digits? eg. 0003 = 4
                $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
                $val = intval($number + 1);
                $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            }
            $month_year = date("m/Y", $tgl);
            $systype->next_reference = "$prefix/$month_year/$nextval";
        }
        if (!$systype->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                CHtml::errorSummary($systype));
        } else {
            return $systype->next_reference;
        }
    }
    function get_next_reference($type)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_id = :type_id");
        $criteria->params = array(':type_id' => $type);
        $model = SysTypes::model()->find($criteria);
        $next = $this->_increment($model->next_reference);
        return $next;
    }
    function _increment($reference, $back = false)
    {
        // New method done by Pete. So f.i. WA036 will increment to WA037 and so on.
        // If $reference contains at least one group of digits,
        // extract first didgits group and add 1, then put all together.
        // NB. preg_match returns 1 if the regex matches completely
        // also $result[0] holds entire string, 1 the first captured, 2 the 2nd etc.
        //
        if (preg_match('/^(\D*?)\/(\d{6})\/(\d+)/', $reference, $result) == 1) {
            list($all, $prefix, $year, $number) = $result;
            $tgl = strtotime($this->tgl);
            $year = date("dmy", $tgl);
            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
            $val = intval($number + ($back ? ($number < 1 ? 0 : -1) : 1));
            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            return "$prefix/$year/$nextval";
        } else
            return $reference;
    }
    function save($type, $id, $reference)
    {
        $this->update_reference($type, $id, $reference); // store in refs table
        // increment default
        $this->save_next_reference($type, $reference);
    }
    function update_reference($type, $id, $reference)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = :type_");
        $criteria->addCondition("type_no = :type_no");
        $criteria->params = array(':type_' => $type, ':type_no' => $id);
        $model = Refs::model()->find($criteria);
        if ($model == null) {
            $model = new Refs();
            $model->type_ = $type;
            $model->type_no = $id;
        }
        $model->reference = $reference;
        if (!$model->save())
            throw new Exception("Gagal menyimpan referensi.");
    }
    function save_next_reference($type, $reference)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_id =" . $type);
        $model = SysTypes::model()->find($criteria);
        $model->next_reference = trim($reference);
        if (!$model->save())
            throw new Exception("Gagal menyimpan referensi selanjutnya.");
    }
    function save_reset($type, $reference)
    {
        //$this->update_reference($type, $id, $reference); // store in refs table
        $next = $this->_reset($reference); // increment default
        $this->save_next_reference($type, $next);
    }
    function _reset($reference, $back = false)
    {
        // New method done by Pete. So f.i. WA036 will increment to WA037 and so on.
        // If $reference contains at least one group of digits,
        // extract first didgits group and add 1, then put all together.
        // NB. preg_match returns 1 if the regex matches completely
        // also $result[0] holds entire string, 1 the first captured, 2 the 2nd etc.
        //
        if (preg_match('/^(\D*?)(\d{4})(\d+)(.*)/', $reference, $result) == 1) {
            list($all, $prefix, $year, $number, $postfix) = $result;
            $year = date("Y");
            $dig_count = strlen($number); // How many digits? eg. 0003 = 4
            $fmt = '%0' . $dig_count . 'd'; // Make a format string - leading zeroes
            $val = intval(1);
            $nextval = sprintf($fmt, $val); // Add one on, and put prefix back on
            return $prefix . $year . $nextval . $postfix;
        } else
            return $reference;
    }
}