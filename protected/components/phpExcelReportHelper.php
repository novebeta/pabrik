<?php
Yii::import("application.extensions.PHPExcel", true);
class PhpExcelReportHelper
{
    private $objReader;
    private $objPHPExcel;
    private $activeSheet;
    public function __construct()
    {
        $this->objReader = new PHPExcel_Reader_Excel5();
        error_reporting(E_ERROR);
    }
    public function loadTemplate($fileExcel)
    {
        $this->objPHPExcel = $this->objReader->load($fileExcel);
        return $this;
    }
    public function setActiveSheetIndex($index)
    {
        $this->objPHPExcel->setActiveSheetIndex($index);
        $this->activeSheet = $this->objPHPExcel->getActiveSheet();
        return $this;
    }
    /*
     * $data : array data
     * $columnData: contoh :
     * 
     *              array(
     *                  array('A'=>'#', 'B'=>'kode_barang', 'C'=>'=IDR'),
     *                  array('B'=>'nama_barang', 'D'=>'nama [nama_depan] umur : [umr]')
     *              )
     * 
     *              kolom A baris ke-1 => nomor urut
     *              kolom B baris ke-1 => value field 'kode_barang',
     *              kolom C baris ke-1 => disi text 'IDR' (bukan dari data)
     *              kolom B baris ke-2 => value field 'nama_barang'
     *              jika field 'nama_depan' bernilai "Susi" dan field 'umr' bernilai 54, maka
     *              kolom D baris ke-2 => "nama Susi umur : 54"
     * 
     * $rowStart : row mulai menulis data
     * $rowCount : jumlah row yang digunakan untuk 1 baris data
     * $mergeCol : column yg di-merge
     *              contoh merge column dari B sampai E dan f sampai G
     *              array('B:E', 'F:G')
     */
    public function mergeBlock($data, $columnData = array(), $rowStart = 1, $rowCount = 1, $mergeCol = array())
    {
        $row = $rowStart;
        $i = 0;
        foreach ($data as $d) {
            if ($i > 0) {
                //insert new row (merge seperlunya)
                $this->activeSheet->insertNewRowBefore($row, $rowCount);
                //merge new rows seperlunya
                foreach ($mergeCol as $valuemerge) {
                    $range = explode(":", $valuemerge);
                    $this->activeSheet->mergeCells($range[0] . ($row) . ':' . $range[1] . ($row));
                }
                //copy format row sebelumnya
                $lastCol = $this->activeSheet->getHighestColumn();
                ++$lastCol;
                for ($num = 0; $num < $rowCount; $num++) {
                    $cur_row = $row + $num;
                    for ($c = 'A'; $c != $lastCol; ++$c) {
                        $cell_source = $this->activeSheet->getCell($c . ($cur_row - $rowCount));
                        $cell_target = $this->activeSheet->getCell($c . $cur_row);
                        $cell_target->setXfIndex($cell_source->getXfIndex()); // black magic here
                    }
                    $this->activeSheet->getRowDimension($cur_row)->setRowHeight($this->activeSheet->getRowDimension($cur_row - $rowCount)->getRowHeight());
                }
            }
            $rowIdx = 0;
            foreach ($columnData as $rowdata) {
                foreach ($rowdata as $key => $value) {
                    if ($value == '#') {
                        $this->activeSheet->setCellValue($key . ($row + $rowIdx), $i + 1);
                    } else if (substr($value, 0, 1) == '=') {
                        $this->activeSheet->setCellValue($key . ($row + $rowIdx), substr($value, 1));
                    } else if (preg_match_all("/\[(.*?)\]/", $value)) {
                        preg_match_all("/\[(.*?)\]/", $value, $matches);
                        $splited = preg_split("/\[(.*?)\]/", $value);
                        $i = 0;
                        $text = $splited[$i];
                        foreach ($matches[1] as $field) {
                            $i++;
                            $text .= $d[$field] . $splited[$i];
                        }
                        $this->activeSheet->setCellValue($key . ($row + $rowIdx), $text);
                    } else {
                        $this->activeSheet->setCellValue($key . ($row + $rowIdx), $d[$value]);
                    }
                }
                $rowIdx++;
            }
            $i++;
            if ($i > 0) $row += $rowCount;
        }
        return $this;
    }
    /*
     *  $fields : array('A1'=>'Rp 1000,-')
     */
    public function mergeField($fields = array())
    {
        foreach ($fields as $key => $value) {
            $this->activeSheet->setCellValue($key, $value);
        }
        return $this;
    }
    /*
     *  disable editing
     *  agar tidak dapat diubah
     */
    public function protectSheet()
    {
        $this->activeSheet->getProtection()->setSheet(true);
        return $this;
    }
    public function unprotectSheet()
    {
        $this->activeSheet->getProtection()->setSheet(true);
        return $this;
    }
    public function downloadExcel($filename)
    {
        $objWriter = new PHPExcel_Writer_Excel5($this->objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        $objWriter->save('php://output');
    }
    public function showHTML($filename)
    {
        $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'HTML');
        $objWriter->save($filename.'.html');
    }
    public function downloadPDF($filename)
    {
        define('_MPDF_TTFONTDATAPATH',Yii::getPathOfAlias('application.runtime').DS);
        //masih ada masalah di $rendererLibraryPath
        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibrary = 'mpdf';
//        $rendererLibraryPath = dirname(__FILE__) . '/../vendors/' . $rendererLibrary;
        $rendererLibraryPath = Yii::getPathOfAlias('application.vendors') .DS. $rendererLibrary;
        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
            die(
                'Please set the $rendererName and $rendererLibraryPath values' . PHP_EOL .
                ' as appropriate for your directory structure'
            );
        }
        $objWriter = new PHPExcel_Writer_PDF_mPDF($this->objPHPExcel);
//        header('Content-type: application/pdf');
//        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
        $objWriter->save($filename.'.pdf');
    }
}
