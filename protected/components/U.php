<?php
class U
{
    static function get_bank_trans_view()
    {
        global $systypes_array;
        $bfw = U::get_balance_before_for_bank_account($_POST['trans_date_mulai'],
            $_POST['bank_act']);
        $arr['data'][] = array(
            'type' => 'Saldo Awal - ' . sql2date($_POST['trans_date_mulai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $bfw >= 0 ? number_format($bfw, 2) : '',
            'kredit' => $bfw < 0 ? number_format($bfw, 2) : '',
            'neraca' => '',
            'person' => ''
        );
        $credit = $debit = 0;
        $running_total = $bfw;
        if ($bfw > 0) {
            $debit += $bfw;
        } else {
            $credit += $bfw;
        }
        $result = U::get_bank_trans_for_bank_account($_POST['bank_act'],
            $_POST['trans_date_mulai'], $_POST['trans_date_sampai']);
        foreach ($result as $myrow) {
            $running_total += $myrow->amount;
            $jemaat = get_jemaat_from_user_id($myrow->users_id);
            $arr['data'][] = array(
                'type' => $systypes_array[$myrow->type],
                'ref' => $myrow->ref,
                'tgl' => sql2date($myrow->trans_date),
                'debit' => $myrow->amount >= 0 ? number_format($myrow->amount, 2)
                    : '',
                'kredit' => $myrow->amount < 0 ? number_format(-$myrow->amount,
                    2) : '',
                'neraca' => number_format($running_total, 2),
                'person' => $jemaat->real_name
            );
            if ($myrow->amount > 0) {
                $debit += $myrow->amount;
            } else {
                $credit += $myrow->amount;
            }
        }
        $arr['data'][] = array(
            'type' => 'Saldo Akhir - ' . sql2date($_POST['trans_date_sampai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $running_total >= 0 ? number_format($running_total, 2) : '',
            'kredit' => $running_total < 0 ? number_format(-$running_total, 2) : '',
            'neraca' => '', // number_format($debit + $credit, 2),
            'person' => ''
        );
        return $arr;
    }
    static function get_balance_before_for_bank_account($from, $bank_account = null)
    {
//        $db = BankTrans::model()->getDbConnection();
        $query = app()->db->createCommand();
        $query->select = "SUM(amount)";
        $query->from = "{{bank_trans}}";
        $query->where('tgl < :from', array(':from' => $from));
        if ($bank_account != null) {
            $query->andWhere('bank_id = :id_bank', array(':id_bank' => $bank_account));
        }
        $total = $query->queryScalar();
        return $total ? $total : 0;
    }
    static function get_bank_trans_for_bank_account($bank_account, $from, $to)
    {
        $criteria = new CDbCriteria();
        if ($bank_account != null) {
            $criteria->addCondition("bank_act =" . $bank_account);
        }
        $criteria->addBetweenCondition("trans_date", $from, $to);
        $criteria->order = "trans_date, id";
        return BankTrans::model()->findAll($criteria);
    }
    static function get_prefs($name)
    {
        $criteria = new CDbCriteria();
        if ($name != null) {
            $criteria->addCondition("name ='$name'");
        } else {
            return null;
        }
        $prefs = SysPrefs::model()->find($criteria);
        return $prefs->value;
    }
    static function get_act_code_from_bank_act($bank_act)
    {
        $bank = Bank::model()->findByPk($bank_act);
        if ($bank != null) {
            return $bank->accountCode->account_code;
        } else {
            return false;
        }
    }
    static function get_sql_for_journal_inquiry($from, $to)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "gl_trans.tran_date,gl_trans.type,refs.reference,Sum(IF(amount>0, amount,0)) AS amount,
    comments.memo_,gl_trans.person_id,gl_trans.type_no")->from('gl_trans')->join(
            'comments',
            'gl_trans.type = comments.type AND gl_trans.type_no = comments.type_no')->Join(
            'refs',
            'gl_trans.type = refs.type AND gl_trans.type_no = refs.type_no')->where(
            "gl_trans.amount!=0 and gl_trans.tran_date >= '$from'
?  ?          AND gl_trans.tran_date <= '$to'")->group('gl_trans.type, gl_trans.type_no')->order(
            'tran_date desc')->queryAll();
        return $rows;
    }
    static function add_gl(
        $type,
        $trans_id,
        $date_,
        $ref,
        $account,
        $memo_,
        $comment_,
        $amount
    )
    {
        $person_id = Yii::app()->user->getId();
        $is_bank_to = self::is_bank_account($account);
        self::add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount,
            $person_id);
        if ($is_bank_to) {
            self::add_bank_trans($type, $trans_id, $is_bank_to, $ref, $date_,
                $amount, $person_id);
        }
        self::add_comments($type, $trans_id, $date_, $comment_);
        // return $trans_id;
    }
    // --------------------------------------------- Gl Trans ---------------------------------------------------------------
    static function is_bank_account($account_code)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(":account_code" => $account_code);
        $bank_act = Bank::model()->find($criteria);
        if ($bank_act != null) {
            return $bank_act->id_bank;
        } else {
            return false;
        }
    }
    static function add_gl_trans(
        $type,
        $trans_id,
        $date_,
        $account,
        $memo_,
        $amount,
        $person_id
    )
    {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        if (!$gl_trans->save()) {
            throw new Exception("Gagal menyimpan jurnal." . CHtml::errorSummary($gl_trans));
        }
    }
    static function add_bank_trans(
        $type,
        $trans_no,
        $bank_act,
        $ref,
        $date_,
        $amount,
        $person_id
    )
    {
        $bank_trans = new BankTrans();
        $bank_trans->type_ = $type;
        $bank_trans->trans_no = $trans_no;
        $bank_trans->bank_id = $bank_act;
        $bank_trans->ref = $ref;
        $bank_trans->tgl = $date_;
        $bank_trans->amount = $amount;
        $bank_trans->id_user = $person_id;
        if (!$bank_trans->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Bank transaction')) . CHtml::errorSummary($bank_trans));
        }
    }
    static function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = new Comments();
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save()) {
                throw new Exception("Gagal menyimpan keterangan." . CHtml::errorSummary($comment));
            }
        }
    }
    static function add_stock_moves($type, $trans_no, $tran_date, $barang_id, $qty, $reference, $price, $cost, $storloc,
                                    $supplier_id)
    {
        $move = new StockMovesMaterial;
        $move->type_no = $type;
        $move->trans_no = $trans_no;
        $move->trans_date = $tran_date;
        $move->price = $price;
        $move->std_cost = $cost;
        $move->reference = $reference;
        $move->qty = $qty;
        $move->material_id = $barang_id;
        $move->supplier_id = $supplier_id;
//        $move->batch = $batch;
//        $move->tgl_expired = $tgl_exp;
        $move->storage_location = $storloc;
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Stock moves')) . CHtml::errorSummary($move));
        }
    }
    static function add_stock_moves_material(
        $type_no, //SUPPIN/SUPOUT
        $trans_no, //terima_id, return_id
        $trans_date,
        $material_id,
        $qty,
        $tgl_expired,
        $no_lot,
        $no_qc,
        $reference,
        $storloc,
        $supplier_id = NULL
    )
    {
        /** @var Material $mat */
        $mat = Material::model()->findByPk($material_id);
        if ($mat == null) {
            throw new Exception('Material tidak ditemukan.');
        }
        $move = new StockMovesMaterial;
        $move->type_no = $type_no;
        $move->trans_no = $trans_no;
        $move->trans_date = $trans_date;
        $move->reference = $reference;
        $move->qty = $qty;
        $move->tgl_expired = $tgl_expired;
        $move->no_lot = $no_lot;
        $move->no_qc = $no_qc;
        $move->material_id = $material_id;
        $move->storage_location = $storloc;
        $move->supplier_id = $supplier_id;
        $move->price = is_null($mat->avg) ? 0 : $mat->avg;
        $move->id_user = Yii::app()->user->getId();
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Stock moves Material')) . CHtml::errorSummary($move));
        }
    }

//    static function add_stock_moves_pack_material(
//        $type,
//        $trans_no, //terima_pack_id
//        $trans_date,
//        $pm_id,
//        $batch,
//        $tgl_exp,
//        $qty,
//        $reference,
//        $price
//    ) {
//        $move = new StockMovesPackMaterial;
//        
//        $query = StockMovesPackMaterial::model()->dbConnection->createCommand("SELECT UUID();");
//        $uuid = $query->queryScalar();
//        $query = StockMovesPackMaterial::model()->dbConnection->createCommand("SELECT CURRENT_TIMESTAMP();");
//        $cts = $query->queryScalar();
//        
//        $move->stock_moves_id = $uuid;
//        $move->type_no = $type;
//        $move->trans_no = $trans_no;
//        $move->trans_date = $trans_date;
//        $move->price = $price;
//        $move->reference = $reference;
//        $move->qty = $qty;
//        $move->tdate = $cts;
//        $move->pm_id = $pm_id;
//        $move->batch = $batch;
//        $move->tgl_exp = $tgl_exp;
//        $move->id_user = Yii::app()->user->getId();
//        if (!$move->save()) {
//            throw new Exception(t('save.model.fail', 'app',
//                    array('{model}' => 'Stock moves (Raw Material)')) . CHtml::errorSummary($move));
//        }
//    }
    static function charge_NoFakturPajak()
    {
        $model = null;
        $criteria = new CDbCriteria();
        $criteria->addCondition('fp_used < fp_jumlah AND fp_used > 0');
        if (DepositFakturPajak::model()->count($criteria) > 0) {
            $model = DepositFakturPajak::model()->find($criteria);
        } else {
            $criteria2 = new CDbCriteria();
            $criteria2->addCondition('fp_used < fp_jumlah');
            $criteria2->order = 'tgl';
            $criteria2->limit = 1;
            $model = DepositFakturPajak::model()->find($criteria2);
            if (!$model) {
                throw new Exception(t('Tidak tersedia deposit No. Faktur Pajak', 'app'));
            }
        }
        if (preg_match("/\d+$/", $model->fp_no, $result) == 1) {
            list($number) = $result;
            $dig_count = strlen($number);
            $fmt = '%0' . $dig_count . 'd';
            $number += $model->fp_used;
            $no_fp = sprintf($fmt, intval($number));
            $model->fp_used++;
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Stock moves')) . CHtml::errorSummary($model));
            }
            return preg_replace("/\d+$/", "", $model->fp_no) . $no_fp;
        }
    }
    // --------------------------------------------- Comments ---------------------------------------------------------------
    static function get_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        return Comments::model()->find($criteria);
    }
    static function update_comments($type, $id, $date_, $memo_)
    {
        if ($date_ == null) {
            U::delete_comments($type, $id);
            U::add_comments($type, $id,
                Yii::app()->dateFormatter->format('yyyy-MM-dd', time()),
                $memo_);
        } else {
            $criteria = new CDbCriteria();
            $criteria->addCondition("type=" . $type);
            $criteria->addCondition("id=" . $id);
            $criteria->addCondition("date_=" . $date_);
            $comment = Comments::model()->find($criteria);
            $comment->memo_ = $memo_;
            $comment->save();
        }
    }
    static function delete_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        $comment = Comments::model()->find($criteria);
        $comment->delete();
    }
    // ---------------------------------------------- Report ----------------------------------------------------------------
    static function get_beban()
    {
        $rows = app()->db->createCommand("SELECT account_code FROM mt_chart_master WHERE account_code REGEXP '^5[1-9]'")->queryAll();
        return $rows;
    }
    static function get_daftar_master_konsumen($code, $nama, $phone, $hp, $hp2, $tempo, $status)
    {
        $query = app()->db->createCommand();
        $query->select("CONCAT(pk.konsumen_code,' ') `Kode Konsumen`, pk.konsumen_name `Nama Konsumen`,
        pk.phone Phone, pk.hp HP, pk.hp2 `HP 2`, pk.tempo Tempo, pk.address Alamat, pk.status `Status`");
        $query->from("{{konsumen}} pk");
        if ($code !== "") {
            $query->andWhere("konsumen_code like :konsumen_code", array(":konsumen_code" => $code . "%"));
        }
        if ($nama !== "") {
            $query->andWhere("konsumen_name like :konsumen_name", array(":konsumen_name" => "%" . $nama . "%"));
        }
        if ($phone !== "") {
            $query->andWhere("phone like :phone", array(":phone" => "%" . $phone . "%"));
        }
        if ($hp !== "") {
            $query->andWhere("hp like :hp", array(":hp" => "%" . $hp . "%"));
        }
        if ($hp2 !== "") {
            $query->andWhere("hp2 like :hp2", array(":hp2" => "%" . $hp2 . "%"));
        }
        if ($tempo !== "") {
            $query->andWhere("tempo like :tempo", array(":tempo" => "%" . $tempo . "%"));
        }
        if ($status !== "") {
            $query->andWhere("status like :status", array(":status" => "%" . $status . "%"));
        }
        return $query->queryAll(true);
    }
    static function get_mutasi_kas_ditangan($start_date, $end_date)
    {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('trans_date', $start_date, $end_date);
        $model = BankTrans::model()->findAll($criteria);
        return $model;
    }
    static function get_arr_kode_rekening_pengeluaran($code = "")
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type='" . Prefs::TypeCostAct() . "'");
        if ($code != "account_code" && $code != "") {
            $criteria->addCondition("account_code='$code'");
        }
        $model = ChartMaster::model()->findAll($criteria);
        $daftar = array();
        foreach ($model as $coderek) {
            $daftar[$coderek['account_code']] = $coderek['account_name'];
        }
        return $daftar;
    }
    static function get_pengeluaran_detil_kode_rekening(
        $start_date,
        $end_date,
        $code
    )
    {
        $rows = Yii::app()->db->createCommand()->select(
            "a.tran_date,a.memo_,IF(a.amount > 0,a.amount,'') as debit,IF(a.amount < 0,-a.amount,'') as kredit")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code
    AND a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->leftJoin('mt_voided c', "a.type_no=c.id AND c.type=a.type")->where(
            "b.account_code=:code and a.type != :type and ISNULL(c.date_)",
            array(
                'code' => $code,
                'type' => VOID
            ))->order("a.tran_date")->queryAll();
        // ->where("b.account_code=:code",array('code'=>$code))
        return $rows;
    }
    static function get_pengeluaran_per_kode_rekening($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_code,b.account_name as nama_rekening,IFNULL(sum(a.amount),0) as total_beban")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code
    AND a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->where("b.account_type=:type and !b.inactive",
            array(
                ':type' => Prefs::TypeCostAct()
            ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }
    static function get_total_pengeluaran($start_date, $end_date, $code = "")
    {
        $kode = $code == "" ? "" : "and b.account_code = '$code'";
        $rows = Yii::app()->db->createCommand()->select("sum(a.amount) as total_beban")->from(
            "mt_gl_trans a")->join("mt_chart_master b",
            "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type $kode",
            array(
                ':start' => $start_date,
                ':end' => $end_date,
                ':type' => Prefs::TypeCostAct()
            ))->queryScalar();
        return $rows == null ? 0 : $rows;
    }
    static function get_detil_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_name as nama_rekening,IFNULL(-sum(a.amount),0) as total_pendapatan")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code and
        a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->where("b.account_type=:type and !b.inactive",
            array(
                ':type' => Prefs::TypePendapatanAct()
            ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }
    static function get_total_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select("-sum(a.amount) as total_pendapatan")->from(
            "mt_gl_trans a")->join("mt_chart_master b",
            "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type",
            array(
                ':start' => $start_date,
                ':end' => $end_date,
                ':type' => Prefs::TypePendapatanAct()
            ))->order("b.account_code")->queryScalar();
        return $rows == null ? 0 : $rows;
    }
    static function get_chart_master_beban()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type = " . Prefs::TypeCostAct());
        return ChartMaster::model()->findAll($criteria);
    }
    static function account_in_gl_trans($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = '$account'");
        $count = GlTrans::model()->count($criteria);
        return $count > 0;
    }
    static function account_used_bank($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = '$account'");
        $count = Bank::model()->count($criteria);
        return $count > 0;
    }
    static function account_used_supplier($account_code, $supplier_id)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(':account_code' => $account_code);
        $count = Supplier::model()->count($criteria);
        if ($supplier_id === NULL) { //create
            return $count > 0;
        } else { //edit
            $criteria2 = new CDbCriteria();
            $criteria2->addCondition("account_code = :account_code AND supplier_id = :supplier_id");
            $criteria2->params = array(':account_code' => $account_code, ':supplier_id' => $supplier_id);
            $count2 = Supplier::model()->count($criteria2);
            return $count > 0 && $count2 < 1;
        }
    }
    static function report_new_customers($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT * FROM {{customers}}
            WHERE DATE(awal) >= :FROM AND DATE(AWAL) <= :TO");
        return $comm->queryAll(true, array(':from' => $from, 'to' => $to));
    }
    static function report_biaya($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT nk.tgl, nk.doc_ref, nk.no_kwitansi,
            nk.keperluan,-nk.total total
            FROM {{kas}} AS nk
            WHERE nk.total < 0 AND DATE(tgl) >= :FROM AND DATE(tgl) <= :TO");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_kartu_stok($barang_id, $tgl_exp, $batch, $from, $to, $storloc)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT
            DATE_FORMAT(tran_date, '%d %b %Y') tgl,
            reference doc_ref,
            CASE type_no
                WHEN " . SURAT_JALAN . " THEN 'KIRIM KE CUSTOMER'
                WHEN " . RETURBELI . " THEN 'RETURBELI'
                WHEN " . SAMPEL_QC . " THEN 'SAMPEL QC'
                WHEN " . HASIL_PRODUKSI . " THEN 'HASIL PRODUKSI'
                WHEN " . SUPPIN . " THEN 'BARANG MASUK'
                WHEN " . SUPPOUT . " THEN 'BARANG KELUAR'
                WHEN " . STOCK_ADJUST_INC . " THEN 'PENYESUAIAN STOK'
                WHEN " . STOCK_ADJUST_DEC . " THEN 'PENYESUAIAN STOK'
            END mvt,
            tgl_exp,
            DATE_FORMAT(tgl_exp, '%m-%Y') tgl_expired,
            batch,
            0 `before`,
            IF(qty >0,qty,0) `in`,
            IF(qty <0,-qty,0) `out`,
            0 `after`,
            qty
        FROM
            pbu_stock_moves
        WHERE
            storage_location = '$storloc'
            AND DATE(tran_date) >= :from 
            AND DATE(tran_date) <= :to
            AND barang_id = :barang_id " .
            ($batch == "" || $batch == NULL ? "" : "AND batch = '$batch' ") .
            "ORDER BY tdate, tran_date");
        return $comm->queryAll(true, array(
            ':barang_id' => $barang_id,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_KartuPersediaanProdukJadi($barang_id, $tgl_exp, $batch, $from, $to, $storloc)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT
            sm.type_no,
            sm.reference doc_ref,
            0 `before`,
            IF(sm.qty >0, DATE_FORMAT(sm.tran_date, '%d %b %Y'), NULL) tgl_masuk,
            IF(sm.qty >0, sm.batch, NULL) batch_masuk,
            IF(sm.qty >0, sm.qty,0) qty_masuk,
            IF(sm.qty <0, DATE_FORMAT(sm.tran_date, '%d %b %Y'), NULL) tgl_keluar,
            IF(sm.qty <0, sm.batch, NULL) batch_keluar,
            IF(sm.qty <0, -sm.qty,0) qty_keluar,
            IF(sm.type_no = 9, c.kode_customer, IF(sm.qty <0, sm.reference, NULL)) tujuan,
            0 `after`,
            sm.qty,
            DATE_FORMAT(sm.tgl_exp, '%m-%Y') tgl_expired
        FROM
            pbu_stock_moves sm
            LEFT JOIN pbu_sj sj ON sj.sj_id = sm.trans_no
            LEFT JOIN pbu_customers c ON c.customer_id = sj.customer_id
        WHERE
            sm.storage_location = '$storloc'
            AND DATE(sm.tran_date) >= :from  
            AND DATE(sm.tran_date) <= :to
            AND sm.barang_id = :barang_id " .
            ($batch == "" || $batch == NULL ? "" : "AND batch = '$batch' ") .
            ($tgl_exp == "" || $tgl_exp == NULL ? "" : "AND DATE_FORMAT(sm.tgl_exp, '%m-%Y') = DATE_FORMAT(STR_TO_DATE('$tgl_exp', '%Y-%m-%d'), '%m-%Y') ") .
            "ORDER BY sm.tdate, sm.tran_date");
        return $comm->queryAll(true, array(
            ':barang_id' => $barang_id,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_barang_keluar($barang_id, $batch, $from, $to)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT DATE_FORMAT(nsm.tran_date,'%d/%m/%Y') AS tgl,
        nsm.reference AS doc_ref,nsm.batch,0 AS awal,
        IF(nsm.qty >0,nsm.qty,0) AS `in`,
        IF(nsm.qty <0,-nsm.qty,0) AS `out`,
        0 AS akhir,nsm.qty,pb.kode_barang,
        pb.nama_barang,nsm.tgl_exp,pc.nama_customer
        FROM pbu_stock_moves AS nsm
        LEFT JOIN pbu_barang AS pb ON nsm.barang_id = pb.barang_id
        LEFT JOIN pbu_sj AS psj ON nsm.trans_no = psj.sj_id AND nsm.type_no = 9
        LEFT JOIN pbu_customers AS pc ON psj.customer_id = pc.customer_id
        LEFT JOIN pbu_sjsim AS psim ON nsm.trans_no = psim.sjsim_id AND nsm.type_no = 12
        WHERE DATE(nsm.tran_date) >= :from AND
        DATE(nsm.tran_date) <= :to AND
        nsm.barang_id = :barang_id AND nsm.batch = :batch
        ORDER BY        nsm.tdate ASC");
        return $comm->queryAll(true, array(
            ':barang_id' => $barang_id,
            ':batch' => $batch,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_terima_barang_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                ptb.doc_ref,
                DATE_FORMAT(ptb.tgl,'%d/%m/%Y') tgl,
                pb.kode_barang,pb.nama_barang,
                ptbd.batch,
                DATE_FORMAT(ptbd.tgl_exp,'%b-%Y') tgl_exp,
                ptbd.qty
        FROM pbu_terima_barang_details AS ptbd
            INNER JOIN pbu_barang AS pb ON ptbd.barang_id = pb.barang_id
            INNER JOIN pbu_terima_barang AS ptb ON ptbd.terima_barang_id = ptb.terima_barang_id
        WHERE ptb.tgl >= :from AND ptb.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_retur_beli_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ps.doc_ref,DATE_FORMAT(ps.tgl,'%d/%m/%Y') tgl,pc.kode_customer,
        pc.nama_customer,pb.kode_barang,pb.nama_barang,
        psd.qty,psd.batch,DATE_FORMAT(psd.tgl_exp,'%b-%Y') tgl_exp
        FROM pbu_sjsim AS ps
        INNER JOIN pbu_sjsim_details AS psd ON psd.sjsim_id = ps.sjsim_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_int_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT ps.doc_ref,ps.doc_ref_inv,
        DATE_FORMAT(ps.tgl,'%d/%m/%Y') AS tgl,
        DATE_FORMAT(ps.tgl_jatuh_tempo,'%d/%m/%Y') AS tgl_jatuh_tempo,
        pc.kode_customer,pc.nama_customer,pb.kode_barang,pb.nama_barang,
        psd.qty,psd.batch,DATE_FORMAT(psd.tgl_exp,'%d/%m/%Y') AS tgl_exp,
        psd.price,psd.total
        FROM pbu_sj AS ps
        INNER JOIN pbu_sj_details AS psd ON psd.sj_id = ps.sj_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_tax_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT ps.doc_ref,ps.doc_ref_inv,
        DATE_FORMAT(ps.tgl,'%d/%m/%Y') AS tgl,
        DATE_FORMAT(ps.tgl_jatuh_tempo,'%d/%m/%Y') AS tgl_jatuh_tempo,
        pc.kode_customer,pc.nama_customer,pb.kode_barang,pb.nama_barang,
        psd.qty,psd.batch,DATE_FORMAT(psd.tgl_exp,'%d/%m/%Y') AS tgl_exp,
        psd.price,psd.total
        FROM pbu_sjtax AS ps
        INNER JOIN pbu_sjtax_details AS psd ON psd.sjtax_id = ps.sjtax_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_credit_note_details($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT ps.doc_ref,
        DATE_FORMAT(ps.tgl,'%d/%m/%Y') AS tgl,pb.kode_barang,
        pb.nama_barang,psd.qty,psd.batch,
        DATE_FORMAT(psd.tgl_exp,'%d/%m/%Y') AS tgl_exp,
        psd.price,psd.total
        FROM pbu_credit_note AS ps
        INNER JOIN pbu_credit_note_details AS psd ON psd.credit_note_id = ps.credit_note_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        WHERE ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_mutasi_stok($from, $to, $showExp, $showNoBatch, $storloc)
    {
        /*if($_POST['USER_PRODUKSI']){
            $comm = Yii::app()->db->createCommand("
                SELECT
                    nb.kode_barang, nb.nama_barang, nsm.batch,
                    SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) 'before',
                    SUM(IF (nsm.qty > 0 AND DATE(nsm.tran_date) >= :from
                      AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) produksi,
                    SUM(IF (nsm.qty < 0 AND DATE(nsm.tran_date) >= :from
                      AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) kirimGPJ,
                    SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) 'after'
                FROM 
                    (SELECT
                        tb.tgl tran_date,
                        tbd.barang_id,
                        -tbd.qty qty,
                        tbd.tgl_exp,
                        tbd.batch
                    FROM pbu_transfer_barang_detail tbd LEFT JOIN pbu_transfer_barang tb ON tb.transfer_barang_id = tbd.transfer_barang_id
                    UNION ALL
                    SELECT
                        bh.tgl tran_date,
                        bh.barang_id,
                        bh.qty,
                        bh.tgl_exp,
                        bh.no_batch batch
                    FROM pbu_batch_hasil bh) AS nsm 
                    LEFT  JOIN pbu_barang AS nb ON (nsm.barang_id = nb.barang_id)
                GROUP BY nb.kode_barang, nb.nama_barang,nsm.batch
                ORDER BY nb.barang_id");
        } else {
            $comm = Yii::app()->db->createCommand(" SELECT nb.kode_barang, nb.nama_barang,nsm.batch,
                            SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) 'before',
            SUM(IF (nsm.qty > 0 AND nsm.type_no = 5 AND DATE(nsm.tran_date) >= :from
                      AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) terima,
                    SUM(IF (nsm.qty < 0 AND nsm.type_no = 9 AND DATE(nsm.tran_date) >= :from
                      AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) sj,
                    SUM(IF (nsm.qty < 0 AND nsm.type_no = 12 AND DATE(nsm.tran_date) >= :from
                      AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) returbeli,
            SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) after
            FROM pbu_barang AS nb LEFT  JOIN pbu_stock_moves AS nsm ON (nsm.barang_id = nb.barang_id)
            GROUP BY nb.kode_barang, nb.nama_barang,nsm.batch ORDER BY nb.barang_id");
        }*/
        $comm = Yii::app()->db->createCommand("
            SELECT
                b.kode_barang kode,
                b.nama_barang nama,
                " . ($showExp ? "DATE_FORMAT(sm.tgl_exp, '%b-%Y') tgl_expired," : "") . "
                " . ($showNoBatch ? "sm.batch no_batch," : "") . "
                SUM(IF (DATE(sm.tran_date) < :from, sm.qty, 0)) 'before',
                SUM(IF (sm.qty > 0 AND sm.type_no IN (" . SUPPIN . ", " . HASIL_PRODUKSI . ") AND DATE(sm.tran_date) >= :from
                  AND DATE(sm.tran_date) <= :to, sm.qty, 0)) suppin,
                SUM(IF (sm.qty < 0 AND sm.type_no IN (" . SUPPOUT . ", " . SURAT_JALAN . ", " . RETURBELI . ", " . SAMPEL_QC . ") AND DATE(sm.tran_date) >= :from
                  AND DATE(sm.tran_date) <= :to, ABS(sm.qty), 0)) suppout,
                SUM(IF (DATE(sm.tran_date) <= :to, sm.qty, 0)) after
            FROM pbu_stock_moves sm
                LEFT JOIN pbu_barang b ON sm.barang_id = b.barang_id
            WHERE
                sm.storage_location = '$storloc'
            GROUP BY 
                b.kode_barang, b.nama_barang
                " . ($showNoBatch ? ", sm.batch" : "") . "
            ORDER BY
                b.barang_id
                " . ($showNoBatch ? ", sm.batch" : "") . "
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function get_sales_trans_for_audit($date)
    {
        $comm = Yii::app()->db->createCommand("SELECT ns.salestrans_id,ns.doc_ref,
        nc.no_customer,nc.nama_customer,nba.nama_bank,Sum(nsd.total) AS total
        FROM nscc_salestrans AS ns
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        INNER JOIN nscc_bank AS nba ON ns.bank_id = nba.bank_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        WHERE nb.grup_id = 1 AND nsd.total >= 0 AND ns.salestrans_id NOT IN(SELECT na.audit_id FROM nscc_audit na)
        AND DATE(ns.tgl) = :date
        GROUP BY ns.doc_ref,nc.no_customer,nc.nama_customer,nba.nama_bank");
        return $comm->queryAll(true, array(':date' => $date));
    }
    static function get_list_audit($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ps.sj_id,ps.doc_ref,ps.total_tax total,pc.kode_customer,
        pc.nama_customer,ps.tgl
        FROM pbu_sj AS ps
        INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
        WHERE ps.adt <> 1 AND ps.tgl >= :from AND ps.tgl <= :to");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_list_invoice($from, $to, $barang_id = "All Item")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($barang_id != "All Item" && $barang_id != "") {
            $where = "AND psd.barang_id = :barang_id";
            $param = array(':from' => $from, ':to' => $to, ':barang_id' => $barang_id);
        }
        $comm = Yii::app()->db->createCommand("
            SELECT 
                ps.doc_ref AS sj,
                ps.doc_ref_inv AS inv,
                DATE_FORMAT(ps.tgl,'%d-%b-%y') AS tgl,
                pc.nama_customer AS cabang,
                pc.nama_badan_hukum,
                pc.alamat_badan_hukum,
                pc.address AS alamat,
                pb.kode_barang AS kode,
                pb.nama_barang AS keterangan,
                psd.batch,
                psd.qty AS jml,
                pb.sat AS satuan,
                DATE_FORMAT(psd.tgl_exp,'%m/%y') AS tgl_exp,
                psd.price*1.1 AS dpp_ppn,
                psd.price AS dpp,
                psd.total AS total_dpp_ppn,
                psd.bruto-psd.totalpot AS total_dpp,
                psd.vatrp AS total_ppn
            FROM pbu_sj AS ps
                INNER JOIN pbu_sj_details AS psd ON psd.sj_id = ps.sj_id
                INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
                INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
            WHERE
                ps.tgl >= :from AND 
                ps.tgl <= :to AND
                ps.no_faktur_pajak IS NOT NULL
                $where
            ORDER BY ps.doc_ref_inv, ps.tgl
        ");
        return $comm->queryAll(true, $param);
    }
    static function report_rekap_tagihan($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                ps.doc_ref_inv AS inv,
                pc.nama_badan_hukum,
                pc.npwp,
                pc.nama_customer AS cabang,
                DATE_FORMAT(ps.tgl,'%d-%b-%y') AS tgl,
                ps.no_faktur_pajak,
                ps.total AS pajak,
                ps.tot_bruto - ps.tot_pot1 - ps.tot_pot2 - ps.tot_pot3 AS dpp,
                ps.tot_vatrp AS ppn
            FROM pbu_sj AS ps
                INNER JOIN pbu_customers AS pc ON ps.customer_id = pc.customer_id
            WHERE ps.tgl >= :from AND ps.tgl <= :to
            ORDER BY ps.doc_ref_inv, ps.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rincian_pembelian($from, $to, $supplier_id = null)
    {
        $where = '';
        $params = [':from' => $from, ':to' => $to];
        if ($supplier_id != null) {
            $where = ' AND pc.supplier_id = :supplier_id';
            $params['supplier_id'] = $supplier_id;
        }
        $comm = Yii::app()->db->createCommand("SELECT 
                pc.*,ps.supplier_name,ps.supplier_code,pm.kode_material,pm.nama_material
                FROM pbu_po_sj_inv pc              
                INNER JOIN pbu_supplier AS ps ON ps.supplier_id = pc.supplier_id
                INNER JOIN pbu_material AS pm ON pm.material_id = pc.barang_id
            WHERE pc.tgl_inv >= :from AND pc.tgl_inv <= :to $where
            ORDER BY pc.doc_ref_inv, pc.tgl_inv
        ");
        return $comm->queryAll(true, $params);
    }
    static function report_rekap_penjualan($from, $to, $customer_id = null)
    {
        $where = '';
        $params = [':from' => $from, ':to' => $to];
        if ($customer_id != null) {
            $where = ' AND pssi.customer_id = :customer_id';
            $params[':customer_id'] = $customer_id;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT pm.kode_material,pm.nama_material,pm.sat,
            Sum(IFNULL(pssi.qty,0)) AS qty,
            Sum(IFNULL(pssi.qty,0)) * pm.avg AS nilai
            FROM pbu_material AS pm
            LEFT JOIN pbu_sls_sj_inv AS pssi ON pssi.barang_id = pm.material_id
            WHERE pssi.tgl_inv >= :from AND pssi.tgl_inv <= :to $where 
            GROUP BY pssi.barang_id,pm.kode_material,
            pm.nama_material,pm.sat,pm.avg
        ");
        return $comm->queryAll(true, $params);
    }
    static function report_rekap_tagihan_hutang($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                ps.doc_ref_inv AS inv,
                pc.npwp,
                pc.supplier_name AS cabang,
                DATE_FORMAT(ps.tgl_inv,'%d-%b-%y') AS tgl,
                ps.no_faktur_tax,
                ps.grand_total
            FROM pbu_terima_barang AS ps
                INNER JOIN pbu_purchase_order AS ppo ON ps.po_id = ppo.po_id
                INNER JOIN pbu_supplier AS pc ON ppo.supplier_id = pc.supplier_id
            WHERE ps.tgl_inv >= :from AND ps.tgl_inv <= :to
            ORDER BY ps.doc_ref_inv, ps.tgl_inv
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_detail_inv_supp($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                ps.doc_ref_inv AS inv,
                pc.npwp,
                pc.supplier_name AS cabang,
                DATE_FORMAT(ps.tgl_inv,'%d-%b-%y') AS tgl,
                ps.no_faktur_tax,
                ps.grand_total
            FROM pbu_terima_barang AS ps
                INNER JOIN pbu_purchase_order AS ppo ON ps.po_id = ppo.po_id
                INNER JOIN pbu_supplier AS pc ON ppo.supplier_id = pc.supplier_id
            WHERE ps.tgl_inv >= :from AND ps.tgl_inv <= :to
            ORDER BY ps.doc_ref_inv, ps.tgl_inv
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_E_Faktur($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                sj.sj_id AS id,
                'FK' AS col1, /*kode*/
                '01' AS col2, /*jenis_transaksi,*/
                '0' AS col3, /*fg_pengganti,*/
                sj.no_faktur_pajak AS col4,
                DATE_FORMAT(sj.tgl, '%c') AS col5, /*masa_pajak,*/
                DATE_FORMAT(sj.tgl, '%Y') AS col6, /*tahun_pajak,*/
                DATE_FORMAT(sj.tgl, '%d/%m/%Y') AS col7, /*tanggal_faktur,*/
                IF(ISNULL(cs.npwp),'00.000.000.0-000.000',cs.npwp) col8,
                cs.nama_badan_hukum AS col9, /*nama,*/
                cs.alamat_badan_hukum AS col10, /*alamat,*/
                sj.total AS col11, /*jumlah_dpp,*/
                (sj.total*0.1) AS col12, /*jumlah_ppn,*/
                0 AS col13, /*jumlah_ppnbm,*/
                '' AS col14, /*id_keterangan_tambahan,*/
                '0' AS col15, /*fg_uang_muka,*/
                '0' AS col16, /*uang_muka_dpp,*/
                '0' AS col17, /*uang_muka_ppn,*/
                '0' AS col18, /*uang_muka_ppnbm,*/
                sj.doc_ref_inv col19 /*referensi*/
            FROM pbu_sj sj
                INNER JOIN pbu_customers cs ON cs.customer_id = sj.customer_id
            WHERE
                DATE(tgl) >= :from AND DATE(tgl) <= :to
                AND sj.no_faktur_pajak IS NOT NULL
            ORDER BY 
                sj.tgl, sj.doc_ref_inv
        ");
        $FK = $comm->queryAll(true, array(':from' => $from, ':to' => $to));
        $data = [];
        foreach ($FK as $v) {
            $data[] = array_slice($v, 1);
            $comm2 = Yii::app()->db->createCommand("
                SELECT
                    'OF' AS col1, /*kode*/
                    bg.kode_barang AS col2, /*kode_objek*/
                    bg.nama_barang AS col3, /*nama*/
                    sd.price AS col4, /*harga_satuan*/
                    sd.qty AS col5, /*jumlah_barang*/
                    sd.total AS col6, /*harga_total*/
                    0 AS col7, /*diskon*/
                    sd.total AS col8, /*dpp*/
                    sd.total*0.1 AS col9, /*ppn*/
                    0 AS col10, /*tarif_ppnbm*/
                    0 AS col11, /*ppnbm*/
                    '' AS col12,
                    '' AS col13,
                    '' AS col14,
                    '' AS col15,
                    '' AS col16,
                    '' AS col17,
                    '' AS col18,
                    '' AS col19
                FROM pbu_sj_details sd
                    INNER JOIN pbu_barang bg ON bg.barang_id = sd.barang_id
                WHERE
                    sd.sj_id = :sj_id
            ");
            $OF = $comm2->queryAll(true, array(':sj_id' => $v['id']));
            foreach ($OF as $v2) {
                $data[] = $v2;
            }
        }
        return $data;
    }
    static function report_rekap_PO($from, $to, $supplier_id, $tipe_material_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                ppo.*,
                ps.supplier_name
            FROM pbu_purchase_order ppo
                LEFT JOIN pbu_supplier AS ps ON ps.supplier_id = ppo.supplier_id
            WHERE
                ppo.tgl >= :from AND ppo.tgl <= :to
                " . ($supplier_id == 'All' ? "" : "AND ppo.supplier_id = '" . $supplier_id . "'") . "
                " . ($tipe_material_id == 'All' ? "" : "AND ppo.tipe_material_id = " . $tipe_material_id) . "
                " . ($status == 'All' ? "" : "AND ppo.status = " . $status) . "
            ORDER BY ppo.doc_ref, ppo.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_PO_details($from, $to, $supplier_id, $tipe_material_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                po.doc_ref,
                po.tgl,
                s.supplier_name,
                po.status,
                m.kode_material,
                pod.qty,
                t.qty qty_terima,
                pod.sat
            FROM pbu_purchase_order AS po
                LEFT JOIN pbu_purchase_order_details AS pod ON po.po_id = pod.po_id
                LEFT JOIN pbu_supplier AS s ON s.supplier_id = po.supplier_id
                LEFT JOIN pbu_material AS m ON m.material_id = pod.material_id
                LEFT JOIN (
                            SELECT
                                    tmd.po_id,
                                    tmd.material_id,
                                    SUM(tmd.qty) qty
                            FROM pbu_terima_material_details AS tmd
                                    LEFT JOIN pbu_terima_material AS tm ON tm.terima_material_id = tmd.terima_material_id
                            GROUP BY
                                    tmd.po_id, tmd.material_id
                    ) AS t ON t.po_id = pod.po_id AND t.material_id = pod.material_id
            WHERE
                po.tgl >= :from AND po.tgl <= :to
                " . ($supplier_id == 'All' ? "" : "AND po.supplier_id = '" . $supplier_id . "'") . "
                " . ($tipe_material_id == 'All' ? "" : "AND po.tipe_material_id = " . $tipe_material_id) . "
                " . ($status == 'All' ? "" : "AND po.status = " . $status) . "
            ORDER BY po.doc_ref, po.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_Sj_Supplier($from, $to, $supplier_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                tm.*,
                ps.supplier_name
            FROM pbu_terima_material tm
                LEFT JOIN pbu_supplier AS ps ON ps.supplier_id = tm.supplier_id
            WHERE
                tm.tgl_terima >= :from AND tm.tgl_terima <= :to
                " . ($supplier_id == 'All' ? "" : "AND tm.supplier_id = '" . $supplier_id . "'") . "
                " . ($status == 'All' ? "" : "AND tm.status = " . $status) . "
            ORDER BY tm.doc_ref, tm.tgl_terima
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_Sj_Supplier_details($from, $to, $supplier_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                tm.no_sj_supplier,
                tm.tgl_sj_supplier,
                s.supplier_name,
                tm.tgl_terima,
                tm.status,
                m.kode_material,
                tmd.qty,
                tmd.sat
            FROM pbu_terima_material_details AS tmd
                LEFT JOIN pbu_terima_material AS tm ON tm.terima_material_id = tmd.terima_material_id
                LEFT JOIN pbu_supplier AS s ON s.supplier_id = tm.supplier_id
                LEFT JOIN pbu_material AS m ON m.material_id = tmd.material_id
            WHERE
                tm.tgl_terima >= :from AND tm.tgl_terima <= :to
                " . ($supplier_id == 'All' ? "" : "AND tm.supplier_id = '" . $supplier_id . "'") . "
                " . ($status == 'All' ? "" : "AND tm.status = " . $status) . "
            ORDER BY tm.doc_ref, tm.tgl_terima
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_INV_Supplier($from, $to, $supplier_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                tm.*,
                date_add(tm.tgl_terima, INTERVAL cast(tm.term_of_payment AS SIGNED) DAY) tgl_jatuh_tempo,
                ps.supplier_name
            FROM pbu_terima_material tm
                LEFT JOIN pbu_supplier AS ps ON ps.supplier_id = tm.supplier_id
            WHERE
                tm.tgl_terima >= :from AND tm.tgl_terima <= :to
                AND tm.status >= " . SJ_SUPPLIER_INVOICED . "
                " . ($supplier_id == 'All' ? "" : "AND tm.supplier_id = '" . $supplier_id . "'") . "
                " . ($status == 'All' ? "" : "AND tm.status = " . $status) . "
                
                " . ($_POST['show_belum_dibayar'] ? "AND tm.status < " . SJ_SUPPLIER_PAID : "") . "
                " . ($_POST['show_lewat_jatuh_tempo'] ? "AND current_date() > date_add(tm.tgl_inv_supplier, INTERVAL cast(tm.term_of_payment as SIGNED) DAY) AND tm.status < " . SJ_SUPPLIER_PAID : "") . "
                
            ORDER BY tm.doc_ref, tm.tgl_terima
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_Pembayaran_Supplier($from, $to, $supplier_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT pps.*, ps.supplier_name
            FROM pbu_pembayaran_supplier pps
                LEFT JOIN pbu_supplier AS ps ON ps.supplier_id = pps.supplier_id
            WHERE
                pps.tgl >= :from AND pps.tgl <= :to
                " . ($supplier_id == 'All' ? "" : "AND pps.supplier_id = '" . $supplier_id . "'") . "
                " . ($status == 'All' ? "" : "AND pps.status = " . $status) . "
            ORDER BY pps.doc_ref, pps.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_Pembayaran_Supplier_details($from, $to, $supplier_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                pps.doc_ref,
                pps.tgl,
                pps.tgl_jatuh_tempo,
                pps.tgl_bayar,
                pps.status,
                ps.supplier_name,
                ppsd.no_inv_supplier,
                ppsd.tgl_inv_supplier,
                ppsd.total_bayar
            FROM pbu_pembayaran_supplier_details ppsd
                LEFT JOIN pbu_pembayaran_supplier AS pps ON pps.pembayaran_supplier_id = ppsd.pembayaran_supplier_id
                LEFT JOIN pbu_supplier AS ps ON ps.supplier_id = pps.supplier_id
            WHERE
                pps.tgl >= :from AND pps.tgl <= :to
                " . ($supplier_id == 'All' ? "" : "AND pps.supplier_id = '" . $supplier_id . "'") . "
                " . ($status == 'All' ? "" : "AND pps.status = " . $status) . "
            ORDER BY pps.doc_ref, pps.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_Pembelian($from, $to, $supplier_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                po.tgl,
                s.supplier_code,
                s.supplier_name,
                pbu_tipe_material.nama_tipe_material,
                po.doc_ref,
                t.no_sj_supplier,
                t.no_inv_supplier,
                t.no_faktur_pajak,
                t.tgl_no_faktur_pajak,
                t.tgl_terima,
                t.tgl_inv_supplier,
                t.term_of_payment,
                date_add(t.tgl_terima, INTERVAL cast(t.term_of_payment AS SIGNED) DAY) tgl_jatuh_tempo,
                t.tgl_bayar,
                m.kode_material,
                m.nama_material,
                pod.sat,
                t.qty,
                t.currency,
                t.IDR_rate,
                t.price,
                (t.qty*t.price) total,
                (t.qty*t.price*t.IDR_rate) dpp,
                (t.qty*t.price*t.IDR_rate*0.1) ppn,
                (t.qty*t.price*t.IDR_rate*t.pph_persen) pph,
                (t.qty*t.price*t.IDR_rate*(1.1-t.pph_persen)) total_bayar
            FROM pbu_purchase_order AS po
                LEFT JOIN pbu_purchase_order_details AS pod ON po.po_id = pod.po_id
                LEFT JOIN pbu_supplier AS s ON s.supplier_id = po.supplier_id
                LEFT JOIN pbu_material AS m ON m.material_id = pod.material_id
                LEFT JOIN pbu_tipe_material ON pbu_tipe_material.tipe_material_id = po.tipe_material_id
                LEFT JOIN (
                        SELECT
                            tmd.po_id,
                            tmd.material_id,
                            tm.no_sj_supplier,
                            tm.no_inv_supplier,
                            tm.no_faktur_pajak,
                            tm.tgl_no_faktur_pajak,
                            tm.tgl_terima,
                            tm.tgl_inv_supplier,
                            tm.term_of_payment,
                            tmd.qty,
                            tmd.price,
                            tm.pph_persen,
                            tm.currency,
                            tm.IDR_rate,
                            pbu_pembayaran_supplier.tgl_bayar
                        FROM pbu_terima_material_details AS tmd
                            LEFT JOIN pbu_terima_material AS tm ON tm.terima_material_id = tmd.terima_material_id
                            LEFT JOIN pbu_pembayaran_supplier ON pbu_pembayaran_supplier.pembayaran_supplier_id = tm.pembayaran_supplier_id
                ) AS t ON t.po_id = pod.po_id AND t.material_id = pod.material_id
            WHERE
                po.tgl >= :from AND po.tgl <= :to
                " . ($supplier_id == 'All' ? "" : "AND po.supplier_id = '" . $supplier_id . "'") . "
            ORDER BY po.doc_ref, po.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
//--------------------------------------- REPORT MATERIAL ----------------------------------------------
    static function report_kartu_stok_Material($material_id, $from, $to, $tgl_expired = NULL, $no_lot = NULL, $no_qc = NULL, $supplier_id = NULL, $storloc)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                DATE_FORMAT(smm.trans_date, '%d %b %Y') tgl,
                smm.reference doc_ref,
                CASE smm.type_no
                    WHEN " . SJ_SUPPLIER . " THEN 'TERIMA DARI SUPPLIER'
                    WHEN " . TANDA_TERIMA . " THEN 'TERIMA DARI SUPPLIER'
                    WHEN " . RETURN_PEMBELIAN . " THEN 'KIRIM KE SUPPLIER'
                    WHEN " . SUPPIN . " THEN 'BARANG MASUK'
                    WHEN " . SUPPOUT . " THEN 'BARANG KELUAR'
                    WHEN " . STANDAR_KONSUMSI . " THEN 'KONSUMSI PRODUKSI'
                    WHEN " . ADDITIONAL_KONSUMSI . " THEN 'PENAMBAHAN KONSUMSI'
                    WHEN " . REDUCTION_KONSUMSI . " THEN 'PENGURANGAN KONSUMSI'
                    WHEN " . STOCK_ADJUST_INC . " THEN 'PENYESUAIAN STOK'
                    WHEN " . STOCK_ADJUST_DEC . " THEN 'PENYESUAIAN STOK'
                END mvt,
                0 `before`,
                IF(smm.qty >0,smm.qty,0) `in`,
                IF(smm.qty <0,-smm.qty,0) `out`,
                0 `after`,
                qty,
                m.sat
            FROM 
                pbu_stock_moves_material smm
                    LEFT JOIN pbu_material m ON m.material_id = smm.material_id
            WHERE 
                DATE(smm.trans_date) >= :from 
                AND DATE(smm.trans_date) <= :to
                AND smm.material_id = :material_id 
                AND smm.storage_location = '$storloc'
                " . ($tgl_expired ? "AND smm.tgl_expired = '$tgl_expired'" : "") . "
                " . ($no_lot ? "AND smm.no_lot = '$no_lot'" : "") . "
                " . ($no_qc ? "AND smm.no_qc = '$no_qc'" : "") . "
                " . ($supplier_id ? "AND smm.supplier_id = '$supplier_id'" : "") . "
            ORDER BY 
                smm.trans_date, smm.tdate
        ");
        return $comm->queryAll(true, array(
            ':material_id' => $material_id,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_kartu_stok_Sparepart($material_id, $from, $to, $supplier_id = NULL, $storloc)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                DATE_FORMAT(smm.trans_date, '%d %b %Y') tgl,
                smm.reference doc_ref,
                0 `before`,
                IF(smm.qty >0,DATE_FORMAT(smm.trans_date, '%d %b %Y'),NULL) tgl_in,
                IF(smm.qty >0,smm.qty,NULL) `in`,
                IF(smm.qty <0,DATE_FORMAT(smm.trans_date, '%d %b %Y'),NULL) tgl_out,
                IF(smm.qty <0,-smm.qty,NULL) `out`,
                0 `after`,
                qty,
                m.sat,
                pm.pelaksana petugas,
                pm.manager_spv spv
            FROM pbu_stock_moves_material smm
                LEFT JOIN pbu_material m ON m.material_id = smm.material_id
                LEFT JOIN pbu_perbaikan_mesin pm ON pm.perbaikan_mesin_id = smm.trans_no
            WHERE 
                DATE(smm.trans_date) >= :from 
                AND DATE(smm.trans_date) <= :to
                AND smm.material_id = :material_id 
                AND smm.storage_location = '$storloc'
                " . ($supplier_id ? "AND smm.supplier_id = '$supplier_id'" : "") . "
            ORDER BY 
                smm.trans_date, smm.tdate
        ");
        return $comm->queryAll(true, array(
            ':material_id' => $material_id,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_mutasi_stok_Material($from, $to, $storloc)
    {
//        if($_POST['USER_PRODUKSI']){
//            $comm = Yii::app()->db->createCommand("
//                SELECT 
//                    m.kode_material AS kode, 
//                    m.nama_material AS nama,
//                    ".($showExp?"DATE_FORMAT(smm.tgl_expired, '%e-%b-%Y') tgl_expired,":"")."
//                    SUM(IF (DATE(smm.trans_date) < :from, smm.qty, 0)) 'before',
//                    SUM(IF (smm.qty > 0 AND smm.type_no = ".SUPPOUT." AND DATE(smm.trans_date) >= :from
//                    AND DATE(smm.trans_date) <= :to, smm.qty, 0)) received,
//                    SUM(IF (smm.qty < 0 AND smm.type_no = 9999 AND DATE(smm.trans_date) >= :from
//                    AND DATE(smm.trans_date) <= :to, ABS(smm.qty), 0)) consumed,
//                    SUM(IF (smm.qty < 0 AND smm.type_no = ".SUPPIN." AND DATE(smm.trans_date) >= :from
//                    AND DATE(smm.trans_date) <= :to, ABS(smm.qty), 0)) returned,
//                    SUM(IF (DATE(smm.trans_date) <= :to, smm.qty, 0)) after
//                FROM
//                    (
//                        SELECT material_id, type_no, trans_date, -qty qty, tgl_expired
//                        FROM pbu_stock_moves_material
//                        WHERE type_no IN (34, 35)
//                    UNION ALL
//                        SELECT material_id, 9999 type_no, trans_date, SUM(qty) qty, tgl_expired
//                        FROM pbu_stock_moves_material
//                        WHERE
//                            type_no IN (34, 35) AND
//                            trans_no IN (SELECT transfer_material_id FROM pbu_transfer_material WHERE batch_id IN (SELECT batch_id FROM pbu.pbu_batch WHERE qty_mixing >0))
//                        GROUP BY material_id, tgl_expired
//                    ) AS smm
//                    LEFT  JOIN pbu_material m ON (smm.material_id = m.material_id)
//                GROUP BY 
//                        m.kode_material, m.nama_material".($showExp?", smm.tgl_expired":"")."
//                ORDER BY 
//                        m.material_id
//            ");
//        }else{
        $comm = Yii::app()->db->createCommand("
                SELECT 
                    m.kode_material kode, 
                    m.nama_material nama,
                    m.sat,
                    SUM(IF (DATE(smm.trans_date) < :from, smm.qty, 0)) 'before',
                    SUM(IF (smm.qty > 0 AND DATE(smm.trans_date) >= :from
                    AND DATE(smm.trans_date) <= :to, smm.qty, 0)) suppin,
                    SUM(IF (smm.qty < 0  AND DATE(smm.trans_date) >= :from
                    AND DATE(smm.trans_date) <= :to, ABS(smm.qty), 0)) suppout,
                    SUM(IF (DATE(smm.trans_date) <= :to, smm.qty, 0)) after
                FROM 
                    pbu_material m LEFT JOIN 
                    pbu_stock_moves_material AS smm ON (smm.material_id = m.material_id) AND smm.storage_location = '$storloc'
                GROUP BY 
                    m.kode_material, m.nama_material
                ORDER BY 
                    m.kode_material
            ");
//        }
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_laporan_produksi($from, $to, $report = FALSE)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                ba.no_bom,
                b.kode_barang,
                b.nama_barang,
                ba.kode_formula,
                b.nama_grup,
                b.qty_per_pot,
                b.qty_per_box,
                ba.tgl_produksi,
                ba.no_batch batch,
                ba.qty" . ($report ? "/1000 " : "") . " batch_size,
                pr.tgl tgl_prepare,
                ba.tgl_mixing,
                ba.qty_mixing" . ($report ? "/1000 " : "") . " qty_mixing,
                tb.tgl tgl_kirim,
                bh.no_batch no_lot,
                bh.qty qty_batch,
                IF(tb.tgl>:to OR tb.tgl IS NULL, 'BDP', '') status_bdp
            FROM
                pbu_batch_hasil bh
                INNER JOIN (SELECT ba.*, bo.no_bom, bo.kode_formula
                            FROM pbu_batch ba
                            LEFT JOIN pbu_bom bo ON ba.bom_id = bo.bom_id
                        ) ba ON ba.batch_id = bh.batch_id
                LEFT JOIN ( SELECT tbd.barang_id, tbd.batch no_batch, tb.tgl 
                            FROM pbu_transfer_barang_detail tbd
                            INNER JOIN pbu_transfer_barang tb
                            ON tbd.transfer_barang_id = tb.transfer_barang_id 
                        ) tb ON bh.no_batch = tb.no_batch
                LEFT JOIN ( SELECT MIN(tgl) tgl, batch_id
                            FROM pbu_transfer_material
                            GROUP BY batch_id
                        ) pr ON pr.batch_id = bh.batch_id
                LEFT JOIN ( SELECT b.*, g.nama_grup
                            FROM pbu_barang b
                            LEFT JOIN pbu_grup g ON b.grup_id = g.grup_id
                         ) b ON bh.barang_id = b.barang_id
            WHERE ba.tgl_mixing >= :from AND ba.tgl_mixing <= :to
        ");
        return $comm->queryAll(true, array(
            ':from' => $from,
            ':to' => $to
        ));
    }
//--------------------------------------- REPORT RAW MATERIAL ----------------------------------------------
    static function report_kartu_stok_Pack_Material($pm_id, $batch, $from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                DATE_FORMAT(nsm.trans_date, '%d %b %Y') tgl,
                nsm.reference doc_ref,
                nsm.batch,
                0 `before`,
                IF(nsm.qty >0,nsm.qty,0) `in`,
                IF(nsm.qty <0,-nsm.qty,0) `out`,
                0 `after`,
                nsm.qty
            FROM 
                pbu_stock_moves_pack_material AS nsm
            WHERE 
                DATE(nsm.trans_date) >= :from 
                AND DATE(nsm.trans_date) <= :to
                AND nsm.pm_id = :pm_id 
                AND nsm.batch = :batch
            ORDER BY 
                nsm.trans_date
        ");
        return $comm->queryAll(true, array(
            ':pm_id' => $pm_id,
            ':batch' => $batch,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_mutasi_stok_Pack_Material($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                nb.kode_pm AS kode, 
                nb.nama_pm AS nama,
                nsm.batch,
		SUM(IF (DATE(nsm.trans_date) < :from, nsm.qty, 0)) 'before',
                SUM(IF (nsm.qty > 0 AND nsm.type_no = 5 AND DATE(nsm.trans_date) >= :from
		  AND DATE(nsm.trans_date) <= :to, nsm.qty, 0)) terima,
		SUM(IF (nsm.qty < 0 AND nsm.type_no = 9 AND DATE(nsm.trans_date) >= :from
		  AND DATE(nsm.trans_date) <= :to, ABS(nsm.qty), 0)) sj,
		SUM(IF (nsm.qty < 0 AND nsm.type_no = 12 AND DATE(nsm.trans_date) >= :from
		  AND DATE(nsm.trans_date) <= :to, ABS(nsm.qty), 0)) returbeli,
                SUM(IF (DATE(nsm.trans_date) <= :to, nsm.qty, 0)) after
            FROM 
                pbu_pack_material AS nb 
                LEFT  JOIN 
                pbu_stock_moves_pack_material AS nsm ON (nsm.pm_id = nb.pm_id)
            GROUP BY 
                nb.kode_pm, 
                nb.nama_pm,
                nsm.batch 
            ORDER BY 
                nb.pm_id
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
//--------------------------------------- PRINT CASH/BANK TRANSFER ----------------------------------------------
    static function save_file($filename, $data)
    {
        if (defined('WRITE_TO_FILE') && WRITE_TO_FILE) {
            $fh = @fopen($filename, 'w');
            if ($fh === false) {
                throw new Exception('Failed create file.');
            }
            fwrite($fh, $data);
            fclose($fh);
        }
    }
    static function get_general_ledger($account_code, $from, $to)
    {
        $where = "";
        $param = array(
            ':account_code' => $account_code,
            ':from' => $from,
            ':to' => $to
        );
        $comm = Yii::app()->db->createCommand("
            SELECT 
                DATE_FORMAT(ngt.tran_date,'%d/%m/%Y') tgl,
                ngt.memo_,
                IF(ngt.amount > 0, ngt.amount, '') AS Debit,
                IF(ngt.amount < 0, -ngt.amount, '') AS Credit,
                0 AS Balance,
                ngt.amount
            FROM 
                pbu_gl_trans ngt
            WHERE 
                ngt.account_code = :account_code AND ngt.visible = 1 AND
                ngt.tran_date >= :from AND 
                ngt.tran_date <= :to $where
        ");
        return $comm->queryAll(true, $param);
    }
    static function get_gl_before($account_code, $date)
    {
        $where = "";
        $param = array(":account_code" => $account_code, ":date" => $date);
        $comm = Yii::app()->db->createCommand("
            SELECT 
                IFNULL(SUM(ngt.amount),0) 
            FROM 
                pbu_gl_trans ngt
            WHERE 
                ngt.account_code = :account_code AND 
                ngt.visible = 1 AND 
                ngt.tran_date < :date $where
        ");
        return $comm->queryScalar($param);
    }
    static function get_general_journal($from, $to)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        $comm = Yii::app()->db->createCommand("
        SELECT 
            DATE_FORMAT(ngt.tran_date,'%d/%m/%Y') tgl,
            ntt.tipe_name,
            pbu_refs.reference,
            ncm.account_code,
            ncm.account_name,
            ngt.memo_,IF(ngt.amount > 0, ngt.amount, '') AS Debit,
            IF(ngt.amount < 0, -ngt.amount, '') AS Credit
        FROM 
            pbu_gl_trans ngt
                INNER JOIN pbu_chart_master ncm
                    ON ngt.account_code = ncm.account_code
                LEFT JOIN pbu_trans_tipe ntt
                    ON ngt.type = ntt.tipe_id
                LEFT JOIN pbu_refs
                    ON ngt.type = pbu_refs.type_ AND ngt.type_no = pbu_refs.type_no
        WHERE 
            ngt.tran_date >= :from AND 
            ngt.visible = 1 AND 
            ngt.tran_date <= :to $where
        ORDER BY 
            ngt.tran_date,
            ngt.type_no
        ");
        return $comm->queryAll(true, $param);
    }
    static function get_bank_trans($from, $to, $bank_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                ntt.tipe_name,
                nbt.ref,
                DATE_FORMAT(nbt.tgl,'%d %b %Y') tgl,
                IF(nbt.amount >= 0,nbt.amount,0) Debit,
                IF(nbt.amount < 0,-nbt.amount,0) Credit, 
                nbt.amount,
                0 balance,
                nbt.amount
            FROM pbu_bank_trans AS nbt
                    INNER JOIN pbu_trans_tipe AS ntt ON nbt.type_ = ntt.tipe_id
            WHERE nbt.visible =1 AND nbt.tgl >= :from AND nbt.tgl <= :to AND nbt.bank_id = :bank_id
            ORDER BY nbt.tgl,nbt.tdate");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to, ':bank_id' => $bank_id));
    }
    static function num_to_month($n)
    {
        $hari = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        return $hari[--$n];
    }
    static function get_user_role()
    {
        $user = Users::model()->findByPk(user()->getId());
        return $user->security_roles_id;
    }
    static function report_rekap_Transfer_Material($from, $to, $pengirim, $penerima, $status, $show_detail)
    {
        if (!$show_detail) {
            $comm = Yii::app()->db->createCommand("
                SELECT
                    tm.tgl,
                    tm.doc_ref,
                    tm.pengirim,
                    tm.penerima,
                    tm.accepted status,
                    tm.note
                FROM pbu_transfer_material tm
                WHERE
                    tm.tgl >= :from AND tm.tgl <= :to
                    " . ($pengirim == 'All' ? "" : "AND tm.pengirim= " . $pengirim) . "
                    " . ($penerima == 'All' ? "" : "AND tm.penerima = " . $penerima) . "
                    " . ($status == 'All' ? "" : "AND tm.accepted = " . $status) . "
                ORDER BY tm.doc_ref, tm.tgl
            ");
        } else {
            $comm = Yii::app()->db->createCommand("
                SELECT
                    tm.tgl,
                    tm.doc_ref,
                    tm.pengirim,
                    tm.penerima,
                    tm.accepted status,
                    tm.note,
                    m.kode_material kode,
                    m.nama_material nama,
                    m.sat sat,
                    tmd.qty qty
                FROM pbu_transfer_material_detail tmd
                    LEFT JOIN pbu_transfer_material tm ON tm.transfer_material_id = tmd.transfer_material_id
                    LEFT JOIN pbu_material m ON m.material_id = tmd.material_id
                WHERE
                    tm.tgl >= :from AND tm.tgl <= :to
                    " . ($pengirim == 'All' ? "" : "AND tm.pengirim= " . $pengirim) . "
                    " . ($penerima == 'All' ? "" : "AND tm.penerima = " . $penerima) . "
                    " . ($status == 'All' ? "" : "AND tm.accepted = " . $status) . "
                ORDER BY tm.doc_ref, tm.tgl
            ");
        }
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_rekap_Transfer_Barang($from, $to, $pengirim, $penerima, $status, $show_detail)
    {
        if (!$show_detail) {
            $comm = Yii::app()->db->createCommand("
                SELECT
                    tm.tgl,
                    tm.doc_ref,
                    tm.pengirim,
                    tm.penerima,
                    tm.accepted status
                FROM pbu_transfer_barang tm
                WHERE
                    tm.tgl >= :from AND tm.tgl <= :to
                    " . ($pengirim == 'All' ? "" : "AND tm.pengirim= " . $pengirim) . "
                    " . ($penerima == 'All' ? "" : "AND tm.penerima = " . $penerima) . "
                    " . ($status == 'All' ? "" : "AND tm.accepted = " . $status) . "
                ORDER BY tm.doc_ref, tm.tgl
            ");
        } else {
            $comm = Yii::app()->db->createCommand("
                SELECT
                    tm.tgl,
                    tm.doc_ref,
                    tm.pengirim,
                    tm.penerima,
                    tm.accepted status,
                    m.kode_barang kode,
                    m.nama_barang nama,
                    m.sat sat,
                    tmd.qty qty
                FROM pbu_transfer_barang_detail tmd
                    LEFT JOIN pbu_transfer_barang tm ON tm.transfer_barang_id = tmd.transfer_barang_id
                    LEFT JOIN pbu_barang m ON m.barang_id = tmd.barang_id
                WHERE
                    tm.tgl >= :from AND tm.tgl <= :to
                    " . ($pengirim == 'All' ? "" : "AND tm.pengirim= " . $pengirim) . "
                    " . ($penerima == 'All' ? "" : "AND tm.penerima = " . $penerima) . "
                    " . ($status == 'All' ? "" : "AND tm.accepted = " . $status) . "
                ORDER BY tm.doc_ref, tm.tgl
            ");
        }
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_history_pembelian($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT
            pm.kode_material,pm.nama_material,pm.sat,ptbd.qty,ptbd.price,ptbd.total,ptb.tgl,ptb.doc_ref_inv
            FROM            pbu_material AS pm
            INNER JOIN pbu_terima_barang_details AS ptbd ON ptbd.barang_id = pm.material_id
            INNER JOIN pbu_terima_barang AS ptb ON ptbd.terima_barang_id = ptb.terima_barang_id
            WHERE ptb.doc_ref_inv IS NOT NULL
            ORDER BY            pm.kode_material ASC        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_giro()
    {
        $comm = Yii::app()->db->createCommand("SELECT pb.nama_bank,pg.giro_id,
            pg.no_giro,DATE_FORMAT(pg.tgl_klr,'%d %M %Y') tgl_klr,
            DATE_FORMAT(pg.tgl_jt,'%d %M %Y') tgl_jt,pg.bank_giro,pg.saldo,
            pg.tdate,pg.user_id,
            pg.tgl_cair,pg.bank_id,pg.used,pg.tolak,pg.notep_,pg.customer_id,
            pc.kode_customer,pc.nama_customer
            FROM pbu_giro AS pg
            INNER JOIN pbu_customers AS pc ON pg.customer_id = pc.customer_id
            LEFT JOIN pbu_bank AS pb ON pg.bank_id = pb.bank_id");
        return $comm->queryAll(true);
    }
    static function report_history_penjualan($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT
            pm.kode_material,pm.nama_material,pm.sat,psd.qty,psd.price,psd.totalpot,psd.total,ps.tgl_inv,ps.doc_ref_inv
            FROM            pbu_material AS pm
            INNER JOIN pbu_sj_details AS psd ON psd.barang_id = pm.material_id
            INNER JOIN pbu_sj AS ps ON psd.sj_id = ps.sj_id
            WHERE ps.doc_ref_inv IS NOT NULL
            ORDER BY            pm.kode_material ASC");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function piutang_due()
    {
        $comm = Yii::app()->db->createCommand("
    SELECT
	gh.customer_id,
	gh.nama_customer,
	SUM(gh.totl0) AS totl0,
	SUM(gh.totl1) AS totl1,
	SUM(gh.totl2) AS totl2,
	SUM(gh.totl3) AS totl3,
	SUM(gh.totalx) AS totalx,
	gh.due
FROM
	(
		SELECT
			hj.customer_id,
			hj.nama_customer,
			hj.doc_ref_inv,
			hj.tgl_jatuh_tempo,

		IF (
			hj.due >= 0
			AND hj.due <= 15,
			hj.total - hj.totalkas,
			0
		) AS totl0,

	IF (
		hj.due >= 16
		AND hj.due <= 30,
		hj.total - hj.totalkas,
		0
	) AS totl1,

IF (
	hj.due >= 30
	AND hj.due <= 60,
	hj.total - hj.totalkas,
	0
) AS totl2,

IF (
	hj.due > 60,
	hj.total - hj.totalkas,
	0
) AS totl3,
 hj.total - hj.totalkas AS totalx,
 hj.due
FROM
	(
		SELECT
			pc.customer_id,
			pc.nama_customer,
			pss.doc_ref_inv,
			IFNULL(qw.totalkas, 0) AS totalkas,
			pss.total,
			DATEDIFF(pss.tgl_jatuh_tempo, NOW()) AS due,
			pss.tgl_jatuh_tempo
		FROM
			pbu_sls_sj_inv_header AS pss
		INNER JOIN pbu_customers AS pc ON pss.customer_id = pc.customer_id
		LEFT JOIN (
			SELECT
				q.reference_item_id,
				SUM(kas_diterima) AS totalkas
			FROM
				pbu_customer_payment_detil AS q
			WHERE
				q.payment_tipe = 1
			GROUP BY
				reference_item_id
		) AS qw ON pss.sj_id = qw.reference_item_id
	) AS hj
WHERE
	hj.due > 0
	) AS gh
WHERE
	gh.totalx != 0
GROUP BY
	gh.customer_id
ORDER BY
	gh.nama_customer ASC");
        return $comm->queryAll(true);
    }
    static function hutang_due()
    {
        $comm = Yii::app()->db->createCommand("
    SELECT
	gh.supplier_id,
	gh.supplier_name,
	SUM(gh.totl0) AS totl0,
	SUM(gh.totl1) AS totl1,
	SUM(gh.totl2) AS totl2,
	SUM(gh.totl3) AS totl3,
	SUM(gh.totalx) AS totalx,
	gh.due
FROM
	(
		SELECT
			hj.supplier_id,
			hj.supplier_name,
			hj.no_invoice,
			hj.tgl_tempo,
		IF (
			hj.due >= 0
			AND hj.due <= 15,
			hj.total - hj.totalkas,
			0
		) AS totl0,
	IF (
		hj.due >= 16
		AND hj.due <= 30,
		hj.total - hj.totalkas,
		0
	) AS totl1,
IF (
	hj.due >= 30
	AND hj.due <= 60,
	hj.total - hj.totalkas,
	0
) AS totl2,
IF (
	hj.due > 60,
	hj.total - hj.totalkas,
	0
) AS totl3,
 hj.total - hj.totalkas AS totalx,
 hj.due
FROM
	(
SELECT
			pc.supplier_id,
			pc.supplier_name,
			pss.no_invoice,
			IFNULL(qw.totalkas, 0) AS totalkas,
			pss.total_inv as total,
			DATEDIFF(pss.tgl_tempo, NOW()) AS due,
			pss.tgl_tempo
		FROM
			pbu_pr_po_tb_inv AS pss
		INNER JOIN pbu_supplier AS pc ON pss.supplier_id = pc.supplier_id
		LEFT JOIN (
			SELECT
				q.reference_item_id,
				SUM(kas_dibayar) AS totalkas
			FROM
				pbu_supplier_payment_detil AS q
			WHERE
				q.payment_tipe = 1
			GROUP BY
				reference_item_id
		) AS qw ON pss.terima_barang_id = qw.reference_item_id
	) AS hj
) AS gh
WHERE
	gh.totalx != 0
GROUP BY
	gh.supplier_id
ORDER BY
	gh.supplier_name ASC

    ");
        return $comm->queryAll(true);
    }
}