<?php
Yii::import('application.models._base.BaseFaStockBdp');
class FaStockBdp extends BaseFaStockBdp
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_BDP_produksi($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                bh.barang_id,
                ba.bom_id,
                ba.batch_id,
                ba.no_batch no_batch,
                ba.qty batch_size,
                SUM(bh.qty) qty
            FROM
                pbu_batch_hasil bh
                INNER JOIN pbu_batch ba ON ba.batch_id = bh.batch_id
                LEFT JOIN ( SELECT tbd.barang_id, tbd.batch no_batch, tb.tgl 
                                FROM pbu_transfer_barang_detail tbd
                                INNER JOIN pbu_transfer_barang tb
                                ON tbd.transfer_barang_id = tb.transfer_barang_id
                            ) tb ON bh.no_batch = tb.no_batch AND bh.barang_id = tb.barang_id
            WHERE 
                ba.tgl_mixing >= :to AND ba.tgl_mixing <= :from
                AND tb.tgl > :from
                OR tb.tgl IS NULL
            GROUP BY bh.barang_id, ba.bom_id, ba.no_batch
        ");
        return $comm->queryAll(true, array(
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_stock_BDP($fin_acc_report_id, $sum = FALSE)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                " . ($sum ? "" : "
                    b.kode_barang,
                    b.nama_barang,
                    o.kode_formula,
                    t.batch_size/1000 batch_size_kg,
                    t.no_batch,
                    t.nilai_rm_per_batch,
                    t.nilai_pm_per_pce,
                    t.nilai_rm,
                    t.nilai_pm,
                ") . "
                " . ($sum ? "SUM(t.qty)" : "t.qty") . " qty,
                " . ($sum ? "SUM(t.total_bj)" : "t.total_bj") . " total_bj,
                " . ($sum ? "SUM(t.nilai_rm_pm)" : "t.nilai_rm_pm") . " nilai_rm_pm
            FROM pbu_fa_stock_bdp t
                " . ($sum ? "" : "LEFT JOIN pbu_barang b ON b.barang_id = t.barang_id
                            LEFT JOIN pbu_bom o ON o.bom_id = t.bom_id") . "
            WHERE fin_acc_report_id = :fin_acc_report_id
        ");
        $param = array(
            ':fin_acc_report_id' => $fin_acc_report_id
        );
        if ($sum)
            return $comm->queryRow(true, $param);
        else
            return $comm->queryAll(true, $param);
    }
    public function beforeValidate()
    {
        if ($this->fa_stock_bdp_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fa_stock_bdp_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function set_nilai_RM_PM_berdasarkan_BOM()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                nilai_rm_per_batch,
                nilai_pm_per_pce,
                total_bj
            FROM pbu_fa_bom_hpp_bj
            WHERE
                bom_id = :bom_id AND
                batch_id = :batch_id AND
                fin_acc_report_id = :fin_acc_report_id
        ");
        $nilai = $comm->queryRow(true, array(
            ':batch_id' => $this->batch_id,
            ':bom_id' => $this->bom_id,
            ':fin_acc_report_id' => $this->fin_acc_report_id
        ));
        If ($nilai) {
            $this->nilai_rm_per_batch = $nilai['nilai_rm_per_batch'];
            $this->nilai_pm_per_pce = $nilai['nilai_pm_per_pce'];
            $this->total_bj = $nilai['total_bj'];
            $nilai_rm = (float)$this->qty / (float)$this->total_bj * (float)$this->nilai_rm_per_batch;
            $nilai_pm = (float)$this->qty * (float)$this->nilai_pm_per_pce;
            $this->nilai_rm = $nilai_rm;
            $this->nilai_pm = $nilai_pm;
            $this->nilai_rm_pm = $nilai_rm + $nilai_pm;
        }
    }
}
