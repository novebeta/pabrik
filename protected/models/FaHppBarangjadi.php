<?php
Yii::import('application.models._base.BaseFaHppBarangjadi');
class FaHppBarangjadi extends BaseFaHppBarangjadi
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_total_hasil_produksi($from, $to, $report = FALSE)
    {
        //total produk hasil produksi
        $comm = Yii::app()->db->createCommand("
            SELECT
                t.barang_id,
                t.bom_id,
                count(t.batch_id) x_produksi,
                t.batch_size" . ($report ? "/1000 " : "") . " batch_size,
                SUM(t.qty) qty
                " . ($report ? ", t.no_bom, b.kode_barang, b.nama_barang " : "") . "
            FROM
                (
                    SELECT
                        bh.barang_id,
                        ba.bom_id,
                        ba.batch_id,
                        ba.qty batch_size,
                        SUM(bh.qty) qty
                        " . ($report ? ", ba.no_bom " : "") . "
                    FROM
                        pbu_batch_hasil bh
                        INNER JOIN (SELECT ba.*, bo.no_bom
                                        FROM pbu_batch ba
                                        LEFT JOIN pbu_bom bo ON ba.bom_id = bo.bom_id
                                    ) ba ON ba.batch_id = bh.batch_id
                    WHERE ba.tgl_mixing >= :from AND ba.tgl_mixing <= :to
                    GROUP BY bh.barang_id, ba.bom_id, ba.batch_id, ba.qty
                ) t
                " . ($report ? "LEFT JOIN pbu_barang b ON t.barang_id = b.barang_id" : "") . "
            GROUP BY t.bom_id
        ");
        return $comm->queryAll(true, array(
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_HPP_BJ($fin_acc_report_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                b.kode_barang,
                b.nama_barang,
                m.no_bom,
                t.batch_size/1000 batch_size,
                t.qty,
                t.x_produksi,
                t.nilai_rm_per_batch,
                t.nilai_rm_total,
                t.nilai_rm_per_pce,
                t.nilai_pm_per_pce,
                t.bop,
                t.hpp_per_pce,
                t.price,
                (t.price-t.hpp_per_pce)/t.price profit
            FROM pbu_fa_hpp_barangjadi t
                LEFT JOIN pbu_barang b ON t.barang_id = b.barang_id
                LEFT JOIN pbu_bom m ON t.bom_id = m.bom_id
            WHERE fin_acc_report_id = :fin_acc_report_id
        ");
        return $comm->queryAll(true, array(
            ':fin_acc_report_id' => $fin_acc_report_id
        ));
    }
    public function beforeValidate()
    {
        if ($this->fa_hpp_barangjadi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fa_hpp_barangjadi_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function hitungHPP_BarangJadi($bop_per_pce)
    {
        $this->set_nilai_RM_PM_berdasarkan_BOM();
        $total_nilai_RM = (int)$this->x_produksi * (float)$this->nilai_rm_per_batch;
        $this->nilai_rm_total = $total_nilai_RM;
        $this->nilai_rm_per_pce = $total_nilai_RM / (int)$this->qty;
        $this->hpp_per_pce = (float)$this->nilai_rm_per_pce + (float)$this->nilai_pm_per_pce + $bop_per_pce;
        $this->bop = $bop_per_pce;
        //Price belum diset
        //$this->price = 0;
        //$this->persen_profit = ((float)$this->price-(float)$this->hpp_per_pce)/(float)$this->price;
    }
    private function set_nilai_RM_PM_berdasarkan_BOM()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                nilai_rm_per_batch,
                nilai_pm_per_pce,
                total_bj
            FROM pbu_fa_bom_hpp_bj
            WHERE
                bom_id = :bom_id AND
                batch_size = :batch_size AND
                fin_acc_report_id = :fin_acc_report_id
        ");
        $nilai = $comm->queryRow(true, array(
            ':batch_size' => $this->batch_size,
            ':bom_id' => $this->bom_id,
            ':fin_acc_report_id' => $this->fin_acc_report_id
        ));
        If ($nilai) {
            $this->nilai_rm_per_batch = $nilai['nilai_rm_per_batch'];
            $this->nilai_pm_per_pce = $nilai['nilai_pm_per_pce'];
        } else {
            $this->nilai_rm_per_batch = 0;
            $this->nilai_pm_per_pce = 0;
        }
    }
}
