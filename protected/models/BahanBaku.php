<?php
Yii::import('application.models._base.BaseBahanBaku');
class BahanBaku extends BaseBahanBaku
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'material_id';
    }
}