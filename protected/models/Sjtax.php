<?php
Yii::import('application.models._base.BaseSjtax');
class Sjtax extends BaseSjtax
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function report_surat_jalan_tax()
    {
        $comm = Yii::app()->db->createCommand("SELECT
        pb.kode_barang,pb.nama_barang,psd.batch,psd.qty,
        pb.sat,DATE_FORMAT(psd.tgl_exp,'%m/%y') tgl_exp,
        psd.ket,psd.price,(psd.price*psd.qty) price_jual
        FROM pbu_sjtax_details AS psd
        INNER JOIN pbu_sjtax AS ps ON psd.sjtax_id = ps.sjtax_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        WHERE ps.sjtax_id = :sjtax_id");
        return $comm->queryAll(true, array(':sjtax_id' => $this->sjtax_id));
    }
    public function report_invoice_tax()
    {
        $comm = Yii::app()->db->createCommand("SELECT
        pb.kode_barang,pb.nama_barang,psd.batch,psd.qty,
        pb.sat,DATE_FORMAT(psd.tgl_exp,'%m/%y') tgl_exp,
        psd.ket,psd.price,(psd.price*psd.qty) price_jual
        FROM pbu_sjtax_details AS psd
        INNER JOIN pbu_sjtax AS ps ON psd.sjtax_id = ps.sjtax_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        WHERE ps.sjtax_id = :sjtax_id");
        return $comm->queryAll(true, array(':sjtax_id' => $this->sjtax_id));
    }
}