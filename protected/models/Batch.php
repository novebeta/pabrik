<?php
Yii::import('application.models._base.BaseBatch');
class Batch extends BaseBatch
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function setStatus($status, $batch_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_batch SET status = :status"
            . " WHERE batch_id = :batch_id");
        return $comm->execute(array(
            ':status' => $status,
            ':batch_id' => $batch_id
        ));
    }
    public static function get_stok_Barang_in_production($showBatch)
    {
        if ($showBatch == 1) $query = "
            SELECT 
                b.kode_barang kode,
                b.nama_barang nama,
                ba.no_batch,
                ba.qty_hasil qty
            FROM 
                pbu_batch ba, pbu_bom bo, pbu_barang b
            WHERE 
                ba.tgl_transfer_gudang IS NULL AND
                ba.qty_hasil > 0 AND
                ba.bom_id = bo.bom_id AND
                bo.barang_id = b.barang_id
            ORDER BY
                b.kode_barang
        ";
        else $query = "
            SELECT 
                b.kode_barang kode,
                b.nama_barang nama,
                SUM(ba.qty_hasil) qty
            FROM 
                pbu_batch ba, pbu_bom bo, pbu_barang b
            WHERE 
                ba.tgl_transfer_gudang IS NULL AND
                ba.qty_hasil > 0 AND
                ba.bom_id = bo.bom_id AND
                bo.barang_id = b.barang_id
            GROUP BY
                kode_barang
        ";
        $comm = Yii::app()->db->createCommand($query);
        return $comm->queryAll(TRUE);
    }
    public function beforeValidate()
    {
        if ($this->batch_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->batch_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
    public function get_prepared_items($tipe_material_id = NULL)
    {
        $tipe_material = "";
        if ($tipe_material_id) {
            $tipe_material = "AND t.tipe_material_id = $tipe_material_id";
        }
        if ($this->status == BATCH_OPEN) {
            $comm = Yii::app()->db->createCommand("
                SELECT
                    t.kode_material,
                    bd.*
                FROM pbu_batch_detail AS bd
                    INNER JOIN pbu_material AS t
                        ON t.material_id = bd.material_id
                        $tipe_material
                WHERE bd.batch_id = :batch_id
            ");
        } else {
            //jika udah prepare maka akan ditampilkan NO. QC nya
            $comm = Yii::app()->db->createCommand("
                SELECT
                    bd.material_id,
                    t.kode_material,
                    t.nama_material,
                    bd.sat,
                    t.qty,
                    t.tgl_expired,
                    t.no_qc,
                    t.no_lot
                FROM pbu_batch_detail bd
                    LEFT JOIN 
                        (SELECT
                            tm.tgl,
                            tmd.material_id,
                            m.kode_material,
                            m.nama_material,
                            m.sat,
                            SUM(IF(tm.pengirim = " . USER_PRODUKSI . ", -tmd.qty, tmd.qty)) qty,
                            tmd.tgl_expired,
                            tmd.no_qc,
                            tmd.no_lot,
                            m.tipe_material_id
                        FROM pbu_transfer_material_detail tmd
                            LEFT JOIN pbu_transfer_material tm ON tmd.transfer_material_id = tm.transfer_material_id
                            LEFT JOIN pbu_material m ON m.material_id = tmd.material_id
                        WHERE tm.batch_id = :batch_id
                        GROUP BY tmd.material_id, tmd.tgl_expired, tmd.no_qc, tmd.no_lot
                        ) t ON bd.material_id = t.material_id
                WHERE
                    bd.batch_id = :batch_id
                    $tipe_material
                ORDER BY t.kode_material, t.tgl
            ");
        }
        return $comm->queryAll(true, array(':batch_id' => $this->batch_id));
    }
    public function get_qty_bom_and_prepared()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                bd.material_id,
                t.kode_material,
                t.nama_material,
                bd.sat,
                bd.qty qty_bom,
                t2.qty_prepared qty_prepared,
                t2.supplier_id,
                t.qty qty,
                t.tgl_expired,
                t.no_qc,
                t.no_lot
            FROM pbu_batch_detail bd
                LEFT JOIN 
                    (SELECT
                        tm.tgl,
                        tmd.material_id,
                        m.kode_material,
                        m.nama_material,
                        SUM(IF(tm.pengirim = " . USER_PRODUKSI . ", -tmd.qty, tmd.qty)) qty,
                        tmd.tgl_expired,
                        tmd.no_qc,
                        tmd.no_lot,
                        m.tipe_material_id
                    FROM pbu_transfer_material_detail tmd
                        LEFT JOIN pbu_transfer_material tm ON tmd.transfer_material_id = tm.transfer_material_id
                        LEFT JOIN pbu_material m ON m.material_id = tmd.material_id
                    WHERE tm.batch_id = :batch_id
                    GROUP BY tmd.material_id, tmd.tgl_expired, tmd.no_qc, tmd.no_lot
                    ) t ON bd.material_id = t.material_id
                LEFT JOIN 
                    (SELECT
                        tm.tgl,
                        tmd.material_id,
                        tmd.supplier_id,
                        m.kode_material,
                        m.nama_material,
                        SUM(IF(tm.pengirim = 9, -tmd.qty, tmd.qty)) qty_prepared
                    FROM pbu_transfer_material_detail tmd
                        LEFT JOIN pbu_transfer_material tm ON tmd.transfer_material_id = tm.transfer_material_id
                        LEFT JOIN pbu_material m ON m.material_id = tmd.material_id
                    WHERE tm.batch_id = :batch_id
                    GROUP BY tmd.material_id
                    ) t2 ON bd.material_id = t2.material_id
            WHERE
                bd.batch_id = :batch_id
            ORDER BY t.kode_material, t.tgl
        ");
        return $comm->queryAll(true, array(':batch_id' => $this->batch_id));
    }
}