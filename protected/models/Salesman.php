<?php
Yii::import('application.models._base.BaseSalesman');
class Salesman extends BaseSalesman
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->salesman_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->salesman_id = $uuid;
        }
        return parent::beforeValidate();
    }
}