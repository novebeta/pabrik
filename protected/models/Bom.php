<?php
Yii::import('application.models._base.BaseBom');
class Bom extends BaseBom
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function getBomProduk($enable)
    {
        $where = "";
        if ($enable !== NULL) {
            $where = "WHERE bo.enable = $enable";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                bo.*,
                ba.kode_barang_rnd,
                ba.kode_barang,
                ba.kode_barang_2,
                ba.nama_barang,
                ba.grup_id,
                ba.qty_per_box,
                ba.qty_per_pot,
                ba.sat,
                ba.tipe_barang_id
            FROM pbu_bom bo
                LEFT JOIN pbu_barang ba ON bo.barang_id = ba.barang_id
            $where
        ");
        return $comm->queryAll();
    }
    public function beforeValidate()
    {
        if ($this->bom_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bom_id = $uuid;
        }
        $this->id_user = User()->getId();
        if ($this->tdate == null) {
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
}
