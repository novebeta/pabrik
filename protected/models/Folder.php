<?php
Yii::import('application.models._base.BaseFolder');
class Folder extends BaseFolder
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->folder_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->folder_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_folder WHERE SUBSTR(doc_ref FROM 6 FOR 4) = :tgl AND type_ = :type_ AND folder_id != :folder_id;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 2, 4),
                ':type_' => $this->type_,
                ':folder_id' => $this->folder_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        if ($this->type_ == 0) {
            $this->doc_ref = 'FASS/' . $tgl . '/' . $newinc;
        } else {
            $this->doc_ref = 'FBRE/' . $tgl . '/' . $newinc;
        }
    }
}