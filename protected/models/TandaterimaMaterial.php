<?php
Yii::import('application.models._base.BaseTandaterimaMaterial');
class TandaterimaMaterial extends BaseTandaterimaMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function GetReturnedItems($return_pembelian_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                rpd.material_id,
                m.kode_material,
                m.nama_material,
                m.sat,
                rpd.tgl_expired,
                IFNULL(SUM(ttmd.qty),0) qty_terima
            FROM pbu_return_pembelian_details rpd
				LEFT JOIN pbu_tandaterima_material ttm ON ttm.return_pembelian_id = rpd.return_pembelian_id
                LEFT JOIN pbu_tandaterima_material_details ttmd ON ttmd.tandaterima_id = ttm.tandaterima_id AND ttmd.material_id = rpd.material_id
                LEFT JOIN pbu_material m ON rpd.material_id = m.material_id
            WHERE
                rpd.return_pembelian_id = :return_pembelian_id
            GROUP BY material_id
        ");
        return $comm->queryAll(true, array(':return_pembelian_id' => $return_pembelian_id));
    }
    public function beforeValidate()
    {
        if ($this->tandaterima_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->tandaterima_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
}
