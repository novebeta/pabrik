<?php
Yii::import('application.models._base.BaseGlTrans');
class GlTrans extends BaseGlTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function jurnal_umum_index($limit, $offset, $tgl = null)
    {
        $where = '';
        $params = [':type' => JURNAL_UMUM];
        if ($tgl != null) {
            $where = ' AND ngt.tran_date = :tgl';
            $params[':tgl'] = $tgl;
        }
//        $comm = Yii::app()->db->createCommand("SELECT
//        ngt.type_no,ngt.tran_date,ngt.amount tot_debit,ngt.amount tot_kredit,
//        pbu_refs.reference
//        FROM pbu_gl_trans AS ngt
//        LEFT JOIN pbu_refs ON ngt.type = pbu_refs.type_ AND ngt.type_no = pbu_refs.type_no
//        WHERE ngt.type = :type AND ngt.amount > 0 AND ngt.tran_date = :tgl AND ngt.visible = 1");
        $comm = Yii::app()->db->createCommand("
            SELECT
                ngt.type_no,ngt.tran_date,ngt.memo_,
                SUM(if(ngt.amount > 0, ngt.amount, 0)) tot_debit,
                -SUM(if(ngt.amount < 0, ngt.amount, 0)) tot_kredit,
                pbu_refs.reference
            FROM 
                pbu_gl_trans AS ngt LEFT JOIN pbu_refs ON ngt.type =  pbu_refs.type_ AND 
                ngt.type_no = pbu_refs.type_no
            WHERE 
                ngt.type = :type AND 
                ngt.visible = 1 $where
            GROUP BY
                ngt.type_no            
        ");
        // LIMIT $limit OFFSET $offset
        return $comm->queryAll(true, $params);
    }
    public static function jurnal_umum_details($type_no)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ngt.type_no,ngt.tran_date,ngt.amount,
        if(ngt.amount >= 0,ngt.amount,0) debit,
        if(ngt.amount < 0,-ngt.amount,0) kredit,
        ngt.counter,ngt.memo_,ngt.account_code
        FROM pbu_gl_trans AS ngt
        WHERE ngt.type = :type AND ngt.type_no = :type_no AND ngt.visible = 1");
        return $comm->queryAll(true, array(':type' => JURNAL_UMUM, ':type_no' => $type_no));
    }
    public static function generete_primary_key($tipe)
    {
        $res = Yii::app()->db->createCommand("
        select IFNULL(MAX(pgt.type_no),0)
        from pbu_gl_trans pgt where pgt.type = :type");
        $counter = $res->queryScalar(array(':type' => $tipe));
        $counter++;
        return $counter;
    }
    public static function get_reference($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = :type_");
        $criteria->addCondition("type_no = :type_no");
        $criteria->params = array(':type_' => $type, ':type_no' => $type_no);
        $model = Refs::model()->find($criteria);
        if ($model == null) {
            return null;
        }
        return $model->reference;
    }
    public function beforeValidate()
    {
        if ($this->counter == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->counter = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }
}