<?php
Yii::import('application.models._base.BaseConversiMaterialDetail');
class ConversiMaterialDetail extends BaseConversiMaterialDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        //if ($this->conversi_material_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->conversi_material_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
