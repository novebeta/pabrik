<?php
Yii::import('application.models._base.BaseFaBop');
class FaBop extends BaseFaBop
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_pengiriman_produksi($from, $to, $report = FALSE)
    {
        //hasil produksi adalah Barang Jadi yang telah dikirim ke gudang
        $comm = Yii::app()->db->createCommand("
            SELECT
                tbd.barang_id,
                tbd.qty
                " . ($report ? ", b.kode_barang, b.nama_barang " : "") . "
            FROM pbu_transfer_barang_detail tbd
		INNER JOIN pbu_transfer_barang tb ON tbd.transfer_barang_id = tb.transfer_barang_id
                " . ($report ? "LEFT JOIN pbu_barang b ON tbd.barang_id = b.barang_id" : "") . "
            WHERE
                tb.pengirim = :pengirim AND tb.penerima = :penerima AND
                tb.tgl >= :from AND tb.tgl <= :to
            GROUP BY
                tbd.barang_id
        ");
        return $comm->queryAll(true, array(
            ':pengirim' => USER_PRODUKSI,
            ':penerima' => USER_GPJ,
            ':from' => $from,
            ':to' => $to
        ));
    }
    public static function get_total_pengiriman_produksi($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                SUM(tbd.qty) qty
            FROM pbu_transfer_barang_detail tbd
		INNER JOIN pbu_transfer_barang tb ON tbd.transfer_barang_id = tb.transfer_barang_id
            WHERE
                tb.pengirim = :pengirim AND tb.penerima = :penerima AND
                tb.tgl >= :from AND tb.tgl <= :to
        ");
        return $comm->queryScalar(array(
            ':pengirim' => USER_PRODUKSI,
            ':penerima' => USER_GPJ,
            ':from' => $from,
            ':to' => $to
        ));
    }
    static function report_BiayaOverheadProduksi($fin_acc_report_id, $sum = FALSE)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                " . ($sum ? "" : "b.kode_barang,") . "
                " . ($sum ? "SUM(bop.qty)" : "bop.qty") . " qty,
                " . ($sum ? "SUM(bop.biaya_tenagakerja_tidak_langsung)" : "bop.biaya_tenagakerja_tidak_langsung") . " biaya_tenagakerja_tidak_langsung,
                " . ($sum ? "SUM(bop.biaya_listrik)" : "bop.biaya_listrik") . " biaya_listrik,
                " . ($sum ? "SUM(bop.biaya_penyusutan_mesin)" : "bop.biaya_penyusutan_mesin") . " biaya_penyusutan_mesin,
                " . ($sum ? "SUM((bop.biaya_tenagakerja_tidak_langsung+bop.biaya_listrik+bop.biaya_penyusutan_mesin))" : "(bop.biaya_tenagakerja_tidak_langsung+bop.biaya_listrik+bop.biaya_penyusutan_mesin)") . " total_bop,
                " . ($sum ? "SUM(bop.bop)" : "bop.bop") . " bop_per_pcs
            FROM pbu_fa_bop bop
                LEFT JOIN pbu_barang b ON bop.barang_id = b.barang_id
            WHERE fin_acc_report_id = :fin_acc_report_id
        ");
        $param = array(
            ':fin_acc_report_id' => $fin_acc_report_id
        );
        if ($sum) return $comm->queryRow(true, $param);
        else return $comm->queryAll(true, $param);
    }
    public function beforeValidate()
    {
        if ($this->fa_bop_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fa_bop_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function hitungBOP($totalHasil, $biaya_tenagakerja_tidak_langsung, $biaya_listrik, $biaya_penyusutan_mesin)
    {
        $persentase = (float)$this->qty / (float)$totalHasil;
        $this->biaya_tenagakerja_tidak_langsung = $persentase * (float)$biaya_tenagakerja_tidak_langsung;
        $this->biaya_listrik = $persentase * (float)$biaya_listrik;
        $this->biaya_penyusutan_mesin = $persentase * (float)$biaya_penyusutan_mesin;
        $totalBOP = (float)$this->biaya_tenagakerja_tidak_langsung + (float)$this->biaya_listrik + (float)$this->biaya_penyusutan_mesin;
        $this->bop = $totalBOP / (float)$this->qty;
    }
}
