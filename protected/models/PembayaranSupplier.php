<?php
Yii::import('application.models._base.BasePembayaranSupplier');
class PembayaranSupplier extends BasePembayaranSupplier
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_InvoiceAndNotaReturn()
    {
        $query = "";
        $query2 = "";
        if (isset($_POST['pembayaran_supplier_id'])) {
            $query = " OR pembayaran_supplier_id = '" . $_POST['pembayaran_supplier_id'] . "' ";
            $query2 = " OR nota_return_pembayaran_supplier_id = '" . $_POST['pembayaran_supplier_id'] . "' ";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                terima_material_id temporary_id,
                no_inv_supplier display_no,
                tgl_inv_supplier display_tgl,
                terima_material_id,
                no_inv_supplier,
                tgl_inv_supplier,
                NULL return_pembelian_id,
                NULL no_nota_return,
                NULL tgl_nota_return,
                dpp,
                tax ppn,
                pph,
                total_bayar
            FROM pbu_terima_material
            WHERE
                supplier_id = :supplier_id
                AND status = :status_invoicesupplier
                $query
            UNION ALL
            SELECT
                return_pembelian_id temporary_id,
                nota_return_doc_ref display_no,
                nota_return_tgl display_tgl,
                terima_material_id,
                NULL no_inv_supplier,
                NULL tgl_inv_supplier,
                return_pembelian_id,
                nota_return_doc_ref no_nota_return,
                nota_return_tgl tgl_nota_return,
                -nota_return_dpp dpp,
                -nota_return_ppn ppn,
                0 pph,
                -(nota_return_dpp+nota_return_ppn) total_bayar
            FROM pbu_return_pembelian
            WHERE
                    supplier_id = :supplier_id
                AND nota_return_status = :status_notareturn
                $query2
            ");
        return $comm->queryAll(true, array(
            ':supplier_id' => $_POST['supplier_id'],
            ':status_invoicesupplier' => SJ_SUPPLIER_INVOICED,
            ':status_notareturn' => NOTA_RETURN_OPEN
        ));
    }
    public function beforeValidate()
    {
        if ($this->pembayaran_supplier_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pembayaran_supplier_id = $uuid;
        }
        if ($this->tdate == null) {
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        if ($this->userid == null) {
            $this->userid = User()->getId();
        }
        if ($this->tgl_bayar != null) {
            $command4 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command4->queryScalar();
            $this->tdate_bayar = $dt;
            $this->userid_bayar = User()->getId();
        }
        return parent::beforeValidate();
    }
    public function getItems()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                dpp, ppn, materai, total, pph, total_bayar,
                IF(return_pembelian_id IS NULL, no_inv_supplier, no_nota_return) no_inv_supplier,
                DATE_FORMAT(IF(return_pembelian_id IS NULL, tgl_inv_supplier, tgl_nota_return), '%e-%b-%y') tgl_inv_supplier
            FROM pbu_pembayaran_supplier_details
            WHERE pembayaran_supplier_id = :pembayaran_supplier_id
        ");
        return $comm->queryAll(true, array(':pembayaran_supplier_id' => $this->pembayaran_supplier_id));
    }
    public function getList_terima_material_id()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT DISTINCT terima_material_id
            FROM pbu_pembayaran_supplier_details
            WHERE pembayaran_supplier_id = :pembayaran_supplier_id
        ");
        return $comm->queryAll(true, array(':pembayaran_supplier_id' => $this->pembayaran_supplier_id));
    }
}