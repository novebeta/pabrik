<?php

Yii::import('application.models._base.BaseReturSalesDetails');

class ReturSalesDetails extends BaseReturSalesDetails
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'sales_detail_id';
    }
}