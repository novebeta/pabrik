<?php
Yii::import('application.models._base.BaseAssembly');
class Assembly extends BaseAssembly
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->assembly_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->assembly_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->assembly_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->assembly_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_assembly WHERE SUBSTR(doc_ref FROM 5 FOR 4) = :tgl AND assembly_id != :assembly_id ;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':assembly_id' => $this->assembly_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'ASM/' . $tgl . '/' . $newinc;
    }
    public function getItems()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT (@num := @num+1) AS num,pbd.material_id,pbd.qty,pm.kode_material,pm.nama_material,pm.sat,pbd.loc_code
            FROM pbu_assembly_details AS pbd
                INNER JOIN pbu_material AS pm ON pbd.material_id = pm.material_id,(SELECT @num:=0) AS y
            WHERE pbd.assembly_id = :assembly_id");
        return $comm->queryAll(true, [':assembly_id' => $this->assembly_id]);
    }
    public static function urutDocRef($month, $year)
    {
        Yii::app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var Assembly[] $ass */
            $criteria = new CDbCriteria();
            $criteria->addCondition("MONTH(tgl) = :month AND YEAR(tgl) = :year");
            $params = [
                ':month' => $month,
                ':year' => $year
            ];
            $criteria->order = "tgl ASC,tdate ASC";
            $criteria->params = $params;
            $ass = Assembly::model()->findAll($criteria);
            $nextval = 1;
            foreach ($ass as $a) {
                $fmt = '%05d';
                $newinc = sprintf($fmt, $nextval);
                $tgl = sql2date($a->tgl, 'yyMMdd');
                $a->doc_ref = 'ASM/' . $tgl . '/' . $newinc;
                if (!$a->save(false)) {
                    throw new Exception('Assembly gagal di simpan.' . "\n");
                }
                Refs::model()->updateAll(['reference' => $a->doc_ref], "type_no = :type_no",
                    array(':type_no' => $a->assembly_id));
                BankTrans::model()->updateAll(['ref' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->assembly_id]);
                StockMovesMaterial::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->assembly_id]);
                Apar::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->assembly_id]);
                $nextval++;
                echo $a->doc_ref . "\n";
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            echo $ex->getMessage();
        }
        Yii::app()->db->autoCommit = true;
    }
}