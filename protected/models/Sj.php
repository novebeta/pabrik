<?php
Yii::import('application.models._base.BaseSj');
class Sj extends BaseSj
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->sj_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sj_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_sj WHERE SUBSTR(doc_ref FROM 4 FOR 4) = :tgl AND sj_id != :sj_id;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':sj_id' => $this->sj_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'SJ/' . $tgl . '/' . $newinc;
        $this->doc_ref_inv = str_replace('SJ', 'INV/FOA', $this->doc_ref);
    }
    public static function urutDocRef($month, $year)
    {
        Yii::app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var Sj[] $ass */
            $criteria = new CDbCriteria();
            $criteria->addCondition("MONTH(tgl) = :month AND YEAR(tgl) = :year");
            $params = [
                ':month' => $month,
                ':year' => $year
            ];
            $criteria->order = "tgl ASC";
            $criteria->params = $params;
            $ass = Sj::model()->findAll($criteria);
            $nextval = 1;
            foreach ($ass as $a) {
                $fmt = '%05d';
                $newinc = sprintf($fmt, $nextval);
                $tgl = sql2date($a->tgl, 'yyMMdd');
                $a->doc_ref = 'SJ/' . $tgl . '/' . $newinc;
                $a->doc_ref_inv = str_replace('SJ', 'INV/FOA', $a->doc_ref);;
                if (!$a->save(false)) {
                    throw new Exception('Sj gagal di simpan.' . "\n");
                }
                Refs::model()->updateAll(['reference' => $a->doc_ref],
                    "type_ = :type_ AND type_no = :type_no",
                    [':type_' => SURAT_JALAN, ':type_no' => $a->sj_id]);
                Refs::model()->updateAll(['reference' => $a->doc_ref_inv],
                    "type_ = :type_ AND type_no = :type_no",
                    [':type_' => PENJUALAN, ':type_no' => $a->sj_id]);
                BankTrans::model()->updateAll(['ref' => $a->doc_ref_inv],
                    'trans_no = :trans_no',
                    [':trans_no' => $a->sj_id]);
                StockMovesMaterial::model()->updateAll(['reference' => $a->doc_ref],
                    'type_no = :type_no AND trans_no = :trans_no',
                    [':type_no' => SURAT_JALAN, ':trans_no' => $a->sj_id]);
                Apar::model()->updateAll(['reference' => $a->doc_ref_inv],
                    'trans_no = :trans_no',
                    [':trans_no' => $a->sj_id]);
                $nextval++;
                echo $a->doc_ref . " => " . $a->doc_ref_inv . "\n";
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            echo $ex->getMessage();
        }
        Yii::app()->db->autoCommit = true;
    }
    public function updateTgl($tgl, $ganti_bulan)
    {
        $this->tgl = $tgl;
        $this->createDocRef($ganti_bulan);
        if (!$this->save()) {
            throw new Exception(CHtml::errorSummary($this));
        }
        StockMovesMaterial::model()->updateAll(['trans_date' => $tgl, 'reference' => $this->doc_ref],
            "type_no = :type_no AND trans_no = :trans_no",
            array(':type_no' => SURAT_JALAN, ':trans_no' => $this->sj_id));
        GlTrans::model()->updateAll(['tran_date' => $tgl], "type = :type_no AND type_no = :trans_no",
            array(':type_no' => SURAT_JALAN, ':trans_no' => $this->sj_id));
        Refs::model()->updateAll(['reference' => $this->doc_ref], "type_ = :type_ AND type_no = :type_no",
            array(':type_' => SURAT_JALAN, ':type_no' => $this->sj_id));
    }
    public function beforeValidate()
    {
        if ($this->sj_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sj_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function create_simulation()
    {
        if (count($this->sjsims) > 0) {
            foreach ($this->sjsims as $simold) {
                SjDetails::model()->deleteAll("sjsim_id = :sjsim_id",
                    array(':sjsim_id' => $simold->sjsim_id));
                $simold->delete();
            }
        }
        $sim = new Sjsim;
        $sim->sj_id = $this->sj_id;
        $sim->tgl = $this->tgl;
        $sim->customer_id = $this->customer_id;
        $sim->doc_ref = "SIM" . $this->doc_ref;
        if (!$sim->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Simulasi Surat Jalan')) .
                CHtml::errorSummary($sim));
        }
        $total = 0;
        foreach ($this->sjDetails as $detail) {
            $sjsim_detail = new SjsimDetails;
            $sjsim_detail->barang_id = $detail->barang_id;
            $sjsim_detail->batch = $detail->batch;
            $sjsim_detail->tgl_exp = $detail->tgl_exp;
            $sjsim_detail->ket = $detail->ket;
            $sjsim_detail->qty = $detail->qty;
            $sjsim_detail->price = $detail->barang->get_harga_wil_tax($sim->customer->wil_tax_id);
            $sjsim_detail->total = $sjsim_detail->qty * $sjsim_detail->price;
            $total += $sjsim_detail->total;
            $sjsim_detail->sjsim_id = $sim->sjsim_id;
            if (!$sjsim_detail->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detil Simulasi Surat Jalan')) .
                    CHtml::errorSummary($sjsim_detail));
            }
        }
        $sim->total = $total;
        if (!$sim->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Simulasi Surat Jalan')) .
                CHtml::errorSummary($sim));
        }
    }
    public function create_IntTax($persen)
    {
//        $sjint = new Sjint;
        $sjtax = new Sjtax;
//        $sjint->sj_id = $sjtax->sj_id = $this->sj_id;
//        $sjint->sjsim_id = $sjtax->sjsim_id = $this->sjsims[0]->sjsim_id;
//        $sjint->tgl = $sjtax->tgl = $this->tgl;
//        $sjint->customer_id = $sjtax->customer_id = $this->customer_id;
//        $sjint->doc_ref = "INT" . $this->doc_ref;
        $sjtax->doc_ref = "TAX" . $this->doc_ref;
        if (!$sjtax->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Pajak')) .
                CHtml::errorSummary($sjtax));
        }
//        if (!$sjint->save()) {
//            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Internal')) .
//                CHtml::errorSummary($sjint));
//        }
        $total_tax = $total_int = 0;
        foreach ($this->sjDetails as $detail) {
            $sjint_detail = new SjintDetails;
            $sjtax_detail = new SjtaxDetails;
            $sjtax_detail->barang_id = $sjint_detail->barang_id = $detail->barang_id;
            $sjtax_detail->batch = $sjint_detail->batch = $detail->batch;
            $sjtax_detail->tgl_exp = $sjint_detail->tgl_exp = $detail->tgl_exp;
            $sjtax_detail->ket = $sjint_detail->ket = $detail->ket;
            $qty_tax = ceil($detail->qty * $persen);
            $qty_int = $detail->qty - $qty_tax;
            $sjtax_detail->qty = $qty_tax;
            $sjint_detail->qty = $qty_int;
            $sjtax_detail->price = $detail->barang->get_harga_wil_tax($sjtax->customer->wil_tax_id);
//            $sjint_detail->price = $detail->barang->get_harga_wil_int($sjint->customer->wil_int_id);
            $sjtax_detail->total = $sjtax_detail->qty * $sjtax_detail->price;
            $sjint_detail->total = $sjint_detail->qty * $sjint_detail->price;
            $total_tax += $sjtax_detail->total;
            $total_int += $sjint_detail->total;
            $sjtax_detail->sjtax_id = $sjtax->sjtax_id;
//            $sjint_detail->sjint_id = $sjint->sjint_id;
            if (!$sjtax_detail->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detil Surat Jalan Pajak')) .
                    CHtml::errorSummary($sjtax_detail));
            }
            if (!$sjint_detail->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detil Surat Jalan Internal')) .
                    CHtml::errorSummary($sjint_detail));
            }
        }
        $sjtax->total = $total_tax;
//        $sjint->total = $total_int;
        if (!$sjtax->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Pajak')) .
                CHtml::errorSummary($sjtax));
        }
//        if (!$sjint->save()) {
//            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Internal')) .
//                CHtml::errorSummary($sjint));
//        }
    }
    public function report_surat_jalan()
    {
        $comm = Yii::app()->db->createCommand("SELECT
        pb.kode_barang,pb.nama_barang,psd.batch,psd.qty,
        pb.sat,DATE_FORMAT(psd.tgl_exp,'%m/%y') tgl_exp,psd.ket
        FROM pbu_sj_details AS psd
        INNER JOIN pbu_sj AS ps ON psd.sj_id = ps.sj_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        WHERE ps.sj_id = :sj_id");
        return $comm->queryAll(true, array(':sj_id' => $this->sj_id));
    }
    public function report_invoice_int()
    {
        $comm = Yii::app()->db->createCommand("SELECT
        pb.kode_barang,pb.nama_barang,psd.batch,psd.qty,
        pb.sat,DATE_FORMAT(psd.tgl_exp,'%m/%y') tgl_exp,psd.ket,psd.price,(psd.bruto) price_jual,
        psd.totalpot
        FROM pbu_sj_details AS psd
        INNER JOIN pbu_sj AS ps ON psd.sj_id = ps.sj_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        WHERE ps.sj_id = :sj_id");
        return $comm->queryAll(true, array(':sj_id' => $this->sj_id));
    }
    public function report_surat_jalan_tax()
    {
        $comm = Yii::app()->db->createCommand("SELECT
        pb.kode_barang,psd.ket,psd.batch,psd.tgl_exp,psd.qty,
        psd.price,psd.total
        FROM pbu_sjtax_details AS psd
        INNER JOIN pbu_sjtax AS ps ON psd.sjtax_id = ps.sjtax_id
        INNER JOIN pbu_barang AS pb ON psd.barang_id = pb.barang_id
        WHERE ps.sj_id = :sj_id");
        return $comm->queryAll(true, array(':sj_id' => $this->sj_id));
    }
}