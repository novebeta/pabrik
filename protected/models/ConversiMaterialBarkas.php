<?php
Yii::import('application.models._base.BaseConversiMaterialBarkas');
class ConversiMaterialBarkas extends BaseConversiMaterialBarkas
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_availableStock_sparepart($material_id = null, $storloc)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                Sum(smm.qty) qty
            FROM pbu_stock_moves_material smm
                LEFT JOIN pbu_material m ON smm.material_id = m.material_id
            WHERE
                smm.storage_location = :storloc
                AND smm.material_id = :material_id
            ");
        return $comm->queryScalar(array(
            ':material_id' => $material_id,
            ':storloc' => $storloc
        ));
    }
    public function beforeValidate()
    {
        if ($this->conversi_material_barkas_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->conversi_material_barkas_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
}
