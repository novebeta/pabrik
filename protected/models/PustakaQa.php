<?php
Yii::import('application.models._base.BasePustakaQa');
class PustakaQa extends BasePustakaQa
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function masterlist_dokumen_internal($activity, $tipe, $aktif, $tidakaktif)
    {
        $active = "";
        if ($aktif && !$tidakaktif) {
            $active = "AND active = 1";
        } else if (!$aktif && $tidakaktif) {
            $active = "AND active = 0";
        }
        $param = array(':activity' => $activity, ':tipe' => $tipe);
        $comm = Yii::app()->db->createCommand("
            SELECT
                qr.pustaka_qa_id,
                q.nomor,
                q.judul,
                qr.revisi_ke,
                qr.kode_revisi,
                DATE_FORMAT(qr.tgl_efektif, '%d-%m-%Y') tgl_efektif,
                '' tgl_rev0, '' tgl_rev1, '' tgl_rev2, '' tgl_rev3, '' tgl_rev4, '' tgl_rev5
            FROM pbu_pustaka_qa_revisi qr
                    LEFT JOIN pbu_pustaka_qa q ON qr.pustaka_qa_id = q.pustaka_qa_id
            WHERE
                pustaka_qa_activity_id = :activity AND
                pustaka_qa_tipe_id = :tipe
                $active
            ORDER BY q.nomor, qr.revisi_ke");
        return $comm->queryAll(true, $param);
    }
    public function beforeValidate()
    {
        if ($this->pustaka_qa_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pustaka_qa_id = $uuid;
        }
        return parent::beforeValidate();
    }
}
