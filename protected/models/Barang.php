<?php
Yii::import('application.models._base.BaseBarang');
class Barang extends BaseBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_barang($grup_id = null)
    {
        $where = "";
        if ($grup_id != null) {
            $where = "WHERE pb.grup_id = '$grup_id'";
        }
        $comm = Yii::app()->db->createCommand("SELECT pb.barang_id, pb.kode_barang,
        pb.nama_barang,pb.ket,pb.sat,pb.grup_id,pb.qty_per_pot,pb.qty_per_pot,psm.batch,
        psm.tgl_exp,Sum(psm.qty) AS qty
        FROM pbu_stock_moves AS psm
        INNER JOIN pbu_barang AS pb ON psm.barang_id = pb.barang_id
        $where
        GROUP BY pb.barang_id,pb.kode_barang,pb.nama_barang,pb.sat,pb.grup_id,pb.qty_per_box,pb.qty_per_pot,psm.batch");
        return $comm->queryAll(true);
    }
    public static function get_batch($barang_id = null, $storloc)
    {
        /*
         * Dipakai saat :
         * - Terima barang
         * - SJ
         * - Retur Beli
         */
        $where = "";
        if ($barang_id != null) {
            $where = "AND pb.barang_id = '$barang_id'";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                psm.barang_id,
                pb.kode_barang,
                pb.nama_barang,
                pb.ket,
                pb.sat,
                pb.grup_id,
                pb.qty_per_pot,
                pb.qty_per_box,
                psm.batch,
                psm.tgl_exp,
                DATE_FORMAT(psm.tgl_exp, '%m-%Y') tgl_exp_group,
                Sum(psm.qty) AS qty
        FROM pbu_stock_moves AS psm
            INNER JOIN pbu_barang AS pb ON psm.barang_id = pb.barang_id
        WHERE
            psm.storage_location = '$storloc'
            $where
        GROUP BY
            psm.barang_id, psm.batch
        HAVING qty != 0
        ");
        return $comm->queryAll(true);
    }
    public static function get_availabel_barang($barang_id, $storloc)
    {
        /*
         * Dipakai saat :
         * - Transfer Barang
         */
        $comm = Yii::app()->db->createCommand("
            SELECT
                sm.barang_id,
                sm.tgl_exp,
                sm.batch,
                SUM(qty) qty,
                DATE_FORMAT(sm.tgl_exp, '%Y-%m') tgl_exp_group
            FROM pbu_stock_moves sm
            WHERE
                sm.barang_id = '$barang_id'
                AND sm.storage_location = '$storloc'
            GROUP BY
                sm.barang_id, sm.batch
            HAVING
                SUM(qty) <> 0
        ");
        return $comm->queryAll(true);
    }
    public static function get_availabel_barang_before($barang_id, $trans_no, $storloc)
    {
        /*
         * Dipakai saat :
         * - Edit Transfer Barang
         */
        $comm = Yii::app()->db->createCommand("
            SELECT
                tgl_exp,
                batch,
                SUM(qty) qty,
                DATE_FORMAT(tgl_exp, '%Y-%m') tgl_exp_group
            FROM
                (SELECT
                    sm.barang_id,
                    sm.tgl_exp,
                    sm.batch,
                    sm.qty
                FROM pbu_stock_moves sm
                WHERE
                    sm.barang_id = '$barang_id'
                    AND sm.storage_location = '$storloc'
                UNION ALL
                SELECT
                    sm.barang_id,
                    sm.tgl_exp,
                    sm.batch,
                    -sm.qty qty
                FROM pbu_stock_moves sm
                WHERE
                    sm.type_no = " . SUPPOUT . "
                    AND sm.barang_id = '$barang_id'
                    AND sm.trans_no = '$trans_no'
                    AND sm.storage_location = '$storloc'
                ) AS t
            GROUP BY
                barang_id, batch
            HAVING
                SUM(qty) <> 0");
        return $comm->queryAll(true);
    }
    public function primaryKey()
    {
        return 'barang_id';
    }
    public function count_biaya_beli($unit, $harga, $tgl, $type_no, $trans_no)
    {
        $mat = Material::model()->findByPk($this->barang_id);
        if ($mat == null) {
            throw new Exception("Barang tidak ditemukan!");
        }
        return $mat->count_biaya_beli($unit, $harga, $tgl, $type_no, $trans_no);
    }
    public function get_harga_wil_int($wil)
    {
        $price = PriceInt::get_price($this->barang_id, $wil);
        if ($price == null) {
            throw new Exception("Harga belum ada di master 'Harga Internal'");
            return 0;
        }
        return $price->amount;
    }
    public function get_harga_wil_tax($wil)
    {
        $price = PriceTax::get_price($this->barang_id, $wil);
        if ($price == null) {
            throw new Exception("Harga belum ada di master 'Harga Internal'");
            return 0;
        }
        return $price->amount;
    }
    protected function beforeDelete()
    {
        $trans = ['CreditNoteDetails', 'PriceInt', 'PriceTax', 'SjDetails', 'SjintDetails',
            'SjsimDetails', 'SjtaxDetails', 'StockMoves', 'TerimaBarangDetails', 'Bom'];
        foreach ($trans as $k) {
            $count = CActiveRecord::model($k)->count("barang_id = :barang_id",
                array(':barang_id' => $this->barang_id));
            if ($count > 0) {
                throw new Exception("Barang ini tidak bisa di hapus karena sudah di pakai transaksi ($k).");
                return false;
            }
        }
        return parent::beforeDelete();
    }
}