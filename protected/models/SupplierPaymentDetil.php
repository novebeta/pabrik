<?php
Yii::import('application.models._base.BaseSupplierPaymentDetil');
class SupplierPaymentDetil extends BaseSupplierPaymentDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->supplier_payment_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->supplier_payment_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}