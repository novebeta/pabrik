<?php
Yii::import('application.models._base.BaseWilInt');
class WilInt extends BaseWilInt
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    protected function beforeDelete()
    {
        $trans = ['Customers', 'PriceInt'];
        foreach ($trans as $k) {
            $count = CActiveRecord::model($k)->count("wil_int_id = :wil_int_id",
                array(':wil_int_id' => $this->wil_int_id));
            if ($count > 0) {
                throw new Exception("Wilayah ini tidak bisa di hapus karena sudah di pakai ($k).");
                return false;
            }
        }
        return parent::beforeDelete();
    }
}