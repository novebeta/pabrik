<?php
Yii::import('application.models._base.BaseRefs');
class Refs extends BaseRefs
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->refs_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->refs_id = $uuid;
        }
        return parent::beforeValidate();
    }
}