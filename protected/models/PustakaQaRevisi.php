<?php
Yii::import('application.models._base.BasePustakaQaRevisi');
class PustakaQaRevisi extends BasePustakaQaRevisi
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->pustaka_qa_revisi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pustaka_qa_revisi_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
}
