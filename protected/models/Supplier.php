<?php
Yii::import('application.models._base.BaseSupplier');
class Supplier extends BaseSupplier
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->supplier_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->supplier_id = $uuid;
        }
        return parent::beforeValidate();
    }
    protected function beforeDelete()
    {
        $trans = [
            'pembayaranSuppliers' => 'Pembayaran Supplier',
            'purchaseOrders' => 'Purchase Order',
            'returnPembelians' => 'Retur Pembelian',
            'supplierBanks' => 'Bank Supplier',
            'supplierPayments' => 'Supplier Payments',
            'tandaterimaMaterials' => 'Tanda Terima Materials',
            'terimaMaterials' => 'Penerimaan Materials',
        ];
        foreach ($trans as $key => $k) {
            if (count($this->$key) > 0) {
                throw new CDbException(get_class($this) . " ini tidak bisa di hapus karena sudah di pakai ($k).");
            }
        }
        return parent::beforeDelete();
    }
}