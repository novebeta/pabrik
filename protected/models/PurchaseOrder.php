<?php
Yii::import('application.models._base.BasePurchaseOrder');
class PurchaseOrder extends BasePurchaseOrder
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function delivery_is_completed($po_id)
    {
        //mengecek apakah proses delivery masih berlangsung
        //(belum semua barang telah diterima)
        $comm = Yii::app()->db->createCommand("
            SELECT
                AVG(IF(t.qty_terima/pod.qty>1, 1, t.qty_terima/pod.qty)) delivered
            FROM pbu_purchase_order_details pod
            LEFT JOIN
                    (SELECT p.material_id, IFNULL(SUM(t.qty),0) qty_terima
                    FROM pbu_purchase_order_details p
                            LEFT JOIN pbu_terima_barang_details t ON t.po_id = p.po_id AND p.material_id = t.barang_id
                    WHERE p.po_id = :po_id
                    GROUP BY material_id) t ON t.material_id = pod.material_id
            WHERE pod.po_id = :po_id
            GROUP BY pod.po_id
        ");
        $result = $comm->queryScalar(array(':po_id' => $po_id));
        return ($result < 1 ? FALSE : TRUE);
    }
    static function deliveries_are_paid($po_id)
    {
        //mengecek apakah semua delivery telah dibayar
        //TRUE = semua delivery telah dibayar
        $comm = Yii::app()->db->createCommand("SELECT
		SUM(IF(tm.status < " . SJ_SUPPLIER_PAID . ", -1000, 1)) paid
            FROM pbu_terima_material tm
                    LEFT JOIN pbu_terima_material_details tmd ON tm.terima_material_id = tmd.terima_material_id
            WHERE po_id = :po_id
            GROUP BY po_id
        ");
        $result = $comm->queryScalar(array(':po_id' => $po_id));
        return ($result < 0 ? FALSE : TRUE);
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->po_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->po_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_purchase_order WHERE SUBSTR(doc_ref FROM 6 FOR 4) = :tgl AND po_id != :po_id ;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':po_id' => $this->po_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'PO/' . $tgl . '/' . $newinc;
    }
    static function setStatus($status, $po_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_purchase_order SET status = :status"
            . " WHERE po_id = :po_id");
        return $comm->execute(array(
            ':status' => $status,
            ':po_id' => $po_id
        ));
    }
    static function getStatus($po_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
		status
            FROM pbu_purchase_order
            WHERE po_id = :po_id
        ");
        return $comm->queryScalar(array(':po_id' => $po_id));
    }
    public function beforeValidate()
    {
        if ($this->po_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->po_id = $uuid;
        }
        if ($this->tdate == null) {
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        if ($this->id_user == null) {
            $this->id_user = User()->getId();
        }
        return parent::beforeValidate();
    }
    public function report()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                pm.kode_material,
                pm.nama_material,
                ppod.sat,
                ppod.qty,
                ppod.price,
                ppod.qty*ppod.price AS total
            FROM pbu_purchase_order_details AS ppod
                INNER JOIN pbu_material AS pm ON pm.material_id = ppod.material_id
            WHERE ppod.po_id = :po_id
        ");
        return $comm->queryAll(true, array(':po_id' => $this->po_id));
    }
}