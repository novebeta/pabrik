<?php
Yii::import('application.models._base.BaseSupplierPayment');
class SupplierPayment extends BaseSupplierPayment
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->supplier_payment_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->supplier_payment_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->supplier_payment_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->supplier_payment_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_supplier_payment WHERE SUBSTR(doc_ref FROM 9 FOR 4) = :tgl AND supplier_payment_id != :supplier_payment_id ;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':supplier_payment_id' => $this->supplier_payment_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'SUPPAY/' . $tgl . '/' . $newinc;
    }
}