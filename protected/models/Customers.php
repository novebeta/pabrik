<?php
Yii::import('application.models._base.BaseCustomers');
class Customers extends BaseCustomers
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    protected function beforeDelete()
    {
        $trans = [
            'customerPayments' => 'Pembayaran Customer',
            'sales' => 'Penjualan',
            'sjs' => 'Surat Jalan'
        ];
        foreach ($trans as $key => $k) {
            if (count($this->$key) > 0) {
                throw new CDbException("Customer ini tidak bisa di hapus karena sudah di pakai ($k).");
            }
        }
        return parent::beforeDelete();
    }
    public function getOutStanding($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT SUM(pa.amount) FROM pbu_apar pa 
          WHERE pa.arus = 1 AND pa.tran_date >= :from AND pa.tran_date <= :to AND pa.subject_id = :customer_id");
        $rsl = $comm->queryScalar([
            ':from' => $from,
            ':to' => $to,
            ':customer_id' => $this->customer_id
        ]);
        return $rsl === false ? 0 : $rsl;
    }
}