<?php
Yii::import('application.models._base.BaseGiro');
class Giro extends BaseGiro
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->giro_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->giro_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function isUsed()
    {
        $count = CustomerPayment::model()->findAllByAttributes([
            'no_bg_cek' => $this->giro_id,
            'void' => 0
        ]);
        return count($count) > 0;
    }
}