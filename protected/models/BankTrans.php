<?php
Yii::import('application.models._base.BaseBankTrans');
class BankTrans extends BaseBankTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_balance($id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(nbt.amount),0) AS total
        FROM pbu_bank_trans nbt
        WHERE nbt.visible = 1 AND nbt.bank_id = :bank_id");
        return $comm->queryScalar(array(':bank_id' => $id));
    }
    public static function get_list_bank_transfer($tgl)
    {
//        $comm = Yii::app()->db->createCommand("SELECT nbt.trans_no,nbt.ref,nbt.tgl trans_date,nbt.amount,
//        (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount <= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1) bank_act_asal,
//        COALESCE((SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1,1),
//         (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1))bank_act_tujuan,
//        IFNULL((SELECT ABS(nbt1.amount) FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = 12 AND nbt1.trans_no = nbt.trans_no LIMIT 1),0) charge,
//        nc.memo_ memo
//        FROM pbu_bank_trans AS nbt
//        LEFT JOIN pbu_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
//        WHERE nbt.type_ = 11 AND nbt.tgl = :tgl AND nbt.visible = 1
//        GROUP BY trans_no");
        $comm = Yii::app()->db->createCommand("
            SELECT 
                nbt.trans_no,
                nbt.ref,
                nbt.tgl trans_date,
                nbt.amount,
                (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = " . BANKTRANSFER . " AND nbt1.amount <= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1) bank_act_asal,
                COALESCE((SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = " . BANKTRANSFER . " AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1,1),
                (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = " . BANKTRANSFER . " AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1))bank_act_tujuan,
                IFNULL((SELECT ABS(nbt1.amount) FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = " . BANKTRANSFERCHARGE . " AND nbt1.trans_no = nbt.trans_no LIMIT 1),0) charge,
                nc.memo_ memo
            FROM 
                pbu_bank_trans AS nbt LEFT JOIN pbu_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
            WHERE 
                nbt.type_ = " . BANKTRANSFER . " AND DATE_FORMAT(tgl,'%m%Y') = DATE_FORMAT(Date(:tgl),'%m%Y') AND nbt.visible = 1
            GROUP BY 
                trans_no
            ORDER BY tgl DESC");
//        if (isset($_POST['tgl']) && $_POST['tgl'] != null) {
//            $criteria->addCondition('DATE_FORMAT(po.tgl,"%m%Y") = DATE_FORMAT(Date(:tgl),"%m%Y")');
//            $criteria->params[':tgl'] = $_POST['tgl'];
//        }
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function get_list_bank_transfer_by_trans_no($trans_no)
    {
//        $comm = Yii::app()->db->createCommand("SELECT nbt.trans_no,nbt.ref,nbt.tgl trans_date,nbt.amount,
//        (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount < 0 AND nbt1.trans_no = nbt.trans_no) bank_act_asal,
//        (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount > 0 AND nbt1.trans_no = nbt.trans_no) bank_act_tujuan,
//        IFNULL((SELECT ABS(nbt1.amount) FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = 12 AND nbt1.trans_no = nbt.trans_no LIMIT 1),0) charge,
//        nc.memo_ memo
//        FROM pbu_bank_trans AS nbt
//        LEFT JOIN pbu_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
//        WHERE nbt.type_ = 11 AND nbt.trans_no = :trans_no
//        GROUP BY trans_no");
        $comm = Yii::app()->db->createCommand("
            SELECT
                nbt.trans_no,
                nbt.ref,
                nbt.tgl trans_date,
                nbt.amount,
                (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = " . BANKTRANSFER . " AND nbt1.amount < 0 AND nbt1.trans_no = nbt.trans_no) bank_act_asal,
                (SELECT nbt1.bank_id FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = " . BANKTRANSFER . " AND nbt1.amount > 0 AND nbt1.trans_no = nbt.trans_no) bank_act_tujuan,
                IFNULL((SELECT ABS(nbt1.amount) FROM pbu_bank_trans nbt1 WHERE nbt1.type_ = " . BANKTRANSFERCHARGE . " AND nbt1.trans_no = nbt.trans_no LIMIT 1),0) charge,
                nc.memo_ memo
            FROM 
                pbu_bank_trans AS nbt LEFT JOIN pbu_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
            WHERE 
                nbt.type_ = " . BANKTRANSFER . " AND 
                nbt.trans_no = :trans_no
            GROUP BY 
                trans_no");
        $comm->setFetchMode(PDO::FETCH_OBJ);
        return $comm->queryRow(true, array(':trans_no' => $trans_no));
    }
    public function beforeValidate()
    {
        if ($this->bank_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bank_trans_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }
}