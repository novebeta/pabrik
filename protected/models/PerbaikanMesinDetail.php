<?php
Yii::import('application.models._base.BasePerbaikanMesinDetail');
class PerbaikanMesinDetail extends BasePerbaikanMesinDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        //if ($this->perbaikan_mesin_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->perbaikan_mesin_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
