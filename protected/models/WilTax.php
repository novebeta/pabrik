<?php
Yii::import('application.models._base.BaseWilTax');
class WilTax extends BaseWilTax
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    protected function beforeDelete()
    {
        $trans = ['Customers', 'PriceTax'];
        foreach ($trans as $k) {
            $count = CActiveRecord::model($k)->count("wil_tax_id = :wil_tax_id",
                array(':wil_tax_id' => $this->wil_tax_id));
            if ($count > 0) {
                throw new Exception("Wilayah ini tidak bisa di hapus karena sudah di pakai ($k).");
                return false;
            }
        }
        return parent::beforeDelete();
    }
}