<?php
Yii::import('application.models._base.BaseSampelQc');
class SampelQc extends BaseSampelQc
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    /*
    public function create_simulation()
    {
        if (count($this->sampelsims) > 0) {
            foreach ($this->sampelsims as $simold) {
                SampelQcDetail::model()->deleteAll("sampelsim_id = :sampelsim_id",
                    array(':sampelsim_id' => $simold->sampelsim_id));
                $simold->delete();
            }
        }
        $sim = new Sjsim; //=============================
        $sim->sj_id = $this->sj_id;
        $sim->tgl = $this->tgl;
        //$sim->customer_id = $this->customer_id;
        $sim->doc_ref = "SIM" . $this->doc_ref;
        if (!$sim->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Simulasi Surat Jalan')) .
                CHtml::errorSummary($sim));
        }
        $total = 0;
        foreach ($this->sampelQcDetails as $detail) {
            $sampelsim_detail = new SampelQcDetail();
            $sampelsim_detail->barang_id = $detail->barang_id;
            $sampelsim_detail->batch = $detail->batch;
            $sampelsim_detail->tgl_exp = $detail->tgl_exp;
            $sampelsim_detail->ket = $detail->ket;
            $sampelsim_detail->qty = $detail->qty;
            $sampelsim_detail->price = $detail->barang->get_harga_wil_tax($sim->customer->wil_tax_id);
            $sampelsim_detail->total = $sampelsim_detail->qty * $sampelsim_detail->price;
            $total += $sampelsim_detail->total;
            $sampelsim_detail->sampelsim_id = $sim->sampelsim_id;
            if (!$sampelsim_detail->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detil Simulasi Sampel QC')) .
                    CHtml::errorSummary($sampelsim_detail));
            }
        }
        $sim->total = $total;
        if (!$sim->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Simulasi Surat Jalan')) .
                CHtml::errorSummary($sim));
        }
    }
    */
    /*
    public function create_IntTax($persen)
    {
        $sjtax = new Sjtax;
        $sjtax->doc_ref = "TAX" . $this->doc_ref;
        if (!$sjtax->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Pajak')) .
                CHtml::errorSummary($sjtax));
        }

        $total_tax = $total_int =0;
        foreach ($this->sjDetails as $detail) {
            $sjint_detail = new SjintDetails;
            $sjtax_detail = new SjtaxDetails;
            $sjtax_detail->barang_id = $sjint_detail->barang_id = $detail->barang_id;
            $sjtax_detail->batch = $sjint_detail->batch = $detail->batch;
            $sjtax_detail->tgl_exp = $sjint_detail->tgl_exp = $detail->tgl_exp;
            $sjtax_detail->ket = $sjint_detail->ket = $detail->ket;
            $qty_tax = ceil($detail->qty * $persen);
            $qty_int = $detail->qty - $qty_tax;
            $sjtax_detail->qty = $qty_tax;
            $sjint_detail->qty = $qty_int;
            $sjtax_detail->price = $detail->barang->get_harga_wil_tax($sjtax->customer->wil_tax_id);
//            $sjint_detail->price = $detail->barang->get_harga_wil_int($sjint->customer->wil_int_id);
            $sjtax_detail->total = $sjtax_detail->qty * $sjtax_detail->price;
            $sjint_detail->total = $sjint_detail->qty * $sjint_detail->price;
            $total_tax += $sjtax_detail->total;
            $total_int += $sjint_detail->total;
            $sjtax_detail->sjtax_id = $sjtax->sjtax_id;
//            $sjint_detail->sjint_id = $sjint->sjint_id;
            if (!$sjtax_detail->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detil Surat Jalan Pajak')) .
                    CHtml::errorSummary($sjtax_detail));
            }
            if (!$sjint_detail->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detil Surat Jalan Internal')) .
                    CHtml::errorSummary($sjint_detail));
            }
        }
        $sjtax->total = $total_tax;
//        $sjint->total = $total_int;
        if (!$sjtax->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Surat Jalan Pajak')) .
                CHtml::errorSummary($sjtax));
        }
    }
    */
}