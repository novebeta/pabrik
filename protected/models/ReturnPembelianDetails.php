<?php
Yii::import('application.models._base.BaseReturnPembelianDetails');
class ReturnPembelianDetails extends BaseReturnPembelianDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        //if ($this->return_pembelian_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->return_pembelian_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
