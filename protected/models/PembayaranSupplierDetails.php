<?php
Yii::import('application.models._base.BasePembayaranSupplierDetails');
class PembayaranSupplierDetails extends BasePembayaranSupplierDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_details()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT *, IFNULL(return_pembelian_id, terima_material_id) temporary_id
            FROM pbu_pembayaran_supplier_details
            WHERE pembayaran_supplier_id = :pembayaran_supplier_id
            ");
        return $comm->queryAll(true, array(
            ':pembayaran_supplier_id' => $_POST['pembayaran_supplier_id']
        ));
    }
    public function beforeValidate()
    {
        //if ($this->pembayaran_supplier_details_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->pembayaran_supplier_details_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}