<?php
Yii::import('application.models._base.BaseTerimaMaterial');
class TerimaMaterial extends BaseTerimaMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function getPOitems($po_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                p.material_id,
                m.kode_material,
                m.nama_material,
                p.sat,
                p.price,
                IFNULL(SUM(t.qty),0) qty_terima
            FROM pbu_purchase_order_details p
                LEFT JOIN pbu_terima_material_details t ON t.po_id = p.po_id AND p.material_id = t.material_id
                LEFT JOIN pbu_material m ON p.material_id = m.material_id
            WHERE
                p.po_id = :po_id
            GROUP BY material_id
        ");
        return $comm->queryAll(true, array(':po_id' => $po_id));
    }
    static function getList_POnumber($terima_material_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT DISTINCT po_id
            FROM pbu_terima_material tm
                    LEFT JOIN pbu_terima_material_details tmd ON tm.terima_material_id = tmd.terima_material_id
            WHERE tm.terima_material_id = :terima_material_id
        ");
        return $comm->queryAll(true, array(':terima_material_id' => $terima_material_id));
    }
    static function setStatus($status, $terima_material_id, $pembayaran_supplier_id = NULL)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_terima_material SET status = :status"
            . ($pembayaran_supplier_id == NULL ? "" : ", pembayaran_supplier_id = '" . $pembayaran_supplier_id . "'")
            . " WHERE terima_material_id = :terima_material_id");
        return $comm->execute(array(
            ':status' => $status,
            ':terima_material_id' => $terima_material_id
        ));
    }
    static function resetStatusFPT_to_INV($pembayaran_supplier_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_terima_material
            SET pembayaran_supplier_id = NULL, status = :status
            WHERE pembayaran_supplier_id = :pembayaran_supplier_id");
        return $comm->execute(array(
            ':status' => SJ_SUPPLIER_INVOICED,
            ':pembayaran_supplier_id' => $pembayaran_supplier_id
        ));
    }
    static function setStatus_PAID($pembayaran_supplier_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_terima_material
            SET status = :status
            WHERE pembayaran_supplier_id = :pembayaran_supplier_id");
        return $comm->execute(array(
            ':status' => SJ_SUPPLIER_PAID,
            ':pembayaran_supplier_id' => $pembayaran_supplier_id
        ));
    }
    static function getListPO($terima_material_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT DISTINCT po_id
            FROM pbu_terima_material_details
            WHERE terima_material_id = :terima_material_id
        ");
        return $comm->queryAll(true, array(':terima_material_id' => $terima_material_id));
    }
    public function getItems()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 0 as num,ptbd.terima_barang_details_id,ptbd.qty,ptbd.price,ptbd.total,ptbd.barang_id,ptbd.ket,
              ptbd.terima_barang_id,ptbd.disc_rp,ptbd.disc,ptbd.bruto,ptbd.vat,ptbd.vatrp,ptbd.po_id,
              ptb.loc_code,pm.kode_material,pm.nama_material,pm.sat
            FROM pbu_terima_barang AS ptb
                INNER JOIN pbu_terima_barang_details AS ptbd ON ptbd.terima_barang_id = ptb.terima_barang_id
                INNER JOIN pbu_material AS pm ON ptbd.barang_id = pm.material_id
            WHERE ptb.terima_barang_id = :terima_barang_id");
        return $comm->queryAll(true, [':terima_barang_id'=> $this->terima_material_id]);
    }
    public function beforeValidate()
    {
        if ($this->terima_material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_material_id = $uuid;
            $this->id_user = User()->getId();
            $this->urole = U::get_user_role();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
}