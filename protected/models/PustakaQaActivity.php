<?php
Yii::import('application.models._base.BasePustakaQaActivity');
class PustakaQaActivity extends BasePustakaQaActivity
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->pustaka_qa_activity_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pustaka_qa_activity_id = $uuid;
        }
        return parent::beforeValidate();
    }
}
