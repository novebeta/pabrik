<?php
Yii::import('application.models._base.BaseBarangSales');
class BarangSales extends BaseBarangSales
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'barang_id';
    }
}