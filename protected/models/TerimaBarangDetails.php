<?php
Yii::import('application.models._base.BaseTerimaBarangDetails');
class TerimaBarangDetails extends BaseTerimaBarangDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->terima_barang_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_barang_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}