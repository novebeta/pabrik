<?php
Yii::import('application.models._base.BaseKonversiMaterialDetails');
class KonversiMaterialDetails extends BaseKonversiMaterialDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->konversi_material_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->konversi_material_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}