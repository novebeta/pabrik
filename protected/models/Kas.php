<?php
Yii::import('application.models._base.BaseKas');
Yii::import('application.components.U');
class Kas extends BaseKas
{
    public static function is_modal_exist($tgl)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("total > 0");
        $criteria->addCondition("type_ = 1");
        $criteria->addCondition("DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $tgl);
        $res = Kas::model()->count($criteria);
        return $res > 0;
    }
    public function getItems()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT pkd.item_name,ABS(pkd.total) total,pkd.account_code
              FROM pbu_kas_detail AS pkd
            WHERE pkd.kas_id = :kas_id");
        return $comm->queryAll(true, [':kas_id' => $this->kas_id]);
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_cash_in($tgl)
    {
//        $where = "";
        $param = array(':tgl' => $tgl);
//        if ($store != null) {
//            $where = "AND store = :store";
//            $param[':store'] = $store;
//        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_kas ns
    WHERE ns.arus = 1 AND ns.visible = 1 AND DATE(ns.tgl) = :tgl");
        return $comm->queryScalar($param);
    }
    public static function get_cash_out($tgl)
    {
//        $where = "";
        $param = array(':tgl' => $tgl);
//        if ($store != null) {
//            $where = "AND store = :store";
//            $param[':store'] = $store;
//        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_kas ns
    WHERE ns.arus = -1 AND ns.visible = 1 AND DATE(ns.tgl) = :tgl");
        return $comm->queryScalar($param);
    }
    public function beforeValidate()
    {
        if ($this->kas_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function createDocRef($arus, $inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->kas_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kas_id = $uuid;
        }
        if ($inc) {
            $pos = 5;
            $prefix = 'CIN/';
            if ($arus < 0) {
                $pos = 6;
                $prefix = 'COUT/';
            }
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_kas WHERE SUBSTR(doc_ref FROM $pos FOR 4) = :tgl AND kas_id != :kas_id;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':kas_id' => $this->kas_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = $prefix . $tgl . '/' . $newinc;
    }
    public static function urutDocRef($arus, $month, $year)
    {
        Yii::app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var Kas[] $ass */
            $criteria = new CDbCriteria();
            $criteria->addCondition("MONTH(tgl) = :month AND YEAR(tgl) = :year AND arus = :arus");
            $params = [
                ':arus' => $arus,
                ':month' => $month,
                ':year' => $year
            ];
            $criteria->order = "tgl ASC,tdate ASC";
            $criteria->params = $params;
            $ass = Kas::model()->findAll($criteria);
            $nextval = 1;
            $prefix = 'CIN/';
            if ($arus < 0) {
                $prefix = 'COUT/';
            }
            foreach ($ass as $a) {
                $fmt = '%05d';
                $newinc = sprintf($fmt, $nextval);
                $tgl = sql2date($a->tgl, 'yyMMdd');
                $a->doc_ref = $prefix . $tgl . '/' . $newinc;
                if (!$a->save(false)) {
                    throw new Exception('Assembly gagal di simpan.' . "\n");
                }
                Refs::model()->updateAll(['reference' => $a->doc_ref], "type_no = :type_no",
                    array(':type_no' => $a->kas_id));
                BankTrans::model()->updateAll(['ref' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->kas_id]);
                StockMovesMaterial::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->kas_id]);
                Apar::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->kas_id]);
                $nextval++;
                echo $a->doc_ref . "\n";
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            echo $ex->getMessage();
        }
        Yii::app()->db->autoCommit = true;
    }

//    protected function afterSave()
//    {
//        parent::afterSave();
//        $is_in = $this->total >= 0;
//        U::add_bank_trans($is_in ? CASHIN : CASHOUT, $this->kas_id, $this->bank_id, $this->doc_ref, $this->tgl,
//            $this->total, $this->user_id);
//    }
}