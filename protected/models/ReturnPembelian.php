<?php
Yii::import('application.models._base.BaseReturnPembelian');
class ReturnPembelian extends BaseReturnPembelian
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function setStatus($status, $return_pembelian_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_return_pembelian SET status = :status"
            . " WHERE return_pembelian_id = :return_pembelian_id");
        return $comm->execute(array(
            ':status' => $status,
            ':return_pembelian_id' => $return_pembelian_id
        ));
    }
    static function resetStatusUSED_to_OPEN($pembayaran_supplier_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_return_pembelian
            SET nota_return_pembayaran_supplier_id = NULL, nota_return_status = :status
            WHERE nota_return_pembayaran_supplier_id = :pembayaran_supplier_id");
        return $comm->execute(array(
            ':status' => NOTA_RETURN_OPEN,
            ':pembayaran_supplier_id' => $pembayaran_supplier_id
        ));
    }
    static function setStatusNotaReturn($nota_return_status, $return_pembelian_id, $pembayaran_supplier_id = NULL)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_return_pembelian SET nota_return_status = :status"
            . ($pembayaran_supplier_id == NULL ? "" : ", nota_return_pembayaran_supplier_id = '" . $pembayaran_supplier_id . "'")
            . " WHERE return_pembelian_id = :return_pembelian_id");
        return $comm->execute(array(
            ':status' => $nota_return_status,
            ':return_pembelian_id' => $return_pembelian_id
        ));
    }
    static function getItems($terima_material_id)
    {
        /*$comm = Yii::app()->db->createCommand("
            SELECT
                tmd.material_id,
                m.kode_material,
                m.nama_material,
                m.sat,
                tmd.qty qty_received,
                tmd.tgl_expired,
                tmd.no_lot no_lot,
                tmd.no_qc no_qc,
                IFNULL(SUM(rpd.qty), 0) qty_returned
            FROM pbu_terima_material_details tmd
                LEFT JOIN pbu_return_pembelian_details rpd ON tmd.material_id = rpd.material_id
                    AND rpd.tgl_expired = tmd.tgl_expired
                    AND rpd.no_lot = tmd.no_lot
                    AND rpd.no_qc = tmd.no_qc
                LEFT JOIN pbu_return_pembelian rp ON rp.return_pembelian_id = rpd.return_pembelian_id
                    AND rp.terima_material_id = tmd.terima_material_id
                LEFT JOIN pbu_material m ON tmd.material_id = m.material_id
            WHERE tmd.terima_material_id = :terima_material_id
            GROUP BY tmd.material_id, tmd.tgl_expired, tmd.no_lot, tmd.no_qc
        ");*/
        $comm = Yii::app()->db->createCommand("
            SELECT
                tmd.material_id,
                m.kode_material,
                m.nama_material,
                m.sat
            FROM pbu_terima_material_details tmd
                LEFT JOIN pbu_material m ON tmd.material_id = m.material_id
            WHERE tmd.terima_material_id = :terima_material_id
            GROUP BY tmd.material_id
        ");
        return $comm->queryAll(true, array(':terima_material_id' => $terima_material_id));
    }
    public function beforeValidate()
    {
        if ($this->return_pembelian_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->return_pembelian_id = $uuid;
            $this->id_user = User()->getId();
            $this->urole = U::get_user_role();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        if ($this->nota_return_doc_ref != NULL &&
            ($this->nota_return_tdate == null || $this->nota_return_tdate == '0000-00-00 00:00:00')
        ) {
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->nota_return_tdate = $dt;
            $this->nota_return_id_user = User()->getId();
        }
        return parent::beforeValidate();
    }
    public function getItemsNotaReturn()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                m.nama_material,
                rpd.qty,
                rpd.sat,
                rpd.price,
                rpd.qty*rpd.price harga_jual
            FROM pbu_return_pembelian_details rpd
                LEFT JOIN pbu_material m ON rpd.material_id = m.material_id
            WHERE
                rpd.return_pembelian_id = :return_pembelian_id
        ");
        return $comm->queryAll(true, array(':return_pembelian_id' => $this->return_pembelian_id));
    }
    public function getItemsTandaTerima()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                m.nama_material,
                rpd.sat,
                rpd.qty,
                rpd.note_return note
            FROM pbu_return_pembelian_details rpd
                LEFT JOIN pbu_material m ON rpd.material_id = m.material_id
            WHERE
                rpd.return_pembelian_id = :return_pembelian_id
        ");
        return $comm->queryAll(true, array(':return_pembelian_id' => $this->return_pembelian_id));
    }
}
