<?php
Yii::import('application.models._base.BaseSparepartMasukDetil');
class SparepartMasukDetil extends BaseSparepartMasukDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        //if ($this->sparepart_masuk_detil_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->sparepart_masuk_detil_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
