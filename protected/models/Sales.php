<?php
Yii::import('application.models._base.BaseSales');
class Sales extends BaseSales
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->sales_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sales_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }

    public function createDocRef($inc = true)
    {
        # SLS/240417/0000000006
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->sales_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sales_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_sales WHERE arus = 1 AND SUBSTR(doc_ref FROM 5 FOR 4) = :tgl AND pbu_sales.sales_id != :sales_id;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':sales_id' => $this->sales_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'SLS/' . $tgl . '/' . $newinc;
    }

    public static function urutDocRef($month, $year)
    {
        Yii::app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var Sales[] $ass */
            $criteria = new CDbCriteria();
            $criteria->addCondition("MONTH(tgl) = :month AND YEAR(tgl) = :year AND arus = 1");
            $params = [
                ':month' => $month,
                ':year' => $year
            ];
            $criteria->order = "tgl ASC,tdate ASC";
            $criteria->params = $params;
            $ass = Sales::model()->findAll($criteria);
            $nextval = 1;
            foreach ($ass as $a) {
                $fmt = '%05d';
                $newinc = sprintf($fmt, $nextval);
                $tgl = sql2date($a->tgl, 'yyMMdd');
                $a->doc_ref = 'SLS/' . $tgl . '/' . $newinc;
                if (!$a->save(false)) {
                    throw new Exception('Sales gagal di simpan.' . "\n");
                }
                Refs::model()->updateAll(['reference' => $a->doc_ref], "type_no = :type_no",
                    array(':type_no' => $a->sales_id));
                BankTrans::model()->updateAll(['ref' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->sales_id]);
                StockMovesMaterial::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->sales_id]);
                Apar::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->sales_id]);
                $nextval++;
                echo $a->doc_ref . "\n";
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            echo $ex->getMessage();
        }
        Yii::app()->db->autoCommit = true;
    }
    public function createDocRefRetur($inc = true)
    {
        # RSLS/240417/0000000006
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->sales_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sales_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_sales WHERE arus = -1 AND SUBSTR(doc_ref FROM 6 FOR 4) = :tgl AND pbu_sales.sales_id != :sales_id;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':sales_id' => $this->sales_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'RSLS/' . $tgl . '/' . $newinc;
    }
    public static function urutReturDocRef($month, $year)
    {
        Yii::app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var Sales[] $ass */
            $criteria = new CDbCriteria();
            $criteria->addCondition("MONTH(tgl) = :month AND YEAR(tgl) = :year AND arus = -1");
            $params = [
                ':month' => $month,
                ':year' => $year
            ];
            $criteria->order = "tgl ASC,tdate ASC";
            $criteria->params = $params;
            $ass = Sales::model()->findAll($criteria);
            $nextval = 1;
            foreach ($ass as $a) {
                $fmt = '%05d';
                $newinc = sprintf($fmt, $nextval);
                $tgl = sql2date($a->tgl, 'yyMMdd');
                $a->doc_ref = 'RSLS/' . $tgl . '/' . $newinc;
                if (!$a->save(false)) {
                    throw new Exception('Sales gagal di simpan.' . "\n");
                }
                Refs::model()->updateAll(['reference' => $a->doc_ref], "type_no = :type_no",
                    array(':type_no' => $a->sales_id));
                BankTrans::model()->updateAll(['ref' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->sales_id]);
                StockMovesMaterial::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->sales_id]);
                Apar::model()->updateAll(['reference' => $a->doc_ref], 'trans_no = :trans_no',
                    [':trans_no' => $a->sales_id]);
                $nextval++;
                echo $a->doc_ref . "\n";
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            echo $ex->getMessage();
        }
        Yii::app()->db->autoCommit = true;
    }
}