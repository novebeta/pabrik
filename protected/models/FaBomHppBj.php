<?php
Yii::import('application.models._base.BaseFaBomHppBj');
class FaBomHppBj extends BaseFaBomHppBj
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function report_laporan_produksi($from, $to)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                ba.bom_id,
                ba.batch_id,
                ba.qty batch_size,
                bh.barang_id,
                SUM(bh.qty) total_bj
            FROM
                pbu_batch_hasil bh
                INNER JOIN pbu_batch ba ON ba.batch_id = bh.batch_id
            WHERE ba.tgl_mixing >= :from AND ba.tgl_mixing <= :to
            GROUP BY ba.bom_id, ba.batch_id, ba.qty, bh.barang_id
        ");
        return $comm->queryAll(true, array(
            ':from' => $from,
            ':to' => $to
        ));
    }
    public function beforeValidate()
    {
        if ($this->fa_bom_hpp_bj_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fa_bom_hpp_bj_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function hitung_nilai_RM_PM_berdasarkan_BOM()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                SUM(IF(t.tipe_material_id=:RM, t.amount, 0)*hpp.hpp_after) nilai_rm_per_batch,
                SUM(IF(t.tipe_material_id=:PM, t.amount, 0)*hpp.hpp_after) nilai_pm_per_pce
            FROM
                (
                    SELECT
                        bd.material_id,
                        m.tipe_material_id,
                        IF(m.tipe_material_id = :RM, (bd.qty/100*:batch_size), (1/b.qty*bd.qty)) amount
                    FROM pbu_bom_detail bd
                        LEFT JOIN pbu_bom b ON b.bom_id = bd.bom_id
                        LEFT JOIN pbu_material m ON m.material_id = bd.material_id
                    WHERE
                        bd.bom_id = :bom_id
                ) t
                LEFT JOIN pbu_fa_hpp_material hpp ON hpp.material_id = t.material_id AND hpp.fin_acc_report_id = :fin_acc_report_id
        ");
        $nilai = $comm->queryRow(true, array(
            ':RM' => MAT_RAW_MATERIAL,
            ':PM' => MAT_PACKAGING,
            ':batch_size' => $this->batch_size,
            ':bom_id' => $this->bom_id,
            ':fin_acc_report_id' => $this->fin_acc_report_id
        ));
        If ($nilai) {
            $this->nilai_rm_per_batch = $nilai['nilai_rm_per_batch'];
            $this->nilai_pm_per_pce = $nilai['nilai_pm_per_pce'];
        } else {
            $this->nilai_rm_per_batch = 0;
            $this->nilai_pm_per_pce = 0;
        }
    }
}
