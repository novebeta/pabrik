<?php
Yii::import('application.models._base.BaseChartClass');
class ChartClass extends BaseChartClass
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->cid == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->cid = $uuid;
        }
        return parent::beforeValidate();
    }
}