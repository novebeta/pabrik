<?php
Yii::import('application.models._base.BaseSupplierBank');
class SupplierBank extends BaseSupplierBank
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->supplier_bank_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->supplier_bank_id = $uuid;
        }
        return parent::beforeValidate();
    }
}