<?php
Yii::import('application.models._base.BaseSparepartMasuk');
class SparepartMasuk extends BaseSparepartMasuk
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->sparepart_masuk_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sparepart_masuk_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
    public function report()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                pm.kode_material kode,
                pm.nama_material nama,
                pmd.qty,
                pmd.sat,
                pmd.note,
                s.supplier_name
            FROM pbu_sparepart_masuk_detil pmd
                INNER JOIN pbu_material pm ON pm.material_id = pmd.material_id
                LEFT JOIN pbu_supplier s ON s.supplier_id = pmd.supplier_id
            WHERE
                pmd.sparepart_masuk_id = :sparepart_masuk_id
        ");
        return $comm->queryAll(true, array(':sparepart_masuk_id' => $this->sparepart_masuk_id));
    }
}