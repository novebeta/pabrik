<?php
Yii::import('application.models._base.BaseChartMaster');
class ChartMaster extends BaseChartMaster
{
    public static function get_child($coa, $inc_header = false)
    {
        $res = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition("kategori = :account_code");
        $criteria->params = array(':account_code' => $coa);
        $chart = ChartMaster::model()->findAll($criteria);
        foreach ($chart as $c) {
            if ($c->header == 1) {
                if ($inc_header) {
                    $res[] = $c;
                }
            } else {
                $res[] = $c;
            }
            $childArr = Self::get_child($c->account_code, $inc_header);
            $res = array_merge($res, $childArr);
        }
        return $res;
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_laba_rugi($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT pcm.account_code,pcm.account_name,
        IFNULL(Sum(pgt.amount),0) total FROM pbu_gl_trans AS pgt
            RIGHT JOIN pbu_chart_master pcm ON (pgt.account_code = pcm.account_code AND
        pgt.tran_date >= :from AND pgt.tran_date <= :to AND pgt.visible = 1)
        WHERE pcm.tipe = 'L' $where
        GROUP BY pcm.account_code;");
        return $comm->queryAll(true, $param);
    }
    public static function get_laba_rugi_new($from, $to)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        $comm = Yii::app()->db->createCommand("SELECT
       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
				IFNULL(Sum(pgt.amount),0) AS total,
        ncc.ctype,nct.id AS id_1
        FROM pbu_chart_master AS ncm
        LEFT JOIN pbu_chart_types AS nct ON ncm.kategori = nct.id
        LEFT JOIN pbu_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN pbu_chart_types AS nct2 ON nct1.parent = nct2.id
        LEFT JOIN pbu_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
        LEFT JOIN pbu_gl_trans AS pgt ON ncm.account_code = pgt.account_code AND
        pgt.tran_date >= :from AND pgt.tran_date <= :to $where AND pgt.visible = 1
        WHERE ncc.ctype IN (4,5,6,7)
        GROUP BY ncm.account_code
        ORDER BY ncm.account_code");
        return $comm->queryAll(true, $param);
    }
    public static function get_neraca_new($to)
    {
        $where = "";
        $param = array(':to' => $to);
        $comm = Yii::app()->db->createCommand("SELECT
       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
				IFNULL(Sum(pgt.amount),0) AS total,
        ncc.ctype,nct.id AS id_1
        FROM pbu_chart_master AS ncm
        LEFT JOIN pbu_chart_types AS nct ON ncm.kategori = nct.id
        LEFT JOIN pbu_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN pbu_chart_types AS nct2 ON nct1.parent = nct2.id
        LEFT JOIN pbu_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
        LEFT JOIN pbu_gl_trans AS pgt ON ncm.account_code = pgt.account_code AND
        pgt.tran_date <= :to $where AND pgt.visible = 1
        WHERE ncc.ctype IN (1,2,3,8,9)
        GROUP BY ncm.account_code
        ORDER BY ncm.account_code");
        return $comm->queryAll(true, $param);
    }
    public static function get_neraca($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT pcm.account_code,pcm.account_name,pcm.header,
    SUM(IF (pgt.tran_date < :from, pgt.amount, 0)) `before`,
    SUM(IF (pgt.amount >= 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, pgt.amount, 0)) `debit`,
    SUM(IF (pgt.amount < 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, ABS(pgt.amount), 0)) `kredit`,
    SUM(IF (pgt.tran_date <= :to, pgt.amount, 0)) `after`
    FROM pbu_gl_trans AS pgt
    RIGHT JOIN pbu_chart_master pcm ON (pgt.account_code = pcm.account_code AND pgt.visible = 1)
    WHERE pcm.tipe = 'N' $where GROUP BY pcm.account_code;");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    public static function get_saldo_until($tgl, $account)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        Sum(pgt.amount) FROM psn_gl_trans AS pgt
        WHERE pgt.tran_date <= :tgl AND
        pgt.account_code = :account_code");
        return $comm->queryScalar(array(':tgl' => $tgl, ':account_code' => $account));
    }
    public static function print_chart($id, &$lr, $arus = 1)
    {
        $arr = array();
        $child = ChartTypes::get_child($id);
        foreach ($child as $child_item) {
            if ($child_item['hide'] != '1') {
                $arr[] = array(
                    'account_code' => '',
                    'account_name' => $child_item['name'],
                    'total' => ''
                );
            }
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $child_item['id']) {
                    $arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $arr = array_merge($arr, self::print_chart($child_item['id'], $lr, $arus));
        }
        return $arr;
    }
    public static function print_chart_konsolidasi($id, &$lr, $store = array())
    {
        $arr = array();
        $label = array();
        $child = ChartTypes::get_child($id);
        foreach ($child as $child_item) {
            if ($child_item['hide'] != '1') {
                $label[] = array(
                    'id' => '',
                    'account_code' => '',
                    'account_name' => $child_item['name']
                );
            }
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $child_item['id']) {
                    foreach ($store as $cab) {
                        $arr[$cab][$item['account_code']] =
                            (($item['saldo_normal'] == 'D' ? 1 : -1) * $item[$cab]);
                        $arr['TOTAL'][$item['account_code']] +=
                            $arr[$cab][$item['account_code']];
                    }
                    unset($lr[$key]);
                }
            }
            $result = self::print_chart_konsolidasi($child_item['id'], $lr, $store);
            $arr = array_merge($arr, $result['arr']);
            $label = array_merge($label, $result['label']);
        }
        return array('arr' => $arr, 'label' => $label);
    }
    protected function beforeDelete()
    {
        if (count($this->glTrans) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan jurnal.');
        }
        if (count($this->banks) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan bank.');
        }
        if (count($this->kasDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan kas detail.');
        }
        if (count($this->suppliers) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan supplier.');
        }
        return parent::beforeDelete();
    }
}