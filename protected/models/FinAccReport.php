<?php
Yii::import('application.models._base.BaseFinAccReport');
class FinAccReport extends BaseFinAccReport
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function isExist($start_date, $end_date)
    {
        return Yii::app()->db->createCommand("SELECT count(*) FROM pbu_fin_acc_report WHERE start_date = '$start_date' AND end_date = '$end_date';")->queryScalar();
    }
    public function beforeValidate()
    {
        if ($this->fin_acc_report_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fin_acc_report_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
    public function get_report_id_before()
    {
        $end_date = Yii::app()->db->createCommand("SELECT end_date FROM pbu_fin_acc_report WHERE end_date < :end_date ORDER BY end_date DESC LIMIT 1 OFFSET 1")
            ->queryScalar(array(':end_date' => $this->end_date));
        if ($end_date) {
            return Yii::app()->db->createCommand("SELECT fin_acc_report_id FROM pbu_fin_acc_report WHERE end_date = :end_date ")->queryScalar(array(':end_date' => $end_date));
        } else {
            return FALSE;
        }
    }
    public function HitungHPP_produksi()
    {
        $stock_material = Yii::app()->db->createCommand("
            SELECT
                SUM(qty_before*hpp_before) stok_awal,
                SUM(qty_pembelian*hpp) pembelian,
                SUM(qty_after*hpp) stok_akhir
            FROM pbu_fa_stock_material
            WHERE fin_acc_report_id = :fin_acc_report_id
        ")->queryRow(TRUE, array(':fin_acc_report_id' => $this->fin_acc_report_id));
        $bop = Yii::app()->db->createCommand("
            SELECT
                SUM(biaya_tenagakerja_tidak_langsung) + SUM(biaya_listrik) + SUM(biaya_penyusutan_mesin) bop
            FROM pbu_fa_bop
            WHERE fin_acc_report_id = :fin_acc_report_id
        ")->queryScalar(array(':fin_acc_report_id' => $this->fin_acc_report_id));
        $BDPbefore = Yii::app()->db->createCommand("
            SELECT
                SUM(nilai_rm_pm)
            FROM pbu_fa_stock_bdp
            WHERE fin_acc_report_id = :fin_acc_report_id
        ")->queryScalar(array(':fin_acc_report_id' => $_POST['fin_acc_report_id_before']));
        $BDPafter = Yii::app()->db->createCommand("
            SELECT
                SUM(nilai_rm_pm)
            FROM pbu_fa_stock_bdp
            WHERE fin_acc_report_id = :fin_acc_report_id
        ")->queryScalar(array(':fin_acc_report_id' => $this->fin_acc_report_id));
        $stock_barang = Yii::app()->db->createCommand("
            SELECT
                SUM(qty_before*hpp_before) stok_awal,
                SUM(qty_in*hpp_per_pce) masuk,
                SUM(qty_out*hpp_out) keluar,
                SUM(qty_after*hpp_after) stok_akhir
            FROM pbu_fa_stock_gpj
            WHERE fin_acc_report_id = :fin_acc_report_id
        ")->queryRow(TRUE, array(':fin_acc_report_id' => $this->fin_acc_report_id));
        $this->value_stock_material_before = (float)$stock_material['stok_awal'];
        $this->value_pembelian_material = (float)$stock_material['pembelian'];
        $this->value_stock_material_after = (float)$stock_material['stok_akhir'];
        $this->value_bop = (float)$bop;
        $this->value_stock_bdp_before = (float)$BDPbefore;
        $this->value_stock_bdp_after = (float)$BDPafter;
        $this->value_stock_barang_before = (float)$stock_barang['stok_awal'];
        $this->value_stock_barang_after = (float)$stock_barang['stok_akhir'];
        $this->value_hpp_penjualan =
            $this->value_stock_material_before
            + $this->value_pembelian_material
            - $this->value_stock_material_after
            + $this->value_bop
            + $this->value_stock_bdp_before
            - $this->value_stock_bdp_after
            + $this->value_stock_barang_before
            - $this->value_stock_barang_after;
    }
}
