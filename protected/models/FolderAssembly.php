<?php
Yii::import('application.models._base.BaseFolderAssembly');
class FolderAssembly extends BaseFolderAssembly
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'folder_id';
    }
}