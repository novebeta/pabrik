<?php
Yii::import('application.models._base.BaseMaterial');
class Material extends BaseMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function getAvg($barang_id)
    {
        $brg = Material::model()->findByPk($barang_id);
        if ($brg != null) {
            return $brg->avg;
        }
        return 0;
    }
    public static function get_availableStock($material_id = null, $noZeroStock = 0, $storloc)
    {
        $where = "";
        $having = "";
        //$storloc = $GLOBALS['storLoc'][U::get_user_role()]['name'];
        if ($material_id != null) {
            $where = "AND smm.material_id = '$material_id'";
        }
        if ($noZeroStock) {
            $having = "HAVING qty > 0";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                smm.tgl_expired,
                smm.no_lot,
                smm.no_qc,
                smm.supplier_id,
                s.supplier_name,
                Sum(smm.qty) qty,
                m.sat
            FROM pbu_stock_moves_material smm
                LEFT JOIN pbu_material m ON smm.material_id = m.material_id
                LEFT JOIN pbu_supplier s ON smm.supplier_id = s.supplier_id
            WHERE
                smm.storage_location = '$storloc'
                $where
            GROUP BY smm.material_id, smm.tgl_expired, smm.no_lot, smm.no_qc, smm.supplier_id
            $having");
        return $comm->queryAll(true);
    }
    public static function get_availableStock_before($material_id, $trans_no, $noZeroStock = 0, $storloc)
    {
        //$storloc = $GLOBALS['storLoc'][U::get_user_role()]['name'];
        $having = "";
        if ($noZeroStock) {
            $having = "HAVING qty > 0";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                tgl_expired,
                no_lot,
                no_qc,
                t.supplier_id,
                s.supplier_name,
                SUM(qty) qty,
                sat
            FROM
                (SELECT
                    smm.material_id,
                    smm.tgl_expired,
                    smm.no_lot,
                    smm.no_qc,
                    smm.supplier_id,
                    smm.qty,
                    m.sat
                FROM pbu_stock_moves_material smm
                    LEFT JOIN pbu_material m ON smm.material_id = m.material_id
                WHERE
                    smm.material_id = '$material_id'
                    AND smm.storage_location = '$storloc'
                UNION ALL
                SELECT
                    smm.material_id,
                    smm.tgl_expired,
                    smm.no_lot,
                    smm.no_qc,
                    smm.supplier_id,
                    -smm.qty qty,
                    m.sat
                FROM pbu_stock_moves_material smm
                    LEFT JOIN pbu_material m ON smm.material_id = m.material_id
                WHERE
                    smm.type_no = " . SUPPOUT . "
                    AND smm.material_id = '$material_id'
                    AND smm.trans_no = '$trans_no'
                    AND smm.storage_location = '$storloc'
                ) AS t
                LEFT JOIN pbu_supplier s ON t.supplier_id = s.supplier_id
            GROUP BY t.material_id, t.tgl_expired, t.no_lot, t.no_qc, t.supplier_id
            $having");
        return $comm->queryAll(true);
    }
    public static function get_availableStock_ReturnBahanBaku($material_id = null, $batch_id = null, $noZeroStock = 0)
    {
        $where = "";
        $where2 = "";
        $having = "";
        $storloc = $GLOBALS['storLoc'][U::get_user_role()]['name'];
        if ($material_id != null) {
            $where = "AND smm.material_id = '$material_id'";
        }
        if ($batch_id != null) {
            $where2 = "AND tm.batch_id = '$batch_id'";
        }
        if ($noZeroStock) {
            $having = "HAVING qty > 0";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                smm.tgl_expired,
                Sum(smm.qty) qty,
                no_lot,
                no_qc,
                smm.supplier_id,
                s.supplier_name,
                m.sat
            FROM pbu_stock_moves_material smm
                LEFT JOIN pbu_material m ON smm.material_id = m.material_id
                LEFT JOIN pbu_transfer_material tm ON smm.trans_no = tm.transfer_material_id
                LEFT JOIN pbu_supplier s ON smm.supplier_id = s.supplier_id
            WHERE
                smm.type_no IN (" . SUPPIN . "," . SUPPOUT . ")
                AND smm.storage_location = '$storloc'
                $where
                $where2
            GROUP BY smm.material_id, smm.tgl_expired, smm.no_lot, smm.no_qc, smm.supplier_id
            $having");
        return $comm->queryAll(true);
    }
    public static function get_availableStock_ReturnBahanBaku_before($material_id, $batch_id, $trans_no, $noZeroStock = 0)
    {
        $having = "";
        $storloc = $GLOBALS['storLoc'][U::get_user_role()]['name'];
        if ($noZeroStock) {
            $having = "HAVING qty > 0";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                tgl_expired,
                SUM(qty) qty,
                no_lot,
                no_qc,
                t.supplier_id,
                s.supplier_name,
                sat
            FROM
                (SELECT
                        smm.material_id,
                        smm.tgl_expired,
                        smm.no_lot,
                        smm.no_qc,
                        smm.supplier_id,
                        smm.qty,
                        m.sat
                FROM pbu_stock_moves_material smm
                        LEFT JOIN pbu_material m ON smm.material_id = m.material_id
                        LEFT JOIN pbu_transfer_material tm ON smm.trans_no = tm.transfer_material_id
                WHERE
                        smm.type_no IN (" . SUPPIN . "," . SUPPOUT . ")
                        AND smm.storage_location = '$storloc'
                        AND smm.material_id = '$material_id'
                        AND tm.batch_id = '$batch_id'
                UNION ALL
                SELECT
                        smm.material_id,
                        smm.tgl_expired,
                        smm.no_lot,
                        smm.no_qc,
                        smm.supplier_id,
                        -smm.qty qty,
                        m.sat
                FROM pbu_stock_moves_material smm
                        LEFT JOIN pbu_material m ON smm.material_id = m.material_id
                        LEFT JOIN pbu_transfer_material tm ON smm.trans_no = tm.transfer_material_id
                WHERE
                        smm.type_no = " . SUPPOUT . "
                        AND smm.storage_location = '$storloc'
                        AND smm.material_id = '$material_id'
                        AND tm.batch_id = '$batch_id'
                        AND smm.trans_no = '$trans_no') AS t
                LEFT JOIN pbu_supplier s ON t.supplier_id = s.supplier_id
            GROUP BY t.material_id, t.tgl_expired, t.no_lot, t.no_qc, t.supplier_id
            $having");
        return $comm->queryAll(true);
    }
    public function beforeValidate()
    {
        if ($this->material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->material_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function count_biaya_beli($unit, $harga, $tgl, $type_no, $trans_no)
    {
        $command = Yii::app()->db->createCommand("select count_price_std(:material_id,:qty,:price,:tgl,:type_no,:trans_no)");
        return $command->queryScalar(array(
            ':material_id' => $this->material_id,
            ':qty' => $unit,
            ':price' => $harga,
            ':tgl' => $tgl,
            ':type_no' => $type_no,
            ':trans_no' => $trans_no
        ));
    }
    public function count_saldo_barang()
    {
        $command = Yii::app()->db->createCommand('select count_saldo_barang(:material_id)');
        return $command->queryScalar(array(':material_id' => $this->material_id));
    }
    public function count_price_std($qty, $price, $tgl, $type_no, $trans_no)
    {
        $command = Yii::app()->db->createCommand(
            'select count_price_std(:material_id,:qty,:price,:tgl,:type_no,:trans_no)'
        );
        return $command->queryScalar(array(
            ':material_id' => $this->material_id,
            ':qty' => $qty,
            ':price' => $price,
            ':tgl' => $tgl,
            ':type_no' => $type_no,
            ':trans_no' => $trans_no,
        ));
    }
    public function get_saldo($loc_code)
    {
        $where_loc_code = '';
        $params = [':material_id' => $this->material_id];
        if ($loc_code != null) {
            $where_loc_code = 'AND storage_location = :loc_code';
            $params[':loc_code'] = $loc_code;
        }
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(psmm.qty),0) FROM
          pbu_stock_moves_material AS psmm 
          WHERE psmm.material_id = :material_id $where_loc_code");
        return $comm->queryScalar($params);
    }
    protected function beforeDelete()
    {
        if (count($this->assemblies) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan assemblies.');
        }
        if (count($this->assemblyDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan details assemblies.');
        }
        if (count($this->breakdowns) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan breakdown.');
        }
        if (count($this->breakdownDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan breakdown detils.');
        }
        if (count($this->stockMovesMaterials) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan stock moves.');
        }
        if (count($this->konversiMaterialDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan konversi material.');
        }
        if (count($this->konversiMaterialDetails1) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan konversi material.');
        }
        if (count($this->purchaseOrderDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan PO.');
        }
        if (count($this->priceInts) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan PO.');
        }
        if (count($this->purchaseRequisitionDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan PO.');
        }
        if (count($this->returnPembelianDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan retur pembelian.');
        }
        if (count($this->salesDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan sales.');
        }
        if (count($this->transferMaterialDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan transfer barang.');
        }
        if (count($this->tandaterimaMaterialDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan tanda terima.');
        }
        if (count($this->stockMovesMaterials) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan tanda terima.');
        }
        if (count($this->transferBarangDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan tanda terima.');
        }
        if (count($this->terimaBarangDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan tanda terima.');
        }
        if (count($this->terimaMaterialDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan tanda terima.');
        }
        return parent::beforeDelete();
    }
}