<?php
Yii::import('application.models._base.BaseFaHppMaterial');
class FaHppMaterial extends BaseFaHppMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_materials_n_hpp_before()
    {
        $where = "";
        if ($_POST['fin_acc_report_id_before']) {
            $where = " AND hm.fin_acc_report_id = '" . $_POST['fin_acc_report_id_before'] . "' ";
        }
        //ambil qty dan hpp laporan sebelumnya
        $comm = Yii::app()->db->createCommand("
            SELECT
                m.material_id,
                IFNULL(hm.qty_after,0) qty_after,
                IFNULL(hm.hpp_after,0) hpp_after
            FROM pbu_material m
                LEFT JOIN pbu_fa_hpp_material hm ON m.material_id = hm.material_id
            WHERE
                m.tipe_material_id IN (" . MAT_RAW_MATERIAL . ", " . MAT_PACKAGING . ")
                $where
        ");
        return $comm->queryAll(true);
    }
    static function report_hpp_material($fin_acc_report_id, $sum = FALSE)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                " . ($sum ? "" : " m.kode_material, m.nama_material, m.sat,") . "
                " . ($sum ? "SUM(t.qty_before)" : "t.qty_before") . " qty_before,
                " . ($sum ? "SUM(t.hpp_before)" : "t.hpp_before") . " hpp_before,
                " . ($sum ? "SUM(t.qty_before*t.hpp_before)" : "t.qty_before*t.hpp_before") . " nominal_before,
                " . ($sum ? "SUM(t.qty)" : "t.qty") . " qty,
                " . ($sum ? "SUM(t.price)" : "t.price") . " price,
                " . ($sum ? "SUM(t.qty*t.price)" : "t.qty*t.price") . " nominal,
                " . ($sum ? "SUM(t.qty_after)" : "t.qty_after") . " qty_after,
                " . ($sum ? "SUM(t.hpp_after)" : "t.hpp_after") . " hpp_after,
                " . ($sum ? "SUM(t.qty_after*t.hpp_after)" : "t.qty_after*t.hpp_after") . " nominal_after
            FROM pbu_fa_hpp_material t
                " . ($sum ? "" : "LEFT JOIN pbu_material m ON t.material_id = m.material_id") . "
            WHERE fin_acc_report_id = :fin_acc_report_id
        ");
        $param = array(
            ':fin_acc_report_id' => $fin_acc_report_id
        );
        if ($sum)
            return $comm->queryRow(true, $param);
        else
            return $comm->queryAll(true, $param);
    }
    public function beforeValidate()
    {
        if ($this->hpp_material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->hpp_material_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function hitungHPP($date_start, $date_end)
    {
        //ambil data pembelian
        $pembelian = Yii::app()->db->createCommand("
            SELECT
                tmd.qty, tmd.sat, tmd.price
            FROM pbu_terima_material_details tmd
                    LEFT JOIN pbu_terima_material tm ON tm.terima_material_id = tmd.terima_material_id
            WHERE
                tmd.material_id = '$this->material_id' AND
                tm.tgl_terima >= '$date_start' AND
                tm.tgl_terima <= '$date_end'
        ")->queryAll(TRUE);
        //hitung HPP
        $qty = 0;
        $nominal = 0;
        foreach ($pembelian as $p) {
            $qty += (float)$p['qty'];
            $price = UnitOfMeasure::get_HargaPerSatuanStandar($p['price'], $p['sat']);
            $nominal += (float)$p['qty'] * $price;
        }
        $qty_after = (float)$this->qty_before + $qty;
        $nominal_after = ((float)$this->qty_before * (float)$this->hpp_before) + $nominal;
        $this->qty = $qty;
        $this->price = $qty ? $nominal / $qty : 0;
        $this->qty_after = $qty_after;
        $this->hpp_after = $qty_after ? ($nominal_after) / $qty_after : 0;
    }
}
