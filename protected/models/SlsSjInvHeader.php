<?php

Yii::import('application.models._base.BaseSlsSjInvHeader');

class SlsSjInvHeader extends BaseSlsSjInvHeader
{
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function primaryKey()
    {
        return 'sj_id';
    }
}