<?php
Yii::import('application.models._base.BaseTerimaMaterialDetails');
class TerimaMaterialDetails extends BaseTerimaMaterialDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        //if ($this->terima_material_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->terima_material_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}