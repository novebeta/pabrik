<?php
Yii::import('application.models._base.BaseTransferBarang');
class TransferBarang extends BaseTransferBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->transfer_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_barang_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
//    public static function get_batch_in_production($barang_id)
//    {
//        $query = "
//            SELECT
//                barang_id,
//                SUM(qty) qty,
//                tgl_exp,
//                batch
//            FROM
//                (SELECT
//                    tbd.barang_id,
//                    -tbd.qty qty,
//                    tbd.tgl_expired tgl_exp,
//                    tbd.batch
//                FROM pbu_transfer_barang_detail tbd
//                UNION ALL
//                SELECT
//                    bh.barang_id,
//                    bh.qty,
//                    bh.tgl_exp,
//                    bh.no_batch batch
//                FROM pbu_batch_hasil bh
//                ) AS t
//            WHERE
//                barang_id = '$barang_id'
//            GROUP BY barang_id, t.tgl_exp, batch
//            HAVING qty != 0
//        ";
//        
//        $comm = Yii::app()->db->createCommand($query);
//        return $comm->queryAll(TRUE);
//    }
    public function report()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                b.kode_barang kode,
                b.nama_barang nama,
                b.sat,
                tbd.batch,
                tbd.qty,
                DATE_FORMAT(tbd.tgl_exp, '%e-%b-%Y') tgl_exp
            FROM pbu_transfer_barang_detail AS tbd
                INNER JOIN pbu_barang AS b
                    ON b.barang_id = tbd.barang_id
            WHERE tbd.transfer_barang_id = :transfer_barang_id
        ");
        return $comm->queryAll(true, array(':transfer_barang_id' => $this->transfer_barang_id));
    }
}
