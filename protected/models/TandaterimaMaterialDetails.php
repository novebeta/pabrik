<?php
Yii::import('application.models._base.BaseTandaterimaMaterialDetails');
class TandaterimaMaterialDetails extends BaseTandaterimaMaterialDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        //if ($this->tandaterima_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->tandaterima_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
