<?php
Yii::import('application.models._base.BaseSysPrefs');
class SysPrefs extends BaseSysPrefs
{
    public static function get_val($index)
    {
        $pref = SysPrefs::model()->find('name_ = :name_',
            array(':name_' => $index));
        return $pref->value_;
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}