<?php
Yii::import('application.models._base.BaseBomDetail');
class BomDetail extends BaseBomDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_items($bom_id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT
                bd.*,
                m.nama_material,
                m.kode_material,
                m.tipe_material_id
            FROM pbu_bom_detail bd
                LEFT JOIN pbu_material m ON bd.material_id = m.material_id
            WHERE bd.bom_id = '$bom_id'");
        return $comm->queryAll(true);
    }
    public function beforeValidate()
    {
        //if ($this->bom_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->bom_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}