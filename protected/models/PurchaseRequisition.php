<?php
Yii::import('application.models._base.BasePurchaseRequisition');
class PurchaseRequisition extends BasePurchaseRequisition
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function setStatus($status, $pr_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_purchase_requisition SET status = :status"
            . " WHERE pr_id = :pr_id");
        return $comm->execute(array(
            ':status' => $status,
            ':pr_id' => $pr_id
        ));
    }
    static function setStatus_PO($status, $pr_id)
    {
        $comm = Yii::app()->db->createCommand("
            UPDATE pbu_purchase_requisition SET status_po = :status"
            . " WHERE pr_id = :pr_id");
        return $comm->execute(array(
            ':status' => $status,
            ':pr_id' => $pr_id
        ));
    }
    public function beforeValidate()
    {
        if ($this->pr_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pr_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        if ($this->reject_no_ba !== null) {
            $this->reject_id_user = User()->getId();
            $command3 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command3->queryScalar();
            $this->reject_tdate = $dt;
        }
        return parent::beforeValidate();
    }
    public function setStatusCLOSED()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                SUM(t.qty)
            FROM
                (SELECT
                    SUM(pod.qty) qty
                FROM pbu_purchase_requisition_details pod
                WHERE
                    pod.pr_id = :pr_id
                UNION ALL
                SELECT
                    -SUM(pod.qty) qty
                FROM pbu_purchase_order_details pod
                    LEFT JOIN pbu_purchase_order po ON po.po_id = pod.po_id
                WHERE
                    po.pr_id = :pr_id) t
        ");
        $val = $comm->queryScalar(array(':pr_id' => $this->pr_id));
        $this->status = $val > 0 ? PR_PROCESS : PR_CLOSED;
    }
    public function report()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                pm.nama_material,
                pprd.sat,
                pprd.qty,
                pprd.note,
                ppr.vendor_name,
                DATE_FORMAT(ppr.tgl_required,'%d-%b-%Y') tgl_required
            FROM pbu_purchase_requisition_details AS pprd
                LEFT JOIN pbu_material AS pm ON pm.material_id = pprd.material_id
                LEFT JOIN pbu_purchase_requisition AS ppr ON ppr.pr_id = pprd.pr_id
            WHERE pprd.pr_id = :pr_id
        ");
        return $comm->queryAll(true, array(':pr_id' => $this->pr_id));
    }
}