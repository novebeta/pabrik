<?php
Yii::import('application.models._base.BaseCustomerPayment');
class CustomerPayment extends BaseCustomerPayment
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->customer_payment_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->customer_payment_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->customer_payment_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->customer_payment_id = $uuid;
        }
        if ($inc) {
            $subtgl = substr($tgl, 0, 4);
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_customer_payment WHERE SUBSTR(doc_ref FROM 8 FOR 4) = :tgl AND customer_payment_id != :customer_payment_id;");
            $nextval = $command->queryScalar([
                ':tgl' => $subtgl,
                ':customer_payment_id' => $this->customer_payment_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'CUSPAY/' . $tgl . '/' . $newinc;
    }
    public static function urutDocRef($month, $year)
    {
        Yii::app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var CustomerPayment[] $ass */
            $criteria = new CDbCriteria();
            $criteria->addCondition("MONTH(tgl) = :month AND YEAR(tgl) = :year");
            $params = [
                ':month' => $month,
                ':year' => $year
            ];
            $criteria->order = "tgl ASC,tdate ASC";
            $criteria->params = $params;
            $ass = CustomerPayment::model()->findAll($criteria);
            $nextval = 1;
            foreach ($ass as $a) {
                $fmt = '%05d';
                $newinc = sprintf($fmt, $nextval);
                $tgl = sql2date($a->tgl, 'yyMMdd');
                $a->doc_ref = 'CUSPAY/' . $tgl . '/' . $newinc;
                if (!$a->save(false)) {
                    throw new Exception('Cust Payment gagal di simpan.' . "\n");
                }
//                $a->createDocRef(true);
                Refs::model()->updateAll(['reference' => $a->doc_ref],
                    "type_no = :type_no",
                    [':type_no' => $a->customer_payment_id]);
                BankTrans::model()->updateAll(['ref' => $a->doc_ref],
                    'trans_no = :trans_no',
                    [':trans_no' => $a->customer_payment_id]);
                StockMovesMaterial::model()->updateAll(['reference' => $a->doc_ref],
                    'trans_no = :trans_no',
                    [':trans_no' => $a->customer_payment_id]);
                Apar::model()->updateAll(['reference' => $a->doc_ref],
                    'trans_no = :trans_no',
                    [':trans_no' => $a->customer_payment_id]);
                $nextval++;
                echo $a->doc_ref . "\n";
            }
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            echo $ex->getMessage();
        }
        Yii::app()->db->autoCommit = true;
    }
    public function updateTgl($tgl, $ganti_bulan)
    {
        $this->tgl = $tgl;
        $this->createDocRef($ganti_bulan);
        if (!$this->save()) {
            throw new Exception(CHtml::errorSummary($this));
        }
        GlTrans::model()->updateAll(['tran_date' => $tgl], "type = :type_no AND type_no = :trans_no",
            array(':type_no' => CUST_PAYMENT, ':trans_no' => $this->customer_payment_id));
        Refs::model()->updateAll(['reference' => $this->doc_ref], "type_ = :type_ AND type_no = :type_no",
            array(':type_' => CUST_PAYMENT, ':type_no' => $this->customer_payment_id));
        BankTrans::model()->updateAll(['tgl' => $tgl, 'ref' => $this->doc_ref], 'type_ = :type_ AND trans_no = :trans_no',
            [':type_' => CUST_PAYMENT, ':trans_no' => $this->customer_payment_id]);
        Apar::model()->updateAll(['tran_date' => $tgl, 'reference' => $this->doc_ref], 'type_no = :type_no AND trans_no = :trans_no',
            [':type_no' => CUST_PAYMENT, ':trans_no' => $this->customer_payment_id]);
    }
    public function getItems()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT pkd.doc_ref,ABS(pkd.kas_diterima) kas_diterima,pkd.remark
              FROM pbu_customer_payment_detil AS pkd
            WHERE pkd.customer_payment_id = :customer_payment_id");
        return $comm->queryAll(true, [':customer_payment_id' => $this->customer_payment_id]);
    }
    public function void__()
    {
        $id = $this->customer_payment_id;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        $status = true;
        $msg = 'Customer Payment berhasil di void.';
        try {
            GlTrans::model()->deleteAll('type = :type AND type_no = :type_no',
                [':type' => CUST_PAYMENT, ':type_no' => $id]);
            BankTrans::model()->deleteAll('type_ = :type_ AND trans_no = :trans_no',
                [':type_' => CUST_PAYMENT, ':trans_no' => $id]);
            Apar::model()->deleteAll('type_no = :type_no AND trans_no = :trans_no',
                [':type_no' => CUST_PAYMENT, ':trans_no' => $id]);
            /** @var CustomerPayment $cp */
            $cp = CustomerPayment::model()->findByPk($id);
            if ($cp->no_bg_cek != null) {
                /** @var Giro $giro */
                $giro = Giro::model()->findByPk($cp->no_bg_cek);
                if ($giro == null) {
                    throw new Exception('Pembayaran menggunakan giro, tetapi giro tidak ditemukan.');
                }
                $giro->used = $giro->isUsed() ? 0 : 1;
                if (!$giro->save()) {
                    throw new Exception(CHtml::errorSummary($giro));
                }
            }
            foreach ($cp->customerPaymentDetils as $row) {
                if ($row->payment_tipe == 0 || $row->payment_tipe == 1) {
                    /** @var $sj Sj */
                    $sj = Sj::model()->findByPk($row->reference_item_id);
                    if ($sj == null) {
                        throw new Exception('Fatal error, Delivery tidak ditemukan!!!');
                    }
                    if ($sj->lunas == 1) {
                        $sj->lunas = 0;
                        if (!$sj->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => 'Delivery')) . CHtml::errorSummary($sj));
                        }
                    }
                }
            }
            $cp->void = 1;
            if (!$cp->save()) {
                throw new Exception('Customer Payment gagal di void');
            }
            $transaction->commit();
            $status = true;
        } catch (Exception $e) {
            $transaction->rollback();
            $status = false;
            $msg = $e->getMessage();
        }
        return [
            'status' => $status,
            'msg' => $msg
        ];
    }
}