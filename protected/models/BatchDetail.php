<?php
Yii::import('application.models._base.BaseBatchDetail');
class BatchDetail extends BaseBatchDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function getBOMitems($bom_id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT
                bd.bom_detail_id,
                bd.bom_id,
                bd.material_id,
                bd.qty AS qty_bom,
                m.nama_material,
                m.kode_material,
                m.tipe_material_id,
                bd.sat
            FROM pbu_bom_detail bd
                LEFT JOIN pbu_material m ON bd.material_id = m.material_id
            WHERE bd.bom_id = '$bom_id'");
        return $comm->queryAll(true);
    }
    public static function getitems($batch_id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT
                bd.batch_detail_id,
                bd.batch_id,
                bd.material_id,
                bd.qty,
                t.qty_bom,
                m.nama_material,
                m.kode_material,
                m.tipe_material_id,
                bd.sat
            FROM pbu_batch_detail bd
                LEFT JOIN (
                    SELECT
                        bod.material_id,
                        bod.qty qty_bom,
                        ba.batch_id
                    FROM pbu_bom_detail bod
                        LEFT JOIN pbu_batch ba ON ba.bom_id = bod.bom_id
                    WHERE
                        ba.batch_id = '$batch_id'
                ) t ON t.batch_id = bd.batch_id AND t.material_id = bd.material_id
                LEFT JOIN pbu_material m ON bd.material_id = m.material_id
            WHERE
                bd.batch_id = '$batch_id'");
        return $comm->queryAll(true);
    }
    public function beforeValidate()
    {
        //if ($this->batch_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->batch_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
