<?php
Yii::import('application.models._base.BaseAssemblyDetails');
class AssemblyDetails extends BaseAssemblyDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->assembly_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->assembly_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}