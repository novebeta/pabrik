<?php
Yii::import('application.models._base.BaseGrup');
class Grup extends BaseGrup
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    protected function beforeDelete()
    {
        $trans = ['Barang'];
        foreach ($trans as $k) {
            $count = CActiveRecord::model($k)->count("grup_id = :grup_id",
                array(':grup_id' => $this->grup_id));
            if ($count > 0) {
                throw new Exception("Grup ini tidak bisa di hapus karena sudah di pakai ($k).");
                return false;
            }
        }
        return parent::beforeDelete();
    }
}