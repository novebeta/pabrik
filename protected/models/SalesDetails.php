<?php
Yii::import('application.models._base.BaseSalesDetails');
class SalesDetails extends BaseSalesDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->sales_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sales_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}