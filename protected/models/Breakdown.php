<?php
Yii::import('application.models._base.BaseBreakdown');
class Breakdown extends BaseBreakdown
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->breakdown_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->breakdown_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->breakdown_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->breakdown_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_breakdown WHERE SUBSTR(doc_ref FROM 7 FOR 4) = :tgl AND breakdown_id != :breakdown_id ;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':breakdown_id' => $this->breakdown_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'BRK/' . $tgl . '/' . $newinc;
    }
    public function getItems(){
        $comm = Yii::app()->db->createCommand("
            SELECT 0 as num,pbd.material_id,pbd.qty,pm.kode_material,pm.nama_material,pm.sat,pbd.loc_code
            FROM pbu_breakdown_details AS pbd
                INNER JOIN pbu_material AS pm ON pbd.material_id = pm.material_id
            WHERE pbd.breakdown_id = :breakdown_id");
        return $comm->queryAll(true, [':breakdown_id'=> $this->breakdown_id]);
    }
}