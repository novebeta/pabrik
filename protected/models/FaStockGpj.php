<?php
Yii::import('application.models._base.BaseFaStockGpj');
class FaStockGpj extends BaseFaStockGpj
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_barangjadi_n_hpp_before()
    {
        $where = "";
        if ($_POST['fin_acc_report_id_before']) {
            $where = " AND fsg.fin_acc_report_id = '" . $_POST['fin_acc_report_id_before'] . "' ";
        }
        //ambil qty dan hpp laporan sebelumnya
        $comm = Yii::app()->db->createCommand("
            SELECT
                b.barang_id,
                IFNULL(fsg.qty_after,0) qty_after,
                IFNULL(fsg.hpp_after,0) hpp_after
            FROM pbu_barang b
                LEFT JOIN pbu_fa_stock_gpj fsg ON b.barang_id = fsg.barang_id
            WHERE
                b.tipe_barang_id IN (" . BRG_PRODUK_LOKAL . ", " . BRG_PRODUK_EXPORT . ")
                $where
        ");
        return $comm->queryAll(true);
    }
    static function report_Stock_BJ($fin_acc_report_id, $sum = FALSE)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                " . ($sum ? "" : " b.kode_barang, b.nama_barang, b.sat,") . "
                " . ($sum ? "SUM(t.qty_before)" : "t.qty_before") . " qty_before,
                " . ($sum ? "SUM(t.hpp_before)" : "t.hpp_before") . " hpp_before,
                " . ($sum ? "SUM(t.qty_before*t.hpp_before)" : "t.qty_before*t.hpp_before") . " nominal_before,
                " . ($sum ? "SUM(t.qty_in)" : "t.qty_in") . " qty_in,
                " . ($sum ? "SUM(t.hpp_per_pce)" : "t.hpp_per_pce") . " hpp_per_pce,
                " . ($sum ? "SUM(t.qty_in*t.hpp_per_pce)" : "t.qty_in*t.hpp_per_pce") . " nominal_in,
                " . ($sum ? "SUM(t.qty_out)" : "t.qty_out") . " qty_out,
                " . ($sum ? "SUM(t.hpp_out)" : "t.hpp_out") . " hpp_out,
                " . ($sum ? "SUM(t.qty_out*t.hpp_out)" : "t.qty_out*t.hpp_out") . " nominal_out,
                " . ($sum ? "SUM(t.qty_after)" : "t.qty_after") . " qty_after,
                " . ($sum ? "SUM(t.hpp_after)" : "t.hpp_after") . " hpp_after,
                " . ($sum ? "SUM(t.qty_after*t.hpp_after)" : "t.qty_after*t.hpp_after") . " nominal_after
            FROM pbu_fa_stock_gpj t
                " . ($sum ? "" : "LEFT JOIN pbu_barang b ON t.barang_id = b.barang_id") . "
            WHERE fin_acc_report_id = :fin_acc_report_id
        ");
        $param = array(
            ':fin_acc_report_id' => $fin_acc_report_id
        );
        if ($sum)
            return $comm->queryRow(true, $param);
        else
            return $comm->queryAll(true, $param);
    }
    public function beforeValidate()
    {
        if ($this->fa_stock_gpj_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fa_stock_gpj_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function hitungHPP($date_start, $date_end)
    {
        //HPP BJ per pce
        $hppBJ = Yii::app()->db->createCommand("
            SELECT hpp_per_pce
            FROM pbu_fa_hpp_barangjadi
            WHERE barang_id = :barang_id AND fin_acc_report_id = :fin_acc_report_id
        ")->queryScalar(array(
            ':barang_id' => $this->barang_id,
            ':fin_acc_report_id' => $this->fin_acc_report_id
        ));
        $this->hpp_per_pce = (float)$hppBJ;
        //ambil data keluar masuk barang di GPJ
        $stockmoveGPJ = Yii::app()->db->createCommand("
            SELECT
                SUM(IF (sm.qty > 0 AND sm.type_no IN (" . SUPPIN . ") AND DATE(sm.tran_date) >= :from
                  AND DATE(sm.tran_date) <= :to, sm.qty, 0)) suppin,
                SUM(IF (sm.qty < 0 AND sm.type_no IN (" . SUPPOUT . ", " . SURAT_JALAN . ", " . RETURBELI . ", " . SAMPEL_QC . ") AND DATE(sm.tran_date) >= :from
                  AND DATE(sm.tran_date) <= :to, ABS(sm.qty), 0)) suppout
            FROM
                pbu_stock_moves sm
            WHERE
                sm.storage_location = :storage_location
                AND sm.barang_id = :barang_id
        ")->queryRow(true, array(
            ':from' => $date_start,
            ':to' => $date_end,
            ':storage_location' => $GLOBALS['storLoc'][USER_GPJ]['name'],
            ':barang_id' => $this->barang_id
        ));
        $this->qty_in = (float)$stockmoveGPJ['suppin'];
        $this->qty_out = (float)$stockmoveGPJ['suppout'];
        $this->qty_after = (float)$this->qty_before + $this->qty_in - $this->qty_out;
        //Hitung HPP
        $totalNilai = $this->qty_before * $this->hpp_before + $this->qty_in * $this->hpp_per_pce;
        $qty = $this->qty_before + $this->qty_in;
        $hpp_after = $qty == 0 ? 0 : $totalNilai / $qty;
        $this->hpp_out = $hpp_after;
        $this->hpp_after = $hpp_after;
    }
}
