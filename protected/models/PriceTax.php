<?php
Yii::import('application.models._base.BasePriceTax');
class PriceTax extends BasePriceTax
{
    public static function get_price($barang_id, $wil_tax_id)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("barang_id = :barang_id");
        $criteria->addCondition("wil_tax_id = :wil_tax_id");
        $criteria->params = array(':barang_id' => $barang_id, ':wil_tax_id' => $wil_tax_id);
        return PriceTax::model()->find($criteria);
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}