<?php
Yii::import('application.models._base.BaseFolderBreakdown');
class FolderBreakdown extends BaseFolderBreakdown
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'folder_id';
    }
}