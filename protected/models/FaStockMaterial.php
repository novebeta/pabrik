<?php
Yii::import('application.models._base.BaseFaStockMaterial');
class FaStockMaterial extends BaseFaStockMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_stock_material_before($fin_acc_report_id)
    {
        $where = "";
        if ($_POST['fin_acc_report_id_before']) {
            $where = " AND fsm.fin_acc_report_id = '" . $_POST['fin_acc_report_id_before'] . "' ";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                m.material_id,
                IFNULL(fsm.qty_after,0) qty_before,
                IFNULL(fsm.hpp,0) hpp_before,
                IFNULL(fhm.hpp_after,0) hpp
            FROM pbu_material m
                LEFT JOIN pbu_fa_stock_material fsm ON m.material_id = fsm.material_id
                LEFT JOIN pbu_fa_hpp_material fhm ON m.material_id = fhm.material_id
            WHERE
                m.tipe_material_id IN (10, 20)
                $where
                AND fhm.fin_acc_report_id = :fin_acc_report_id
        ");
        return $comm->queryAll(true, array(':fin_acc_report_id' => $fin_acc_report_id));
    }
    public function beforeValidate()
    {
        if ($this->fa_stock_material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fa_stock_material_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function hitungStock($date_start, $date_end)
    {
        //ambil data pembelian dan stock terakir (ambil dari Mutasi stock)
        $stcok = Yii::app()->db->createCommand("
                SELECT 
                    m.material_id,
                    SUM(IF (smm.qty > 0 AND smm.type_no IN (" . SJ_SUPPLIER . "," . RETURN_PEMBELIAN . ") AND DATE(smm.trans_date) >= :from 
                        AND DATE(smm.trans_date) <= :to, smm.qty, 0)) qty_pembelian,
                    SUM(IF (DATE(smm.trans_date) <= :to, smm.qty, 0)) qty_after
                FROM 
                    pbu_stock_moves_material AS smm
                    LEFT JOIN pbu_material m ON (smm.material_id = m.material_id)
                WHERE
                    smm.storage_location = :storage_location AND
                    smm.material_id = :material_id
                GROUP BY 
                    m.material_id
            ")->queryRow(true, array(
            ':from' => $date_start,
            ':to' => $date_end,
            ':storage_location' => $GLOBALS['storLoc'][USER_PPIC]['name'],
            ':material_id' => $this->material_id
        ));
        //simpan
        $this->qty_pembelian = $stcok['qty_pembelian'];
        $this->qty_after = $stcok['qty_after'];
    }
}
