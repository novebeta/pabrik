<?php
Yii::import('application.models._base.BaseKonversiMaterial');
class KonversiMaterial extends BaseKonversiMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->konversi_material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->konversi_material_id= $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'yyMMdd');
        if ($this->konversi_material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->konversi_material_id = $uuid;
        }
        if ($inc) {
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_konversi_material WHERE SUBSTR(doc_ref FROM 6 FOR 4) = :tgl AND 
          konversi_material_id != :konversi_material_id ;");
            $nextval = $command->queryScalar([
                ':tgl' => substr($tgl, 0, 4),
                ':konversi_material_id' => $this->konversi_material_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'KON/' . $tgl . '/' . $newinc;
    }
}