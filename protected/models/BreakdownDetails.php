<?php
Yii::import('application.models._base.BaseBreakdownDetails');
class BreakdownDetails extends BaseBreakdownDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->breakdown_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->breakdown_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}