<?php
Yii::import('application.models._base.BaseReturSales');
class ReturSales extends BaseReturSales
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'sales_id';
    }
}