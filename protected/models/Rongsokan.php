<?php
Yii::import('application.models._base.BaseRongsokan');
class Rongsokan extends BaseRongsokan
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'material_id';
    }
}