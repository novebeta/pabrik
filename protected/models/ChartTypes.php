<?php
Yii::import('application.models._base.BaseChartTypes');
class ChartTypes extends BaseChartTypes
{
    public static function get_chart_types_by_class($ctype)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        nct.id,nct.`name`,nct.parent,nct.hide
        FROM pbu_chart_types AS nct
        INNER JOIN pbu_chart_class AS ncc ON nct.class_id = ncc.cid
        WHERE ncc.ctype = :ctype AND LENGTH(parent) = 0
        ORDER BY nct.seq");
        return $comm->queryAll(true, array(':ctype' => $ctype));
    }
    public static function get_child($id)
    {
        $comm = Yii::app()->db->createCommand("SELECT * FROM
        pbu_chart_types AS nct WHERE nct.parent = :id
        ORDER BY nct.seq");
        return $comm->queryAll(true, array(':id' => $id));
    }
    public static function get_arr_all_child($type_id)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('parent = :parent');
        $criteria->order = 'seq';
        $criteria->params = array(':parent' => $type_id);
        $chart = ChartTypes::model()->findAll($criteria);
        $arr = array();
        foreach ($chart as $key => $item) {
            $arr[] = array(
                'id' => $item->id,
                'account_code' => '',
                'account_name' => $item->name,
                'node_type' => 'T',
                'node_name' => 'Chart Types',
                'hide' => $item->hide,
                'parent' => $item->parent,
                'class_id' => $item->class_id,
                'seq' => $item->seq,
                'children' => self::get_arr_all_child($item->id)
            );
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('kategori = :kategori');
        $criteria->order = 'account_code';
        $criteria->params = array(':kategori' => $type_id);
        $chartMaster = ChartMaster::model()->findAll($criteria);
//        $arr = array();
        foreach ($chartMaster as $key => $item) {
            $arr[] = array(
                'id' => $item->account_code,
                'account_code' => $item->account_code,
                'account_name' => $item->account_name,
                'node_type' => 'M',
                'node_name' => 'COA',
                'saldo_normal' => $item->saldo_normal,
                'kategori' => $item->kategori,
                'tipe' => $item->tipe,
                'leaf' => true
            );
        }
        return $arr;
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id = $uuid;
        }
        return parent::beforeValidate();
    }
}