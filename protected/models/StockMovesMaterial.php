<?php
Yii::import('application.models._base.BaseStockMovesMaterial');
class StockMovesMaterial extends BaseStockMovesMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_saldo_item($material_id, $storage_location)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM pbu_stock_moves_material AS nsm
        WHERE nsm.material_id = :material_id AND nsm.storage_location = :storage_location");
        return $comm->queryScalar(array(
            ':material_id' => $material_id,
            ':storage_location' => $storage_location
        ));
    }
    protected function afterSave()
    {
        parent::afterSave();
        if ($this->qty < 0) {
            $count = self::get_saldo_item($this->material_id, $this->storage_location);
            if ($count < 0) {
                throw new Exception('Stok material ' . $this->material->nama_material .
                    ' tidak cukup, kurang ' . abs($count));
            }
        }
    }
    public static function get_saldo_item_before($kode_material, $tgl, $tgl_expired = NULL, $no_lot = NULL, $no_qc = NULL, $supplier_id = NULL, $storloc)
    {
        $param = array(
            ':kode_material' => $kode_material,
            ':tgl' => $tgl
        );
        $comm = Yii::app()->db->createCommand("
            SELECT COALESCE(Sum(smm.qty),0) 
            FROM pbu_stock_moves_material AS smm
                    INNER JOIN pbu_material m ON smm.material_id = m.material_id
            WHERE  
            smm.material_id = :kode_material 
            AND smm.trans_date < :tgl
            AND smm.storage_location = '$storloc'
            " . ($tgl_expired ? "AND smm.tgl_expired = '$tgl_expired'" : "") . "
            " . ($no_lot ? "AND smm.no_lot = '$no_lot'" : "") . "
            " . ($no_qc ? "AND smm.no_qc = '$no_qc'" : "") . "
            " . ($supplier_id ? "AND smm.supplier_id = '$supplier_id'" : "") . "
        ");
        return $comm->queryScalar($param);
    }
    public function beforeValidate()
    {
        if ($this->stock_moves_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->stock_moves_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
}