<?php
Yii::import('application.models._base.BasePerbaikanMesin');
class PerbaikanMesin extends BasePerbaikanMesin
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->perbaikan_mesin_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->perbaikan_mesin_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
    public function reportPemakaianSparepart()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                pm.kode_material,
                pm.nama_material,
                DATE_FORMAT(pmd.tgl, '%e-%b-%Y') tgl,
                pmd.qty,
                pmd.sat,
                pmd.ket,
                ps.supplier_name
            FROM pbu_perbaikan_mesin_detail pmd
                INNER JOIN pbu_material pm ON pm.material_id = pmd.material_id
                INNER JOIN pbu_supplier ps ON ps.supplier_id = pmd.supplier_id
            WHERE
                pmd.perbaikan_mesin_id = :perbaikan_mesin_id
        ");
        return $comm->queryAll(true, array(':perbaikan_mesin_id' => $this->perbaikan_mesin_id));
    }
}
