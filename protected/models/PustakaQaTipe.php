<?php
Yii::import('application.models._base.BasePustakaQaTipe');
class PustakaQaTipe extends BasePustakaQaTipe
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->pustaka_qa_tipe_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pustaka_qa_tipe_id = $uuid;
        }
        return parent::beforeValidate();
    }
}
