<?php
Yii::import('application.models._base.BaseTransferBarangDetail');
class TransferBarangDetail extends BaseTransferBarangDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function getitems($id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT
                bd.*,
                b.kode_barang,
                b.nama_barang,
                b.sat,
                b.grup_id,
                b.tipe_barang_id
            FROM pbu_transfer_barang_detail bd
                LEFT JOIN pbu_barang b ON bd.barang_id = b.barang_id
            WHERE transfer_barang_id = '$id'");
        return $comm->queryAll(true);
    }
    public static function getitems2($id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT *
            FROM pbu_transfer_barang_detail
            WHERE transfer_barang_id = '$id'");
        return $comm->queryAll(true);
    }
    public function beforeValidate()
    {
        //if ($this->transfer_barang_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->transfer_barang_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
