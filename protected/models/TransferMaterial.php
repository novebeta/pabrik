<?php
Yii::import('application.models._base.BaseTransferMaterial');
class TransferMaterial extends BaseTransferMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function getTransferedItems($batch_id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT
                tm.transfer_material_id,
                tm.tgl,
                tm.doc_ref,
                smm.material_id,
                m.kode_material,
                m.nama_material,
                smm.qty,
                bd.sat,
                smm.tgl_expired,
                smm.no_lot,
                smm.no_qc,
                m.tipe_material_id
            FROM pbu_stock_moves_material smm
                LEFT JOIN pbu_transfer_material tm ON smm.trans_no = tm.transfer_material_id
                LEFT JOIN pbu_material m ON smm.material_id = m.material_id
                LEFT JOIN pbu_batch_detail bd ON smm.material_id = bd.material_id AND tm.batch_id = bd.batch_id
            WHERE 
                smm.type_no IN (" . SUPPIN . ", " . SUPPOUT . ")
                AND tm.batch_id = '$batch_id'
                AND smm.storage_location = '" . $GLOBALS['storLoc'][USER_PRODUKSI]['name'] . "'
            ORDER BY smm.tdate
            ");
        return $comm->queryAll(true);
    }
    public function beforeValidate()
    {
        if ($this->transfer_material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_material_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        if ($this->accepted == 1 && $this->tdate_accept == null) {
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate_accept = $dt;
        }
        return parent::beforeValidate();
    }
    public function report()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                m.kode_material kode,
                m.nama_material nama,
                tmd.sat,
                DATE_FORMAT(tmd.tgl_expired, '%e-%b-%Y') tgl_expired,
                no_lot,
                no_qc,
                tmd.qty
            FROM pbu_transfer_material_detail AS tmd
                INNER JOIN pbu_material AS m
                    ON m.material_id = tmd.material_id
            WHERE tmd.transfer_material_id = :transfer_material_id
        ");
        return $comm->queryAll(true, array(':transfer_material_id' => $this->transfer_material_id));
    }
}
