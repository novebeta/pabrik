<?php
Yii::import('application.models._base.BaseTipeMaterial');
class TipeMaterial extends BaseTipeMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    protected function beforeDelete()
    {
        if (count($this->materials) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan Material.');
        }
        return parent::beforeDelete();
    }
    public function beforeValidate()
    {
        if ($this->tipe_material_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->tipe_material_id = $uuid;
        }
        return parent::beforeValidate();
    }
}