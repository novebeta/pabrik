<?php
Yii::import('application.models._base.BaseTerimaBarang');
class TerimaBarang extends BaseTerimaBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function pembelian_faktur($dari, $sampai)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ptb.doc_ref_inv, ptb.tgl_inv,PS.supplier_name,
        ptb.tgl_tempo,ptb.doc_ref,ppp.doc_ref AS doc_ref_po,
        ppp.doc_ref_pr,ptb.no_invoice,ptb.tanda_terima,ptb.total
        FROM pbu_po_pr AS ppp
        INNER JOIN pbu_terima_barang AS ptb ON ptb.po_in_id = ppp.po_in_id
        INNER JOIN pbu_supplier AS PS ON ppp.supplier_id = PS.supplier_id
        WHERE ptb.arus = 0 AND ptb.tgl_inv >= :dari AND ptb.tgl_inv <= :sampai
        ORDER BY ptb.doc_ref_inv ASC");
        return $comm->queryAll(true, array(':dari' => $dari, ':sampai' => $sampai));
    }
    static function pembelian_detil($dari, $sampai)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ptb.doc_ref_inv,ptb.tgl_inv,ptb.no_invoice,
        ps.supplier_name,pb.kode_barang,pb.nama_barang,
        ptbd.qty,pb.sat,ptbd.price,ptbd.total
        FROM pbu_po_pr AS ppp
        INNER JOIN pbu_supplier AS ps ON ppp.supplier_id = ps.supplier_id
        INNER JOIN pbu_terima_barang AS ptb ON ptb.po_in_id = ppp.po_in_id
        INNER JOIN pbu_terima_barang_details AS ptbd ON ptbd.terima_barang_id = ptb.terima_barang_id
        INNER JOIN pbu_barang AS pb ON ptbd.barang_id = pb.barang_id
        WHERE ptb.arus = 0 AND ptb.tgl_inv >= :dari AND ptb.tgl_inv <= :sampai
        ORDER BY ptb.doc_ref_inv ASC");
        return $comm->queryAll(true, array(':dari' => $dari, ':sampai' => $sampai));
    }
    public function getItems()
    {
        $comm = Yii::app()->db->createCommand("
            SELECT (@num := @num+1) as num,ptbd.terima_barang_details_id,ptbd.qty,ptbd.price,ptbd.total,ptbd.barang_id,ptbd.ket,
              ptbd.terima_barang_id,ptbd.disc_rp,ptbd.disc,ptbd.bruto,ptbd.vat,ptbd.vatrp,ptbd.po_id,
              ptb.loc_code,pm.kode_material,pm.nama_material,pm.sat
            FROM pbu_terima_barang AS ptb
                INNER JOIN pbu_terima_barang_details AS ptbd ON ptbd.terima_barang_id = ptb.terima_barang_id
                INNER JOIN pbu_material AS pm ON ptbd.barang_id = pm.material_id,(select @num:=0) as y
            WHERE ptb.terima_barang_id = :terima_barang_id");
        return $comm->queryAll(true, [':terima_barang_id' => $this->terima_barang_id]);
    }
    public function beforeValidate()
    {
        if ($this->terima_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_barang_id = $uuid;
        }
//        if ($this->id == null) {
//            $this->grn_id_user = Yii::app()->user->getId();
//        }
//        if ($this->grn_tdate == null) {
//            $this->grn_tdate = new CDbExpression('NOW()');
//        }
        return parent::beforeValidate();
    }

    public function createDocRef($inc = true)
    {
        $newinc = substr($this->doc_ref, -5);
        $tgl = sql2date($this->tgl, 'ddMMyy');
        if ($this->terima_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_barang_id = $uuid;
        }
        if ($inc) {
            $subtgl = substr($tgl, 2, 4);
            $command = $this->dbConnection->createCommand("SELECT (IFNULL(MAX(SUBSTR(doc_ref,-5)),0)+1)
          FROM pbu_terima_barang WHERE SUBSTR(doc_ref FROM 6 FOR 4) = :tgl AND terima_barang_id != :terima_barang_id;");
            $nextval = $command->queryScalar([
                ':tgl' => $subtgl,
                ':terima_barang_id' => $this->terima_barang_id
            ]);
            $fmt = '%05d';
            $newinc = sprintf($fmt, $nextval);
        }
        $this->doc_ref = 'TI/' . $tgl . '/' . $newinc;
    }
}