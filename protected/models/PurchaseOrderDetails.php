<?php
Yii::import('application.models._base.BasePurchaseOrderDetails');
class PurchaseOrderDetails extends BasePurchaseOrderDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    static function get_items($po_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                pd.po_id,
                p.doc_ref no_po,
                pd.material_id,
                pd.qty,
                pd.sat,
                pd.price
            FROM pbu_purchase_order_details pd
                LEFT JOIN pbu_purchase_order p ON p.po_id = pd.po_id
            WHERE
                pd.po_id = :po_id
        ");
        return $comm->queryAll(true, array(':po_id' => $po_id));
    }
    public function beforeValidate()
    {
        //if ($this->po_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->po_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}