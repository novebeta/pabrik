<?php
Yii::import('application.models._base.BaseStockMoves');
class StockMoves extends BaseStockMoves
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_saldo_item_before($kode_barang, $tgl)
    {
        $where = "";
        $param = array(':kode_barang' => $kode_barang, ':tgl' => $tgl);
        $comm = Yii::app()->db->createCommand("SELECT
        COALESCE(Sum(nsm.qty),0) FROM nscc_stock_moves AS nsm
        INNER JOIN nscc_barang nb ON nsm.barang_id = nb.barang_id
        WHERE  nb.kode_barang = :kode_barang AND nsm.tran_date < :tgl");
        return $comm->queryScalar($param);
    }
    public static function get_saldo_item_batch_before($kode_barang, $tgl_exp, $batch, $tgl, $storloc)
    {
//        $where = "";
        $param = array(
            ':kode_barang' => $kode_barang,
            ':tgl' => $tgl,
            //':batch' => $batch
        );
//        $comm = Yii::app()->db->createCommand("SELECT
//        COALESCE(Sum(nsm.qty),0) FROM pbu_stock_moves AS nsm
//        INNER JOIN pbu_barang nb ON nsm.barang_id = nb.barang_id
//        WHERE  nsm.barang_id = :kode_barang AND nsm.batch = :batch
//        AND nsm.tran_date < :tgl");
        $comm = Yii::app()->db->createCommand("
            SELECT
                COALESCE(Sum(nsm.qty),0)
            FROM
                (   SELECT
                        *
                    FROM pbu_stock_moves
                    WHERE
                        barang_id = :kode_barang 
                        AND storage_location = '$storloc' 
                ) nsm
            WHERE
                " . ($batch == "" || $batch == NULL ? "" : "nsm.batch = '$batch' ") .
            "AND nsm.tran_date < :tgl");
        return $comm->queryScalar($param);
    }
    public static function get_saldo_item($barang_id, $store)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM pbu_stock_moves AS nsm
        WHERE nsm.barang_id = :barang_id AND nsm.store = :store");
        return $comm->queryScalar(array(
            ':barang_id' => $barang_id,
            ':store' => $store
        ));
    }
    public function beforeValidate()
    {
        if ($this->stock_moves_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->stock_moves_id = $uuid;
            $this->id_user = User()->getId();
            $command2 = $this->dbConnection->createCommand("SELECT NOW();");
            $dt = $command2->queryScalar();
            $this->tdate = $dt;
        }
        return parent::beforeValidate();
    }
}