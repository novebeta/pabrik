<?php
Yii::import('application.models._base.BasePriceInt');
class PriceInt extends BasePriceInt
{
    public static function get_price($barang_id, $wil_int_id)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("barang_id = :barang_id");
        $criteria->addCondition("wil_int_id = :wil_int_id");
        $criteria->params = array(':barang_id' => $barang_id, ':wil_int_id' => $wil_int_id);
        return PriceInt::model()->find($criteria);
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}