<?php
Yii::import('application.models._base.BaseUnitOfMeasure');
class UnitOfMeasure extends BaseUnitOfMeasure
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_HargaPerSatuanStandar($price, $sat)
    {
        $conversion = Yii::app()->db->createCommand("SELECT conversion FROM pbu.pbu_unit_of_measure WHERE code = '$sat' LIMIT 1;")->queryScalar();
        return (float)$price / (float)$conversion;
    }
    public static function get_qtySatuan($qty, $sat)
    {
        $conversion = Yii::app()->db->createCommand("SELECT conversion FROM pbu.pbu_unit_of_measure WHERE code = '$sat' LIMIT 1;")->queryScalar();
        return (float)$qty / (float)$conversion;
    }
    public static function get_qtySatuanStandar($qty, $sat)
    {
        $conversion = Yii::app()->db->createCommand("SELECT conversion FROM pbu.pbu_unit_of_measure WHERE code = '$sat' LIMIT 1;")->queryScalar();
        return (float)$qty * (float)$conversion;
    }
}
