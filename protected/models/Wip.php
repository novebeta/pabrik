<?php
Yii::import('application.models._base.BaseWip');
class Wip extends BaseWip
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'material_id';
    }
}