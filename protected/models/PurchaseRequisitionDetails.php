<?php
Yii::import('application.models._base.BasePurchaseRequisitionDetails');
class PurchaseRequisitionDetails extends BasePurchaseRequisitionDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_material_create_PO($pr_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                prd.material_id,
                prd.qty,
                t.qty_purchased,
                m.sat,
                m.dimension,
                m.kode_material,
                m.nama_material
            FROM pbu_purchase_requisition_details prd
                LEFT JOIN (
                    SELECT
                        pod.material_id,
                        SUM(pod.qty) qty_purchased
                    FROM pbu_purchase_order_details pod
                        LEFT JOIN pbu_purchase_order po ON po.po_id = pod.po_id
                    WHERE
                        po.pr_id = :pr_id
                    GROUP BY
                        pod.material_id
                ) t ON prd.material_id = t.material_id
                LEFT JOIN pbu_material m ON prd.material_id = m.material_id
            WHERE
                prd.pr_id = :pr_id
            ");
        return $comm->queryAll(true, array(
            ':pr_id' => $pr_id
        ));
    }
    public static function get_items_create_PO($pr_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                prd.material_id,
                (prd.qty-IFNULL(t.qty_purchased,0)) qty,
                prd.price,
                prd.note,
                prd.sat
            FROM pbu_purchase_requisition_details prd
                LEFT JOIN (
                    SELECT
                        pod.material_id,
                        SUM(pod.qty) qty_purchased
                    FROM pbu_purchase_order_details pod
                        LEFT JOIN pbu_purchase_order po ON po.po_id = pod.po_id
                    WHERE
                        po.pr_id = :pr_id
                    GROUP BY
                        pod.material_id
                ) t ON prd.material_id = t.material_id
            WHERE
                prd.pr_id = :pr_id
            ");
        return $comm->queryAll(true, array(
            ':pr_id' => $pr_id
        ));
    }
    public function beforeValidate()
    {
        //if ($this->pr_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->pr_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}