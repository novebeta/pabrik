<?php
Yii::import('application.models._base.BaseTransferMaterialDetail');
class TransferMaterialDetail extends BaseTransferMaterialDetail
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function getitems($id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT
                bd.*,
                m.nama_material,
                m.kode_material,
                m.tipe_material_id
            FROM pbu_transfer_material_detail bd
                LEFT JOIN pbu_material m ON bd.material_id = m.material_id
            WHERE bd.transfer_material_id = '$id'");
        return $comm->queryAll(true);
    }
    public static function getitems2($id)
    {
        $comm = Yii::app()->db->createCommand(
            "SELECT *
            FROM pbu_transfer_material_detail
            WHERE transfer_material_id = '$id'");
        return $comm->queryAll(true);
    }
    public function beforeValidate()
    {
        //if ($this->transfer_material_detail_id == null) {
        $command = $this->dbConnection->createCommand("SELECT UUID();");
        $uuid = $command->queryScalar();
        $this->transfer_material_detail_id = $uuid;
        //}
        return parent::beforeValidate();
    }
}
