<?php

/**
 * This is the model base class for the table "{{grup}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Grup".
 *
 * Columns in table "{{grup}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $grup_id
 * @property string $nama_grup
 *
 */
abstract class BaseGrup extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{grup}}';
	}

	public static function representingColumn() {
		return 'nama_grup';
	}

	public function rules() {
		return array(
			array('grup_id, nama_grup', 'required'),
			array('grup_id', 'length', 'max'=>36),
			array('nama_grup', 'length', 'max'=>50),
			array('grup_id, nama_grup', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'grup_id' => Yii::t('app', 'Grup'),
			'nama_grup' => Yii::t('app', 'Nama Grup'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('grup_id', $this->grup_id, true);
		$criteria->compare('nama_grup', $this->nama_grup, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}