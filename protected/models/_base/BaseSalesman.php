<?php

/**
 * This is the model base class for the table "{{salesman}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Salesman".
 *
 * Columns in table "{{salesman}}" available as properties of the model,
 * followed by relations of table "{{salesman}}" available as properties of the model.
 *
 * @property string $salesman_id
 * @property string $salesman_code
 * @property string $salesman_name
 * @property integer $status
 * @property string $bagian
 * @property string $tempat_lahir
 * @property string $tgl_lahir
 * @property string $alamat
 * @property string $phone
 * @property string $tgl_gabung
 *
 * @property Sales[] $sales
 */
abstract class BaseSalesman extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{salesman}}';
	}

	public static function representingColumn() {
		return 'salesman_id';
	}

	public function rules() {
		return array(
			array('salesman_id', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('salesman_id', 'length', 'max'=>36),
			array('salesman_code, bagian, tempat_lahir, phone', 'length', 'max'=>20),
			array('salesman_name, alamat', 'length', 'max'=>100),
			array('tgl_lahir, tgl_gabung', 'safe'),
			array('salesman_code, salesman_name, status, bagian, tempat_lahir, tgl_lahir, alamat, phone, tgl_gabung', 'default', 'setOnEmpty' => true, 'value' => null),
			array('salesman_id, salesman_code, salesman_name, status, bagian, tempat_lahir, tgl_lahir, alamat, phone, tgl_gabung', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'sales' => array(self::HAS_MANY, 'Sales', 'salesman_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'salesman_id' => Yii::t('app', 'Salesman'),
			'salesman_code' => Yii::t('app', 'Salesman Code'),
			'salesman_name' => Yii::t('app', 'Salesman Name'),
			'status' => Yii::t('app', 'Status'),
			'bagian' => Yii::t('app', 'Bagian'),
			'tempat_lahir' => Yii::t('app', 'Tempat Lahir'),
			'tgl_lahir' => Yii::t('app', 'Tgl Lahir'),
			'alamat' => Yii::t('app', 'Alamat'),
			'phone' => Yii::t('app', 'Phone'),
			'tgl_gabung' => Yii::t('app', 'Tgl Gabung'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('salesman_id', $this->salesman_id, true);
		$criteria->compare('salesman_code', $this->salesman_code, true);
		$criteria->compare('salesman_name', $this->salesman_name, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('bagian', $this->bagian, true);
		$criteria->compare('tempat_lahir', $this->tempat_lahir, true);
		$criteria->compare('tgl_lahir', $this->tgl_lahir, true);
		$criteria->compare('alamat', $this->alamat, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('tgl_gabung', $this->tgl_gabung, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}