<?php
/**
 * This is the model base class for the table "{{tandaterima_pack_material}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "TandaterimaPackMaterial".
 *
 * Columns in table "{{tandaterima_pack_material}}" available as properties of the model,
 * followed by relations of table "{{tandaterima_pack_material}}" available as properties of the model.
 *
 * @property string $tandaterima_pack_id
 * @property string $doc_ref
 * @property double $total
 * @property string $tgl
 * @property string $tanda_terima
 * @property string $note
 *
 * @property TandaterimaPackMaterialDetails[] $tandaterimaPackMaterialDetails
 */
abstract class BaseTandaterimaPackMaterial extends GxActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function representingColumn()
    {
        return 'doc_ref';
    }
    public function tableName()
    {
        return '{{tandaterima_pack_material}}';
    }
    public function rules()
    {
        return array(
            array('tandaterima_pack_id, doc_ref, tgl', 'required'),
            array('total', 'numerical'),
            array('tandaterima_pack_id', 'length', 'max' => 36),
            array('doc_ref', 'length', 'max' => 50),
            array('tanda_terima, note', 'length', 'max' => 100),
            array('total, tanda_terima, note', 'default', 'setOnEmpty' => true, 'value' => null),
            array('tandaterima_pack_id, doc_ref, total, tgl, tanda_terima, note', 'safe', 'on' => 'search'),
        );
    }
    public function relations()
    {
        return array(
            'tandaterimaPackMaterialDetails' => array(self::HAS_MANY, 'TandaterimaPackMaterialDetails', 'tandaterima_pack_id'),
        );
    }
    public function pivotModels()
    {
        return array();
    }
    public function attributeLabels()
    {
        return array(
            'tandaterima_pack_id' => Yii::t('app', 'Tandaterima Pack'),
            'doc_ref' => Yii::t('app', 'Doc Ref'),
            'total' => Yii::t('app', 'Total'),
            'tgl' => Yii::t('app', 'Tgl'),
            'tanda_terima' => Yii::t('app', 'Tanda Terima'),
            'note' => Yii::t('app', 'Note'),
        );
    }
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('tandaterima_pack_id', $this->tandaterima_pack_id, true);
        $criteria->compare('doc_ref', $this->doc_ref, true);
        $criteria->compare('total', $this->total);
        $criteria->compare('tgl', $this->tgl, true);
        $criteria->compare('tanda_terima', $this->tanda_terima, true);
        $criteria->compare('note', $this->note, true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }
}