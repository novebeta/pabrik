<?php
/**
 * This is the model base class for the table "{{sjsim}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Sjsim".
 *
 * Columns in table "{{sjsim}}" available as properties of the model,
 * followed by relations of table "{{sjsim}}" available as properties of the model.
 *
 * @property string $sjsim_id
 * @property string $doc_ref
 * @property double $total
 * @property string $customer_id
 * @property string $tgl
 *
 * @property Customers $customer
 * @property SjsimDetails[] $sjsimDetails
 */
abstract class BaseSjsim extends GxActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function representingColumn()
    {
        return 'doc_ref';
    }
    public function tableName()
    {
        return '{{sjsim}}';
    }
    public function rules()
    {
        return array(
            array('sjsim_id, doc_ref, customer_id, tgl', 'required'),
            array('total', 'numerical'),
            array('sjsim_id, customer_id', 'length', 'max' => 36),
            array('doc_ref', 'length', 'max' => 50),
            array('total', 'default', 'setOnEmpty' => true, 'value' => null),
            array('sjsim_id, doc_ref, total, customer_id, tgl', 'safe', 'on' => 'search'),
        );
    }
    public function relations()
    {
        return array(
            'customer' => array(self::BELONGS_TO, 'Customers', 'customer_id'),
            'sjsimDetails' => array(self::HAS_MANY, 'SjsimDetails', 'sjsim_id'),
        );
    }
    public function pivotModels()
    {
        return array();
    }
    public function attributeLabels()
    {
        return array(
            'sjsim_id' => Yii::t('app', 'Sjsim'),
            'doc_ref' => Yii::t('app', 'Doc Ref'),
            'total' => Yii::t('app', 'Total'),
            'customer_id' => Yii::t('app', 'Customer'),
            'tgl' => Yii::t('app', 'Tgl'),
        );
    }
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('sjsim_id', $this->sjsim_id, true);
        $criteria->compare('doc_ref', $this->doc_ref, true);
        $criteria->compare('total', $this->total);
        $criteria->compare('customer_id', $this->customer_id);
        $criteria->compare('tgl', $this->tgl, true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }
}