<?php

/**
 * This is the model base class for the table "{{price_int}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PriceInt".
 *
 * Columns in table "{{price_int}}" available as properties of the model,
 * followed by relations of table "{{price_int}}" available as properties of the model.
 *
 * @property string $price_int_id
 * @property double $amount
 * @property string $wil_int_id
 * @property string $barang_id
 *
 * @property Material $barang
 */
abstract class BasePriceInt extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{price_int}}';
	}

	public static function representingColumn() {
		return 'wil_int_id';
	}

	public function rules() {
		return array(
			array('price_int_id, wil_int_id, barang_id', 'required'),
			array('amount', 'numerical'),
			array('price_int_id, wil_int_id, barang_id', 'length', 'max'=>36),
			array('amount', 'default', 'setOnEmpty' => true, 'value' => null),
			array('price_int_id, amount, wil_int_id, barang_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'barang' => array(self::BELONGS_TO, 'Material', 'barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'price_int_id' => Yii::t('app', 'Price Int'),
			'amount' => Yii::t('app', 'Amount'),
			'wil_int_id' => Yii::t('app', 'Wil Int'),
			'barang_id' => Yii::t('app', 'Barang'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('price_int_id', $this->price_int_id, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('wil_int_id', $this->wil_int_id, true);
		$criteria->compare('barang_id', $this->barang_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}