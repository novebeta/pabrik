<?php

/**
 * This is the model base class for the table "{{supplier_bank}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SupplierBank".
 *
 * Columns in table "{{supplier_bank}}" available as properties of the model,
 * followed by relations of table "{{supplier_bank}}" available as properties of the model.
 *
 * @property string $supplier_bank_id
 * @property string $nama_bank
 * @property string $no_rekening
 * @property string $nama_rekening
 * @property string $alamat_bank
 * @property string $currency
 * @property string $supplier_id
 *
 * @property Supplier $supplier
 */
abstract class BaseSupplierBank extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{supplier_bank}}';
	}

	public static function representingColumn() {
		return 'nama_bank';
	}

	public function rules() {
		return array(
			array('supplier_bank_id, nama_bank, no_rekening, supplier_id', 'required'),
			array('supplier_bank_id, supplier_id', 'length', 'max'=>36),
			array('nama_bank, no_rekening, nama_rekening, alamat_bank', 'length', 'max'=>50),
			array('currency', 'length', 'max'=>3),
			array('nama_rekening, alamat_bank, currency', 'default', 'setOnEmpty' => true, 'value' => null),
			array('supplier_bank_id, nama_bank, no_rekening, nama_rekening, alamat_bank, currency, supplier_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplier_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'supplier_bank_id' => Yii::t('app', 'Supplier Bank'),
			'nama_bank' => Yii::t('app', 'Nama Bank'),
			'no_rekening' => Yii::t('app', 'No Rekening'),
			'nama_rekening' => Yii::t('app', 'Nama Rekening'),
			'alamat_bank' => Yii::t('app', 'Alamat Bank'),
			'currency' => Yii::t('app', 'Currency'),
			'supplier_id' => Yii::t('app', 'Supplier'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('supplier_bank_id', $this->supplier_bank_id, true);
		$criteria->compare('nama_bank', $this->nama_bank, true);
		$criteria->compare('no_rekening', $this->no_rekening, true);
		$criteria->compare('nama_rekening', $this->nama_rekening, true);
		$criteria->compare('alamat_bank', $this->alamat_bank, true);
		$criteria->compare('currency', $this->currency, true);
		$criteria->compare('supplier_id', $this->supplier_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}