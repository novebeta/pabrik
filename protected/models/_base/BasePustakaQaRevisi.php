<?php
/**
 * This is the model base class for the table "{{pustaka_qa_revisi}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PustakaQaRevisi".
 *
 * Columns in table "{{pustaka_qa_revisi}}" available as properties of the model,
 * followed by relations of table "{{pustaka_qa_revisi}}" available as properties of the model.
 *
 * @property string $pustaka_qa_revisi_id
 * @property string $tgl_efektif
 * @property string $tgl_review
 * @property string $kode_revisi
 * @property string $file_name
 * @property string $pustaka_qa_id
 * @property string $tdate
 * @property integer $revisi_ke
 * @property integer $aktif
 * @property string $id_user
 *
 * @property PustakaQa $pustakaQa
 */
abstract class BasePustakaQaRevisi extends GxActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function representingColumn()
    {
        return 'pustaka_qa_revisi_id';
    }
    public function tableName()
    {
        return '{{pustaka_qa_revisi}}';
    }
    public function rules()
    {
        return array(
            array('pustaka_qa_revisi_id', 'required'),
            array('revisi_ke, aktif', 'numerical', 'integerOnly' => true),
            array('pustaka_qa_revisi_id, pustaka_qa_id, id_user', 'length', 'max' => 36),
            array('kode_revisi', 'length', 'max' => 45),
            array('file_name', 'length', 'max' => 100),
            array('tgl_efektif, tgl_review, tdate', 'safe'),
            array('tgl_efektif, tgl_review, kode_revisi, file_name, pustaka_qa_id, tdate, revisi_ke, aktif, id_user', 'default', 'setOnEmpty' => true, 'value' => null),
            array('pustaka_qa_revisi_id, tgl_efektif, tgl_review, kode_revisi, file_name, pustaka_qa_id, tdate, revisi_ke, aktif, id_user', 'safe', 'on' => 'search'),
        );
    }
    public function relations()
    {
        return array(
            'pustakaQa' => array(self::BELONGS_TO, 'PustakaQa', 'pustaka_qa_id'),
        );
    }
    public function pivotModels()
    {
        return array();
    }
    public function attributeLabels()
    {
        return array(
            'pustaka_qa_revisi_id' => Yii::t('app', 'Pustaka Qa Revisi'),
            'tgl_efektif' => Yii::t('app', 'Tgl Efektif'),
            'tgl_review' => Yii::t('app', 'Tgl Review'),
            'kode_revisi' => Yii::t('app', 'Kode Revisi'),
            'file_name' => Yii::t('app', 'File Name'),
            'pustaka_qa_id' => Yii::t('app', 'Pustaka Qa'),
            'tdate' => Yii::t('app', 'Tdate'),
            'revisi_ke' => Yii::t('app', 'Revisi Ke'),
            'aktif' => Yii::t('app', 'Aktif'),
            'id_user' => Yii::t('app', 'Id User'),
        );
    }
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('pustaka_qa_revisi_id', $this->pustaka_qa_revisi_id, true);
        $criteria->compare('tgl_efektif', $this->tgl_efektif, true);
        $criteria->compare('tgl_review', $this->tgl_review, true);
        $criteria->compare('kode_revisi', $this->kode_revisi, true);
        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('pustaka_qa_id', $this->pustaka_qa_id);
        $criteria->compare('tdate', $this->tdate, true);
        $criteria->compare('revisi_ke', $this->revisi_ke);
        $criteria->compare('aktif', $this->aktif);
        $criteria->compare('id_user', $this->id_user, true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }
}