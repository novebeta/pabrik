<?php

/**
 * This is the model base class for the table "{{fa_bom_hpp_bj}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "FaBomHppBj".
 *
 * Columns in table "{{fa_bom_hpp_bj}}" available as properties of the model,
 * followed by relations of table "{{fa_bom_hpp_bj}}" available as properties of the model.
 *
 * @property string $fa_bom_hpp_bj_id
 * @property string $bom_id
 * @property string $batch_id
 * @property string $barang_id
 * @property double $batch_size
 * @property double $total_bj
 * @property double $nilai_rm_per_batch
 * @property double $nilai_pm_per_pce
 * @property string $fin_acc_report_id
 *
 * @property Batch $batch
 * @property FinAccReport $finAccReport
 * @property Bom $bom
 */
abstract class BaseFaBomHppBj extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{fa_bom_hpp_bj}}';
	}

	public static function representingColumn() {
		return 'fa_bom_hpp_bj_id';
	}

	public function rules() {
		return array(
			array('fa_bom_hpp_bj_id', 'required'),
			array('batch_size, total_bj, nilai_rm_per_batch, nilai_pm_per_pce', 'numerical'),
			array('fa_bom_hpp_bj_id, bom_id, batch_id, barang_id, fin_acc_report_id', 'length', 'max'=>36),
			array('bom_id, batch_id, barang_id, batch_size, total_bj, nilai_rm_per_batch, nilai_pm_per_pce, fin_acc_report_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('fa_bom_hpp_bj_id, bom_id, batch_id, barang_id, batch_size, total_bj, nilai_rm_per_batch, nilai_pm_per_pce, fin_acc_report_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'batch' => array(self::BELONGS_TO, 'Batch', 'batch_id'),
			'finAccReport' => array(self::BELONGS_TO, 'FinAccReport', 'fin_acc_report_id'),
			'bom' => array(self::BELONGS_TO, 'Bom', 'bom_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'fa_bom_hpp_bj_id' => Yii::t('app', 'Fa Bom Hpp Bj'),
			'bom_id' => Yii::t('app', 'Bom'),
			'batch_id' => Yii::t('app', 'Batch'),
			'barang_id' => Yii::t('app', 'Barang'),
			'batch_size' => Yii::t('app', 'Batch Size'),
			'total_bj' => Yii::t('app', 'Total Bj'),
			'nilai_rm_per_batch' => Yii::t('app', 'Nilai Rm Per Batch'),
			'nilai_pm_per_pce' => Yii::t('app', 'Nilai Pm Per Pce'),
			'fin_acc_report_id' => Yii::t('app', 'Fin Acc Report'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('fa_bom_hpp_bj_id', $this->fa_bom_hpp_bj_id, true);
		$criteria->compare('bom_id', $this->bom_id);
		$criteria->compare('batch_id', $this->batch_id);
		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('batch_size', $this->batch_size);
		$criteria->compare('total_bj', $this->total_bj);
		$criteria->compare('nilai_rm_per_batch', $this->nilai_rm_per_batch);
		$criteria->compare('nilai_pm_per_pce', $this->nilai_pm_per_pce);
		$criteria->compare('fin_acc_report_id', $this->fin_acc_report_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}