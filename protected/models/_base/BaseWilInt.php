<?php

/**
 * This is the model base class for the table "{{wil_int}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "WilInt".
 *
 * Columns in table "{{wil_int}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $wil_int_id
 * @property string $nama_wilayah
 *
 */
abstract class BaseWilInt extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{wil_int}}';
	}

	public static function representingColumn() {
		return 'nama_wilayah';
	}

	public function rules() {
		return array(
			array('wil_int_id, nama_wilayah', 'required'),
			array('wil_int_id', 'length', 'max'=>36),
			array('nama_wilayah', 'length', 'max'=>50),
			array('wil_int_id, nama_wilayah', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'wil_int_id' => Yii::t('app', 'Wil Int'),
			'nama_wilayah' => Yii::t('app', 'Nama Wilayah'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('wil_int_id', $this->wil_int_id, true);
		$criteria->compare('nama_wilayah', $this->nama_wilayah, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}