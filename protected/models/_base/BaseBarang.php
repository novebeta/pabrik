<?php

/**
 * This is the model base class for the table "{{barang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Barang".
 *
 * Columns in table "{{barang}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $barang_id
 * @property string $kode_barang
 * @property string $nama_barang
 * @property string $ket
 * @property string $sat
 * @property string $grup_id
 * @property double $qty_per_box
 * @property double $qty_per_pot
 * @property string $kode_barang_2
 * @property string $kode_barang_rnd
 * @property string $tipe_barang_id
 *
 */
abstract class BaseBarang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{barang}}';
	}

	public static function representingColumn() {
		return 'barang_id';
	}

	public function rules() {
		return array(
			array('barang_id, kode_barang, nama_barang', 'required'),
			array('qty_per_box, qty_per_pot', 'numerical'),
			array('barang_id, grup_id, tipe_barang_id', 'length', 'max'=>36),
			array('kode_barang, kode_barang_2, kode_barang_rnd', 'length', 'max'=>50),
			array('nama_barang', 'length', 'max'=>100),
			array('ket', 'length', 'max'=>255),
			array('sat', 'length', 'max'=>10),
			array('ket, sat, grup_id, qty_per_box, qty_per_pot, kode_barang_2, kode_barang_rnd, tipe_barang_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('barang_id, kode_barang, nama_barang, ket, sat, grup_id, qty_per_box, qty_per_pot, kode_barang_2, kode_barang_rnd, tipe_barang_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'barang_id' => Yii::t('app', 'Barang'),
			'kode_barang' => Yii::t('app', 'Kode Barang'),
			'nama_barang' => Yii::t('app', 'Nama Barang'),
			'ket' => Yii::t('app', 'Ket'),
			'sat' => Yii::t('app', 'Sat'),
			'grup_id' => Yii::t('app', 'Grup'),
			'qty_per_box' => Yii::t('app', 'Qty Per Box'),
			'qty_per_pot' => Yii::t('app', 'Qty Per Pot'),
			'kode_barang_2' => Yii::t('app', 'Kode Barang 2'),
			'kode_barang_rnd' => Yii::t('app', 'Kode Barang Rnd'),
			'tipe_barang_id' => Yii::t('app', 'Tipe Barang'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('kode_barang', $this->kode_barang, true);
		$criteria->compare('nama_barang', $this->nama_barang, true);
		$criteria->compare('ket', $this->ket, true);
		$criteria->compare('sat', $this->sat, true);
		$criteria->compare('grup_id', $this->grup_id, true);
		$criteria->compare('qty_per_box', $this->qty_per_box);
		$criteria->compare('qty_per_pot', $this->qty_per_pot);
		$criteria->compare('kode_barang_2', $this->kode_barang_2, true);
		$criteria->compare('kode_barang_rnd', $this->kode_barang_rnd, true);
		$criteria->compare('tipe_barang_id', $this->tipe_barang_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}