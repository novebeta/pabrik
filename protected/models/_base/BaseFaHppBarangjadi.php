<?php

/**
 * This is the model base class for the table "{{fa_hpp_barangjadi}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "FaHppBarangjadi".
 *
 * Columns in table "{{fa_hpp_barangjadi}}" available as properties of the model,
 * followed by relations of table "{{fa_hpp_barangjadi}}" available as properties of the model.
 *
 * @property string $fa_hpp_barangjadi_id
 * @property string $barang_id
 * @property string $bom_id
 * @property double $batch_size
 * @property double $qty
 * @property integer $x_produksi
 * @property double $nilai_rm_per_batch
 * @property double $nilai_rm_total
 * @property double $nilai_rm_per_pce
 * @property double $nilai_pm_per_pce
 * @property double $bop
 * @property double $hpp_per_pce
 * @property double $price
 * @property double $persen_profit
 * @property string $fin_acc_report_id
 *
 * @property FinAccReport $finAccReport
 * @property Bom $bom
 */
abstract class BaseFaHppBarangjadi extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{fa_hpp_barangjadi}}';
	}

	public static function representingColumn() {
		return 'barang_id';
	}

	public function rules() {
		return array(
			array('fa_hpp_barangjadi_id, barang_id, bom_id, fin_acc_report_id', 'required'),
			array('x_produksi', 'numerical', 'integerOnly'=>true),
			array('batch_size, qty, nilai_rm_per_batch, nilai_rm_total, nilai_rm_per_pce, nilai_pm_per_pce, bop, hpp_per_pce, price, persen_profit', 'numerical'),
			array('fa_hpp_barangjadi_id, barang_id, bom_id, fin_acc_report_id', 'length', 'max'=>36),
			array('batch_size, qty, x_produksi, nilai_rm_per_batch, nilai_rm_total, nilai_rm_per_pce, nilai_pm_per_pce, bop, hpp_per_pce, price, persen_profit', 'default', 'setOnEmpty' => true, 'value' => null),
			array('fa_hpp_barangjadi_id, barang_id, bom_id, batch_size, qty, x_produksi, nilai_rm_per_batch, nilai_rm_total, nilai_rm_per_pce, nilai_pm_per_pce, bop, hpp_per_pce, price, persen_profit, fin_acc_report_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'finAccReport' => array(self::BELONGS_TO, 'FinAccReport', 'fin_acc_report_id'),
			'bom' => array(self::BELONGS_TO, 'Bom', 'bom_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'fa_hpp_barangjadi_id' => Yii::t('app', 'Fa Hpp Barangjadi'),
			'barang_id' => Yii::t('app', 'Barang'),
			'bom_id' => Yii::t('app', 'Bom'),
			'batch_size' => Yii::t('app', 'Batch Size'),
			'qty' => Yii::t('app', 'Qty'),
			'x_produksi' => Yii::t('app', 'X Produksi'),
			'nilai_rm_per_batch' => Yii::t('app', 'Nilai Rm Per Batch'),
			'nilai_rm_total' => Yii::t('app', 'Nilai Rm Total'),
			'nilai_rm_per_pce' => Yii::t('app', 'Nilai Rm Per Pce'),
			'nilai_pm_per_pce' => Yii::t('app', 'Nilai Pm Per Pce'),
			'bop' => Yii::t('app', 'Bop'),
			'hpp_per_pce' => Yii::t('app', 'Hpp Per Pce'),
			'price' => Yii::t('app', 'Price'),
			'persen_profit' => Yii::t('app', 'Persen Profit'),
			'fin_acc_report_id' => Yii::t('app', 'Fin Acc Report'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('fa_hpp_barangjadi_id', $this->fa_hpp_barangjadi_id, true);
		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('bom_id', $this->bom_id);
		$criteria->compare('batch_size', $this->batch_size);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('x_produksi', $this->x_produksi);
		$criteria->compare('nilai_rm_per_batch', $this->nilai_rm_per_batch);
		$criteria->compare('nilai_rm_total', $this->nilai_rm_total);
		$criteria->compare('nilai_rm_per_pce', $this->nilai_rm_per_pce);
		$criteria->compare('nilai_pm_per_pce', $this->nilai_pm_per_pce);
		$criteria->compare('bop', $this->bop);
		$criteria->compare('hpp_per_pce', $this->hpp_per_pce);
		$criteria->compare('price', $this->price);
		$criteria->compare('persen_profit', $this->persen_profit);
		$criteria->compare('fin_acc_report_id', $this->fin_acc_report_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}