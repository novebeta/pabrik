<?php

/**
 * This is the model base class for the table "{{bom}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Bom".
 *
 * Columns in table "{{bom}}" available as properties of the model,
 * followed by relations of table "{{bom}}" available as properties of the model.
 *
 * @property string $bom_id
 * @property string $kode_formula
 * @property string $no_bom
 * @property string $tgl
 * @property string $bentuk_sediaan
 * @property double $qty
 * @property string $tdate
 * @property string $id_user
 * @property integer $enable
 * @property string $material_id
 *
 * @property Batch[] $batches
 * @property Material $material
 * @property BomDetail[] $bomDetails
 * @property FaBomHppBj[] $faBomHppBjs
 * @property FaHppBarangjadi[] $faHppBarangjadis
 * @property FaStockBdp[] $faStockBdps
 */
abstract class BaseBom extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{bom}}';
	}

	public static function representingColumn() {
		return 'kode_formula';
	}

	public function rules() {
		return array(
			array('bom_id, kode_formula, tgl, material_id', 'required'),
			array('enable', 'numerical', 'integerOnly'=>true),
			array('qty', 'numerical'),
			array('bom_id, kode_formula, id_user, material_id', 'length', 'max'=>36),
			array('no_bom', 'length', 'max'=>45),
			array('bentuk_sediaan', 'length', 'max'=>50),
			array('tdate', 'safe'),
			array('no_bom, bentuk_sediaan, qty, tdate, id_user, enable', 'default', 'setOnEmpty' => true, 'value' => null),
			array('bom_id, kode_formula, no_bom, tgl, bentuk_sediaan, qty, tdate, id_user, enable, material_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'batches' => array(self::HAS_MANY, 'Batch', 'bom_id'),
			'material' => array(self::BELONGS_TO, 'Material', 'material_id'),
			'bomDetails' => array(self::HAS_MANY, 'BomDetail', 'bom_id'),
			'faBomHppBjs' => array(self::HAS_MANY, 'FaBomHppBj', 'bom_id'),
			'faHppBarangjadis' => array(self::HAS_MANY, 'FaHppBarangjadi', 'bom_id'),
			'faStockBdps' => array(self::HAS_MANY, 'FaStockBdp', 'bom_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'bom_id' => Yii::t('app', 'Bom'),
			'kode_formula' => Yii::t('app', 'Kode Formula'),
			'no_bom' => Yii::t('app', 'No Bom'),
			'tgl' => Yii::t('app', 'Tgl'),
			'bentuk_sediaan' => Yii::t('app', 'Bentuk Sediaan'),
			'qty' => Yii::t('app', 'Qty'),
			'tdate' => Yii::t('app', 'Tdate'),
			'id_user' => Yii::t('app', 'Id User'),
			'enable' => Yii::t('app', 'Enable'),
			'material_id' => Yii::t('app', 'Material'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('bom_id', $this->bom_id, true);
		$criteria->compare('kode_formula', $this->kode_formula, true);
		$criteria->compare('no_bom', $this->no_bom, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('bentuk_sediaan', $this->bentuk_sediaan, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('id_user', $this->id_user, true);
		$criteria->compare('enable', $this->enable);
		$criteria->compare('material_id', $this->material_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}