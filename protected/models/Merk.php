<?php
Yii::import('application.models._base.BaseMerk');
class Merk extends BaseMerk
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->merk_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->merk_id = $uuid;
        }
        return parent::beforeValidate();
    }
}