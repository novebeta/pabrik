<?php
Yii::import('application.models._base.BaseCustomerPaymentDetil');
class CustomerPaymentDetil extends BaseCustomerPaymentDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->customer_payment_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->customer_payment_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}