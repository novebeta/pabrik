<?php
Yii::import('application.models._base.BaseApar');
class Apar extends BaseApar
{
    public static function addPiutang($type_no, $trans_no, $tran_date, $customer_id, $doc_ref, $amount, $for_trans_no)
    {
        $piu = new Apar;
        $piu->type_no = $type_no;
        $piu->trans_no = $trans_no;
        $piu->arus = 1;
        $piu->tran_date = $tran_date;
        $piu->subject_id = $customer_id;
        $piu->reference = $doc_ref;
        $piu->amount = $amount;
        $piu->for_trans_no = $for_trans_no;
        if (!$piu->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Apar')) .
                CHtml::errorSummary($piu));
        }
    }
    public static function updatePiutang($type_no, $trans_no, $tran_date, $customer_id, $doc_ref, $amount, $for_trans_no)
    {
        $piu = Apar::model()->find("type_no = :type_no AND trans_no = :trans_no",
            array(":type_no" => $type_no, ":trans_no" => $trans_no));
        //$piu->type_no = $type_no;
        //$piu->trans_no = $trans_no;
        $piu->arus = 1;
        $piu->tran_date = $tran_date;
        $piu->subject_id = $customer_id;
        $piu->reference = $doc_ref;
        $piu->amount = $amount;
        $piu->for_trans_no = $for_trans_no;
        if (!$piu->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Apar')) .
                CHtml::errorSummary($piu));
        }
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function addHutang($type_no, $trans_no, $tran_date, $supplier_id, $doc_ref, $amount, $for_trans_no)
    {
        $hut = new Apar;
        $hut->type_no = $type_no;
        $hut->trans_no = $trans_no;
        $hut->arus = -1;
        $hut->tran_date = $tran_date;
        $hut->subject_id = $supplier_id;
        $hut->reference = $doc_ref;
        $hut->amount = $amount;
        $hut->for_trans_no = $for_trans_no;
        if (!$hut->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Apar')) .
                CHtml::errorSummary($hut));
        }
    }
    public function beforeValidate()
    {
        if ($this->apar_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->apar_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
}