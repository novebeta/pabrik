<?php
define('COA_VOUCHER', '431050');
define('COA_GRUP_BIAYA', 'fb2ebfbb-c0e5-11e5-9a87-00ff107009ba'); //define('COA_GRUP_BIAYA', '611000');
define('COA_GRUP_HUTANG', '244000d2-f351-11e6-a96a-201a069ec688'); //define('COA_GRUP_HUTANG', '211000');
define('COA_GRUP_PENDAPATAN', '70c143db-bf1d-11e5-b82e-00ff107009ba'); //define('COA_GRUP_PENDAPATAN', '700000');
define('COA_GRUP_BANK', '79667e1d-9b2c-11e5-a972-00fff414d6a3'); //define('COA_GRUP_BANK', '112000');
define('COA_GRUP_PIUTANG', '79668114-9b2c-11e5-a972-00fff414d6a3'); //define('COA_GRUP_BANK', '112000');
define('COA_PERSEDIAAN', '114101');
define('COA_VAT', '212020');
define('COA_GRUP_KAS', '79667842-9b2c-11e5-a972-00fff414d6a3'); //define('COA_GRUP_KAS', '111000');
define('COA_BIAYA_ADM_BANK', '750000');
define('COA_VAT_BELI', '115001');
define('COA_SALES_RETURN', '421010');
define('COA_SALES_GRUP', '400000');
define('COA_SALES', '400000');
define('COA_PURCHASE_GRUP', '511000');
define('COA_HPP_GRUP', '521000');
define('COA_TRANDE_RECEIVABLES', '113000');
define('COA_FEE_CARD', '611240');
define('COA_LABA_RUGI', '330000');
define('COA_PENDAPATAN', '4100');
define('COA_HPP', '500000');
define('COA_PIUTANG', '113001');
define('COA_HUTANG', '2210');
define('COA_PURCHASE_RETURN', '5200');
define('COA_SELISIH_PERSEDIAAN', '114101');
define('COA_UANGMUKA', '213000');
define('COA_DISKJUAL', '611900');
define('COA_PENDAPATAANLAIN', '730000');
define('COA_PIUTANG_GIRO', '113005');
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
define('PENJUALAN', 1);
define('CASHOUT', 2);
define('CASHIN', 3);
define('RETURJUAL', 4);
define('SUPPIN', 5); //#MVT [+] transfer masuk Material dan barang
define('SUPP_INV', 18);
define('SUPPOUT', 6); //#MVT [-] transfer keluar Material dan barang
define('TENDER', 7);
define('AUDIT', 8);
define('SURAT_JALAN', 9); //#MVT [-] kirim barang ke customer
define('SJ_EXPEDISI', 91);
define('SJ_BARANGRONGSOKAN', 92);
define('CNOTE', 10);
define('SJTAX', 11);
define('RETURBELI', 12); //#MVT [-] Pengembalian barang ke produksi
define('JURNAL_UMUM', 13);
define('BANKTRANSFER', 14);
define('BANKTRANSFERCHARGE', 15);
define('SAMPEL_QC', 16); //#MVT [-] Pengambilan sample oleh QC
define('STANDAR_KONSUMSI', 17); //#MVT [-] standard konsumsi material berdasarkan BOM
define('ADDITIONAL_KONSUMSI', 18); //#MVT [-] penambahan konsumsi material
define('REDUCTION_KONSUMSI', 19); //#MVT [+] pengurangan konsumsi material
define('HASIL_PRODUKSI', 20); //#MVT [+] stok barang nambah dari hasil produksi
define('LABARUGI', 21);
define('PR_RAW_MATERIAL', 22);
define('PR_PACKAGING', 23);
define('PR_GENERAL', 24);
define('PO_RAW_MATERIAL', 25);
define('PO_PACKAGING', 26);
define('PO_GENERAL', 27);
define('SJ_SUPPLIER', 28); //#MVT [+] penerimaan material dari vendor
define('RETURN_PEMBELIAN', 29); //#MVT [-] retur pembelian material dari supplier
define('NOTA_RETURN', 30);
define('TANDA_TERIMA', 31); //#MVT [+] penerimaan barang pengganti return dari supplier
define('FPT', 32);
define('FPT_TUNAI', 33);
define('TRANSFER_MATERIAL', 34); //Doc Ref Transfer material
define('TRANSFER_BARANG', 35); //Doc Ref Transfer BARANG
define('STOCK_ADJUST_INC', 36); //#MVT [+] stock adjustment menambah stok
define('STOCK_ADJUST_DEC', 37); //#MVT [-] stock adjustment mengurangi stok
define('PERBAIKAN_MESIN', 38);
define('TANDATERIMA_SPAREPART_BEKAS', 39);
define('KONVERSI_SPAREPART_BARKAS', 40);
define('BREAKDOWN', 41);
define('BREAKDOWN_VOID', 53);
define('ASSEMBLY', 42);
define('ASSEMBLY_VOID', 52);
define('KONVERSI_MATERIAL', 43);
define('SUPP_RETURN', 44);
define('CUST_RETURN', 45);
define('CUST_PAYMENT', 46);
define('SUPP_PAYMENT', 47);
define('CUST_INV', 48);
define('SERVICE_IN', 49);
define('SERVICE_OUT', 50);
define('KELOLASTOK', 51);
define('GIRO_CAIR', 52);
/*
 * TIPE BARANG/PRODUK
 * merujuk tabel 'pbu_tipe_barang'
 * jenis barang/produk yang dapat diinput ke Surat Jalan
 * 
 * BRG_PRODUK_LOKAL = produk untuk lokal
 * BRG_PRODUK_EXPORT = produk untuk export
 * BRG_EXPEDISI = biaya expedisi (dibebankan ke customer) disertakan ke surat jalan
 * BRG_BARANGRONGSOKAN = barang bekas
 */
define('BRG_PRODUK_LOKAL', 10);
define('BRG_PRODUK_EXPORT', 11);
define('BRG_EXPEDISI', 20);
define('BRG_BARANGRONGSOKAN', 30);
/*
 * MAXIMUM JUMLAH ITEM
 * pembatasan maksimum jumlah item/barang/produk yang dapat diinput dalam detail
 * tujuan : agar muat 1 lembar ketika dicetak
 */
define('MAX_ITEM_SJ_PRODUK_LOKAL', 50);
define('MAX_ITEM_SJ_PRODUK_EXPORT', 50);
define('MAX_ITEM_SJ_EXPEDISI', 30);
define('MAX_ITEM_SJ_BARANGRONGSOKAN', 30);
define('MAX_ITEM_PO', 11);
/*
 * staus umum
 */
define('OPEN', 0);
define('CLOSE', 1);
/*
 * TIPE MATERIAL
 * merujuk tabel 'pbu_tipe_material'
 * jenis-jenis barang yang dapat diinput ke dokumen PR/PO
 */
define('MAT_RAW_MATERIAL', 10);
define('MAT_PACKAGING', 20);
define('MAT_GENERAL', 30);
define('MAT_SPAREPART', 40);
define('MAT_SPAREPART_BEKAS', 41);
/* Jenis Tipe Material */
define('BAHAN_BAKU', 1);
define('WIP', 2);
define('BARANG_JADI', 3);
define('RONGSOKAN', 4);
//STATUS PURCHASE REQUISITION
define('PR_OPEN', 0);       //PR baru dibuat
define('PR_PROCESS', 1);   //PR belum semua dibuat PO
define('PR_REJECTED', 2);   //ditolak
define('PR_CLOSED', 4);     //ditutup (jadi PO)
//STATUS PURCHASE ORDER
define('P0_OPEN', 0);       //baru dibuat, data PO dapat diubah dan tidak bisa dicetak
define('P0_RELEASED', 1);   //PO tdk dapat diubah dan bisa dicetak
define('PO_PARTIALLY_RECEIVED', 2);   //barang diterima (parsial)
define('PO_RECEIVED', 3);   //semua barang telah diterima
define('PO_INVOICED', 4);   //muncul invoice
define('PO_CANCELED', 5);   //PO dibatalkan
define('PO_CLOSED', 6);     //PO ditutup (semua barang telah diterima dan dibayar)
//STATUS SURAT JALAN SUPPLIER
define('SJ_SUPPLIER_OPEN', 0);      //sj baru diinput, data sj status open (masih bisa diubah)
define('SJ_SUPPLIER_INVOICED', 1);  //sj sudah ada invoice, data sj tdk dapat diubah, data invoice status open (masih bisa diubah)
define('SJ_SUPPLIER_FPT', 2);       //FPT untuk invoice telah dibuat, data invoice tdk dapat diubah, data FPT dapat diubah
define('SJ_SUPPLIER_PAID', 3);      //invoice telah dibayar, data FTP tdk dapat diubah
//TIPE INVOICE
define('TIPE_INV_DEFAULT', 0);      //invoice dari SJ/penerimaan barang
define('TIPE_INV_DOWNPAYMENT', 1);  //invoice untuk DP, belum ada penerimaan barang
define('TIPE_INV_TANPA_SJ', 2);     //invoice tanpa penerimaan barang
//STATUS FPT (Form Pengajuan Transfer) untuk pembayaran ke Supplier
define('FPT_OPEN', 0);      //sj baru diinput, data sj status open (masih bisa diubah)
define('FPT_PAID', 1);  //sj sudah ada invoice, data sj tdk dapat diubah, data invoice status open (masih bisa diubah)
//STATUS RETURN PEMBELIAN
define('RETURN_PEMBELIAN_OPEN', 0);         //return baru diinput, status open (masih bisa diubah)
define('RETURN_PEMBELIAN_DELIVERED', 1);     //tanda barang telah dikirim, data tdk dapat diubah dan bisa dicetak
define('RETURN_PEMBELIAN_RECEIVED', 2);     //barang pengganti telah diterima
define('RETURN_PEMBELIAN_NOTA_RETURN', 3);  //telah diterbitkan NOTA RETURN, tidak ada brg pengganti
//STATUS NOTA RETURN
define('NOTA_RETURN_OPEN', 0);
define('NOTA_RETURN_USED', 1);      //nota return telah digunakan untuk pembayaran (FPT)
define('NOTA_RETURN_CANCELED', 2);
//STATUS SURAT JALAN CUSTOMER
define('SJ_OPEN', 0);
define('SJ_PARTIAL', 1);
define('SJ_DELIVERED', 2);
define('SJ_INVOICED', 3);
define('SJ_PAID', 4);
/*
 * STATUS BACTK PRODUKSI (PRODUCTION ORDER)
 */
define('BATCH_OPEN', 0);                //masih dapat diubah
define('BATCH_PREPARED', 1);            //sudah direlease bisa diproduksi
define('BATCH_MIXED', 2);  //barang jadi sebagian
define('BATCH_FINISHED', 3);           //jumlah barang jadi terpenuhi
/*
 * USER ROLE
 * merujuk table 'pbu_security_roles'
 * tujuan : untuk pembatasan akses saat transaksi
 * contoh : pada transaksi Purchase Requisition
 *      USER_PPIC hanya boleh membuat PR untuk bahan baku dan bahan kemas
 *      USER_GA hanya boleh membuat PR untuk barang umum/general
 */
define('USER_ADMIN', 2);
define('USER_GPJ', 3);
define('USER_PPIC', 4);
define('USER_GA', 5);
define('USER_PURCHASING', 6);
define('USER_PEMBELIAN', 7);
define('USER_RND', 8);
define('USER_PRODUKSI', 9);
define('USER_QA', 10);
define('USER_UMUM', 11);
define('USER_FINANCE', 12);
define('USER_QC', 100);
define('USER_EKSTERNAL', 101);
define('BANK_ALLOW_MINUS', '422d36eb-027c-11e7-95f5-201a069ec688,8c10eac8-15c0-11e7-b79a-00ff4b145794');
/*
 * STORAGE LOCATION
 * untuk partisi pergerakan barang (stock movement)
 * 'havestock' => jika 0 maka auto accept saat transfer stock (material/barang)
 */
global $storLoc;
$storLoc = array(
    USER_PPIC => array("name" => "PPIC", "havestock" => 1),
    USER_GA => array("name" => "GA", "havestock" => 1),
    USER_PRODUKSI => array("name" => "PROD", "havestock" => 1),
    USER_GPJ => array("name" => "GPJ", "havestock" => 1),
    //USER_ADMIN => "GPJ",
    USER_RND => array("name" => "RnD", "havestock" => 0),
    USER_QC => array("name" => "QC", "havestock" => 0),
    USER_EKSTERNAL => array("name" => "EKSTERNAL", "havestock" => 0)
);
/*
 * Pembagian tipe material yang ditangani
 */
global $tipeMaterialRestiction;
$tipeMaterialRestiction = array(
    USER_PPIC => array(MAT_RAW_MATERIAL, MAT_PACKAGING),
    USER_GA => array(MAT_GENERAL, MAT_SPAREPART, MAT_SPAREPART_BEKAS)
);
//FOLDER SIMPAN FILE DOKUMEN QA
define('DOC_QA_FOLDER', './doc/');
//BACKUP DATABASE
define('MYSQL', 'D:\xampp\mysql\bin\mysql');
define('MYSQLDUMP', 'D:\xampp\mysql\bin\mysqldump');
define('GZIP', 'D:\xampp\gzip');
global $systypes_array;
$systypes_array = array(
    PENJUALAN => "Sales",
    CASHOUT => "Cash Out",
    CASHIN => "Cash In",
    RETURJUAL => "Return Sales",
    SUPPIN => "Receive Supplier Item",
    SUPPOUT => "Return Supplier Item",
    TENDER => "Tender Declaration",
    AUDIT => "Audit",
    CNOTE => "Credit Note",
    RETURBELI => "Retur Beli"
);
//	GL account classes
//
define('CL_CURRENT_ASSETS', 1);
define('CL_CURRENT_LIABILITIES', 2);
define('CL_EQUITY', 3);
define('CL_INCOME', 4);
define('CL_COGS', 5);
define('CL_EXPENSE', 6);
define('CL_OTHER_INCOME', 7);
define('CL_FIXED_ASSETS', 8);
define('CL_LONGTERM_LIABILITIES', 9);
$class_types = array(
    CL_CURRENT_ASSETS => "Current Assets",
    CL_FIXED_ASSETS => "Fixed Assets",
    CL_CURRENT_LIABILITIES => "Current Liabilities",
    CL_LONGTERM_LIABILITIES => "Longterm Liabilities",
    CL_EQUITY => "Equity",
    CL_INCOME => "Income",
    CL_COGS => "Cost of Goods Sold",
    CL_EXPENSE => "Cost",
    CL_OTHER_INCOME => "Other Income and Expenses",
);
/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}

/**
 * @param $number
 * @return int
 */
function is_angka($number)
{
    if (strlen($number) == 0) {
        return false;
    }
    switch (gettype($number)) {
        case "NULL":
            return false;
        case "resource":
            return false;
        case "object":
            return false;
        case "array":
            return false;
        case "unknown type":
            return false;
    }
    return preg_match("/^-?([\$]?)([0-9,\s]*\.?[0-9]*)$/", $number);
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
    return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

function dbTrans()
{
    return Yii::app()->db->beginTransaction();
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null)
{
    static $baseUrl;
    if ($baseUrl === null) $baseUrl = Yii::app()->getRequest()->getBaseUrl();
    return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name)
{
    return Yii::app()->params[$name];
}

function Encrypt($string)
{
    return base64_encode(Yii::app()->getSecurityManager()->encrypt($string));
}

function Decrypt($string)
{
    return Yii::app()->getSecurityManager()->decrypt(base64_decode($string));
}

function generatePassword($length = 8)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

function date2longperiode($date, $format)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->format($format, $timestamp);
}

function period2date($month, $year)
{
    $timestamp = DateTime::createFromFormat('d/m/Y', "01/$month/$year");
    $start = $timestamp->format('Y-m-d');
    $end = $timestamp->format('Y-m-t');
    return array('start' => $start, 'end' => $end);
}

function get_number($number)
{
    return str_replace(",", "", $number);
}

function sql2date($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    return Yii::app()->dateFormatter->format($format, $timestamp);
}

function date2sql($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, $format);
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', $timestamp);
}

function sql2long_date($date)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->formatDateTime($timestamp, 'long', false);
}

function get_date_tomorrow()
{
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', time() + (1 * 24 * 60 * 60));
}

function get_time_now()
{
    return Yii::app()->dateFormatter->format('HH:mm:ss', time());
}

function get_date_today($format = 'yyyy-MM-dd')
{
    return Yii::app()->dateFormatter->format($format, time());
}

function Now($formatDate = 'yyyy-MM-dd')
{
    return get_date_today($formatDate) . ' ' . get_time_now();
}

function percent_format($value, $decimal = 0)
{
    return number_format($value * 100, $decimal) . '%';
}

function curr_format($value, $decimal = 0)
{
    return "Rp" . number_format($value * 100, $decimal);
}

function acc_format($value, $decimal = 0)
{
    $normalize = $value < 0 ? -$value : $value;
    $print = number_format($normalize, $decimal);
    return $value < 0 ? "($print)" : $print;
}

function round_up($number, $precision = 2)
{
    $number = round($number, $precision);
    $fig = (int)str_pad('1', $precision, '0');
    return (ceil($number * $fig) / $fig);
}

function round_down($number, $precision = 2)
{
    $number = round($number, $precision);
    $fig = (int)str_pad('1', $precision, '0');
    return (floor($number * $fig) / $fig);
}

function format_number_report($num, $digit = 0)
{
    if (!is_angka($num)) {
        return $num;
    }
    return (isset($_POST['format']) && $_POST['format'] == 'excel') ? $num : number_format($num, $digit);
}

function mysql2excel($myql_date)
{
    return strtotime($myql_date) + ((strtotime('1970-01-01') - strtotime('1900-01-01')) * 86400);
}

function spellNumber($n)
{
    $dasar = array(1 => 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan');
    $angka = array(1000000000, 1000000, 1000, 100, 10, 1);
    $satuan = array('milyar', 'juta', 'ribu', 'ratus', 'puluh', '');
    $i = 0;
    $str = '';
    while ($n != 0) {
        $count = (int)($n / $angka[$i]);
        if ($count >= 10) $str .= spellNumber($count) . " " . $satuan[$i] . " ";
        else if ($count > 0 && $count < 10)
            $str .= $dasar[$count] . " " . $satuan[$i] . " ";
        $n -= $angka[$i] * $count;
        $i++;
    }
    $str = preg_replace("/satu puluh (\w+)/i", "\\1 belas", $str);
    $str = preg_replace("/satu (ribu|ratus|puluh|belas)/i", "se\\1", $str);
    return ucwords(trim(str_replace("  ", " ", $str)));
}

function formatDatefromString($date_string, $format)
{
    $date = date_create($date_string);
    return date_format($date, $format);
}