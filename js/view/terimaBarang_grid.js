jun.TerimaBarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Surat Jalan Supplier",
    id: 'docs-jun.TerimaBarangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'NO. GRN',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tanda Terima',
            sortable: true,
            resizable: true,
            dataIndex: 'tanda_terima',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 100,
            renderer: function (a, b, c) {
                if (a == '1') {
                    return 'SUDAH INVOICE'
                } else {
                    return 'BELUM INVOICE';
                }
            }
        }
    ],
    initComponent: function () {
        jun.rztTerimaBarang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglterimabaranggridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTerimaBarang;
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                //{
                //    xtype: 'button',
                //    text: 'Buat Penerimaan Barang',
                //    ref: '../btnAdd'
                //},
                //{
                //    xtype: 'tbseparator'
                //},
                {
                    xtype: 'button',
                    text: 'Lihat Surat Jalan Supplier',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Surat Jalan Supplier',
                    ref: '../btnPrintSupplierInvoice'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Update Harga Goods Receival Notes',
                //     ref: '../btnEditHarga'
                // },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Supplier Invoice',
                    ref: '../btnAddSupplierInvoice'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglterimabaranggridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-PrintSuratJalanSupplier",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "terima_barang_id",
                            ref: "../../terima_barang_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.TerimaBarangGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnEditHarga.on('Click', this.updateRec, this);
        this.btnAddSupplierInvoice.on('Click', this.createSupplierInvoice, this);
        this.btnPrintSupplierInvoice.on('Click', this.printSupplierInvoice, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.selectTgl, this);
        //this.status.on('select', function () {
        //    this.store.reload();
        //}, this);
        // this.store.load();
        jun.rztTerimaBarang.removeAll();
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    printSupplierInvoice: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Goods Receival Notes");
            return;
        }
        Ext.getCmp("form-PrintSuratJalanSupplier").getForm().standardSubmit = !0;
        Ext.getCmp("form-PrintSuratJalanSupplier").getForm().url = "Report/PrintSuratJalanSupp";
        this.terima_barang_id.setValue(selectedz.json.terima_barang_id);
        var form = Ext.getCmp('form-PrintSuratJalanSupplier').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    createSupplierInvoice: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Goods Receival Notes");
            return;
        }
        if (selectedz.json.status != '0') {
            Ext.MessageBox.alert("Warning", "Supplier Invoice sudah dibuat.");
            return;
        }
        var idz = selectedz.json.terima_barang_id;
        var form = new jun.SupplierInvoiceWin({modez: 0});
        form.show(this);
        this.record.data.tgl_inv = DATE_NOW;
        this.record.data.status = 1;
        this.record.data.tgl_tempo = DATE_NOW.getDate() + DEFAULT_JATUH_TEMPO_PURCHASE;
        form.formz.getForm().loadRecord(this.record);
        jun.rztTerimaBarangDetails.baseParams = {
            terima_barang_id: idz
        };
        jun.rztTerimaBarangDetails.load();
        jun.rztTerimaBarangDetails.baseParams = {};
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TerimaBarangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Goods Receival Notes");
            return;
        }
        var idz = selectedz.json.terima_barang_id;
        var form = new jun.TerimaBarangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTerimaBarangDetails.baseParams = {
            terima_barang_id: idz
        };
        jun.rztTerimaBarangDetails.load();
        jun.rztTerimaBarangDetails.baseParams = {};
    },
    updateRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin update harga GRN ini?', this.updateRecYes, this);
    },
    updateRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih GRN");
            return;
        }
        Ext.Ajax.request({
            url: 'TerimaBarang/UpdateHarga/id/' + record.json.terima_barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTerimaBarang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.SupplierInvoiceGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Supplier Invoices",
    id: 'docs-jun.SupplierInvoiceGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_inv',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_inv',
            width: 100
        },
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'no_invoice',
            width: 100
        },
        {
            header: 'No. Faktur Pajak',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur_tax',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztSupplierInvoice.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl_inv = Ext.getCmp('tglsupplierinvoicegridid');
                    b.params.tgl_inv = tgl_inv.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztSupplierInvoice;
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                //{
                //    xtype: 'button',
                //    text: 'Buat Penerimaan Barang',
                //    ref: '../btnAdd'
                //},
                //{
                //    xtype: 'tbseparator'
                //},
                {
                    xtype: 'button',
                    text: 'Lihat Invoice',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Invoice',
                    ref: '../btnEdtSupplierInvoice'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Retur Pembelian',
                    ref: '../btnAddSupplierReturn'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Supplier Invoice',
                    ref: '../btnPrintSupplierInvoice'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglsupplierinvoicegridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-PrintInvSupplier",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "terima_barang_id",
                            ref: "../../terima_barang_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
                //,
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Create Supplier Credit Notes',
                //     ref: '../btnAddSupplierCreditNotes'
                // }
            ]
        };
        jun.SupplierInvoiceGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnAddSupplierCreditNotes.on('Click', this.createSuppCreditNote, this);
        this.btnPrintSupplierInvoice.on('Click', this.printSupplierInvoice, this);
        this.btnAddSupplierReturn.on('Click', this.createSuppReturn, this);
        this.btnEdtSupplierInvoice.on('Click', this.editSupplierInvoice, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.selectTgl, this);
        jun.rztSupplierInvoice.removeAll();
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    printSupplierInvoice: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Goods Receival Notes");
            return;
        }
        Ext.getCmp("form-PrintInvSupplier").getForm().standardSubmit = !0;
        Ext.getCmp("form-PrintInvSupplier").getForm().url = "Report/PrintInvSupp";
        this.terima_barang_id.setValue(selectedz.json.terima_barang_id);
        var form = Ext.getCmp('form-PrintInvSupplier').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    createSuppReturn: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Supplier Invoice");
            return;
        }
        var idz = selectedz.json.terima_barang_id;
        jun.rztReturnPembelianDetails.baseParams = {
            po_id: selectedz.json.po_id
        };
        jun.rztReturnPembelianDetails.load();
        jun.rztReturnPembelianDetails.baseParams = {};
        var form = new jun.ReturnPembelianWin({
            modez: 0,
            terima_barang_id: idz,
            title: "Buat Return Pembelian",
            storeDetail: jun.rztReturnPembelianDetails,
            iconCls: 'silk13-lorry'
        });
        form.show(this);
        this.record.data.no_faktur_tax = "";
        this.record.data.no_inv_supplier = this.record.data.no_invoice;
        this.record.data.no_sj_supplier = this.record.data.tanda_terima;
        this.record.data.doc_ref = "";
        this.record.data.arus = 1;
        this.record.data.parent_id = this.record.data.terima_barang_id;
        this.record.data.tgl = DATE_NOW;
        form.formz.getForm().loadRecord(this.record);
        // jun.rztTerimaBarangDetails.baseParams = {
        //     arus: 1,
        //     terima_barang_id: idz
        // };
        // jun.rztTerimaBarangDetails.load();
        // jun.rztTerimaBarangDetails.baseParams = {};
    },
    createSuppCreditNote: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Supplier Invoice");
            return;
        }
        var idz = selectedz.json.terima_barang_id;
        var form = new jun.SuppCreditNoteWin({modez: 0});
        form.show(this);
        this.record.data.no_faktur_tax = "";
        this.record.data.doc_ref = "";
        this.record.data.tgl = DATE_NOW;
        this.record.data.arus = 1;
        this.record.data.parent_id = this.record.data.terima_barang_id;
        form.formz.getForm().loadRecord(this.record);
        jun.rztSuppCreditNoteDetails.baseParams = {
            terima_barang_id: idz
        };
        jun.rztSuppCreditNoteDetails.load();
        jun.rztSuppCreditNoteDetails.baseParams = {};
    },
    editSupplierInvoice: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Goods Receival Notes");
            return;
        }
        var idz = selectedz.json.terima_barang_id;
        var form = new jun.SupplierInvoiceWin({modez: 3});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTerimaBarangDetails.baseParams = {
            terima_barang_id: idz
        };
        jun.rztTerimaBarangDetails.load();
        jun.rztTerimaBarangDetails.baseParams = {};
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SupplierInvoiceWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Supplier Invoice");
            return;
        }
        var idz = selectedz.json.terima_barang_id;
        var form = new jun.SupplierInvoiceWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTerimaBarangDetails.baseParams = {
            terima_barang_id: idz
        };
        jun.rztTerimaBarangDetails.load();
        jun.rztTerimaBarangDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'SupplierInvoice/delete/id/' + record.json.terima_barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSupplierInvoice.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
