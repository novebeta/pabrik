jun.PembayaranSupplierstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembayaranSupplierstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembayaranSupplierStoreId',
            url: 'PembayaranSupplier',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembayaran_supplier_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'nama_rekening'},
                {name: 'no_rekening'},
                {name: 'bank'},
                {name: 'supplier_id'},
                {name: 'supp_nama_rekening'},
                {name: 'supp_no_rekening'},
                {name: 'supp_bank'},
                {name: 'supp_alamat_bank'},
                {name: 'supp_rekening_currency'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'tujuan_pembayaran'},
                {name: 'cara_pembayaran'},
                {name: 'dpp', type: 'float'},
                {name: 'ppn', type: 'float'},
                {name: 'materai', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'pph', type: 'float'},
                {name: 'total_bayar', type: 'float'},
                {name: 'tdate'},
                {name: 'userid'},
                {name: 'tgl_bayar'},
                {name: 'tdate_bayar'},
                {name: 'userid_bayar'},
                {name: 'status'}
            ]
        }, cfg));
    }
});
jun.rztPembayaranSupplier = new jun.PembayaranSupplierstore();
jun.rztStatusPembayaranSupplier = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'displayText'
    ],
    data: [
        [FPT_OPEN, 'OPEN'],
        [FPT_PAID, 'PAID']
    ]
});
jun.rztPaymentMethodStore = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'des'
    ],
    data: [
        ['TRANSFER', ''],
        ['TUNAI', '']
    ]
});