jun.SparepartMasukStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SparepartMasukStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SparepartMasukStoreId',
            url: 'SparepartMasuk',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sparepart_masuk_id'},
                {name: 'doc_ref'},
                {name: 'tgl', type: 'date'},
                {name: 'nama'},
                {name: 'note'},
                {name: 'id_user'},
                {name: 'tdate'}
            ]
        }, cfg));
    }
});
jun.rztSparepartMasuk = new jun.SparepartMasukStore();
