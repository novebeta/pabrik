jun.PurchaseOrderWin = Ext.extend(Ext.Window, {
    title: 'Surat Jalan',
    modez: 1,
    width: 1000,
    height: 580,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-PurchaseOrder',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. PO",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "No. PR",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'pr_doc_ref',
                        id: 'form-PurchaseOrder_pr_doc_ref',
                        ref: '../pr_doc_ref',
                        width: 175,
                        // readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Attn Name",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'uctextfield',
                        hideLabel: false,
                        name: 'attn_name',
                        id: 'form-PurchaseOrder_attn_name',
                        ref: '../attn_name',
                        allowBlank: false,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tanggal PO",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        value: new Date(),
                        x: 85,
                        y: 92,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Supplier",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        id: 'form-PurchaseOrder_supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        ref: '../cmbSupplier',
                        x: 400,
                        y: 2,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "PIC Supplier",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        hideLabel: false,
                        name: 'supplier_cp',
                        id: 'form-PurchaseOrder_supplier_cp',
                        ref: '../supplier_cp',
                        allowBlank: false,
                        width: 175,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Term of Payment",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        name: 'term_of_payment',
                        ref: '../top',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 400,
                        y: 62
                    },
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     forceSelection: false,
                    //     store: jun.rztTermOfPaymentStore,
                    //     hiddenName: 'term_of_payment',
                    //     valueField: 'val',
                    //     displayField: 'val',
                    //     itemSelector: "div.search-item",
                    //     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                    //         '<span style="width:100%;display:inline-block;"><b>{val}</b><br>{des}</span>',
                    //         "</div></tpl>"),
                    //     value: '',
                    //     ref: '../top',
                    //     allowBlank: false,
                    //     width: 175,
                    //     x: 400,
                    //     y: 62
                    // },
                    {
                        xtype: "label",
                        text: "Tgl Delivery",
                        x: 295,
                        y: 95
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_delivery',
                        fieldLabel: 'tgl',
                        name: 'tgl_delivery',
                        id: 'form-PurchaseOrder_tgl_delivery',
                        format: 'd M Y',
                        x: 400,
                        y: 92,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Currency",
                        x: 595,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: false,
                        store: jun.rztCurrencyStore,
                        hiddenName: 'currency',
                        valueField: 'val',
                        displayField: 'val',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{val}</b><br>{des}</span>',
                            "</div></tpl>"),
                        value: 'IDR',
                        maxLength: 3,
                        ref: '../currency',
                        allowBlank: false,
                        width: 175,
                        x: 690,
                        y: 2
                    },
                    new jun.PurchaseOrderDetailsGrid({
                        x: 5,
                        y: 125,
                        height: 225,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        max_item: MAX_ITEM_PO,
                        store: this.storeDetail,
                        readOnly: ((this.modez < 2) ? false : true)
                    }),
                    {
                        xtype: "label",
                        text: "Comments/Instructions",
                        x: 5,
                        y: 125 + 225 + 10
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        name: 'note',
                        ref: '../note',
                        width: 350,
                        height: 110,
                        x: 5,
                        y: 125 + 225 + 30
                    },
                    {
                        xtype: "label",
                        text: "Total Excl. Tax",
                        x: 570,
                        y: 125 + 225 + 10
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        decimalPrecision: 4,
                        name: 'total',
                        ref: '../total',
                        id: 'form-PurchaseOrder-total',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 2
                    },
                    {
                        xtype: "label",
                        text: "Discount",
                        x: 570,
                        y: 125 + 225 + 10 + 30
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        decimalPrecision: 4,
                        enableKeyEvents: true,
                        name: 'discount',
                        ref: '../discount',
                        id: 'form-PurchaseOrder-discount',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 2 + 30
                    },
                    {
                        xtype: "label",
                        text: "Total Afte Disc.",
                        x: 570,
                        y: 125 + 225 + 10 + 60
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        decimalPrecision: 4,
                        name: 'dpp',
                        ref: '../dpp',
                        id: 'form-PurchaseOrder-dpp',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 2 + 60
                    },
                    {
                        xtype: "label",
                        text: "Tax",
                        x: 570,
                        y: 125 + 225 + 10 + 90
                    },
                    // {
                    //     xtype: 'numericfield',
                    //     id: 'form-PurchaseOrder-persen_tax',
                    //     name: 'persen_tax',
                    //     ref: '../persen_tax',
                    //     enableKeyEvents: true,
                    //     allowBlank: false,
                    //     width: 40,
                    //     value: 10,
                    //     x: 570 + 60,
                    //     y: 125 + 225 + 10 - 2 + 90
                    // },
                    // {
                    //     xtype: "label",
                    //     text: "%",
                    //     x: 570 + 60 + 40 + 5,
                    //     y: 125 + 225 + 10 + 90
                    // },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        decimalPrecision: 4,
                        name: 'tax',
                        ref: '../tax',
                        id: 'form-PurchaseOrder-tax',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 2 + 90
                    },
                    {
                        xtype: "label",
                        text: "Grand Total",
                        x: 570,
                        y: 125 + 225 + 10 + 120
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        decimalPrecision: 4,
                        name: 'grand_total',
                        ref: '../grand_total',
                        id: 'form-PurchaseOrder-grand_total',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 2 + 120
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PurchaseOrderWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.total.on('change', this.recalculate, this);
        // this.discount.on('keyup', this.recalculate, this);
        // this.persen_tax.on('keyup', this.recalculate, this);
        this.cmbSupplier.on('select', this.oncmbSupplierChange, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
            this.btnCancel.setText('Batal');
        } else if (this.modez == 0) { //create
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
            this.btnCancel.setText('Batal');
        } else { //after audit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnCancel.setText('Tutup');
            this.formz.getForm().applyToFields({readOnly: true});
        }
        jun.rztComboMaterialCreatePO.baseParams = {
            pr_id: this.pr_id
        };
        jun.rztComboMaterialCreatePO.reload();
        jun.rztComboMaterialCreatePO.baseParams = {};
    },
    onWinClose: function () {
        this.storeDetail.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        if (this.griddetils.store.find('price', '0') > -1) {
            Ext.MessageBox.alert("Warning", "Semua item harus diinput harganya.");
            return;
        }
        this.btnDisabled(true);
        var urlz = 'PurchaseOrder/create/';
        Ext.getCmp('form-PurchaseOrder').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                po_id: this.po_id,
                // pr_id: this.pr_id,
                status: P0_OPEN,
                // tipe_material_id: this.tipe_material_id,
                detil: Ext.encode(Ext.pluck(
                    this.storeDetail.data.items, "data"))
            },
            success: function (f, a) {
//                if (this.modez == 0) jun.rztPurchaseRequisition.reload();
//                if (this.modez == 1) jun.rztPurchaseOrder.reload();
                if (Ext.getCmp('docs-jun.PurchaseOrderGrid') !== undefined) Ext.getCmp('docs-jun.PurchaseOrderGrid').reloadStore();
                if (Ext.getCmp('docs-jun.PurchaseRequisitionGrid') !== undefined) Ext.getCmp('docs-jun.PurchaseRequisitionGrid').reloadStore();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PurchaseOrder').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    // recalculate: function () {
    //     if (!this.discount.getValue()) this.discount.setValue(0);
    //     if (!this.persen_tax.getValue()) this.persen_tax.setValue(0);
    //     var disc = parseFloat(this.discount.getValue());
    //     var persen_tax = parseFloat(this.persen_tax.getValue()) / 100;
    //     var total = parseFloat(this.total.getValue());
    //     var dpp = total - disc;
    //     this.dpp.setValue(dpp);
    //     this.tax.setValue(dpp * persen_tax);
    //     this.grand_total.setValue(dpp * (1 + persen_tax));
    // },
    oncmbSupplierChange: function (combo, record, index) {
        this.supplier_cp.setValue(record.data.contact_person);
        this.top.setValue(record.data.term_of_payment);
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(false);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});