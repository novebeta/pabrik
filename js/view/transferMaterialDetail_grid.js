jun.TransferMaterialDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Material Detail",
    id: 'docs-jun.TransferMaterialDetailGrid',
    iconCls: "silk-grid",
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    viewConfig: {
        forceFit: true
    },
    // plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'Kode  Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 90,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 120,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            readOnly: true,
            dataIndex: 'qty',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
            // renderer: function (value, metaData, record, rowIndex) {
            //     if (record.store)
            //         return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')), "0,0.00");
            //     else
            //         return value;
            // },
            // summaryType: 'sum',
            // summaryRenderer: function (v, params, data) {
            //     return Ext.util.Format.number(jun.konversiSatuanDitampilkan(v, data.data.sat), "0,0.00");
            // }
        }//,
        // {
        //     header: 'Satuan',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'sat',
        //     width: 40,
        //     summaryType: 'max'
        // },
        // {
        //     header: 'Tgl Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_expired',
        //     width: 60,
        //     renderer: Ext.util.Format.dateRenderer('d M Y')
        // },
        // {
        //     header: 'No. Lot',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'no_lot',
        //     width: 70
        // },
        // {
        //     header: 'No. QC',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'no_qc',
        //     width: 70
        // },
        // {
        //     header: 'Supplier',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'supplier_id',
        //     width: 100,
        //     renderer: jun.renderNamaSupplier
        // },
        // {
        //     header: 'Tipe Material',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tipe_material_id',
        //     width: 80,
        //     renderer: jun.renderTipeMaterial
        // }
    ],
    initComponent: function () {
        this.tbar = {
            xtype: 'toolbar',
            // layout: 'vbox',
            // height: 63,
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangCmp, //jun.rztMaterialCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_barang}</b><br>{nama_barang}</span>',
                                "</div></tpl>"),
                            allowBlank: false,
                            listWidth: 400,
                            width: 175,
                            ref: '../../material',
                            displayField: 'kode_barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty:'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            width: 100,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            text: 'Add',
                            ref: '../../btnAdd',
                            xtype: 'button',
                            scale: 'small',
                            width: 50
                        },
                        {
                            text: 'Edit',
                            ref: '../../btnEdit',
                            xtype: 'button',
                            scale: 'small',
                            width: 50
                        },
                        {
                            text: 'Del',
                            ref: '../../btnDelete',
                            xtype: 'button',
                            scale: 'small',
                            width: 50
                        }
                    ]
                }
            ]
        };
        jun.TransferMaterialDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.material.getValue();
        var qty = parseFloat(this.qty.getValue());
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "Bahan baku harus disefinisikan.");
            return;
        }
        if (qty < 0) {
            Ext.MessageBox.alert("Error", "Qty bahan baku harus lebih dari nol.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', barang_id);
            record.set('qty', qty);
            record.commit();
        } else {
            var c = jun.rztTransferMaterialDetail.recordType,
                d = new c({
                    material_id: barang_id,
                    qty: qty
                });
            jun.rztTransferMaterialDetail.add(d);
        }
        this.material.reset();
        this.qty.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.material.setValue(record.data.material_id);
            this.qty.setValue(record.data.qty);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});