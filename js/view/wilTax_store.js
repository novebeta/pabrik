jun.WilTaxstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.WilTaxstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WilTaxStoreId',
            url: 'WilTax',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'wil_tax_id'},
                {name: 'nama_wilayah'}
            ]
        }, cfg));
    }
});
jun.rztWilTax = new jun.WilTaxstore();
jun.rztWilTaxLib = new jun.WilTaxstore();
jun.rztWilTaxCmp = new jun.WilTaxstore();
//jun.rztWilTax.load();
