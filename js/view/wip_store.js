jun.Wipstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Wipstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WipStoreId',
            url: 'Wip',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'sat'},
                {name: 'grup_id'},
                {name: 'qty_per_box'},
                {name: 'qty_per_pot'},
                {name: 'kode_barang_2'},
                {name: 'kode_barang_rnd'},
                {name: 'tipe_barang_id'}
            ]
        }, cfg));
    }
});
jun.rztWip = new jun.Wipstore();
jun.rztWipCmp = new jun.Wipstore();
jun.rztWipLib = new jun.Wipstore();
//jun.rztWip.load();
