jun.InvoiceSupplierGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Invoice Supplier",
    id: 'docs-jun.InvoiceSupplierGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tanggal Terima',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_terima_inv',
            width: 100
        },
        {
            header: 'Tanggal SJ',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_terima',
            width: 100
        },
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'no_inv_supplier',
            width: 100
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 100,
            renderer: jun.renderNamaSuppliers
        },
        {
            header: 'T.O.P',
            sortable: true,
            resizable: true,
            dataIndex: 'term_of_payment',
            width: 100
        },
        {
            header: 'Jatuh Tempo',
            sortable: true,
            resizable: true,
            dataIndex: 'term_of_payment',
            width: 100,
            renderer: function (value, metaData, record, rowIndex) {
                var n = value.match(/\d+/);
                var newdate = jun.dateAdd(record.get('tgl_terima'), parseInt(n[0]));
                if (Number(record.get('status')) < SJ_SUPPLIER_PAID && jun.dateDiff(newdate, new Date()) < 1) {
                    metaData.style = "background-color: #FFD4D4;";
                }
                return newdate.getFullYear() + "-" + (newdate.getMonth() + 1) + "-" + newdate.getDate();
            }
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 100,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case SJ_SUPPLIER_INVOICED :
                        return 'OPEN';
                    case SJ_SUPPLIER_FPT :
                        return 'FPT';
                    case SJ_SUPPLIER_PAID :
                        return 'PAID';
                }
            }
        }
    ],
    userPermission: function (actCreate, actEdit) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        //this.viewAllData = (this.actCreatePO || this.actReject);
    },
    initComponent: function () {
        this.store = jun.rztInvoiceSupplier;
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1);
                break;
            case USER_PPIC:
                this.userPermission(0, 0);
                break;
            case USER_GA:
                this.userPermission(0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0);
                break;
            case USER_PEMBELIAN:
                this.userPermission(1, 1);
                break;
            default:
                this.userPermission(0, 0);
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Lihat' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_sj: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.InvoiceSupplierGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_sj: SJ_SUPPLIER_INVOICED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.InvoiceSupplierGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'FPT',
                                status_sj: SJ_SUPPLIER_FPT,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.InvoiceSupplierGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'PAID',
                                status_sj: SJ_SUPPLIER_PAID,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.InvoiceSupplierGrid').setFilterData(m.status_sj);
                                    }
                                }
                            }
                        ]
                    }
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.InvoiceSupplierGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(SJ_SUPPLIER_INVOICED);
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (this.actEdit) {
            this.btnEdit.setIconClass(r.get('status') == SJ_SUPPLIER_INVOICED ? 'silk13-pencil' : 'silk13-eye');
            this.btnEdit.setText(r.get('status') == SJ_SUPPLIER_INVOICED ? "Ubah" : "Lihat");
        }
    },
    setFilterData: function (sts) {
        this.status_sj = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        this.store.baseParams = {
            mode: "grid",
            filter: 'invoice',
            status: this.status_sj
        };
        this.store.reload();
    },
    loadForm: function () {
        var form = new jun.InvoiceSupplierWin({
            modez: 0,
            iconCls: 'silk13-add',
            storeDetail: jun.rztInvoiceSupplierDetails,
            tipeInvoice: TIPE_INV_TANPA_SJ
        });
        form.show(this);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Invoice.");
            return;
        }
        var ttl = "";
        switch (Number(selectedz.json.status)) {
            case SJ_SUPPLIER_INVOICED :
                ttl = 'OPEN';
                break;
            case SJ_SUPPLIER_FPT :
                ttl = 'FPT';
                break;
            case SJ_SUPPLIER_PAID :
                ttl = 'PAID';
                break;
        }
        var filter = selectedz.json.status == SJ_SUPPLIER_INVOICED;
        var form = new jun.InvoiceSupplierWin({
            modez: (filter ? 1 : 2),
            id: selectedz.json.terima_material_id,
            title: (filter ? "Ubah" : "Lihat") + " Invoice Supplier [" + ttl + "]",
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye',
            storeDetail: jun.rztInvoiceSupplierDetails,
            tipeInvoice: selectedz.json.tipe_invoice
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.griddetils.store.baseParams = {terima_material_id: selectedz.json.terima_material_id};
        form.griddetils.store.load();
        form.griddetils.store.baseParams = {};
    }
});
