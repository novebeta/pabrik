jun.AssemblyWin = Ext.extend(Ext.Window, {
    title: 'Assembly',
    modez: 1,
    folder_id: 1,
    width: 930,
    height: 465,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Assembly',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'hidden',
                        ref: '../folder',
                        name: 'folder_id'
                    },
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Material :",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode_material',
                            'nama_material'
                        ],
                        mode: 'local',
                        store: jun.rztMaterialCmp,
                        hiddenName: 'material_id',
                        valueField: 'material_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode</th><th>Nama Material</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_material}</td><td>{nama_material}</td>',
                            '</tr></tpl></tbody></table>'),
                        width: 175,
                        x: 85,
                        y: 32,
                        allowBlank: false,
                        // enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../material_id',
                        displayField: 'nama_material'
                    },
                    {
                        xtype: "label",
                        text: "Qty :",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'compositefield',
                        fieldLabel: 'Date Range',
                        msgTarget: 'side',
                        anchor: '-20',
                        defaults: {
                            flex: 1
                        },
                        items: [
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                name: 'qty',
                                width: 100,
                                minValue: 1,
                                value: 1
                            },
                            {
                                xtype: 'button',
                                ref: '../../btnBOM',
                                text: 'Load BOM',
                                width: 72
                            }
                        ],
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tgl:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note_',
                        id: 'note_id',
                        ref: '../note_',
                        width: 175,
                        height: 52,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Gudang:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'loc_code',
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'loc_code',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        width: 175,
                        x: 720,
                        y: 2
                    },
                    new jun.AssemblyDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AssemblyWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnBOM.on('click', this.onbtnBOMclick, this);
        jun.rztAssemblyDetails.removeAll();
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onbtnBOMclick: function (t, e) {
        t.setDisabled(true);
        var id = this.material_id.getValue();
        if (id == '') {
            t.setDisabled(false);
            Ext.Msg.alert('Error', 'Material harus dipilih');
            return;
        }
        jun.rztAssemblyDetails.load({
            params: {
                material_id: this.material_id.getValue(),
                qty: this.qty.getValue()
            },
            callback: function () {
                t.setDisabled(false);
            }
        }, this);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Assembly/update/id/' + this.id;
        } else {
            urlz = 'Assembly/create/';
        }

        this.folder.setValue(this.folder_id);
        Ext.getCmp('form-Assembly').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztAssemblyDetails.data.items, "data")),
                id: this.id,
                // folder_id: this.folder_id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztAssembly.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Assembly').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});