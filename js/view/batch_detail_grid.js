jun.BatchDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Material Detail",
    id: 'docs-jun.BatchDetailGrid',
    iconCls: "silk-grid",
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    view: new Ext.grid.GroupingView({
        forceFit: true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})',
        showGroupName: false,
        enableNoGroups: false,
        enableGroupingMenu: true,
        hideGroupedColumn: true
    }),
    plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'Kode  Material',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_material',
            width: 90
        },
        {
            header: 'Nama  Material',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_material',
            width: 160
        },
        {
            header: 'Quantity',
            sortable: true,
            resizable: true,
            readOnly: true,
            dataIndex: 'qty',
            width: 50,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                if (record.store)
                    return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')), "0,0.00");
                else
                    return value;
            },
            summaryType: 'sum',
            summaryRenderer: function (v, params, data) {
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(v, data.data.sat), "0,0.00");
            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 80,
            summaryType: 'max'
        },
        {
            header: 'Tipe Material',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_material_id',
            width: 80,
            renderer: jun.renderTipeMaterial
        }
    ],
    initComponent: function () {
        this.store = jun.rztBatchDetail;
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 5,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Material :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztMaterialForBOM,
                                hiddenName: 'material_id',
                                valueField: 'material_id',
                                readOnly: true,
                                ref: '../../material',
                                displayField: 'kode_material'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Quantity:'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                width: 80,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'satuan',
                                ref: '../../satuan'
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../kode_material'
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../nama_material'
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../sat'
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../tipe_material_id'
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 1,
                        id: 'btnsalesdetilid',
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Edit',
                                width: 50,
                                ref: '../../btnEdit'
                            }
                        ]
                    }
                ]
            };
        }
        jun.BatchDetailGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    btnDisable: function (s) {
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    resetForm: function () {
        this.material.reset();
        this.kode_material.reset();
        this.nama_material.reset();
        this.qty.reset();
        this.sat.reset();
        this.satuan.setText('');
        this.tipe_material_id.reset();
    },
    loadForm: function () {
        var material_id = this.material.getValue();
        if (!material_id) {
            Ext.MessageBox.alert("Error", "Semua field harus diisi dan quantity tidak boleh nol");
            return false;
        } else if (!this.qty.getValue()) {
            Ext.MessageBox.alert("Error", "Quantity harus lebih besar dari nol.");
            return false;
        }
        if (this.store.find('material_id', material_id) != -1) {
            if (this.btnEdit.text == 'Save') {
                if (this.sm.getSelected().data.material_id != material_id) {
                    Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                    this.resetForm();
                    return false;
                }
            } else {
                Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                return false;
            }
        }
        var kode_material = this.kode_material.getValue();
        var nama_material = this.nama_material.getValue();
        var sat = this.sat.getValue();
        var qty = jun.konversiSatuanDisimpan(parseFloat(this.qty.getValue()), sat);
        var tipe_material_id = this.tipe_material_id.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('kode_material', kode_material);
            record.set('nama_material', nama_material);
            record.set('qty', qty);
            record.set('sat', sat);
            record.set('tipe_material_id', tipe_material_id);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    material_id: material_id,
                    kode_material: kode_material,
                    nama_material: nama_material,
                    qty: qty,
                    sat: sat,
                    tipe_material_id: tipe_material_id
                });
            this.store.add(d);
        }
        this.resetForm();
        return true;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.material.setValue(record.data.material_id);
            this.kode_material.setValue(record.data.kode_material);
            this.nama_material.setValue(record.data.nama_material);
            this.qty.setValue(jun.konversiSatuanDitampilkan(record.data.qty, record.data.sat));
            this.sat.setValue(record.data.sat);
            this.satuan.setText(record.data.sat);
            this.tipe_material_id.setValue(record.data.tipe_material_id);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            if (this.loadForm()) {
                btn.setText("Edit");
                this.btnDisable(false);
            }
        }
    },
    calculateQty: function (satuan) {
        //Perhitungan dalam satuan terkecil Gram/Piece
        var qty_capacity = this.qty_capacity;
        var bom_qty = this.recordBOM.get('qty');
        var bom_qty_per_pot = this.recordBOM.get('qty_per_pot');
        this.store.each(function (r) {
            var qty = 0;
            if (r.get('tipe_material_id') == MAT_RAW_MATERIAL) {
                qty = qty_capacity * (r.get('qty_bom') / 100);
                r.set('sat', satuan);
            } else {
                qty = qty_capacity / ((bom_qty / r.get('qty_bom')) * bom_qty_per_pot);
            }
            r.set('qty', qty);
            r.commit();
        });
    }
});
jun.BatchKonsumsiDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Material Detail",
    id: 'docs-jun.BatchKonsumsiDetailGrid',
    iconCls: "silk-grid",
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    view: new Ext.grid.GroupingView({
        forceFit: true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})',
        showGroupName: false,
        enableNoGroups: false,
        enableGroupingMenu: true,
        hideGroupedColumn: true
    }),
    plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 80,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Kode  Material',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_material',
            width: 90
        },
        {
            header: 'Nama  Material',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_material',
            width: 160
        },
        {
            header: 'Quantity',
            sortable: true,
            resizable: true,
            readOnly: true,
            dataIndex: 'qty',
            width: 50,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                if (value > 0) metaData.style += "background-color: #CCFFCC; text-align: right;";
                else metaData.style += "background-color: #FFD4FF; text-align: right;";
                if (record.store)
                    return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')), "0,0.00");
                else
                    return value;
            },
            summaryType: 'sum',
            summaryRenderer: function (v, params, data) {
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(v, data.data.sat), "0,0.00");
            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 40,
            summaryType: 'max'
        },
        {
            header: 'Tgl. Expired',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_expired',
            width: 60,
            renderer: Ext.util.Format.dateRenderer('d-m-Y')
        },
        {
            header: 'No. Lot',
            sortable: true,
            resizable: true,
            dataIndex: 'no_lot',
            width: 80
        },
        {
            header: 'No. QC',
            sortable: true,
            resizable: true,
            dataIndex: 'no_qc',
            width: 80
        },
        {
            header: 'No. Tanda Terima',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 80
        },
        {
            header: 'Tipe Material',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_material_id',
            width: 80,
            renderer: jun.renderTipeMaterial
        }
    ],
    initComponent: function () {
        this.store = jun.rztBatchKonsumsiDetail;
        jun.BatchKonsumsiDetailGrid.superclass.initComponent.call(this);
    }
});
jun.BatchHasilDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Hasil Produksi",
    id: 'docs-jun.BatchHasilDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 70,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'No. Lot',
            sortable: true,
            resizable: true,
            dataIndex: 'no_batch',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 70,
            align: "right",
            renderer: Ext.util.Format.numberRenderer()
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 50,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                return jun.renderSatuanBarang(value);
            }
        },
        {
            header: 'Tgl Expired',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_exp',
            width: 70,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        }
    ],
    initComponent: function () {
        this.store = jun.rztBatchHasil;
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 5,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Tgl :'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../../tgl',
                                width: 110,
                                format: 'd-m-Y',
                                value: new Date(),
                                style: 'margin-bottom:2px'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: ' No. Lot :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../no_batch',
                                width: 150,
                                value: this.recordBatch.get('no_batch'),
                                style: 'margin-bottom:2px'
                            },
                            {
                                xtype: 'label', text: ''
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Tgl.Exp :'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../../tgl_expired',
                                width: 110,
                                format: 'd-m-Y',
                                value: this.recordBatch.get('tgl_expired')
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                width: 150,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: jun.renderSatuanBarang(this.barang_id)
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            xtype: 'button',
                            scale: 'large',
                            height: 44,
                            width: 40
                        },
                        items: [
                            {
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.BatchHasilDetailGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
        }
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    resetForm: function () {
        this.tgl.reset();
        this.no_batch.reset();
        this.qty.reset();
        this.tgl_expired.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item");
            return;
        }
        if (btn.text == 'Edit') {
            this.loadEditForm(record);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function (record) {
        this.tgl.setValue(record.data.tgl);
        this.no_batch.setValue(record.data.no_batch);
        this.qty.setValue(record.data.qty);
        this.tgl_expired.setValue(record.data.tgl_exp);
    },
    loadForm: function () {
        var tgl = this.tgl.getValue();
        var no_batch = this.no_batch.getValue();
        var qty = parseFloat(this.qty.getValue());
        var tgl_expired = this.tgl_expired.getValue();
        if (!no_batch || !qty || !tgl_expired || !tgl) {
            Ext.MessageBox.alert("Error", " Semua field harus diisi.");
            return;
        } else if (qty == 0) {
            Ext.MessageBox.alert("Error", " Quantity tidak boleh nol.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('tgl', tgl);
            record.set('barang_id', this.barang_id);
            record.set('no_batch', no_batch);
            record.set('qty', qty);
            record.set('tgl_exp', tgl_expired);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    tgl: tgl,
                    barang_id: this.barang_id,
                    no_batch: no_batch,
                    qty: qty,
                    tgl_exp: tgl_expired
                });
            this.store.add(d);
        }
        this.resetForm();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});