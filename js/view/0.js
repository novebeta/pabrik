jun.getQtyStock = function (i, l, lbl) {
    var params;
    if (l == null) {
        params = {
            material_id: i
        }
    } else {
        params = {
            material_id: i,
            loc_code: l
        }
    }
    Ext.Ajax.request({
        url: 'StockMoves/QtyStock',
        method: 'POST',
        params: params,
        success: function (f, a) {
            var response = Ext.decode(f.responseText);
            if (response.success) {
                lbl.setText('Stok : ' + Ext.util.Format.number(parseFloat(response.msg), "0,000"));
                lbl.ownerCt.doLayout();
            }
        },
        failure: function (f, a) {
            switch (a.failureType) {
                case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
            }
        }
    });
};
jun.jenisMaterial = new Ext.data.ArrayStore({
    fields: ["id", "text"],
    data: [
        [1, "Bahan Baku"],
        [2, "Setengah Jadi"],
        [3, "Barang Jadi"]
    ]
});
jun.cmbJenisMaterial = Ext.extend(Ext.form.ComboBox, {
    displayField: "text",
    valueField: "id",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    emptyText: "Pilih Jenis Barang",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.jenisMaterial;
        jun.cmbJenisMaterial.superclass.initComponent.call(this)
    }
});
jun.Golstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Golstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GolStoreId',
            url: 'Gol',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {
                    name: 'gol_id'
                },
                {
                    name: 'nama_gol'
                }
            ]
        }, cfg));
    }
});
jun.rztGol = new jun.Golstore();
jun.rztGolLib = new jun.Golstore();
jun.rztGolCmp = new jun.Golstore();
jun.getGrup = function (a) {
    var b = jun.rztGrup, c = b.findExact("grup_id", a);
    return b.getAt(c)
};
jun.getTax = function (a) {
    var brg = jun.getBarang(a);
    var grp = jun.getGrup(brg.data.grup_id);
    var vat = 0;
    if (grp.data.tax == "1" && grp.data.vat > 0) {
        vat = grp.data.vat / 100;
    }
    return vat;
};
jun.isCatJasa = function (a) {
    var brg = jun.getBarang(a);
    var grp = jun.getGrup(brg.data.grup_id);
    return grp.data.kategori_id == 1;
};
jun.getBarang = function (a) {
    var b = jun.rztBarangLib;
    c = b.findExact("barang_id", a);
    return b.getAt(c);
};
jun.renderBarang = function (a, b, c) {
    var jb = jun.getBarang(a);
    if (jb == null) {
        return "";
    }
    return jb.data.nama_barang;
};
jun.renderKodeBarang = function (a, b, c) {
    var jb = jun.getBarang(a);
    if (jb == null) {
        return "";
    }
    return jb.data.kode_barang;
};
jun.getBahanBaku = function (a) {
    var b = jun.rztBahanBakuLib;
    c = b.findExact("material_id", a);
    return b.getAt(c);
};
jun.renderBahanBaku = function (a, b, c) {
    var jb = jun.getBahanBaku(a);
    return jb.data.nama_barang;
};
jun.renderKodeBahanBaku = function (a, b, c) {
    var jb = jun.getBahanBaku(a);
    if (jb == null) {
        return "";
    }
    return jb.data.kode_barang;
};
jun.getWIP = function (a) {
    var b = jun.rztWipLib;
    c = b.findExact("material_id", a);
    return b.getAt(c);
};
jun.renderWIP = function (a, b, c) {
    var jb = jun.getWIP(a);
    return jb.data.nama_barang;
};
jun.renderKodeWIP = function (a, b, c) {
    var jb = jun.getWIP(a);
    if (jb == null) {
        return "";
    }
    return jb.data.kode_barang;
};
jun.getMaterial = function (a) {
    var b = jun.rztMaterialLib;
    c = b.findExact("material_id", a);
    return b.getAt(c);
};
jun.renderKodeBarang2 = function (a, b, c) {
    var jb = jun.getBarang(a);
    if (jb == null) {
        return "";
    }
    return jb.data.kode_barang_2;
};
jun.renderSatuanBarang = function (a, b, c) {
    var jb = jun.getBarang(a);
    if (jb == null) {
        return "";
    }
    return jb.data.sat;
};
jun.getTipeBarang = function (a) {
    var b = jun.rztTipeBarangCmp;
    c = b.findExact("tipe_barang_id", a);
    return b.getAt(c);
};
jun.renderTipeBarang = function (a, b, c) {
    var jb = jun.getTipeBarang(a);
    return jb.data.nama_tipe_barang;
};
jun.getGrupBarang = function (a) {
    var b = jun.rztGrupCmp;
    c = b.findExact("grup_id", a);
    return b.getAt(c);
};
jun.renderGrupBarang = function (a, b, c) {
    var jb = jun.getGrupBarang(a);
    return jb.data.nama_grup;
};
jun.getMaterial = function (a) {
    var b = jun.rztMaterialLib;
    c = b.findExact("material_id", a);
    return b.getAt(c);
};
jun.renderKodeMaterial = function (a, b, c) {
    var jb = jun.getMaterial(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode_material;
};
jun.renderNamaMaterial = function (a, b, c) {
    var jb = jun.getMaterial(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_material;
};
jun.renderSatuanMaterial = function (a, b, c) {
    var jb = jun.getMaterial(a);
    return jb.data.sat;
};
jun.getTipeMaterial = function (a) {
    var b = jun.rztTipeMaterialLib;
    c = b.findExact("tipe_material_id", a);
    return b.getAt(c);
};
jun.renderTipeMaterial = function (a, b, c) {
    var jb = jun.getTipeMaterial(a);
    return jb.data.nama_tipe_material;
};
jun.getStoreLocation = function (a) {
    var b = jun.rztStorageLocation;
    c = b.findExact("id", a);
    return b.getAt(c);
};
jun.renderStorageLocationName = function (a, b, c) {
    var jb = jun.getStoreLocation(a);
    return jb.data.name;
};
jun.renderStorageLocationHavestock = function (a, b, c) {
    var jb = jun.getStoreLocation(a);
    return jb.data.havestock;
};
jun.getPustakaQAtipe = function (a) {
    var b = jun.rztTipeDokumenQA;
    c = b.findExact("pustaka_qa_tipe_id", a);
    return b.getAt(c);
};
jun.renderPustakaQAtipe_name = function (a, b, c) {
    if (!a) return "";
    var jb = jun.getPustakaQAtipe(a);
    return jb.data.tipe_name;
};
jun.getPustakaQAactivity = function (a) {
    var b = jun.rztActivityQA;
    c = b.findExact("pustaka_qa_activity_id", a);
    return b.getAt(c);
};
jun.renderPustakaQAactivity_name = function (a, b, c) {
    if (!a) return "";
    var jb = jun.getPustakaQAactivity(a);
    return jb.data.activity_name;
};
jun.getPustakaQA = function (a) {
    var b = jun.rztPustakaQALib;
    c = b.findExact("pustaka_qa_id", a);
    return b.getAt(c);
};
jun.renderPustakaQA_nomor = function (a, b, c) {
    if (!a) return "";
    var jb = jun.getPustakaQA(a);
    return jb.data.nomor;
};
jun.getUnitOfMeasure = function (a) {
    var b = jun.rztUnitOfMeasureLib;
    c = b.findExact("code", a);
    return b.getAt(c);
};
jun.renderUnitOfMeasure_dimension = function (a, b, c) {
    if (!a) return "";
    var jb = jun.getUnitOfMeasure(a);
    return jb.data.dimension;
};
jun.getSupplier = function (a) {
    var b = jun.rztSupplierLib;
    c = b.findExact("supplier_id", a);
    return b.getAt(c);
};
jun.renderNamaSupplier = function (a, b, c) {
    if (!a) return "";
    var jb = jun.getSupplier(a);
    return jb.data.supplier_name;
};
//====================================== Bill Of Material
jun.getBom = function (a) {
    var b = jun.rztBomLib;
    c = b.findExact("bom_id", a);
    return b.getAt(c);
};
jun.renderBomKodeBarang = function (a, b, c) {
    //var jb = jun.getBarang(jun.getBom(a).data.barang_id);
    var jb = jun.getBom(a);
    return jb.data.kode_barang_rnd;
};
jun.renderBomNamaBarang = function (a, b, c) {
    //var jb = jun.getBarang(jun.getBom(a).data.barang_id);
    var jb = jun.getBom(a);
    return jb.data.nama_barang;
};
jun.renderBomKodeFormula = function (a, b, c) {
    var jb = jun.getBom(a);
    return jb.data.kode_formula;
};
//====================================== Chart Class
jun.getChartClass = function (a) {
    var b = jun.rztChartClassLib, c = b.findExact("cid", a);
    return b.getAt(c);
};
jun.renderChartClass = function (a, b, c) {
    var jb = jun.getChartClass(a);
    if (jb == null) {
        return '';
    }
    return jb.data.class_name;
};
jun.getChartClassType = function (a) {
    var b = jun.rztChartClassTypeLib, c = b.findExact("ctype", a);
    return b.getAt(c);
};
jun.renderChartClassType = function (a, b, c) {
    var jb = jun.getChartClassType(a);
    if (jb == null) {
        return '';
    }
    return jb.data.class_type_name;
};
jun.getChartTypes = function (a) {
    var b = jun.rztChartTypesLib, c = b.findExact("id", a);
    return b.getAt(c);
};
jun.renderChartTypes = function (a, b, c) {
    var jb = jun.getChartTypes(a);
    if (jb == null) {
        return '';
    }
    return jb.data.name;
};
jun.getCoa = function (a) {
    var b = jun.rztChartMasterLib, c = b.findExact("account_code", a);
    return b.getAt(c)
};
jun.renderCoa = function (a, b, c) {
    var jb = jun.getCoa(a);
    if (jb == null) {
        return '';
    }
    return jb.data.account_name;
};
jun.getGol = function (a) {
    var b = jun.rztGolLib, c = b.findExact("gol_id", a);
    return b.getAt(c)
};
jun.renderGol = function (a, b, c) {
    var jb = jun.getGol(a);
    return jb.data.nama_gol;
};
jun.getWilayahInt = function (a) {
    var b = jun.rztWilIntLib;
    c = b.findExact("wil_int_id", a);
    return b.getAt(c)
};
jun.renderWilayahInt = function (a, b, c) {
    var jb = jun.getWilayahInt(a);
    if (jb == null) {
        return "";
    }
    return jb.data.nama_wilayah;
};
jun.getCustomers = function (a) {
    var b = jun.rztCustomersLib;
    c = b.findExact("customer_id", a);
    return b.getAt(c)
};
jun.renderCustomers = function (a, b, c) {
    var jb = jun.getCustomers(a);
    if(jb == undefined){
        return '';
    }
    return jb.data.nama_customer;
};
jun.renderKodeCustomers = function (a, b, c) {
    var jb = jun.getCustomers(a);
    return jb.data.kode_customer;
};
jun.getSuppliers = function (a) {
    var b = jun.rztSupplierLib;
    c = b.findExact("supplier_id", a);
    return b.getAt(c);
};
jun.renderNamaSuppliers = function (a, b, c) {
    var jb = jun.getSuppliers(a);
    if (a == null || a == "" || a == undefined)
        return "";
    return jb.data.supplier_name;
};
jun.getKategori = function (a) {
    var b = jun.rztKategori, c = b.findExact("kategori_id", a);
    return b.getAt(c);
};
jun.renderKategori = function (a, b, c) {
    var jb = jun.getKategori(a);
    return jb.data.nama_kategori;
};
jun.getBeauty = function (a) {
    var b = jun.rztBeauty, c = b.findExact("beauty_id", a);
    return b.getAt(c);
};
jun.renderBeauty = function (a, b, c) {
    if (a == null || a == "" || a == undefined)
        return "";
    var jb = jun.getBeauty(a);
    if (jb == null || jb == "" || jb == undefined)
        return "";
    return jb.data.nama_beauty;
};
jun.getNegara = function (a) {
    var b = jun.rztNegaraLib, c = b.findExact("negara_id", a);
    return b.getAt(c)
};
jun.renderNegara = function (a, b, c) {
    var jb = jun.getNegara(a);
    return jb.data.nama_negara;
};
jun.getProvinsi = function (a) {
    var b = jun.rztProvinsiLib, c = b.findExact("provinsi_id", a);
    return b.getAt(c)
};
jun.renderProvinsi = function (a, b, c) {
    var jb = jun.getProvinsi(a);
    return jb.data.nama_provinsi;
};
jun.getKota = function (a) {
    var b = jun.rztKotaLib, c = b.findExact("kota_id", a);
    return b.getAt(c)
};
jun.renderKota = function (a, b, c) {
    var jb = jun.getKota(a);
    return jb.data.nama_kota;
};
jun.getBank = function (a) {
    var b = jun.rztBankLib, c = b.findExact("bank_id", a);
    return b.getAt(c)
};
jun.renderBank = function (a, b, c) {
    var jb = jun.getBank(a);
    return jb.data.nama_bank;
};
jun.BackupRestoreWin = Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Backup / Restore",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4; padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file restore (*.pbu.gz)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Delete All Transaction",
                    hidden: !1,
                    ref: "../btnDelete"
                },
                {
                    xtype: "button",
                    text: "Download Backup",
                    hidden: !1,
                    ref: "../btnBackup"
                },
                {
                    xtype: "button",
                    text: "Upload Restore",
                    hidden: !1,
                    ref: "../btnRestore"
                }
            ]
        };
        jun.BackupRestoreWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
        this.btnRestore.on("click", this.btnRestoreClick, this);
        this.btnDelete.on("click", this.deleteRec, this);
    },
    onbtnBackupClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().reset();
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "Site/BackupAll";
        var form = Ext.getCmp('form-BackupRestoreWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnRestoreClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = false;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "site/RestoreAll";
        if (Ext.getCmp("form-BackupRestoreWin").getForm().isValid()) {
            Ext.getCmp("form-BackupRestoreWin").getForm().submit({
                url: 'site/RestoreAll',
                waitMsg: 'Uploading your restore...',
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.Msg.alert('Successfully', response.msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Confirm', 'Are you sure delete all transaction?', this.btnDeleteClick, this);
    },
    btnDeleteClick: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: 'site/DeleteTransAll',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.Max_item_SJ = function (tipeBarang) {
    switch (tipeBarang) {
        case BRG_PRODUK_LOKAL:
            return MAX_ITEM_SJ_PRODUK_LOKAL;
            break;
        case BRG_PRODUK_EXPORT:
            return MAX_ITEM_SJ_PRODUK_EXPORT;
            break;
        case BRG_EXPEDISI:
            return MAX_ITEM_SJ_EXPEDISI;
            break;
        case BRG_BARANGRONGSOKAN:
            return MAX_ITEM_SJ_BARANGRONGSOKAN;
            break;
        default:
            return 30;
    }
}
jun.dateAdd = function (stringDate, addDay) {
    var date = new Date(stringDate);
    date.setDate(date.getDate() + addDay);
    return new Date(date);
}
jun.dateDiff = function (date1, date2) {
    var timeDiff = date1.getTime() - date2.getTime();
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
}
/*KONVERSI SATUAN*/
jun.konversiSatuanDisimpan = function (qty, sat) {
    //dimensi BERAT, akan disimpan di database dengan satuan gram
    var r = jun.getUnitOfMeasure(sat);
    return qty * r.get('conversion');
}
jun.konversiSatuanDitampilkan = function (qty, sat) {
    //dimensi BERAT disimpan di database dengan satuan gram
    //maka angka perlu disesuaikan dengan satuan yang akan ditampilkan
    var r = jun.getUnitOfMeasure(sat);
    return qty / r.get('conversion');
}
