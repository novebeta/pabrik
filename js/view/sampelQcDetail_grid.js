jun.SampelQcDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SampelQcDetail",
    id: 'docs-jun.SampelQcDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Kode Barang-2',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang2
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Batch',
            sortable: true,
            resizable: true,
            dataIndex: 'batch',
            width: 100
        },
        {
            header: 'Expired',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_exp',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            readOnly: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSampelQcDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_barang}</b><br>{kode_barang_2}</span>',
                                "</div></tpl>"),
                            allowBlank: false,
                            //listWidth: 300,
                            ref: '../../barang',
                            displayField: 'kode_barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Batch :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            mode: 'local',
                            lazyRender: true,
                            autoSelect: false,
                            hideTrigger: true,
                            matchFieldWidth: !1,
                            pageSize: 20,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:235px;display:inline-block;">{batch}</span>',
                                '<span style="width:100px;display:inline-block;">{tgl_exp:date("F Y")}</span>',
                                '<span style="width:80px;display:inline-block;float:right;text-align:right;">{qty:number( "0,0" )}</span>',
                                "</div></tpl>"),
                            allowBlank: false,
                            listWidth: 470,
                            //forceSelection: true,
                            store: jun.rztBarangBatchCmp,
//                            hiddenName: 'barang_id',
                            valueField: 'batch',
                            ref: '../../batch',
                            displayField: 'batch'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Expired:'
                        },
                        {
                            xtype: 'xdatefield',
                            ref: '../../tgl_exp',
                            fieldLabel: 'tgl_exp',
                            //name: 'tgl_exp',
                            readOnly: true,
                            format: 'd M Y'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 570,
                            colspan: 7,
                            maxLength: 255
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SampelQcDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.batch.on('select', this.onSelectBatch, this);
        this.barang.on('select', this.onSelectBrg, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onSelectBrg: function (c, r, i) {
        jun.rztBarangBatchCmp.baseParams = {
            barang_id: r.data.barang_id
        };
        jun.rztBarangBatchCmp.load();
        jun.rztBarangBatchCmp.baseParams = {};
        this.qty.reset();
        this.batch.reset();
        this.tgl_exp.reset();
    },
    onSelectBatch: function (combo, record, index) {
        this.batch.setValue(record.data.batch);
        this.tgl_exp.setValue(record.data.tgl_exp);
        this.qty.setValue(record.data.qty);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected");
            return
        }
        var barang = jun.getBarang(barang_id);
        var qty = parseFloat(this.qty.getValue());
        var batch = this.batch.getValue();
        var tgl_exp = this.tgl_exp.getValue().format('Y-m-d');
        var ketpot = this.ketpot.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('batch', batch);
            record.set('tgl_exp', tgl_exp);
            record.set('qty', qty);
            record.set('ket', ketpot);
            record.commit();
        } else {
            var c = jun.rztSampelQcDetail.recordType,
                d = new c({
                    barang_id: barang_id,
                    batch: batch,
                    tgl_exp: tgl_exp,
                    qty: qty,
                    ket: ketpot
                });
            jun.rztSampelQcDetail.add(d);
        }
//        this.store.refreshData();
        this.barang.reset();
        this.qty.reset();
        this.batch.reset();
        this.tgl_exp.reset();
        this.ketpot.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.batch.setValue(record.data.batch);
            this.tgl_exp.setValue(record.data.tgl_exp);
            this.qty.setValue(record.data.qty);
            this.ketpot.setValue(record.data.ket);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Sampel QC");
            return;
        }
        var idz = selectedz.json.sampel_detail_id;
        var form = new jun.SjDetailsWin({modez: 1, id: idz}); //==================
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    },
    onSelectBarang: function (combo, record, index) {
        this.batch.setValue(record.data.batch);
        this.tgl_exp.setValue(record.data.tgl_exp);
        this.qty.setValue(record.data.qty);
    }
});
