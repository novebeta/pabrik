jun.ReportInventoryMovements = Ext.extend(Ext.Window, {
    title: "Inventory Movements",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) jun.rztStorageLocationCmp.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovements",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Gudang',
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'storloc',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        ref: '../storloc',
                        allowBlank: false,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportInventoryCard = Ext.extend(Ext.Window, {
    title: "Inventory Card",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 245,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportInventoryCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Gudang',
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztStorageLocation,
                        hiddenName: 'storloc',
                        valueField: 'id',
                        displayField: 'name',
                        // value: UROLE,
                        ref: '../storloc',
                        allowBlank: false,
                        anchor: "100%"
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode_material',
                            'nama_material'
                        ],
                        mode: 'local',
                        store: jun.rztMaterialCmp,
                        hiddenName: 'barang_id',
                        valueField: 'material_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode</th><th>Nama Material</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_material}</td><td>{nama_material}</td>',
                            '</tr></tpl></tbody></table>'),
                        width: 175,
                        allowBlank: false,
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../barang',
                        displayField: 'nama_material'
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        fieldLabel: 'Batch',
                        triggerAction: 'all',
                        mode: 'local',
                        lazyRender: true,
                        autoSelect: false,
                        hideTrigger: true,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:260px;display:inline-block;">{batch}</span>',
                            '<span style="width:100px;display:inline-block;">{tgl_exp:date("F Y")}</span>',
                            '<span style="width:100px;display:inline-block;float:right;">{qty:number( "0,0" )}</span>',
                            "</div></tpl>"),
                        //allowBlank: false,
                        listWidth: 500,
                        //forceSelection: true,
                        store: jun.rztBarangBatchCmp,
                        hiddenName: 'batch',
                        valueField: 'batch',
                        ref: '../batch',
                        displayField: 'batch',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "tgl_exp",
                        ref: "../tgl_exp"
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Kartu Persediaan Produk Jadi',
                        ref: '../format_KPPJ'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryCard.superclass.initComponent.call(this);
        this.barang.on('select', this.onSelectBrg, this);
        this.batch.on('select', this.onSelectBatch, this);
        this.batch.on('change', this.onChangeBatch, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onSelectBrg: function (c, r, i) {
        this.batch.store.baseParams = {
            barang_id: r.data.barang_id,
            storloc: this.storloc.getValue()
        };
        this.batch.store.load();
        this.batch.store.baseParams = {};
        this.batch.reset();
    },
    onSelectBatch: function (c, r, i) {
        this.tgl_exp.setValue(r.get('tgl_exp'));
    },
    onChangeBatch: function (c, n, o) {
        if (c.getRawValue() == "")
            this.tgl_exp.setValue("");
    },
    onbtnPdfclick: function () {
        this.requestReport(
            this.format_KPPJ.getValue() ? "Report/KartuPersediaanProdukJadi" : "Report/InventoryCard",
            "html",
            "_blank"
        );
    },
    onbtnSaveclick: function () {
        this.requestReport(
            this.format_KPPJ.getValue() ? "Report/KartuPersediaanProdukJadi" : "Report/InventoryCard",
            "excel",
            "myFrame"
        );
    },
    requestReport: function (url, format, t) {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = url;
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTerimaBarangDetails = Ext.extend(Ext.Window, {
    title: "Penerimaan Barang Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTerimaBarangDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTerimaBarangDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().url = "Report/TerimaBarangDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTerimaBarangDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaBarangDetails").getForm().url = "Report/TerimaBarangDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportTerimaBarangDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportReturBeliDetails = Ext.extend(Ext.Window, {
    title: "Retur Beli Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportReturBeliDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturBeliDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturBeliDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturBeliDetails").getForm().url = "Report/ReturBeliDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturBeliDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportReturBeliDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturBeliDetails").getForm().url = "Report/ReturBeliDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportReturBeliDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSJDetails = Ext.extend(Ext.Window, {
    title: "Surat Jalan Internal Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSJDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSJDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSJDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJDetails").getForm().url = "Report/SJDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSJDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSJDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJDetails").getForm().url = "Report/SJDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSJDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportINVDetails = Ext.extend(Ext.Window, {
    title: "Invoice Internal Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportINVDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportINVDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportINVDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVDetails").getForm().url = "Report/INVDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportINVDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportINVDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVDetails").getForm().url = "Report/INVDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportINVDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSJTAXDetails = Ext.extend(Ext.Window, {
    title: "Surat Jalan Pajak Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSJTAXDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSJTAXDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSJTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJTAXDetails").getForm().url = "Report/SJTAXDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSJTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSJTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSJTAXDetails").getForm().url = "Report/SJTAXDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSJTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportINVTAXDetails = Ext.extend(Ext.Window, {
    title: "Invoice Pajak Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportINVTAXDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportINVTAXDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportINVTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVTAXDetails").getForm().url = "Report/INVTAXDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportINVTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportINVTAXDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportINVTAXDetails").getForm().url = "Report/INVTAXDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportINVTAXDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportCreditNoteDetails = Ext.extend(Ext.Window, {
    title: "Credit Note Detail",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportCreditNoteDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportCreditNoteDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().url = "Report/CreditNoteDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportCreditNoteDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCreditNoteDetails").getForm().url = "Report/CreditNoteDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportCreditNoteDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportBarangKeluar = Ext.extend(Ext.Window, {
    title: "Barang Keluar",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportBarangKeluar",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Item',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBarangCmp,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:260px;display:inline-block;"><b>{kode_barang}</b><br>{kode_barang_2}<br><i>{nama_barang}</i></span>',
                            "</div></tpl>"),
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        fieldLabel: 'Batch',
                        triggerAction: 'all',
                        mode: 'local',
                        lazyRender: true,
                        autoSelect: false,
                        hideTrigger: true,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:260px;display:inline-block;">{batch}</span>',
                            '<span style="width:100px;display:inline-block;">{tgl_exp:date("F Y")}</span>',
                            '<span style="width:100px;display:inline-block;float:right;">{qty:number( "0,0" )}</span>',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 500,
                        //forceSelection: true,
                        store: jun.rztBarangBatchCmp,
                        hiddenName: 'batch',
                        valueField: 'batch',
                        ref: '../batch',
                        displayField: 'batch'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBarangKeluar.superclass.initComponent.call(this);
        this.barang.on('select', this.onSelectBrg, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onSelectBrg: function (c, r, i) {
        jun.rztBarangBatchCmp.baseParams = {
            barang_id: r.data.barang_id
        };
        jun.rztBarangBatchCmp.load();
        jun.rztBarangBatchCmp.baseParams = {};
        this.batch.reset();
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBarangKeluar").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBarangKeluar").getForm().url = "Report/BarangKeluar";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBarangKeluar').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportBarangKeluar").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBarangKeluar").getForm().url = "Report/BarangKeluar";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportBarangKeluar').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportStokProduksi = Ext.extend(Ext.Window, {
    title: "Stok Produksi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 140,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportStokProduksi",
                labelWidth: 70,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Jenis Stok',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: [
                                'jenis_stok',
                                'displayText'
                            ],
                            data: [[1, 'Barang Jadi'], [2, 'Bahan Baku']]
                        }),
                        hiddenName: 'jenis_stok',
                        valueField: 'jenis_stok',
                        ref: '../jenis_stok',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan No. Batch',
                        name: 'show_batch',
                        ref: '../show_batch'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportStokProduksi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onSelectBrg: function (c, r, i) {
        jun.rztBarangBatchCmp.baseParams = {
            barang_id: r.data.barang_id
        };
        jun.rztBarangBatchCmp.load();
        jun.rztBarangBatchCmp.baseParams = {};
        this.batch.reset();
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportStokProduksi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportStokProduksi").getForm().url = "Report/StokProduksi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportStokProduksi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportStokProduksi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportStokProduksi").getForm().url = "Report/StokProduksi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportStokProduksi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.GenerateLabaRugi = Ext.extend(Ext.Window, {
    title: "Generate Profit Lost",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-GenerateLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Month',
                        ref: '../month',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: "Pilih Bulan",
                        selectOnFocus: !0,
                        store: jun.bulan,
                        hiddenName: 'month',
                        name: 'month',
                        valueField: 'noBulan',
                        displayField: 'namaBulan',
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "spinnerfield",
                        fieldLabel: "Year",
                        name: "year",
                        ref: "../year",
                        maxLength: 4,
                        minValue: 2012,
                        maxValue: 9999,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    //iconCls: "silk13-page_white_excel",
                    text: "Generate",
                    ref: "../btnSave"
                }
            ]
        };
        jun.GenerateLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        var dt = new Date();
        this.year.setValue(dt.format('Y'));
    },
    onbtnSaveclick: function () {
        Ext.getCmp('form-GenerateLabaRugi').getForm().submit({
            url: 'Report/GenerateLabaRugi',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportGeneralLedger = Ext.extend(Ext.Window, {
    title: "General Ledger",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
//        if (jun.rztStoreCmp.getTotalCount() === 0) {
//            jun.rztStoreCmp.load();
//        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGeneralLedger",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'COA',
                        store: jun.rztChartMasterLib,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        forceSelection: true,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        displayField: 'account_code',
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
//                    ,
//                    {
//                        xtype: 'combo',
//                        typeAhead: true,
//                        fieldLabel: 'Branch',
//                        ref: '../store',
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'local',
//                        store: jun.rztStoreCmp,
//                        hiddenName: 'store',
//                        name: 'store',
//                        valueField: 'store_kode',
//                        displayField: 'store_kode',
//                        emptyText: "All Branch",
//                        value: STORE,
//                        anchor: '100%'
//                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralLedger.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = "Report/GeneralLedger";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/GeneralLedger";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = url;
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportGeneralJournal = Ext.extend(Ext.Window, {
    title: "General Journal",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGeneralJournal",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralJournal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralJournal").getForm().url = "Report/GeneralJournal";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/GeneralJournal";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralJournal").getForm().url = url;
        var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportLabaRugi = Ext.extend(Ext.Window, {
    title: "Profit and Loss Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaRugi").getForm().url = "Report/LabaRugi";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportLabaRugi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/LabaRugi";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaRugi").getForm().url = url;
        var form = Ext.getCmp('form-ReportLabaRugi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportNeraca = Ext.extend(Ext.Window, {
    title: "Neraca Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportNeraca",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportNeraca.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportNeraca").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNeraca").getForm().url = "Report/Neraca";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportNeraca').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportNeraca").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNeraca").getForm().url = "Report/Neraca";
        this.format.setValue("openOffice");
        var form = Ext.getCmp('form-ReportNeraca').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekeningKoran = Ext.extend(Ext.Window, {
    title: "Rekening Koran",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekeningKoran",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bank',
                        store: jun.rztBankTransCmpPusat,
                        ref: '../bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekeningKoran.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekeningKoran").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekeningKoran").getForm().url = "Report/RekeningKoran";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekeningKoran').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/RekeningKoran";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportRekeningKoran").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekeningKoran").getForm().url = url;
        var form = Ext.getCmp('form-ReportRekeningKoran').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportEFaktur = Ext.extend(Ext.Window, {
    title: "E-Faktur",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportEFaktur",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_text",
                    text: "Save to CSV",
                    ref: "../btnCSV"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportEFaktur.superclass.initComponent.call(this);
        this.btnCSV.on("click", this.onbtnCSV, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportEFaktur").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportEFaktur").getForm().url = "Report/EFaktur";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportEFaktur').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportEFaktur").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportEFaktur").getForm().url = "Report/EFaktur";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportEFaktur').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnCSV: function () {
        Ext.getCmp("form-ReportEFaktur").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportEFaktur").getForm().url = "Report/EFaktur";
        this.format.setValue("csv");
        var form = Ext.getCmp('form-ReportEFaktur').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportListInvoice = Ext.extend(Ext.Window, {
    title: "List Invoice",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportListInvoice",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Item',
                        triggerAction: 'all',
                        emptyText: 'All Item',
                        lazyRender: true,
                        mode: 'local',
                        name: "barang_id",
                        forceSelection: true,
                        store: jun.rztBarangCmp,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:260px;display:inline-block;"><b>{kode_barang}</b><br>{kode_barang_2}<br><i>{nama_barang}</i></span>',
                            "</div></tpl>"),
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportListInvoice.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportListInvoice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportListInvoice").getForm().url = "Report/ListINV";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportListInvoice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapTagihan = Ext.extend(Ext.Window, {
    title: "Rekap Invoice Piutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapTagihan",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapTagihan.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportRekapTagihan").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapTagihan").getForm().url = "Report/RekapTagihan";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportRekapTagihan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapPenjualan = Ext.extend(Ext.Window, {
    title: "Rekap Penjualan",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapPenjualan",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapPenjualan.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportRekapPenjualan").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPenjualan").getForm().url = "Report/RekapPenjualan";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportRekapPenjualan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapTagihanHutang = Ext.extend(Ext.Window, {
    title: "Rekap Invoice Hutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapTagihanHutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapTagihanHutang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportRekapTagihanHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapTagihanHutang").getForm().url = "Report/RekapTagihanHutang";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportRekapTagihanHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRincianPembelian = Ext.extend(Ext.Window, {
    title: "Rincian Pembelian",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) jun.rztSupplierCmp.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRincianPembelian",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All Supplier',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRincianPembelian.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "_blank");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportRincianPembelian").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRincianPembelian").getForm().url = "Report/ReportRincianPembelian";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportRincianPembelian').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportPurchaseOrder = Ext.extend(Ext.Window, {
    title: "Rekap Purchase Order",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 240,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztTipeMaterialLib.getTotalCount() === 0) jun.rztTipeMaterialLib.load();
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPurchaseOrder",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Tipe Barang',
                        emptyText: 'All',
                        store: jun.rztTipeMaterialLib,
                        hiddenName: 'tipe_material_id',
                        valueField: 'tipe_material_id',
                        displayField: 'nama_tipe_material',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: jun.rztStatusPOStore,
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Detail',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportPurchaseOrder.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportPurchaseOrder").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchaseOrder").getForm().url = "Report/RekapPurchaseOrder";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportPurchaseOrder').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTerimaMaterial = Ext.extend(Ext.Window, {
    title: "Rekap Surat Jalan Supplier",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 215,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTerimaMaterial",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: jun.rztStatusSuratJalanSupplierStore,
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Detail',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTerimaMaterial.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportTerimaMaterial").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaMaterial").getForm().url = "Report/RekapTerimaMaterial";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportTerimaMaterial').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSupplierInvoice = Ext.extend(Ext.Window, {
    title: "Rekap Invoice Supplier",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 240,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSupplierInvoice",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: jun.rztStatusInvoiceSupplierStore,
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Belum dibayar',
                        name: 'show_belum_dibayar'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Belum dibayar dan lewat jatuh tempo',
                        name: 'show_lewat_jatuh_tempo'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSupplierInvoice.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportSupplierInvoice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSupplierInvoice").getForm().url = "Report/RekapSupplierInvoice";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportSupplierInvoice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportPembayaranSupplier = Ext.extend(Ext.Window, {
    title: "Rekap FPT",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 215,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPembayaranSupplier",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: jun.rztStatusPembayaranSupplier,
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Detail',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportPembayaranSupplier.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportPembayaranSupplier").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPembayaranSupplier").getForm().url = "Report/RekapPembayaranSupplier";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportPembayaranSupplier').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportPembelian = Ext.extend(Ext.Window, {
    title: "Rekap Pembelian",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPembelian",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportPembelian.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportPembelian").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPembelian").getForm().url = "Report/RekapPembelian";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportPembelian').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportInventoryMovementsMaterial = Ext.extend(Ext.Window, {
    title: "Inventory Movements",
    iconCls: "silk13-report",
    modez: 1,
    width: 450,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) jun.rztStorageLocationCmp.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovementsMaterial",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Gudang',
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'storloc',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        ref: '../storloc',
                        allowBlank: false,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    // {
                    //     xtype: 'checkbox',
                    //     fieldLabel: "",
                    //     boxLabel: 'Tampilkan Tanggal Expired',
                    //     name: 'show_expired_date',
                    //     ref: '../show_expired_date',
                    //     hidden: (UROLE == USER_GA ? true : false)
                    // },
                    // {
                    //     xtype: 'checkbox',
                    //     fieldLabel: "",
                    //     boxLabel: 'Tampilkan Nomor Lot',
                    //     name: 'show_no_lot',
                    //     ref: '../show_no_lot',
                    //     hidden: (UROLE == USER_GA ? true : false)
                    // },
                    // {
                    //     xtype: 'checkbox',
                    //     fieldLabel: "",
                    //     boxLabel: 'Tampilkan Nomor QC',
                    //     name: 'show_no_qc',
                    //     ref: '../show_no_qc',
                    //     hidden: (UROLE == USER_GA ? true : false)
                    // },
                    // {
                    //     xtype: 'checkbox',
                    //     fieldLabel: "",
                    //     boxLabel: 'Tampilkan Supplier',
                    //     name: 'show_supplier',
                    //     ref: '../show_supplier'
                    // },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovementsMaterial.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel");
    },
    requestReport: function ($format) {
        Ext.getCmp("form-ReportInventoryMovementsMaterial").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovementsMaterial").getForm().url = "Report/InventoryMovementsMaterial";
        this.format.setValue($format);
        var form = Ext.getCmp('form-ReportInventoryMovementsMaterial').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportInventoryCardMaterial = Ext.extend(Ext.Window, {
    title: "Inventory Card",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 225,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-ReportInventoryCardMaterial",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Gudang',
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'storloc',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        ref: '../storloc',
                        allowBlank: false,
                        anchor: "100%"
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode_material',
                            'nama_material'
                        ],
                        mode: 'local',
                        fieldLabel: 'Material',
                        store: jun.rztMaterialCmp,
                        hiddenName: 'material_id',
                        valueField: 'material_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode</th><th>Nama Material</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_material}</td><td>{nama_material}</td>',
                            '</tr></tpl></tbody></table>'),
                        anchor: "100%",
                        allowBlank: false,
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../material',
                        displayField: 'nama_material'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryCardMaterial.superclass.initComponent.call(this);
        // this.storloc.on('select', this.onSelectStorloc, this);
        this.material.on('select', this.onSelectMaterial_expired, this);
        // this.tgl_expired.on('select', this.onSelecttgl_expired, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel");
    },
    requestReport: function ($format) {
        Ext.getCmp("form-ReportInventoryCardMaterial").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCardMaterial").getForm().url = "Report/InventoryCardMaterial";
        this.format.setValue($format);
        var form = Ext.getCmp('form-ReportInventoryCardMaterial').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportProduksi = Ext.extend(Ext.Window, {
    title: "Laporan Produksi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportProduksi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportProduksi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportProduksi").getForm().url = "Report/LaporanProduksi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportProduksi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportProduksi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportProduksi").getForm().url = "Report/LaporanProduksi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportProduksi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTransferMaterial = Ext.extend(Ext.Window, {
    title: "Rekap Transfer Material",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 240,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTransferMaterial",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Pengirim',
                        emptyText: 'All',
                        store: jun.rztStorageLocation,
                        hiddenName: 'pengirim',
                        valueField: 'id',
                        displayField: 'name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Penerima',
                        emptyText: 'All',
                        store: jun.rztStorageLocation,
                        hiddenName: 'penerima',
                        valueField: 'id',
                        displayField: 'name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: jun.rztStatusAcceptedTandaTerima,
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Detail',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTransferMaterial.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportTransferMaterial").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTransferMaterial").getForm().url = "Report/RekapTransferMaterial";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportTransferMaterial').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTransferBarang = Ext.extend(Ext.Window, {
    title: "Rekap Transfer Barang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 240,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTransferBarang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Pengirim',
                        emptyText: 'All',
                        store: jun.rztStorageLocation,
                        hiddenName: 'pengirim',
                        valueField: 'id',
                        displayField: 'name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Penerima',
                        emptyText: 'All',
                        store: jun.rztStorageLocation,
                        hiddenName: 'penerima',
                        valueField: 'id',
                        displayField: 'name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: jun.rztStatusAcceptedTandaTerima,
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Detail',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTransferBarang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportTransferBarang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTransferBarang").getForm().url = "Report/RekapTransferBarang";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportTransferBarang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.ReportGiro = Ext.extend(Ext.Window, {
    title: "Laporan Giro",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGiro",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    // {
                    //     fieldLabel: 'From',
                    //     xtype: 'xdatefield',
                    //     format: 'd M Y',
                    //     name: "tglfrom",
                    //     ref: '../tglfrom',
                    //     anchor: "100%"
                    // },
                    // {
                    //     fieldLabel: 'To',
                    //     xtype: 'xdatefield',
                    //     format: 'd M Y',
                    //     name: "tglto",
                    //     ref: '../tglto',
                    //     anchor: "100%"
                    // },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGiro").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGiro").getForm().url = "Report/ReportGiro";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGiro').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportGiro").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGiro").getForm().url = "Report/ReportGiro";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportGiro').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportBukuPiutang = Ext.extend(Ext.Window, {
    title: "Buku Piutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 1,
    height: 1,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBukuPiutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        value: 'excel',
                        ref: "../format"
                    }
                ]
            }
        ];
        jun.ReportBukuPiutang.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
    },
    onActivate: function(){
        Ext.getCmp("form-ReportBukuPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBukuPiutang").getForm().url = "Report/BukuPiutang";
        var form = Ext.getCmp('form-ReportBukuPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
        this.close();
    }
});
jun.ReportBukuHutang = Ext.extend(Ext.Window, {
    title: "Buku Hutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 1,
    height: 1,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBukuHutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        value: 'excel',
                        ref: "../format"
                    }
                ]
            }
        ];
        jun.ReportBukuHutang.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
    },
    onActivate: function(){
        Ext.getCmp("form-ReportBukuHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBukuHutang").getForm().url = "Report/BukuHutang";
        var form = Ext.getCmp('form-ReportBukuHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
        this.close();
    }
});