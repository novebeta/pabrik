jun.Grupstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Grupstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GrupStoreId',
            url: 'Grup',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'grup_id'},
                {name: 'nama_grup'}
            ]
        }, cfg));
    }
});
jun.rztGrup = new jun.Grupstore();
jun.rztGrupLib = new jun.Grupstore();
jun.rztGrupCmp = new jun.Grupstore();
//jun.rztGrup.load();
