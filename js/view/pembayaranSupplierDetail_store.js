jun.PembayaranSupplierDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembayaranSupplierDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembayaranSupplierDetailsStoreId',
            url: 'PembayaranSupplierDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembayaran_supplier_details_id'},
                {name: 'pembayaran_supplier_id'},
                {name: 'terima_material_id'},
                {name: 'no_inv_supplier'},
                {name: 'tgl_inv_supplier'},
                {name: 'return_pembelian_id'},
                {name: 'no_nota_return'},
                {name: 'tgl_nota_return'},
                {name: 'dpp', type: 'float'},
                {name: 'ppn', type: 'float'},
                {name: 'materai', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'pph', type: 'float'},
                {name: 'total_bayar', type: 'float'},
                {name: 'temporary_id'}
            ]
        }, cfg));
    },
    refreshData: function () {
        Ext.getCmp('form-PembayaranSupplier-dpp').setValue(this.sum('dpp'));
        Ext.getCmp('form-PembayaranSupplier-ppn').setValue(this.sum('ppn'));
        Ext.getCmp('form-PembayaranSupplier-materai').setValue(this.sum('materai'));
        Ext.getCmp('form-PembayaranSupplier-total').setValue(this.sum('total'));
        Ext.getCmp('form-PembayaranSupplier-pph').setValue(this.sum('pph'));
        Ext.getCmp('form-PembayaranSupplier-total_bayar').setValue(this.sum('total_bayar'));
    }
});
jun.rztPembayaranSupplierDetails = new jun.PembayaranSupplierDetailsstore();
//digunakan untuk store Combo pembuatan FPT
//berisi daftar Invoice yang akan dibayar dan 
//Nota Return untuk pengurangan tagihan
jun.ListInvoiceDanNotaReturnStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ListInvoiceDanNotaReturnStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembayaranSupplierStoreId',
            url: 'PembayaranSupplier/GetInvoiceAndNotaReturn',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'temporary_id'},
                {name: 'display_no'},
                {name: 'display_tgl'},
                {name: 'terima_material_id'},
                {name: 'no_inv_supplier'},
                {name: 'tgl_inv_supplier'},
                {name: 'return_pembelian_id'},
                {name: 'no_nota_return'},
                {name: 'tgl_nota_return'},
                {name: 'dpp', type: 'float'},
                {name: 'ppn', type: 'float'},
                {name: 'pph', type: 'float'},
                {name: 'total_bayar', type: 'float'}
            ]
        }, cfg));
    }
});