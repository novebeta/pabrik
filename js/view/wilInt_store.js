jun.WilIntstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.WilIntstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WilIntStoreId',
            url: 'WilInt',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'wil_int_id'},
                {name: 'nama_wilayah'}
            ]
        }, cfg));
    }
});
jun.rztWilInt = new jun.WilIntstore();
jun.rztWilIntCmp = new jun.WilIntstore();
jun.rztWilIntLib = new jun.WilIntstore();
//jun.rztWilInt.load();
