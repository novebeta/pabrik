jun.PenjualanKreditGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Penjualan",
    id: 'docs-jun.PenjualanGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_id',
            width: 100,
            renderer: jun.renderCustomers
        },
        {
            header: 'Jatuh Tempo',
            sortable: true,
            resizable: true,
            dataIndex: 'tempo',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        jun.rztSalesKredit.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglsalesgridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztSalesKredit;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Penjualan',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Penjulan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Penjulan',
                    ref: '../btnShow'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Surat Jalan',
                    ref: '../btnAddSJ'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Close Sales',
                    ref: '../btnCloseSales'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglsalesgridid',
                    ref: '../tgl',
                    noPastYears: false
                })
            ]
        };
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        if (jun.rztSalesmanCmp.getTotalCount() === 0) {
            jun.rztSalesmanCmp.load();
        }
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztBank.getTotalCount() === 0) {
            jun.rztBank.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        jun.PenjualanKreditGrid.superclass.initComponent.call(this);
        jun.rztSalesKredit.removeAll();
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnAddSJ.on('Click', this.loadFormSJ, this);
        // this.sales.on('select', this.onSalesChange, this);
        // this.tgl.on('select', this.onSalesChange, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnShow.on('Click', this.loadShowForm, this);
        this.btnCloseSales.on('Click', this.onbtnCloseClick, this);
        // this.btnCetak.on('Click', this.onbtnCetakClick, this);

        this.tgl.on('select', this.selectTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.load();
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    onbtnCloseClick: function (t, e) {
        t.setDisabled(true);
        var selectedz = this.sm.getSelected();
        if (selectedz.json.received == 2) {
            Ext.MessageBox.alert("Warning", "Barang sudak diterima semua.");
            t.setDisabled(false);
            return;
        }
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih faktur");
            t.setDisabled(false);
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin close penjualan ini?',
            function (btn) {
                if (btn == 'no') {
                    t.setDisabled(false);
                    return;
                }
                Ext.Ajax.request({
                    url: 'Sales/update',
                    method: 'POST',
                    params: {
                        sales_id: this.record.data.sales_id
                    },
                    success: function (f, a) {
                        jun.rztSalesKredit.reload();
                        var response = Ext.decode(f.responseText);
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: response.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert(a.response.statusText, a.response.responseText);
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                        t.setDisabled(false);
                    }
                });
            }, this);
    },
    loadFormSJ: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning",
                "Anda belum memilih faktur");
            return;
        }
        if (selectedz.json.received == 2) {
            Ext.MessageBox.alert("Warning", "Barang sudak diterima semua.");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.SjWin({
            modez: 0
        });
        form.sales_id.setValue(selectedz.json.sales_id);
        form.customer_id.setValue(selectedz.json.customer_id);
        form.show();
        jun.rztSjDetails.baseParams = {
            sales_id: idz
        };
        jun.rztSjDetails.load();
        jun.rztSjDetails.baseParams = {};
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        // var tgl = this.tgl.getValue();
        // var sales = this.sales.getValue();
        // if (tgl == "" || sales == "") {
        //     Ext.MessageBox.alert("Warning",
        //         "Anda belum memilih Tanggal atau Sales");
        //     return;
        // }
        var form = new jun.SalesKreditWin({
            modez: 0
        });
        form.show();
        // form.tgl.setValue(tgl);
        // form.sales.setValue(sales);
        form.btnSave.setVisible(true);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih faktur");
            return;
        }
        if (selectedz.json.received > 0) {
            Ext.MessageBox.alert("Warning", "Sales tidak bisa diedit karena sudah ada penerimaan.");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesKreditWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetail.baseParams = {
            sales_id: idz
        };
        jun.rztSalesDetail.load();
        jun.rztSalesDetail.baseParams = {};
    },
    loadShowForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih faktur");
            return;
        }
        // if (selectedz.json.received > 0) {
        //     Ext.MessageBox.alert("Warning", "Sales tidak bisa diedit karena sudah ada penerimaan.");
        //     return;
        // }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesKreditWin({
            modez: 2,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetail.baseParams = {
            sales_id: idz
        };
        jun.rztSalesDetail.load();
        jun.rztSalesDetail.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan',
            'Apakah anda yakin ingin menghapus data ini?',
            this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Penjualan/delete/id/' + record.json.penjualan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPenjualanKredit.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});
jun.ReturPenjualanKreditGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Retur Penjualan",
    id: 'docs-jun.ReturPenjualanGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_id',
            width: 100,
            renderer: jun.renderCustomers
        },
        {
            header: 'Jatuh Tempo',
            sortable: true,
            resizable: true,
            dataIndex: 'tempo',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        jun.rztReturSales.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglretursalesgridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztReturSales;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Retur Penjualan',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Retur Penjulan',
                    ref: '../btnShow'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Void Retur Penjulan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglretursalesgridid',
                    ref: '../tgl',
                    noPastYears: false
                })

                //,
                // {
                //     xtype: 'tbseparator'
                // }//,
                // {
                //     xtype: 'button',
                //     text: 'Buat Surat Jalan',
                //     ref: '../btnAddSJ'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Close Sales',
                //     ref: '../btnCloseSales'
                // }
            ]
        };
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        if (jun.rztSalesmanCmp.getTotalCount() === 0) {
            jun.rztSalesmanCmp.load();
        }
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztBank.getTotalCount() === 0) {
            jun.rztBank.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        jun.ReturPenjualanKreditGrid.superclass.initComponent.call(this);
        jun.rztSalesKredit.removeAll();
        this.btnAdd.on('Click', this.loadForm, this);
        this.tgl.on('select', this.selectTgl, this);
        // this.btnAddSJ.on('Click', this.loadFormSJ, this);
        // this.sales.on('select', this.onSalesChange, this);
        // this.tgl.on('select', this.onSalesChange, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnShow.on('Click', this.loadShowForm, this);
        // this.btnCloseSales.on('Click', this.onbtnCloseClick, this);
        // this.btnCetak.on('Click', this.onbtnCetakClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.load();
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        // var tgl = this.tgl.getValue();
        // var sales = this.sales.getValue();
        // if (tgl == "" || sales == "") {
        //     Ext.MessageBox.alert("Warning",
        //         "Anda belum memilih Tanggal atau Sales");
        //     return;
        // }
        var form = new jun.ReturSalesWin({
            modez: 0
        });
        form.show();
        form.btnSave.setVisible(true);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih faktur");
            return;
        }
        if (selectedz.json.received > 0) {
            Ext.MessageBox.alert("Warning", "Sales tidak bisa diedit karena sudah ada penerimaan.");
            return;
        }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesKreditWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetail.baseParams = {
            sales_id: idz
        };
        jun.rztSalesDetail.load();
        jun.rztSalesDetail.baseParams = {};
    },
    loadShowForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih faktur");
            return;
        }
        // if (selectedz.json.received > 0) {
        //     Ext.MessageBox.alert("Warning", "Sales tidak bisa diedit karena sudah ada penerimaan.");
        //     return;
        // }
        var idz = selectedz.json.sales_id;
        var form = new jun.SalesKreditWin({
            modez: 2,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSalesDetail.baseParams = {
            sales_id: idz
        };
        jun.rztSalesDetail.load();
        jun.rztSalesDetail.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan',
            'Apakah anda yakin ingin menghapus data ini?',
            this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ReturPenjualan/delete/id/' + record.json.penjualan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturPenjualanKredit.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                var msg = 'server-side failure with status code ' + f.status;
                if (a.failureType == 'server') {
                    msg = a.result.msg;
                }
                Ext.MessageBox.show({
                    title: 'Warning',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});