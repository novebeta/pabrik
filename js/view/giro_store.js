jun.Girostore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Girostore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GiroStoreId',
            url: 'Giro',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'giro_id'},
                {name: 'no_giro'},
                {name: 'tgl_klr'},
                {name: 'tgl_jt'},
                {name: 'bank_giro'},
                {name: 'saldo'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'tgl_cair'},
                {name: 'bank_id'},
                {name: 'used'},
                {name: 'tolak'},
                {name: 'notep_'},
                {name: 'customer_id'}
            ]
        }, cfg));
    }
});
jun.rztGiro = new jun.Girostore();
jun.rztGiroCmp = new jun.Girostore({
    url: 'Giro/sisa'
});
jun.rztGiroLib = new jun.Girostore();
jun.rztGiroLib.load();
