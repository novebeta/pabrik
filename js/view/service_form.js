jun.ServiceWin = Ext.extend(Ext.Window, {
    title: 'Service Masuk',
    modez: 1,
    width: 500,
    height: 495,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Service',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc. Ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        readOnly: true,
                        width: 175
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        value: DATE_NOW,
                        width: 175
                    },
                    {
                        xtype: 'combo',
                        // style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Customer',
                        forceSelection: true,
                        store: jun.rztCustomersCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{kode_customer}</b><br>{nama_customer}</span>',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 400,
                        ref: '../customer',
                        displayField: 'kode_customer'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Gudang',
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'loc_code',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        width: 175
                    },
                    new jun.ServiceDetailsGrid({
                        frameHeader: !1,
                        header: !1,
                        height: 300
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ServiceWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        jun.rztServiceDetails.removeAll(true);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Service/update/id/' + this.id;
        } else {
            urlz = 'Service/create/';
        }
        Ext.getCmp('form-Service').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztServiceDetails.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztService.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Service').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ServiceOutWin = Ext.extend(Ext.Window, {
    title: 'Service Keluar',
    modez: 1,
    width: 770,
    height: 495,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;',
                id: 'form-ServiceOut',
                labelWidth: 100,
                labelAlign: 'left',
                // layout: 'form',
                layout: {
                    type: 'vbox',
                    padding: '5',
                    align: 'stretch'
                },
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "panel",
                        anchor: '100%',
                        layout: 'hbox',
                        bodyStyle: 'background-color: #E4E4E4;',
                        layoutConfig: {
                            padding: 5,
                            align: 'stretch'
                        },
                        defaults: {
                            frame: false,
                            flex: 1
                        },
                        margins: '0 0 5 0',
                        height: 65,
                        items: [
                            {
                                xtype: "panel",
                                labelWidth: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                bodyStyle: 'background-color: #E4E4E4;',
                                border: false,
                                margins: '0 25 0 0',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Doc. Ref',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'doc_ref',
                                        id: 'doc_refid',
                                        ref: '../../../doc_ref',
                                        maxLength: 50,
                                        readOnly: true,
                                        anchor: "100%"
                                    },
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../../../tgl',
                                        fieldLabel: 'Tgl',
                                        name: 'tgl',
                                        id: 'tglid',
                                        format: 'd M Y',
                                        value: DATE_NOW,
                                        anchor: "100%"
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                labelWidth: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                bodyStyle: 'background-color: #E4E4E4;',
                                border: false,
                                items: [
                                    {
                                        xtype: 'combo',
                                        // style: 'margin-bottom:2px',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        fieldLabel: 'Customer',
                                        forceSelection: true,
                                        store: jun.rztCustomersCmp,
                                        hiddenName: 'customer_id',
                                        valueField: 'customer_id',
                                        itemSelector: "div.search-item",
                                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                            '<span style="width:100%;display:inline-block;"><b>{kode_customer}</b><br>{nama_customer}</span>',
                                            "</div></tpl>"),
                                        allowBlank: false,
                                        listWidth: 400,
                                        ref: '../../../customer',
                                        displayField: 'kode_customer',
                                        anchor: "100%"
                                    },
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        forceSelection: true,
                                        fieldLabel: 'Gudang',
                                        ref: '../../../gudang',
                                        store: jun.rztStorageLocationCmp,
                                        hiddenName: 'loc_code',
                                        valueField: 'loc_code',
                                        displayField: 'loc_code',
                                        anchor: "100%"
                                    }
                                ]
                            }
                        ]
                    },
                    new jun.ServiceDetailsGrid({
                        frameHeader: !1,
                        margins: '0 0 5 0',
                        header: !1,
                        flex: 1
                    }),
                    new jun.MaterialUseGrid({
                        frameHeader: !1,
                        // margins: 5,
                        header: !1,
                        flex: 1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ServiceOutWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        jun.rztServiceDetails.removeAll(true);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Service/update/id/' + this.id;
        } else {
            urlz = 'Service/create/';
        }
        Ext.getCmp('form-ServiceOut').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztServiceDetails.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztServiceOut.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ServiceOut').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});