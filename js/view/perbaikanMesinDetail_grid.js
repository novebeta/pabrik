jun.PerbaikanMesinDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Perbaikan Mesin Detail",
    id: 'docs-jun.PerbaikanMesinDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 60,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 80,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 120,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Sat',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 40
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 120,
            renderer: jun.renderNamaSuppliers
        },
        {
            header: 'Ket',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 150
        }
    ],
    initComponent: function () {
        this.store = jun.rztPerbaikanMesinDetail;
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 9,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Tgl :'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../../tgl',
                                width: 90,
                                format: 'd-m-Y',
                                value: new Date(),
                                style: 'margin-bottom:2px'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Sparepart:'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztMaterialSparepart,
                                hiddenName: 'material_id',
                                valueField: 'material_id',
                                ref: '../../material',
                                width: 120,
                                displayField: 'kode_material',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<div style="width:100%;display:inline-block;"><span style="font-weight: bold">{kode_material}</span><br>{nama_material}</div>',
                                    "</div></tpl>")
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Supplier:'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                mode: 'local',
                                lazyRender: true,
                                autoSelect: false,
                                hideTrigger: true,
                                matchFieldWidth: !1,
                                pageSize: 20,
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="width:140px;display:inline-block;">{supplier_name}</span>',
                                    '<span style="width:80px;display:inline-block;text-align: right;">{qty:number( "0,0" )}</span>',
                                    '<span style="width:60px;display:inline-block;">&nbsp&nbsp{sat}</span>',
                                    "</div></tpl>"),
                                //allowBlank: false,
                                listWidth: 310,
                                forceSelection: true,
                                store: new jun.MaterialExpDateStore(),
                                hiddenName: 'supplier_id',
                                valueField: 'supplier_id',
                                ref: '../../supplier_id',
                                width: 120,
                                displayField: 'supplier_name'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty:'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                width: 80,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'satuan',
                                ref: '../../satuan'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Note :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../ket',
                                width: 570,
                                colspan: 7,
                                maxLength: 255
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../sat'
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../available_qty',
                                value: 0
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'large',
                            width: 40
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.PerbaikanMesinDetailGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.material.on('select', this.onSelectMaterial, this);
            this.supplier_id.on('select', this.onSelectSupplier, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onSelectMaterial: function (c, r, i) {
        this.loadComboSupplier(c.getValue());
        this.qty.reset();
        this.sat.setValue(r.get('sat'));
        this.satuan.setText(r.get('sat'));
    },
    loadComboSupplier: function (v) {
        this.supplier_id.store.load({
            params: {material_id: v, noZeroStock: 1}
        });
        this.supplier_id.reset();
    },
    onSelectSupplier: function (c, r, i) {
        this.qty.setValue(r.get('qty'));
        this.available_qty.setValue(r.get('qty'));
    },
    resetForm: function () {
        this.tgl.reset();
        this.material.reset();
        this.supplier_id.reset();
        this.qty.reset();
        this.sat.reset();
        this.ket.reset();
        this.satuan.setText('');
        this.available_qty.reset();
    },
    loadForm: function () {
        var tgl = this.tgl.getValue();
        var material_id = this.material.getValue();
        var supplier_id = this.supplier_id.getValue();
        var qty = parseFloat(this.qty.getValue());
        if (!tgl || !material_id || !supplier_id) {
            Ext.MessageBox.alert("Error", "Semua field harus diisi");
            return false;
        } else if (qty <= 0) {
            Ext.MessageBox.alert("Error", "Quantity harus lebih besar dari nol.");
            return false;
        }
        var sat = this.sat.getValue();
        var ket = this.ket.getValue();
        if (parseFloat(this.available_qty.getValue()) <= 0 || qty > parseFloat(this.available_qty.getValue())) {
            Ext.MessageBox.alert("Error", "Jumlah stok <b>" + this.material.getRawValue() + "</b> tidak mencukupi.<br>Kekurangan : <b>" + (parseFloat(this.available_qty.getValue()) - qty) + "</b> " + sat);
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('tgl', tgl);
            record.set('material_id', material_id);
            record.set('qty', qty);
            record.set('sat', sat);
            record.set('supplier_id', supplier_id);
            record.set('ket', ket);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    tgl: tgl,
                    material_id: material_id,
                    qty: qty,
                    sat: sat,
                    supplier_id: supplier_id,
                    ket: ket
                });
            this.store.add(d);
        }
        this.resetForm();
        return true;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.supplier_id.store.load({
                params: {
                    material_id: record.data.material_id,
                    noZeroStock: 1,
                    trans_no: record.data.perbaikan_mesin_id
                },
                callback: this.loadEditForm,
                scope: this
            });
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            if (this.loadForm()) {
                btn.setText("Edit");
                this.btnDisable(false);
            }
        }
    },
    loadEditForm: function () {
        var record = this.sm.getSelected();
        this.tgl.setValue(record.data.tgl);
        this.material.setValue(record.data.material_id);
        this.qty.setValue(record.data.qty);
        this.sat.setValue(record.data.sat);
        this.satuan.setText(record.data.sat);
        this.supplier_id.setValue(record.data.supplier_id);
        this.ket.setValue(record.data.ket);
        var i = this.supplier_id.store.find('supplier_id', record.data.supplier_id);
        var sisa_stock = 0;
        if (i > -1) {
            sisa_stock = this.supplier_id.store.getAt(i).get('qty');
            this.available_qty.setValue(sisa_stock);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
