jun.ReturnPembelianDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Return Pembelian",
    id: 'docs-jun.ReturnPembelianDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 80,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 70,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
            // renderer : function(value, metaData, record, rowIndex){
            //     return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')), "0,0.00");
            // }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 50
        },
        // {
        //     header: 'Tgl Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_expired',
        //     width: 70,
        //     renderer: Ext.util.Format.dateRenderer('d-m-Y')
        // },
        // {
        //     header: 'No. Lot',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'no_lot',
        //     width: 70
        // },
        // {
        //     header: 'No. QC',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'no_lot',
        //     width: 70
        // },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_return',
            width: 100
        }
    ],
    initComponent: function () {
        // if(!this.readOnly){
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Kode Material :'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama Material</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'),
                            allowBlank: false,
                            enableKeyEvents: true,
                            listWidth: 400,
                            width: 100,
                            ref: '../../material',
                            id: 'form-PurchaseOrder_combo_material',
                            displayField: 'kode_material'
                        },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     text: 'Exp :'
                        // },
                        // {
                        //     xtype: 'combo',
                        //     style: 'margin-bottom:2px',
                        //     typeAhead: true,
                        //     triggerAction: 'all',
                        //     mode: 'local',
                        //     lazyRender: true,
                        //     autoSelect: false,
                        //     hideTrigger: true,
                        //     matchFieldWidth: !1,
                        //     pageSize: 20,
                        //     itemSelector: "div.search-item",
                        //     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        //         '<span style="width:100px;display:inline-block;">{tgl_expired:date("d-m-Y")}</span>',
                        //         '<span style="width:100px;display:inline-block;text-align:right;">{qty:number( "0,0" )}</span>',
                        //         '<span style="width:190px;display:inline-block;float:right;text-align:right; color: red;">{no_lot} / {no_qc}</span>',
                        //         "</div></tpl>"),
                        //     allowBlank: false,
                        //     listWidth: 420,
                        //     forceSelection: true,
                        //     // store: new jun.TerimaMaterialDetailsstore(),
                        //     width: 100,
                        //     valueField: 'tgl_expired',
                        //     ref: '../../cmb_expired',
                        //     displayField: 'tgl_expired'
                        // },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 70,
                            minValue: 0
                        },

                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stok'
                        },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     ref: '../../satuan',
                        //     text: 'Unit'
                        // },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../note',
                            width: 300,
                            colspan: 4,
                            maxLength: 50
                        }
                        // {
                        //     xtype: 'hidden',
                        //     ref: '../../tgl_expired',
                        //     value: 'Unit'
                        // },
                        // {
                        //     xtype: 'hidden',
                        //     ref: '../../no_lot',
                        //     value: 'Unit'
                        // },
                        // {
                        //     xtype: 'hidden',
                        //     ref: '../../no_qc',
                        //     value: 'Unit'
                        // },
                        // {
                        //     xtype: 'hidden',
                        //     ref: '../../sat',
                        //     value: 'Unit'
                        // }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large',
                        width: 30
                    },
                    items: [
                        // {
                        //     xtype: 'button',
                        //     text: 'Add',
                        //     height: 44,
                        //     ref: '../../btnAdd'
                        // },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        // }
        jun.ReturnPembelianDetailsGrid.superclass.initComponent.call(this);
        // if(!this.readOnly){
        // this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.material.on('select', this.showStock, this);
        // this.material.on('select', this.onSelectMaterial, this);
        // this.cmb_expired.on('select', this.onSelectCmbExpired, this);
        // this.cmb_expired.on('change', this.onChangeCmbExpired, this);
        // }
        this.material.on('select', this.showStock, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    showStock: function () {
        var material_id = this.material.getValue();
        var loc_code = Ext.getCmp('retur_beli_loc_code_id').getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stok);
        }
    },
    // onSelectMaterial: function (c, r, i) {
    //     this.loadStoreCmbExpired(c.getValue());
    //     this.qty.reset();
    //     this.sat.setValue(r.get('sat'));
    //     this.satuan.setText(this.sat.getValue());
    //     this.tgl_expired.reset();
    //     this.note.reset();
    // },
    loadStoreCmbExpired: function (v) {
        this.cmb_expired.store.baseParams = {
            terima_material_id: this.terima_material_id,
            material_id: v
        };
        this.cmb_expired.store.load();
        this.cmb_expired.store.baseParams = {};
    },
    onSelectCmbExpired: function (c, r, i) {
        this.qty.setValue(r.get('qty'));
        this.tgl_expired.setValue(r.get('tgl_expired'));
        this.no_lot.setValue(r.get('no_lot'));
        this.no_qc.setValue(r.get('no_qc'));
        c.setRawValue(Ext.util.Format.date(c.getValue(), 'd-m-Y'));
    },
    onChangeCmbExpired: function (c, n, o) {
        c.setRawValue(Ext.util.Format.date(c.getValue(), 'd-m-Y'));
    },
    btnDisable: function (s) {
        // this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    resetForm: function () {
        this.material.reset();
        // this.cmb_expired.reset();
        this.qty.reset();
        // this.sat.reset();
        // this.satuan.setText(this.sat.getValue());
        // this.tgl_expired.reset();
        this.note.reset();
    },
    loadForm: function () {
        var material_id = this.material.getValue();
        if (material_id == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih item.");
            return;
        }
        // var no_lot = this.no_lot.getValue();
        // var no_qc = this.no_qc.getValue();
//      kunci material + no lot + no qc sama tidak boleh masuk dua kali
//         if (this.store.findBy(function (r) {
//                 var result = false;
//                 if (r.get('material_id') == material_id && r.get('no_lot') == no_lot && r.get('no_qc') == no_qc) result = true;
//                 return result;
//             }) != -1) {
//             if (this.btnEdit.text == 'Save') {
//                 var s = this.sm.getSelected();
//                 if (s.get('material_id') != material_id && s.get('no_lot') != no_lot && s.get('no_qc') != no_qc) {
//                     Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
//                     this.resetForm();
//                     return false;
//                 }
//             } else {
//                 Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
//                 return false;
//             }
//         }
//         var sat = this.sat.getValue();
//         var qty = !this.qty.getValue() ? 0 : jun.konversiSatuanDisimpan(parseFloat(this.qty.getValue()), sat);
        var qty = this.qty.getValue();
        // var r = this.cmb_expired.store.getAt(this.cmb_expired.store.find('material_id', material_id));
        // if ((r.get('qty') - qty) < 0) {
        //     Ext.MessageBox.alert("Error", "Jumlah return tidak boleh melebihi jumlah barang yang telah diterima.");
        //     //if(this.btnEdit.text == 'Save' ) this.resetForm();
        //     return;
        // }
        // var tgl_expired = this.tgl_expired.getValue();
        var note = this.note.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('qty', qty);
            // record.set('sat', sat);
            // record.set('tgl_expired', tgl_expired);
            // record.set('no_lot', no_lot);
            // record.set('no_qc', no_qc);
            record.set('note_return', note);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    material_id: material_id,
                    qty: qty,
                    // sat: sat,
                    // tgl_expired: tgl_expired,
                    // no_lot: no_lot,
                    // no_qc: no_qc,
                    note_return: note
                });
            this.store.add(d);
        }
        this.resetForm();
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.material.setValue(record.data.material_id);
            // this.cmb_expired.setValue(record.data.tgl_expired);
            // this.cmb_expired.setRawValue(Ext.util.Format.date(this.cmb_expired.getValue(), 'd-m-Y'));
            // this.loadStoreCmbExpired(record.data.material_id);
            this.qty.setValue(record.data.qty);
            // this.sat.setValue(record.data.sat);
            // this.satuan.setText(this.sat.getValue());
            // this.tgl_expired.setValue(record.data.tgl_expired);
            // this.no_lot.setValue(record.data.no_lot);
            // this.no_qc.setValue(record.data.no_qc);
            this.note.setValue(record.data.note_return);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
