jun.PembayaranSupplierDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Terima Material Details",
    id: 'docs-jun.PembayaranSupplierDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'DPP',
            sortable: true,
            resizable: true,
            dataIndex: 'dpp',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'ppn',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Materai',
            sortable: true,
            resizable: true,
            dataIndex: 'materai',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 60,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                metaData.style += "background-color: #D4D4FF;";
                return Ext.util.Format.number(value, "0,0");
            }
        },
        {
            header: 'PPH',
            sortable: true,
            resizable: true,
            dataIndex: 'pph',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Dibayar',
            sortable: true,
            resizable: true,
            dataIndex: 'total_bayar',
            width: 60,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                metaData.style += "background-color: #AAD4FF;";
                return Ext.util.Format.number(value, "0,0");
            }
        },
        {
            header: 'No. Invoice / Nota Return',
            sortable: true,
            resizable: true,
            dataIndex: 'no_inv_supplier',
            width: 100,
            renderer: function (value, metaData, record, rowIndex) {
                return (!value ? record.get('no_nota_return') : value);
            }
        },
        {
            header: 'Tgl Invoice / Nota Return',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_inv_supplier',
            width: 80,
            renderer: function (value, metaData, record, rowIndex) {
                return (!value ? record.get('tgl_nota_return') : value);
            }
        }
    ],
    initComponent: function () {
        this.store = jun.rztPembayaranSupplierDetails;
        this.storeINV = new jun.ListInvoiceDanNotaReturnStore();
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 4,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: ' No. Invoice :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: this.storeINV,
                                valueField: 'temporary_id',
                                displayField: 'display_no',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<div style="width:100%;display:inline-block;"><span style="font-weight: bold">{display_no}</span><br>{display_tgl}</div>',
                                    "</div></tpl>"),
                                ref: '../../cmbNoINV'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Materai :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../materai',
                                style: 'margin-bottom:2px',
                                width: 70,
                                value: 0,
                                minValue: 0
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'small',
                            width: 40
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.PembayaranSupplierDetailsGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
        }
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
    },
    onStoreChange: function (s, b, d) {
        s.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    resetForm: function () {
        this.cmbNoINV.reset();
        this.materai.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item");
            return;
        }
        if (btn.text == 'Edit') {
            this.loadEditForm(record);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function (record) {
        this.cmbNoINV.setValue(record.data.terima_material_id);
        this.materai.setValue(record.data.materai);
    },
    loadForm: function () {
        var temporary_id = this.cmbNoINV.getValue();
        if (temporary_id == "") {
            Ext.MessageBox.alert("Error", " Anda belum memilih no Invoice.");
            return;
        }
        var r = this.storeINV.getAt(this.storeINV.findExact('temporary_id', temporary_id));
        var txt = !r.get('return_pembelian_id') ? "Invoice" : "Nota Return";
        var $err_msg = "No. " + txt + " ini sudah ditambahkan.<br>No. " + txt + " : " + r.get('display_no');
        if (this.store.find('temporary_id', temporary_id) != -1) {
            if (this.btnEdit.text == 'Save') {
                if (this.sm.getSelected().data.temporary_id != temporary_id) {
                    Ext.MessageBox.alert("Error", $err_msg);
                    this.resetForm();
                    return;
                }
            } else {
                Ext.MessageBox.alert("Error", $err_msg);
                this.resetForm();
                return;
            }
        }
        var terima_material_id = r.get('terima_material_id');
        var return_pembelian_id = r.get('return_pembelian_id');
        var no_inv_supplier = r.get('no_inv_supplier');
        var tgl_inv_supplier = r.get('tgl_inv_supplier');
        var no_nota_return = r.get('no_nota_return');
        var tgl_nota_return = r.get('tgl_nota_return');
        var dpp = r.get('dpp');
        var ppn = r.get('ppn');
        var pph = r.get('pph');
        var materai = parseFloat(this.materai.getValue() == "" ? 0 : this.materai.getValue());
        var total_bayar = dpp + ppn + materai;
        var total_bayar_after_pph = total_bayar - pph;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('terima_material_id', terima_material_id);
            record.set('no_inv_supplier', no_inv_supplier);
            record.set('tgl_inv_supplier', tgl_inv_supplier);
            record.set('return_pembelian_id', return_pembelian_id);
            record.set('no_nota_return', no_nota_return);
            record.set('tgl_nota_return', tgl_nota_return);
            record.set('dpp', dpp);
            record.set('ppn', ppn);
            record.set('pph', pph);
            record.set('materai', materai);
            record.set('total', total_bayar);
            record.set('total_bayar', total_bayar_after_pph);
            record.set('temporary_id', temporary_id);
            record.commit();
        } else {
            var c = jun.rztPembayaranSupplierDetails.recordType,
                d = new c({
                    terima_material_id: terima_material_id,
                    no_inv_supplier: no_inv_supplier,
                    tgl_inv_supplier: tgl_inv_supplier,
                    return_pembelian_id: return_pembelian_id,
                    no_nota_return: no_nota_return,
                    tgl_nota_return: tgl_nota_return,
                    dpp: dpp,
                    ppn: ppn,
                    pph: pph,
                    materai: materai,
                    total: total_bayar,
                    total_bayar: total_bayar_after_pph,
                    temporary_id: temporary_id
                });
            jun.rztPembayaranSupplierDetails.add(d);
        }
        this.resetForm();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
