jun.SjsimGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Retur Beli",
    id: 'docs-jun.SjsimGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. RB',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_id',
            width: 100,
            renderer: jun.renderCustomers
        }
        /*
         {
         header:'tgl',
         sortable:true,
         resizable:true,
         dataIndex:'tgl',
         width:100
         },
         {
         header:'persen_tax',
         sortable:true,
         resizable:true,
         dataIndex:'persen_tax',
         width:100
         },
         */
    ],
    initComponent: function () {
        //jun.rztBarangBatchCmp.reload();
        jun.rztSjsim.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglsjsimgridid').getValue()
                    }
                }
            }
        });
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        this.store = jun.rztSjsim;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Retur Beli',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Retur Beli',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglsjsimgridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.SjsimGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', function () {
            this.store.reload();
        }, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SjsimWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sjsim_id;
        var form = new jun.SjsimWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSjsimDetails.baseParams = {
            sjsim_id: idz
        };
        jun.rztSjsimDetails.load();
        jun.rztSjsimDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Sjsim/delete/id/' + record.json.sjsim_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSjsim.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
