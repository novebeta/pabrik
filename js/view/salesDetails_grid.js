jun.SalesDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "DetilPenjualan",
    id: 'docs-jun.SalesDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Code',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        // {
        //     header: 'Sat',
        //     resizable: true,
        //     dataIndex: 'sat',
        //     width: 100
        // },
        {
            header: 'Qty',
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc1',
            sortable: true,
            resizable: true,
            dataIndex: 'disc1',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc2',
            sortable: true,
            resizable: true,
            dataIndex: 'disc2',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc3',
            sortable: true,
            resizable: true,
            dataIndex: 'disc3',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Discount',
            sortable: true,
            resizable: true,
            dataIndex: 'totalpot',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Nominal',
            sortable: true,
            resizable: true,
            dataIndex: 'nominal',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSalesDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 9,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate(
                                '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode Barang</th><th>Nama Barang</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'
                            ),
                            width: 200,
                            allowBlank: false,
                            enableKeyEvents: true,
                            forceSelection: true,
                            listWidth: 400,
                            ref: '../../barang',
                            id: 'barangdetailsid',
                            displayField: 'nama_material'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stok'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 100,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'VAT :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            id: 'vatid',
                            style: 'margin-bottom:2px',
                            ref: '../../vat',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 200,
                            colspan: 2,
                            maxLength: 255
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc1 :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            style: 'margin-bottom:2px',
                            ref: '../../disc1',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc2 :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            style: 'margin-bottom:2px',
                            ref: '../../disc2',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc3 :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            style: 'margin-bottom:2px',
                            ref: '../../disc3',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SalesDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        // this.barang.on('select', this.onChangeBarang, this);
        this.barang.on('select', this.showStock, this);
        // this.sat.on('select', this.onChangeSat, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.price.on('keyup', this.onDiscChange, this);
        // this.disc.on('keyup', this.onDiscChange, this);
        // this.disca.on('keyup', this.onDiscChange, this);
        // this.barang.on('expand', this.afterRender_, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    showStock: function () {
        var material_id = this.barang.getValue();
        // var loc_code = this.loc_code.getValue();
        if (material_id != '') {
            jun.getQtyStock(material_id, null, this.stok);
        }
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
//        this.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            // this.onChangeBarang();
            this.qty.setValue(record.data.jml);
            this.price.setValue(record.data.price);
            this.disc.setValue(record.data.disc1);
            this.disca.setValue(record.data.pot);
            this.vat.setValue(record.data.vat);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty, 2);
        if (a.id == "discdetilid" || a.id == "priceid" || a.id == "qtyid") {
            this.disca.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    // onChangeBarang: function (c, r, i) {
    //     this.price.setValue(r.data.price);
    // },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        // kunci barang sama tidak boleh masuk dua kali
        // if (this.btnEdit.text != 'Save') {
        //     var a = jun.rztSalesDetail.findExact("barang_id", barang_id);
        //     if (a > -1) {
        //         Ext.MessageBox.alert("Error", "Item already entered");
        //         return
        //     }
        // }
        // var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var jml = parseFloat(this.qty.getValue());
        var disc1 = parseFloat(this.disc1.getValue());
        var disc2 = parseFloat(this.disc2.getValue());
        var disc3 = parseFloat(this.disc3.getValue());
        var vat = parseInt(this.vat.getValue());
        var ketpot = this.ketpot.getValue();
        var bruto = round(price * jml, 2);
        var pot1 = (disc1 / 100) * bruto;
        var pot2 = (disc2 / 100) * (bruto - pot1);
        var pot3 = (disc3 / 100) * (bruto - pot1 - pot2);
        var totalpot = pot1 + pot2 + pot3;
        var sub_total = bruto - totalpot;
        var vatrp = 0;
        if (vat > 0) {
            vatrp = (vat / 100) * sub_total;
        }
        var nominal = sub_total + vatrp;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('jml', jml);
            record.set('price', price);
            record.set('disc1', disc1);
            record.set('disc2', disc2);
            record.set('disc3', disc3);
            record.set('pot1', pot1);
            record.set('pot2', pot2);
            record.set('pot3', pot3);
            record.set('totalpot', totalpot);
            record.set('nominal', nominal);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('bruto', bruto);
            record.set('ketpot', ketpot);
            record.commit();
        } else {
            var c = jun.rztSalesDetail.recordType,
                d = new c({
                    barang_id: barang_id,
                    jml: jml,
                    price: price,
                    disc1: disc1,
                    disc2: disc2,
                    disc3: disc3,
                    pot1: pot1,
                    pot2: pot2,
                    pot3: pot3,
                    totalpot: totalpot,
                    nominal: nominal,
                    vat: vat,
                    vatrp: vatrp,
                    ketpot: ketpot,
                    bruto: bruto
                });
            jun.rztSalesDetail.add(d);
        }
        this.store.refreshData();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.disc1.reset();
        this.disc2.reset();
        this.disc3.reset();
        this.vat.reset();
        this.ketpot.reset();
    },
    // loadEditForm: function () {
    //     var selectedz = this.sm.getSelected();
    //     if (selectedz == "") {
    //         Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
    //         return;
    //     }
    //     var idz = selectedz.json.detil_penjualan;
    //     var form = new jun.DetilPenjualanWin({modez: 1, id: idz});
    //     form.show(this);
    //     form.formz.getForm().loadRecord(this.record);
    // },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus item ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
        this.store.refreshData()
    }
});
jun.ReturSalesDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "DetilPenjualan",
    id: 'docs-jun.ReturSalesDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Code',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        // {
        //     header: 'Sat',
        //     resizable: true,
        //     dataIndex: 'sat',
        //     width: 100
        // },
        {
            header: 'Qty',
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc1',
            sortable: true,
            resizable: true,
            dataIndex: 'disc1',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc2',
            sortable: true,
            resizable: true,
            dataIndex: 'disc2',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc3',
            sortable: true,
            resizable: true,
            dataIndex: 'disc3',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Discount',
            sortable: true,
            resizable: true,
            dataIndex: 'totalpot',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Nominal',
            sortable: true,
            resizable: true,
            dataIndex: 'nominal',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        },
        {
            header: 'Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztReturSalesDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 11,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate(
                                '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode Barang</th><th>Nama Barang</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'
                            ),
                            width: 200,
                            allowBlank: false,
                            enableKeyEvents: true,
                            forceSelection: true,
                            listWidth: 400,
                            ref: '../../barang',
                            id: 'barangdetailsid',
                            displayField: 'nama_material'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stok'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Gdg :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            ref: '../../loc_code',
                            forceSelection: true,
                            fieldLabel: 'loc_code',
                            store: jun.rztStorageLocationCmp,
                            valueField: 'loc_code',
                            width: 100,
                            displayField: 'loc_code'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'VAT :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            id: 'vatid',
                            style: 'margin-bottom:2px',
                            ref: '../../vat',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 200,
                            colspan: 4,
                            maxLength: 255
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc1 :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            style: 'margin-bottom:2px',
                            ref: '../../disc1',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc2 :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            style: 'margin-bottom:2px',
                            ref: '../../disc2',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc3 :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            style: 'margin-bottom:2px',
                            ref: '../../disc3',
                            width: 100,
                            readOnly: false,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnReturSalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.ReturSalesDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        // this.barang.on('select', this.onChangeBarang, this);
        this.barang.on('select', this.showStock, this);
        this.loc_code.on('select', this.showStock, this);
        // this.sat.on('select', this.onChangeSat, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.price.on('keyup', this.onDiscChange, this);
        // this.disc.on('keyup', this.onDiscChange, this);
        // this.disca.on('keyup', this.onDiscChange, this);
        // this.barang.on('expand', this.afterRender_, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    showStock: function () {
        var material_id = this.barang.getValue();
        var loc_code = this.loc_code.getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stok);
        }
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
//        this.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            // this.onChangeBarang();
            this.qty.setValue(record.data.jml);
            this.price.setValue(record.data.price);
            this.disc.setValue(record.data.disc1);
            this.disca.setValue(record.data.pot);
            this.vat.setValue(record.data.vat);
            this.loc_code.setValue(record.data.loc_code);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty, 2);
        if (a.id == "discdetilid" || a.id == "priceid" || a.id == "qtyid") {
            this.disca.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    // onChangeBarang: function (c, r, i) {
    //     this.price.setValue(r.data.price);
    // },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        // kunci barang sama tidak boleh masuk dua kali
        // if (this.btnEdit.text != 'Save') {
        //     var a = jun.rztReturSalesDetail.findExact("barang_id", barang_id);
        //     if (a > -1) {
        //         Ext.MessageBox.alert("Error", "Item already entered");
        //         return
        //     }
        // }
        // var barang = jun.getBarang(barang_id);
        var loc_code = this.loc_code.getValue();
        if (loc_code == "") {
            Ext.MessageBox.alert("Error", "Gudang harus dipilih.");
            return;
        }
        var price = parseFloat(this.price.getValue());
        var jml = parseFloat(this.qty.getValue());
        var disc1 = parseFloat(this.disc1.getValue());
        var disc2 = parseFloat(this.disc2.getValue());
        var disc3 = parseFloat(this.disc3.getValue());
        var vat = parseInt(this.vat.getValue());
        var ketpot = this.ketpot.getValue();
        var bruto = round(price * jml, 2);
        var pot1 = (disc1 / 100) * bruto;
        var pot2 = (disc2 / 100) * (bruto - pot1);
        var pot3 = (disc3 / 100) * (bruto - pot1 - pot2);
        var totalpot = pot1 + pot2 + pot3;
        var sub_total = bruto - totalpot;
        var vatrp = 0;
        if (vat > 0) {
            vatrp = (vat / 100) * sub_total;
        }
        var nominal = sub_total + vatrp;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('jml', jml);
            record.set('price', price);
            record.set('disc1', disc1);
            record.set('disc2', disc2);
            record.set('disc3', disc3);
            record.set('pot1', pot1);
            record.set('pot2', pot2);
            record.set('pot3', pot3);
            record.set('totalpot', totalpot);
            record.set('nominal', nominal);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('bruto', bruto);
            record.set('ketpot', ketpot);
            record.set('loc_code', loc_code);
            record.commit();
        } else {
            var c = jun.rztReturSalesDetail.recordType,
                d = new c({
                    barang_id: barang_id,
                    jml: jml,
                    price: price,
                    disc1: disc1,
                    disc2: disc2,
                    disc3: disc3,
                    pot1: pot1,
                    pot2: pot2,
                    pot3: pot3,
                    totalpot: totalpot,
                    nominal: nominal,
                    vat: vat,
                    vatrp: vatrp,
                    ketpot: ketpot,
                    bruto: bruto,
                    loc_code: loc_code
                });
            jun.rztReturSalesDetail.add(d);
        }
        this.store.refreshData();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.disc1.reset();
        this.disc2.reset();
        this.disc3.reset();
        this.vat.reset();
        this.ketpot.reset();
    },
    // loadEditForm: function () {
    //     var selectedz = this.sm.getSelected();
    //     if (selectedz == "") {
    //         Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
    //         return;
    //     }
    //     var idz = selectedz.json.detil_penjualan;
    //     var form = new jun.DetilPenjualanWin({modez: 1, id: idz});
    //     form.show(this);
    //     form.formz.getForm().loadRecord(this.record);
    // },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus item ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
        this.store.refreshData()
    }
});