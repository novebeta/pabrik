jun.CustomerPaymentDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "CustomerPaymentDetil",
    id: 'docs-jun.CustomerPaymentDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_id',
            renderer: jun.renderCustomers
        },
        {
            header: 'Remark',
            sortable: true,
            resizable: true,
            dataIndex: 'remark',
            width: 100
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Uang Muka',
            sortable: true,
            resizable: true,
            dataIndex: 'uang_muka',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Hutang Dibayar',
            sortable: true,
            resizable: true,
            dataIndex: 'uang_diterima',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total Hutang Dibayar',
            sortable: true,
            resizable: true,
            dataIndex: 'kas_diterima',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Lebih Bayar',
            sortable: true,
            resizable: true,
            dataIndex: 'lebih_bayar',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Sisa',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztCustomerPaymentDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            text: '\xA0No. Invoice:\xA0'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'doc_ref',
                                'no_faktur'
                            ],
                            mode: 'local',
                            store: jun.rztCustomerPaymentDetilCmp,
                            valueField: 'reference_item_id',
                            itemSelector: 'tr.search-item',
                            matchFieldWidth: !1,
                            tpl: new Ext.XTemplate(
                                '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Doc Ref</th><th>No Faktur</th><th>Tgl</th><th>Total</th><th>Sisa</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{doc_ref}</td><td>{no_faktur}</td><td>{tgl:date("d/m/Y")}</td><td>{kas_diterima:number("0,0.00")}</td><td>{sisa:number("0,0.00")}</td><td>{nama_barang}</td>',
                                '</tr></tpl></tbody></table>'
                            ),
                            width: 200,
                            allowBlank: false,
                            enableKeyEvents: true,
                            listWidth: 750,
                            ref: "../../jual",
                            lastQuery: "",
                            displayField: 'doc_ref'
                        },
                        {
                            xtype: 'label',
                            // hidden: true,
                            text: '\xA0Uang Muka :\xA0'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../uang_muka',
                            allowNegative: false,
                            value: 0,
                            // hidden: true,
                            width: 75
                        },
                        {
                            xtype: 'label',
                            // hidden: true,
                            text: '\xA0Disc :\xA0'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../disc',
                            allowNegative: false,
                            value: 0,
                            // hidden: true,
                            width: 75
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Hutang Dibayar :\xA0'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../kas',
                            allowNegative: false,
                            value: 0,
                            width: 75
                        },
                        {
                            xtype: 'label',
                            // hidden: true,
                            text: '\xA0Uang Lebih :\xA0'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../lebih_bayar',
                            allowNegative: false,
                            value: 0,
                            // hidden: true,
                            width: 75
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Remark :\xA0'
                        },
                        {
                            xtype: 'textfield',
                            //hidden:true,
                            name: 'remarkt',
                            id: 'remarktid',
                            ref: '../../remarkt',
                            maxLength: 100,
                            colspan: 9,
                            width: 400,
                            //allowBlank: ,
                            anchor: '100%'
                        },
                        // {
                        //     xtype: 'button',
                        //     text: 'Add',
                        //     ref: '../btnAdd'
                        // },
                        // {
                        //     xtype: 'tbseparator'
                        // },
                        // {
                        //     xtype: 'button',
                        //     text: 'Del',
                        //     ref: '../btnDel'
                        // }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    // id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        // {
                        //     xtype: 'button',
                        //     text: 'Edit',
                        //     height: 44,
                        //     ref: '../../btnEdit'
                        // },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDel'
                        }
                    ]
                }
            ]
        };
        jun.CustomerPaymentDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadEditForm, this);
        this.btnDel.on('Click', this.deleteRec, this);
        this.jual.on('select', this.selectJual, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    selectJual: function (combo, record, index) {
        this.kas.setValue(record.data.sisa);
        this.recordJual = record;
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CustomerPaymentDetilWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var jual1 = this.jual.getValue();
        if (jual1 == "") {
            Ext.MessageBox.alert("Error", "Faktur harus dipilih.");
            return
        }
        var kas = parseFloat(this.kas.getValue());
        // var uang = parseFloat(this.uang_diterima.getValue());
        //if (kas == 0) {
        //    Ext.MessageBox.alert("Error", "Kas dibayar tidak boleh 0.");
        //    return;
        //}
        var a = this.store.findExact("reference_item_id", this.recordJual.data.reference_item_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Faktur sudah diinput.");
            return;
        }
        var x = this.recordJual.data.doc_ref.split('0');
        var uang_diterima = kas;
        var remark = this.remarkt.getValue();
        var uang_muka = parseFloat(this.uang_muka.getValue());
        var disc = parseFloat(this.disc.getValue());
        var lebih_bayar = parseFloat(this.lebih_bayar.getValue());
        var kas_diterima = uang_muka + uang_diterima + disc;
        if (kas_diterima === 0) {
            Ext.MessageBox.alert("Error", "Total hutang yang dibayar tidak boleh nol.");
            return;
        }
        if (kas_diterima != 0 && round(Math.abs(kas_diterima),2) > round(Math.abs(this.recordJual.data.sisa),2)) {
            Ext.MessageBox.alert("Error", "Total hutang yang dibayar tidak boleh lebih dari sisa.");
            return;
        }
        var sisa = parseFloat(this.recordJual.data.sisa) - kas_diterima;
        var c = jun.rztCustomerPaymentDetil.recordType;
        d = new c({
            reference_item_id: this.recordJual.data.reference_item_id,
            customer_id: this.recordJual.data.customer_id,
            kas_diterima: kas_diterima,
            uang_muka: uang_muka,
            disc: disc,
            lebih_bayar: lebih_bayar,
            remark: remark,
            uang_diterima: uang_diterima,
            doc_ref: this.recordJual.data.doc_ref,
            tgl: this.recordJual.data.tgl,
            no_faktur: this.recordJual.data.no_faktur,
            payment_tipe: this.recordJual.data.payment_tipe,
            sisa: sisa,
            total: parseFloat(this.recordJual.data.kas_dibayar),
            total_sisa: parseFloat(this.recordJual.data.sisa)
        });
        jun.rztCustomerPaymentDetil.add(d);
        this.store.refreshData();
        this.jual.reset();
        this.kas.reset();
        this.disc.reset();
        this.lebih_bayar.reset();
        this.uang_muka.reset();
        this.remarkt.reset();
        jun.rztCustomerPaymentDetilCmp.remove(this.recordJual);
        this.recordJual = null;
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        record.data.kas_dibayar = record.data.total;
        record.data.sisa = record.data.total_sisa;
        jun.rztCustomerPaymentDetilCmp.add(record);
        this.store.remove(record);
        this.store.refreshData();
    }
});
