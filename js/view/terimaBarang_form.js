jun.TerimaBarangWin = Ext.extend(Ext.Window, {
    title: 'Surat Jalan Supplier',
    modez: 1,
    width: 950,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TerimaBarang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. GRN:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    // {
                    //     xtype: "label",
                    //     text: "Grup Barang:",
                    //     x: 295,
                    //     y: 5
                    // },
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     ref: '../grup',
                    //     forceSelection: true,
                    //     fieldLabel: 'Grup',
                    //     store: jun.rztGrupCmp,
                    //     emptyText: "ALL",
                    //     hiddenName: 'grup_id',
                    //     valueField: 'grup_id',
                    //     displayField: 'nama_grup',
                    //     allowBlank: true,
                    //     x: 400,
                    //     y: 2,
                    //     width: 175
                    // },
                    {
                        xtype: "label",
                        text: "Tanda Terima:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tanda_terima',
                        id: 'tanda_terimaid',
                        ref: '../tanda_terima',
                        maxLength: 100,
                        x: 400,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        allowBlank: false,
                        format: 'd M Y',
                        x: 85,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Gudang:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'loc_code',
                        id: 'loc_code_terima_barang_id',
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'loc_code',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        x: 400,
                        y: 2
                    },
                    // {
                    //     xtype: "label",
                    //     text: "Ongkos Kirim:",
                    //     x: 625,
                    //     y: 5
                    // },
                    // {
                    //     xtype: 'numericfield',
                    //     name: 'delivery',
                    //     id: 'deliveryid',
                    //     ref: '../delivery',
                    //     maxLength: 30,
                    //     value: 0,
                    //     x: 730,
                    //     y: 2
                    // },
                    new jun.TerimaBarangDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: 'hidden',
                        name: "delivery",
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        name: "po_id"
                    },
                    {
                        xtype: 'hidden',
                        name: "status",
                        value: 0
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TerimaBarangWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.grup.on('change', this.onGrupChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztTerimaBarangDetails.removeAll();
    },
    onGrupChange: function (combo, record, index) {
        var grup = combo.getValue();
        jun.rztBarangCmp.baseParams = {
            grup_id: grup
        };
        jun.rztBarangCmp.reload();
        jun.rztBarangCmp.baseParams = {};
        this.griddetils.barang.reset();
        this.griddetils.batch.reset();
        this.griddetils.tgl_exp.reset();
        this.griddetils.qty.reset();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'TerimaBarang/create/';
        Ext.getCmp('form-TerimaBarang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztTerimaBarangDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztTerimaBarang.reload();
                jun.rztPurchaseOrder.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TerimaBarang').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SupplierInvoiceWin = Ext.extend(Ext.Window, {
    title: 'Supplier Invoices',
    modez: 1,
    width: 925,
    height: 465,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SupplierInvoice',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_inv',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_inv',
                        ref: '../doc_ref_inv',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_inv',
                        name: 'tgl_inv',
                        value: DATE_NOW,
                        allowBlank: false,
                        format: 'd M Y',
                        x: 85,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Jatuh Tempo:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_tempo',
                        name: 'tgl_tempo',
                        // value: DATE_NOW,
                        allowBlank: false,
                        format: 'd M Y',
                        //readOnly: true,
                        x: 85,
                        y: 62,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Pajak:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_faktur_tax',
                        ref: '../no_faktur_tax',
                        maxLength: 50,
                        x: 400,
                        y: 2,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Invoice:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_invoice',
                        ref: '../no_invoice',
                        maxLength: 50,
                        x: 400,
                        y: 32,
                        width: 175
                    },
                    new jun.SupplierInvoiceDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 225,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 295,
                        y: 330
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        ref: '../total',
                        readOnly: true,
                        x: 400,
                        y: 328,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Disc:",
                        x: 295,
                        y: 360
                    },
                    {
                        xtype: 'numericfield',
                        name: 'discount',
                        ref: '../discount',
                        readOnly: true,
                        x: 400,
                        y: 358,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Tax:",
                        x: 615,
                        y: 330
                    },
                    {
                        xtype: 'numericfield',
                        name: 'tax',
                        ref: '../tax',
                        readOnly: true,
                        x: 715,
                        y: 328,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 615,
                        y: 360
                    },
                    {
                        xtype: 'numericfield',
                        name: 'grand_total',
                        ref: '../grand_total',
                        readOnly: true,
                        x: 715,
                        y: 358,
                        width: 175
                    },
                    {
                        xtype: 'hidden',
                        name: "status",
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        name: "terima_barang_id"
                    },
                    {
                        xtype: 'hidden',
                        name: "top",
                        ref: '../top'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SupplierInvoiceWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.tgl_inv.on('select', this.onSelectTglInv, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
    },
    onSelectTglInv: function (t, d) {
        var date = new Date();
        date.setDate(d.getDate() + parseInt(this.top.getValue()));
        // var tempo = jun.dateAdd(d.getDate(), parseInt(this.top.getValue()));
        this.tgl_tempo.setValue(date);
    },
    onWinClose: function () {
        jun.rztTerimaBarangDetails.removeAll();
        jun.rztPurchaseOrder.reload();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'TerimaBarang/update/';
        if (this.modez == 3) {
            urlz = 'TerimaBarang/revisiInvoice/';
        }
        Ext.getCmp('form-SupplierInvoice').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztTerimaBarang.reload();
                jun.rztSupplierInvoice.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-SupplierInvoice').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});