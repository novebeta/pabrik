jun.TandaTerimaMaterialDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Terima Material Details",
    id: 'docs-jun.TandaTerimaMaterialDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode  Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 140,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 80,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0.0")
        },
        // {
        //     header: 'Satuan',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'sat',
        //     width: 50
        // },
        // {
        //     header: 'Tgl Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_expired',
        //     width: 70,
        //     renderer: Ext.util.Format.dateRenderer('d-m-Y')
        // },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 120
        }
    ],
    initComponent: function () {
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 5,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Material :'
                            },
                            {
                                xtype: 'mfcombobox',
                                searchFields: [
                                    'kode_material',
                                    'nama_material'
                                ],
                                mode: 'local',
                                store: new jun.ReturnedItemsStore,
                                valueField: 'material_id',
                                itemSelector: 'tr.search-item',
                                tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                    '<th>Kode</th><th>Nama Material</th></tr></thead>',
                                    '<tbody><tpl for="."><tr class="search-item">',
                                    '<td>{kode_material}</td><td>{nama_material}</td>',
                                    '</tr></tpl></tbody></table>'),
                                allowBlank: false,
                                listWidth: 400,
                                width: 100,
                                ref: '../../material',
                                displayField: 'kode_material'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                width: 80,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Stok : 0',
                                ref: '../../stok'
                            },
                            // {
                            //     xtype: 'label',
                            //     style: 'margin:5px',
                            //     ref: '../../satuan',
                            //     text: 'Unit'
                            // },
                            // {
                            //     xtype: 'label',
                            //     style: 'margin:5px',
                            //     text: 'Exp :'
                            // },
                            // {
                            //     xtype: 'xdatefield',
                            //     ref: '../../tgl_expired',
                            //     name: 'tgl_expired',
                            //     colspan: 2,
                            //     format: 'd M Y'
                            // },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Note :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../note',
                                width: 307,
                                colspan: 4,
                                maxLength: 100
                            }//,
                            // {
                            //     xtype: 'hidden',
                            //     ref: '../../sat',
                            //     value: 'Unit'
                            // }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        id: 'btnsalesdetilid',
                        defaults: {
                            scale: 'large',
                            width: 45
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                height: 44,
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                height: 44,
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                height: 44,
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.TandaTerimaMaterialDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.material.on('select', this.onSelect, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    showStock: function () {
        var material_id = this.material.getValue();
        var loc_code = Ext.getCmp('tanda_terima_loc_code_id').getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stok);
        }
    },
    onSelect: function (c, r, i) {
        this.qty.reset();
        // this.satuan.setText(r.get('sat'));
        // this.sat.setValue(r.get('sat'));
        // this.tgl_expired.reset();
        this.note.reset();
        this.showStock();
    }
    ,
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    }
    ,
    resetForm: function () {
        this.material.reset();
        this.qty.reset();
        // this.sat.reset();
        // this.satuan.setText(this.sat.getValue());
        // this.tgl_expired.reset();
        this.note.reset();
    }
    ,
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.material.setValue(record.data.material_id);
            this.qty.setValue(record.data.qty);
            this.qty.setMaxValue(record.data.qty);
            // this.sat.setValue(record.data.sat);
            // this.satuan.setText(record.data.sat);
            // this.tgl_expired.setValue(record.data.tgl_expired);
            this.note.setValue(record.data.note);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    }
    ,
    loadForm: function () {
        var material_id = this.material.getValue();
        // var tgl_expired = this.tgl_expired.getValue();
        if (!material_id) {
            Ext.MessageBox.alert("Error", "Field Material dan Expired Date harus diisi.");
            return;
        }
        //err_msg = "Material ini sudah ditambahkan.<br>Kode material : " + this.material.getRawValue();
        // if (this.store.findBy(function (r) {
        //         return r.get('material_id') == material_id;
        //     }) != -1) {
        //     if (this.btnEdit.text == 'Save') {
        //         if (this.sm.getSelected().data.material_id != material_id) {
        //             Ext.MessageBox.alert("Error", $err_msg);
        //             this.resetForm();
        //             return;
        //         }
        //     } else {
        //         Ext.MessageBox.alert("Error", $err_msg);
        //         this.resetForm();
        //         return;
        //     }
        // }
        var qty = parseFloat(!this.qty.getValue() ? 0 : this.qty.getValue());
        if (qty > this.qty.maxValue) {
            Ext.MessageBox.alert("Error", "Qty tidak boleh lebih besar dari " + this.qty.maxValue);
            return;
        }
        // var sat = this.sat.getValue();
        var note = this.note.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('qty', qty);
            // record.set('sat', sat);
            // record.set('tgl_expired', tgl_expired);
            record.set('note', note);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    material_id: material_id,
                    qty: qty,
                    // sat: sat,
                    // tgl_expired: tgl_expired,
                    note: note
                });
            this.store.add(d);
        }
        this.resetForm();
    }
    ,
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    }
    ,
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
        this.commitChanges();
    }
});
