jun.BatchGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Production Order",
    id: 'docs-jun.BatchGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
//        {
//            header: 'Tanggal',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'tgl_produksi',
//            width: 100,
//            renderer: Ext.util.Format.dateRenderer('d M Y')
//        },
        {
            header: 'Nomor Batch',
            sortable: true,
            resizable: true,
            dataIndex: 'no_batch',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 80,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case BATCH_OPEN :
                        metaData.style += "background-color: #55AAFF;";
                        return 'OPEN';
                    case BATCH_PREPARED :
                        metaData.style += "background-color: #9BF79B;";
                        return 'PREPARED';
                    case BATCH_MIXED :
                        metaData.style += "background-color: #9BF79B;";
                        return 'MIXED';
                    case BATCH_FINISHED :
                        metaData.style += "background-color: #55AAFF;";
                        return 'FINISHED';
                    default :
                        return '-';
                }
            }
        },
        {
            header: 'Kode Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'bom_id',
            renderer: jun.renderBomKodeBarang,
            width: 120
        },
        {
            header: 'Nama Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'bom_id',
            renderer: jun.renderBomNamaBarang,
            width: 200
        },
        {
            header: 'Kode Formula',
            sortable: true,
            resizable: true,
            dataIndex: 'bom_id',
            renderer: jun.renderBomKodeFormula,
            width: 120
        },
        {
            header: 'Capacity',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            align: "right",
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(value, record.get('sat')), "0,0.00");
            },
            width: 80
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 30
        },
        {
            header: 'Tgl Mixing',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_mixing',
            width: 80,
            renderer: Ext.util.Format.dateRenderer('d-m-Y')
        },
        {
            header: 'Hasil Mixing',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_mixing',
            align: "right",
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                if (value > 0) metaData.style = "background-color: #CCFFCC; text-align: right;";
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(value, record.get('sat')), "0,0.00");
            },
            width: 80
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 30
        },
        {
            header: 'Qty Hasil',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_hasil',
            align: "right",
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                if (value > 0) metaData.style = "background-color: #CCFFCC; text-align: right;";
                return Ext.util.Format.number(value, '0,0');
            },
            width: 80
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat_hasil',
            width: 30
        }
    ],
    userPermission: function (actCreate, actEdit, actPrepare, actReturn, actMixing, actPrint, actConfirm, actConsume, actClose) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrepare = actPrepare;
        this.actReturn = actReturn;
        this.actMixing = actMixing;
        this.actPrint = actPrint;
        this.actConfirm = actConfirm;
        this.actConsume = actConsume;
        this.actClose = actClose;
        //this.viewAllData = (this.actCreatePO || this.actReject);
    },
    initComponent: function () {
        if (jun.rztBomLib.getTotalCount() === 0) jun.rztBomLib.load();
        if (jun.rztBomLibForCreateBatch.getTotalCount() === 0) jun.rztBomLibForCreateBatch.load();
        if (jun.rztBarangLib.getTotalCount() === 0) jun.rztBarangLib.load();
        if (jun.rztTipeMaterialLib.getTotalCount() === 0) jun.rztTipeMaterialLib.load();
        if (jun.rztMaterialForBOM.getTotalCount() === 0) jun.rztMaterialForBOM.load();
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1, 1, 1, 1, 1, 1, 1);
                break;
            case USER_PPIC:
                this.userPermission(1, 1, 1, 0, 0, 1, 0, 1, 1);
                break;
            case USER_GA:
                this.userPermission(0, 0, 0, 0, 0, 0, 0, 0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0, 0, 0, 0, 0, 0, 0, 0);
                break;
            case USER_PEMBELIAN:
                this.userPermission(0, 0, 0, 0, 0, 0, 0, 0, 0);
                break;
            case USER_RND:
                this.userPermission(0, 0, 0, 0, 0, 0, 0, 0, 0);
                break;
            case USER_PRODUKSI:
                this.userPermission(0, 0, 0, 1, 1, 0, 1, 1, 0);
                break;
            default:
                this.userPermission(0, 0, 0, 0, 0, 0, 0, 0, 0);
        }
        this.store = jun.rztBatch;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Prepare BB',
                    ref: '../btnPrepare',
                    iconCls: 'silk13-flag_green',
                    hidden: this.actPrepare ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Return BB',
                    ref: '../btnReturn',
                    hidden: this.actReturn ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Hasil Mixing',
                    ref: '../btnMixing',
                    hidden: this.actMixing ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Hasil Produksi',
                    ref: '../btnConfirm',
                    iconCls: 'silk13-package',
                    hidden: this.actConfirm ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Pemakaian BB',
                    ref: '../btnConsume',
                    hidden: this.actConsume ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actPrepare || this.actReturn || this.actPrint || this.actConfirm || this.actConsume) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>All</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_batch: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.BatchGrid').setFilterData(m.status_batch);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_batch: BATCH_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.BatchGrid').setFilterData(m.status_batch);
                                    }
                                }
                            },
                            {
                                text: 'PREPARED',
                                status_batch: BATCH_PREPARED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.BatchGrid').setFilterData(m.status_batch);
                                    }
                                }
                            },
                            {
                                text: 'MIXED',
                                status_batch: BATCH_MIXED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.BatchGrid').setFilterData(m.status_batch);
                                    }
                                }
                            },
                            {
                                text: 'FINISHED',
                                status_batch: BATCH_FINISHED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.BatchGrid').setFilterData(m.status_batch);
                                    }
                                }
                            }
                        ]
                    }
                },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Nomor Batch</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'no_batch',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Nomor Batch',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'no_batch';
                                        Ext.getCmp('docs-jun.BatchGrid').txtFind.focus().setValue("");
                                    }
                                }
//                            },
//                            {
//                                text: 'Kode Produk',
//                                listeners: {
//                                    click: function(m, e){
//                                        m.parentMenu.ownerCt.setText('<b>'+m.text+'</b>');
//                                        m.parentMenu.ownerCt.fieldsearch = 'pbu_barang.kode_barang_rnd';
//                                        Ext.getCmp('docs-jun.BatchGrid').txtFind.focus().setValue("");
//                                    }
//                                }
//                            },
//                            {
//                                text: 'Nama Produk',
//                                listeners: {
//                                    click: function(m, e){
//                                        m.parentMenu.ownerCt.setText('<b>'+m.text+'</b>');
//                                        m.parentMenu.ownerCt.fieldsearch = 'pbu_barang.nama_barang';
//                                        Ext.getCmp('docs-jun.BatchGrid').txtFind.focus().setValue("");
//                                    }
//                                }
//                            },
//                            {
//                                text: 'Kode Formula',
//                                listeners: {
//                                    click: function(m, e){
//                                        m.parentMenu.ownerCt.setText('<b>'+m.text+'</b>');
//                                        m.parentMenu.ownerCt.fieldsearch = 'pbu_bom.kode_formula';
//                                        Ext.getCmp('docs-jun.BatchGrid').txtFind.focus().setValue("");
//                                    }
//                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportBatchGrid",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "batch_id",
                            ref: "../../batch_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.BatchGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrepare.on('Click', this.btnPrepareOnClick, this);
        this.btnReturn.on('Click', this.btnReturnOnClick, this);
        this.btnPrint.on('Click', this.btnPrintOnClick, this);
        this.btnMixing.on('Click', this.btnMixingOnClick, this);
        this.btnConfirm.on('Click', this.btnConfirmOnClick, this);
        this.btnConsume.on('Click', this.btnConsumeOnClick, this);
        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData('all');
        this.status_batch = 'all';
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (this.actEdit) {
            this.btnEdit.setIconClass(r.get('status') == BATCH_OPEN ? 'silk13-pencil' : 'silk13-eye');
            this.btnEdit.setText(r.get('status') == BATCH_OPEN ? "Ubah" : "Lihat");
        }
    },
    setFilterData: function (sts) {
        this.status_batch = sts;
        if (this.store.baseParams.status && this.store.baseParams.status == sts) {
            this.store.baseParams = {
                mode: "grid",
                status: this.status_batch,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                mode: "grid",
                status: this.status_batch,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.moveFirst();
        }
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.fieldsearch = this.btnFind.fieldsearch;
            this.valuesearch = c.getValue();
            this.store.baseParams = {
                mode: "grid",
                status: this.status_batch,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.moveFirst();
        }
    },
    loadForm: function () {
        var form = new jun.BatchWin({
            modez: 0,
            title: "Buat Production Order",
            iconCls: 'silk13-add',
            //storeDetail: jun.rztCreateBatchDetail,
            recordBOM: null,
            qty_capacity: 1000
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        var filter = selectedz.json.status == BATCH_OPEN && this.actEdit;
        var batch_id = selectedz.json.batch_id;
        var rBom = jun.rztBomLib.getAt(jun.rztBomLib.findExact("bom_id", selectedz.json.bom_id));
        var ttl = "";
        switch (Number(selectedz.json.status)) {
            case BATCH_OPEN :
                ttl = "OPEN";
                break;
            case BATCH_PREPARED :
                ttl = "PREPARED";
                break;
            case BATCH_MIXED :
                ttl = "MIXED";
                break;
            case BATCH_FINISHED :
                ttl = "FINISHED";
                break;
        }
        var form = new jun.BatchWin({
            modez: ( filter ? 1 : 2),
            batch_id: batch_id,
            //storeDetail: jun.rztBatchDetail,
            recordBOM: rBom,
            qty_capacity: this.record.get('qty'),
            title: ( filter ? "Ubah" : "Lihat") + " Production Order [" + ttl + "]",
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.capacity.setValue(jun.konversiSatuanDitampilkan(this.record.get('qty'), this.record.get('sat')));
        form.produk.setText(rBom.get('kode_barang_rnd') + " - " + rBom.get('nama_barang'));
        form.kode_formula.setText(rBom.get('kode_formula'));
        form.griddetil.store.load({
            params: {
                batch_id: batch_id
            }
        });
    },
    btnPrepareOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status == BATCH_FINISHED) {
            Ext.MessageBox.alert("Warning", "Tidak dapat melakukan persiapan untuk Batch ini.<br>Batch telah ditutup.");
            return;
        }
        this.prepareBahanBaku();
    },
    prepareBahanBaku: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        var filter = selectedz.json.status != BATCH_FINISHED && this.actPrepare;
        var batch_id = selectedz.json.batch_id;
//        var rBom = jun.rztBomLib.getAt(jun.rztBomLib.findExact("bom_id", selectedz.json.bom_id));
        var form = new jun.TransferMaterialWin({
            modez: 0,
            batch_id: batch_id,
            title: "Prepare Bahan Baku",
            iconCls: 'silk13-flag_green',
            storeDetail: jun.rztBatchDetail,
            storeComboMaterial: new jun.Materialstore({baseParams: {bom_id: selectedz.json.bom_id}}),
            tglExpiredStore: new jun.MaterialExpDateStore(),
            accepting: false
        });
        form.show(this);
//        form.formz.getForm().loadRecord(this.record);
        form.penerima.setValue(USER_PRODUKSI);
        form.penerima.setReadOnly(true);
        form.storeComboMaterial.load();
        form.note.setValue("Prepare (" + selectedz.json.no_batch + ")");
        if (selectedz.json.status == BATCH_OPEN) {
            form.griddetil.store.load({
                params: {
                    batch_id: batch_id
                }
            });
        }
    },
    btnReturnOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status == BATCH_OPEN) {
            Ext.MessageBox.alert("Warning", "Tidak dapat melakukan return.<br>Belum ada persiapan bahan baku untuk Batch ini.");
            return;
        }
        if (status == BATCH_FINISHED) {
            Ext.MessageBox.alert("Warning", "Tidak dapat melakukan return.<br>Batch telah ditutup.");
            return;
        }
        this.returnBahanBaku();
    },
    returnBahanBaku: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        var filter = selectedz.json.status != BATCH_FINISHED && this.actPrepare;
        var batch_id = selectedz.json.batch_id;
//        var rBom = jun.rztBomLib.getAt(jun.rztBomLib.findExact("bom_id", selectedz.json.bom_id));
        var form = new jun.TransferMaterialWin({
            modez: 0,
            batch_id: batch_id,
            storeDetail: jun.rztBatchDetail,
            storeComboMaterial: new jun.Materialstore({baseParams: {bom_id: selectedz.json.bom_id}}),
            tglExpiredStore: new jun.MaterialExpDateStore({url: 'Material/ExpiredDateReturnBahanBaku'}),
            title: "Return Bahan Baku"
        });
        form.show(this);
//        form.formz.getForm().loadRecord(this.record);
        form.penerima.setValue(USER_PPIC);
        form.penerima.setReadOnly(true);
        form.storeComboMaterial.load();
        form.note.setValue("Return (" + selectedz.json.no_batch + ")");
    },
    btnPrintOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        Ext.getCmp("form-ReportBatchGrid").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBatchGrid").getForm().url = "Report/PrintBatch";
        this.batch_id.setValue(selectedz.json.batch_id);
        var form = Ext.getCmp('form-ReportBatchGrid').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnMixingOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Batch.");
            return;
        }
        var filter = (selectedz.json.status == BATCH_PREPARED || selectedz.json.status == BATCH_MIXED) && this.actMixing;
        var form = new jun.BatchInputMixingWin({
            modez: ( filter ? 1 : 2),
            batch_id: selectedz.json.batch_id,
            sat: this.record.get('sat')
        });
        form.show(this);
        if (parseFloat(selectedz.json.qty_mixing) > 0) form.formz.getForm().loadRecord(this.record);
        form.no_batch.setText(': ' + this.record.json.no_batch);
        form.kode_produk.setText(': ' + jun.renderBomKodeBarang(this.record.json.bom_id));
        form.nama_produk.setText(': ' + jun.renderBomNamaBarang(this.record.json.bom_id));
        form.kode_formula.setText(': ' + jun.renderBomKodeFormula(this.record.json.bom_id));
        form.qty.setText(': '
            + formatNumber(jun.konversiSatuanDitampilkan(this.record.get('qty'), this.record.get('sat')))
            + ' '
            + this.record.get('sat'));
        form.qty_mixing.setValue(jun.konversiSatuanDitampilkan(this.record.get('qty_mixing'), this.record.get('sat')));
        form.sat_mixing.setText(' ' + this.record.get('sat'));
    },
    btnConfirmOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        var filter = selectedz.json.status == BATCH_MIXED && this.actConfirm;
        var batch_id = selectedz.json.batch_id;
        var rBom = jun.rztBomLib.getAt(jun.rztBomLib.findExact("bom_id", selectedz.json.bom_id));
        var form = new jun.BatchInputHasilWin({
            modez: ( filter ? 1 : 2),
            batch_id: batch_id,
            barang_id: rBom.get('barang_id'),
            recordBatch: this.record,
            title: ( filter ? "Edit" : "View") + " Hasil Produksi",
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.produk.setText(rBom.get('kode_barang_rnd') + " - " + rBom.get('nama_barang'));
        form.kode_formula.setText(rBom.get('kode_formula'));
        form.final.setValue(selectedz.json.status == BATCH_FINISHED ? true : false);
        form.qty.setValue(jun.konversiSatuanDitampilkan(this.record.get('qty'), this.record.get('sat')));
        form.satuan.setText(this.record.get('sat'));
        form.griddetil.store.load({
            params: {
                batch_id: batch_id
            }
        });
    },
    btnConsumeOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih data.");
            return;
        }
        var batch_id = selectedz.json.batch_id;
        var rBom = jun.rztBomLib.getAt(jun.rztBomLib.findExact("bom_id", selectedz.json.bom_id));
        var form = new jun.BatchKonsumsiWin({
            title: "Pemakaian Bahan Baku",
            iconCls: 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.produk.setText(rBom.get('kode_barang_rnd') + " - " + rBom.get('nama_barang'));
        form.kode_formula.setText(rBom.get('kode_formula'));
        form.qty.setValue(jun.konversiSatuanDitampilkan(this.record.get('qty'), this.record.get('sat')));
        form.sat.setText(this.record.get('sat'));
        form.griddetil.store.load({
            params: {
                batch_id: batch_id
            }
        });
    }
});
