jun.ConversiMaterialDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ConversiMaterialDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ConversiMaterialDetailStoreId',
            url: 'ConversiMaterialDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'conversi_material_detail_id'},
                {name: 'conversi_material_barkas_id'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.rztConversiMaterialDetail = new jun.ConversiMaterialDetailstore();
jun.ConversiBarkasDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ConversiBarkasDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ConversiBarkasDetailStoreId',
            url: 'ConversiBarkasDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'conversi_barkas_detail_id'},
                {name: 'conversi_material_barkas_id'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.rztConversiBarkasDetail = new jun.ConversiBarkasDetailstore();