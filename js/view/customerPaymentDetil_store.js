jun.CustomerPaymentDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CustomerPaymentDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomerPaymentDetilStoreId',
            url: 'CustomerPaymentDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'customer_payment_detil_id'},
                {name: 'kas_diterima', type: 'float'},
                {name: 'sisa', type: 'float'},
                {name: 'total_sisa', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'payment_tipe'},
                {name: 'reference_item_id'},
                {name: 'customer_payment_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'remark'},
                {name: 'uang_diterima', type: 'float'},
                {name: 'customer_id'},
                {name: 'disc', type: 'float'},
                {name: 'lebih_bayar', type: 'float'},
                {name: 'uang_muka', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var uang_diterima_tot = this.sum("uang_diterima");
        var lebih_bayar_tot = this.sum("lebih_bayar");
        Ext.getCmp("total_customer_payment_id").setValue(uang_diterima_tot + lebih_bayar_tot);
        // var totalditerima = this.sum("uang_diterima");
        // Ext.getCmp("uang_diterima_id").setValue(totalditerima);
    }
});
jun.rztCustomerPaymentDetil = new jun.CustomerPaymentDetilstore();
jun.rztCustomerPaymentDetilCmp = new jun.CustomerPaymentDetilstore();
