jun.TipeMaterialWin = Ext.extend(Ext.Window, {
    title: 'Tipe Material',
    modez: 1,
    width: 400,
    height: 315,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TipeMaterial',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Tipe',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_tipe_material',
                        id: 'nama_tipe_materialid',
                        ref: '../nama_tipe_material',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    new jun.cmbJenisMaterial({
                        fieldLabel: 'Jenis',
                        hideLabel: false,
                        //hidden:true,
                        hiddenName: 'jenis',
                        id: 'jenisid',
                        ref: '../jenis',
                        //allowBlank: ,
                        anchor: '100%'
                    }),
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        columnWidth: .33,
                        boxLabel: "Have Stok",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "have_stock"
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'COA Jual',
                        hideLabel: false,
                        //hidden:true,
                        name: 'coa_jual',
                        id: 'coa_jualid',
                        ref: '../coa_jual',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'COA Beli',
                        hideLabel: false,
                        //hidden:true,
                        name: 'coa_beli',
                        id: 'coa_beliid',
                        ref: '../coa_beli',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'COA Persediaan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'coa_persediaan',
                        id: 'coa_persediaanid',
                        ref: '../coa_persediaan',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'COA HPP',
                        hideLabel: false,
                        //hidden:true,
                        name: 'coa_hpp',
                        id: 'coa_hppid',
                        ref: '../coa_hpp',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'COA Retur Jusl',
                        hideLabel: false,
                        //hidden:true,
                        name: 'coa_retur_jual',
                        id: 'coa_retur_jualid',
                        ref: '../coa_retur_jual',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'COA Disc Jual',
                        hideLabel: false,
                        //hidden:true,
                        name: 'coa_jual_disc',
                        id: 'coa_jual_discid',
                        ref: '../coa_jual_disc',
                        maxLength: 15,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TipeMaterialWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'TipeMaterial/update/id/' + this.id;
        } else {
            urlz = 'TipeMaterial/create/';
        }
        Ext.getCmp('form-TipeMaterial').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztTipeMaterial.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TipeMaterial').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});