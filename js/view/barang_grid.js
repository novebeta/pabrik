jun.BarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Barang",
    id: 'docs-jun.BarangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang (R&D)',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang_rnd',
            width: 80
        },
        {
            header: 'Kode Barang (Customer)',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 80
        },
        {
            header: 'Kode Barang 3',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang_2',
            width: 80
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 120
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 40
        },
        {
            header: 'Tipe Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_barang_id',
            width: 60,
            renderer: jun.renderTipeBarang
        }
        /*
         {
         header:'qty_per_box',
         sortable:true,
         resizable:true,
         dataIndex:'qty_per_box',
         width:100
         },
         */
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        this.store = jun.rztBarang;
        if (jun.rztGrupCmp.getTotalCount() === 0) jun.rztGrupCmp.load();
        if (jun.rztTipeBarangCmp.getTotalCount() === 0) jun.rztTipeBarangCmp.load();
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1);
                break;
            case USER_GPJ:
                this.userPermission(0, 1, 0);
                break;
            case USER_PPIC:
                this.userPermission(0, 0, 0);
                break;
            case USER_GA:
                this.userPermission(0, 0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0, 0);
                break;
            case USER_PEMBELIAN:
                this.userPermission(0, 0, 0);
                break;
            case USER_RND:
                this.userPermission(1, 1, 0);
                break;
            default:
                this.userPermission(0, 0, 0);
        }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Barang',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: this.actDelete ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actDelete || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Filter : <b>All Barang</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        ref: '../../menuFilter',
                        id: 'BarangGrid_menuFilterid',
                        items: [
                            {
                                text: 'All Barang',
                                tipe_barang_id: 0,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Filter : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.BarangGrid').setFilterData(m.tipe_barang_id);
                                    }
                                }
                            }
                        ]
                    }
                },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Kode Barang (R&D)</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'kode_barang_rnd',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Kode Barang (R&D)',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_barang_rnd';
                                        Ext.getCmp('docs-jun.BarangGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Kode Barang (Customer)',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_barang';
                                        Ext.getCmp('docs-jun.BarangGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Kode Barang 3',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_barang_2';
                                        Ext.getCmp('docs-jun.BarangGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Nama Barang',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'nama_barang';
                                        Ext.getCmp('docs-jun.BarangGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Note',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'ket';
                                        Ext.getCmp('docs-jun.BarangGrid').txtFind.focus().setValue("");
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                }
            ]
        };
        var tipeBarangStore = new jun.TipeBarangstore();
        tipeBarangStore.load({
            scope: this,
            callback: this.addMenuFilter
        });
        this.store.baseParams = {mode: "grid"}; // untuk pagination
        this.store.reload();
        jun.BarangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tipe_barang_id_filter = 0;
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    addMenuFilter: function (recarray) {
        for (i = 0; i < recarray.length; i++) {
            r = recarray[i];
            Ext.getCmp('BarangGrid_menuFilterid').add({
                text: r.data.nama_tipe_barang,
                tipe_barang_id: r.data.tipe_barang_id,
                listeners: {
                    click: function (m, e) {
                        m.parentMenu.ownerCt.setText('Filter : <b>' + m.text + '</b>');
                        Ext.getCmp('docs-jun.BarangGrid').setFilterData(m.tipe_barang_id);
                    }
                }
            });
        }
    },
    setFilterData: function (n) {
        this.tipe_barang_id_filter = n;
        if (this.store.baseParams.tipe_barang_id && parseInt(this.store.baseParams.tipe_barang_id) == n) {
            this.store.baseParams = {
                tipe_barang_id: this.tipe_barang_id_filter,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                tipe_barang_id: this.tipe_barang_id_filter,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.moveFirst();
        }
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.fieldsearch = this.btnFind.fieldsearch;
            this.valuesearch = c.getValue();
            this.store.baseParams = {
                tipe_barang_id: this.tipe_barang_id_filter,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BarangWin({
            modez: 0,
            title: "Buat Data Barang Baru",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Barang");
            return;
        }
        var idz = selectedz.json.barang_id;
        var form = new jun.BarangWin({
            modez: this.actEdit ? 1 : 2,
            id: idz,
            title: (this.actEdit ? "Ubah" : "Lihat") + " Data Barang : " + selectedz.json.kode_barang,
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Barang.");
            return;
        }
        Ext.MessageBox.confirm(
            'Pertanyaan', "Apakah anda yakin ingin menghapus data Barang ini?<br><br>Kode barang : " + record.json.kode_barang + "<br>Nama barang : " + record.json.nama_barang,
            this.deleteRecYes,
            this
        );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Barang/delete/id/' + record.json.barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBarang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
