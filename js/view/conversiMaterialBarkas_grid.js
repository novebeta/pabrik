jun.ConversiMaterialBarkasGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Konversi Sparepart > Barang Bekas",
    id: 'docs-jun.ConversiMaterialBarkasGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Ref.',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 80
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 250
        }
    ],
    userPermission: function (actCreate, actEdit, actPrint) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
    },
    initComponent: function () {
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztMaterialSparepartBekas.getTotalCount() === 0) {
            jun.rztMaterialSparepartBekas.load();
        }
        if (jun.rztBarangBekas.getTotalCount() === 0) {
            jun.rztBarangBekas.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztUnitOfMeasureLib.getTotalCount() === 0) {
            jun.rztUnitOfMeasureLib.load();
        }
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1);
                break;
            case USER_GA:
                this.userPermission(1, 1, 1);
                break;
            default:
                this.userPermission(0, 0, 0);
        }
        this.store = jun.rztConversiMaterialBarkas;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Konversi',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
//                {
//                    xtype: 'tbseparator',
//                    hidden: (this.actCreate || this.actEdit || this.actView)?false:true
//                },
//                {
//                    xtype: 'button',
//                    text: 'Print Tanda Terima',
//                    ref: '../btnPrint',
//                    iconCls: 'silk13-page_white_excel',
//                    hidden: this.actPrint?false:true
//                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportConversiMaterialBarkas",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "sparepart_masuk_id",
                            ref: "../../sparepart_masuk_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.ConversiMaterialBarkasGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.btnAddOnClick, this);
        this.btnEdit.on('Click', this.btnEditOnClick, this);
//        this.btnPrint.on('Click', this.btnPrintPROnClick, this);
        this.on('rowdblclick', this.btnEditOnClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnAddOnClick: function () {
        var form = new jun.ConversiMaterialBarkasWin({
            modez: 0,
            title: "Buat Konversi Sparepart",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    btnEditOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        var form = new jun.ConversiMaterialBarkasWin({
            modez: 1,
            conversi_material_barkas_id: selectedz.json.conversi_material_barkas_id,
            title: "Ubah" + " Konversi Sparepart",
            iconCls: 'silk13-pencil'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztConversiMaterialDetail.baseParams = {conversi_material_barkas_id: selectedz.json.conversi_material_barkas_id};
        jun.rztConversiMaterialDetail.load();
        jun.rztConversiMaterialDetail.baseParams = {};
        jun.rztConversiBarkasDetail.baseParams = {conversi_material_barkas_id: selectedz.json.conversi_material_barkas_id};
        jun.rztConversiBarkasDetail.load();
        jun.rztConversiBarkasDetail.baseParams = {};
//    },
//    btnPrintPROnClick: function(){
//        var selectedz = this.sm.getSelected();
//        if (selectedz == undefined) {
//            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
//            return;
//        }
//        Ext.getCmp("form-ReportConversiMaterialBarkas").getForm().standardSubmit = !0;
//        Ext.getCmp("form-ReportConversiMaterialBarkas").getForm().url = "Report/PrintTandaTerimaConversiMaterialBarkas";
//        this.sparepart_masuk_id.setValue(selectedz.json.sparepart_masuk_id);
//        var form = Ext.getCmp('form-ReportConversiMaterialBarkas').getForm();
//        var el = form.getEl().dom;
//        var target = document.createAttribute("target");
//        target.nodeValue = "myFrame";
//        el.setAttributeNode(target);
//        el.action = form.url;
//        el.submit();
    }
});
