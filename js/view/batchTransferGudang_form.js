jun.BatchTransferGudangWin = Ext.extend(Ext.Window, {
    title: 'Input Hasil Produksi',
    width: 500,
    height: 250,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-BatchTransferGudangWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label", text: "Kode Produk",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        id: 'kode_produk',
                        x: 100,
                        y: 5
                    },
                    {
                        xtype: "label", text: "Nama Produk",
                        x: 5,
                        y: 22
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        id: 'nama_produk',
                        x: 100,
                        y: 22
                    },
                    {
                        xtype: "label", text: "Kode Formula",
                        x: 5,
                        y: 39
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        id: 'kode_formula',
                        x: 100,
                        y: 39
                    },
                    {
                        xtype: "label", text: "Tanggal Produksi",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        id: 'tgl_produksi',
                        x: 100,
                        y: 65
                    },
                    {
                        xtype: "label", text: "No. Batch",
                        x: 5,
                        y: 82
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        id: 'no_batch',
                        x: 100,
                        y: 82
                    },
                    {
                        xtype: "label",
                        text: "Quantity",
                        x: 5,
                        y: 99
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        id: 'qty',
                        x: 100,
                        y: 99
                    },
                    {
                        xtype: "label",
                        text: "Quantity Hasil",
                        x: 5,
                        y: 116
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        id: 'qty_hasil',
                        x: 100,
                        y: 116
                    },
                    {
                        xtype: "label",
                        text: "Tanggal Transfer Gudang",
                        x: 5,
                        y: 146
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../../tgl_transfer_gudang',
                        id: 'tgl_transfer_gudang',
                        format: 'd M Y',
                        x: 140,
                        y: 146 - 3
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BatchTransferGudangWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-BatchTransferGudangWin').getForm().submit({
            url: 'Batch/transferGudang/id/' + this.batch_id,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBatchProduksi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});