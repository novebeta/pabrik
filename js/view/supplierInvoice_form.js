jun.InvoiceSupplierWin = Ext.extend(Ext.Window, {
    title: 'Invoice Supplier',
    modez: 1,
    width: 900,
    height: 550,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-InvoiceSupplierWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
//                    {
//                        xtype: "label",
//                        text: "Doc Ref.:",
//                        x: 5,
//                        y: 5
//                    },
//                    {
//                        xtype: 'textfield',
//                        name: 'doc_ref_inv',
//                        ref: '../doc_ref_inv',
//                        readOnly: true,
//                        width: 175,
//                        x: 5+80,
//                        y: 2
//                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_terima_inv',
                        fieldLabel: 'tgl_terima_inv',
                        name: 'tgl_terima_inv',
                        allowBlank: false,
                        format: 'd M Y',
                        value: new Date(),
                        x: 5 + 80,
                        y: 2,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        ref: '../cmbSupplier',
                        x: 5 + 80,
                        y: 32,
                        allowBlank: false,
                        readOnly: this.tipeInvoice == TIPE_INV_TANPA_SJ ? false : true,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "T.O.P.:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: false,
                        store: jun.rztTermOfPaymentStore,
                        hiddenName: 'term_of_payment',
                        valueField: 'val',
                        displayField: 'val',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{val}</b><br>{des}</span>',
                            "</div></tpl>"),
                        value: '',
                        ref: '../term_of_payment',
                        id: 'term_of_payment',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 80,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Currency :",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: false,
                        store: jun.rztCurrencyStore,
                        hiddenName: 'currency',
                        valueField: 'val',
                        displayField: 'val',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{val}</b><br>{des}</span>',
                            "</div></tpl>"),
                        value: 'IDR',
                        maxLength: 3,
                        ref: '../currency',
                        allowBlank: false,
                        readOnly: true,
                        width: 175,
                        x: 5 + 80,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "No. Invoice:",
                        x: 5 + 80 + 210,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'no_inv_supplier',
                        id: 'no_inv_supplier',
                        ref: '../no_inv_supplier',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 80 + 210 + 90,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tgl. Invoice:",
                        x: 5 + 80 + 210,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_inv_supplier',
                        name: 'tgl_inv_supplier',
                        allowBlank: false,
                        format: 'd M Y',
                        x: 5 + 80 + 210 + 90,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Faktur Pajak:",
                        x: 5 + 80 + 210,
                        y: 65
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'no_faktur_pajak',
                        id: 'no_faktur_pajak',
                        ref: '../no_faktur_pajak',
                        allowBlank: this.tipeInvoice == TIPE_INV_TANPA_SJ ? true : false,
                        width: 175,
                        x: 5 + 80 + 210 + 90,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tgl Faktur Pajak:",
                        x: 5 + 80 + 210,
                        y: 95
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_no_faktur_pajak',
                        name: 'tgl_no_faktur_pajak',
                        allowBlank: this.tipeInvoice == TIPE_INV_TANPA_SJ ? true : false,
                        format: 'd M Y',
                        x: 5 + 80 + 210 + 90,
                        y: 92,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Kurs:",
                        hidden: true,
                        x: 5 + 80 + 210 + 90 + 210,
                        y: 35
                    },
                    {
                        xtype: 'numericfield',
                        name: 'IDR_rate',
                        ref: '../IDR_rate',
                        allowBlank: false,
                        hidden: true,
                        width: 175,
                        value: 1,
                        x: 5 + 80 + 210 + 90 + 210 + 90,
                        y: 32
                    },
                    new jun.InvoiceSupplierDetailsGrid({
                        x: 5,
                        y: 125,
                        height: 225,
                        frameHeader: !1,
                        //anchor: "100% 100%",
                        header: !1,
                        ref: "../griddetils",
                        store: this.storeDetail,
                        tipeInvoice: this.tipeInvoice,
                        readOnly: ((this.modez < 2) ? false : true)
                    }),
                    {
                        xtype: "label",
                        text: "Total Excl. Tax:",
                        x: 570 - 350,
                        y: 125 + 225 + 10
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        name: 'total',
                        ref: '../total',
                        id: 'form-InvoiceSupplier-total',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690 - 350,
                        y: 125 + 225 + 10 - 3
                    },
                    {
                        xtype: "label",
                        text: "Discount:",
                        x: 570 - 350,
                        y: 125 + 225 + 10 + 30
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        enableKeyEvents: true,
                        name: 'discount',
                        ref: '../discount',
                        id: 'form-InvoiceSupplier-discount',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690 - 350,
                        y: 125 + 225 + 10 - 3 + 30
                    },
                    {
                        xtype: "label",
                        text: "DPP:",
                        x: 570 - 350,
                        y: 125 + 225 + 10 + 60
                    },
                    {
                        xtype: 'numericfield',
                        id: 'form-InvoiceSupplier-persen_dpp',
                        name: 'persen_dpp',
                        ref: '../persen_dpp',
                        enableKeyEvents: true,
                        allowBlank: false,
                        //hidden: this.downPayment?false:true,
                        width: 40,
                        value: 100,
                        x: 570 - 350 + 60,
                        y: 125 + 225 + 10 - 3 + 60
                    },
                    {
                        xtype: "label",
                        text: "%",
                        //hidden: this.downPayment?false:true,
                        x: 570 - 350 + 60 + 40 + 5,
                        y: 125 + 225 + 10 + 60
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        name: 'dpp',
                        ref: '../dpp',
                        id: 'form-InvoiceSupplier-dpp',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690 - 350,
                        y: 125 + 225 + 10 - 3 + 60
                    },
                    {
                        xtype: "label",
                        text: "PPN:",
                        x: 570,
                        y: 125 + 225 + 10
                    },
                    {
                        xtype: 'numericfield',
                        name: 'persen_tax',
                        ref: '../persen_tax',
                        id: 'form-InvoiceSupplier-persen_tax',
                        enableKeyEvents: true,
                        allowBlank: false,
                        width: 40,
                        value: 10,
                        x: 570 + 60,
                        y: 125 + 225 + 10 - 3
                    },
                    {
                        xtype: "label",
                        text: "%",
                        x: 570 + 60 + 40 + 5,
                        y: 125 + 225 + 10
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        name: 'tax',
                        ref: '../tax',
                        id: 'form-InvoiceSupplier-tax',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 3
                    },
                    {
                        xtype: "label",
                        text: "PPH:",
                        x: 570,
                        y: 125 + 225 + 10 + 30
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: [
                                'value',
                                'displayText'
                            ],
                            data: [[0, '0 %'], [0.02, '2 %'], [0.03, '3 %'], [0.04, '4 %']]
                        }),
                        hiddenName: 'pph_persen',
                        valueField: 'value',
                        ref: '../pph_persen',
                        id: 'form-InvoiceSupplier-pph_persen',
                        displayField: 'displayText',
                        width: 55,
                        value: 0,
                        x: 570 + 60,
                        y: 125 + 225 + 10 - 3 + 30
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        name: 'pph',
                        ref: '../pph',
                        id: 'form-InvoiceSupplier-pph',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 3 + 30
                    },
                    {
                        xtype: "label",
                        text: "Grand Total:",
                        x: 570,
                        y: 125 + 225 + 10 + 60
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        name: 'total_bayar',
                        ref: '../grand_total',
                        id: 'form-InvoiceSupplier-grand_total',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 125 + 225 + 10 - 3 + 60
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.InvoiceSupplierWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.cmbSupplier.on('select', this.onSelectSupplier, this);
        this.on("close", this.onWinClose, this);
        this.persen_dpp.on('keyup', this.calculateData, this);
        this.persen_tax.on('keyup', this.calculateData, this);
        this.total.on('change', this.calculateData, this);
        this.discount.on('keyup', this.calculateData, this);
        this.pph_persen.on('select', this.calculateData, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else { //after audit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-InvoiceSupplierWin').getForm().submit({
            url: 'TerimaMaterial/CreateInvoice',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                tipeInvoice: this.tipeInvoice,
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                if (Ext.getCmp('docs-jun.InvoiceSupplierGrid') !== undefined) Ext.getCmp('docs-jun.InvoiceSupplierGrid').reloadStore();
                if (Ext.getCmp('docs-jun.TerimaMaterialGrid') !== undefined) Ext.getCmp('docs-jun.TerimaMaterialGrid').reloadStore();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    calculateData: function () {
        this.griddetils.store.refreshData();
    },
    onSelectSupplier: function (c, r, i) {
        this.term_of_payment.setValue(r.get('term_of_payment'));
        this.term_of_payment.setRawValue(r.get('term_of_payment'));
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});