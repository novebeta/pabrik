jun.UnitOfMeasureStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.UnitOfMeasureStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'UnitOfMeasureStoreId',
            url: 'UnitOfMeasure',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'code'},
                {name: 'desc'},
                {name: 'dimension'},
                {name: 'conversion', type: 'float'},
                {name: 'base'}
            ]
        }, cfg));
    }
});
jun.rztUnitOfMeasureLib = new jun.UnitOfMeasureStore();
jun.rztUnitOfMeasureBerat = new jun.UnitOfMeasureStore({baseParams: {dimension: 'BERAT'}});