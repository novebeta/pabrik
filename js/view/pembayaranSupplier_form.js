jun.PembayaranSupplierWin = Ext.extend(Ext.Window, {
    title: 'Form Pengajuan Transfer',
    modez: 1,
    width: 900,
    height: 510,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.supplierAccountStore = new jun.SupplierBankStore();
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-PembayaranSupplier',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. FPT:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        readOnly: true,
                        width: 175,
                        x: 5 + 90,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tgl:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        allowBlank: false,
                        format: 'd M Y',
                        value: new Date(),
                        x: 5 + 90,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Jatuh Tempo:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        fieldLabel: 'tgl_jatuh_tempo',
                        name: 'tgl_jatuh_tempo',
                        allowBlank: false,
                        format: 'd M Y',
                        x: 5 + 90,
                        y: 62,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Currency:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: false,
                        store: jun.rztCurrencyStore,
                        hiddenName: 'supp_rekening_currency',
                        valueField: 'val',
                        displayField: 'val',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{val}</b><br>{des}</span>',
                            "</div></tpl>"),
                        value: 'IDR',
                        maxLength: 3,
                        ref: '../supp_rekening_currency',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 5,
                        y: 125
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        ref: '../cmbSupplier',
                        x: 5 + 90,
                        y: 122,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "TUJUAN TRANSFER",
                        x: 5 + 90 + 205,
                        y: 5
                    },
                    {
                        xtype: "label",
                        text: "Nama Rekening:",
                        x: 5 + 90 + 205,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: this.supplierAccountStore,
                        hiddenName: 'supp_nama_rekening',
                        valueField: 'nama_rekening',
                        displayField: 'nama_rekening',
                        ref: '../cmbSuppNamaRekening',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{nama_rekening}</b><br>{nama_bank} - ({currency})<br>{alamat_bank}</span>',
                            "</div></tpl>"),
                        x: 5 + 90 + 205 + 90,
                        y: 32,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Rekening:",
                        x: 5 + 90 + 205,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        name: 'supp_no_rekening',
                        ref: '../supp_no_rekening',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90 + 205 + 90,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Bank Tujuan:",
                        x: 5 + 90 + 205,
                        y: 95
                    },
                    {
                        xtype: 'textfield',
                        name: 'supp_bank',
                        ref: '../supp_bank',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90 + 205 + 90,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "Alamat Bank:",
                        x: 5 + 90 + 205,
                        y: 125
                    },
                    {
                        xtype: 'textfield',
                        name: 'supp_alamat_bank',
                        ref: '../supp_alamat_bank',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90 + 205 + 90,
                        y: 122
                    },
                    {
                        xtype: "label",
                        text: "ASAL TRANSFER",
                        x: 5 + 90 + 205 + 90 + 205,
                        y: 5
                    },
                    {
                        xtype: "label",
                        text: "Nama Rekening:",
                        x: 5 + 90 + 205 + 90 + 205,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'nama_rekening',
                        ref: '../nama_rekening',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90 + 205 + 90 + 205 + 90,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "No. Rekening:",
                        x: 5 + 90 + 205 + 90 + 205,
                        y: 65
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'no_rekening',
                        id: 'no_rekening',
                        ref: '../no_rekening',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90 + 205 + 90 + 205 + 90,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Bank:",
                        x: 5 + 90 + 205 + 90 + 205,
                        y: 95
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'bank',
                        id: 'bank',
                        ref: '../bank',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90 + 205 + 90 + 205 + 90,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "CaraPembayaran:",
                        x: 5 + 90 + 205 + 90 + 205,
                        y: 125
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPaymentMethodStore,
                        hiddenName: 'cara_pembayaran',
                        valueField: 'val',
                        displayField: 'val',
                        value: 'TRANSFER',
                        ref: '../cara_pembayaran',
                        id: 'cara_pembayaran',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 90 + 205 + 90 + 205 + 90,
                        y: 122
                    },
                    new jun.PembayaranSupplierDetailsGrid({
                        x: 5,
                        y: 155,
                        frameHeader: !1,
                        height: 225,
                        //anchor: "100% 100%",
                        header: !1,
                        ref: "../griddetils",
                        readOnly: ((this.modez < 2 && !this.konfirmasiBayar) ? false : true)
                    }),
                    {
                        xtype: "label",
                        text: "Tujuan Pembayaran :",
                        x: 5,
                        y: 155 + 225 + 10
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'tujuan_pembayaran',
                        ref: '../tujuan_pembayaran',
                        width: 450,
                        x: 5,
                        y: 155 + 225 + 30
                    },
                    {
                        xtype: "label",
                        text: "Total Bayar:",
                        x: 570,
                        y: 155 + 225 + 10
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        name: 'total_bayar',
                        ref: '../total_bayar',
                        id: 'form-PembayaranSupplier-total_bayar',
                        allowBlank: false,
                        width: 175,
                        value: 0,
                        x: 690,
                        y: 155 + 225 + 10 - 2
                    },
                    {xtype: "hidden", name: "dpp", id: 'form-PembayaranSupplier-dpp'},
                    {xtype: "hidden", name: "ppn", id: 'form-PembayaranSupplier-ppn'},
                    {xtype: "hidden", name: "materai", id: 'form-PembayaranSupplier-materai'},
                    {xtype: "hidden", name: "total", id: 'form-PembayaranSupplier-total'},
                    {xtype: "hidden", name: "pph", id: 'form-PembayaranSupplier-pph'}
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Konfirmasi Pembayaran',
                    iconCls: 'silk13-money',
                    ref: '../btnKonfirmasiPembayaran'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PembayaranSupplierWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnKonfirmasiPembayaran.on('click', this.KonfirmasiPembayaran, this);
        this.cmbSupplier.on('select', this.cmbSupplierOnChange, this);
        this.cmbSuppNamaRekening.on('select', this.cmbSuppNamaRekeningOnChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 0) { //create
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
            this.btnKonfirmasiPembayaran.setVisible(false);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(this.konfirmasiBayar ? false : true);
            this.btnKonfirmasiPembayaran.setVisible(this.konfirmasiBayar ? true : false);
            if (this.konfirmasiBayar) this.formz.getForm().applyToFields({readOnly: true});
        } else { //after audit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnKonfirmasiPembayaran.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    cmbSupplierOnChange: function (combo, record, index) {
        this.supplierAccountStore.reload({params: {supplier_id: combo.getValue()}});
        this.cmbSuppNamaRekening.reset();
        this.supp_no_rekening.reset();
        this.supp_bank.reset();
        this.supp_alamat_bank.reset();
        this.griddetils.cmbNoINV.reset();
        this.griddetils.storeINV.reload({
            params: {
                'supplier_id': combo.getValue()
            }
        });
    },
    reloadComboStore: function (suppID, pembayaran_supplier_id) {
        //ketika proses Edit FPT
        this.supplierAccountStore.reload({params: {supplier_id: suppID}});
        this.griddetils.storeINV.reload({
            params: {
                'supplier_id': suppID,
                'pembayaran_supplier_id': pembayaran_supplier_id
            }
        });
    },
    cmbSuppNamaRekeningOnChange: function (combo, record, index) {
        this.supp_no_rekening.setValue(record.get('no_rekening'));
        this.supp_bank.setValue(record.get('nama_bank'));
        this.supp_alamat_bank.setValue(record.get('alamat_bank'));
        this.supp_rekening_currency.setValue(record.get('currency'));
    },
    KonfirmasiPembayaran: function () {
        var form = new jun.KonfirmasiPembayaranFPTWIn({
            id: this.id
        });
        this.close();
        form.show(this);
    },
    onWinClose: function () {
        jun.rztPembayaranSupplierDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-PembayaranSupplier').getForm().submit({
            url: 'PembayaranSupplier/create/',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztPembayaranSupplierDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztPembayaranSupplier.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PembayaranSupplier').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.KonfirmasiPembayaranFPTWIn = Ext.extend(Ext.Window, {
    title: "Konfirmasi Pembayaran FPT",
    width: 400,
    height: 145,
    layout: "form",
    modal: true,
    padding: 5,
    resizable: false,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-KonfirmasiPembayaranFPTWIn",
                labelWidth: 140,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tanggal Permbayaran FPT',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl_bayar",
                        ref: '../tgl_bayar',
                        value: new Date(),
                        allowBlank: false,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Kurs IDR',
                        xtype: 'numericfield',
                        name: 'IDR_rate',
                        ref: '../IDR_rate',
                        allowBlank: false,
                        value: 1,
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Konfirmasi Bayar",
                    ref: "../btnKonfirmasi"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.KonfirmasiPembayaranFPTWIn.superclass.initComponent.call(this);
        this.btnKonfirmasi.on("click", this.btnKonfirmasiOnclick, this);
        this.btnCancel.on("click", this.btnCancelOnclick, this);
    },
    btnDisabled: function (v) {
        this.btnKonfirmasi.setDisabled(v);
    },
    btnKonfirmasiOnclick: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-KonfirmasiPembayaranFPTWIn').getForm().submit({
            url: 'PembayaranSupplier/KonfirmasiBayar/',
            timeOut: 1000,
            scope: this,
            params: {
                id: this.id
            },
            success: function (f, a) {
                //jun.rztPembayaranSupplier.reload();
                if (Ext.getCmp('docs-jun.PembayaranSupplierGrid') !== undefined) Ext.getCmp('docs-jun.PembayaranSupplierGrid').reloadStore();
                if (Ext.getCmp('docs-jun.InvoiceSupplierGrid') !== undefined) Ext.getCmp('docs-jun.InvoiceSupplierGrid').reloadStore();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    btnCancelOnclick: function () {
        this.close();
    }
});