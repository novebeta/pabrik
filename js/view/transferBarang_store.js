jun.TransferBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferBarangStoreId',
            url: 'TransferBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_barang_id'},
                {name: 'doc_ref'},
                {name: 'tgl', type: 'date'},
                {name: 'pengirim', type: 'int'},
                {name: 'penerima', type: 'int'},
                {name: 'note'},
                {name: 'accepted', type: 'int'},
                {name: 'tdate_accept', type: 'date'},
                {name: 'tdate', type: 'date'},
                {name: 'id_user'},
                {name: 'trans_out', type: 'int'}
            ]
        }, cfg));
    }
});
jun.rztTransferBarang = new jun.TransferBarangstore();