jun.StorageLocationGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Gudang",
    id: 'docs-jun.StorageLocationGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code',
            width: 100
        },
        {
            header: 'Nama Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'location_name',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'delivery_address',
            width: 100
        }/*
         ,
         {
         header: 'phone',
         sortable: true,
         resizable: true,
         dataIndex: 'phone',
         width: 100
         },
         {
         header: 'phone2',
         sortable: true,
         resizable: true,
         dataIndex: 'phone2',
         width: 100
         },
         {
         header: 'fax',
         sortable: true,
         resizable: true,
         dataIndex: 'fax',
         width: 100
         }

         {
         header:'email',
         sortable:true,
         resizable:true,
         dataIndex:'email',
         width:100
         },
         {
         header:'contact',
         sortable:true,
         resizable:true,
         dataIndex:'contact',
         width:100
         },
         {
         header:'active',
         sortable:true,
         resizable:true,
         dataIndex:'active',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztStorageLocation;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Gudang',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Gudang',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Gudang',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.StorageLocationGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.StorageLocationWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.loc_code;
        var form = new jun.StorageLocationWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'StorageLocation/delete/id/' + record.json.loc_code,
            method: 'POST',
            success: function (f, a) {
                jun.rztStorageLocation.reload();
                jun.rztStorageLocationCmp.reload();
                jun.rztStorageLocationLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
