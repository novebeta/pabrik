jun.TerimaMaterialWin = Ext.extend(Ext.Window, {
    title: 'Penerimaan Material',
    modez: 1,
    width: 900,
    height: 470,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-TerimaMaterial',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
//                    {
//                        xtype: "label",
//                        text: "Doc Ref.:",
//                        x: 5,
//                        y: 5
//                    },
//                    {
//                        xtype: 'textfield',
//                        name: 'doc_ref',
//                        ref: '../doc_ref',
//                        readOnly: true,
//                        width: 175,
//                        x: 5+80,
//                        y: 2
//                    },
                    {
                        xtype: "label",
                        text: "Tgl Terima:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_terima',
                        fieldLabel: 'tgl_terima',
                        name: 'tgl_terima',
                        allowBlank: false,
                        format: 'd M Y',
                        value: new Date(),
                        x: 5 + 80,
                        y: 2,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Penerima:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'recipient',
                        ref: '../recipient',
                        maxLength: 50,
                        x: 5 + 80,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'supplier_id',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        ref: '../cmbSupplier',
                        x: 5 + 80,
                        y: 62,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. SJ:",
                        x: 5 + 80 + 220,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'no_sj_supplier',
                        id: 'no_sj_supplierid',
                        ref: '../no_sj_supplier',
                        allowBlank: false,
                        width: 175,
                        x: 5 + 80 + 220 + 80,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tgl SJ:",
                        x: 5 + 80 + 220,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_sj_supplier',
                        name: 'tgl_sj_supplier',
                        allowBlank: false,
                        format: 'd M Y',
                        x: 5 + 80 + 220 + 80,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "hidden",
                        name: "term_of_payment",
                        ref: '../term_of_payment'
                    },
                    new jun.TerimaMaterialDetailsGrid({
                        x: 5,
                        y: 95,
                        frameHeader: !1,
                        anchor: "100% 100%",
                        header: !1,
                        ref: "../griddetils",
                        readOnly: ((this.modez < 2) ? false : true),
                        restrictedEdit: this.restrictedEdit
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TerimaMaterialWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.cmbSupplier.on('select', this.cmbSupplierOnChange, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
            if (this.restrictedEdit) this.formz.getForm().applyToFields({readOnly: true});
        } else if (this.modez == 0) { //create
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else { //after audit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    cmbSupplierOnChange: function (combo, record, index) {
        this.griddetils.storePO.reload({
            params: {
                supplier_id: combo.getValue(),
                urole: UROLE
            }
        });
        this.term_of_payment.setValue(record.get("term_of_payment"));
        this.griddetils.po.reset();
        this.griddetils.material.reset();
        this.griddetils.qty.reset();
        this.griddetils.satuan.setText("");
        this.griddetils.note.reset();
    },
    onWinClose: function () {
        jun.rztTerimaMaterialDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-TerimaMaterial').getForm().submit({
            url: 'TerimaMaterial/create/',
            timeOut: 1000,
            scope: this,
            urole: UROLE,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztTerimaMaterialDetails.data.items, "data"))
            },
            success: function (f, a) {
                //jun.rztTerimaMaterial.reload();
                if (Ext.getCmp('docs-jun.TerimaMaterialGrid') !== undefined) Ext.getCmp('docs-jun.TerimaMaterialGrid').reloadStore();
                if (Ext.getCmp('docs-jun.PurchaseOrderGrid') !== undefined) Ext.getCmp('docs-jun.PurchaseOrderGrid').reloadStore();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TerimaMaterial').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});