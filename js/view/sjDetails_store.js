jun.SjDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SjDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjDetailsStoreId',
            url: 'SjDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sj_details_id'},
                {name: 'qty', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'totalpot', type: 'float'},
                {name: 'sj_id'},
                {name: 'barang_id'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'ket'},
                {name: 'disc1'},
                {name: 'pot1'},
                {name: 'disc2'},
                {name: 'pot2'},
                {name: 'disc3'},
                {name: 'pot3'},
                {name: 'bruto'},
                {name: 'hpp'},
                {name: 'vat'},
                {name: 'vatrp'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
jun.rztSjDetails = new jun.SjDetailsstore();
//jun.rztSjDetails.load();
