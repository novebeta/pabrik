jun.MaterialGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Material",
    id: 'docs-jun.MaterialGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_material',
            width: 80
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_material',
            width: 120
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 40
        },
        {
            header: 'Tipe Material',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_material_id',
            width: 60,
            renderer: jun.renderTipeMaterial
        }
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        this.store = jun.rztMaterial;
        if (jun.rztTipeMaterialLib.getTotalCount() === 0) jun.rztTipeMaterialLib.load();
        if (jun.rztTipeMaterialCmp.getTotalCount() === 0) jun.rztTipeMaterialCmp.load();
        if (jun.rztMerkCmp.getTotalCount() === 0) jun.rztMerkCmp.load();
        this.userPermission(1, 1, 1);
        // switch (UROLE) {
        //     case USER_ADMIN:
        //         this.userPermission(1, 1, 1);
        //         break;
        //     case USER_PPIC:
        //         this.userPermission(1, 1, 0);
        //         break;
        //     case USER_GA:
        //         this.userPermission(1, 1, 0);
        //         break;
        //     case USER_PURCHASING:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_PEMBELIAN:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_RND:
        //         this.userPermission(1, 1, 0);
        //         break;
        //     default:
        //         this.userPermission(0, 0, 0);
        // }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: this.actDelete ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actDelete || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Tipe : <b>All Material</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        ref: '../../menuFilter',
                        id: 'MaterialGrid_menuFilterid',
                        items: [
                            {
                                text: 'All Material',
                                tipe_material_id: 0,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Tipe : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.MaterialGrid').setFilterData(m.tipe_material_id);
                                    }
                                }
                            }
                        ]
                    }
                },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Kode Material</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'kode_material',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Kode Material',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_material';
                                        Ext.getCmp('docs-jun.MaterialGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Nama Material',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'nama_material';
                                        Ext.getCmp('docs-jun.MaterialGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Note',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'ket';
                                        Ext.getCmp('docs-jun.MaterialGrid').txtFind.focus().setValue("");
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                }
            ]
        };
        var tipeMaterialStore = new jun.TipeMaterialstore();
        tipeMaterialStore.load({
            scope: this,
            callback: this.addMenuFilter
        });
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        jun.MaterialGrid.superclass.initComponent.call(this);
        this.btnAdd.on('click', this.loadForm, this);
        this.btnEdit.on('click', this.loadEditForm, this);
        this.btnDelete.on('click', this.deleteRec, this);
        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tipe_material_id_filter = 0;
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    addMenuFilter: function (recarray) {
        for (i = 0; i < recarray.length; i++) {
            r = recarray[i];
            Ext.getCmp('MaterialGrid_menuFilterid').add({
                text: r.data.nama_tipe_material,
                tipe_material_id: r.data.tipe_material_id,
                listeners: {
                    click: function (m, e) {
                        m.parentMenu.ownerCt.setText('Tipe : <b>' + m.text + '</b>');
                        Ext.getCmp('docs-jun.MaterialGrid').setFilterData(m.tipe_material_id);
                    }
                }
            });
        }
    },
    setFilterData: function (n) {
        this.tipe_material_id_filter = n;
        if (this.store.baseParams.tipe_material_id && parseInt(this.store.baseParams.tipe_material_id) == n) {
            this.store.baseParams = {
                tipe_material_id: this.tipe_material_id_filter,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                tipe_material_id: this.tipe_material_id_filter,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.moveFirst();
        }
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.fieldsearch = this.btnFind.fieldsearch;
            this.valuesearch = c.getValue();
            this.store.baseParams = {
                tipe_material_id: this.tipe_material_id_filter,
                fieldsearch: this.fieldsearch,
                valuesearch: this.valuesearch
            };
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.MaterialWin({
            modez: 0,
            title: "Buat Data Material Baru",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Material");
            return;
        }
        var idz = selectedz.json.material_id;
        var form = new jun.MaterialWin({
            modez: this.actEdit ? 1 : 2,
            id: idz,
            title: (this.actEdit ? "Ubah" : "Lihat") + " Data Material : " + selectedz.json.kode_material,
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.ontipeSelect();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Material");
            return;
        }
        Ext.MessageBox.confirm(
            'Hapus Data Material',
            "Apakah anda yakin ingin menghapus data Material ini?<br><br>Kode material : " + record.json.kode_material + "<br>Nama material : " + record.json.nama_material,
            this.deleteRecYes,
            this
        );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Material/delete/id/' + record.json.material_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztMaterial.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
