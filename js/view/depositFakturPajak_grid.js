jun.DepositFakturPajakGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Deposit Nomer Faktur Pajak",
    id: 'docs-jun.DepositFakturPajakGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Faktur Pajak',
            sortable: true,
            resizable: true,
            dataIndex: 'fp_no',
            width: 100
        },
        {
            header: 'Jumlah',
            sortable: true,
            resizable: true,
            dataIndex: 'fp_jumlah',
            align: 'right',
            width: 50
        },
        {
            header: 'Sisa',
            sortable: true,
            resizable: true,
            align: 'right',
            width: 50,
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                sisa = record.json.fp_jumlah - record.json.fp_used;
                if (sisa > 0) metaData.style = "background-color: #CCFFCC; text-align: right;";
                if (record.json.fp_used == 0) metaData.style = "background-color: #9BF79B; text-align: right;";
                return sisa;
            }
        },
        {
            header: 'Tanggal input',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztDepositFakturPajak;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.reload({params: {mode: "grid"}});
        jun.rztDepositFakturPajak.sort('tgl', 'ASC');
        jun.DepositFakturPajakGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.DepositFakturPajakWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data");
            return;
        }
        var idz = selectedz.json.fp_id;
        var form = new jun.DepositFakturPajakWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm(
            'Pertanyaan', "Apakah anda yakin ingin menghapus data ini?<br><br>No Faktur Pajak : " + record.json.fp_no,
            this.deleteRecYes,
            this
        );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'DepositFakturPajak/delete/id/' + record.json.fp_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztDepositFakturPajak.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
;



