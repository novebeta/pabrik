jun.PurchaseRequisitionWin = Ext.extend(Ext.Window, {
    title: 'Purchase Request',
    modez: 1,
    width: 900,
    height: 500,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-PurchaseRequisition',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. PR",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        value: new Date(),
                        x: 85,
                        y: 32,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Tgl Diperlukan",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_required',
                        fieldLabel: 'tgl',
                        name: 'tgl_required',
                        id: 'tgl_required_id',
                        format: 'd M Y',
                        x: 85,
                        y: 62,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Pemohon",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        hideLabel: false,
                        name: 'pemohon',
                        ref: '../pemohon',
                        allowBlank: false,
                        width: 175,
                        x: 375,
                        y: 2
                    },
//                    {
//                        xtype: "label",
//                        text: "Nama vendor",
//                        x: 295,
//                        y: 35
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'vendor_name',
//                        ref: '../vendor_name',
//                        width: 175,
//                        x: 400,
//                        y: 32
//                    },
//                    {
//                        xtype: "label",
//                        text: "PIC Vendor",
//                        x: 295,
//                        y: 65
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'vendor_contact',
//                        ref: '../vendor_contact',
//                        width: 175,
//                        x: 400,
//                        y: 62
//                    },
                    {
                        xtype: "label",
                        text: "PR note",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        name: 'note',
                        ref: '../note',
                        width: 270,
                        height: 50,
                        x: 375,
                        y: 32
                    },
                    new jun.PurchaseRequisitionDetailsGrid({
                        x: 5,
                        y: 95,
                        //height: 225+60,
                        anchor: '100% 100%',
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        max_item: MAX_ITEM_PO,
                        readOnly: (( !(this.modez < 2) || this.rejectPR) ? true : false)
                    })
//                    {
//                        xtype: "label",
//                        text: "Total",
//                        x: 570+60,
//                        y: 95+225+10+60
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        readOnly: true,
//                        name: 'total',
//                        ref: '../total',
//                        id: 'form-PurchaseRequisition-total',
//                        width: 175,
//                        value: 0,
//                        x: 690,
//                        y: 95+225+10-2+60
//                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Reject PR',
                    ref: '../btnReject',
                    iconCls: 'silk13-cross'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PurchaseRequisitionWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnReject.on('click', this.btnRejectOnClick, this);
//        this.total.on('change', this.onIDRdiscountChange, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            this.btnReject.setVisible(false);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(this.rejectPR ? false : true);
            this.btnReject.setVisible(this.rejectPR ? true : false);
            if (this.rejectPR) this.formz.getForm().applyToFields({readOnly: true});
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnReject.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
        jun.rztMaterialCmp.baseParams = {
            tipe_material_id: this.tipe_material_id
        };
        jun.rztMaterialCmp.reload();
        jun.rztMaterialCmp.baseParams = {};
    },
    onWinClose: function () {
        jun.rztPurchaseRequisitionDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'PurchaseRequisition/create/';
        Ext.getCmp('form-PurchaseRequisition').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                pr_id: this.pr_id,
                tipe_material_id: this.tipe_material_id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztPurchaseRequisitionDetails.data.items, "data"))
            },
            success: function (f, a) {
                //jun.rztPurchaseRequisition.reload();
                if (Ext.getCmp('docs-jun.PurchaseRequisitionGrid') !== undefined) Ext.getCmp('docs-jun.PurchaseRequisitionGrid').reloadStore();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PurchaseRequisition').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    },
    btnRejectOnClick: function () {
        var form = new jun.RejectPurchaseRequisitionWin({
            pr_id: this.pr_id
        });
        form.show(this);
        form.no_pr.setText(this.doc_ref.getValue());
        this.close();
    }
});
jun.RejectPurchaseRequisitionWin = Ext.extend(Ext.Window, {
    title: "Reject Purchase Request",
    width: 400,
    height: 175,
    layout: "form",
    iconCls: 'silk13-cross',
    modal: true,
    padding: 5,
    resizable: false,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-RejectPurchaseRequisitionWin",
                labelWidth: 140,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'No. PR',
                        xtype: 'label',
                        ref: '../no_pr',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl_reject",
                        ref: '../tgl_reject',
                        value: new Date(),
                        allowBlank: false,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'No. Berita Acara',
                        xtype: 'uctextfield',
                        allowBlank: false,
                        name: "no_ba",
                        ref: '../no_ba',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Reject",
                    ref: "../btnReject",
                    iconCls: 'silk13-cross'
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.RejectPurchaseRequisitionWin.superclass.initComponent.call(this);
        this.btnReject.on("click", this.btnRejectOnclick, this);
        this.btnCancel.on("click", this.btnCancelOnclick, this);
    },
    btnRejectOnclick: function () {
        //this.btnDisabled(true);
        Ext.getCmp('form-RejectPurchaseRequisitionWin').getForm().submit({
            url: 'PurchaseRequisition/Reject',
            timeOut: 1000,
            scope: this,
            params: {
                pr_id: this.pr_id
            },
            success: function (f, a) {
                jun.rztPurchaseRequisition.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnCancelOnclick: function () {
        this.close();
    }
});