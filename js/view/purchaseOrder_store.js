jun.PurchaseOrderStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchaseOrderStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchaseOrderStoreId',
            url: 'PurchaseOrder',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_id'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'tgl'},
                {name: 'note'},
                {name: 'tgl_delivery'},
                {name: 'supplier_id'},
                {name: 'supplier_cp'},
                {name: 'term_of_payment'},
                {name: 'attn_name'},
                {name: 'total', type: 'float'},
                {name: 'discount', type: 'float'},
                {name: 'dpp', type: 'float'},
                {name: 'tax', type: 'float'},
                {name: 'persen_tax', type: 'float'},
                {name: 'grand_total', type: 'float'},
                {name: 'currency'},
                {name: 'IDR_rate', type: 'float'},
                {name: 'tipe_material_id', type: 'int'},
                {name: 'pr_id'},
                {name: 'pr_doc_ref'},
                {name: 'status', type: 'int'},
                {name: 'adt'}
            ]
        }, cfg));
    }
});
jun.rztPurchaseRequisition = new jun.PurchaseOrderStore();
jun.rztPurchaseOrder = new jun.PurchaseOrderStore();
jun.rztPurchaseOrderLib = new jun.PurchaseOrderStore();
jun.rztPurchaseOrderCmp = new jun.PurchaseOrderStore();
jun.rztStatusPOStore = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'displayText'
    ],
    data: [
        [P0_OPEN, 'OPEN'],
        [P0_RELEASED, 'RELEASED'],
        [PO_PARTIALLY_RECEIVED, 'PARTIALLY RECEIVED'],
        [PO_RECEIVED, 'RECEIVED'],
        [PO_CANCELED, 'CANCELED'],
        [PO_CLOSED, 'CLOSED']
    ]
});
jun.rztTermOfPaymentStore = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'des'
    ],
    data: [
        ['30 HARI', '']
//        ['TUNAI', 'Pembelian langsung dengan kasbon'],
//        ['COD', 'Cash On Delivery']
    ]
});