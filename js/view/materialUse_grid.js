jun.MaterialUseGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "MaterialUse",
    id: 'docs-jun.MaterialUseGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            renderer: jun.renderKodeMaterial,
            width: 100
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            renderer: jun.renderNamaMaterial,
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Kode Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztMaterialUse;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Material :'
                },
                {
                    xtype: 'combo',
                    // style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztMaterialCmp, //jun.rztMaterialCmp,
                    valueField: 'material_id',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<span style="width:100%;display:inline-block;"><b>{kode_material}</b><br>{nama_material}</span>',
                        "</div></tpl>"),
                    allowBlank: false,
                    listWidth: 400,
                    ref: '../material',
                    displayField: 'kode_material'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Qty :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../qty',
                    width: 75,
                    value: 1,
                    minValue: 0
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Gudang :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    fieldLabel: 'loc_code',
                    store: jun.rztStorageLocationCmp,
                    ref: '../loc_code',
                    valueField: 'loc_code',
                    displayField: 'loc_code'
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.MaterialUseGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var material_id = this.material.getValue();
        var loc_code = this.loc_code.getValue();
        if (loc_code == "" || loc_code == undefined) {
            Ext.MessageBox.alert("Error", "Gudang harus disefinisikan.");
            return;
        }
        if (material_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected");
            return
        }
        var qty = parseFloat(this.qty.getValue());
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('jml', qty);
            record.set('loc_code', loc_code);
            record.commit();
        } else {
            var c = jun.rztMaterialUse.recordType,
                d = new c({
                    material_id: material_id,
                    jml: qty,
                    loc_code: loc_code
                });
            jun.rztMaterialUse.add(d);
        }
        this.material.reset();
        this.qty.reset();
        this.loc_code.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.material.setValue(record.data.material_id);
            this.qty.setValue(record.data.jml);
            this.loc_code.setValue(record.data.loc_code);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
})
