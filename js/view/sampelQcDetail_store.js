jun.SampelQcDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SampelQcDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SampelQcDetailStoreId',
            url: 'SampelQcDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sampel_detail_id'},
                {name: 'qty', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'sampel_id'},
                {name: 'barang_id'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'ket'},
                {name: 'price_tax', type: 'float'},
                {name: 'total_tax', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztSampelQcDetail = new jun.SampelQcDetailstore();
