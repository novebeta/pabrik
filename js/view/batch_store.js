jun.Batchstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Batchstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BatchStoreId',
            url: 'Batch',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'batch_id'},
                {name: 'tgl_produksi', type: 'date'},
                {name: 'no_batch'},
                {name: 'bom_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'qty_per_pot', type: 'float'},
                {name: 'qty_per_box', type: 'float'},
                {name: 'tgl_mixing', type: 'date'},
                {name: 'qty_mixing', type: 'float'},
                {name: 'tgl_expired', type: 'date'},
                {name: 'qty_hasil', type: 'float'},
                {name: 'sat_hasil'},
                {name: 'status', type: 'int'},
                {name: 'tdate'},
                {name: 'id_user'}
            ]
        }, cfg));
    }
});
jun.rztBatch = new jun.Batchstore();
//===== STORE BATCH DETAILS
jun.BatchDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BatchDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BatchDetailstoreId',
            url: 'bomdetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'batch_detail_id'},
                {name: 'batch_id'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'}
            ]
        }, cfg));
    }
});
var readerBatchDetail = new Ext.data.JsonReader({
    idProperty: 'batch_detail_id',
    fields: [
        {name: 'batch_detail_id'},
        {name: 'batch_id'},
        {name: 'material_id'},
        {name: 'kode_material'},
        {name: 'nama_material'},
        {name: 'qty_bom', type: 'float'},
        {name: 'qty', type: 'float'},
        {name: 'sat'},
        {name: 'tipe_material_id'}
    ]
});
//jun.rztCreateBatchDetail = new Ext.data.GroupingStore({
//    reader: readerBatchDetail,
//    url: 'BatchDetail/GetBOMItems',
//    sortInfo: {field: 'kode_material', direction: 'ASC'},
//    groupField: 'tipe_material_id'
//});
jun.rztBatchDetail = new Ext.data.GroupingStore({
    reader: readerBatchDetail,
    url: 'BatchDetail/GetItems',
    sortInfo: {field: 'kode_material', direction: 'ASC'},
    groupField: 'tipe_material_id'
});
//===== STORE KONSUMSI BAHAN BAKU
var readerBatchKonsumsiDetail = new Ext.data.JsonReader({
    idProperty: 'batch_detail_id',
    fields: [
        {name: 'transfer_material_id'},
        {name: 'tgl', type: 'date'},
        {name: 'material_id'},
        {name: 'kode_material'},
        {name: 'nama_material'},
        {name: 'qty', type: 'float'},
        {name: 'sat'},
        {name: 'tgl_expired'},
        {name: 'no_lot'},
        {name: 'no_qc'},
        {name: 'tipe_material_id'},
        {name: 'doc_ref'}
    ]
});
jun.rztBatchKonsumsiDetail = new Ext.data.GroupingStore({
    reader: readerBatchKonsumsiDetail,
    url: 'TransferMaterial/GetTransferedItems',
    sortInfo: {field: 'kode_material', direction: 'ASC'},
    groupField: 'tipe_material_id'
});
////===== STORE BATCH HASIL PRODUK JADI
jun.BatchHasilStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BatchHasilStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BatchHasilStoreId',
            url: 'BatchHasil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'batch_hasil_id'},
                {name: 'batch_id'},
                {name: 'no_batch'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'tgl_exp', type: 'date'},
                {name: 'tgl', type: 'date'},
                {name: 'tdate', type: 'date'}
            ]
        }, cfg));
    }
});
jun.rztBatchHasil = new jun.BatchHasilStore();