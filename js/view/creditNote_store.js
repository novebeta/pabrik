jun.CreditNotestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CreditNotestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CreditNoteStoreId',
            url: 'CreditNote',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'credit_note_id'},
                {name: 'customer_id'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'tgl'}
            ]
        }, cfg));
    }
});
jun.rztCreditNote = new jun.CreditNotestore();
//jun.rztCreditNote.load();
