jun.NotaReturnWin = Ext.extend(Ext.Window, {
    title: 'Return Pembelian',
    modez: 1,
    width: 900,
    height: 490,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-NotaReturn',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No.:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'nota_return_doc_ref',
                        ref: '../nota_return_doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../nota_return_tgl',
                        name: 'nota_return_tgl',
                        format: 'd M Y',
                        x: 85,
                        y: 32,
                        allowBlank: false,
                        value: new Date(),
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. FP:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'nota_return_no_faktur_pajak',
                        ref: '../nota_return_no_faktur_pajak',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Tgl. FP:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../nota_return_tgl_faktur_pajak',
                        name: 'nota_return_tgl_faktur_pajak',
                        format: 'd M Y',
                        x: 85,
                        y: 92,
                        allowBlank: false,
                        readOnly: true,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "PENJUAL",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: "label",
                        text: "Nama:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'nota_return_penjual',
                        ref: '../nota_return_penjual',
                        width: 175,
                        allowBlank: false,
                        readOnly: true,
                        x: 380,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'nota_return_alamat_penjual',
                        ref: '../nota_return_alamat_penjual',
                        width: 175,
                        allowBlank: false,
                        readOnly: true,
                        x: 380,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "NPWP:",
                        x: 295,
                        y: 95
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'nota_return_npwp_penjual',
                        ref: '../nota_return_npwp_penjual',
                        width: 175,
                        allowBlank: false,
                        readOnly: true,
                        x: 380,
                        y: 92
                    },
                    {
                        xtype: 'hidden',
                        hideLabel: false,
                        name: 'nota_return_nama_pembeli',
                        ref: '../nota_return_nama_pembeli'
                    },
                    {
                        xtype: 'hidden',
                        hideLabel: false,
                        name: 'nota_return_pembeli',
                        ref: '../nota_return_pembeli'
                    },
                    {
                        xtype: 'hidden',
                        hideLabel: false,
                        name: 'nota_return_alamat',
                        ref: '../nota_return_alamat'
                    },
                    {
                        xtype: 'hidden',
                        hideLabel: false,
                        name: 'nota_return_npwp',
                        ref: '../nota_return_npwp'
                    },
                    new jun.NotaReturnDetailsGrid({
                        x: 5,
                        y: 125,
                        height: 190,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 295,
                        y: 125 + 205
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        value: 0,
                        name: 'nota_return_bruto',
                        ref: '../nota_return_bruto',
                        id: 'form-NotaReturn-nota_return_bruto',
                        allowBlank: false,
                        width: 175,
                        x: 380,
                        y: 125 + 205 - 2
                    },
                    {
                        xtype: "label",
                        text: "Disc:",
                        x: 295,
                        y: 125 + 205 + 30
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        name: 'nota_return_discount',
                        ref: '../nota_return_discount',
                        id: 'form-NotaReturn-nota_return_discount',
                        allowBlank: false,
                        readOnly: true,
                        value: 0,
                        width: 175,
                        x: 380,
                        y: 125 + 205 - 2 + 30
                    },
                    {
                        xtype: "label",
                        text: "PPN:",
                        x: 570,
                        y: 125 + 205
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        value: 0,
                        name: 'nota_return_ppn',
                        ref: '../nota_return_ppn',
                        id: 'form-NotaReturn-nota_return_ppn',
                        allowBlank: false,
                        width: 175,
                        x: 690,
                        y: 125 + 205 - 2
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 570,
                        y: 125 + 205 + 30
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        name: 'nota_return_total',
                        ref: '../nota_return_total',
                        id: 'form-NotaReturn-nota_return_total',
                        allowBlank: false,
                        readOnly: true,
                        value: 0,
                        width: 175,
                        x: 690,
                        y: 125 + 205 - 2 + 30
                    }//,
                    // {
                    //     xtype: "label",
                    //     text: "PPnBM:",
                    //     x: 570,
                    //     y: 125+205+60
                    // },
                    // {
                    //     xtype: 'numericfield',
                    //     hideLabel: false,
                    //     //enableKeyEvents: true,
                    //     name: 'nota_return_ppnbm',
                    //     ref: '../nota_return_ppnbm',
                    //     id: 'form-NotaReturn-nota_return_ppnbm',
                    //     allowBlank: false,
                    //     width: 175,
                    //     value: 0,
                    //     minValue: 0,
                    //     x: 690,
                    //     y: 125+205-2+60
                    // }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.NotaReturnWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 0) { //create
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    onWinClose: function () {
        jun.rztNotaReturnDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'ReturnPembelian/CreateNotaReturn/';
        Ext.getCmp('form-NotaReturn').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id
            },
            success: function (f, a) {
                if (Ext.getCmp('docs-jun.NotaReturnGrid') !== undefined) Ext.getCmp('docs-jun.NotaReturnGrid').reloadStore();
                if (Ext.getCmp('docs-jun.ReturnPembelianGrid') !== undefined) Ext.getCmp('docs-jun.ReturnPembelianGrid').reloadStore();
                //jun.rztReturnPembelian.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    Ext.getCmp('form-NotaReturn').getForm().reset();
                    this.btnDisabled(false);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
