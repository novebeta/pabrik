jun.ReturnPembelianDetailsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturnPembelianDetailsStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturnPembelianDetailStoreId',
            url: 'ReturnPembelianDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'return_pembelian_detail_id'},
                {name: 'return_pembelian_id'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'note_return'},
                {name: 'sat'},
                {name: 'price', type: 'float'},
                {name: 'tgl_expired', type: 'date'},
                {name: 'total', type: 'float'},
                {name: 'disc_rp', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'no_lot'},
                {name: 'no_qc'}
            ]
        }, cfg));
    },
    refreshData: function () {
        var dpp = 0, qty = 0;
        // this.each(function (r) {
        //     qty = jun.konversiSatuanDitampilkan(r.get('qty'), r.get('sat'));
        //     dpp += parseFloat(qty) * parseFloat(r.get('price'));
        // });
        // Ext.getCmp('form-NotaReturn-nota_return_dpp').setValue(dpp);
        // Ext.getCmp('form-NotaReturn-nota_return_ppn').setValue(dpp * 0.1);
    }
});
jun.rztReturnPembelianDetails = new jun.ReturnPembelianDetailsStore();
jun.rztCreateReturnPembelianDetails = new jun.ReturnPembelianDetailsStore({url: 'TerimaMaterialDetails'});
jun.rztNotaReturnDetails = new jun.ReturnPembelianDetailsStore();
jun.ReturnPembelianItemsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturnPembelianDetailsStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturnPembelianDetailStoreId',
            url: 'ReturnPembelian/GetItems',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'kode_material'},
                {name: 'nama_material'},
                {name: 'sat'}/*,
                 {name:'qty_received', type: 'float'},
                 {name:'tgl_expired', type: 'date'},
                 {name:'no_lot'},
                 {name:'no_qc'},
                 {name:'qty_returned', type: 'float'}*/
            ]
        }, cfg));
    }
});