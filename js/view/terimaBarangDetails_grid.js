jun.TerimaBarangDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TerimaBarangDetails",
    id: 'docs-jun.TerimaBarangDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        // {
        //     header: 'Batch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'batch',
        //     width: 100
        // },
        // {
        //     header: 'Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_exp',
        //     width: 100
        // },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztTerimaBarangDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama Material</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'),
                            allowBlank: false,
                            enableKeyEvents: true,
                            listWidth: 400,
                            width: 100,
                            ref: '../../barang',
                            displayField: 'kode_material'//,
                            // readOnly: true
                        },
//                         {
//                             xtype: 'label',
//                             style: 'margin:5px',
//                             text: 'Batch :'
//                         },
//                         {
//                             xtype: 'combo',
//                             style: 'margin-bottom:2px',
//                             typeAhead: true,
//                             triggerAction: 'all',
//                             mode: 'local',
//                             lazyRender: true,
//                             autoSelect: false,
//                             hideTrigger: true,
//                             matchFieldWidth: !1,
//                             pageSize: 20,
//                             itemSelector: "div.search-item",
//                             tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
//                                 '<span style="width:260px;display:inline-block;">{batch}</span>',
//                                 '<span style="width:100px;display:inline-block;">{tgl_exp:date("F Y")}</span>',
//                                 '<span style="width:100px;display:inline-block;float:right;">{qty:number( "0,0" )}</span>',
//                                 "</div></tpl>"),
//                             allowBlank: false,
//                             listWidth: 500,
//                             //forceSelection: true,
//                             store: jun.rztBarangBatchCmp,
// //                            hiddenName: 'barang_id',
//                             valueField: 'batch',
//                             ref: '../../batch',
//                             displayField: 'batch'
//                         },
//                         {
//                             xtype: 'label',
//                             style: 'margin:5px',
//                             text: 'Expired:'
//                         },
//                         {
//                             xtype: 'xdatefield',
//                             ref: '../../tgl_exp',
//                             fieldLabel: 'tgl_exp',
//                             name: 'tgl_exp',
//                             format: 'd M Y'
//                         },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 1,
                            minValue: 1
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stok'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../ket',
                            width: 200,
                            colspan: 3,
                            maxLength: 255
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.TerimaBarangDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        // this.barang.on('select', this.onSelectBrg, this);
        // this.batch.on('select', this.onSelectBatch, this);
        this.barang.on('select', this.showStock, this);
        this.btnAdd.setDisabled(true);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    showStock: function () {
        var material_id = this.barang.getValue();
        var loc_code = Ext.getCmp('loc_code_terima_barang_id').getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stok);
        }
    },
    onSelectBrg: function (c, r, i) {
        jun.rztBarangBatchCmp.baseParams = {
            barang_id: r.data.barang_id
        };
        jun.rztBarangBatchCmp.load();
        jun.rztBarangBatchCmp.baseParams = {};
        this.qty.reset();
        // this.batch.reset();
        // this.tgl_exp.reset();
    },
    onSelectBatch: function (combo, record, index) {
        this.batch.setValue(record.data.batch);
        this.tgl_exp.setValue(record.data.tgl_exp);
        //this.qty.setValue(record.data.qty);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(true);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            // jun.rztBarangBatchCmp.baseParams = {
            //     barang_id: record.data.barang_id
            // };
            // jun.rztBarangBatchCmp.load();
            // jun.rztBarangBatchCmp.baseParams = {};
            this.barang.setValue(record.data.barang_id);
            // this.batch.setValue(record.data.batch);
            // this.tgl_exp.setValue(record.data.tgl_exp);
            this.qty.setValue(record.data.qty);
            this.qty.setMaxValue(record.data.qty);
            this.ket.setValue(record.data.ket);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            if (this.loadForm() == 0) {
                btn.setText("Edit");
                this.btnDisable(false);
            }
        }
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return 1;
        }
        // if (this.tgl_exp.getValue() == "") {
        //     Ext.MessageBox.alert("Error", "Tgl Expired harus diisi.");
        //     return 1;
        // }
        var qty = this.qty.getValue();
        if (qty == "") {
            Ext.MessageBox.alert("Error", "Qty harus diisi.");
            return 1;
        }
        qty = parseFloat(qty);
        if (qty > this.qty.maxValue) {
            Ext.MessageBox.alert("Error", 'The maximum value for this field is ' + this.qty.maxValue);
            return 1;
        }
        // if (this.batch.getErrors().length > 0) {
        //     var error = "";
        //     for (var i = 0; i < this.batch.getErrors().length; ++i) {
        //         error += "[Batch] " + this.batch.getErrors()[i] + "<br/>";
        //     }
        //     Ext.MessageBox.alert("Error", error);
        //     return 1;
        // }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztTerimaBarangDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Barang sudah diinput.");
//                return
//            }
//        }
//         var batch = this.batch.getValue();
//         var tgl_exp = this.tgl_exp.getValue().format('Y-m-d');
        //var qty = parseFloat(this.qty.getValue());
        var ket = this.ket.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            // record.set('batch', batch);
            // record.set('tgl_exp', tgl_exp);
            record.set('qty', qty);
            record.set('ket', ket);
            record.commit();
        } else {
            var c = jun.rztTerimaBarangDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    // batch: batch,
                    // tgl_exp: tgl_exp,
                    qty: qty,
                    ket: ket
                });
            jun.rztTerimaBarangDetails.add(d);
        }
//        clearText();
        this.barang.reset();
        // this.batch.reset();
        // this.tgl_exp.reset();
        this.qty.reset();
        this.ket.reset();
        return 0;
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
jun.SupplierInvoiceDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SupplierInvoiceDetails",
    id: 'docs-jun.SupplierInvoiceDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        // {
        //     header: 'Batch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'batch',
        //     width: 100
        // },
        // {
        //     header: 'Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_exp',
        //     width: 100
        // },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc (Rp)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc_rp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'VAT',
            sortable: true,
            resizable: true,
            dataIndex: 'vatrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztTerimaBarangDetails;
        jun.SupplierInvoiceDetailsGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});
jun.SuppReturnDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SuppReturnDetails",
    id: 'docs-jun.SuppReturnDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Batch',
            sortable: true,
            resizable: true,
            dataIndex: 'batch',
            width: 100
        },
        {
            header: 'Expired',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_exp',
            width: 100
        },
        {
            header: 'Jml',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztTerimaBarangDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Jml :'
                },
                {
                    xtype: 'numericfield',
                    id: 'qtyid',
                    ref: '../qty',
                    style: 'margin-bottom:2px',
                    width: 50,
                    value: 1,
                    minValue: 0
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Note :'
                },
                {
                    xtype: 'textfield',
                    ref: '../ket',
                    width: 500,
                    maxLength: 255
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.SuppReturnDetailsGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            //this.barang.setValue(record.data.barang_id);
            //this.batch.setValue(record.data.batch);
            //this.tgl_exp.setValue(record.data.tgl_exp);
            this.qty.setValue(record.data.qty);
            //this.price.setValue(record.data.price);
            this.ket.setValue(record.data.ket);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        //var barang_id = this.barang.getValue();
        //if (barang_id == "") {
        //    Ext.MessageBox.alert("Error", "Barang harus dipilih.");
        //    return
        //}
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztCreditNoteDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Barang sudah diinput.");
//                return
//            }
//        }
//        var batch = this.batch.getValue();
//        var tgl_exp = this.tgl_exp.getValue();
        var qty = parseFloat(this.qty.getValue());
        //var price = parseFloat(this.price.getValue());
        var ket = this.ket.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            var total = qty * record.data.price;
            //record.set('barang_id', barang_id);
            //record.set('batch', batch);
            //record.set('tgl_exp', tgl_exp);
            record.set('qty', qty);
            //record.set('price', price);
            record.set('total', total);
            record.set('ket', ket);
            record.commit();
        }
        //this.barang.reset();
        //this.batch.reset();
        //this.tgl_exp.reset();
        this.qty.reset();
        //this.price.reset();
        this.ket.reset();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});