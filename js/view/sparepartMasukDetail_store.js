jun.SparepartMasukDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SparepartMasukDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SparepartMasukDetailStoreId',
            url: 'SparepartMasukDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sparepart_masuk_detil_id'},
                {name: 'sparepart_masuk_id'},
                {name: 'material_id'},
                {name: 'supplier_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.rztSparepartMasukDetail = new jun.SparepartMasukDetailstore();
