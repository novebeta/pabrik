jun.SupplierWin = Ext.extend(Ext.Window, {
    title: 'Supplier',
    modez: 1,
    width: 700,
    height: 610,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Supplier',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Perusahaan',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'supplier_name',
                        ref: '../supplier_name',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Perusahaan',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'supplier_code',
                        ref: '../supplier_code',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Contact person',
                        hideLabel: false,
                        name: 'contact_person',
                        ref: '../contact_person',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Alamat',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'alamat',
                        ref: '../alamat',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kota',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'kota',
                        ref: '../kota',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Negara',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'negara',
                        ref: '../negara',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode POS',
                        hideLabel: false,
                        //allowBlank: false,
                        name: 'kodepos',
                        ref: '../kodepos',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Telephone',
                        hideLabel: false,
                        name: 'telepon',
                        ref: '../telepon',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Fax',
                        hideLabel: false,
                        name: 'fax',
                        ref: '../fax',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        hideLabel: false,
                        name: 'email',
                        ref: '../email',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NPWP',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'npwp',
                        ref: '../npwp',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Term Of Payment',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'term_of_payment',
                        maxLength: 50,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'COA',
                        store: jun.rztChartMasterCmpHutang,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        displayField: 'account_code',
                        anchor: '100%'
                    },
                    new jun.SupplierBankGrid({
                        frameHeader: !1,
                        header: !1,
                        hideLabel: true,
                        height: 140,
                        readOnly: ((this.modez < 2) ? false : true)
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SupplierWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 0) {
            urlz = 'Supplier/create/';
        } else if (this.modez == 1) {
            urlz = 'Supplier/update/id/' + this.id;
        }
        Ext.getCmp('form-Supplier').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                rekening: Ext.encode(Ext.pluck(jun.rztSupplierBank.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztSupplier.reload();
                jun.rztSupplierLib.reload();
                jun.rztSupplierCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Supplier').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onWinClose: function () {
        jun.rztSupplierBank.removeAll();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SupplierBankGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SjDetails",
    id: 'docs-jun.SupplierBankGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_bank',
            width: 100
        },
        {
            header: 'a/n',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_rekening',
            width: 100
        },
        {
            header: 'No. Rekening',
            sortable: true,
            resizable: true,
            dataIndex: 'no_rekening',
            width: 100
        },
        {
            header: 'Currency',
            sortable: true,
            resizable: true,
            dataIndex: 'currency',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat_bank',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSupplierBank;
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 6,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Bank :'
                            },
                            {
                                xtype: 'textfield',
                                id: 'nama_bankid',
                                ref: '../../nama_bank',
                                width: 120
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'No. Rekening :'
                            },
                            {
                                xtype: 'textfield',
                                id: 'no_rekeningid',
                                ref: '../../no_rekening',
                                width: 120
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Currency :'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: false,
                                store: jun.rztCurrencyStore,
                                hiddenName: 'currency',
                                valueField: 'val',
                                displayField: 'val',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="width:100%;display:inline-block;"><b>{val}</b><br>{des}</span>',
                                    "</div></tpl>"),
                                value: 'IDR',
                                ref: '../../currency',
                                allowBlank: false,
                                width: 100
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Atas nama :'
                            },
                            {
                                xtype: 'textfield',
                                id: 'nama_rekeningid',
                                ref: '../../nama_rekening',
                                width: 120
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Alamat :'
                            },
                            {
                                xtype: 'textfield',
                                id: 'alamat_bankid',
                                ref: '../../alamat_bank',
                                colspan: 3,
                                width: 290
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        id: 'btnsalesdetilid',
                        defaults: {
                            scale: 'large',
                            width: 35
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            }
                            //                        {
                            //                            xtype: 'button',
                            //                            text: 'Del',
                            //                            ref: '../../btnDelete'
                            //                        }
                        ]
                    }
                ]
            };
        }
        jun.SupplierBankGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            //        this.btnDelete.on('Click', this.deleteRec, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
//        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var no_rekening = this.no_rekening.getValue();
        var nama_bank = this.nama_bank.getValue();
        var currency = this.currency.getValue();
        var nama_rekening = this.nama_rekening.getValue();
        var alamat_bank = this.alamat_bank.getValue();
        if (nama_bank == "" || no_rekening == "") {
            Ext.MessageBox.alert("Error", "Semua field harus diisi.");
            return
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('nama_bank', nama_bank);
            record.set('no_rekening', no_rekening);
            record.set('currency', currency);
            record.set('nama_rekening', nama_rekening);
            record.set('alamat_bank', alamat_bank);
            record.commit();
        } else {
            var c = jun.rztSupplierBank.recordType,
                d = new c({
                    nama_bank: nama_bank,
                    no_rekening: no_rekening,
                    currency: currency,
                    nama_rekening: nama_rekening,
                    alamat_bank: alamat_bank
                });
            jun.rztSupplierBank.add(d);
        }
        this.nama_bank.reset();
        this.no_rekening.reset();
        this.currency.reset();
        this.nama_rekening.reset();
        this.alamat_bank.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.nama_bank.setValue(record.data.nama_bank);
            this.no_rekening.setValue(record.data.no_rekening);
            this.currency.setValue(record.data.currency);
            this.nama_rekening.setValue(record.data.nama_rekening);
            this.alamat_bank.setValue(record.data.alamat_bank);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});