jun.DetilPenjualanWin = Ext.extend(Ext.Window, {
    title: 'DetilPenjualan',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-DetilPenjualan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'penjualan_id',
                        store: jun.rztPenjualanKredit,
                        hiddenName: 'penjualan_id',
                        forceSelection: true,
                        valueField: 'penjualan_id',
                        //displayField: 'Penjualan::model()->representingColumn()',
                        displayField: 'doc_ref',
                        //allowBlank:false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'barang_id',
                        store: jun.rztBarang,
                        hiddenName: 'barang_id',
                        forceSelection: true,
                        valueField: 'barang_id',
                        //displayField: 'Barang::model()->representingColumn()',
                        displayField: 'barcode',
                        //allowBlank:false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'sat',
                        hideLabel: false,
                        //hidden:true,
                        name: 'sat',
                        id: 'satid',
                        ref: '../sat',
                        maxLength: 10,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'jml',
                        hideLabel: false,
                        //hidden:true,
                        name: 'jml',
                        id: 'jmlid',
                        ref: '../jml',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'price',
                        hideLabel: false,
                        //hidden:true,
                        name: 'price',
                        id: 'priceid',
                        ref: '../price',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'disc1',
                        hideLabel: false,
                        //hidden:true,
                        name: 'disc1',
                        id: 'disc1id',
                        ref: '../disc1',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'disc2',
                        hideLabel: false,
                        //hidden:true,
                        name: 'disc2',
                        id: 'disc2id',
                        ref: '../disc2',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'pot',
                        hideLabel: false,
                        //hidden:true,
                        name: 'pot',
                        id: 'potid',
                        ref: '../pot',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'nominal',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nominal',
                        id: 'nominalid',
                        ref: '../nominal',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'pcs',
                        hideLabel: false,
                        //hidden:true,
                        name: 'pcs',
                        id: 'pcsid',
                        ref: '../pcs',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.DetilPenjualanWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'DetilPenjualan/update/id/' + this.id;
        } else {
            urlz = 'DetilPenjualan/create/';
        }
        Ext.getCmp('form-DetilPenjualan').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztDetilPenjualan.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-DetilPenjualan').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});