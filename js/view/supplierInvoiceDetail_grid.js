jun.InvoiceSupplierDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Terima Material Details",
    id: 'docs-jun.InvoiceSupplierDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 160,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 60,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')), "0,0");
            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 40
        },
        {
            header: 'Harga satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 80,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            width: 80,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')) * record.data.price, "0,0.00");
            }
        }
    ],
    initComponent: function () {
        //this.store = jun.rztInvoiceSupplierDetails;
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 7,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Kode :'
                            },
//                            {
//                                xtype: 'textfield',
//                                id: 'kodematerial',
//                                ref: '../../kodematerial',
//                                style: 'margin-bottom:2px',
//                                readOnly: this.tipeInvoice == TIPE_INV_TANPA_SJ? false : true,
//                                //width: 120,
//                                minValue: 0
//                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztMaterialLib,
                                hiddenName: 'material_id',
                                valueField: 'material_id',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<div style="width:100%;display:inline-block;"><span style="font-weight: bold">{kode_material}</span><br>{nama_material}</div>',
                                    "</div></tpl>"),
                                ref: '../../kodematerial',
                                readOnly: this.tipeInvoice == TIPE_INV_TANPA_SJ ? false : true,
                                displayField: 'kode_material'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'qtyid',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                readOnly: this.tipeInvoice == TIPE_INV_TANPA_SJ ? false : true,
                                width: 80,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                ref: '../../satuan',
                                text: 'Unit'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Harga :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'priceid',
                                ref: '../../price',
                                style: 'margin-bottom:2px',
                                width: 100,
                                minValue: 0
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../sat'
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'small',
                            width: 50
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd',
                                hidden: this.tipeInvoice == TIPE_INV_TANPA_SJ ? false : true
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                ref: '../../btnDelete',
                                hidden: this.tipeInvoice == TIPE_INV_TANPA_SJ ? false : true
                            }
                        ]
                    }
                ]
            };
        }
        jun.InvoiceSupplierDetailsGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.kodematerial.on('select', this.onSelectMaterial, this);
        }
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
    },
    onStoreChange: function (s, b, d) {
        s.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    resetForm: function () {
        this.kodematerial.reset();
        this.qty.reset();
        this.satuan.setText("Unit");
        this.price.reset();
    },
    onSelectMaterial: function (c, r, i) {
        this.qty.reset();
        this.sat.setValue(r.get('sat'));
        this.satuan.setText(r.get('sat'));
        this.price.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item");
            return;
        }
        if (btn.text == 'Edit') {
            this.loadEditForm(record);
            btn.setText("Save");
        } else {
            this.loadForm();
            btn.setText("Edit");
        }
    },
    loadEditForm: function (record) {
        this.kodematerial.setValue(record.data.material_id);
        this.qty.setValue(jun.konversiSatuanDitampilkan(record.data.qty, record.data.sat));
        this.sat.setValue(record.data.sat);
        this.satuan.setText(record.data.sat);
        this.price.setValue(record.data.price);
    },
    loadForm: function () {
        var kodematerial = this.kodematerial.getValue();
        var sat = this.sat.getValue();
        var qty = jun.konversiSatuanDisimpan(parseFloat(this.qty.getValue()), sat);
        var price = parseFloat(this.price.getValue());
        if (this.tipeInvoice == TIPE_INV_TANPA_SJ) {
            if (!kodematerial || !qty || !price) {
                Ext.MessageBox.alert("Error", "Semua field harus diisi.");
                return;
            }
        }
        if (isNaN(price)) {
            Ext.MessageBox.alert("Error", "Harga harus diisi.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            if (this.tipeInvoice == TIPE_INV_TANPA_SJ) {
                record.set('material_id', kodematerial);
                record.set('sat', sat);
                record.set('qty', qty);
            }
            record.set('price', price);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    material_id: kodematerial,
                    sat: sat,
                    qty: qty,
                    price: price
                });
            this.store.add(d);
        }
        this.resetForm();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
