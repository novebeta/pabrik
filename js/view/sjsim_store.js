jun.Sjsimstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Sjsimstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjsimStoreId',
            url: 'Sjsim',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sjsim_id'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'sj_id'},
                {name: 'total_input'},
                {name: 'customer_id'},
                {name: 'tgl'},
                {name: 'persen_tax'}
            ]
        }, cfg));
    }
});
jun.rztSjsim = new jun.Sjsimstore();
//jun.rztSjsim.load();
jun.Sales2Auditstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Sales2Auditstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'Sales2AuditstoreId',
            url: 'sj/GetAudit',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sj_id'},
                {name: 'doc_ref'},
                {name: 'kode_customer'},
                {name: 'nama_customer'},
                {name: 'tgl'},
                {name: 'cetak'},
                {name: 'total', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (a) {
        var data = this.query('cetak', true);
        var total = 0;
        data.each(function (entry) {
            total += entry.data.total;
        });
        //kurang_inputauditid
        var pajak = parseFloat(Ext.getCmp("total_inputauditid").getValue());
        var kurang = pajak - total;
        Ext.getCmp("totalauditid").setValue(total);
        Ext.getCmp("kurang_inputauditid").setValue(kurang);
    }
});
jun.rztSales2Audit = new jun.Sales2Auditstore();
