jun.TerimaMaterialDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Terima Material Details",
    id: 'docs-jun.TerimaMaterialDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'no_po',
            width: 100
        },
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 160,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 70,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')), "0,0.00");
            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 50
        },
        {
            header: 'Tgl Expired',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_expired',
            width: 70,
            renderer: Ext.util.Format.dateRenderer('d-m-Y')
        },
        {
            header: 'No. Lot',
            sortable: true,
            resizable: true,
            dataIndex: 'no_lot',
            width: 70
        },
        {
            header: 'No. QC',
            sortable: true,
            resizable: true,
            dataIndex: 'no_qc',
            width: 70
        }
    ],
    initComponent: function () {
        this.store = jun.rztTerimaMaterialDetails;
        this.storePO = new jun.PurchaseOrderStore();
        this.storePOdetails = new jun.PurchaseOrderDetailsstore();
        this.storePOitems = new jun.POitemsstore();
        if (!this.readOnly) {
            //var qc = this.input_noQC? true : false;
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 7,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: ' No. PO :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                readOnly: this.restrictedEdit,
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: this.storePO,
                                width: 180,
                                hiddenName: 'po_id',
                                valueField: 'po_id',
                                displayField: 'doc_ref',
                                ref: '../../po'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: ' Material :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                readOnly: this.restrictedEdit,
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: this.storePOitems,
                                width: 150,
                                hiddenName: 'material_id',
                                valueField: 'material_id',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<div style="width:100%;display:inline-block;"><span style="font-weight: bold">{kode_material}</span><br>{nama_material}</div>',
                                    "</div></tpl>"),
                                ref: '../../material',
                                displayField: 'kode_material'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                readOnly: this.restrictedEdit,
                                id: 'qtyid',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                width: 110,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                ref: '../../satuan',
                                text: 'Unit'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Exp :'
                            },
                            {
                                xtype: 'xdatefield',
                                ref: '../../tgl_expired',
                                name: 'tgl_expired',
                                width: 180,
                                format: 'd M Y'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'No. Lot :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../no_lot',
                                width: 150,
                                maxLength: 45
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'No. Qc :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../no_qc',
                                width: 150,
                                colspan: 2,
                                maxLength: 45
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Note :',
                                hidden: true
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../note',
                                width: 450,
                                maxLength: 50,
                                hidden: true
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../sat',
                                value: 'Unit'
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../price',
                                value: 0
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'large',
                            width: 30
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                height: 44,
                                ref: '../../btnAdd',
                                hidden: this.restrictedEdit
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                height: 44,
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                height: 44,
                                ref: '../../btnDelete',
                                hidden: this.restrictedEdit
                            }
                        ]
                    }
                ]
            };
        }
        jun.TerimaMaterialDetailsGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.material.on('select', this.materialOnSelect, this);
            this.po.on('select', this.PoOnSelect, this);
        }
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    PoOnSelect: function (c, r, i) {
        this.storePOdetails.load({params: {po_id: c.getValue()}});
        this.storePOitems.load({params: {po_id: c.getValue()}});
        this.resetForm();
    },
    materialOnSelect: function (combo, record, index) {
        var data = this.storePOdetails.getAt(
            this.storePOdetails.find('material_id', combo.getValue())
        );
        var qty_po = jun.konversiSatuanDitampilkan(Number(data.get('qty')), data.get('sat'));
        var qty_terima = jun.konversiSatuanDitampilkan(Number(record.get('qty_terima')), data.get('sat'));
        this.qty.setValue(qty_po - qty_terima);
        this.satuan.setText(record.get('sat'));
        this.sat.setValue(record.get('sat'));
        this.price.setValue(record.get('price'));
        this.tgl_expired.reset();
        this.no_lot.reset();
        this.no_qc.reset();
        this.note.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    resetForm: function () {
        //this.po.reset();
        this.material.reset();
        this.qty.reset();
        this.satuan.setText("Unit");
        this.sat.reset();
        this.price.reset();
        this.tgl_expired.reset();
        this.no_lot.reset();
        this.no_qc.reset();
        this.note.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item");
            return;
        }
        if (btn.text == 'Edit') {
            this.storePOdetails.load({params: {po_id: record.data.po_id}});
            this.storePOitems.load({
                params: {po_id: record.data.po_id},
                callback: this.loadEditForm(record)
            });
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function (record) {
        this.po.setValue(record.data.po_id);
        this.material.setValue(record.data.material_id);
        this.material.setRawValue(jun.renderKodeMaterial(record.data.material_id));
        this.qty.setValue(jun.konversiSatuanDitampilkan(record.data.qty, record.data.sat));
        this.satuan.setText(record.data.sat);
        this.sat.setValue(record.data.sat);
        this.price.setValue(record.data.price);
        this.note.setValue(record.data.note);
        this.no_lot.setValue(record.data.no_lot);
        this.no_qc.setValue(record.data.no_qc);
        this.tgl_expired.setValue(record.data.tgl_expired);
    },
    loadForm: function () {
        var po = this.po.getValue();
        var no_po = this.po.getRawValue();
        var material = this.material.getValue();
        var sat = this.sat.getValue();
        var qty = jun.konversiSatuanDisimpan(parseFloat(this.qty.getValue()), sat);
        var tgl_expired = this.tgl_expired.getValue();
        if (!po || !material || isNaN(qty)) {
            Ext.MessageBox.alert("Error", " Semua field harus diisi.");
            return;
        } else if (qty == 0) {
            Ext.MessageBox.alert("Error", " Quantity tidak boleh nol.");
            return;
        }
        $err_msg = "Material ini sudah ditambahkan.<br>No. Purchase Order : " + no_po + "<br>Kode material : " + this.material.getRawValue() + "<br>Tgl Expired : " + tgl_expired;
        if (jun.rztTerimaMaterialDetails.findBy(function (r) {
                return r.get('po_id') == po && r.get('material_id') == material && r.get('tgl_expired') == tgl_expired;
            }) != -1) {
            if (this.btnEdit.text == 'Save') {
                if (this.sm.getSelected().data.po_id != po || this.sm.getSelected().data.material_id != material || this.sm.getSelected().data.tgl_expired != tgl_expired) {
                    Ext.MessageBox.alert("Error", $err_msg);
                    this.resetForm();
                    return;
                }
            } else {
                Ext.MessageBox.alert("Error", $err_msg);
                return;
            }
        }
        var price = this.price.getValue();
        var note = this.note.getValue();
        var no_lot = this.no_lot.getValue();
        var no_qc = this.no_qc.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('po_id', po);
            record.set('no_po', no_po);
            record.set('material_id', material);
            record.set('qty', qty);
            record.set('sat', sat);
            record.set('price', price);
            record.set('tgl_expired', tgl_expired);
            record.set('note', note);
            record.set('no_lot', no_lot);
            record.set('no_qc', no_qc);
            record.commit();
        } else {
            var c = jun.rztTerimaMaterialDetails.recordType,
                d = new c({
                    po_id: po,
                    no_po: no_po,
                    material_id: material,
                    qty: qty,
                    sat: sat,
                    price: price,
                    tgl_expired: tgl_expired,
                    note: note,
                    no_lot: no_lot,
                    no_qc: no_qc
                });
            jun.rztTerimaMaterialDetails.add(d);
        }
        this.resetForm();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
