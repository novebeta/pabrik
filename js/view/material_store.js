jun.Materialstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Materialstore.superclass.constructor.call(this, Ext.apply({
            id: 'material_id',
            storeId: 'MaterialStoreId',
            url: 'Material',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'kode_material'},
                {name: 'nama_material'},
                {name: 'ket'},
                {name: 'sat'},
                {name: 'dimension'},
                {name: 'avg'},
                {name: 'tipe_material_id'},
                {name: 'kode_barang_rnd'},
                {name: 'kode_barang_2'},
                {name: 'grup_id'},
                {name: 'qty_per_box'},
                {name: 'qty_per_pot'}
            ]
        }, cfg));
    }
});
jun.rztMaterial = new jun.Materialstore();
jun.rztMaterialLib = new jun.Materialstore();
jun.rztMaterialLib.load();
jun.rztMaterialCmp = new jun.Materialstore();
jun.rztMaterialForBOM = new jun.Materialstore({baseParams: {materialforbom: 1}});
jun.rztMaterialSparepart = new jun.Materialstore({baseParams: {perbaikan_mesin: 1}});
jun.rztMaterialSparepartBekas = new jun.Materialstore({baseParams: {tipe_material_id: MAT_SPAREPART_BEKAS}});
// jun.TipeMaterialstore = Ext.extend(Ext.data.JsonStore, {
//     constructor: function (cfg) {
//         cfg = cfg || {};
//         jun.TipeMaterialstore.superclass.constructor.call(this, Ext.apply({
//             storeId: 'TipeMaterialStoreId',
//             url: 'TipeMaterial',
//             root: 'results',
//             totalProperty: 'total',
//             fields: [
//                 {name: 'tipe_material_id'},
//                 {name: 'nama_tipe_material'},
//                 {name: 'have_stock'}
//             ]
//         }, cfg));
//     }
// });
// jun.rztTipeMaterialLib = new jun.TipeMaterialstore();
// jun.rztTipeMaterialCmp = new jun.TipeMaterialstore();
//jun.rztTipeMaterialCmp2 = new jun.TipeMaterialstore();
jun.MaterialExpDateStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MaterialExpDateStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MaterialStoreId',
            url: 'Material/ExpiredDate',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tgl_expired'},
                {name: 'no_lot'},       //khusus bahan baku
                {name: 'no_qc'},        //khusus bahan baku
                {name: 'supplier_id'},
                {name: 'supplier_name'},
                {name: 'qty', type: 'float'},
                {name: 'sat'}
            ]
        }, cfg));
    }
});