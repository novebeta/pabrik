jun.SupplierGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Supplier",
    id: 'docs-jun.SupplierGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Supplier Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_code',
            width: 200
        },
        {
            header: 'Supplier Name',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_name',
            width: 200
        },
        {
            header: 'Kota',
            sortable: true,
            resizable: true,
            dataIndex: 'kota',
            width: 100
        },
        {
            header: 'COA',
            sortable: true,
            resizable: true,
            dataIndex: 'account_code',
            width: 50
        }
    ],
    initComponent: function () {
        if (jun.rztChartMasterCmpHutang.getTotalCount() === 0) {
            jun.rztChartMasterCmpHutang.load();
        }
        this.store = jun.rztSupplier;
        // switch (UROLE) {
        //     case USER_ADMIN:
        //         this.userPermission(1, 1, 1);
        //         break;
        //     case USER_PPIC:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_GA:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_PURCHASING:
        //         this.userPermission(1, 1, 0);
        //         break;
        //     case USER_PEMBELIAN:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     default:
        //         this.userPermission(0, 0, 0);
        // }
        this.userPermission(1, 1, 1);
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete'
                },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Nama supplier</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'supplier_name',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Nama supplier',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'supplier_name';
                                        Ext.getCmp('docs-jun.SupplierGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Kota',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kota';
                                        Ext.getCmp('docs-jun.SupplierGrid').txtFind.focus().setValue("");
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.SupplierGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.fieldsearch = this.btnFind.fieldsearch;
            this.valuesearch = c.getValue();
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SupplierWin({
            modez: 0,
            title: "Buat Data Supplier",
            iconCls: 'silk13-add'
        });
        form.show();
        jun.rztSupplierBank.removeAll();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Supplier");
            return;
        }
        var idz = selectedz.json.supplier_id;
        var form = new jun.SupplierWin({
            modez: this.actEdit ? 1 : 2,
            id: idz,
            title: ( this.actEdit ? "Ubah" : "Lihat") + " Data Supplier",
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSupplierBank.baseParams = {supplier_id: idz};
        jun.rztSupplierBank.load();
        jun.rztSupplierBank.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Supplier/delete/id/' + record.json.supplier_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSupplier.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
