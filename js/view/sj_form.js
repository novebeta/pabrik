jun.SjWin = Ext.extend(Ext.Window, {
    title: 'Surat Jalan',
    modez: 1,
    width: 900,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Sj',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. SJ:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        x: 85,
                        y: 32,
                        value: DATE_NOW,
                        allowBlank: false,
                        editable: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'customer_id',
                        store: jun.rztCustomersCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'kode_customer',
                        ref: '../customer_id',
                        allowBlank: false,
                        readOnly: true,
                        x: 400,
                        y: 2,
                        width: 175
                    },
                    // {
                    //     xtype: "label",
                    //     text: "Gudang:",
                    //     x: 295,
                    //     y: 35
                    // },
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     fieldLabel: 'loc_code',
                    //     id:'sj_loc_code_id',
                    //     store: jun.rztStorageLocationCmp,
                    //     hiddenName: 'loc_code',
                    //     valueField: 'loc_code',
                    //     displayField: 'loc_code',
                    //     width: 175,
                    //     x: 400,
                    //     y: 32
                    // },
                    // {
                    //     xtype: "label",
                    //     text: "Tgl Jatuh Tempo:",
                    //     x: 595,
                    //     y: 5
                    // },
                    // {
                    //     xtype: 'hidden',
                    //     ref: '../tgl_jatuh_tempo',
                    //     fieldLabel: 'tgl',
                    //     name: 'tgl_jatuh_tempo',
                    //     format: 'd M Y',
                    //     x: 690,
                    //     y: 2,
                    //     allowBlank: false,
                    //     editable: false,
                    //     width: 175
                    // },
                    new jun.SjDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        modex: this.modez,
                        ref: "../griddetils",
                        max_item: jun.Max_item_SJ(this.tipeBarang)
                    }),
                    {
                        xtype: 'hidden',
                        ref: "../sales_id",
                        name: 'sales_id'
                    }
//                    {
//                        xtype: "label",
//                        text: "Total:",
//                        x: 615,
//                        y: 365
//                    },
//                    {
//                        xtype: 'numericfield',
//                        fieldLabel: 'total',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'total',
//                        id: 'totalid',
//                        ref: '../total',
//                        maxLength: 30,
//                        x: 695,
//                        y: 362
//                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SjWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.grup.on('change', this.onGrupChange, this);
        // this.tgl.on('select', this.setJatuhTempo, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
            this.griddetils.topToolbar.setVisible(false);
        } else if (this.modez == 0) {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            this.griddetils.topToolbar.setVisible(true);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.griddetils.topToolbar.setVisible(false);
        }
        // jun.rztBarangBatchCmp.reload();
        // jun.rztBarangCmp.baseParams = {tipe_barang_id: this.tipeBarang};
        // jun.rztBarangCmp.reload();
        // jun.rztBarangCmp.baseParams = {};
    },
    onWinClose: function () {
        jun.rztSjDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    setJatuhTempo: function (d, date) {
        var tglExp = d.getValue();
        var jatuh_tempo = 45; //hari
        tglExp.setDate(tglExp.getDate() + jatuh_tempo);
        this.tgl_jatuh_tempo.setValue(tglExp);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if(this.modez == 0){
            urlz = 'Sj/create/';
        }else{
            urlz = 'Sj/UpdateTgl/';
        }
        Ext.getCmp('form-Sj').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztSjDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztSlsSjInvHeader.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Sj').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onGrupChange: function (combo, record, index) {
        var grup = combo.getValue();
        this.griddetils.barang.store.baseParams = {
            grup_id: grup,
            tipe_barang_id: this.tipeBarang
        };
        this.griddetils.barang.store.reload();
        this.griddetils.barang.store.baseParams = {};
        this.griddetils.barang.reset();
        this.griddetils.batch.reset();
        this.griddetils.tgl_exp.reset();
        this.griddetils.qty.reset();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SalesInvoiceWin = Ext.extend(Ext.Window, {
    title: 'Sales Invoice',
    modez: 1,
    width: 925,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SalesInvoice',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref_inv',
                        ref: '../doc_ref_inv',
                        width: 175,
                        x: 95,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Jatuh Tempo:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        name: 'tgl_jatuh_tempo',
                        allowBlank: true,
                        format: 'd M Y',
                        readOnly: true,
                        x: 400,
                        y: 62,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Barang Diterima:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        name: 'tgl',
                        allowBlank: false,
                        format: 'd M Y',
                        readOnly: true,
                        x: 95,
                        y: 62,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Pajak:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_faktur_tax',
                        ref: '../no_faktur_tax',
                        maxLength: 50,
                        x: 400,
                        y: 2,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_inv',
                        name: 'tgl_inv',
                        format: 'd M Y',
                        x: 95,
                        y: 32,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Keterangan:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket',
                        ref: '../ket',
                        x: 400,
                        y: 35,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Biaya Ekspedisi:",
                        x: 600,
                        y: 5
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'biaya_lain',
                        id: 'biaya_lainid',
                        ref: '../biaya_lain',
                        width: 150,
                        x: 700,
                        y: 2
                    },
                    new jun.SalesInvoiceDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 225,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 615,
                        y: 330
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalinvid',
                        ref: '../total',
                        readOnly: true,
                        x: 715,
                        y: 328,
                        width: 175
                    },
                    {
                        xtype: 'hidden',
                        name: "status",
                        value: 1
                    }//,
                    // {
                    //     xtype: 'hidden',
                    //     name: "sj_id"
                    // },
                    // {
                    //     xtype: 'hidden',
                    //     name: "termofpayment",
                    //     ref: '../termofpayment'
                    // }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalesInvoiceWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        // this.tgl_diterima.on('change', this.onchangetgl, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else if (this.modez == 0) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
        // jun.rztBarangBatchCmp.reload();
    },
    onWinClose: function () {
        jun.rztSjDetails.removeAll();
        // jun.rztSo.reload();
    },
    onchangetgl: function () {
        //this.record.data.tgl_jatuh_tempo = this.record.data.tgl_diterima.add(Date.DAY, parseInt(this.record.data.termofpayment));
        //this.tgl_jatuh_tempo.setValue(this.tgl_diterima.add(Date.DAY, parseInt(3)));
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Sj/update/';
        Ext.getCmp('form-SalesInvoice').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztSjDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztSjCetakInvoice.reload();
                jun.rztSj.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-SalesInvoice').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});