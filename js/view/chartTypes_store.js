jun.ChartTypesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ChartTypesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ChartTypesStoreId',
            url: 'ChartTypes',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'id'},
                {name: 'name'},
                {name: 'class_id'},
                {name: 'parent'},
                {name: 'up'},
                {name: 'seq'}
            ]
        }, cfg));
    }
});
jun.rztChartTypes = new jun.ChartTypesstore();
jun.rztChartTypesCmp = new jun.ChartTypesstore();
jun.rztChartTypesLib = new jun.ChartTypesstore();
jun.rztChartTypesLib.load();
