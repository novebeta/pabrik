jun.HppStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.HppStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'HppStoreId',
            url: 'Hpp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'hpp_id'},
                {name: 'start_date', type: 'date'},
                {name: 'end_date', type: 'date'},
                {name: 'id_user'},
                {name: 'tdate', type: 'date'}
            ]
        }, cfg));
    }
});
jun.rztHpp = new jun.HppStore();