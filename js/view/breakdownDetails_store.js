jun.BreakdownDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BreakdownDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BreakdownDetailsStoreId',
            url: 'BreakdownDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'breakdown_detail_id'},
                {name: 'breakdown_id'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'price_std'},
                {name: 'loc_code'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        Ext.getCmp('total_qty_breakdown_id').setValue(this.sum("qty"));
    }
});
jun.rztBreakdownDetails = new jun.BreakdownDetailsstore();
//jun.rztBreakdownDetails.load();
