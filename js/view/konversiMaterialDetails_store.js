jun.KonversiMaterialDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KonversiMaterialDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KonversiMaterialDetailsStoreId',
            url: 'KonversiMaterialDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'konversi_material_details_id'},
                {name: 'dari_material_id'},
                {name: 'dari_qty'},
                {name: 'dari_price_std'},
                {name: 'total_std'},
                {name: 'ke_material_id'},
                {name: 'ke_qty'},
                {name: 'ke_price_std'},
                {name: 'konversi_material_id'},
                {name: 'loc_code_old'},
                {name: 'loc_code_new'}
            ]
        }, cfg));
    }
});
jun.rztKonversiMaterialDetails = new jun.KonversiMaterialDetailsstore();
//jun.rztKonversiMaterialDetails.load();
