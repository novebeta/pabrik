jun.Merkstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Merkstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MerkStoreId',
            url: 'Merk',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'merk_id'},
                {name: 'nama_'}
            ]
        }, cfg));
    }
});
jun.rztMerk = new jun.Merkstore();
jun.rztMerkCmp = new jun.Merkstore();
jun.rztMerkLib = new jun.Merkstore();
//jun.rztMerk.load();
