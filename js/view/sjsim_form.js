jun.SjsimWin = Ext.extend(Ext.Window, {
    title: 'Retur Beli',
    modez: 1,
    width: 900,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Sjsim',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. RB:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'customer_id',
                        store: jun.rztCustomersCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'kode_customer',
                        x: 400,
                        y: 2,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Grup Barang:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Grup',
                        emptyText: "ALL",
                        ref: '../grup',
                        store: jun.rztGrupCmp,
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        x: 400,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        x: 85,
                        y: 32,
                        allowBlank: false,
                        width: 175
                    },
                    new jun.SjsimDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SjsimWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.grup.on('change', this.onGrupChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onGrupChange: function (combo, record, index) {
        var grup = combo.getValue();
        this.griddetils.barang.store.baseParams = {
            grup_id: grup
        };
        this.griddetils.barang.store.reload();
        this.griddetils.barang.store.baseParams = {};
        this.griddetils.barang.reset();
        this.griddetils.batch.reset();
        this.griddetils.tgl_exp.reset();
        this.griddetils.qty.reset();
    },
    onWinClose: function () {
        jun.rztSjsimDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Sjsim/create/';
        Ext.getCmp('form-Sjsim').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztSjsimDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztSjsim.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    Ext.getCmp('form-Sjsim').getForm().reset();
                    this.btnDisabled(false);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.Sales2AuditWin = Ext.extend(Ext.Window, {
    title: "SJ Audit",
    modez: 1,
    width: 925,
    height: 440,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E8E8E8;padding: 10px",
                id: "form-Sales2AuditWin",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                anchor: "100% 100%",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: "Dari :",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tglfrom',
                        fieldLabel: 'Date',
                        name: 'tglfrom',
                        id: 'tglfromauditid',
                        format: 'd M Y',
                        width: 175,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Sampai :",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tglto',
                        fieldLabel: 'Date',
                        name: 'tglto',
                        id: 'tgltoauditid',
                        format: 'd M Y',
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Total :",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Total',
                        name: 'total',
                        id: 'totalauditid',
                        ref: '../total',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Perkiraan Pajak :",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Total',
                        name: 'total_input',
                        id: 'total_inputauditid',
                        ref: '../total_input',
                        value: 0,
                        maxLength: 30,
                        width: 175,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Kekurangan :",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Total',
                        id: 'kurang_inputauditid',
                        ref: '../kurang_input',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 175,
                        x: 705,
                        y: 2
                    },
                    new jun.Sales2AuditGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save to Audit",
                    ref: "../btnSave"
                }
            ]
        };
        jun.Sales2AuditWin.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.tglfrom.on('select', this.ontglChangeclick, this);
        this.tglto.on('select', this.ontglChangeclick, this);
        this.total_input.on('change', this.calculate_kurang, this);
        this.on("close", this.onWinClose, this);
        jun.rztSales2Audit.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tglfrom: Ext.getCmp('tglfromauditid').getValue(),
                        tglto: Ext.getCmp('tgltoauditid').getValue()
                    }
                }
            }
        });
    },
    calculate_kurang: function () {
        jun.rztSales2Audit.refreshData();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
    },
    onWinClose: function () {
        jun.rztSales2Audit.removeAll();
    },
    ontglChangeclick: function () {
        var tglfrom = this.tglfrom.getValue();
        var tglto = this.tglto.getValue();
        if (tglfrom == undefined || tglfrom == '' || tglto == undefined || tglto == '') {
            return;
        }
        jun.rztSales2Audit.load();
    },
    onbtnSaveclick: function () {
        var data = jun.rztSales2Audit.query('cetak', true);
        var sj_id = [];
        data.each(function (entry) {
            sj_id.push(entry.data.sj_id);
        });
        Ext.getCmp('form-Sales2AuditWin').getForm().submit({
            url: "Sj/CreateAudit",
            scope: this,
            params: {
                sj_id: Ext.encode(sj_id)
            },
            success: function (f, a) {
                //jun.rztSjint.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    }
});
jun.Sales2AuditGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales2Audit",
    id: 'docs-jun.Sales2AuditGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. SJ',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Kode Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_customer',
            width: 100
        },
        {
            header: 'Nama Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            xtype: 'checkcolumn',
            header: 'To Audit',
            dataIndex: 'cetak',
            width: 55
        }
    ],
    initComponent: function () {
        this.store = jun.rztSales2Audit;
        jun.Sales2AuditGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('mousedown', this.onClick, this);
    },
    onClick: function () {
        this.store.commitChanges();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});
