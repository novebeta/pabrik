jun.HppGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "HPP",
    id: 'docs-jun.HppGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Dari Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'start_date',
            width: 80
        },
        {
            header: 'Sampai Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'end_date',
            width: 80
        }
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        this.store = jun.rztHpp;
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1);
                break;
            case USER_PPIC:
                this.userPermission(1, 1, 0);
                break;
            case USER_GA:
                this.userPermission(1, 1, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0, 0);
                break;
            case USER_PEMBELIAN:
                this.userPermission(0, 0, 0);
                break;
            case USER_RND:
                this.userPermission(1, 1, 0);
                break;
            default:
                this.userPermission(0, 0, 0);
        }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: this.actDelete ? false : true
//                },
//                '->',
//                {
//                    xtype: 'button',
//                    text: '<b>Kode Hpp</b>',
//                    ref: '../btnFind',
//                    iconCls: 'silk13-find',
//                    fieldsearch: 'kode_material',
//                    menu:{
//                        xtype: 'menu',
//                        items:[
//                            {
//                                text: 'Kode Hpp',
//                                listeners: {
//                                    click: function(m, e){
//                                        m.parentMenu.ownerCt.setText('<b>'+m.text+'</b>');
//                                        m.parentMenu.ownerCt.fieldsearch = 'kode_material';
//                                        Ext.getCmp('docs-jun.HppGrid').txtFind.focus().setValue("");
//                                    }
//                                }
//                            },
//                            {
//                                text: 'Nama Hpp',
//                                listeners: {
//                                    click: function(m, e){
//                                        m.parentMenu.ownerCt.setText('<b>'+m.text+'</b>');
//                                        m.parentMenu.ownerCt.fieldsearch = 'nama_material';
//                                        Ext.getCmp('docs-jun.HppGrid').txtFind.focus().setValue("");
//                                    }
//                                }
//                            }
//                        ]
//                    }
//                },
//                {
//                    xtype: 'textfield',
//                    ref: '../txtFind',
//                    emptyText: 'Teks pencarian ...',
//                    enableKeyEvents: true
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        jun.HppGrid.superclass.initComponent.call(this);
        this.btnAdd.on('click', this.loadForm, this);
        this.btnEdit.on('click', this.loadEditForm, this);
        this.btnDelete.on('click', this.deleteRec, this);
//        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.store.baseParams = {fieldsearch: this.btnFind.fieldsearch, valuesearch: c.getValue()};
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.HppWin({
            modez: 0,
            title: "HPP",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data");
            return;
        }
        var form = new jun.HppWin({
            modez: this.actEdit ? 1 : 2,
            hpp_id: selectedz.json.hpp_id,
            title: (this.actEdit ? "Ubah" : "Lihat") + " HPP",
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm(
            'Hapus Data HPP',
            "Apakah anda yakin ingin menghapus data HPP ini?<br><br>Tanggal Mulai : " + record.json.start_date + "<br>Tanggal akhir : " + record.json.end_date,
            this.deleteRecYes,
            this
        );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Hpp/delete/id/' + record.json.material_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztHpp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
