jun.Servicestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Servicestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ServiceStoreId',
            url: 'Service',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'service_id'},
                {name: 'doc_ref'},
                {name: 'sub_total'},
                {name: 'total'},
                {name: 'lunas'},
                {name: 'tgl'},
                {name: 'id_user'},
                {name: 'parent'},
                {name: 'tdate'},
                {name: 'arus'},
                {name: 'loc_code'},
                {name: 'customer_id'}
            ]
        }, cfg));
    }
});
jun.rztService = new jun.Servicestore();
jun.rztServiceOut = new jun.Servicestore();
//jun.rztService.load();
