jun.SupplierPaymentstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SupplierPaymentstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SupplierPaymentStoreId',
            url: 'SupplierPayment',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'supplier_payment_id'},
                {name: 'total'},
                {name: 'no_bg_cek'},
                {name: 'no_bukti'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'bank_id'},
                {name: 'supplier_id'},
                {name: 'tdate'},
                {name: 'id_user'}
            ]
        }, cfg));
    }
});
jun.rztSupplierPayment = new jun.SupplierPaymentstore({baseParams: {mode: 'grid'}});
//jun.rztSupplierPayment.load();
