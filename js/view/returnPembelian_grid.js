jun.ReturnPembelianGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Return Pembelian",
    id: 'docs-jun.ReturnPembelianGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Return',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 80
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 150,
            renderer: jun.renderNamaSuppliers
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 80,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case RETURN_PEMBELIAN_OPEN :
                        return 'OPEN';
                    case RETURN_PEMBELIAN_DELIVERED :
                        return 'DELIVERED';
                    case RETURN_PEMBELIAN_RECEIVED :
                        return 'RECEIVED';
                    case RETURN_PEMBELIAN_NOTA_RETURN :
                        return 'NOTA RETURN RELEASED';
                }
            }
        },
        {
            header: 'No. Nota Return',
            sortable: true,
            resizable: true,
            dataIndex: 'nota_return_doc_ref',
            width: 80,
            renderer: function (value, metaData, record, rowIndex) {
                if (Number(record.get('nota_return_status')) == NOTA_RETURN_CANCELED)
                    metaData.style = "text-decoration: line-through;";
                return value;
            }
        }
    ],
    userPermission: function (actEdit, actPrint, actKirim, actNotaReturn, actTandaTerima) {
        this.actEdit = true;
        this.actView = true;
        this.actPrint = true;
        this.actKirim = true;
        this.actNotaReturn = true;
        this.actTandaTerima = true;
        // -- jika yang buka purchasing maka akan menampilkan semua return
        // -- jika yang buka bukan purchasing maka hanya return miliknya yang akan ditampilkan
        this.viewAllData = (this.actNotaReturn || UROLE == USER_PURCHASING);
    },
    initComponent: function () {
        this.store = jun.rztReturnPembelian;
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        if (jun.rztMaterialCmp.getTotalCount() === 0) jun.rztMaterialCmp.load();
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) jun.rztStorageLocationCmp.load();
        this.userPermission(0, 0, 0, 0, 0);
        // switch(UROLE){
        //     case USER_ADMIN: this.userPermission(1,1,1,1,1); break;
        //     case USER_PPIC: this.userPermission(1,1,1,0,0); break;
        //     case USER_GA: this.userPermission(1,1,1,0,0); break;
        //     case USER_PURCHASING: this.userPermission(0,0,0,0,0); break;
        //     case USER_PEMBELIAN: this.userPermission(0,0,0,1,0); break;
        //     default: this.userPermission(0,0,0,0,0);
        // }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print Tanda Terima',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status Kirim',
                    ref: '../btnDeliver',
                    iconCls: 'silk13-lorry_go',
                    hidden: this.actKirim ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Buat Nota Return',
                    ref: '../btnNotaReturn',
                    hidden: this.actNotaReturn ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Tanda Terima',
                    ref: '../btnTandaTerima',
                    hidden: this.actTandaTerima ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actPrint || this.actKirim || this.actNotaReturn || this.actTandaTerima) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_sj: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.ReturnPembelianGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_sj: RETURN_PEMBELIAN_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.ReturnPembelianGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'DELIVERED',
                                status_sj: RETURN_PEMBELIAN_DELIVERED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.ReturnPembelianGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'RECEIVED',
                                status_sj: RETURN_PEMBELIAN_RECEIVED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.ReturnPembelianGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'NOTA RETURN RELEASED',
                                status_sj: RETURN_PEMBELIAN_NOTA_RETURN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.ReturnPembelianGrid').setFilterData(m.status_sj);
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReturnPembelianGrid",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "return_pembelian_id",
                            ref: "../../return_pembelian_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {items: [{xtype: 'paging', store: this.store, displayInfo: true, pageSize: 20}]};
        jun.ReturnPembelianGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.btnPrintOnClick, this);
        this.btnDeliver.on('Click', this.btnDeliverOnClick, this);
        this.btnNotaReturn.on('Click', this.btnNotaReturnOnClick, this);
        this.btnTandaTerima.on('Click', this.btnTandaTerimaOnClick, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(RETURN_PEMBELIAN_OPEN);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (this.actEdit) {
            this.btnEdit.setIconClass(r.get('status') == RETURN_PEMBELIAN_OPEN ? 'silk13-pencil' : 'silk13-eye');
            this.btnEdit.setText(r.get('status') == RETURN_PEMBELIAN_OPEN ? "Ubah" : "Lihat");
        }
    },
    setFilterData: function (sts) {
        this.status_return = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        this.store.baseParams = {
            mode: "grid",
            status: this.status_return,
            urole: (this.viewAllData ? 'all' : UROLE)
            //uid: (this.viewAllData? 'all' : UID)
        };
        this.store.load();
    },
    loadEditForm: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        var r = this.sm.getSelected().json;
        var ttl = "";
        switch (Number(r.status)) {
            case RETURN_PEMBELIAN_OPEN :
                ttl = 'OPEN';
                break;
            case RETURN_PEMBELIAN_DELIVERED :
                ttl = 'DELIVERED';
                break;
            case RETURN_PEMBELIAN_RECEIVED :
                ttl = 'RECEIVED';
                break;
            case RETURN_PEMBELIAN_NOTA_RETURN :
                ttl = 'NOTA RETURN RELEASED';
                break;
        }
        var filter = r.status == RETURN_PEMBELIAN_OPEN && this.actEdit;
        var form = new jun.ReturnPembelianWin({
            modez: (filter ? 1 : 2),
            id: r.return_pembelian_id,
            terima_material_id: this.record.get('terima_material_id'),
            title: (filter ? "Ubah" : "Lihat") + " Return Pembelian [" + ttl + "]",
            storeDetail: jun.rztReturnPembelianDetails,
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztReturnPembelianDetails.baseParams = {
            return_pembelian_id: r.return_pembelian_id
        };
        jun.rztReturnPembelianDetails.load();
        jun.rztReturnPembelianDetails.baseParams = {};
        if (filter) {
            form.griddetils.material.store.baseParams = {terima_material_id: r.terima_material_id};
            form.griddetils.material.store.load();
            form.griddetils.material.store.baseParams = {};
        }
    },
    btnPrintOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Return.");
            return;
        }
        Ext.getCmp("form-ReturnPembelianGrid").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReturnPembelianGrid").getForm().url = "Report/PrintTandaTerima";
        this.return_pembelian_id.setValue(selectedz.json.return_pembelian_id);
        var form = Ext.getCmp('form-ReturnPembelianGrid').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnDeliverOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data.");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status != RETURN_PEMBELIAN_OPEN) {
            msg = "Status data sudah terkirim.";
            Ext.MessageBox.alert("Warning", msg);
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Set status data menjadi terkirim?', this.deliver, this);
    },
    deliver: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'ReturnPembelian/Deliver/id/' + record.json.return_pembelian_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturnPembelian.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnNotaReturnOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Return.");
            return;
        }
        var r = this.sm.getSelected().json;
        var status = Number(r.status);
        if (status >= RETURN_PEMBELIAN_RECEIVED) {
            msg = "";
            switch (status) {
                case RETURN_PEMBELIAN_RECEIVED :
                    msg = "Barang pengganti telah diterima.";
                    break;
                case RETURN_PEMBELIAN_NOTA_RETURN :
                    msg = "Nota Return telah dibuat.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "Tidak dapat membuat Nota Return.<br>" + msg);
            return;
        }
        var rTM = new jun.TerimaBarangstore();
        rTM.load({
            params: {
                terima_barang_id: r.terima_barang_id
            },
            callback: function (r, o, s) {
                //console.log(r);
                if (Number(r[0].get('status')) >= SJ_SUPPLIER_INVOICED) {
                    Ext.getCmp('docs-jun.ReturnPembelianGrid').createNotaReturn(r[0]);
                } else {
                    Ext.MessageBox.alert("Warning", "Tidak dapat membuat Nota Return.<br> Belum ada Invoice untuk No. SJ : " + r[0].get('no_sj_supplier'));
                    return;
                }
            }
        });
    },
    createNotaReturn: function (tm) {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Return.");
            return;
        }
        var r = this.sm.getSelected().json;
        var form = new jun.NotaReturnWin({
            modez: 0,
            id: r.return_pembelian_id,
            title: "Buat Nota Return"
        });
        form.show(this);
        jun.rztNotaReturnDetails.load({
            params: {
                return_pembelian_id: r.return_pembelian_id
            },
            callback: function (r, o, s) {
                this.refreshData();
            }
        });
        jun.rztNotaReturnDetails.baseParams = {};
        form.nota_return_no_faktur_pajak.setValue(tm.get('no_faktur_tax'));
        form.nota_return_tgl_faktur_pajak.setValue(tm.get('tgl_inv'));
        var rSupp = jun.rztSupplierLib.getAt(jun.rztSupplierLib.findExact('supplier_id', this.record.get('supplier_id')));
        form.nota_return_penjual.setValue(rSupp.get('supplier_name'));
        form.nota_return_alamat_penjual.setValue(rSupp.get('alamat') + " " + rSupp.get('kota'));
        form.nota_return_npwp_penjual.setValue(rSupp.get('npwp'));
        form.nota_return_nama_pembeli.setValue(jun.recordSysPrefs('nama_terdaftar_pajak'));
        form.nota_return_pembeli.setValue(jun.recordSysPrefs('pbu_nama_badan_hukum'));
        form.nota_return_alamat.setValue(jun.recordSysPrefs('pbu_alamat'));
        form.nota_return_npwp.setValue(jun.recordSysPrefs('pbu_npwp'));
        form.nota_return_bruto.setValue(r.nota_return_bruto);
        form.nota_return_discount.setValue(r.nota_return_discount);
        form.nota_return_ppn.setValue(r.nota_return_ppn);
        form.nota_return_total.setValue(r.nota_return_total);
    },
    btnTandaTerimaOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Return.");
            return;
        }
        var r = this.sm.getSelected().json;
        var status = Number(r.status);
        if (status == RETURN_PEMBELIAN_NOTA_RETURN) {
            Ext.MessageBox.alert("Warning", "Tidak ada barang pengganti untuk data return ini.");
            return;
        }
        var form = new jun.TandaTerimaWin({
            modez: 0,
            storeDetails: jun.rztCreateTandaTerimaMaterialDetails,
            title: 'Buat Tanda Terima Material'
        });
        form.show(this);
        form.doc_ref_return.setValue(r.doc_ref);
        form.cmbSupplier.setValue(r.supplier_id);
        form.return_pembelian_id.setValue(r.return_pembelian_id);
        jun.rztCreateTandaTerimaMaterialDetails.baseParams = {return_pembelian_id: r.return_pembelian_id};
        jun.rztCreateTandaTerimaMaterialDetails.load();
        jun.rztCreateTandaTerimaMaterialDetails.baseParams = {};
        form.griddetils.material.store.baseParams = {return_pembelian_id: r.return_pembelian_id};
        form.griddetils.material.store.load();
        form.griddetils.material.store.baseParams = {};
    }
});
