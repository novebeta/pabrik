var readerTransferBarangDetail = new Ext.data.JsonReader({
    idProperty: 'transfer_barang_detail_id',
    fields: [
        {name: 'transfer_barang_detail_id'},
        {name: 'transfer_barang_id'},
        {name: 'kode_barang'}, //data sementara
        {name: 'nama_barang'}, //data sementara
        {name: 'barang_id'},
        {name: 'tgl_exp'},
        {name: 'qty', type: 'float'},
        {name: 'sat'},  //data sementara
        {name: 'batch'},
        {name: 'grup_id'}, //data sementara
        {name: 'tipe_barang_id'} //data sementara
    ]
});
jun.rztTransferBarangDetail = new Ext.data.GroupingStore({
    reader: readerTransferBarangDetail,
    url: 'TransferBarang/GetItems',
    sortInfo: {field: 'kode_barang', direction: 'ASC'}//,
    // groupField: 'grup_id'
});
//==== Store untuk Combo expired
jun.BarangBatchTransferStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangBatchTransferStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangBatchTransferStoreId',
            url: 'Barang/BatchTransfer',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'tgl_exp'},
                {name: 'batch'},
                {name: 'qty', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztBarangBatchTransfer = new jun.BarangBatchTransferStore();