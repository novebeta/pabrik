jun.CustomerPaymentUpdateTgl = Ext.extend(Ext.Window, {
    title: "Update TGL",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-CustomerPaymentUpdateTgl",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tgl',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tglfrom',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Update TGL",
                    ref: "../btnSave"
                }
            ]
        };
        jun.CustomerPaymentUpdateTgl.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        this.btnSave.setDisabled(true);
        var urlz = 'CustomerPayment/updateTgl/';
        Ext.getCmp('form-CustomerPaymentUpdateTgl').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                id: this.id
            },
            success: function (f, a) {
                jun.rztCustomerPayment.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnSave.setDisabled(false);
            }
        });
    }
});
jun.CustomerPaymentGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Customer Payments",
    id: 'docs-jun.CustomerPaymentGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        }, {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        // {
        //     header: 'Nama Customer',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'nama_customer',
        //     width: 100
        // },
        {
            header: 'No. BG/Cek',
            sortable: true,
            resizable: true,
            dataIndex: 'no_giro',
            width: 100
        },
        {
            header: 'No. Bukti',
            sortable: true,
            resizable: true,
            dataIndex: 'no_bukti',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            align: "right",
            width: 100,
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztCustomerPayment;
        jun.rztCustomerPayment.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglcustomerpaymentgridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztGiroCmp.getTotalCount() === 0) {
            jun.rztGiroCmp.load({
                params: {
                    'used': 0
                }
            });
        }
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create Customer Payment',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Customer Payment',
                    ref: '../btnView'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Update Tgl',
                    ref: '../btnUpdateTgl'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Void Customer Payment',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Customer Payment',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglcustomerpaymentgridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportCustPay",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "customer_payment_id",
                            ref: "../../customer_payment_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
                //,
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'label',
                //    style: 'margin:5px',
                //    text: 'Tgl :'
                //},
                //{
                //    xtype: 'xdatefield',
                //    id: 'tglcustpaymentgridid',
                //    ref: '../tgl'
                //}
            ]
        };
        this.store.reload();
        jun.CustomerPaymentGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnView.on('Click', this.loadEditForm, this);
        this.btnUpdateTgl.on('Click', this.loadEditTgl, this);
        this.btnEdit.on('Click', this.deleteRec, this);
        this.tgl.on('select', this.selectTgl, this);
        //this.tgl.on('select', function () {
        //    this.store.reload();
        //}, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnPrint.on('Click', this.print, this);
        this.store.removeAll();
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    loadEditTgl: function (t, e) {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Customer Payment");
            return;
        }
        var idz = selectedz.json.customer_payment_id;
        var form = new jun.CustomerPaymentUpdateTgl({id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    print: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Customer Payment");
            return;
        }
        // var idz = selectedz.json.breakdown_id;
        Ext.getCmp("form-ReportCustPay").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCustPay").getForm().url = "Report/PrintCustPay";
        this.customer_payment_id.setValue(selectedz.json.customer_payment_id);
        var form = Ext.getCmp('form-ReportCustPay').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CustomerPaymentWin({modez: 0});
        form.show();
    },
    loadEditForm: function (t, e) {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Customer Payment");
            return;
        }
        var idz = selectedz.json.customer_payment_id;
        var modez = 2;
        if (t.getText() == 'Edit Customer Payment') {
            modez = 1;
        }
        var form = new jun.CustomerPaymentWin({modez: modez, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //jun.rztCustomerPaymentDetil.load({customer_payment_id: idz});
        jun.rztCustomerPaymentDetil.baseParams = {
            customer_payment_id: idz
        };
        jun.rztCustomerPaymentDetil.load();
        jun.rztCustomerPaymentDetil.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin void data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.data.void == '1') {
            Ext.MessageBox.alert("Warning", "Customer Payment sudah di void.");
            return;
        }
        Ext.Ajax.request({
            url: 'CustomerPayment/Void/id/' + record.json.customer_payment_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztCustomerPayment.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
