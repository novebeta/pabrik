jun.AssemblyDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AssemblyDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssemblyDetailsStoreId',
            url: 'AssemblyDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'assembly_detail_id'},
                {name: 'assembly_id'},
                {name: 'material_id'},
                {name: 'qty'},
                {name: 'price_std'},
                {name: 'total_std'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
jun.rztAssemblyDetails = new jun.AssemblyDetailsstore();
//jun.rztAssemblyDetails.load();
