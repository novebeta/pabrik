jun.SecurityRolesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SecurityRolesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SecurityRolesStoreId',
            url: 'SecurityRoles',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'security_roles_id'},
                {name: 'role'},
                {name: 'ket'},
                {name: 'sections'},
                {name: '001'},
                {name: '002'},
                {name: '100'},
                {name: '101'},
                {name: '102'},
                {name: '103'},
                {name: '104'},
                {name: '105'},
                {name: '106'},
                {name: '107'},
                {name: '108'},
                {name: '109'},
                {name: '110'},
                {name: '111'},
                {name: '112'},
                {name: '113'},
                {name: '114'},
                {name: '115'},
                {name: '116'},
                {name: '117'},
                {name: '118'},
                {name: '200'},
                {name: '201'},
                {name: '202'},
                {name: '203'},
                {name: '204'},
                {name: '205'},
                {name: '206'}, //TransferBarangGrid
                {name: '207'},
                {name: '208'},
                {name: '209'},
                {name: '210'},
                {name: '211'},
                {name: '212'},
                {name: '213'},
                {name: '214'},
                {name: '215'},
                {name: '216'},
                {name: '217'},
                {name: '218'},
                {name: '219'},
                {name: '220'},
                {name: '233'},
                {name: '234'},
                {name: '235'},
                {name: '236'},
                {name: '237'},
                {name: '238'},
                {name: '239'},
                {name: '240'},
                {name: '241'}, //Giro
                // {name: '221'}, //PR
                // {name: '222'}, //PO
                // {name: '223'}, //SJ Supplier
                // {name: '224'}, //Inv Supllier
                // {name: '225'}, //Pembayaran Supllier
                // {name: '226'}, //Return Pembelian
                // {name: '227'}, //Nota return
                // {name: '228'}, //Tanda Terima Material
                // {name: '229'}, //Laporan FA
                // {name: '230'}, //Form Perbaikan (teknik)
                // {name: '231'}, //Tandaterima sparepart (teknik)
                // {name: '232'}, //Tkonversi sparepart -> barkas (teknik)
                {name: '300'},
                {name: '301'},
                {name: '302'}, //Mutasi Stok (Material)
                {name: '303'},
                {name: '304'},
                {name: '305'}, //Kartu Stok (Material)
                {name: '306'}, //laporan produksi
                {name: '307'},
                {name: '308'},
                {name: '309'},
                {name: '310'},
                {name: '311'},
                {name: '312'},
                {name: '313'},
                {name: '314'},
                {name: '315'},
                {name: '316'},
                {name: '317'},
                {name: '318'},
                {name: '319'},
                {name: '320'},
                {name: '321'},
                {name: '322'},
                {name: '323'},
                {name: '324'}, //rekap PO
                {name: '325'}, //rekap SJ Suplier /terima material
                {name: '326'}, //rekap tagihan /supplier invoice
                {name: '327'}, //rekap Pembayaran Supplier / FPT
                {name: '328'}, //rekap Pembelian
                {name: '329'}, //rekap Transfer Material
                {name: '330'}, //rekap Transfer Barang
                {name: '331'},
                {name: '332'},
                {name: '333'},
                {name: '334'},
                {name: '335'},
                {name: '400'},
                {name: '401'},
                {name: '402'},
                {name: '403'},
                {name: '500'}, //pustaka QA
                {name: '501'}, //jenis dokumen QA
                {name: '502'}, //jenis kegiatan QA
                {name: '503'}, //kategori activity QA
                {name: '504'},  //level doc
                {name: '505'}  //level doc
            ]
        }, cfg));
    }
});
jun.rztSecurityRoles = new jun.SecurityRolesstore();
//jun.rztSecurityRoles.load();
