jun.Sjintstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Sjintstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjintStoreId',
            url: 'Sjint',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sjint_id'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'sj_id'},
                {name: 'customer_id'},
                {name: 'sjsim_id'},
                {name: 'tgl'}
            ]
        }, cfg));
    }
});
jun.rztSjint = new jun.Sjintstore();
//jun.rztSjint.load();
