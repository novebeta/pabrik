jun.Sjstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Sjstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjStoreId',
            url: 'Sj',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'doc_ref'},
                {name: 'total', type: 'float'},
                {name: 'customer_id'},
                {name: 'tgl'},
                {name: 'adt'},
                {name: 'doc_ref_inv'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'no_faktur_pajak'},
                {name: 'tipe_material_id'},
                {name: 'parent_id'},
                {name: 'sales_id'},
                {name: 'loc_code'},
                {name: 'tgl_inv'},
                {name: 'inv_id_user'},
                {name: 'arus'},
                {name: 'inv_tdate'},
                {name: 'status'},
                {name: 'lunas'},
                {name: 'tot_pot1', type: 'float'},
                {name: 'tot_pot2', type: 'float'},
                {name: 'tot_pot3', type: 'float'},
                {name: 'tot_bruto', type: 'float'},
                {name: 'tot_vatrp', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztSj = new jun.Sjstore({baseParams: {status: 0}});
jun.rztSjCetakInvoice = new jun.Sjstore({baseParams: {status: 1}});
//jun.rztSjInvoiceCmp = new jun.Sjstore();
//jun.rztSj.load();
