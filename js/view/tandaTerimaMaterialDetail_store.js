jun.TandaTerimaMaterialDetailsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TandaTerimaMaterialDetailsStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TandaTerimaMaterialDetailsStoreId',
            url: 'TandaTerimaMaterialDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tandaterima_detail_id'},
                {name: 'tandaterima_id'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'tgl_expired', type: 'date'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.rztTandaTerimaMaterialDetails = new jun.TandaTerimaMaterialDetailsStore();
jun.rztCreateTandaTerimaMaterialDetails = new jun.TandaTerimaMaterialDetailsStore({url: 'ReturnPembelianDetails'});
//Store untuk Returned items, yang dipakai saat penerimaan tanda terima
jun.ReturnedItemsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturnedItemsStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturnedItemsStoreId',
            url: 'TandaTerimaMaterial/GetReturnedItems',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'kode_material'},
                {name: 'nama_material'},
                {name: 'sat'},
                {name: 'qty_terima', type: 'float'},
                {name: 'tgl_expired', type: 'date'}
            ]
        }, cfg));
    }
});