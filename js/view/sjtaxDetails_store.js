jun.SjtaxDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SjtaxDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjtaxDetailsStoreId',
            url: 'SjtaxDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sjtax_details_id'},
                {name: 'qty'},
                {name: 'price'},
                {name: 'total'},
                {name: 'barang_id'},
                {name: 'sjtax_id'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'ket'}
            ]
        }, cfg));
    }
});
jun.rztSjtaxDetails = new jun.SjtaxDetailsstore();
//jun.rztSjtaxDetails.load();
