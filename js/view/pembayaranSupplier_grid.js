jun.PembayaranSupplierGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pembayaran Supplier",
    id: 'docs-jun.PembayaranSupplierGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. FPT',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 100,
            renderer: jun.renderNamaSuppliers
        },
        {
            header: 'Metode Bayar',
            sortable: true,
            resizable: true,
            dataIndex: 'cara_pembayaran',
            width: 100
        },
        {
            header: 'Tanggal Bayar',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_bayar',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 40,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case FPT_OPEN :
                        return 'OPEN';
                    case FPT_PAID :
                        return 'PAID';
                }
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actPrint, actConfirm) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
        this.actConfirm = actConfirm;
        //this.viewAllData = (this.actPrint || this.actConfirm  || UROLE==USER_PEMBELIAN);
    },
    initComponent: function () {
        this.store = jun.rztPembayaranSupplier;
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1, 1);
                break;
            case USER_PPIC:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_GA:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_PEMBELIAN:
                this.userPermission(1, 1, 1, 1);
                break;
            default:
                this.userPermission(0, 0, 0, 0);
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat FPT',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print FPT',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Konfirmasi Pembayaran',
                    ref: '../btnKonfirmasiPembayaran',
                    iconCls: 'silk13-money',
                    hidden: this.actConfirm ? false : true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_fpt: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PembayaranSupplierGrid').setFilterData(m.status_fpt);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_fpt: FPT_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PembayaranSupplierGrid').setFilterData(m.status_fpt);
                                    }
                                }
                            },
                            {
                                text: 'PAID',
                                status_fpt: FPT_PAID,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PembayaranSupplierGrid').setFilterData(m.status_fpt);
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-printFPT",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "pembayaran_supplier_id",
                            ref: "../../pembayaran_supplier_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.PembayaranSupplierGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printFPT, this);
        this.btnKonfirmasiPembayaran.on('Click', this.KonfirmasiPembayaran, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(FPT_OPEN);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    setFilterData: function (sts) {
        this.status_fpt = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        this.store.baseParams = {
            mode: "grid",
            status: this.status_fpt
        };
        this.store.reload();
    },
    loadForm: function () {
        var form = new jun.PembayaranSupplierWin({
            modez: 0,
            title: "Buat FPT",
            konfirmasiBayar: false,
            iconCls: 'silk13-add'
        });
        form.show(this);
        form.nama_rekening.setValue(
            jun.rztSysPrefs.getAt(jun.rztSysPrefs.findExact('name_', 'pbu_nama_rekening')).get('value_')
        );
        form.no_rekening.setValue(
            jun.rztSysPrefs.getAt(jun.rztSysPrefs.findExact('name_', 'pbu_nomor_rekening')).get('value_')
        );
        form.bank.setValue(
            jun.rztSysPrefs.getAt(jun.rztSysPrefs.findExact('name_', 'pbu_bank')).get('value_')
        );
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data FPT.");
            return;
        }
        var idz = selectedz.json.pembayaran_supplier_id;
        var filter = selectedz.json.status == FPT_OPEN;
        var form = new jun.PembayaranSupplierWin({
            modez: (filter ? 1 : 2),
            id: idz,
            title: (filter ? "Edit" : "View") + " FPT",
            konfirmasiBayar: false,
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.reloadComboStore(this.record.get('supplier_id'), this.record.get('pembayaran_supplier_id'));
        jun.rztPembayaranSupplierDetails.baseParams = {pembayaran_supplier_id: idz};
        jun.rztPembayaranSupplierDetails.load();
        jun.rztPembayaranSupplierDetails.baseParams = {};
    },
    printFPT: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data FPT");
            return;
        }
        if (selectedz.json.status != SJ_SUPPLIER_OPEN) {
            Ext.MessageBox.alert("Warning", "Data FPT ini telah dibayar.");
            return;
        }
        Ext.getCmp("form-printFPT").getForm().standardSubmit = !0;
        Ext.getCmp("form-printFPT").getForm().url = "Report/PrintFPT";
        this.pembayaran_supplier_id.setValue(selectedz.json.pembayaran_supplier_id);
        var form = Ext.getCmp('form-printFPT').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    KonfirmasiPembayaran: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Penerimaan.");
            return;
        }
        if (selectedz.json.status != SJ_SUPPLIER_OPEN) {
            Ext.MessageBox.alert("Warning", "Data FPT ini telah dibayar.");
            return;
        }
        var idz = selectedz.json.pembayaran_supplier_id;
        var form = new jun.PembayaranSupplierWin({
            modez: 1,
            id: idz,
            title: "Konfirmasi Pembayaran FPT",
            konfirmasiBayar: true,
            iconCls: 'silk13-money'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.reloadComboStore(this.record.get('supplier_id'), this.record.get('pembayaran_supplier_id'));
        jun.rztPembayaranSupplierDetails.baseParams = {pembayaran_supplier_id: idz};
        jun.rztPembayaranSupplierDetails.load();
        jun.rztPembayaranSupplierDetails.baseParams = {};
    }
});
