jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 800,
    height: 500,
    layout: "form",
    modal: !0,
    padding: 3,
//    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout: "column",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Group",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "101"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Barang",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "102"
                            // },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Material",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "103"
                            },
//                            {
//                                fieldLabel: "",
//                                columnWidth: .33,
//                                boxLabel: "Packaging Material",
//                                value: 0,
//                                inputValue: 1,
//                                uncheckedValue: 0,
//                                name: "104"
//                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Bill Of Material",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Bank",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "106"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Chart Of Account",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "107"
                            },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Wilayah",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "108"
                            // },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Wilayah Pajak",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "109"
                            },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Harga Jual",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "110"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Harga Pajak",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "111"
                            // },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Customer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "112"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "113"
                            },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Tipe Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "115"
                            // },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Salesman",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "116"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Gudang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "117"
                            },
                            //,
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Deposit Faktur Pajak",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "203"
                            // }
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Merk",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "118"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout: "column",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Purchase Request",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "201"
                            // },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Purchase Order",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "202"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Surat Jalan Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "203"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Invoice Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "204"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Pembayaran Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "205"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Return Pembelian",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "206"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Nota Return",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "207"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Tanda Terima Material",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "208"
                            },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Production Order",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "209"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Transfer Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "210"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Penerimaan Barang",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "204"
                            // },
//                            {
//                                fieldLabel: "",
//                                columnWidth: .33,
//                                boxLabel: "Penerimaan Material",
//                                value: 0,
//                                inputValue: 1,
//                                uncheckedValue: 0,
//                                name: "205"
//                            },
//                             {
//                                 fieldLabel: "",
//                                 columnWidth: .33,
//                                 boxLabel: "Penjualan Tunai",
//                                 value: 0,
//                                 inputValue: 1,
//                                 uncheckedValue: 0,
//                                 name: "220"
//                             },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Penjualan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "220"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Retur Penjualan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "240"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Surat Jalan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "211"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Cetak Invoice",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "212"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Credit Note",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "213"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Giro",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "241"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Pembayaran Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "236"
                            },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Transfer Barang",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "214"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Retur Beli",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "207"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Retur Beli Raw Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "208"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Retur Beli Packaging Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "209"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Tanda Terima Raw Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "210"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Tanda Terima Packaging Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "211"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Barang Keluar Raw Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "212"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Barang Keluar Packaging Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "213"
                            // },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Kas/Bank Masuk",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "215"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Kas/Bank Keluar",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "216"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Kas/Bank Transfer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "217"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Generate Profit Lost",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "218"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "219"
                            },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Sampel QC",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "220"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Tanda Terima Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "228"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Laporan FA",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "229"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Perbaikan Mesin/Utility",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "230"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Tanda Terima Sparepart",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "231"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Konversi Sparepart > Barang Bekas",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "232"
                            // }
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Assembly",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "233"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Breakdown",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "234"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Konversi Barang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "235"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Kelola Persediaan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "239"
                            }
                            //,
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Service Masuk",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "237"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Service Keluar",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "238"
                            // }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout: "column",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Mutasi Stok",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "301"
                            // },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Mutasi Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
//                            {
//                                fieldLabel: "",
//                                columnWidth: .33,
//                                boxLabel: "Mutasi Stok (Packaging)",
//                                value: 0,
//                                inputValue: 1,
//                                uncheckedValue: 0,
//                                name: "303"
//                            },
//                             {
//                                 fieldLabel: "",
//                                 columnWidth: .33,
//                                 boxLabel: "Kartu Stok",
//                                 value: 0,
//                                 inputValue: 1,
//                                 uncheckedValue: 0,
//                                 name: "304"
//                             },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Kartu Stok",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "305"
                            },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Laporan Produksi",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "306"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Barang Keluar",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "307"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Penerimaan Barang Detail",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "308"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Retur Beli Detail",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "309"
                            // },
                            // {
                            //     fieldLabel: "",
                            //     columnWidth: .33,
                            //     boxLabel: "Surat Jalan Internal Detail",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "310"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Invoice Internal Detail",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "311"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Surat Jalan Pajak Detail",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "312"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Invoice Pajak Detail",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "313"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Credit Note Detail",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "314"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Stok Produksi",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "315"
                            // },
                            {
                                columnWidth: .33,
                                boxLabel: "General Ledger",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "316"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "317"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Profit Lost",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "318"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Neraca",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "319"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Rekening Koran",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "320"
                            },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "E-Faktur",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "321"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "List Invoice",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "322"
                            // },
                            {
                                columnWidth: .33,
                                boxLabel: "Rekap Invoice Hutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "331"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Rincian Pembelian",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "332"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Rekap Invoice Piutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "323"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Report Giro",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "333"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Report Buku Piutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "334"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Report Buku Hutang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "335"
                            },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Rekap Purchase Order",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "324"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Rekap Surat Jalan Supplier",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "325"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Rekap Invoice Supplier",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "326"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Rekap FPT",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "327"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Rekap Pembelian",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "328"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Rekap Transfer Material",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "329"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Rekap Transfer Barang",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "330"
                            // }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: "column",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            }
                        ]
                    },
                    // {
                    //     xtype: "panel",
                    //     title: "QA Section",
                    //     layout: "column",
                    //     bodyStyle: "padding: 10px",
                    //     defaultType: "checkbox",
                    //     items: [
                    //         {
                    //             fieldLabel: "",
                    //             columnWidth: .33,
                    //             boxLabel: "Dokumn QA",
                    //             value: 0,
                    //             inputValue: 1,
                    //             uncheckedValue: 0,
                    //             name: "500"
                    //         },
                    //         {
                    //             fieldLabel: "",
                    //             columnWidth: .33,
                    //             boxLabel: "Master: Jenis Dokumen QA",
                    //             value: 0,
                    //             inputValue: 1,
                    //             uncheckedValue: 0,
                    //             name: "501"
                    //         },
                    //         {
                    //             fieldLabel: "",
                    //             columnWidth: .33,
                    //             boxLabel: "Master: Jenis Kegiatan QA",
                    //             value: 0,
                    //             inputValue: 1,
                    //             uncheckedValue: 0,
                    //             name: "502"
                    //         },
                    //         {
                    //             fieldLabel: "",
                    //             columnWidth: .33,
                    //             boxLabel: "Master: Kategori Kegiatan QA",
                    //             value: 0,
                    //             inputValue: 1,
                    //             uncheckedValue: 0,
                    //             name: "503"
                    //         },
                    //         {
                    //             fieldLabel: "",
                    //             columnWidth: .33,
                    //             boxLabel: "Master: Level Dokumen QA",
                    //             value: 0,
                    //             inputValue: 1,
                    //             uncheckedValue: 0,
                    //             name: "504"
                    //         },
                    //         {
                    //             fieldLabel: "",
                    //             columnWidth: .33,
                    //             boxLabel: "Masterlist Dokumen Internal",
                    //             value: 0,
                    //             inputValue: 1,
                    //             uncheckedValue: 0,
                    //             name: "505"
                    //         }
                    //     ]
                    // },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: "column",
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            },
                            {
                                fieldLabel: "",
                                columnWidth: .33,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "002"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/";
        Ext.getCmp("form-SecurityRoles").getForm().submit({
            url: a,
            timeOut: 1e3,
            scope: this,
            success: function (a, b) {
                jun.rztSecurityRoles.reload();
                var c = Ext.decode(b.response.responseText);
                this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
            },
            failure: function (a, b) {
                Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                this.btnDisabled(!1);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});