jun.ServiceDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ServiceDetails",
    id: 'docs-jun.ServiceDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            renderer: jun.renderBarang,
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztServiceDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Barang :'
                },
                {
                    xtype: 'combo',
                    // style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztBarangCmp, //jun.rztMaterialCmp,
                    valueField: 'barang_id',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<span style="width:100%;display:inline-block;"><b>{kode_barang}</b><br>{nama_barang}</span>',
                        "</div></tpl>"),
                    allowBlank: false,
                    listWidth: 400,
                    ref: '../barang',
                    id: 'barangdetailsid',
                    displayField: 'kode_barang'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Qty :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../qty',
                    width: 75,
                    value: 1,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    margins: 5,
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    margins: 5,
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    margins: 5,
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.ServiceDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected");
            return
        }
        var qty = parseFloat(this.qty.getValue());
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('jml', qty);
            record.commit();
        } else {
            var c = jun.rztServiceDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    jml: qty
                });
            jun.rztServiceDetails.add(d);
        }
        this.barang.reset();
        this.qty.reset();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.jml);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
