jun.CreditNoteWin = Ext.extend(Ext.Window, {
    title: 'Credit Note',
    modez: 1,
    width: 950,
    height: 470,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-CreditNote',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. CN:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode_customer',
                            'nama_customer'
                        ],
                        mode: 'local',
                        store: jun.rztCustomersCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Customer</th><th>Nama Customer</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_customer}</td><td>{nama_customer}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        matchFieldWidth: !1,
                        allowBlank: false,
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../customer',
                        displayField: 'kode_customer',
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    // {
                    //     xtype: "label",
                    //     text: "Grup Barang:",
                    //     x: 295,
                    //     y: 5
                    // },
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     ref: '../grup',
                    //     forceSelection: true,
                    //     fieldLabel: 'Grup',
                    //     store: jun.rztGrupCmp,
                    //     emptyText: "ALL",
                    //     hiddenName: 'grup_id',
                    //     valueField: 'grup_id',
                    //     displayField: 'nama_grup',
                    //     allowBlank: true,
                    //     x: 400,
                    //     y: 2,
                    //     width: 175
                    // },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y', x: 85,
                        y: 32,
                        width: 175
                    },
                    new jun.CreditNoteDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: "label",
                        text: "Total :",
                        x: 672,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'total_credite_note_id',
                        ref: '../doc_ref',
                        width: 192,
                        readOnly: true,
                        value: 0,
                        x: 722,
                        y: 362
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CreditNoteWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.grup.on('change', this.onGrupChange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onGrupChange: function (combo, record, index) {
        var grup = combo.getValue();
        jun.rztBarangCmp.baseParams = {
            grup_id: grup
        };
        jun.rztBarangCmp.reload();
        jun.rztBarangCmp.baseParams = {};
        this.griddetils.barang.reset();
        this.griddetils.batch.reset();
        this.griddetils.tgl_exp.reset();
        this.griddetils.qty.reset();
        this.griddetils.price.reset();
    },
    onActivate: function () {
        jun.rztCreditNoteDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'CreditNote/create/';
        Ext.getCmp('form-CreditNote').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztCreditNoteDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztCreditNote.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    Ext.getCmp('form-CreditNote').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});