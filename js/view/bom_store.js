jun.Bomstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Bomstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BomStoreId',
            url: 'bom',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bom_id'},
                {name: 'material_id'},
                {name: 'no_bom'},
                {name: 'kode_formula'},
                {name: 'tgl', type: 'date'},
                {name: 'bentuk_sediaan'},
                {name: 'qty'},
                {name: 'sat'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'enable'},
                {name: 'kode_material'},  //tidak disimpan di database
                {name: 'nama_material'}   //tidak disimpan di database
            ]
        }, cfg));
    }
});
jun.rztBom = new jun.Bomstore();
//jun.rztBomLib = new jun.Bomstore();
//jun.rztBomCmp = new jun.Bomstore();
////========================================
jun.BomDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BomDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BomDetailstoreId',
            url: 'bomdetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bom_detail_id'},
                {name: 'bom_id'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
//jun.rztBomDetail = new jun.BomDetailstore();
var readerBomDetail = new Ext.data.JsonReader({
    idProperty: 'taskId',
    fields: [
        {name: 'bom_detail_id'},
        {name: 'bom_id'},
        {name: 'material_id'},
        {name: 'kode_material'},
        {name: 'nama_material'},
        {name: 'qty', type: 'float'},
        {name: 'sat'},
        {name: 'tipe_material_id'},
        {name: 'loc_code'}
    ]
});
jun.rztBomDetail = new Ext.data.GroupingStore({
    reader: readerBomDetail,
    url: 'BomDetail/GetItems',
    sortInfo: {field: 'kode_material', direction: 'ASC'},
    groupField: 'tipe_material_id'
});
//==========================
jun.BomVsBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BomVsBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BomVsBarangStoreId',
            url: 'Bom/BomProduk',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bom_id'},
                {name: 'barang_id'},
                {name: 'no_bom'},
                {name: 'kode_formula'},
                {name: 'tgl'},
                {name: 'bentuk_sediaan'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'kode_barang_rnd'},
                {name: 'kode_barang'},
                {name: 'kode_barang_2'},
                {name: 'nama_barang'},
                {name: 'grup_id'},
                {name: 'qty_per_pot', type: 'float'},
                {name: 'qty_per_box', type: 'float'},
                {name: 'tipe_barang_id'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
jun.rztBomLib = new jun.BomVsBarangstore();
jun.rztBomLibForCreateBatch = new jun.BomVsBarangstore({baseParams: {enable: 1}});
