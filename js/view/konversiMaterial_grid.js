jun.KonversiMaterialGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Konversi Material",
    id: 'docs-jun.KonversiMaterialGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        }
    ],
    initComponent: function () {

        jun.rztKonversiMaterial.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglkonversimaterialgridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztKonversiMaterial;
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Konversi Material',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Konversi Material',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglkonversimaterialgridid',
                    ref: '../tgl',
                    noPastYears: false
                })
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.KonversiMaterialGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.tgl.on('select', this.selectTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KonversiMaterialWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.konversi_material_id;
        var form = new jun.KonversiMaterialWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztKonversiMaterialDetails.baseParams = {
            konversi_material_id: idz
        };
        jun.rztKonversiMaterialDetails.load();
        jun.rztKonversiMaterialDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'KonversiMaterial/delete/id/' + record.json.konversi_material_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKonversiMaterial.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
