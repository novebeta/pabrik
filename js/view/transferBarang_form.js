jun.TransferBarangWin = Ext.extend(Ext.Window, {
    title: 'Transfer Barang',
    modez: 1,
    width: 900,
    height: 480,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-TransferBarang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Nomor",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 75,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        allowBlank: false,
                        format: 'd M Y',
                        value: new Date(),
                        x: 75,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Pengirim",
                        x: 290,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztStorageLocation,
                        hiddenName: 'pengirim',
                        valueField: 'id',
                        displayField: 'name',
                        value: UROLE,
                        ref: '../pengirim',
                        allowBlank: false,
                        readOnly: true,
                        width: 175,
                        x: 365,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Penerima",
                        x: 290,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztStorageLocation,
                        hiddenName: 'penerima',
                        valueField: 'id',
                        displayField: 'name',
                        ref: '../penerima',
                        allowBlank: false,
                        width: 175,
                        x: 365,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Note",
                        x: 5 + 285 + 285,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../note',
                        name: 'note',
                        width: 240,
                        height: 50,
                        x: 365 + 175 + 75 + 10,
                        y: 2
                    },
                    new jun.TransferBarangDetailGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        anchor: '100% 100%',
                        trans_id: this.trans_id,
                        readOnly: ((this.modez < 2) ? false : true)
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Setuju',
                    ref: '../btnAccept',
                    iconCls: 'silk13-tick'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferBarangWin.superclass.initComponent.call(this);
        this.btnAccept.on('click', this.onbtnAcceptclick, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.penerima.on('select', this.onSelectPenerima, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnAccept.setVisible(false);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnAccept.setVisible(false);
            this.btnSave.setVisible(false);
        } else { //view
            this.btnAccept.setVisible(this.accepting ? true : false);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onSelectPenerima: function (c, r, i) {
        if (c.getValue() == this.pengirim.getValue()) c.reset();
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-TransferBarang').getForm().submit({
            url: 'TransferBarang/create/',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                trans_id: this.trans_id,
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                //jun.rztTransferBarang.reload();
                if (Ext.getCmp('docs-jun.TransferBarangGrid') !== undefined) Ext.getCmp('docs-jun.TransferBarangGrid').reloadStore();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferBarang').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnAcceptclick: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-TransferBarang').getForm().submit({
            url: 'TransferBarang/Accept/id/' + this.trans_id,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                if (jun.rztTransferBarang.getTotalCount() !== 0) jun.rztTransferBarang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});