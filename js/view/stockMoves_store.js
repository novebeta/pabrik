jun.StockMovesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StockMovesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StockMovesStoreId',
            url: 'StockMoves',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'stock_moves_id'},
                {name: 'type_no'},
                {name: 'trans_no'},
                {name: 'tran_date'},
                {name: 'price'},
                {name: 'reference'},
                {name: 'qty'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'barang_id'}
            ]
        }, cfg));
    }
});
jun.rztStockMoves = new jun.StockMovesstore();
//jun.rztStockMoves.load();
jun.KelolaStokStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KelolaStokStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KelolaStokStoreId',
            url: 'StockMoves/kelolaStok',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'trans_no'},
                {name: 'trans_date'},
                {name: 'reference'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.KelolaStokDetilStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KelolaStokDetilStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KelolaStokDetilStoreId',
            url: 'StockMoves/kelolaStokDetil',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'qty'},
                {name: 'storage_location'}
            ]
        }, cfg));
    }
});
jun.rztKelolaStok = new jun.KelolaStokStore();
jun.rztKelolaStokDetil = new jun.KelolaStokDetilStore();