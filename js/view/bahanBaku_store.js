jun.BahanBakustore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BahanBakustore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BahanBakuStoreId',
            url: 'BahanBaku',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'sat'},
                {name: 'grup_id'},
                {name: 'qty_per_box'},
                {name: 'qty_per_pot'},
                {name: 'kode_barang_2'},
                {name: 'kode_barang_rnd'},
                {name: 'tipe_barang_id'}
            ]
        }, cfg));
    }
});
jun.rztBahanBaku = new jun.BahanBakustore();
jun.rztBahanBakuCmp = new jun.BahanBakustore();
jun.rztBahanBakuLib = new jun.BahanBakustore();
//jun.rztBahanBaku.load();
