jun.Refsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Refsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RefsStoreId',
            url: 'Refs',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'refs_id'},
                {name: 'type_no'},
                {name: 'type_'},
                {name: 'reference'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztRefs = new jun.Refsstore();
//jun.rztRefs.load();
