jun.TransferBarangDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TerimaBarangDetails",
    id: 'docs-jun.TransferBarangDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // view: new Ext.grid.GroupingView({
    //     forceFit: true,
    //     groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})',
    //     showGroupName: false,
    //     enableNoGroups: false,
    //     enableGroupingMenu: true,
    //     hideGroupedColumn: true
    // }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 90
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 120
        },
        // {
        //     header: 'Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_exp',
        //     width: 50,
        //     renderer: Ext.util.Format.dateRenderer('M-Y')
        // },
        // {
        //     header: 'Batch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'batch',
        //     width: 80
        // },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer(),
            summaryType: 'sum'
        }//,
        // {
        //     header: 'Satuan',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'sat',
        //     width: 40
        // },
        // {
        //     header: 'Tipe Barang',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tipe_barang_id',
        //     width: 40,
        //     renderer: jun.renderTipeBarang
        // },
        // {
        //     header: 'Grup Barang',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'grup_id',
        //     width: 40,
        //     renderer: jun.renderGrupBarang
        // }
    ],
    initComponent: function () {
        this.store = jun.rztTransferBarangDetail;
        // if (!this.readOnly) {
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangCmp,
                            valueField: 'barang_id',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_barang}</b><br>{kode_barang_2}</span>',
                                "</div></tpl>"),
                            ref: '../../barang',
                            displayField: 'kode_barang'
                        },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     text: 'Batch :'
                        // },
                        // {
                        //     xtype: 'combo',
                        //     style: 'margin-bottom:2px',
                        //     typeAhead: true,
                        //     triggerAction: 'all',
                        //     mode: 'local',
                        //     lazyRender: true,
                        //     autoSelect: false,
                        //     hideTrigger: true,
                        //     editable: false,
                        //     matchFieldWidth: !1,
                        //     pageSize: 20,
                        //     itemSelector: "div.search-item",
                        //     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        //         '<span style="width:190px;display:inline-block;">{batch}</span>',
                        //         '<span style="width:90px;display:inline-block;">{tgl_exp:date("F Y")}</span>',
                        //         '<span style="width:80px;display:inline-block;float:right;text-align:right;">{qty:number( "0,0" )}</span>',
                        //         "</div></tpl>"),
                        //     allowBlank: false,
                        //     listWidth: 420,
                        //     width: 150,
                        //     forceSelection: true,
                        //     store: jun.rztBarangBatchTransfer,
                        //     valueField: 'batch',
                        //     ref: '../../batch',
                        //     displayField: 'batch'
                        // },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     text: 'Expired:'
                        // },
                        // {
                        //     xtype: 'xdatefield',
                        //     ref: '../../tgl_exp2',
                        //     format: 'M-Y',
                        //     readOnly: true
                        // },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 70,
                            value: 1,
                            minValue: 0
                        },
                        // {
                        //     xtype: 'hidden',
                        //     ref: '../../tgl_exp'
                        // },
                        {
                            xtype: 'hidden',
                            ref: '../../kode_barang'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../nama_barang'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../grup_id'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../tipe_barang_id'
                        }//,
                        // {
                        //     xtype: 'hidden',
                        //     ref: '../../sat'
                        // }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'small',
                        width: 40
                        //height: 44
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        // }
        jun.TransferBarangDetailGrid.superclass.initComponent.call(this);
        // if (!this.readOnly) {
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        // this.barang.on('select', this.onSelectBrg, this);
        // this.batch.on('select', this.onSelectBatch, this);
        // }
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onSelectBrg: function (c, r, i) {
        this.loadStoreComboBatch(r.data.barang_id);
        this.qty.reset();
        // this.batch.reset();
        // this.tgl_exp.reset();
        // this.tgl_exp2.reset();
        this.kode_barang.setValue(r.get('kode_barang'));
        this.nama_barang.setValue(r.get('nama_barang'));
        // this.sat.setValue(r.get('sat'));
        this.grup_id.setValue(r.get('grup_id'));
        this.tipe_barang_id.setValue(r.get('tipe_barang_id'));
    },
    loadStoreComboBatch: function (v) {
        if (this.trans_id) {
            $param = {params: {barang_id: v, trans_no: this.trans_id}};
        } else {
            $param = {params: {barang_id: v}};
        }
        this.batch.store.load($param);
    },
    onSelectBatch: function (combo, record, index) {
        //this.batch.setValue(record.data.batch);
        this.tgl_exp.setValue(record.data.tgl_exp);
        this.tgl_exp2.setValue(record.data.tgl_exp);
        this.qty.setValue(record.data.qty);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.loadStoreComboBatch(record.get('barang_id'));
            this.barang.setValue(record.get('barang_id'));
            // this.batch.setValue(record.get('batch'));
            // this.tgl_exp.setValue(record.get('tgl_exp'));
            // this.tgl_exp2.setValue(new Date(record.get('tgl_exp')));
            this.qty.setValue(record.get('qty'));
            this.kode_barang.setValue(record.get('kode_barang'));
            this.nama_barang.setValue(record.get('nama_barang'));
            // this.sat.setValue(record.get('sat'));
            this.grup_id.setValue(record.get('grup_id'));
            this.tipe_barang_id.setValue(record.get('tipe_barang_id'));
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztTransferBarangDetail.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Barang sudah diinput.");
//                return
//            }
//        }
        var kode_barang = this.kode_barang.getValue();
        var nama_barang = this.nama_barang.getValue();
        // var sat = this.sat.getValue();
        var tipe_barang_id = this.tipe_barang_id.getValue();
        var grup_id = this.grup_id.getValue();
        // var batch = this.batch.getValue();
        // var tgl_exp = this.tgl_exp.getValue();
        var qty = parseFloat(this.qty.getValue());
        //kunci barang sama tidak boleh masuk dua kali
        // $err_msg = "Barang <b>" + kode_barang
        //     + (tgl_exp ? "</b> <br>tanggal expired: <b>" + tgl_exp : "")
        //     + (batch ? "</b>, <br>No. Batch : <b>" + batch : "")
        //     + "</b> <br>sudah ditambahkan.";
        // if (this.store.findBy(function (r) {
        //         return r.get('barang_id') == barang_id && r.get('tgl_exp') == tgl_exp && r.get('batch') == batch;
        //     }) != -1) {
        //     if (this.btnEdit.text == 'Save') {
        //         if (this.sm.getSelected().data.barang_id != barang_id || this.sm.getSelected().data.tgl_exp != tgl_exp
        //             || this.sm.getSelected().data.batch != batch) {
        //             Ext.MessageBox.alert("Error", $err_msg);
        //             this.resetForm();
        //             return;
        //         }
        //     } else {
        //         Ext.MessageBox.alert("Error", $err_msg);
        //         return;
        //     }
        // }
        //cek input qty
        // var available_qty =
        //     this.batch.store.getAt(this.batch.store.findBy(
        //         function (r) {
        //             //console.log(r.get('batch')+" = "+batch+"; "+r.get('tgl_exp')+" = "+tgl_exp);
        //             return r.get('batch') == batch && r.get('tgl_exp') == tgl_exp;
        //         }
        //     )).get('qty');
        // if (available_qty <= 0 || qty > available_qty) {
        //     Ext.MessageBox.alert("Error", "Jumlah stok <b>" + kode_barang + "</b> tidak mencukupi.<br>Kekurangan : <b style='color:red;'>" + Ext.util.Format.number((available_qty - qty), '0,0') + "</b> " + sat);
        //     return;
        // }
        //reformat tgl aga bisa disimpan
        //var tgl_exp = this.tgl_exp2.getValue().format('Y-m-d');
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            // record.set('batch', batch);
            // record.set('tgl_exp', tgl_exp);
            record.set('qty', qty);
            record.set('kode_barang', kode_barang);
            record.set('nama_barang', nama_barang);
            // record.set('sat', sat);
            record.set('tipe_barang_id', tipe_barang_id);
            record.set('grup_id', grup_id);
            record.commit();
        } else {
            var c = jun.rztTransferBarangDetail.recordType,
                d = new c({
                    barang_id: barang_id,
                    // batch: batch,
                    // tgl_exp: tgl_exp,
                    qty: qty,
                    kode_barang: kode_barang,
                    nama_barang: nama_barang,
                    // sat: sat,
                    tipe_barang_id: tipe_barang_id,
                    grup_id: grup_id
                });
            jun.rztTransferBarangDetail.add(d);
        }
//        clearText();
        this.resetForm();
    },
    resetForm: function () {
        this.barang.reset();
        // this.batch.reset();
        // this.tgl_exp.reset();
        // this.tgl_exp2.reset();
        this.qty.reset();
        this.kode_barang.reset();
        this.nama_barang.reset();
        // this.sat.reset();
        this.grup_id.reset();
        this.tipe_barang_id.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
        this.store.commitChanges();
    }
});
