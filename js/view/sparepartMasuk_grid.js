jun.SparepartMasukGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Tanda Terima Sparepart",
    id: 'docs-jun.SparepartMasukGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Ref.',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 80
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 250
        }
    ],
    userPermission: function (actCreate, actEdit, actPrint) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
    },
    initComponent: function () {
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztMaterialSparepart.getTotalCount() === 0) {
            jun.rztMaterialSparepart.load();
        }
        if (jun.rztSupplierLib.getTotalCount() === 0) {
            jun.rztSupplierLib.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1);
                break;
            case USER_GA:
                this.userPermission(1, 1, 1);
                break;
            default:
                this.userPermission(0, 0, 0);
        }
        this.store = jun.rztSparepartMasuk;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Penerimaan',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print Tanda Terima',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportSparepartMasuk",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "sparepart_masuk_id",
                            ref: "../../sparepart_masuk_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.SparepartMasukGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.btnAddOnClick, this);
        this.btnEdit.on('Click', this.btnEditOnClick, this);
        this.btnPrint.on('Click', this.btnPrintPROnClick, this);
        this.on('rowdblclick', this.btnEditOnClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnAddOnClick: function () {
        var form = new jun.SparepartMasukWin({
            modez: 0,
            title: "Buat Tanda Terima Sparepart",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    btnEditOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        var form = new jun.SparepartMasukWin({
            modez: 1,
            sparepart_masuk_id: selectedz.json.sparepart_masuk_id,
            title: "Ubah" + " Tanda Terima Sparepart",
            iconCls: 'silk13-pencil'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSparepartMasukDetail.baseParams = {sparepart_masuk_id: selectedz.json.sparepart_masuk_id};
        jun.rztSparepartMasukDetail.load();
        jun.rztSparepartMasukDetail.baseParams = {};
    },
    btnPrintPROnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        Ext.getCmp("form-ReportSparepartMasuk").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSparepartMasuk").getForm().url = "Report/PrintTandaTerimaSparepartMasuk";
        this.sparepart_masuk_id.setValue(selectedz.json.sparepart_masuk_id);
        var form = Ext.getCmp('form-ReportSparepartMasuk').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
