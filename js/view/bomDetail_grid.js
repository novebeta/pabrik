jun.BomDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Material Detail",
    id: 'docs-jun.BomDetailGrid',
    iconCls: "silk-grid",
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    view: new Ext.grid.GroupingView({
        forceFit: true,
        groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})',
        showGroupName: false,
        enableNoGroups: false,
        enableGroupingMenu: true,
        hideGroupedColumn: true
    }),
    plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'Kode  Material',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_material',
            width: 90
        },
        {
            header: 'Nama  Material',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_material',
            width: 160
        },
        {
            header: 'Quantity',
            sortable: true,
            resizable: true,
            readOnly: true,
            dataIndex: 'qty',
            width: 50,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            summaryType: 'sum'
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 80,
            renderer: function (v, m, r, i) {
                return r.get('tipe_material_id') == MAT_RAW_MATERIAL ? '%' : v;
            }
        },
        {
            header: 'Tipe Material',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_material_id',
            width: 80,
            renderer: jun.renderTipeMaterial
        },
        {
            header: 'Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code'
        }
    ],
    initComponent: function () {
        this.store = jun.rztBomDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Material :'
                        },
                        {
                            xtype: 'mfcombobox',
                            style: 'margin-bottom:2px',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            // hiddenName: 'material_id',
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate(
                                '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode Material</th><th>Nama Material</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'
                            ),
                            // allowBlank: false,
                            enableKeyEvents: true,
                            listWidth: 400,
                            ref: '../../material',
                            displayField: 'kode_material'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Quantity:'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 50,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'satuan',
                            ref: '../../satuan'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Gudang :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'loc_code',
                            store: jun.rztStorageLocationCmp,
                            ref: '../../storage_location',
                            valueField: 'loc_code',
                            displayField: 'loc_code'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../kode_material'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../nama_material'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../sat'
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../tipe_material_id'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            width: 50,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            width: 50,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Delete',
                            width: 50,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.BomDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.material.on('select', this.onSelectMaterial, this);
        this.material.on('change', this.onChangeMaterial, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onSelectMaterial: function (c, r, i) {
        this.kode_material.setValue(r.get('kode_material'));
        this.nama_material.setValue(r.get('nama_material'));
        this.sat.setValue(r.get('sat'));
        this.satuan.setText(r.get('tipe_material_id') == MAT_RAW_MATERIAL ? '%' : r.get('sat'));
        this.tipe_material_id.setValue(r.get('tipe_material_id'));
    },
    onChangeMaterial: function (c, n, o) {
        if (c.selectByValue(n)) {
            var i = c.getStore().find('material_id', c.getValue());
            this.onSelectMaterial(c, c.getStore().getAt(i), i);
        } else {
            c.focus();
        }
    },
    resetForm: function () {
        this.material.reset();
        this.kode_material.reset();
        this.nama_material.reset();
        this.qty.reset();
        this.sat.reset();
        this.satuan.setText('');
        this.tipe_material_id.reset();
        this.storage_location.reset();
        this.material.focus();
    },
    loadForm: function () {
        var material_id = this.material.getValue();
        if (!material_id || !this.qty.getValue()) {
            Ext.MessageBox.alert("Error", "Semua field harus diisi");
            return;
        }
        if (this.store.find('material_id', material_id) != -1) {
            if (this.btnEdit.text == 'Save') {
                if (this.sm.getSelected().data.material_id != material_id) {
                    Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                    this.resetForm();
                    return;
                }
            } else {
                Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                this.material.focus();
                return;
            }
        }
        var kode_material = this.kode_material.getValue();
        var nama_material = this.nama_material.getValue();
        var qty = parseFloat(this.qty.getValue());
        var sat = this.sat.getValue();
        var tipe_material_id = this.tipe_material_id.getValue();
        var storage_location = this.storage_location.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('kode_material', kode_material);
            record.set('nama_material', nama_material);
            record.set('qty', qty);
            record.set('sat', sat);
            record.set('tipe_material_id', tipe_material_id);
            record.set('loc_code', storage_location);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    material_id: material_id,
                    kode_material: kode_material,
                    nama_material: nama_material,
                    qty: qty,
                    sat: sat,
                    tipe_material_id: tipe_material_id,
                    loc_code: storage_location
                });
            this.store.add(d);
        }
        this.resetForm();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.material.setValue(record.data.material_id);
            this.kode_material.setValue(record.data.kode_material);
            this.nama_material.setValue(record.data.nama_material);
            this.qty.setValue(record.data.qty);
            this.sat.setValue(record.data.sat);
            this.tipe_material_id.setValue(record.data.tipe_material_id);
            this.storage_location.setValue(record.data.loc_code);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data yang akan dihapus.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus Data  Material ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});