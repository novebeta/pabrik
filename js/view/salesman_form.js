jun.SalesmanWin = Ext.extend(Ext.Window, {
    title: 'Salesman',
    modez: 1,
    width: 400,
    height: 390,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Salesman',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Sales',
                        hideLabel: false,
                        //hidden:true,
                        name: 'salesman_code',
                        id: 'salesman_codeid',
                        ref: '../salesman_code',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Sales',
                        hideLabel: false,
                        //hidden:true,
                        name: 'salesman_name',
                        id: 'salesman_nameid',
                        ref: '../salesman_name',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Tempat Lahir',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tempat_lahir',
                        id: 'tempat_lahirid',
                        ref: '../tempat_lahir',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_lahir',
                        fieldLabel: 'Tanggal lahir',
                        name: 'tgl_lahir',
                        id: 'tgl_lahirid',
                        format: 'd M Y',
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    // new jun.comboBagian({
                    //     fieldLabel: 'Bagian',
                    //     name: 'bagian',
                    //     id: 'bagianid',
                    //     ref: '../bagian',
                    //     anchor: '100%'
                    // }),
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Phone',
                        hideLabel: false,
                        //hidden:true,
                        name: 'phone',
                        id: 'phoneid',
                        ref: '../phone',
                        maxLength: 20,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_gabung',
                        fieldLabel: 'Tanggal Gabung',
                        name: 'tgl_gabung',
                        id: 'tgl_gabungid',
                        format: 'd M Y',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Alamat',
                        hideLabel: false,
                        //hidden:true,
                        name: 'alamat',
                        id: 'alamatid',
                        ref: '../alamat',
                        height: 80,
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    new jun.comboActive({
                        fieldLabel: "Status",
                        hideLabel: !1,
                        width: 100,
                        height: 20,
                        name: "status",
                        id: "statusid",
                        ref: "../status",
                        hiddenName: "status"
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalesmanWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Salesman/update/id/' + this.id;
        } else {
            urlz = 'Salesman/create/';
        }
        Ext.getCmp('form-Salesman').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztSalesman.reload();
                jun.rztSalesmanLib.reload();
                jun.rztSalesmanCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Salesman').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});