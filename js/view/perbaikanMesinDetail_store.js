jun.PerbaikanMesinDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PerbaikanMesinDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PerbaikanMesinDetailStoreId',
            url: 'PerbaikanMesinDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'perbaikan_mesin_detail_id'},
                {name: 'perbaikan_mesin_id'},
                {name: 'tgl', type: 'date'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'supplier_id'},
                {name: 'ket'}
            ]
        }, cfg));
    }
});
jun.rztPerbaikanMesinDetail = new jun.PerbaikanMesinDetailstore();