jun.NotaReturnGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Nota Return",
    id: 'docs-jun.NotaReturnGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Nota Return',
            sortable: true,
            resizable: true,
            dataIndex: 'nota_return_doc_ref',
            width: 100
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 100,
            renderer: jun.renderNamaSuppliers
        },
        {
            header: 'No. Faktur Pajak',
            sortable: true,
            resizable: true,
            dataIndex: 'nota_return_no_faktur_pajak',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'nota_return_dpp',
            width: 80,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                var total = Number(record.get('nota_return_dpp')) + Number(record.get('nota_return_ppn')) + Number(record.get('nota_return_ppnbm'));
                return Ext.util.Format.number(total, '0,0');
            }
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'nota_return_status',
            width: 60,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case NOTA_RETURN_OPEN :
                        return 'OPEN';
                    case NOTA_RETURN_USED :
                        return 'USED';
                    case NOTA_RETURN_CANCELED :
                        return 'CANCELED';
                }
            }
        }
    ],
    userPermission: function (actPrint, actCancel) {
        this.actView = true;
        this.actPrint = true;
        this.actCancel = true;
        this.viewAllData = (this.actPrint || this.actCancel || UROLE == USER_PEMBELIAN);
    },
    initComponent: function () {
        this.store = jun.rztNotaReturn;
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        this.userPermission(0, 0);
        // switch (UROLE) {
        //     case USER_ADMIN:
        //         this.userPermission(1, 1);
        //         break;
        //     case USER_PPIC:
        //         this.userPermission(0, 0);
        //         break;
        //     case USER_GA:
        //         this.userPermission(0, 0);
        //         break;
        //     case USER_PURCHASING:
        //         this.userPermission(0, 0);
        //         break;
        //     case USER_PEMBELIAN:
        //         this.userPermission(1, 1);
        //         break;
        //     default:
        //         this.userPermission(0, 0);
        // }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Lihat',
                    ref: '../btnEdit',
                    iconCls: 'silk13-eye',
                    hidden: this.actView ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel',
                    iconCls: 'silk13-cross',
                    hidden: this.actCancel ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    iconCls: "silk13-page_white_excel",
                    hidden: this.actPrint ? false : true,
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCancel || this.actPrint) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_notareturn: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.NotaReturnGrid').setFilterData(m.status_notareturn);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_notareturn: NOTA_RETURN_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.NotaReturnGrid').setFilterData(m.status_notareturn);
                                    }
                                }
                            },
                            {
                                text: 'USED',
                                status_notareturn: NOTA_RETURN_USED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.NotaReturnGrid').setFilterData(m.status_notareturn);
                                    }
                                }
                            },
                            {
                                text: 'CANCELED',
                                status_notareturn: NOTA_RETURN_CANCELED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.NotaReturnGrid').setFilterData(m.status_notareturn);
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportNotaReturn",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "return_pembelian_id",
                            ref: "../../return_pembelian_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {items: [{xtype: 'paging', store: this.store, displayInfo: true, pageSize: 20}]};
        jun.NotaReturnGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnCancel.on('Click', this.btnCancelOnClick, this);
        this.btnPrint.on('Click', this.printNotaReturn, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(NOTA_RETURN_OPEN);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    setFilterData: function (sts) {
        this.status_notareturn = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        this.store.baseParams = {
            mode: "grid",
            filter: 'nota_return',
            nota_return_status: this.status_notareturn
        };
        this.store.reload();
    },
    loadEditForm: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        var r = this.sm.getSelected().json;
        var ttl = "";
        switch (Number(r.nota_return_status)) {
            case NOTA_RETURN_OPEN :
                ttl = 'OPEN';
                break;
            case NOTA_RETURN_USED :
                ttl = 'USED';
                break;
            case NOTA_RETURN_CANCELED :
                ttl = 'CANCELED';
                break;
        }
        var form = new jun.NotaReturnWin({
            modez: 2,
            id: r.return_pembelian_id,
            title: "Lihat Nota Return [" + ttl + "]",
            iconCls: 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztNotaReturnDetails.baseParams = {
            return_pembelian_id: r.return_pembelian_id
        };
        jun.rztNotaReturnDetails.load();
        jun.rztNotaReturnDetails.baseParams = {};
    },
    btnCancelOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data.");
            return;
        }
        var status = Number(this.sm.getSelected().json.nota_return_status);
        if (status != NOTA_RETURN_OPEN) {
            switch (status) {
                case NOTA_RETURN_USED :
                    msg = "Nota Return telah digunakan untuk pembayaran.";
                    break;
                case NOTA_RETURN_CANCELED :
                    msg = "Nota Return telah dibatalkan.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "Tidak dapat membatalkan Nota Return ini.<br>" + msg);
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin akan membatalkan Nota Return ini?', this.cancelNotaReturn, this);
    },
    cancelNotaReturn: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'ReturnPembelian/CancelNotaReturn/id/' + record.json.return_pembelian_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztNotaReturn.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printNotaReturn: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Nota Return");
            return;
        }
        var status = Number(this.sm.getSelected().json.nota_return_status);
        if (status == NOTA_RETURN_CANCELED) {
            Ext.MessageBox.alert("Warning", "Tidak dapat mencetak Nota Return.<br>Nota return " + this.sm.getSelected().json.nota_return_doc_ref + " telah dibatalkan.");
            return;
        }
        Ext.getCmp("form-ReportNotaReturn").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNotaReturn").getForm().url = "Report/PrintNotaReturn";
        this.return_pembelian_id.setValue(selectedz.json.return_pembelian_id);
        var form = Ext.getCmp('form-ReportNotaReturn').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
