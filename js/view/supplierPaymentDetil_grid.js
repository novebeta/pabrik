jun.SupplierPaymentDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SupplierPaymentDetil",
    id: 'docs-jun.SupplierPaymentDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur',
            width: 100
        },
        {
            header: 'Kas Dibayar',
            sortable: true,
            resizable: true,
            dataIndex: 'kas_dibayar',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Sisa',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztSupplierPaymentDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: '\xA0No. Invoice:\xA0'
                },
                {
                    xtype: "combo",
                    typeAhead: !0,
                    triggerAction: "all",
                    lazyRender: !0,
                    mode: "local",
                    store: jun.rztSupplierPaymentDetilCmp,
                    forceSelection: !0,
                    valueField: "reference_item_id",
                    matchFieldWidth: !1,
                    itemSelector: 'tr.search-item',
                    tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                        '<th>Doc. Ref</th><th>No Faktur</th><th>Tgl</th><th>Total</th><th>Sisa</th></tr></thead>',
                        '<tbody><tpl for="."><tr class="search-item">',
                        '<td>{doc_ref}</td><td>{no_faktur}</td><td>{tgl:date("d/m/Y")}</td><td>{kas_dibayar:number("0,0.00")}</td><td>{sisa:number("0,0.00")}</td>',
                        '</tr></tpl></tbody></table>'),
                    displayField: "doc_ref",
                    listWidth: 750,
                    editable: !0,
                    ref: "../jual",
                    lastQuery: ""
                },
                {
                    xtype: 'label',
                    text: '\xA0Kas Dibayar :\xA0'
                },
                {
                    xtype: 'numericfield',
                    ref: '../kas',
                    value: 0,
                    width: 100
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDel'
                }
            ]
        };
        jun.SupplierPaymentDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadEditForm, this);
        //this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDel.on('Click', this.deleteRec, this);
        this.jual.on('select', this.selectJual, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    selectJual: function (combo, record, index) {
        this.kas.setValue(record.data.sisa);
        this.recordJual = record;
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SupplierPaymentDetilWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var jual = this.jual.getValue();
        if (jual == "") {
            Ext.MessageBox.alert("Error", "Faktur harus dipilih.");
            return
        }
        var kas = parseFloat(this.kas.getValue());
        //if (kas == 0) {
        //    Ext.MessageBox.alert("Error", "Kas dibayar tidak boleh 0.");
        //    return;
        //}
        var a = this.store.findExact("reference_item_id", this.recordJual.data.reference_item_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Faktur sudah diinput.");
            return;
        }
        if (kas != 0 && Math.abs(kas) > Math.abs(this.recordJual.data.sisa)) {
            Ext.MessageBox.alert("Error", "Kas yang dibayar tidak boleh lebih dari sisa.");
            return;
        }
        var sisa = parseFloat(this.recordJual.data.sisa) - kas;
        var c = jun.rztSupplierPaymentDetil.recordType,
            d = new c({
                reference_item_id: this.recordJual.data.reference_item_id,
                kas_dibayar: kas,
                doc_ref: this.recordJual.data.doc_ref,
                tgl: this.recordJual.data.tgl,
                no_faktur: this.recordJual.data.no_faktur,
                payment_tipe: this.recordJual.data.payment_tipe,
                sisa: sisa,
                total: parseFloat(this.recordJual.data.kas_dibayar),
                total_sisa: parseFloat(this.recordJual.data.sisa)
            });
        jun.rztSupplierPaymentDetil.add(d);
        this.store.refreshData();
        this.jual.reset();
        this.kas.reset();
        jun.rztSupplierPaymentDetilCmp.remove(this.recordJual);
        this.recordJual = null;
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        record.data.kas_dibayar = record.data.total;
        record.data.sisa = record.data.total_sisa;
        jun.rztSupplierPaymentDetilCmp.add(record);
        this.store.remove(record);
        this.store.refreshData();
    }
});
