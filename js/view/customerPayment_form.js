jun.CustomerPaymentWin = Ext.extend(Ext.Window, {
    title: 'Customer Payment',
    modez: 1,
    width: 950,
    height: 470,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-CustomerPayment',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc.Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        value: DATE_NOW,
                        x: 85,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode_customer',
                            'nama_customer'
                        ],
                        emptyText: 'All',
                        mode: 'local',
                        store: jun.rztCustomersCmp,
                        // hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Customer</th><th>Nama Customer</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_customer}</td><td>{nama_customer}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        matchFieldWidth: !1,
                        allowBlank: true,
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../customer',
                        displayField: 'kode_customer',
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Bank:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        ref: '../bank',
                        forceSelection: true,
                        fieldLabel: 'bank_id',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "No. Bukti:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_bukti',
                        ref: '../no_bukti',
                        x: 715,
                        y: 2,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. BG/CEK:",
                        x: 615,
                        y: 35
                    },
                    {
                        // xtype: 'textfield',
                        // hideLabel: false,
                        // //hidden:true,
                        // name: 'no_bg_cek',
                        // ref: '../no_bg_cek',
                        // maxLength: 100,
                        xtype: 'mfcombobox',
                        searchFields: [
                            'no_giro'
                        ],
                        mode: 'local',
                        store: jun.rztGiroCmp,
                        itemSelector: 'tr.search-item',
                        matchFieldWidth: !1,
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>No. Giro</th><th>Tgl Klr</th><th>Tgl Jt</th><th>Saldo</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{no_giro}</td><td>{tgl_klr:date("d/m/Y")}</td><td>{tgl_klr:date("d/m/Y")}</td><td>{saldo:number("0,0.00")}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        allowBlank: true,
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../giro',
                        forceSelection: true,
                        hiddenName: 'no_bg_cek',
                        valueField: 'giro_id',
                        displayField: 'no_giro',
                        x: 715,
                        y: 32,
                        width: 175
                    },
                    new jun.CustomerPaymentDetilGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: "label",
                        text: "Total :",
                        x: 672,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'total_customer_payment_id',
                        ref: "../total",
                        width: 192,
                        readOnly: true,
                        minValue: 0,
                        value: 0,
                        x: 722,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "Remark:",
                        x: 5,
                        y: 365
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'remark',
                        hideLabel: false,
                        //hidden:true,
                        name: 'remark',
                        id: 'remarkid',
                        ref: '../remark',
                        width: 175,
                        readOnly: false,
                        x: 85,
                        y: 362
                    }//,
                    // {
                    //     xtype: "label",
                    //     text: "Uang Diterima :",
                    //     x: 295,
                    //     y: 365
                    // },
                    // {
                    //     xtype: 'numericfield',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'uang_diterima',
                    //     id: 'uang_diterima_id',
                    //     ref: "../uang_diterima",
                    //     readOnly: true,
                    //     width: 192,
                    //     minValue: 0,
                    //     value: 0,
                    //     x: 380,
                    //     y: 362
                    // }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CustomerPaymentWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customer.on('change', this.onCustomerChange, this);
        this.bank.on('select', this.onBankSelect, this);
        this.giro.on('select', this.onGiroSelect, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
    },
    onBankSelect: function () {
        this.giro.reset();
        this.recGiro = null;
        this.customer.setDisabled(false);
    },
    onGiroSelect: function (c, r, i) {
        this.bank.reset();
        this.recGiro = r;
        this.customer.setValue(r.data.customer_id);
        this.onCustomerChange();
        // this.customer.setDisabled(true);
    },
    onWinClose: function () {
        jun.rztCustomerPaymentDetil.removeAll();
        jun.rztCustomerPaymentDetilCmp.removeAll();
    },
    onCustomerChange: function () {
        //this.doc_ref.reset();
        // this.bank.reset();
        var customer_id = this.customer.getValue();
        if (this.giro.getValue() != '') {
            jun.rztCustomerPaymentDetil.removeAll();
        }
        jun.rztCustomerPaymentDetilCmp.removeAll();
        //jun.rztCustomerPaymentDetilCmp.load({customer_id: customer_id});
        jun.rztCustomerPaymentDetilCmp.baseParams = {
            customer_id: customer_id
        };
        jun.rztCustomerPaymentDetilCmp.load();
        // this.total.reset();
        // this.uang_diterima.reset();
        // jun.rztCustomerPaymentDetilCmp.baseParams = {};
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        var total_pay = parseFloat(this.total.getValue());
        if (total_pay >= 0) {
            this.btnDisabled(true);
            if (this.giro.getValue() != '') {
                if (this.recGiro.data.used != '0') {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: 'Giro sudah terpakai.',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    this.btnDisabled(false);
                    return;
                }
                // if (total_pay != parseFloat(this.recGiro.data.saldo)) {
                //     Ext.MessageBox.show({
                //         title: 'Info',
                //         msg: 'Saldo Giro tidak sama dengan total hutang yang dibayar.',
                //         buttons: Ext.MessageBox.OK,
                //         icon: Ext.MessageBox.INFO
                //     });
                //     this.btnDisabled(false);
                //     return;
                // }
            }
            if (this.bank.getValue() == '' && this.giro.getValue() == '') {
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: 'Silahkan pilih salah satu pembayaran.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.btnDisabled(false);
                return;
            }
            var urlz = 'CustomerPayment/create/';
            Ext.getCmp('form-CustomerPayment').getForm().submit({
                url: urlz,
                timeOut: 1000,
                scope: this,
                params: {
                    mode: this.modez,
                    id: this.id,
                    detil: Ext.encode(Ext.pluck(
                        jun.rztCustomerPaymentDetil.data.items, "data"))
                },
                success: function (f, a) {
                    jun.rztCustomerPayment.reload();
                    jun.rztGiroCmp.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    if (this.modez == 0) {
                        Ext.getCmp('form-CustomerPayment').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if (this.closeForm) {
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }
            });
        } else {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Jumlah uang tidak boleh negatif',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
        }
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});