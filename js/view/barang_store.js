jun.Barangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Barangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangStoreId',
            url: 'Barang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang_rnd'},
                {name: 'kode_barang'},
                {name: 'kode_barang_2'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'sat'},
                {name: 'grup_id'},
                {name: 'qty_per_box'},
                {name: 'qty_per_pot'},
                {name: 'tipe_barang_id'}
            ]
        }, cfg));
    },
    filterGrup: function (grup_id) {
        this.clearFilter();
        this.filter([
            {
                property: "grup_id",
                value: grup_id
            }
        ]);
    }
});
jun.rztBarang = new jun.Barangstore();
jun.rztBarangLib = new jun.Barangstore();
jun.rztBarangCmp = new jun.Barangstore();
jun.rztBarangBekas = new jun.Barangstore({baseParams: {tipe_barang_id: BRG_BARANGRONGSOKAN}});
jun.BarangBatchstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangBatchstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangBatchStoreId',
            url: 'Barang/Batch',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'sat'},
                {name: 'grup_id'},
                {name: 'qty_per_box'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'qty'}
            ]
        }, cfg));
    }
});
jun.rztBarangBatch = new jun.BarangBatchstore();
jun.rztBarangBatchLib = new jun.BarangBatchstore();
jun.rztBarangBatchCmp = new jun.BarangBatchstore();
//---Tipe Barang
jun.TipeBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TipeBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TipeBarangStoreId',
            url: 'TipeBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tipe_barang_id'},
                {name: 'nama_tipe_barang'},
                {name: 'have_stock'}
            ]
        }, cfg));
    }
});
jun.rztTipeBarangCmp = new jun.TipeBarangstore();
