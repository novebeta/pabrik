jun.CreditNoteDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "CreditNoteDetails",
    id: 'docs-jun.CreditNoteDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        // {
        //     header: 'Batch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'batch',
        //     width: 100
        // },
        // {
        //     header: 'Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_exp',
        //     width: 100
        // },
        {
            header: 'Jml',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztCreditNoteDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            displayField: 'kode_barang'
                        },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     text: 'Batch :'
                        // },
                        // {
                        //     xtype: 'textfield',
                        //     fieldLabel: 'batch',
                        //     hideLabel: false,
                        //     //hidden:true,
                        //     name: 'batch',
                        //     ref: '../../batch',
                        //     maxLength: 50
                        // },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     text: 'Expired:'
                        // },
                        // {
                        //     xtype: 'xdatefield',
                        //     ref: '../../tgl_exp',
                        //     fieldLabel: 'tgl_exp',
                        //     name: 'tgl_exp',
                        //     format: 'd M Y'
                        // },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Jml :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../ket',
                            // width: 611,
                            colspan: 5,
                            maxLength: 255
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.CreditNoteDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
    },
    onStoreChange: function () {
        jun.rztCreditNoteDetails.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            // this.batch.setValue(record.data.batch);
            // this.tgl_exp.setValue(record.data.tgl_exp);
            this.qty.setValue(record.data.qty);
            this.price.setValue(record.data.price);
            this.ket.setValue(record.data.ket);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztCreditNoteDetails.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Barang sudah diinput.");
                return
            }
        }
        // var batch = this.batch.getValue();
        // var tgl_exp = this.tgl_exp.getValue();
        var qty = parseFloat(this.qty.getValue());
        var price = parseFloat(this.price.getValue());
        var total = qty * price;
        var ket = this.ket.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            // record.set('batch', batch);
            // record.set('tgl_exp', tgl_exp);
            record.set('qty', qty);
            record.set('price', price);
            record.set('total', total);
            record.set('ket', ket);
            record.commit();
        } else {
            var c = jun.rztCreditNoteDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    // batch: batch,
                    // tgl_exp: tgl_exp,
                    qty: qty,
                    price: price,
                    total: total,
                    ket: ket
                });
            jun.rztCreditNoteDetails.add(d);
        }
//        clearText();
        this.barang.reset();
        // this.batch.reset();
        // this.tgl_exp.reset();
        this.qty.reset();
        this.price.reset();
        this.ket.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
        this.onStoreChange();
    }
});
