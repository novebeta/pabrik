jun.SjDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SjDetails",
    id: 'docs-jun.SjDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    // modez: 1,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        },
        {
            header: 'Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSjDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztMaterialCmp,
                            hiddenName: 'material_id',
                            valueField: 'material_id',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_material}</b><br>{nama_material}</span>',
                                "</div></tpl>"),
                            allowBlank: false,
                            readOnly: true,
                            //listWidth: 300,
                            // width: 300,
                            ref: '../../barang',
                            displayField: 'kode_material'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stok'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 100,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../ketpot',
                            width: 300,
                            colspan: 2,
                            maxLength: 255
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Gudang :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'loc_code',
                            store: jun.rztStorageLocationCmp,
                            valueField: 'loc_code',
                            id: 'sj_loc_code_id',
                            width: 100,
                            displayField: 'loc_code'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        // {
                        //     xtype: 'button',
                        //     text: 'Add',
                        //     height: 44,
                        //     ref: '../../btnAdd'
                        // },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SjDetailsGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        // this.batch.on('select', this.onSelectBatch, this);
        // this.barang.on('select', this.onSelectBrg, this);
        // this.barang.on('select', this.showStock, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        // if (this.modez != 0) {
        //     this.topToolbar.setVisible(false);
        // }
    },
    showStock: function () {
        var material_id = this.barang.getValue();
        var loc_code = Ext.getCmp('sj_loc_code_id').getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stok);
        }
    },
    btnDisable: function (s) {
        // this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected");
            return
        }
        // kunci barang sama tidak boleh masuk dua kali
        //   if (this.btnEdit.text != 'Save') {
        //       var a = jun.rztSjDetails  .findExact("barang_id", barang_id);
        //       if (a > -1) {
        //           Ext.MessageBox.alert("Error", "Item already entered");
        //           return
        //       }
        //   }
        // var barang = jun.getBarang(barang_id);
        var loc_code = Ext.getCmp('sj_loc_code_id').getValue();
        if (loc_code == "") {
            Ext.MessageBox.alert("Error", "Gudang harus dipilih.");
            return;
        }
        var qty = parseFloat(this.qty.getValue());
        if (qty > this.qty.maxValue) {
            Ext.MessageBox.alert("Error", "Qty tidak boleh lebih dari " + qty);
            return;
        }
        // var batch = this.batch.getValue();
        // var tgl_exp = this.tgl_exp.getValue().format('Y-m-d');
        var ketpot = this.ketpot.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('loc_code', loc_code);
            // record.set('tgl_exp', tgl_exp);
            record.set('qty', qty);
            record.set('ket', ketpot);
            record.commit();
            this.btnEdit.setText("Edit");
        } else {
            if (jun.rztSjDetails.getCount() < this.max_item) {
                var c = jun.rztSjDetails.recordType,
                    d = new c({
                        barang_id: barang_id,
                        loc_code: loc_code,
                        // tgl_exp: tgl_exp,
                        qty: qty,
                        ket: ketpot
                    });
                jun.rztSjDetails.add(d);
            } else {
                Ext.MessageBox.alert("Warning", "Tidak dapat menambah item baru.<br>Maksimal jumlah item : <b>" + this.max_item + "<b>");
            }
        }
//        this.store.refreshData();
        this.barang.reset();
        this.qty.reset();
        Ext.getCmp('sj_loc_code_id').reset();
        // this.tgl_exp.reset();
        this.ketpot.reset();
        this.btnDisable(false);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            Ext.getCmp('sj_loc_code_id').setValue(record.data.loc_code);
            // this.tgl_exp.setValue(record.data.tgl_exp);
            this.qty.setValue(record.data.qty);
            this.qty.setMaxValue(record.data.qty);
            this.ketpot.setValue(record.data.ket);
            btn.setText("Save");
            this.btnDisable(true);
            this.showStock();
        } else {
            this.loadForm();
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sj_details_id;
        var form = new jun.SjDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    },
    onSelectBarang: function (combo, record, index) {
        this.batch.setValue(record.data.batch);
        this.tgl_exp.setValue(record.data.tgl_exp);
        this.qty.setValue(record.data.qty);
    }
});
jun.SalesInvoiceDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SalesInvoiceDetails",
    id: 'docs-jun.SalesInvoiceDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        // {
        //     header: 'Batch',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'batch',
        //     width: 100
        // },
        // {
        //     header: 'Expired',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tgl_exp',
        //     width: 100
        // },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0")
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'totalpot',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        // {
        //     header: 'Adl Disc',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'adl_disc_rp',
        //     width: 100,
        //     align: "right",
        //     renderer: Ext.util.Format.numberRenderer("0,0.00")
        // },
        {
            header: 'Tax',
            sortable: true,
            resizable: true,
            dataIndex: 'vatrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSjDetails;
        // this.tbar = {
        //     xtype: 'toolbar',
        //     items: [
        //         {
        //             xtype: 'buttongroup',
        //             columns: 8,
        //             defaults: {
        //                 scale: 'small'
        //             },
        //             items: [
        //
        //                 {
        //                     xtype: 'numericfield',
        //                     id: 'adl_disc',
        //                     ref: '../../adl_disc',
        //                     style: 'margin-bottom:2px',
        //                     width: 50,
        //                     value: 1,
        //                     minValue: 1
        //                 }
        //             ]
        //         },
        //         {
        //             xtype: 'buttongroup',
        //             columns: 3,
        //             id: 'btnsalesdetilid',
        //             defaults: {
        //                 scale: 'large'
        //             },
        //             items: [
        //
        //                 {
        //                     xtype: 'button',
        //                     text: 'Edit',
        //                     height: 44,
        //                     ref: '../../btnEdit'
        //                 }
        //             ]
        //         }
        //     ]
        // };
        jun.SalesInvoiceDetailsGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.onClickbtnEdit, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        //this.batch.on('select', this.onSelectBatch, this);
        //this.barang.on('select', this.onSelectBrg, this);
        //this.btnAdd.setDisabled(true);
        //this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.adl_disc.setValue(record.data.adl_disc);
            this.adl_disc.setMaxValue(100);
            btn.setText("Save");
            //this.btnDisable(true);
        } else {
            if (this.loadForm() == 0) {
                btn.setText("Edit");
            }
        }
    },
    loadForm: function () {
        var record2 = this.sm.getSelected();
        var adl_disc = this.adl_disc.getValue();
        if (adl_disc == "") {
            Ext.MessageBox.alert("Error", "Diskon tambahan harus diisi.");
            return 1;
        }
        adl_disc = parseFloat(adl_disc);
        if (adl_disc > this.adl_disc.maxValue) {
            Ext.MessageBox.alert("Error", 'The maximum value for this field is ' + this.adl_disc.maxValue);
            return 1;
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztSjDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already entered");
//                return
//            }
//        }
//        var barang = jun.getBarang(barang_id);
        //var qty = parseFloat(this.qty.getValue());
        var bruto = parseFloat(record2.data.bruto).toFixed(2);
        var disc_rp = parseFloat(record2.data.disc_rp).toFixed(2);
        var sub_total = parseFloat(bruto - disc_rp).toFixed(2);
        var adl_disc = this.adl_disc.getValue();
        var adl_disc_rp = parseFloat(sub_total * adl_disc / 100).toFixed(2);
        var sub_totalx = parseFloat(sub_total - adl_disc_rp).toFixed(2);
        var tax_rp = parseFloat(sub_totalx * 10 / 100).toFixed(2);
        var f = parseFloat((sub_totalx - 0) + (tax_rp - 0)).toFixed(2);
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('adl_disc', adl_disc);
            record.set('adl_disc_rp', adl_disc_rp);
            record.set('tax_rp', tax_rp);
            record.set('total', f);
            record.commit();
        }
//        this.store.refreshData();
        this.adl_disc.reset();
        return 0;
    }
});
jun.SjReturnDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SjReturnDetails",
    id: 'docs-jun.SjReturnDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Batch',
            sortable: true,
            resizable: true,
            dataIndex: 'batch',
            width: 100
        },
        {
            header: 'Expired',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_exp',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
//        {
//            header: 'Harga',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'price',
//            width: 100
//        },
//        {
//            header: 'Total',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'total',
//            width: 100
//        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSjDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Jml :'
                },
                {
                    xtype: 'numericfield',
                    id: 'qtyid',
                    ref: '../qty',
                    style: 'margin-bottom:2px',
                    width: 50,
                    value: 1,
                    minValue: 0
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Note :'
                },
                {
                    xtype: 'textfield',
                    ref: '../ket',
                    width: 500,
                    maxLength: 255
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.SjReturnDetailsGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        //this.batch.on('select', this.onSelectBatch, this);
        //this.barang.on('select', this.onSelectBrg, this);
        //this.btnAdd.setDisabled(true);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnDisable: function (s) {
        //this.btnAdd.setDisabled(true);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var qty = parseFloat(this.qty.getValue());
        //var price = parseFloat(this.price.getValue());
        var ket = this.ket.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            var total = qty * record.data.price;
            //record.set('barang_id', barang_id);
            //record.set('batch', batch);
            //record.set('tgl_exp', tgl_exp);
            record.set('qty', qty);
            record.set('total', total);
            record.set('ket', ket);
            record.commit();
        }
//        clearText();
//        this.barang.reset();
//        this.batch.reset();
//        this.tgl_exp.reset();
        this.qty.reset();
        //this.price.reset();
        this.ket.reset();
        return 0;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.qty.setValue(record.data.qty);
            //this.price.setValue(record.data.price);
            this.ket.setValue(record.data.ket);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sj_details_id;
        var form = new jun.SjDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    },
    onSelectBarang: function (combo, record, index) {
        this.qty.setValue(record.data.qty);
    }
});