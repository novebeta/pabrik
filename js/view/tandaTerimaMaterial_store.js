jun.TandaTerimaMaterialStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TandaTerimaMaterialStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TandaTerimaMaterialStoreId',
            url: 'TandaTerimaMaterial',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tandaterima_id'},
                {name: 'return_pembelian_id'},
                {name: 'doc_ref_return'},
                {name: 'supplier_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'tanda_terima'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
jun.rztTandaTerimaMaterial = new jun.TandaTerimaMaterialStore();