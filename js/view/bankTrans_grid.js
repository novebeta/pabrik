jun.BankTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Cash/Bank Transfer",
    id: 'docs-jun.BankTransGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'trans_date',
            width: 100
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Charge',
            sortable: true,
            resizable: true,
            dataIndex: 'charge',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        jun.rztBankTrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tgl: Ext.getCmp('tglbanktransfergridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztBankTrans;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Cash/Bank Transfer',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Cash/Bank Transfer',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Cash/Bank Transfer',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },

                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglbanktransfergridid',
                    ref: '../tgl',
                    noPastYears: false
                })
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     id: 'tglbanktransgridid',
                //     ref: '../tgl'
                // }
            ]
        };
        jun.BankTransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printTransfer, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.store.removeAll();
    },
    printTransfer: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'BankTrans/print',
            method: 'POST',
            scope: this,
            params: {
                trans_no: record.json.trans_no
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                findPrinterReceipt();
                if (notReady()) {
                    return;
                }
//                qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                while (!qz.isDoneAppending()) {}
//                qz.append("\x1B\x40"); // 1
//                //qz.append("\x1B\x21\x08"); // 2
//                qz.append("\x1B\x21\x01"); // 3
//                qz.append(response.msg);
//                qz.append("\x1D\x56\x41"); // 4
//                qz.append("\x1B\x40"); // 5
//                qz.print();
                qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
                qz.printHTML();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TranferBankWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.bank_trans_id;
        var form = new jun.TranferBankWin({modez: 1, id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BankTrans/delete/id/' + record.json.bank_trans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBankTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
