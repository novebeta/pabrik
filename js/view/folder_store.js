jun.Folderstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Folderstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FolderStoreId',
            url: 'Folder',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'folder_id'},
{name:'type_'},
{name:'doc_ref'},
{name:'tgl'},
{name:'note_'},
                
            ]
        }, cfg));
    }
});
jun.rztFolder = new jun.Folderstore();
//jun.rztFolder.load();
