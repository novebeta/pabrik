jun.BarangWin = Ext.extend(Ext.Window, {
    title: 'Barang',
    modez: 1,
    width: 450,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Barang',
                labelWidth: 150,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Barang (R&D)',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_barang_rnd',
                        ref: '../kode_barang_rnd',
                        maxLength: 50,
                        allowBlank: (UROLE == USER_GPJ ? true : false),
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Barang (Customer)',
                        hideLabel: false,
                        allowBlank: (UROLE == USER_RND ? true : false),
                        readOnly: (UROLE == USER_RND ? true : false),
                        //hidden:true,
                        name: 'kode_barang',
                        ref: '../kode_barang',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Barang 3',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_barang_2',
                        ref: '../kode_barang2',
                        maxLength: 50,
                        //allowBlank: ,
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Barang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_barang',
                        id: 'nama_barangid',
                        ref: '../nama_barang',
                        maxLength: 100,
                        allowBlank: (UROLE == USER_GPJ ? true : false),
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Note',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket',
                        id: 'ketid',
                        ref: '../ket',
                        maxLength: 255,
                        //allowBlank: 1,
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Satuan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'sat',
                        id: 'satid',
                        ref: '../sat',
                        maxLength: 10,
                        allowBlank: false,
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: false,
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        fieldLabel: 'Grup',
                        store: jun.rztGrupCmp,
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Qty Per Pot/botol (gram)',
                        hideLabel: false,
                        //hidden:true,
                        name: 'qty_per_pot',
                        id: 'qty_per_potid',
                        ref: '../qty_per_pot',
                        maxLength: 30,
                        allowBlank: false,
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Qty Per Box',
                        hideLabel: false,
                        //hidden:true,
                        name: 'qty_per_box',
                        id: 'qty_per_boxid',
                        ref: '../qty_per_box',
                        maxLength: 30,
                        allowBlank: false,
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: false,
                        readOnly: (UROLE == USER_GPJ ? true : false),
                        fieldLabel: 'Tipe Barang',
                        store: jun.rztTipeBarangCmp,
                        hiddenName: 'tipe_barang_id',
                        valueField: 'tipe_barang_id',
                        displayField: 'nama_tipe_barang',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BarangWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Barang/update/id/' + this.id;
        } else {
            urlz = 'Barang/create/';
        }
        Ext.getCmp('form-Barang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBarang.reload();
                jun.rztBarangLib.reload();
                jun.rztBarangCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Barang').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});