jun.ConversiMaterialBarkasWin = Ext.extend(Ext.Window, {
    title: 'Conversi Material Barkas',
    modez: 1,
    width: 900,
    height: 500,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-ConversiMaterialBarkas',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. Ref.",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        value: new Date(),
                        x: 85,
                        y: 32,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Note",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        name: 'note',
                        ref: '../note',
                        width: 270,
                        height: 50,
                        x: 295 + 85,
                        y: 2
                    },
                    {
                        xtype: "tabpanel",
                        activeTab: 0,
                        anchor: '100% 100%',
                        height: 320,
                        x: 5,
                        y: 65,
                        items: [
                            {
                                title: 'Sparepart Bekas',
                                frame: false,
                                layout: 'fit',
                                border: false,
                                iconCls: 'silk13-delete',
                                items: [
                                    new jun.ConversiMaterialDetailGrid({
                                        x: 5,
                                        y: 65,
                                        frameHeader: !1,
                                        header: !1,
                                        border: 0,
                                        ref: "../../../materialgriddetils",
                                        readOnly: (this.modez < 2 ? false : true)
                                    })
                                ]
                            },
                            {
                                title: 'Barang Bekas',
                                frame: false,
                                layout: 'fit',
                                border: false,
                                iconCls: 'silk13-add',
                                items: [
                                    new jun.ConversiBarkasDetailGrid({
                                        x: 5,
                                        y: 65,
                                        frameHeader: !1,
                                        header: !1,
                                        border: 0,
                                        ref: "../../../barkasgriddetils",
                                        readOnly: (this.modez < 2 ? false : true)
                                    })
                                ]
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ConversiMaterialBarkasWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(this.rejectPR ? false : true);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    onWinClose: function () {
        this.materialgriddetils.store.removeAll();
        this.barkasgriddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'ConversiMaterialBarkas/create/';
        Ext.getCmp('form-ConversiMaterialBarkas').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                conversi_material_barkas_id: this.conversi_material_barkas_id,
                materialdetil: Ext.encode(Ext.pluck(
                    this.materialgriddetils.store.data.items, "data")),
                barkasdetil: Ext.encode(Ext.pluck(
                    this.barkasgriddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                if (Ext.getCmp('docs-jun.ConversiMaterialBarkasGrid') !== undefined) Ext.getCmp('docs-jun.ConversiMaterialBarkasGrid').store.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ConversiMaterialBarkas').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});