jun.TransferMaterialGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Barang",
    id: 'docs-jun.TransferMaterialGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        // {
        //     header: '',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'pengirim',
        //     width: 8,
        //     align: "center",
        //     renderer: function (value, metaData, record, rowIndex) {
        //         if (record.get('pengirim') == UROLE) {
        //             (metaData.style += "background-color: #FFD4FF;");
        //             metaData.css += ' icon-red_arrow_right ';
        //         }
        //         if (record.get('penerima') == UROLE) {
        //             (metaData.style += "background-color: #CCFFCC;");
        //             metaData.css += ' icon-green_arrow_left ';
        //         }
        //         return "";
        //     }
        // },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 45,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Nomor',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 60
        },
        // {
        //     header: 'Pengirim',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'pengirim',
        //     width: 40,
        //     renderer: jun.renderStorageLocationName
        // },
        // {
        //     header: 'Penerima',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'penerima',
        //     width: 40,
        //     renderer: jun.renderStorageLocationName
        // },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 120
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'accepted',
            width: 20,
            renderer: function (value, metaData, record, rowIndex) {
                //return parseInt(value)? 'DITERIMA':'BELUM DITERIMA';
                value && (metaData.css += ' silk13-tick ');
                return '';
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actPrint, actAccept) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
        this.actAccept = actAccept;
    },
    initComponent: function () {
        this.store = jun.rztTransferMaterial;
        // if (jun.rztBomLib.getTotalCount() === 0) jun.rztBomLib.load();
        // if (jun.rztMaterialForBOM.getTotalCount() === 0) jun.rztMaterialForBOM.load();
        if (jun.rztTipeMaterialLib.getTotalCount() === 0) jun.rztTipeMaterialLib.load();
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) jun.rztStorageLocationCmp.load();
        if (jun.rztBarangCmp.getTotalCount() === 0) jun.rztBarangCmp.load();
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        this.userPermission(1, 1, 1, 1);
        // switch(UROLE){
        //     case USER_ADMIN:        this.userPermission(0,0,1,1); break;
        //     case USER_PPIC:         this.userPermission(1,1,1,1); break;
        //     case USER_GA:           this.userPermission(0,0,0,0); break;
        //     case USER_PURCHASING:   this.userPermission(0,0,0,0); break;
        //     case USER_PEMBELIAN:    this.userPermission(0,0,0,0); break;
        //     case USER_RND:          this.userPermission(1,1,1,1); break;
        //     case USER_PRODUKSI:     this.userPermission(0,1,1,1); break;
        //     default:                this.userPermission(0,0,0,0);
        // }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'splitbutton',
                    text: 'Kirim',
                    ref: '../btnAddSend',
                    iconCls: 'icon-red_arrow_right',
                    hidden: this.actCreate ? false : true,
                    menu: {
                        xtype: 'menu',
                        items: [
                            {text: 'Terima', ref: '../../../btnAddReceive', iconCls: 'icon-green_arrow_left'}
                        ]
                    }
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Terima',
                    ref: '../btnAccept',
                    iconCls: 'silk13-tick',
                    hidden: this.actAccept ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actAccept || this.actPrint) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>All</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TransferMaterialGrid').setFilterData(m.status);
                                    }
                                }
                            },
                            {
                                text: 'BELUM DITERIMA',
                                status: 0,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TransferMaterialGrid').setFilterData(m.status);
                                    }
                                }
                            },
                            {
                                text: 'DITERIMA',
                                status: 1,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TransferMaterialGrid').setFilterData(m.status);
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-TransferMaterialGrid",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "transfer_material_id",
                            ref: "../../transfer_material_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.TransferMaterialGrid.superclass.initComponent.call(this);
        this.btnAddSend.on('Click', this.addSend, this);
        this.btnAddReceive.on('Click', this.addReceive, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.btnPrintOnClick, this);
        this.btnAccept.on('Click', this.btnAcceptOnClick, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData('all');
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.actAccept && this.btnAccept.setDisabled(!(!r.get('accepted') && r.get('trans_out')));
        // this.actPrint && this.btnPrint.setDisabled(!(r.get('pengirim') === UROLE));
        if (this.actEdit) {
            //var filterEdit = (!parseInt(r.get('accepted')) || storloc[parseInt(r.get('penerima'))].havestock == 0 || (r.get('penerima')=== UROLE && !r.get('trans_out'))) && this.actEdit && parseInt(r.get('pengirim')) == UROLE;
            var filterEdit = ((!parseInt(r.get('accepted')) ) && this.actEdit && r.get('trans_out'));
            this.btnEdit.setIconClass(filterEdit ? 'silk13-pencil' : 'silk13-eye');
            this.btnEdit.setText(filterEdit ? "Ubah" : "Lihat");
        }
    },
    setFilterData: function (sts) {
        this.status = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        if (this.store.baseParams.status && this.store.baseParams.status == this.status) {
            this.store.baseParams = {
                mode: "grid",
                status: this.status,
                urole: UROLE
            };
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                mode: "grid",
                status: this.status,
                urole: UROLE
            };
            this.botbar.moveFirst();
        }
    },
    addSend: function () {
        this.loadForm(true);
    },
    addReceive: function () {
        this.loadForm(false);
    },
    loadForm: function (transOut) {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined && (transOut == false)) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        var form = new jun.TransferMaterialWin({
            modez: 0,
            title: transOut ? 'Kirim Barang' : 'Terima Barang',
            iconCls: transOut ? 'icon-red_arrow_right' : 'icon-green_arrow_left',
            storeDetail: jun.rztTransferMaterialDetail,
            // storeComboMaterial: jun.rztMaterialForBOM,
            // tglExpiredStore: new jun.MaterialExpDateStore(),
            accepting: false,
            transOut: transOut
        });
        if (transOut == false) {
            form.formz.getForm().loadRecord(this.record);
            var idz = selectedz.json.transfer_material_id;
            jun.rztTransferMaterialDetail.baseParams = {
                transfer_material_id: idz
            };
            jun.rztTransferMaterialDetail.load();
            jun.rztTransferMaterialDetail.baseParams = {};
        }
        form.show(this);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        this.accepting = false;
        // var batch_id = this.record.get('batch_id');
        // if (!batch_id) this.showWinEditForm(null);
        // else
        // {
        //     var r = new jun.Batchstore();
        //     r.load({
        //         params: {batch_id: batch_id},
        //         scope: this,
        //         callback: this.showWinEditForm
        //     });
        // }
    },
    showWinEditForm: function (r) {
        var selectedz = this.sm.getSelected().json;
        var idz = selectedz.transfer_material_id;
        var filterEdit = ((!parseInt(selectedz.accepted)) && parseInt(selectedz.trans_out));
//        var filterUser = this.accepting? !(parseInt(selectedz.pengirim) == USER_PPIC) : parseInt(selectedz.pengirim) == USER_PPIC;
//         var batch_id = this.record.get('batch_id');
        var form = new jun.TransferMaterialWin({
            modez: this.accepting ? 2 : (filterEdit ? 1 : 2),
            trans_id: idz,
            title: (this.accepting ? "Terima" : (filterEdit ? "Ubah" : "Lihat")) + " Transfer Material",
            iconCls: (this.accepting ? 'silk13-tick' : (filterEdit ? 'silk13-pencil' : 'silk13-eye')),
            storeDetail: jun.rztTransferMaterialDetail,
            storeComboMaterial: jun.rztMaterialCmp,
            accepting: this.accepting,
            transOut: parseInt(selectedz.trans_out)
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTransferMaterialDetail.baseParams = {
            transfer_material_id: idz
        };
        jun.rztTransferMaterialDetail.load();
        jun.rztTransferMaterialDetail.baseParams = {};
    },
    btnPrintOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        Ext.getCmp("form-TransferMaterialGrid").getForm().standardSubmit = !0;
        Ext.getCmp("form-TransferMaterialGrid").getForm().url = "Report/PrintTransferMaterial";
        this.transfer_material_id.setValue(selectedz.json.transfer_material_id);
        var form = Ext.getCmp('form-TransferMaterialGrid').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnAcceptOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        this.accepting = true;
        this.showWinEditForm(null);
    }
});
