jun.SalesDetailsanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalesDetailsanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DetilPenjualanStoreId',
            autoLoad: !1,
            autoSave: !1,
            url: 'SalesDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_detail_id'},
                {name: 'sales_id'},
                {name: 'sat'},
                {name: 'jml', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'disc2', type: 'float'},
                {name: 'pot1', type: 'float'},
                {name: 'nominal', type: 'float'},
                {name: 'pcs'},
                {name: 'totalpot', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'hpp', type: 'float'},
                {name: 'barang_id'},
                {name: 'ketpot'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'disc3', type: 'float'},
                {name: 'pot2', type: 'float'},
                {name: 'pot3', type: 'float'},
                {name: 'loc_code'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var total = this.sum("nominal");
        var disc = this.sum("totalpot");
        var bruto = this.sum("bruto");
        var vatrp = this.sum("vatrp");
        var tot_pot1 = this.sum("pot1");
        var tot_pot2 = this.sum("pot2");
        var tot_pot3 = this.sum("pot3");
        Ext.getCmp("totalpotsalesid").setValue(disc);
        Ext.getCmp("brutosalesid").setValue(bruto);
        Ext.getCmp("vatrpsalesid").setValue(vatrp);
        Ext.getCmp("totalsalesid").setValue(total);
        Ext.getCmp("tot_pot1salesid").setValue(tot_pot1);
        Ext.getCmp("tot_pot2salesid").setValue(tot_pot2);
        Ext.getCmp("tot_pot3salesid").setValue(tot_pot3);
    }
});
jun.rztSalesDetail = new jun.SalesDetailsanstore();
jun.ReturSalesDetailsanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturSalesDetailsanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturDetilPenjualanStoreId',
            autoLoad: !1,
            autoSave: !1,
            url: 'SalesDetails/retur',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_detail_id'},
                {name: 'sales_id'},
                {name: 'sat'},
                {name: 'jml', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'disc2', type: 'float'},
                {name: 'pot1', type: 'float'},
                {name: 'nominal', type: 'float'},
                {name: 'pcs'},
                {name: 'totalpot', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'hpp', type: 'float'},
                {name: 'barang_id'},
                {name: 'ketpot'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'disc3', type: 'float'},
                {name: 'pot2', type: 'float'},
                {name: 'pot3', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var total = this.sum("nominal");
        var disc = this.sum("totalpot");
        var bruto = this.sum("bruto");
        var vatrp = this.sum("vatrp");
        var tot_pot1 = this.sum("pot1");
        var tot_pot2 = this.sum("pot2");
        var tot_pot3 = this.sum("pot3");
        Ext.getCmp("totalpotsalesid").setValue(disc);
        Ext.getCmp("brutosalesid").setValue(bruto);
        Ext.getCmp("vatrpsalesid").setValue(vatrp);
        Ext.getCmp("totalsalesid").setValue(total);
        Ext.getCmp("tot_pot1salesid").setValue(tot_pot1);
        Ext.getCmp("tot_pot2salesid").setValue(tot_pot2);
        Ext.getCmp("tot_pot3salesid").setValue(tot_pot3);
    }
});
jun.rztReturSalesDetail = new jun.ReturSalesDetailsanstore();
