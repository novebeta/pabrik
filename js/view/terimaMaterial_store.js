jun.TerimaMaterialstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TerimaMaterialstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaMaterialStoreId',
            url: 'TerimaMaterial',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'terima_material_id'},
                {name: 'doc_ref'},
                {name: 'supplier_id'},
                {name: 'tgl_terima'},
                {name: 'no_sj_supplier'},
                {name: 'tgl_sj_supplier'},
                {name: 'recipient'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'doc_ref_inv'},
                {name: 'tgl_terima_inv'},
                {name: 'no_inv_supplier'},
                {name: 'tgl_inv_supplier'},
                {name: 'no_faktur_pajak'},
                {name: 'tgl_no_faktur_pajak'},
                {name: 'currency'},
                {name: 'IDR_rate', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'discount', type: 'float'},
                {name: 'persen_dpp', type: 'float'},
                {name: 'dpp', type: 'float'},
                {name: 'persen_tax', type: 'float'},
                {name: 'tax', type: 'float'},
                {name: 'pph_persen', type: 'float'},
                {name: 'pph', type: 'float'},
                {name: 'total_bayar', type: 'float'},
                {name: 'term_of_payment'},
                {name: 'tdate_inv'},
                {name: 'id_user_inv'},
                {name: 'tipe_invoice', type: 'integer'},
                {name: 'pembayaran_supplier_id'},
                {name: 'status'},
                {name: 'urole', type: 'int'}
            ]
        }, cfg));
    }
});
jun.rztTerimaMaterial = new jun.TerimaMaterialstore();
jun.rztInvoiceSupplier = new jun.TerimaMaterialstore();
//Store untuk PO items, yang dipakai saat penerimaan material
jun.POitemsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.POitemsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'POitemsstoreStoreId',
            url: 'TerimaMaterial/GetPoItems',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'kode_material'},
                {name: 'nama_material'},
                {name: 'sat'},
                {name: 'price'},
                {name: 'qty_terima'}
            ]
        }, cfg));
    }
});
jun.rztStatusSuratJalanSupplierStore = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'displayText'
    ],
    data: [
        [SJ_SUPPLIER_OPEN, 'OPEN'],
        [SJ_SUPPLIER_INVOICED, 'INVOICED'],
        [SJ_SUPPLIER_FPT, 'FPT'],
        [SJ_SUPPLIER_PAID, 'PAID']
    ]
});
jun.rztStatusInvoiceSupplierStore = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'displayText'
    ],
    data: [
        [SJ_SUPPLIER_INVOICED, 'OPEN'],
        [SJ_SUPPLIER_FPT, 'FPT'],
        [SJ_SUPPLIER_PAID, 'PAID']
    ]
});