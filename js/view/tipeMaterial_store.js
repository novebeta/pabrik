jun.TipeMaterialstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TipeMaterialstore.superclass.constructor.call(this, Ext.apply({
            id: 'tipe_material_id',
            storeId: 'TipeMaterialStoreId',
            url: 'TipeMaterial',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tipe_material_id'},
                {name: 'nama_tipe_material'},
                {name: 'have_stock'},
                {name: 'purchasable'},
                {name: 'salesable'},
                {name: 'jenis'},
                {name: 'coa_jual'},
                {name: 'coa_beli'},
                {name: 'coa_persediaan'},
                {name: 'coa_hpp'},
                {name: 'coa_retur_jual'},
                {name: 'coa_jual_disc'}
            ]
        }, cfg));
    }
});
jun.rztTipeMaterial = new jun.TipeMaterialstore();
jun.rztTipeMaterialCmp = new jun.TipeMaterialstore();
jun.rztTipeMaterialLib = new jun.TipeMaterialstore();
//jun.rztTipeMaterial.load();
