jun.Salesmanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Salesmanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesmanStoreId',
            url: 'Salesman',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salesman_id'},
                {name: 'salesman_code'},
                {name: 'salesman_name'},
                {name: 'status'},
                {name: 'bagian'},
                {name: 'tempat_lahir'},
                {name: 'tgl_lahir'},
                {name: 'alamat'},
                {name: 'phone'},
                {name: 'tgl_gabung'}
            ]
        }, cfg));
    },
    FilterData: function () {
        this.filter([
            {property: "status", value: "1"}
        ])
    }
});
jun.rztSalesman = new jun.Salesmanstore();
jun.rztSalesmanCmp = new jun.Salesmanstore();
jun.rztSalesmanLib = new jun.Salesmanstore();
//jun.rztSalesman.load();
