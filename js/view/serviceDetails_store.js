jun.ServiceDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ServiceDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ServiceDetailsStoreId',
            url: 'ServiceDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'service_detail_id'},
                {name: 'service_id'},
                {name: 'sat'},
                {name: 'jml'},
                {name: 'nominal'},
                {name: 'hpp'},
                {name: 'barang_id'}
            ]
        }, cfg));
    }
});
jun.rztServiceDetails = new jun.ServiceDetailsstore();
//jun.rztServiceDetails.load();
