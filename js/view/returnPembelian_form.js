jun.ReturnPembelianWin = Ext.extend(Ext.Window, {
    title: 'Return Pembelian',
    modez: 1,
    width: 900,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-ReturnPembelian',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. Return:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        x: 85,
                        y: 32,
                        allowBlank: false,
                        value: new Date(),
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Gudang:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'loc_code',
                        id: 'retur_beli_loc_code_id',
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'loc_code',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        x: 400,
                        y: 2
                    },
                    // {
                    //     xtype: "label",
                    //     text: "Supplier :",
                    //     x: 295,
                    //     y: 5
                    // },
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     fieldLabel: 'supplier_id',
                    //     store: jun.rztSupplierLib,
                    //     hiddenName: 'supplier_id',
                    //     valueField: 'supplier_id',
                    //     displayField: 'supplier_name',
                    //     ref: '../supplier_id',
                    //     x: 400,
                    //     y: 2,
                    //     allowBlank: false,
                    //     readOnly: true,
                    //     width: 175
                    // },
                    {
                        xtype: "label",
                        text: "No. SJ Supplier:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'no_sj_supplier',
                        id: 'no_sj_supplierid',
                        ref: '../no_sj_supplier',
                        width: 175,
                        allowBlank: false,
                        readOnly: true,
                        x: 400,
                        y: 32
                    },
                    new jun.ReturnPembelianDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        // terima_material_id: this.terima_material_id,
                        store: this.storeDetail
                        // readOnly: ((this.modez < 2)?false:true)
                    }),
                    {
                        xtype: 'hidden',
                        hideLabel: false,
                        name: 'no_inv_supplier',
                        ref: '../no_inv_supplier'
                    },
                    {
                        xtype: 'hidden',
                        hideLabel: false,
                        name: 'terima_barang_id',
                        ref: '../terima_barang_id'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturnPembelianWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        // this.btnCancel.setText(this.modez < 2? 'Batal':'Tutup');
        // if (this.modez == 1) { //edit
        //     this.btnSave.setVisible(false);
        //     this.btnSaveClose.setVisible(true);
        // } else if (this.modez == 0){ //create
        //     this.btnSave.setVisible(false);
        //     this.btnSaveClose.setVisible(true);
        // }else{ //view
        //     this.btnSave.setVisible(false);
        //     this.btnSaveClose.setVisible(false);
        //     this.formz.getForm().applyToFields({ readOnly: true });
        // }
    },
    onWinClose: function () {
        this.storeDetail.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'ReturnPembelian/create/';
        Ext.getCmp('form-ReturnPembelian').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            // urole: UROLE,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    this.storeDetail.data.items, "data"))
            },
            success: function (f, a) {
                if (Ext.getCmp('docs-jun.ReturnPembelianGrid') !== undefined) Ext.getCmp('docs-jun.ReturnPembelianGrid').reloadStore();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    Ext.getCmp('form-ReturnPembelian').getForm().reset();
                    this.btnDisabled(false);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
