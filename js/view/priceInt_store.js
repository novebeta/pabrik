jun.PriceIntstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PriceIntstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PriceIntStoreId',
            url: 'PriceInt',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'price_int_id'},
                {name: 'amount'},
                {name: 'wil_int_id'},
                {name: 'barang_id'}
            ]
        }, cfg));
    }
});
jun.rztPriceInt = new jun.PriceIntstore();
//jun.rztPriceInt.load();
