jun.FolderBreakdownstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FolderBreakdownstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FolderBreakdownStoreId',
            url: 'Folder/Breakdown',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'folder_id'},
                {name: 'type_'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'note_'}
            ]
        }, cfg));
    }
});
jun.rztFolderBreakdown = new jun.FolderBreakdownstore();
//jun.rztFolderBreakdown.load();
