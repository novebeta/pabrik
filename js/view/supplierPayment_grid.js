jun.SupplierPaymentGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pembayaran Supplier",
    id: 'docs-jun.SupplierPaymentGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. BG/Cek',
            sortable: true,
            resizable: true,
            dataIndex: 'no_bg_cek',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'No. Bukti',
            sortable: true,
            resizable: true,
            dataIndex: 'no_bukti',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        if (jun.rztSupplierCmp.getTotalCount() === 0) jun.rztSupplierCmp.load();
        jun.rztSupplierPayment.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglsupplierpaymentgridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztSupplierPayment;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Pembayaran',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Pembayaran',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Supplier Payment',
                    ref: '../btnPrintSupplierPayment'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglsupplierpaymentgridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-PrintPaymentSupplier",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "supplier_payment_id",
                            ref: "../../supplier_payment_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.SupplierPaymentGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrintSupplierPayment.on('Click', this.printSupplierPayment, this);
        this.tgl.on('select', this.selectTgl, this);
        //this.tgl.on('select', function () {
        //    this.store.reload();
        //}, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        // this.store.removeAll();
        // this.store.load();
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    printSupplierPayment: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Supplier Payment");
            return;
        }
        Ext.getCmp("form-PrintPaymentSupplier").getForm().standardSubmit = !0;
        Ext.getCmp("form-PrintPaymentSupplier").getForm().url = "Report/PembayaranSupp";
        this.supplier_payment_id.setValue(selectedz.json.supplier_payment_id);
        var form = Ext.getCmp('form-PrintPaymentSupplier').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SupplierPaymentWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.supplier_payment_id;
        var form = new jun.SupplierPaymentWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //jun.rztSupplierPaymentDetil.load({supplier_payment_id:idz});
        jun.rztSupplierPaymentDetil.baseParams = {
            supplier_payment_id: idz
        };
        jun.rztSupplierPaymentDetil.load();
        jun.rztSupplierPaymentDetil.baseParams = {};
    }
});
