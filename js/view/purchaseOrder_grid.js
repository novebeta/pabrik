jun.PurchaseOrderGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Order",
    id: 'docs-jun.PurchaseOrderGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 80
        },
        {
            header: 'No. PR',
            sortable: true,
            resizable: true,
            dataIndex: 'pr_doc_ref',
            width: 80
        },
        {
            header: 'Attn. Name',
            sortable: true,
            resizable: true,
            dataIndex: 'attn_name',
            width: 80
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 60
        },
        {
            header: 'Tgl Delivery',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_delivery',
            width: 60
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 120,
            renderer: jun.renderNamaSuppliers
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 40,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case P0_OPEN :
                        return 'OPEN';
                    case PO_PARTIALLY_RECEIVED :
                        return 'PARTIALLY RECEIVED';
                    case PO_RECEIVED :
                        return 'RECEIVED';
                    case PO_INVOICED :
                        return 'INVOICED';
                    case PO_CANCELED :
                        return 'CANCELED';
                    case PO_CLOSED :
                        return 'CLOSED';
                }
            }
        }
    ],
    userPermission: function (actEdit, actPrint, actRelease, actCancel, actInputInvoice, viewPrice) {
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
        this.actRelease = actRelease;
        this.actCancel = actCancel;
        this.actInputInvoice = actInputInvoice;
        this.viewPrice = viewPrice;
        //-- jika yang buka purchasing maka akan menampilkan semua PO
        //-- jika yang buka bukan purchasing maka hanya PO untuk PR miliknya yang akan ditampilkan
        this.viewAllData = (this.actPrint || this.actRelease || this.actCancel);
    },
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztSupplierLib.getTotalCount() === 0) {
            jun.rztSupplierLib.load();
        }
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        if (jun.rztStorageLocationLib.getTotalCount() === 0) {
            jun.rztStorageLocationLib.load();
        }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1, 1, 1, 0);
                break;
            case USER_PPIC:
                this.userPermission(0, 0, 0, 0, 0, 0);
                break;
            case USER_GA:
                this.userPermission(0, 0, 0, 0, 0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(1, 1, 1, 1, 0, 1);
                break;
            case USER_PEMBELIAN:
                this.userPermission(0, 0, 0, 0, 1, 1);
                break;
            default:
                this.userPermission(0, 0, 0, 0, 0, 0);
        }
        this.store = jun.rztPurchaseOrder;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat PO',
                    ref: '../btnBuatPO',
                    iconCls: 'silk13-tick'
                },
                {
                    xtype: 'tbseparator'//,
                    // hidden: (this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actEdit || this.actView) ? false : true
                },
//                {
//                    xtype: 'button',
//                    text: 'Release PO',
//                    ref: '../btnRelease',
//                    iconCls: 'silk13-flag_green',
//                    hidden: this.actRelease?false:true
//                },
                {
                    xtype: 'button',
                    text: 'Print PO',
                    ref: '../btnPrintPO',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Cancel PO',
                    ref: '../btnCancel',
                    iconCls: 'silk13-cross',
                    hidden: this.actCancel ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Close PO',
                    ref: '../btnClose',
                    iconCls: 'silk13-stop'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Penerimaan Barang',
                    ref: '../btnAddTB'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Input Invoice',
                //     ref: '../btnInvoiceSupplier',
                //     iconCls: 'silk13-page_white_edit',
                //     hidden: this.actInputInvoice?false:true
                // },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    ref: '../tgl',
                    noPastYears: false
                }),
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportPurchaseOrder",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "po_id",
                            ref: "../../po_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actRelease || this.actCancel || this.actPrint || this.actInputInvoice) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_po: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_po: P0_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'PARTIALLY RECEIVED',
                                status_po: PO_PARTIALLY_RECEIVED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'RECEIVED',
                                status_po: PO_RECEIVED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'INVOICED',
                                status_po: PO_INVOICED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'CANCELED',
                                status_po: PO_CANCELED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'CLOSED',
                                status_po: PO_CLOSED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            }
                        ]
                    }
                }
            ]
        };
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             ref: '../botbar',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }
        //     ]
        // };
        jun.PurchaseOrderGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.editOrViewPO, this);
//        this.btnRelease.on('Click', this.btnReleaseOnClick, this);
        this.btnBuatPO.on('Click', this.btnBuatPOonClick, this);
        this.btnCancel.on('Click', this.btnCancelOnClick, this);
        this.btnClose.on('Click', this.btnCloseOnClick, this);
        this.btnPrintPO.on('Click', this.printPurchaseOrder, this);
        // this.btnInvoiceSupplier.on('Click', this.InputInvoiceSupplier, this);
        this.btnAddTB.on('Click', this.addTB, this);
        this.tgl.on('select', this.selectTgl, this);
        this.on('rowdblclick', this.editOrViewPO, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(P0_OPEN);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (this.actEdit) {
            var filter = r.get('adt') == "0" && r.get('status') == P0_OPEN;
            this.btnEdit.setIconClass(filter ? 'silk13-pencil' : 'silk13-eye');
            this.btnEdit.setText(filter ? "Ubah" : "Lihat");
        }
    },
    btnBuatPOonClick: function () {
        // if (this.sm.getSelected() == undefined) {
        //     Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data PR");
        //     return;
        // }
        // if (!this.actCreatePO) {
        //     //-- hanya user purchasing yang dapat membuat PO
        //     Ext.MessageBox.alert("Warning", "Anda tidak dapat membuat PO.");
        //     return;
        // }
        // var status = Number(this.sm.getSelected().json.status);
        // if (status != PR_OPEN && status != PR_PROCESS) {
        //     //-- hanya PR status OPEN yang dapat dibuat PO
        //     var msg = "";
        //     switch (status) {
        //         case PR_CLOSED :
        //             msg = "Purchase Order untuk PR ini telah dibuat.";
        //             break;
        //         case PR_REJECTED :
        //             msg = "PR telah ditolak.";
        //             break;
        //     }
        //     Ext.MessageBox.alert("Warning", "Tidak dapat membuat Purchase Order untuk PR ini.<br>" + msg);
        //     return;
        // }
        this.createPO();
    },
    createPO: function () {
        // if (this.sm.getSelected() == undefined) {
        //     Ext.MessageBox.alert("Warning", "Anda belum memilih data PR.");
        //     return;
        // }
        // var r = this.sm.getSelected().json;
        var form = new jun.PurchaseOrderWin({
            modez: 0,
            // tipe_material_id: r.tipe_material_id,
            title: "Buat Purchase Order",
            storeDetail: jun.rztCreatePODetails,
            // pr_id: r.pr_id,
            iconCls: 'silk13-tick'
        });
        form.show(this);
    },
    addTB: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Purchase Order");
            return;
        }
        if (selectedz.json.status > P0_RELEASED) {
            Ext.MessageBox.alert("Warning", "Purchase Order tidak bisa dibuat karena status PO tidak lagi RELEASE");
            return;
        }
        var idz = selectedz.json.po_id;
        var form = new jun.TerimaBarangWin({modez: 0});
        form.show(this);
        this.record.data.tgl = DATE_NOW;
        this.record.data.status = 0;
        this.record.data.doc_ref = null;
        form.formz.getForm().loadRecord(this.record);
        jun.rztTerimaBarangDetails.baseParams = {
            po_id: idz
        };
        jun.rztTerimaBarangDetails.load();
        jun.rztTerimaBarangDetails.baseParams = {};
    },
    selectTgl: function (t, d) {
        this.reloadStore();
    },
    setFilterData: function (sts) {
        this.status_po = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        if (this.store.baseParams.status && this.store.baseParams.status == this.status_po) {
            this.store.baseParams = {
                mode: "grid",
                viewprice: this.viewPrice,
                status: this.status_po,
                tgl: this.tgl.getValue(),
                urole: (this.viewAllData ? 'all' : UROLE)
            };
            // this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                mode: "grid",
                viewprice: this.viewPrice,
                status: this.status_po,
                tgl: this.tgl.getValue(),
                urole: (this.viewAllData ? 'all' : UROLE)
            };
            // this.botbar.moveFirst();
        }
        this.store.load();
    },
    editOrViewPO: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Purchase Order");
            return;
        }
        //hanya PO berstatus OPEN yang dapat diedit
        var filter = selectedz.json.status == 0;//selectedz.json.adt == "0" && (selectedz.json.status <= PO_RECEIVED) && this.actEdit;
        var ttl = "";
        switch (Number(selectedz.json.status)) {
            case P0_OPEN :
                ttl = "OPEN";
                break;
            case P0_RELEASED :
                ttl = "RELEASED";
                break;
            case PO_PARTIALLY_RECEIVED :
                ttl = "PARTIALLY RECEIVED";
                break;
            case PO_RECEIVED :
                ttl = "RECEIVED";
                break;
            //case PO_INVOICED : ttl = "INVOICED"; break;
            case PO_CANCELED :
                ttl = "CANCELED";
                break;
            case PO_CLOSED :
                ttl = "CLOSED";
                break;
        }
        var form = new jun.PurchaseOrderWin({
            modez: (filter ? 1 : 2),
            po_id: selectedz.json.po_id,
            title: ( filter ? "Ubah" : "Lihat") + " Purchase Order [" + ttl + "]",
            storeDetail: jun.rztPurchaseOrderDetails,
            tipe_material_id: selectedz.json.tipe_material_id,
            pr_id: selectedz.json.pr_id,
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPurchaseOrderDetails.baseParams = {
            po_id: selectedz.json.po_id,
            viewprice: this.viewPrice
        };
        jun.rztPurchaseOrderDetails.load();
        jun.rztPurchaseOrderDetails.baseParams = {};
    },
    btnCloseOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data PO");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status != P0_OPEN && status != PO_RECEIVED) {
            var msg = "";
            switch (status) {
                // case PO_RECEIVED :
                //     msg = "Sudah ada penerimaan barang untuk PO ini.";
                //     break;
                //case PO_INVOICED : msg = "PO telah dibayar."; break;
                case PO_CANCELED :
                    msg = "Telah dilakukan pembatalan untuk PO ini.";
                    break;
                case PO_CLOSED :
                    msg = "PO telah ditutup.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "Purchase Order tidak dapat diclose.<br>" + msg);
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin akan menutup PO ini?',
            function (btn) {
                if (btn == 'no') {
                    return;
                }
                var record = this.sm.getSelected();
                Ext.Ajax.request({
                    url: 'PurchaseOrder/Close/id/' + record.json.po_id,
                    method: 'POST',
                    success: function (f, a) {
                        jun.rztPurchaseOrder.reload();
                        var response = Ext.decode(f.responseText);
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: response.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }, this);
    },
    btnCancelOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data PO");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        // if (status != P0_OPEN && status != P0_RELEASED) {
        if (status != P0_OPEN) {
            var msg = "";
            switch (status) {
                case PO_RECEIVED :
                    msg = "Sudah ada penerimaan barang untuk PO ini.";
                    break;
                //case PO_INVOICED : msg = "PO telah dibayar."; break;
                case PO_CANCELED :
                    msg = "Telah dilakukan pembatalan untuk PO ini.";
                    break;
                case PO_CLOSED :
                    msg = "PO telah ditutup.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "Purchase Order tidak dapat dibatalkan.<br>" + msg);
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin akan membatalkan PO ini?', this.cancelPO, this);
    },
    cancelPO: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'PurchaseOrder/Cancel/id/' + record.json.po_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPurchaseOrder.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnReleaseOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data PO");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status != P0_OPEN) {
            var msg = "";
            switch (status) {
                case PO_CANCELED :
                    msg = "Purchase Order tidak dapat direlease.<br>Telah dilakukan pembatalan untuk PO ini.";
                    break;
                default :
                    msg = "Purchase Order telah direlease.";
                    break;
            }
            Ext.MessageBox.alert("Warning", msg);
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin akan merelease PO ini?<br>PO yang telah direlease tidak dapat dilakukan pengubahan.', this.releasePO, this);
    },
    releasePO: function (btn) {
        /*
         if (btn == 'no') {
         return;
         }
         */
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'PurchaseOrder/Release/id/' + record.json.po_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPurchaseOrder.reload();
                /*
                 var response = Ext.decode(f.responseText);
                 Ext.MessageBox.show({
                 title: 'Info',
                 msg: response.msg,
                 buttons: Ext.MessageBox.OK,
                 icon: Ext.MessageBox.INFO
                 });
                 */
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printPurchaseOrder: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Purchase Order");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status == P0_OPEN) this.releasePO();
        /*
         if (status != P0_RELEASED && status != PO_RECEIVED){
         var msg = "";
         switch(status){
         case P0_OPEN : msg = "PO belum direlease."; break;
         case PO_CANCELED : msg = "Telah dilakukan pembatalan untuk PO ini."; break;
         case PO_CLOSED : msg = "PO telah ditutup."; break;
         }
         Ext.MessageBox.alert("Warning", "Tidak dapat mengunduh dokumen Purchase Order.<br>"+msg);
         return;
         }
         */
        Ext.getCmp("form-ReportPurchaseOrder").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchaseOrder").getForm().url = "Report/PrintPurchaseOrder";
        this.po_id.setValue(selectedz.json.po_id);
        var form = Ext.getCmp('form-ReportPurchaseOrder').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    InputInvoiceSupplier: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        if (selectedz.json.status != P0_RELEASED) {
            var msg = "";
            switch (selectedz.json.status) {
                case P0_OPEN :
                    msg = "Tidak dapat input Invoice. Purchase Order belum direlease.";
                    return;
                default :
                    msg = "Tidak dapat input Invoice.";
            }
            Ext.MessageBox.alert("Warning", msg);
            return;
        }
        var form = new jun.InvoiceSupplierWin({
            modez: 0,
            iconCls: 'silk13-page_white_edit',
            title: "Invoice Supplier [ Down Payment ]",
            storeDetail: jun.rztInvoiceSupplierDetailsForDP,
            tipeInvoice: TIPE_INV_DOWNPAYMENT
        });
        form.show(this);
        //form.formz.getForm().loadRecord(this.record);
        form.cmbSupplier.setValue(this.record.get("supplier_id"));
        form.term_of_payment.setValue(this.record.get("term_of_payment"));
        form.currency.setValue(this.record.get("currency"));
        form.IDR_rate.setValue(this.record.get("IDR_rate"));
        form.griddetils.store.baseParams = {po_id: selectedz.json.po_id};
        form.griddetils.store.load({
            callback: function () {
                form.griddetils.store.refreshData();
            },
            scope: this
        });
        form.griddetils.store.baseParams = {};
    }
});
