jun.TerimaBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TerimaBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaBarangStoreId',
            url: 'TerimaBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'terima_barang_id'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'discount', type: 'float'},
                {name: 'dpp', type: 'float'},
                {name: 'tax', type: 'float'},
                {name: 'persen_tax', type: 'float'},
                {name: 'grand_total', type: 'float'},
                {name: 'delivery', type: 'float'},
                {name: 'top', type: 'float'},
                {name: 'tgl'},
                {name: 'tanda_terima'},
                {name: 'po_in_id'},
                {name: 'no_faktur_tax'},
                {name: 'no_invoice'},
                {name: 'status'},
                {name: 'tgl_inv'},
                {name: 'grn_tdate'},
                {name: 'inv_tdate'},
                {name: 'grn_id_user'},
                {name: 'inv_id_user'},
                {name: 'doc_ref_inv'},
                {name: 'tgl_tempo'},
                {name: 'arus'},
                {name: 'parent_id'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
jun.rztTerimaBarang = new jun.TerimaBarangstore({baseParams: {mode: 'grid'}});
jun.rztSupplierInvoice = new jun.TerimaBarangstore({baseParams: {status: 1, mode: 'grid'}});
//jun.rztTerimaBarang.load();
