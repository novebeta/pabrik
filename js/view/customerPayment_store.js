jun.CustomerPaymentstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CustomerPaymentstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomerPaymentStoreId',
            url: 'CustomerPayment',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'customer_payment_id'},
                {name: 'total', type: 'float'},
                {name: 'no_bg_cek'},
                {name: 'no_giro'},
                {name: 'no_bukti'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'bank_id'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'customer_id'},
                {name: 'remark'},
                {name: 'uang_diterima', type: 'float'},
                {name: 'nama_customer'},
                {name: 'void'}
            ]
        }, cfg));
    }
});
jun.rztCustomerPayment = new jun.CustomerPaymentstore({baseParams: {mode: 'grid'}});
//jun.rztCustomerPayment.load();
