jun.HppWin = Ext.extend(Ext.Window, {
    title: 'Hpp',
    modez: 1,
    width: 400,
    height: 220,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Hpp',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../start_date',
                        fieldLabel: 'tgl',
                        name: 'start_date',
                        format: 'd M Y',
                        value: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
                        allowBlank: false,
                        hideLabel: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../end_date',
                        fieldLabel: 'tgl',
                        name: 'end_date',
                        format: 'd M Y',
                        value: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0),
                        allowBlank: false,
                        hideLabel: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Hitung',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.HppWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
        } else { //view
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-Hpp').getForm().submit({
            url: 'Hpp/create',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                hpp_id: this.hpp_id
            },
            success: function (f, a) {
                jun.rztHpp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Hpp').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});