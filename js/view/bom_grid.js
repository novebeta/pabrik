jun.BomGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Bill Of Material",
    id: 'docs-jun.BomGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No BOM',
            sortable: true,
            resizable: true,
            dataIndex: 'no_bom',
            width: 80
        },
        {
            header: 'Tgl Keluar',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 60,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Kode Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_material',
            width: 80
        },
        {
            header: 'Nama Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_material',
            width: 140
        },
        {
            header: 'Kode Formula',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_formula',
            width: 80
        },
        {
            header: 'Aktif',
            sortable: true,
            resizable: true,
            dataIndex: 'enable',
            width: 25,
            renderer: function (value, metaData, record, rowIndex) {
                (value == 1) && (metaData.css += ' silk13-tick ');
                return '';
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        this.store = jun.rztBom;
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        if (jun.rztMaterialCmp.getTotalCount() === 0) jun.rztMaterialCmp.load();
        if (jun.rztTipeMaterialLib.getTotalCount() === 0) jun.rztTipeMaterialLib.load();
        if (jun.rztMaterialForBOM.getTotalCount() === 0) jun.rztMaterialForBOM.load();
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) jun.rztStorageLocationCmp.load();
        this.userPermission(1, 1, 1);
        // switch (UROLE) {
        //     case USER_ADMIN:
        //         this.userPermission(1, 1, 1);
        //         break;
        //     case USER_PPIC:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_GA:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_PURCHASING:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_PEMBELIAN:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_RND:
        //         this.userPermission(1, 1, 1);
        //         break;
        //     default:
        //         this.userPermission(0, 0, 0);
        // }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat BOM',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: this.actDelete ? false : true
                },
                // {
                //     xtype: 'tbseparator',
                //     hidden: (this.actCreate || this.actEdit || this.actDelete || this.actView) ? false : true
                // },
                // {
                //     xtype: 'button',
                //     text: 'Tipe : <b>All</b>',
                //     ref: '../btnFilter',
                //     menu: {
                //         xtype: 'menu',
                //         ref: '../../menuFilter',
                //         id: 'BomGrid_menuFilterid',
                //         items: [
                //             {
                //                 text: 'All',
                //                 grup_id: '0',
                //                 listeners: {
                //                     click: function (m, e) {
                //                         m.parentMenu.ownerCt.setText('Tipe : <b>' + m.text + '</b>');
                //                         Ext.getCmp('docs-jun.BomGrid').setFilterData(m.grup_id);
                //                     }
                //                 }
                //             }
                //         ]
                //     }
                // },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Kode Produk</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'kode_barang_rnd',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Nomor BOM',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'no_bom';
                                        Ext.getCmp('docs-jun.BomGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Kode Produk',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_material';
                                        Ext.getCmp('docs-jun.BomGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Nama Produk',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'nama_material';
                                        Ext.getCmp('docs-jun.BomGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Kode Formula',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_formula';
                                        Ext.getCmp('docs-jun.BomGrid').txtFind.focus().setValue("");
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                }
            ]
        };
        //this.storeBarang = [];
        // this.groupMaterialStore = new jun.Grupstore({baseParams: {havestock: 1}});
        // this.groupMaterialStore.on({
        //     scope: this,
        //     load: {
        //         fn: this.addMenu
        //     }
        // });
        // this.groupMaterialStore.load();
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        jun.BomGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.txtFind.on('keyup', this.find, this);
        this.btnAdd.on('Click', this.onbtnAddClick, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        // this.filter_grup_id = '0';
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    // addMenu: function () {
    //     this.groupMaterialStore.each(function (r) {
    //         //menu add/create
    //         Ext.getCmp('menuAddBOMid').add({
    //             text: r.data.nama_grup,
    //             nama_grup: r.data.nama_grup,
    //             grup_id: r.data.grup_id,
    //             listeners: {
    //                 click: function (m, e) {
    //                     Ext.getCmp('docs-jun.BomGrid').addBOM(m.grup_id, this.nama_grup);
    //                 }
    //             }
    //         });
    //         //menu filter
    //         Ext.getCmp('BomGrid_menuFilterid').add({
    //             text: r.data.nama_grup,
    //             grup_id: r.data.grup_id,
    //             listeners: {
    //                 click: function (m, e) {
    //                     m.parentMenu.ownerCt.setText('Tipe : <b>' + m.text + '</b>');
    //                     Ext.getCmp('docs-jun.BomGrid').setFilterData(m.grup_id);
    //                 }
    //             }
    //         });
    //     });
    // },
    // setFilterData: function (n) {
    //     this.filter_grup_id = n;
    //     if (this.store.baseParams.grup_id && this.store.baseParams.grup_id == n) {
    //         this.store.baseParams = {
    //             mode: "grid",
    //             grup_id: this.filter_grup_id,
    //             fieldsearch: this.filter_fieldsearch,
    //             valuesearch: this.filter_valuesearch
    //         };
    //         this.botbar.doRefresh();
    //     } else {
    //         this.store.baseParams = {
    //             mode: "grid",
    //             grup_id: this.filter_grup_id,
    //             fieldsearch: this.filter_fieldsearch,
    //             valuesearch: this.filter_valuesearch
    //         };
    //         this.botbar.moveFirst();
    //     }
    // },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.filter_valuesearch = c.getValue();
            this.filter_fieldsearch = this.btnFind.fieldsearch;
            this.store.baseParams = {
                mode: "grid",
                grup_id: this.filter_grup_id,
                fieldsearch: this.filter_fieldsearch,
                valuesearch: this.filter_valuesearch
            };
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onbtnAddClick: function (t, e) {
        t.setDisabled(true);
        var form = new jun.BomWin({
            modez: 0,
            title: "Buat BOM",
            iconCls: 'silk13-add'
        });
        form.show();
        t.setDisabled(false);
    },
    addBOM: function (group, nama) {
        var form = new jun.BomWin({
            modez: 0,
            // grup_id: group,
            // storeBarang: new jun.Barangstore({
            //     baseParams: {grup_id: group}
            // }),
            title: "Buat BOM - " + nama,
            iconCls: 'silk13-add'
        });
        form.show();
        // form.storeBarang.load();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data BOM.");
            return;
        }
        var bom_id = selectedz.json.bom_id;
        var brg = jun.rztBarangLib.getAt(jun.rztBarangLib.find('barang_id', selectedz.json.barang_id));
        //console.log(brg.get('grup_id'));
        var form = new jun.BomWin({
            modez: this.actEdit ? 1 : 2,
            id: bom_id,
            // grup_id: brg.get('grup_id'),
            //storeBarang: this.storeBarang[brg.get('grup_id')],
            storeBarang: jun.rztBarangLib,
            title: (this.actEdit ? "Ubah" : "View") + " BOM",
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.storeBarang.load();
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        // form.satuan.setText(brg.get('sat') + " @ " + brg.get('qty_per_pot') + " gr");
        form.griddetil.store.baseParams = {bom_id: bom_id};
        form.griddetil.store.load();
        form.griddetil.store.baseParams = {};
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data BOM yang akan dihapus.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data BOM ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'bom/delete/id/' + record.json.bom_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBom.reload();
                if (jun.rztBomLib.getTotalCount() !== 0) jun.rztBomLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
