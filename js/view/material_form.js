jun.MaterialWin = Ext.extend(Ext.Window, {
    title: 'Material',
    modez: 1,
    width: 425,
    height: 290,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Material',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Material',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_material',
                        ref: '../kode_material',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Material',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_material',
                        id: 'nama_materialid',
                        ref: '../nama_material',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Note',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket',
                        id: 'ketid',
                        ref: '../ket',
                        maxLength: 255,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Satuan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'sat',
                        id: 'satid',
                        ref: '../sat',
                        maxLength: 10,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: false,
                        fieldLabel: 'Tipe Material',
                        store: jun.rztTipeMaterialCmp,
                        ref: '../tipe',
                        hiddenName: 'tipe_material_id',
                        valueField: 'tipe_material_id',
                        displayField: 'nama_tipe_material',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Stok Minimal',
                        name: 'stok_min',
                        id: 'stok_minid',
                        ref: '../stok_min',
                        maxLength: 30,
                        value: 0
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: true,
                        fieldLabel: 'Merk',
                        ref: '../merk',
                        store: jun.rztMerkCmp,
                        hiddenName: 'merk_id',
                        valueField: 'merk_id',
                        displayField: 'nama_',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Harga Rata-rata',
                        name: 'avg',
                        ref: '../avg',
                        maxLength: 30
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.MaterialWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.tipe.on('select', this.ontipeSelect, this);
        // this.cmbSatuan.on('select', this.cmbSatuanOnSelect, this);
        // this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    ontipeSelect: function (c, r, i) {
        // var d = jun.rztTipeMaterialCmp.findExact('tipe_material_id', this.tipe.getValue());
        // var z = jun.rztTipeMaterialCmp.getAt(d);
        var z = jun.rztTipeMaterialCmp.getById(this.tipe.getValue());
        this.merk.setDisabled(z.data.jenis != '3');
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    cmbSatuanOnSelect: function (c, r, i) {
        this.dimension.setValue(r.get('dimension'));
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 0) {
            urlz = 'Material/create/';
        } else if (this.modez == 1) {
            urlz = 'Material/update/id/' + this.id;
        }
        Ext.getCmp('form-Material').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztMaterial.reload();
                jun.rztMaterialLib.reload();
                jun.rztMaterialCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Material').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});