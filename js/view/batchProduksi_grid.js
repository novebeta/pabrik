jun.BatchProduksiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Batch (Produksi)",
    id: 'docs-jun.BatchProduksiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nomor Batch',
            sortable: true,
            resizable: true,
            dataIndex: 'no_batch',
            width: 60
        },
        {
            header: 'Kode Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'bom_id',
            renderer: jun.renderBomKodeBarang,
            width: 60
        },
        {
            header: 'Nama Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'bom_id',
            renderer: jun.renderBomNamaBarang,
            width: 150
        },
        {
            header: 'Kode Formula',
            sortable: true,
            resizable: true,
            dataIndex: 'bom_id',
            renderer: jun.renderBomKodeFormula,
            width: 60
        },
        {
            header: 'Quantity',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            align: "right",
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                return value >= 100000 ? value / 100000 : value >= 1000 ? value / 1000 : value;
            },
            width: 40
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                return value >= 100000 ? 'Quintal' : value >= 1000 ? 'Kilogram' : 'gram';
            },
            width: 30
        },
        {
            header: 'Qty Hasil',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_hasil',
            align: "right",
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                if (value > 0) metaData.style = "background-color: #CCFFCC; text-align: right;";
                return value >= 100000 ? value / 100000 : value >= 1000 ? value / 1000 : value;
            },
            width: 40
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_hasil',
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                return value >= 100000 ? 'Quintal' : value >= 1000 ? 'Kilogram' : 'gram';
            },
            width: 30
        },
        {
            header: 'Tanggal Expired',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_exp',
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                var myDate = new Date(value);
                return myDate.format('d M Y');
            },
            width: 50
        },
        {
            header: 'Tanggal Transfer',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_transfer_gudang',
            renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
                var myDate = new Date(value);
                return value !== null ? myDate.format('d M Y') : '';
            },
            width: 50
        }
    ],
    initComponent: function () {
        jun.rztBomLib.load();
        jun.rztBarangLib.load();
        jun.rztBomVsBarang.load();
        this.store = jun.rztBatchProduksi;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Konsumsi Raw Material',
                    ref: '../btnKonsumsiRaw'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Konsumsi Packaging',
                    ref: '../btnKonsumsiPack'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Input Barang jadi',
                    ref: '../btnInputProduk'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Transfer Gudang',
                    ref: '../btnTransferGudang'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    text: 'Tanggal Produksi :',
                    ref: '../label1'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglbatchProduksigridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.BatchProduksiGrid.superclass.initComponent.call(this);
        this.btnKonsumsiRaw.on('Click', this.loadFormRaw, this);
        this.btnKonsumsiPack.on('Click', this.loadFormPack, this);
        this.btnInputProduk.on('Click', this.loadFormProduk, this);
        this.btnTransferGudang.on('Click', this.loadFormTransferGudang, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', function () {
            this.store.reload()
        }, this);
        jun.rztBatchProduksi.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        mode: "grid",
                        'tgl': Ext.getCmp('tglbatchProduksigridid').getValue()
                    }
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadFormRaw: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Batch");
            return;
        }
        var form = new jun.BatchKonsumsiRawWin({
            'batch_id': selectedz.json.batch_id,
            'bom_id': selectedz.json.bom_id
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        var tgl = new Date(this.record.json.tgl);
        form.formz.getComponent('tgl_produksi').setText(': ' + tgl.formatDate());
        form.formz.getComponent('no_batch').setText(': ' + this.record.json.no_batch);
        form.formz.getComponent('kode_produk').setText(': ' + jun.renderBomKodeBarang(this.record.json.bom_id));
        form.formz.getComponent('nama_produk').setText(': ' + jun.renderBomNamaBarang(this.record.json.bom_id));
        form.formz.getComponent('kode_formula').setText(': ' + jun.renderBomKodeFormula(this.record.json.bom_id));
        form.formz.getComponent('qty').setText(': ' + formatNumber(this.record.json.qty) + ' gram');
    },
    loadFormPack: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Batch");
            return;
        }
        var form = new jun.BatchKonsumsiPackWin({
            'batch_id': selectedz.json.batch_id,
            'bom_id': selectedz.json.bom_id
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        var tgl = new Date(this.record.json.tgl);
        form.formz.getComponent('tgl_produksi').setText(': ' + tgl.formatDate());
        form.formz.getComponent('no_batch').setText(': ' + this.record.json.no_batch);
        form.formz.getComponent('kode_produk').setText(': ' + jun.renderBomKodeBarang(this.record.json.bom_id));
        form.formz.getComponent('nama_produk').setText(': ' + jun.renderBomNamaBarang(this.record.json.bom_id));
        form.formz.getComponent('kode_formula').setText(': ' + jun.renderBomKodeFormula(this.record.json.bom_id));
        form.formz.getComponent('qty').setText(': ' + formatNumber(this.record.json.qty) + ' gram');
    },
    loadFormProduk: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Batch");
            return;
        }
        var form = new jun.BatchInputProdukWin({batch_id: selectedz.json.batch_id});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        var tgl = new Date(this.record.json.tgl);
        form.formz.getComponent('tgl_produksi').setText(': ' + tgl.formatDate());
        form.formz.getComponent('no_batch').setText(': ' + this.record.json.no_batch);
        form.formz.getComponent('kode_produk').setText(': ' + jun.renderBomKodeBarang(this.record.json.bom_id));
        form.formz.getComponent('nama_produk').setText(': ' + jun.renderBomNamaBarang(this.record.json.bom_id));
        form.formz.getComponent('kode_formula').setText(': ' + jun.renderBomKodeFormula(this.record.json.bom_id));
        form.formz.getComponent('qty').setText(': ' + formatNumber(this.record.json.qty) + ' gram');
    },
    loadFormTransferGudang: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Batch");
            return;
        } else if (Number(selectedz.json.qty_hasil) <= 0) {
            Ext.MessageBox.alert("Warning", "Batch belum memiliki barang jadi.");
            return;
        }
        var form = new jun.BatchTransferGudangWin({batch_id: selectedz.json.batch_id});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        var tgl = new Date(this.record.json.tgl);
        form.formz.getComponent('tgl_produksi').setText(': ' + tgl.formatDate());
        form.formz.getComponent('no_batch').setText(': ' + this.record.json.no_batch);
        form.formz.getComponent('kode_produk').setText(': ' + jun.renderBomKodeBarang(this.record.json.bom_id));
        form.formz.getComponent('nama_produk').setText(': ' + jun.renderBomNamaBarang(this.record.json.bom_id));
        form.formz.getComponent('kode_formula').setText(': ' + jun.renderBomKodeFormula(this.record.json.bom_id));
        form.formz.getComponent('qty').setText(': ' + formatNumber(this.record.json.qty) + ' gram');
        form.formz.getComponent('qty_hasil').setText(': ' + formatNumber(this.record.json.qty_hasil) + ' gram');
    }
});
Date.prototype.formatDate = function () {
    return this.toUTCString().substring(5, 16);
};
function formatNumber(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}