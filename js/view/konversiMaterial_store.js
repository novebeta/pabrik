jun.KonversiMaterialstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KonversiMaterialstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KonversiMaterialStoreId',
            url: 'KonversiMaterial',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'konversi_material_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'note_'}
            ]
        }, cfg));
    }
});
jun.rztKonversiMaterial = new jun.KonversiMaterialstore();
//jun.rztKonversiMaterial.load();
