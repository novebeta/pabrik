jun.TandaTerimaMaterialGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Tanda Terima Material",
    id: 'docs-jun.TandaTerimaMaterialGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref.',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tanda Terima',
            sortable: true,
            resizable: true,
            dataIndex: 'tanda_terima',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    // userPermission: function(actEdit){
    //     this.actEdit = true;
    //     this.actView = true;
    //     this.viewAllData = (this.actCreatePO || this.actReject);
    // },
    userPermission: function (actEdit) {
        this.actEdit = true;
        this.actView = true;
        //this.viewAllData = (this.actCreatePO || this.actReject);
    },
    initComponent: function () {
        jun.rztTandaTerimaMaterial.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglterimagridid').getValue()
                    };
                }
            }
        });
        this.store = jun.rztTandaTerimaMaterial;
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) jun.rztStorageLocationCmp.load();
        this.bbar = {items: [{xtype: 'paging', store: this.store, displayInfo: true, pageSize: 20}]};
        this.userPermission(0);
        // switch(UROLE){
        //     case USER_ADMIN: this.userPermission(1); break;
        //     case USER_PPIC: this.userPermission(1); break;
        //     case USER_GA: this.userPermission(1); break;
        //     case USER_PURCHASING: this.userPermission(0); break;
        //     case USER_PEMBELIAN: this.userPermission(0); break;
        //     default: this.userPermission(0);
        // }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglterimagridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.TandaTerimaMaterialGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.tgl.on('select', function () {
            this.store.load();
        }, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadEditForm: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Tanda Terima.");
            return;
        }
        var r = this.sm.getSelected().json;
        var filter = this.actEdit;
        var form = new jun.TandaTerimaWin({
            modez: ( filter ? 1 : 2),
            id: r.tandaterima_id,
            storeDetails: jun.rztTandaTerimaMaterialDetails,
            title: ( filter ? "Edit" : "View") + " Tanda Terima Material",
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTandaTerimaMaterialDetails.baseParams = {tandaterima_id: r.tandaterima_id};
        jun.rztTandaTerimaMaterialDetails.load();
        jun.rztTandaTerimaMaterialDetails.baseParams = {};
        if (filter) {
            form.griddetils.material.store.baseParams = {return_pembelian_id: r.return_pembelian_id};
            form.griddetils.material.store.load();
            form.griddetils.material.store.baseParams = {};
        }
    }
});
