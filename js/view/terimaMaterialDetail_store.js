jun.TerimaMaterialDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TerimaMaterialDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaMaterialDetailsStoreId',
            url: 'TerimaMaterialDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'terima_material_detail_id'},
                {name: 'po_id'},
                {name: 'no_po'}, //tidak disimpan di database, hanya data sementara
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'sat'},
                {name: 'price', type: 'float'},
                {name: 'tgl_expired', type: 'date'},
                {name: 'note'},
                {name: 'no_lot'},
                {name: 'no_qc'},
                {name: 'note_inv'},
                {name: 'terima_material_id'}
            ]
        }, cfg));
    },
    refreshData: function () {
        var total = 0;
        this.each(function (r) {
            total += jun.konversiSatuanDitampilkan(r.get('qty'), r.get('sat')) * r.get('price');
        });
        var persen_dpp = parseFloat(Ext.getCmp('form-InvoiceSupplier-persen_dpp').getValue());
        var disc = parseFloat(Ext.getCmp('form-InvoiceSupplier-discount').getValue());
        var pph_persen = parseFloat(Ext.getCmp('form-InvoiceSupplier-pph_persen').getValue());
        var persen_tax = parseFloat(Ext.getCmp('form-InvoiceSupplier-persen_tax').getValue());
        var dpp = (total - disc) * persen_dpp / 100;
        var tax = dpp * persen_tax / 100;
        var pph = dpp * pph_persen;
        Ext.getCmp('form-InvoiceSupplier-total').setValue(total);
        Ext.getCmp('form-InvoiceSupplier-dpp').setValue(dpp);
        Ext.getCmp('form-InvoiceSupplier-tax').setValue(tax);
        Ext.getCmp('form-InvoiceSupplier-pph').setValue(pph);
        Ext.getCmp('form-InvoiceSupplier-grand_total').setValue(dpp + tax - pph);
    }
});
jun.rztTerimaMaterialDetails = new jun.TerimaMaterialDetailsstore();
jun.rztInvoiceSupplierDetails = new jun.TerimaMaterialDetailsstore();
jun.rztInvoiceSupplierDetailsForDP = new jun.TerimaMaterialDetailsstore({url: 'PurchaseOrderDetails/GetItems'});