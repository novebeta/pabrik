jun.CreditNoteDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CreditNoteDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CreditNoteDetailsStoreId',
            url: 'CreditNoteDetails',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'credit_note_details_id'},
                {name: 'qty'},
                {name: 'price'},
                {name: 'total', type: 'float'},
                {name: 'barang_id'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'ket'},
                {name: 'credit_note_id'}
            ]
        }, cfg));
    },
    refreshData: function () {
        var total = this.sum("total");
        Ext.getCmp('total_credite_note_id').setValue(total);
    }
});
jun.rztCreditNoteDetails = new jun.CreditNoteDetailsstore();
//jun.rztCreditNoteDetails.load();
