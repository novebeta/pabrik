jun.Sjtaxstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Sjtaxstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjtaxStoreId',
            url: 'Sjtax',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sjtax_id'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'customer_id'},
                {name: 'tgl'},
                {name: 'tgl_jatuh_tempo'}
            ]
        }, cfg));
    }
});
jun.rztSjtax = new jun.Sjtaxstore();
//jun.rztSjtax.load();
