jun.ConversiMaterialBarkasStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ConversiMaterialBarkasStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ConversiMaterialBarkasStoreId',
            url: 'ConversiMaterialBarkas',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'conversi_material_barkas_id'},
                {name: 'doc_ref'},
                {name: 'tgl', type: 'date'},
                {name: 'note'},
                {name: 'id_user'},
                {name: 'tdate'}
            ]
        }, cfg));
    }
});
jun.rztConversiMaterialBarkas = new jun.ConversiMaterialBarkasStore();
