jun.Assemblystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Assemblystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssemblyStoreId',
            url: 'Assembly',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'assembly_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'note_'},
                {name: 'material_id'},
                {name: 'folder_id'},
                {name: 'price_std_old'},
                {name: 'price_std_new'},
                {name: 'price'},
                {name: 'qty'},
                {name: 'price_total'},
                {name: 'loc_code'},
                {name: 'void'}
            ]
        }, cfg));
    }
});
jun.rztAssembly = new jun.Assemblystore();
//jun.rztAssembly.load();
