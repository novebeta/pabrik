jun.FolderAssemblystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FolderAssemblystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FolderAssemblyStoreId',
            url: 'Folder/Assembly',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'folder_id'},
                {name: 'type_'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'note_'}
            ]
        }, cfg));
    }
});
jun.rztFolderAssembly = new jun.FolderAssemblystore();
//jun.rztFolderAssembly.load();
