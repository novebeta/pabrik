jun.DepositFakturPajakstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DepositFakturPajakstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DepositFakturPajakStoreId',
            url: 'DepositFakturPajak',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'fp_id'},
                {name: 'fp_no'},
                {name: 'fp_jumlah'},
                {name: 'fp_used'},
                {name: 'tgl'}
            ]
        }, cfg));
    }
});
jun.rztDepositFakturPajak = new jun.DepositFakturPajakstore();
jun.rztDepositFakturPajakLib = new jun.DepositFakturPajakstore();
jun.rztDepositFakturPajakCmp = new jun.DepositFakturPajakstore();
