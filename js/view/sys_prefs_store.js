jun.SysPrefsStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SysPrefsStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SysPrefsStoreId',
            url: 'SysPrefs',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'sys_prefs_id'},
                {name: 'name_'},
                {name: 'value_'}
            ]
        }, cfg));
    }
});
jun.rztSysPrefs = new jun.SysPrefsStore();
jun.recordSysPrefs = function (name_) {
    return jun.rztSysPrefs.getAt(jun.rztSysPrefs.findExact('name_', name_)).get('value_');
}