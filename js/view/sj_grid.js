jun.SjGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Surat Jalan",
    id: 'docs-jun.SjGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_sj',
            width: 100
        },
        {
            header: 'Penjualan',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_sales',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_sj',
            width: 100
        },
        {
            header: 'Kode Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_customer',
            width: 100
        },
        {
            header: 'Nama Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztSlsSjInvHeader.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglsjgridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        // this.store = jun.rztSj;
        this.store = jun.rztSlsSjInvHeader;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Edit Surat Jalan',
                    ref: '../btnEdit'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     id: 'tglsjgridid',
                //     ref: '../tgl'
                // },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Surat Jalan',
                    ref: '../btnPrintSJ'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Create Sales Invoice',
                    ref: '../btnAddSalesInvoice'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglsjgridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportSuratJalan",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "type",
                            ref: "../../type_report"
                        },
                        {
                            xtype: "hidden",
                            name: "sj_id",
                            ref: "../../sj_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        jun.SjGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnAddExport.on('Click', this.loadForm, this);
        // this.btnAddEkspedisi.on('Click', this.loadForm, this);
        // this.btnAddBarangRongsokan.on('Click', this.loadForm, this);
        this.btnAddSalesInvoice.on('Click', this.createSalesInvoice, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrintSJ.on('Click', this.printSj, this);
        this.tgl.on('select', this.selectTgl, this);
//        this.btnPrintSJInt.on('Click', this.printSj, this);
        //this.btnPrintSJTax.on('Click', this.printSj, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    createSalesInvoice: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Delivery");
            return;
        }
        if (selectedz.json.status_sj != '0') {
            Ext.MessageBox.alert("Warning", "Sales Invoice sudah dibuat.");
            return;
        }
        var idz = selectedz.json.sj_id;
        var form = new jun.SalesInvoiceWin({modez: 0, id: idz});
        form.show(this);
        this.record.data.tgl_inv = DATE_NOW;//Date.parseDate(this.record.data.tgl, 'Y-m-d');
        this.record.data.status = 1;
        this.record.data.tgl = this.record.data.tgl_sj;
        //this.record.data.tgl_jatuh_tempo = this.record.data.tgl_inv.add(Date.DAY, parseInt(this.record.data.termofpayment));
        form.formz.getForm().loadRecord(this.record);
        jun.rztSjDetails.baseParams = {
            sj_id: idz,
            termofpayment: this.record.data.termofpayment
        };
        jun.rztSjDetails.load();
        jun.rztSjDetails.baseParams = {};
    },
    printSj: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Surat jalan");
            return;
        }
        // var format_SJ;
        // switch (parseInt(selectedz.json.tipe_barang)) {
        //     case BRG_PRODUK_LOKAL:
        //         format_SJ = "SJ_produk_lokal";
        //         break;
        //     case BRG_PRODUK_EXPORT:
        //         format_SJ = "SJ_produk_export";
        //         break;
        //     case BRG_EXPEDISI:
        //         format_SJ = "SJ_expedisi";
        //         break;
        //     case BRG_BARANGRONGSOKAN:
        //         format_SJ = "SJ_barang_rongsokan";
        //         break;
        //     default:
        //         Ext.MessageBox.alert("Warning", "Data yang dipilih tidak memiliki print out Surat Jalan.");
        //         return;
        // }


        // Ext.getCmp("form-ReportSuratJalan").getForm().standardSubmit = !0;
        // Ext.getCmp("form-ReportSuratJalan").getForm().url = "Report/PrintSuratJalan";
        // this.type_report.setValue('SJ_produk_lokal');
        // this.sj_id.setValue(selectedz.json.sj_id);
        // var form = Ext.getCmp('form-ReportSuratJalan').getForm();
        // var el = form.getEl().dom;
        // var target = document.createAttribute("target");
        // target.nodeValue = "_blank";
        // el.setAttributeNode(target);
        // el.action = form.url;
        // el.submit();


        Ext.Ajax.request({
            url: 'Report/PrintSuratJalan',
            method: 'POST',
            scope: this,
            params: {
                sj_id: selectedz.json.sj_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (notReady()) {
                    return;
                }
                var msg = [{type: 'raw', data: Base64.decode(response.msg)}];
                // var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                print(PRINTER_RECEIPT, msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Surat jalan");
            return;
        }
        var idz = selectedz.json.sj_id;
        var form = new jun.SjWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSjDetails.baseParams = {sj_id: idz};
        jun.rztSjDetails.load();
        jun.rztSjDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Sj/delete/id/' + record.json.sj_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSj.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
