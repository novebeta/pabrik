jun.SupplierPaymentDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SupplierPaymentDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SupplierPaymentDetilStoreId',
            url: 'SupplierPaymentDetil',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'supplier_payment_detil_id'},
                {name: 'kas_dibayar', type: 'float'},
                {name: 'no_faktur'},
                {name: 'sisa', type: 'float'},
                {name: 'payment_tipe'},
                {name: 'reference_item_id'},
                {name: 'supplier_payment_id'},
                {name: 'doc_ref'},
                {name: 'tgl'}
            ]
        }, cfg));
    },
    refreshData: function (a) {
        var subtotal = this.sum("kas_dibayar");
        Ext.getCmp("total_supplier_payment_id").setValue(subtotal);
    }
});
jun.rztSupplierPaymentDetil = new jun.SupplierPaymentDetilstore();
jun.rztSupplierPaymentDetilCmp = new jun.SupplierPaymentDetilstore();
//jun.rztSupplierPaymentDetil.load();
