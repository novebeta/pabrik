jun.Rongsokanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Rongsokanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RongsokanStoreId',
            url: 'Rongsokan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'sat'},
                {name: 'grup_id'},
                {name: 'qty_per_box'},
                {name: 'qty_per_pot'},
                {name: 'kode_barang_2'},
                {name: 'kode_barang_rnd'},
                {name: 'tipe_barang_id'}
            ]
        }, cfg));
    }
});
jun.rztRongsokan = new jun.Rongsokanstore();
//jun.rztRongsokan.load();
