jun.CustomersGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Customers",
    id: 'docs-jun.CustomersGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_customer',
            width: 70
        },
        {
            header: 'Nama Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'address',
            width: 120
        },
        {
            header: 'NPWP',
            sortable: true,
            resizable: true,
            dataIndex: 'npwp',
            width: 70
        }
        /*
         {
         header:'wil_tax_id',
         sortable:true,
         resizable:true,
         dataIndex:'wil_tax_id',
         width:100
         },
         */
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        this.store = jun.rztCustomers;
        if (jun.rztSalesmanCmp.getTotalCount() === 0) {
            jun.rztSalesmanCmp.load();
        }
        if (jun.rztWilIntCmp.getTotalCount() === 0) jun.rztWilIntCmp.load();
        if (jun.rztWilTaxCmp.getTotalCount() === 0) jun.rztWilTaxCmp.load();
        this.userPermission(1, 1, 1);
        // switch (UROLE) {
        //     case USER_ADMIN:
        //         this.userPermission(1, 1, 1);
        //         break;
        //     case USER_PPIC:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_GA:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_PURCHASING:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_PEMBELIAN:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     case USER_RND:
        //         this.userPermission(0, 0, 0);
        //         break;
        //     default:
        //         this.userPermission(0, 0, 0);
        // }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: this.actDelete ? false : true
                },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Kode Customer</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'kode_customer',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Kode Customer',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_customer';
                                        Ext.getCmp('docs-jun.CustomersGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Nama Customer',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'nama_customer';
                                        Ext.getCmp('docs-jun.CustomersGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Alamat',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'address';
                                        Ext.getCmp('docs-jun.CustomersGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'NPWP',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'npwp';
                                        Ext.getCmp('docs-jun.CustomersGrid').txtFind.focus().setValue("");
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.CustomersGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.fieldsearch = this.btnFind.fieldsearch;
            this.valuesearch = c.getValue();
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CustomersWin({
            modez: 0,
            title: "Buat Data Customer Baru",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersWin({
            modez: this.actEdit ? 1 : 2,
            id: idz,
            title: (this.actEdit ? "Ubah" : "Lihat") + " Data Customer",
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Customer");
            return;
        }
        console.log(record);
        Ext.MessageBox.confirm(
            'Hapus Data Customer',
            "Apakah anda yakin ingin menghapus data ini?<br><br>Kode customer : " + record.json.kode_customer + "<br>Nama customer : " + record.json.nama_customer,
            this.deleteRecYes,
            this
        );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Customer");
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/delete/id/' + record.json.customer_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztCustomers.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
