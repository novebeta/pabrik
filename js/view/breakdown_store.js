jun.Breakdownstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Breakdownstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BreakdownStoreId',
            url: 'Breakdown',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'breakdown_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'note_'},
                {name: 'qty'},
                {name: 'price'},
                {name: 'material_id'},
                {name: 'folder_id'},
                {name: 'total'},
                {name: 'total_qty'},
                {name: 'avg_price'},
                {name: 'loc_code'},
                {name: 'void'}
            ]
        }, cfg));
    }
});
jun.rztBreakdown = new jun.Breakdownstore();
//jun.rztBreakdown.load();
