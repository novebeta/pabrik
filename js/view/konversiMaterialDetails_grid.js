jun.KonversiMaterialDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "KonversiMaterialDetails",
    id: 'docs-jun.KonversiMaterialDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Material Lama',
            sortable: true,
            resizable: true,
            dataIndex: 'dari_material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Qty Lama',
            sortable: true,
            resizable: true,
            dataIndex: 'dari_qty',
            width: 100
        },
        {
            header: 'Kode Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code_old',
            width: 100
        },
        {
            header: 'Material Baru',
            sortable: true,
            resizable: true,
            dataIndex: 'ke_material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Qty Baru',
            sortable: true,
            resizable: true,
            dataIndex: 'ke_qty',
            width: 100
        },
        {
            header: 'Kode Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code_new',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztKonversiMaterialDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 7,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Material Lama :'
                        },
                        {
                            xtype: 'mfcombobox',
                            style: 'margin-bottom:2px',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama Material</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'),
                            width: 175,
                            allowBlank: false,
                            enableKeyEvents: true,
                            listWidth: 400,
                            ref: '../../item_old',
                            displayField: 'kode_material'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty_old',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Gudang :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'loc_code',
                            store: jun.rztStorageLocationCmp,
                            ref: '../../loc_code_old',
                            valueField: 'loc_code',
                            displayField: 'loc_code',
                            width: 100
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stock_old'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Material Baru :'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama Material</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'),
                            width: 175,
                            allowBlank: false,
                            enableKeyEvents: true,
                            listWidth: 400,
                            ref: '../../item_new',
                            displayField: 'kode_material'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty_new',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Gudang :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'loc_code',
                            store: jun.rztStorageLocationCmp,
                            ref: '../../loc_code_new',
                            valueField: 'loc_code',
                            displayField: 'loc_code',
                            width: 100
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stock_new'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.KonversiMaterialDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.item_old.on('select', this.showStockOld, this);
        this.loc_code_old.on('select', this.showStockOld, this);
        this.item_new.on('select', this.showStockNew, this);
        this.loc_code_new.on('select', this.showStockNew, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    showStockOld: function () {
        var material_id = this.item_old.getValue();
        var loc_code = this.loc_code_old.getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stock_old);
        }
    },
    showStockNew: function () {
        var material_id = this.item_new.getValue();
        var loc_code = this.loc_code_new.getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stock_new);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var old_material_id = this.item_old.getValue();
        var old_qty = parseFloat(this.qty_old.getValue());
        var loc_code_old = this.loc_code_old.getValue();
        var new_material_id = this.item_new.getValue();
        var new_qty = parseFloat(this.qty_new.getValue());
        var loc_code_new = this.loc_code_new.getValue();
        if (loc_code_old == "" || loc_code_old == undefined) {
            Ext.MessageBox.alert("Error", "Gudang harus didefinisikan.");
            return;
        }
        if (loc_code_new == "" || loc_code_new == undefined) {
            Ext.MessageBox.alert("Error", "Gudang harus didefinisikan.");
            return;
        }
        if (old_material_id == "" || old_material_id == undefined ||
            new_material_id == "" || new_material_id == undefined) {
            Ext.MessageBox.alert("Error", "Material harus disefinisikan.");
            return;
        }
        if (old_qty <= 0 || new_qty <= 0) {
            Ext.MessageBox.alert("Error", "Qty material harus lebih dari nol.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('dari_material_id', old_material_id);
            record.set('dari_qty', old_qty);
            record.set('ke_material_id', new_material_id);
            record.set('ke_qty', new_qty);
            record.set('loc_code_old', loc_code_old);
            record.set('loc_code_new', loc_code_new);
            record.commit();
        } else {
            var c = jun.rztKonversiMaterialDetails.recordType,
                d = new c({
                    dari_material_id: old_material_id,
                    dari_qty: old_qty,
                    ke_material_id: new_material_id,
                    ke_qty: new_qty,
                    loc_code_old: loc_code_old,
                    loc_code_new: loc_code_new
                });
            jun.rztKonversiMaterialDetails.add(d);
        }
        this.item_old.reset();
        this.qty_old.reset();
        this.item_new.reset();
        this.qty_new.reset();
        this.loc_code_old.reset();
        this.loc_code_new.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.item_old.setValue(record.data.dari_material_id);
            this.qty_old.setValue(record.data.dari_qty);
            this.item_new.setValue(record.data.ke_material_id);
            this.qty_new.setValue(record.data.ke_qty);
            this.loc_code_old.setValue(record.data.loc_code_old);
            this.loc_code_new.setValue(record.data.loc_code_new);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
