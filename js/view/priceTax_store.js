jun.PriceTaxstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PriceTaxstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PriceTaxStoreId',
            url: 'PriceTax',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'price_tax_id'},
                {name: 'amount'},
                {name: 'wil_tax_id'},
                {name: 'barang_id'}
            ]
        }, cfg));
    }
});
jun.rztPriceTax = new jun.PriceTaxstore();
//jun.rztPriceTax.load();
