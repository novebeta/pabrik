jun.rztCurrencyStore = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'des'
    ],
    data: [
        ['IDR', 'Indonesian Rupiah'],
        ['USD', 'United States Dollar'],
        ['EUR', 'European Euro'],
        ['JPY', 'Japanese Yen']
    ]
});