jun.TerimaBarangDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TerimaBarangDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaBarangDetailsStoreId',
            url: 'TerimaBarangDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'terima_barang_details_id'},
                {name: 'qty', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'disc_rp', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'barang_id'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'ket'},
                {name: 'terima_barang_id'}
            ]
        }, cfg));
    }
});
jun.rztTerimaBarangDetails = new jun.TerimaBarangDetailsstore();
//jun.rztTerimaBarangDetails.load();
