jun.FinAccReportStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FinAccReportStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FinAccReportStoreId',
            url: 'FinAccReport',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'fin_acc_report_id'},
                {name: 'start_date', type: 'date'},
                {name: 'end_date', type: 'date'},
                {name: 'biaya_tenagakerja_tidak_langsung', type: 'float'},
                {name: 'biaya_listrik', type: 'float'},
                {name: 'biaya_penyusutan_mesin', type: 'float'},
                {name: 'id_user'},
                {name: 'tdate', type: 'date'}
            ]
        }, cfg));
    }
});
jun.rztFinAccReport = new jun.FinAccReportStore();
//jun.rztMaterialForBOM = new jun.FinAccReportStore({baseParams: {materialforbom: 1}});