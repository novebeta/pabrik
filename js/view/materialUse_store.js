jun.MaterialUsestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MaterialUsestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MaterialUseStoreId',
            url: 'MaterialUse',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_use_id'},
                {name: 'service_id'},
                {name: 'sat'},
                {name: 'jml'},
                {name: 'nominal'},
                {name: 'hpp'},
                {name: 'material_id'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
jun.rztMaterialUse = new jun.MaterialUsestore();
//jun.rztMaterialUse.load();
