jun.NotaReturnDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Nota Return Details",
    id: 'docs-jun.NotaReturnDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 250,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 70,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        // {
        //     header: 'Satuan',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'sat',
        //     width: 50
        // },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc (Rp)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc_rp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'VAT (Rp)',
            sortable: true,
            resizable: true,
            dataIndex: 'vatrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
            // renderer : function(value, metaData, record, rowIndex){
            //     var qty = record.get('qty');
            //     var total = Number(qty)*Number(record.get('price'));
            //     return Ext.util.Format.number(total, '0,0');
            // }
        }
    ],
    initComponent: function () {
        this.store = jun.rztNotaReturnDetails;
        // this.tbar = {
        //     xtype: 'toolbar',
        //     items: [
        //         {
        //             xtype: 'buttongroup',
        //             columns: 4,
        //             defaults: {
        //                 scale: 'small'
        //             },
        //             items: [
        //                 {
        //                     xtype: 'label',
        //                     style: 'margin:5px',
        //                     text: 'Kode Material :'
        //                 },
        //                 {
        //                     xtype: 'combo',
        //                     style: 'margin-bottom:2px',
        //                     typeAhead: true,
        //                     triggerAction: 'all',
        //                     lazyRender: true,
        //                     mode: 'local',
        //                     forceSelection: true,
        //                     store: new jun.Materialstore(),
        //                     hiddenName: 'material_id',
        //                     valueField: 'material_id',
        //                     itemSelector: "div.search-item",
        //                     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
        //                         '<div style="width:100%;display:inline-block;"><span style="font-weight: bold">{kode_material}</span><br>{nama_material}</div>',
        //                         "</div></tpl>"),
        //                     ref: '../../material',
        //                     displayField: 'kode_material'
        //                 },
        //                 {
        //                     xtype: 'label',
        //                     style: 'margin:5px',
        //                     text: 'Qty :'
        //                 },
        //                 {
        //                     xtype: 'numericfield',
        //                     ref: '../../qty',
        //                     style: 'margin-bottom:2px',
        //                     width: 70,
        //                     value: 1,
        //                     minValue: 0
        //                 },
        //                 {
        //                     xtype: 'label',
        //                     style: 'margin:5px',
        //                     text: 'Note :'
        //                 },
        //                 {
        //                     xtype: 'textfield',
        //                     ref: '../../note',
        //                     width: 400,
        //                     colspan: 3,
        //                     maxLength: 50
        //                 }
        //             ]
        //         },
        //         {
        //             xtype: 'buttongroup',
        //             columns: 3,
        //             id: 'btnsalesdetilid',
        //             defaults: {
        //                 scale: 'large',
        //                 width: 30
        //             },
        //             items: [
        //                 {
        //                     xtype: 'button',
        //                     text: 'Add',
        //                     height: 44,
        //                     ref: '../../btnAdd'
        //                 },
        //                 {
        //                     xtype: 'button',
        //                     text: 'Edit',
        //                     height: 44,
        //                     ref: '../../btnEdit'
        //                 },
        //                 {
        //                     xtype: 'button',
        //                     text: 'Del',
        //                     height: 44,
        //                     ref: '../../btnDelete'
        //                 }
        //             ]
        //         }
        //     ]
        // };
        jun.NotaReturnDetailsGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        //this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        //this.material.on('select', this.onSelectBarang, this);
        //this.getSelectionModel().on('rowselect', this.getrow, this);
    }//,
//    onSelectBarang: function(c,r,i){
//        this.qty.reset();
//        this.sat.setValue(r.get('sat'));
//        this.note.reset();
//    },
//    btnDisable: function (s) {
//        this.btnAdd.setDisabled(s);
//        this.btnDelete.setDisabled(s);
//        if (s) {
//            this.sm.lock();
//        } else {
//            this.sm.unlock();
//        }
//    },
//    getrow: function (sm, idx, r) {
//        this.record = r;
//        var selectedz = this.sm.getSelections();
//    },
//    loadForm: function () {
//        var material_id = this.material.getValue();
//        if (material_id == "") {
//            Ext.MessageBox.alert("Error", "Item must selected");
//            return
//        }
////      kunci material sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = this.store.findExact("material_id", material_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
//                return
//            }
//        }
//        var qty = parseFloat(this.qty.getValue());
//        var sat = this.sat.getValue();
//        var note = this.note.getValue();
//        if (this.btnEdit.text == 'Save') {
//            var record = this.sm.getSelected();
//            record.set('material_id', material_id);
//            record.set('qty', qty);
//            record.set('sat', sat);
//            record.set('note_return', note);
//            record.commit();
//        } else {
//            var c = this.store.recordType,
//                d = new c({
//                    material_id: material_id,
//                    qty: qty,
//                    sat: sat,
//                    note_return: note
//                });
//            this.store.add(d);
//        }
//        this.material.reset();
//        this.qty.reset();
//        this.sat.reset();
//        this.note.reset();
//    },
//    loadEditForm: function (btn) {
//        var record = this.sm.getSelected();
//        if (record == undefined) {
//            Ext.MessageBox.alert("Warning", "You have not selected an item");
//            return;
//        }
//        if (btn.text == 'Edit') {
//            this.material.setValue(record.data.material_id);
//            this.qty.setValue(record.data.qty);
//            this.sat.setValue(record.data.sat);
//            this.note.setValue(record.data.note_return);
//            btn.setText("Save");
//            this.btnDisable(true);
//        } else {
//            this.loadForm();
//            btn.setText("Edit");
//            this.btnDisable(false);
//        }
//    },
//    deleteRec: function () {
//        var record = this.sm.getSelected();
//        // Check is list selected
//        if (record == undefined) {
//            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
//            return;
//        }
//        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
//    },
//    deleteRecYes: function (btn) {
//        if (btn == 'no') {
//            return;
//        }
//        var record = this.sm.getSelected();
//        // Check is list selected
//        if (record == undefined) {
//            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
//            return;
//        }
//        this.store.remove(record);
//    }
});
