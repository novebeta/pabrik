jun.KasDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KasDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KasDetailStoreId',
            url: 'KasDetail',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kas_detail_id'},
                {name: 'item_name'},
                {name: 'total', type: 'float'},
                {name: 'kas_id'},
                {name: 'account_code'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        Ext.getCmp('totalkasid').setValue(this.sum("total"));
    }
});
jun.rztKasDetail = new jun.KasDetailstore();
//jun.rztKasDetail.load();
