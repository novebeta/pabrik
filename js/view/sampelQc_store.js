jun.SampelQcstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SampelQcstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SampelQcStoreId',
            url: 'SampelQc',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sampel_id'},
                {name: 'doc_ref'},
                {name: 'total', type: 'float'},
                {name: 'tgl'},
                {name: 'adt'},
                {name: 'total_tax', type: 'float'},
                {name: 'nama'}
            ]
        }, cfg));
    }
});
jun.rztSampelQc = new jun.SampelQcstore();
