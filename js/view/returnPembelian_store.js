jun.ReturnPembelianStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReturnPembelianStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReturnPembelianId',
            url: 'ReturnPembelian',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'return_pembelian_id'},
                {name: 'terima_barang_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'supplier_id'},
                {name: 'no_sj_supplier'},
                {name: 'no_inv_supplier'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'status'},
                {name: 'nota_return_doc_ref'},
                {name: 'nota_return_tgl'},
                {name: 'nota_return_dpp'},
                {name: 'nota_return_ppn'},
                {name: 'nota_return_ppnbm'},
                {name: 'nota_return_no_faktur_pajak'},
                {name: 'nota_return_tgl_faktur_pajak'},
                {name: 'nota_return_nama_pembeli'},
                {name: 'nota_return_pembeli'},
                {name: 'nota_return_alamat'},
                {name: 'nota_return_npwp'},
                {name: 'nota_return_penjual'},
                {name: 'nota_return_alamat_penjual'},
                {name: 'nota_return_npwp_penjual'},
                {name: 'nota_return_pembayaran_supplier_id'},
                {name: 'nota_return_tdate'},
                {name: 'nota_return_id_user'},
                {name: 'nota_return_status'},
                {name: 'nota_return_total', type: 'float'},
                {name: 'nota_return_discount', type: 'float'},
                {name: 'nota_return_bruto', type: 'float'},
                {name: 'urole', type: 'int'},
                {name: 'loc_code'}
            ]
        }, cfg));
    }
});
jun.rztReturnPembelian = new jun.ReturnPembelianStore();
jun.rztNotaReturn = new jun.ReturnPembelianStore();