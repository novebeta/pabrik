jun.StorageLocationstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StorageLocationstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StorageLocationStoreId',
            url: 'StorageLocation',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'loc_code'},
                {name: 'location_name'},
                {name: 'delivery_address'},
                {name: 'phone'},
                {name: 'phone2'},
                {name: 'fax'},
                {name: 'email'},
                {name: 'contact'},
                {name: 'active'}
            ]
        }, cfg));
    }
});
jun.rztStorageLocation = new jun.StorageLocationstore();
jun.rztStorageLocationCmp = new jun.StorageLocationstore();
jun.rztStorageLocationLib = new jun.StorageLocationstore();
//jun.rztStorageLocation.load();
