jun.ConversiMaterialDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ConversiMaterialDetail",
    id: 'docs-jun.ConversiMaterialDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 150,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 50,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(record.get('qty'), record.get('sat')), "0,0.00");
            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 50
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 150
        }
    ],
    initComponent: function () {
        this.store = jun.rztConversiMaterialDetail;
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 6,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Sparepart :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztMaterialSparepartBekas,
                                hiddenName: 'material_id',
                                valueField: 'material_id',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="width:100%;display:inline-block;"><b>{kode_material}</b><br>{nama_material}</span>',
                                    "</div></tpl>"),
                                allowBlank: false,
                                //listWidth: 300,
                                ref: '../../material',
                                displayField: 'kode_material'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                allowBlank: false,
                                width: 110,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Satuan :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                editable: false,
                                readOnly: true,
                                allowBlank: false,
                                store: jun.rztUnitOfMeasureLib,
                                hiddenName: 'sat',
                                valueField: 'code',
                                displayField: 'code',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="width:100%;display:inline-block;"><b>{code}</b><br>{desc}</span>',
                                    "</div></tpl>"),
                                ref: '../../satuan',
                                width: 60
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Note :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../note',
                                style: 'margin-bottom:2px',
                                width: 460,
                                colspan: 5
                            },
                            {
                                xtype: 'hidden',
                                ref: '../../available_qty'
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        id: 'btnsalesdetilid',
                        defaults: {
                            xtype: 'button',
                            scale: 'large',
                            height: 44,
                            width: 50
                        },
                        items: [
                            {
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.ConversiMaterialDetailGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.material.on('select', this.onSelectMaterial, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    onSelectMaterial: function (c, r, i) {
        //this.satuan.store.reload({ params:{ 'dimension': r.get('dimension') } });
        Ext.Ajax.request({
            url: 'ConversiMaterialBarkas/GetQtySparepart',
            success: function (r) {
                qty = isNaN(r.responseText) ? 0 : parseFloat(r.responseText);
                this.setQty(qty);
            },
            params: {
                material_id: r.get('material_id'),
                sat: r.get('sat')
            },
            scope: this
        });
        this.qty.reset();
        this.satuan.setValue(r.get('sat'));
        this.note.reset();
    },
    setQty: function (qty) {
        this.qty.setValue(qty);
        this.available_qty.setValue(qty);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var material_id = this.material.getValue();
        if (material_id == "" || this.qty.getValue() == 0) {
            Ext.MessageBox.alert("Error", "Kode barang dan quantity harus disi.");
            return false;
        }
        if (this.store.find('material_id', material_id) != -1) {
            if (this.btnEdit.text == 'Save') {
                if (this.sm.getSelected().data.material_id != material_id) {
                    Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                    this.material.reset();
                    this.qty.reset();
                    this.satuan.reset();
                    this.note.reset();
                    return false;
                }
            } else {
                Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                return false;
            }
        }
        var material = jun.getMaterial(material_id);
        var sat = this.satuan.getValue();
        var qty = jun.konversiSatuanDisimpan(parseFloat(this.qty.getValue()), sat);
        var note = this.note.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('qty', qty);
            record.set('sat', sat);
            record.set('note', note);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    material_id: material_id,
                    qty: qty,
                    sat: sat,
                    note: note
                });
            this.store.add(d);
        }
        this.material.reset();
        this.qty.reset();
        this.satuan.reset();
        this.note.reset();
        return true;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            //this.satuan.store.reload({ params:{ 'dimension': jun.renderUnitOfMeasure_dimension(record.data.sat) } });
            this.material.setValue(record.data.material_id);
            this.qty.setValue(jun.konversiSatuanDitampilkan(record.data.qty, record.data.sat));
            this.satuan.setValue(record.data.sat);
            this.note.setValue(record.data.note);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            if (this.loadForm()) {
                btn.setText("Edit");
                this.btnDisable(false);
            }
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        var idz = selectedz.json.po_detail_id;
        var form = new jun.ConversiMaterialDetailWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
jun.ConversiBarkasDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ConversiBarkasDetail",
    id: 'docs-jun.ConversiBarkasDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 150,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 50,
            align: "right",
            renderer: function (value, metaData, record, rowIndex) {
                return Ext.util.Format.number(record.get('qty'), "0,0.00");
            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 50
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 150
        }
    ],
    initComponent: function () {
        this.store = jun.rztConversiBarkasDetail;
        if (!this.readOnly) {
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 6,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Barang :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztBarangBekas,
                                hiddenName: 'barang_id',
                                valueField: 'barang_id',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="width:100%;display:inline-block;"><b>{kode_barang}</b><br>{nama_barang}</span>',
                                    "</div></tpl>"),
                                allowBlank: false,
                                //listWidth: 300,
                                ref: '../../barang',
                                displayField: 'kode_barang'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                allowBlank: false,
                                width: 110,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Satuan :'
                            },
                            {
                                xtype: 'textfield',
                                //readOnly: true,
                                ref: '../../satuan',
                                width: 60
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Note :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../note',
                                style: 'margin-bottom:2px',
                                width: 460,
                                colspan: 5
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        id: 'btnsalesdetilid',
                        defaults: {
                            xtype: 'button',
                            scale: 'large',
                            height: 44,
                            width: 50
                        },
                        items: [
                            {
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.ConversiBarkasDetailGrid.superclass.initComponent.call(this);
        if (!this.readOnly) {
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.barang.on('select', this.onSelectBarang, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    onSelectBarang: function (c, r, i) {
        this.qty.reset();
        this.satuan.setValue(r.get('sat'));
        this.note.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || this.qty.getValue() == 0) {
            Ext.MessageBox.alert("Error", "Kode barang dan quantity harus disi.");
            return false;
        }
        if (this.store.find('barang_id', barang_id) != -1) {
            if (this.btnEdit.text == 'Save') {
                if (this.sm.getSelected().data.barang_id != barang_id) {
                    Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                    this.barang.reset();
                    this.qty.reset();
                    this.satuan.reset();
                    this.note.reset();
                    return false;
                }
            } else {
                Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                return false;
            }
        }
        var sat = this.satuan.getValue();
        var qty = parseFloat(this.qty.getValue());
        var note = this.note.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('sat', sat);
            record.set('note', note);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    sat: sat,
                    note: note
                });
            this.store.add(d);
        }
        this.barang.reset();
        this.qty.reset();
        this.satuan.reset();
        this.note.reset();
        return true;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
            this.satuan.setValue(record.data.sat);
            this.note.setValue(record.data.note);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            if (this.loadForm()) {
                btn.setText("Edit");
                this.btnDisable(false);
            }
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        var idz = selectedz.json.po_detail_id;
        var form = new jun.ConversiBarkasDetailWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});