jun.BarangSalesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangSalesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangSalesStoreId',
            url: 'Barang/Sales',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'sat'},
                {name: 'grup_id'},
                {name: 'qty_per_box'},
                {name: 'qty_per_pot'},
                {name: 'kode_barang_2'},
                {name: 'kode_barang_rnd'},
                {name: 'tipe_barang_id'},
                {name: 'price'},
                {name: 'wil_int_id'}
            ]
        }, cfg));
    }
});
jun.rztBarangSales = new jun.BarangSalesstore();
//jun.rztBarangSales.load();
