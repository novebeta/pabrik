jun.SampelQcGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sampel QC",
    id: 'docs-jun.SampelQcGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztSampelQc.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglsampelgridid').getValue()
                    }
                }
            }
        });
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        this.store = jun.rztSampelQc;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Sampel QC',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Sampel QC',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglsampelgridid',
                    ref: '../tgl'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportSuratJalan",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "type",
                            ref: "../../type_report"
                        },
                        {
                            xtype: "hidden",
                            name: "sampel_id",
                            ref: "../../sampel_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.SampelQcGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', function () {
            this.store.reload();
        }, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SampelQcWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Sampel QC");
            return;
        }
        var idz = selectedz.json.sampel_id;
        var modez = selectedz.json.adt == "0" ? 1 : 2;
        var form = new jun.SampelQcWin({modez: modez, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztSampelQcDetail.baseParams = {
            sampel_id: idz
        };
        jun.rztSampelQcDetail.load();
        jun.rztSampelQcDetail.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data Sampel ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Sampel");
            return;
        }
        Ext.Ajax.request({
            url: 'sampelqc/delete/id/' + record.json.sampel_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSampelQc.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
