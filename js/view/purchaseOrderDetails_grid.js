jun.PurchaseOrderDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PurchaseOrderDetails",
    id: 'docs-jun.PurchaseOrderDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 150,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        // {
        //     header: 'Satuan',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'sat',
        //     width: 50
        // },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.0000")
        },
        {
            header: 'Bruto',
            sortable: true,
            resizable: true,
            dataIndex: 'bruto',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.0000")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 150
        }
    ],
    initComponent: function () {
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 13,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Bahan Baku :'
                        },
                        {
                            xtype: 'mfcombobox',
                            searchFields: [
                                'kode_material',
                                'nama_material'
                            ],
                            mode: 'local',
                            store: jun.rztMaterialCmp,
                            valueField: 'material_id',
                            itemSelector: 'tr.search-item',
                            tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                '<th>Kode</th><th>Nama Material</th></tr></thead>',
                                '<tbody><tpl for="."><tr class="search-item">',
                                '<td>{kode_material}</td><td>{nama_material}</td>',
                                '</tr></tpl></tbody></table>'),
                            allowBlank: false,
                            enableKeyEvents: true,
                            listWidth: 400,
                            width: 100,
                            ref: '../../material',
                            id: 'form-PurchaseOrder_combo_material',
                            displayField: 'kode_material'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Stok : 0',
                            ref: '../../stok'
                        },
                        // {
                        //     xtype: 'combo',
                        //     style: 'margin-bottom:2px',
                        //     typeAhead: true,
                        //     triggerAction: 'all',
                        //     lazyRender: true,
                        //     mode: 'local',
                        //     forceSelection: true,
                        //     store: jun.rztMaterialCmp, //jun.rztMaterialCmp,
                        //     hiddenName: 'material_id',
                        //     valueField: 'material_id',
                        //     itemSelector: "div.search-item",
                        //     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        //         '<span style="width:100%;display:inline-block;"><b>{kode_material}</b><br>{nama_material}</span>',
                        //         "</div></tpl>"),
                        //     allowBlank: false,
                        //     listWidth: 400,
                        //     width: 100,
                        //     ref: '../../material',
                        //     id: 'form-PurchaseOrder_combo_material',
                        //     displayField: 'kode_material'
                        // },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 50,
                            minValue: 0
                        },
                        // {
                        //     xtype: 'combo',
                        //     style: 'margin-bottom:2px',
                        //     typeAhead: true,
                        //     triggerAction: 'all',
                        //     lazyRender: true,
                        //     mode: 'local',
                        //     forceSelection: true,
                        //     editable: false,
                        //     allowBlank: false,
                        //     store: new jun.UnitOfMeasureStore(),
                        //     hiddenName: 'sat',
                        //     valueField: 'code',
                        //     displayField: 'code',
                        //     itemSelector: "div.search-item",
                        //     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        //         '<span style="width:100%;display:inline-block;"><b>{code}</b><br>{desc}</span>',
                        //         "</div></tpl>"),
                        //     ref: '../../satuan',
                        //     width: 60
                        // },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            decimalPrecision: 4,
                            enableKeyEvents: true,
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc (%) :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discid',
                            ref: '../../disc',
                            enableKeyEvents: true,
                            style: 'margin-bottom:2px',
                            width: 40,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc (Rp):'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'disc_rpid',
                            ref: '../../disc_rp',
                            enableKeyEvents: true,
                            decimalPrecision: 4,
                            style: 'margin-bottom:2px',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'VAT :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'vatid',
                            ref: '../../vat',
                            style: 'margin-bottom:2px',
                            width: 40,
                            value: 0,
                            minValue: 0
                        },

                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'noteid',
                            ref: '../../note',
                            style: 'margin-bottom:2px',
                            width: 620,
                            colspan: 12
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        xtype: 'button',
                        scale: 'large',
                        height: 44,
                        width: 50
                    },
                    items: [
                        {
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            text: 'Edit',
                            ref: '../../btnEdit'
                        },
                        {
                            text: 'Del',
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.PurchaseOrderDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.material.on('select', this.onSelectMaterial, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.disc_rp.on('keyup', this.onDiscChange, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.price.on('keyup', this.onDiscChange, this);
        // this.material.on('select', this.showStock, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
    },
    showStock: function () {
        var material_id = this.material.getValue();
        // var loc_code = this.loc_code.getValue();
        if (material_id != '') {
            jun.getQtyStock(material_id, null, this.stok);
        }
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disc_rp.getValue());
        var bruto = round(price * qty, 4);
        if (a.id == "discid" || a.id == "priceid" || a.id == "qtyid") {
            this.disc_rp.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "disc_rpid") {
                this.disc.setValue(round((disca / bruto) * 100, 2));
            }
        }
    },
    onStoreChange: function (s, b, d) {
        s.refreshData();
    },
    onSelectMaterial: function (c, r, i) {
        // this.satuan.store.reload({ params:{ 'dimension': r.get('dimension') } });
        this.qty.reset();
        // this.satuan.setValue(r.get('sat'));
        this.price.reset();
        this.disc.reset();
        this.disc_rp.reset();
        this.vat.reset();
        this.note.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    resetForm: function () {
        this.material.reset();
        this.qty.reset();
        this.price.reset();
        this.disc.reset();
        this.disc_rp.reset();
        this.vat.reset();
        this.note.reset();
        this.showStock();
    },
    loadForm: function () {
        var material_id = this.material.getValue();
        if (material_id == "" || this.qty.getValue() == 0 || this.price.getValue() == "") {
            Ext.MessageBox.alert("Error", "Kode material, quantity dan harga harus diisi");
            return false;
        }
        if (this.store.find('material_id', material_id) != -1) {
            if (this.btnEdit.text == 'Save') {
                if (this.sm.getSelected().data.material_id != material_id) {
                    Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                    this.resetForm();
                    return false;
                }
            } else {
                Ext.MessageBox.alert("Error", "Material ini sudah ditambahkan.");
                return false;
            }
        }
        //var material = jun.getMaterial(material_id);
        // var sat = this.satuan.getValue();
        var qty = parseFloat(this.qty.getValue());
        var price = parseFloat(this.price.getValue());
        var bruto = qty * price;
        var disc = parseFloat(this.disc.getValue());
        var disc_rp = 0;
        if (disc != 0) {
            disc_rp = bruto * disc * 0.01;
        } else {
            disc_rp = parseFloat(this.disc_rp.getValue());
        }
        var vat = parseFloat(this.vat.getValue());
        var vatrp = 0;
        if (vat != 0) {
            vatrp = (bruto - disc_rp) * vat * 0.01;
        }
        var total = bruto - disc_rp + vatrp;
        var note = this.note.getValue();
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('qty', qty);
            // record.set('sat', sat);
            record.set('price', price);
            record.set('bruto', bruto);
            record.set('disc', disc);
            record.set('disc_rp', disc_rp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total', total);
            record.set('note', note);
            record.commit();
        } else {
            if (this.store.getCount() < this.max_item) {
                var c = this.store.recordType,
                    d = new c({
                        material_id: material_id,
                        qty: qty,
                        // sat: sat,
                        price: price,
                        bruto: bruto,
                        disc: disc,
                        disc_rp: disc_rp,
                        vat: vat,
                        vatrp: vatrp,
                        total: total,
                        note: note
                    });
                this.store.add(d);
            } else {
                Ext.MessageBox.alert("Warning", "Tidak dapat menambah item baru.<br>Maksimal jumlah item : <b>" + this.max_item + "<b>");
            }
        }
        this.resetForm();
        return true;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            // this.satuan.store.reload({params: {'dimension': jun.renderUnitOfMeasure_dimension(record.data.sat)}});
            this.material.setValue(record.data.material_id);
            this.qty.setValue(record.data.qty);
            this.price.setValue(record.data.price);
            this.disc.setValue(record.data.disc);
            this.disc_rp.setValue(record.data.disc_rp);
            this.vat.setValue(record.data.vat);
            this.note.setValue(record.data.note);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            if (this.loadForm()) {
                btn.setText("Edit");
                this.btnDisable(false);
            }
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        var idz = selectedz.json.po_detail_id;
        var form = new jun.PurchaseOrderDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});