jun.BreakdownDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Hasil Proses Breakdown",
    id: 'docs-jun.BreakdownDetailsGrid',
    // iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Kode Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'loc_code',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztBreakdownDetails;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Material :'
                },
                {
                    xtype: 'mfcombobox',
                    searchFields: [
                        'kode_material',
                        'nama_material'
                    ],
                    mode: 'local',
                    store: jun.rztMaterialCmp,
                    valueField: 'material_id',
                    itemSelector: 'tr.search-item',
                    tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                        '<th>Kode</th><th>Nama Material</th></tr></thead>',
                        '<tbody><tpl for="."><tr class="search-item">',
                        '<td>{kode_material}</td><td>{nama_material}</td>',
                        '</tr></tpl></tbody></table>'),
                    width: 175,
                    allowBlank: false,
                    enableKeyEvents: true,
                    listWidth: 400,
                    ref: '../bahan_baku',
                    displayField: 'nama_material'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Qty :'
                },
                {
                    xtype: 'numericfield',
                    id: 'qtyid',
                    ref: '../qty',
                    // style: 'margin-bottom:2px',
                    enableKeyEvents: true,
                    value: 1,
                    width: 100,
                    minValue: 0
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Gudang :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    fieldLabel: 'loc_code',
                    store: jun.rztStorageLocationCmp,
                    ref: '../loc_code',
                    valueField: 'loc_code',
                    displayField: 'loc_code'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Stok : 0',
                    ref: '../stock_d'
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd',
                    style: 'margin-left:20px'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.BreakdownDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.bahan_baku.on('select', this.showStock, this);
        this.loc_code.on('select', this.showStock, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    showStock: function () {
        var material_id = this.bahan_baku.getValue();
        var loc_code = this.loc_code.getValue();
        if (material_id != '' && loc_code != '') {
            jun.getQtyStock(material_id, loc_code, this.stock_d);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var material_id = this.bahan_baku.getValue();
        var qty = parseFloat(this.qty.getValue());
        var loc_code = this.loc_code.getValue();
        if (loc_code == "" || loc_code == undefined) {
            Ext.MessageBox.alert("Error", "Gudang harus disefinisikan.");
            return;
        }
        if (material_id == "" || material_id == undefined) {
            Ext.MessageBox.alert("Error", "Bahan baku harus disefinisikan.");
            return;
        }
        if (qty < 0) {
            Ext.MessageBox.alert("Error", "Qty bahan baku harus lebih dari nol.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('material_id', material_id);
            record.set('qty', qty);
            record.set('loc_code', loc_code);
            record.commit();
        } else {
            var c = jun.rztBreakdownDetails.recordType,
                d = new c({
                    material_id: material_id,
                    qty: qty,
                    loc_code: loc_code
                });
            jun.rztBreakdownDetails.add(d);
        }
        this.bahan_baku.reset();
        this.qty.reset();
        this.loc_code.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.bahan_baku.setValue(record.data.material_id);
            this.qty.setValue(record.data.qty);
            this.loc_code.setValue(record.data.loc_code);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
