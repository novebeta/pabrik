jun.SjintDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SjintDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjintDetailsStoreId',
            url: 'SjintDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sjint_details_id'},
                {name: 'qty'},
                {name: 'price'},
                {name: 'total'},
                {name: 'barang_id'},
                {name: 'sjint_id'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'ket'}
            ]
        }, cfg));
    }
});
jun.rztSjintDetails = new jun.SjintDetailsstore();
//jun.rztSjintDetails.load();
