jun.PerbaikanMesinGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Perbaikan Mesin/Utility",
    id: 'docs-jun.PerbaikanMesinGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Referensi',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl Lapor',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_lapor',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Pelapor',
            sortable: true,
            resizable: true,
            dataIndex: 'pelapor',
            width: 100
        },
        {
            header: 'Divisi / Dept',
            sortable: true,
            resizable: true,
            dataIndex: 'departemen',
            width: 100
        },
        {
            header: 'Nomor PP',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_pp',
            width: 100
        },
        {
            header: 'Tgl Selesai',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_selesai',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Pelaksana',
            sortable: true,
            resizable: true,
            dataIndex: 'pelaksana',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 60,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case 0 :
                        return 'OPEN';
                    case 1 :
                        return 'FINISHED';
                }
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actPrint) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
    },
    initComponent: function () {
        this.store = jun.rztPerbaikanMesin;
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        if (jun.rztMaterialSparepart.getTotalCount() === 0) jun.rztMaterialSparepart.load();
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1);
                break;
            case USER_GA:
                this.userPermission(1, 1, 1);
                break;
            default:
                this.userPermission(0, 0, 0);
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Laporan',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print Form',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actPrint) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_pp: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PerbaikanMesinGrid').setFilterData(m.status_pp);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_pp: 0,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PerbaikanMesinGrid').setFilterData(m.status_pp);
                                    }
                                }
                            },
                            {
                                text: 'FINISHED',
                                status_pp: 1,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PerbaikanMesinGrid').setFilterData(m.status_pp);
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-printPerbaikanMesin",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "perbaikan_mesin_id",
                            ref: "../../perbaikan_mesin_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.PerbaikanMesinGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printPerbaikanMesin, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(0);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    setFilterData: function (sts) {
        this.status_pp = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        if (this.store.baseParams.status && this.store.baseParams.status == this.status_pp) {
            this.store.baseParams = {
                mode: "grid",
                status: this.status_pp
            };
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                mode: "grid",
                status: this.status_pp
            };
            this.botbar.moveFirst();
        }
    },
    loadForm: function () {
        var form = new jun.PerbaikanMesinWin({
            modez: 0,
            title: "Buat Laporan Perbaikan Mesin",
            konfirmasiBayar: false,
            iconCls: 'silk13-add'
        });
        form.show(this);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Perbaikan Mesin.");
            return;
        }
        var idz = selectedz.json.perbaikan_mesin_id;
        var filter = selectedz.json.status == OPEN && this.actEdit;
        var form = new jun.PerbaikanMesinWin({
            modez: ( filter ? 1 : 2),
            perbaikan_mesin_id: idz,
            title: ( filter ? "Ubah" : "Lihat") + " Laporan Perbaikan Mesin",
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        if (this.record.get('status') == OPEN) form.mutu.reset();
        form.griddetils.store.baseParams = {perbaikan_mesin_id: idz};
        form.griddetils.store.load();
        form.griddetils.store.baseParams = {};
    },
    printPerbaikanMesin: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Perbaikan Mesin");
            return;
        }
        Ext.getCmp("form-printPerbaikanMesin").getForm().standardSubmit = !0;
        Ext.getCmp("form-printPerbaikanMesin").getForm().url = "Report/PrintPerbaikanMesin";
        this.perbaikan_mesin_id.setValue(selectedz.json.perbaikan_mesin_id);
        var form = Ext.getCmp('form-printPerbaikanMesin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
