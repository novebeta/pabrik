jun.PerbaikanMesinWin = Ext.extend(Ext.Window, {
    title: 'Form Laporan Perbaikan Mesin',
    modez: 1,
    width: 900,
    height: 530,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.supplierAccountStore = new jun.SupplierBankStore();
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 5px',
                id: 'form-PerbaikanMesin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. Ref.:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        readOnly: true,
                        width: 175,
                        x: 5 + 90,
                        y: 2
                    },
                    {
                        xtype: "tabpanel",
                        activeTab: 0,
                        height: 320,
                        x: 5,
                        y: 35,
                        items: [
                            {
                                title: 'Laporan Kerusakan',
                                frame: false,
                                layout: 'absolute',
                                bodyStyle: "background-color: #E8E8E8; padding: 10px",
                                border: false,
                                items: [
                                    {
                                        xtype: "label",
                                        text: "Divisi/Dept.:",
                                        x: 8,
                                        y: 5 + 8
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'departemen',
                                        ref: '../../departemen',
                                        allowBlank: false,
                                        width: 175,
                                        x: 8 + 90,
                                        y: 2 + 8
                                    },
                                    {
                                        xtype: "label",
                                        text: "Lokasi:",
                                        x: 8,
                                        y: 35 + 8
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'lokasi_kerja',
                                        ref: '../../lokasi_kerja',
                                        allowBlank: false,
                                        width: 175,
                                        x: 8 + 90,
                                        y: 32 + 8
                                    },
                                    {
                                        xtype: "label",
                                        text: "Tanggal Laporan:",
                                        x: 298,
                                        y: 5 + 8
                                    },
                                    {
                                        xtype: 'xdatefield',
                                        format: 'd M Y',
                                        name: 'tgl_lapor',
                                        ref: '../../tgl_lapor',
                                        allowBlank: false,
                                        width: 175,
                                        x: 400,
                                        y: 2 + 8
                                    },
                                    {
                                        xtype: "label",
                                        text: "Dilaporkan oleh:",
                                        x: 298,
                                        y: 35 + 8
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'pelapor',
                                        ref: '../../pelapor',
                                        allowBlank: false,
                                        width: 175,
                                        x: 400,
                                        y: 32 + 8
                                    },
                                    {
                                        xtype: "label",
                                        text: "Permintaan perbaikan:",
                                        x: 8,
                                        y: 65 + 8
                                    },
                                    {
                                        xtype: 'textarea',
                                        name: 'permintaan_perbaikan',
                                        ref: '../../permintaan_perbaikan',
                                        allowBlank: false,
                                        height: 190,
                                        width: 847,
                                        x: 8,
                                        y: 65 + 20 + 8
                                    }
                                ]
                            },
                            {
                                title: 'Laporan Perbaikan',
                                frame: false,
                                layout: 'absolute',
                                bodyStyle: "background-color: #E8E8E8; padding: 5px",
                                border: false,
                                items: [
                                    {
                                        xtype: "label",
                                        text: "Diterima:",
                                        x: 8,
                                        y: 5 + 8
                                    },
                                    {
                                        xtype: 'compositefield',
                                        width: 175,
                                        x: 8 + 90,
                                        y: 2 + 8,
                                        defaults: {
                                            flex: 1
                                        },
                                        items: [
                                            {
                                                xtype: 'xdatefield',
                                                format: 'd M Y',
                                                name: 'tgl_terima',
                                                width: 100
                                            },
                                            {
                                                xtype: 'displayfield',
                                                value: 'jam'
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'jam_terima',
                                                width: 45
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "label",
                                        text: "Nomor PP:",
                                        x: 8,
                                        y: 35 + 8
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'nomor_pp',
                                        ref: '../../nomor_pp',
                                        width: 175,
                                        x: 8 + 90,
                                        y: 32 + 8
                                    },
                                    {
                                        xtype: "label",
                                        text: "Selesai:",
                                        x: 298,
                                        y: 5 + 8
                                    },
                                    {
                                        xtype: 'compositefield',
                                        width: 175,
                                        x: 400,
                                        y: 2 + 8,
                                        defaults: {
                                            flex: 1
                                        },
                                        items: [
                                            {
                                                xtype: 'xdatefield',
                                                format: 'd M Y',
                                                name: 'tgl_selesai',
                                                width: 100
                                            },
                                            {
                                                xtype: 'displayfield',
                                                value: 'jam'
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'jam_selesai',
                                                width: 45
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "label",
                                        text: "Pelaksana:",
                                        x: 298,
                                        y: 35 + 8
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'pelaksana',
                                        ref: '../../pelaksana',
                                        width: 175,
                                        x: 400,
                                        y: 32 + 8
                                    },
                                    {
                                        xtype: "label",
                                        text: "Laporan pekerjaan:",
                                        x: 8,
                                        y: 65 + 8
                                    },
                                    {
                                        xtype: 'textarea',
                                        name: 'laporan_pekerjaan',
                                        ref: '../../laporan_pekerjaan',
                                        height: 190,
                                        width: 847,
                                        x: 8,
                                        y: 65 + 20 + 8
                                    }
                                ]
                            },
                            {
                                title: 'Pemakaian Sparepart',
                                frame: false,
                                layout: 'fit',
                                border: false,
                                items: [
                                    new jun.PerbaikanMesinDetailGrid({
                                        x: 5,
                                        y: 155,
                                        frameHeader: !1,
                                        height: 225,
                                        //anchor: "100% 100%",
                                        header: !1,
                                        ref: "../../../griddetils",
                                        readOnly: ((this.modez < 2) ? false : true)
                                    })
                                ]
                            }
                        ]
                    },
                    {
                        xtype: "label",
                        text: "Adm. Engineering:",
                        x: 8,
                        y: 5 + 360 + 20
                    },
                    {
                        xtype: 'textfield',
                        name: 'adm_engineering',
                        ref: '../adm_engineering',
                        width: 175,
                        x: 8 + 100,
                        y: 2 + 360 + 20
                    },
                    {
                        xtype: "label",
                        text: "Manager/SPV:",
                        x: 8,
                        y: 35 + 360 + 20
                    },
                    {
                        xtype: 'textfield',
                        name: 'manager_spv',
                        ref: '../manager_spv',
                        width: 175,
                        x: 8 + 100,
                        y: 32 + 360 + 20
                    },
                    {
                        xtype: 'fieldset',
                        checkboxToggle: false,
                        title: 'Serah Terima Hasil',
                        autoHeight: true,
                        collapsed: false,
                        layout: 'form',
                        x: 298,
                        y: 360,
                        width: 305,
                        items: [
                            {
                                xtype: 'xdatefield',
                                fieldLabel: 'Tanggal',
                                format: 'd M Y',
                                name: 'tgl_serahterima',
                                ref: '../../tgl_serahterima',
                                width: 175
                            },
                            {
                                xtype: "combo",
                                fieldLabel: 'Mutu',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: new Ext.data.ArrayStore({
                                    idIndex: 0,
                                    fields: [{name: 'myId', type: 'float'}, 'displayText'],
                                    data: [[1, 'Memuaskan'], [-1, 'Tidak memuaskan']]
                                }),
                                valueField: 'myId',
                                displayField: 'displayText',
                                width: 175,
                                ref: '../../mutu',
                                name: 'mutu'
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PerbaikanMesinWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(this.rejectPR ? false : true);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-PerbaikanMesin').getForm().submit({
            url: 'PerbaikanMesin/create/',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                perbaikan_mesin_id: this.perbaikan_mesin_id,
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztPerbaikanMesin.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PerbaikanMesin').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});