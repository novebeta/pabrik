jun.SupplierStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SupplierStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SupplierStoreId',
            url: 'Supplier',
            //autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'supplier_id'},
                {name: 'supplier_code'},
                {name: 'supplier_name'},
                {name: 'contact_person'},
                {name: 'alamat'},
                {name: 'kota'},
                {name: 'negara'},
                {name: 'kodepos'},
                {name: 'telepon'},
                {name: 'fax'},
                {name: 'email'},
                {name: 'npwp'},
                {name: 'account_code'},
                {name: 'term_of_payment'}
            ]
        }, cfg));
    }
});
jun.rztSupplier = new jun.SupplierStore();
jun.rztSupplierLib = new jun.SupplierStore();
jun.rztSupplierCmp = new jun.SupplierStore();
jun.SupplierBankStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SupplierBankStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SupplierBankStoreId',
            url: 'SupplierBank',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'supplier_bank_id'},
                {name: 'nama_bank'},
                {name: 'no_rekening'},
                {name: 'nama_rekening'},
                {name: 'alamat_bank'},
                {name: 'currency'},
                {name: 'supplier_id'}
            ]
        }, cfg));
    }
});
jun.rztSupplierBank = new jun.SupplierBankStore();
//jun.SupplierSalesStore = Ext.extend(Ext.data.JsonStore, {
//    constructor: function (cfg) {
//        cfg = cfg || {};
//        jun.SupplierSalesStore.superclass.constructor.call(this, Ext.apply({
//            storeId: 'SupplierSalesStoreId',
//            url: 'SupplierSales',
//            autoLoad: true,
//            root: 'results',
//            totalProperty: 'total',
//            fields: [
//                {name: 'supplier_sales_id'},
//                {name: 'supplier_sales_name'},
//                {name: 'supplier_id'}
//            ]
//        }, cfg));
//    }
//});
//jun.rztSupplierSales = new jun.SupplierSalesStore();
//jun.rztSupplierSalesCmp = new jun.SupplierSalesStore();