jun.KasGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Masuk",
    id: 'docs-jun.KasGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        if (jun.rztBankTransCmp.getTotalCount() === 0) {
            jun.rztBankTransCmp.load();
        }
        if (jun.rztChartMasterCmpPendapatan.getTotalCount() === 0) {
            jun.rztChartMasterCmpPendapatan.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        jun.rztKas.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tgl: Ext.getCmp('tglkasmasukgridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztKas;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Kas/Bank Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Kas/Bank Masuk',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Masuk',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglkasmasukgridid',
                    ref: '../tgl',
                    noPastYears: false
                })
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     ref: '../tgl',
                //     id: 'tglcashgrid'
                // }
            ]
        };
        jun.KasGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnPrint.on('Click', this.printKas, this);
        this.store.removeAll();
    },
    printKas: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'kas/print',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.kas_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                findPrinterReceipt();
                if (notReady()) {
                    return;
                }
//                qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                while (!qz.isDoneAppending()) {}
//                qz.append("\x1B\x40"); // 1
//                qz.append("\x1B\x21\x08"); // 2
//                qz.append("\x1B\x21\x01"); // 3
//                qz.append(response.msg);
//                qz.append("\x1D\x56\x41"); // 4
//                qz.append("\x1B\x40"); // 5
//                qz.print();
                qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
                qz.printHTML();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWin({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
//        form.modal.setValue(this.record.data.type_ == '1');
//        form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    }
});
jun.KasGridOut = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Keluar",
    id: 'docs-jun.KasGridOut',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'no_kwitansi',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBankTransCmp.getTotalCount() === 0) {
            jun.rztBankTransCmp.load();
        }
        if (jun.rztChartMasterCmpBiaya.getTotalCount() === 0) {
            jun.rztChartMasterCmpBiaya.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        jun.rztKasKeluar.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tgl: Ext.getCmp('tglkaskeluargridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztKasKeluar;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Kas/Bank Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Kas/Bank Keluar',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Keluar',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },

                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglkaskeluargridid',
                    ref: '../tgl',
                    noPastYears: false
                })
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     ref: '../tgl',
                //     id: 'tglcashgridout'
                // }
            ]
        };
        jun.KasGridOut.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printKas, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.store.removeAll();
    },
    printKas: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'kas/print',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.kas_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
                qz.printHTML();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinOut({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinOut({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    }
});
jun.KasGridPusat = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Masuk",
    id: 'docs-jun.KasGridPusat',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        if (jun.rztBankCmpPusat.getTotalCount() === 0) {
            jun.rztBankCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        jun.rztKasPusat.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglkasmasukpusatgridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztKasPusat;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Kas/Bank Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Kas/Bank Masuk',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'Hapus Kas/Bank Masuk',
                //     ref: '../btnDel'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Masuk',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglkasmasukpusatgridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     ref: '../tgl',
                //     id: 'tglcashgridpusat'
                // },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportKasMasuk",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "kas_id",
                            ref: "../../kas_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.KasGridPusat.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDel.on('Click', this.deleteRec, this);
        this.btnPrint.on('Click', this.btnprintClick, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },

    btnprintClick: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Kas Masuk");
            return;
        }
        // var idz = selectedz.json.breakdown_id;
        Ext.getCmp("form-ReportKasMasuk").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKasMasuk").getForm().url = "Report/PrintKas";
        this.kas_id.setValue(selectedz.json.kas_id);
        var form = Ext.getCmp('form-ReportKasMasuk').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinPusat({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinPusat({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
//        form.modal.setValue(this.record.data.type_ == '1');
//        form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'Kas/delete',
            method: 'POST',
            params: {
                id: record.json.kas_id
            },
            success: function (f, a) {
                jun.rztKasPusat.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.KasGridPusatOut = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas/Bank Keluar",
    id: 'docs-jun.KasGridPusatOut',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'no_kwitansi',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'keperluan',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBankCmpPusat.getTotalCount() === 0) {
            jun.rztBankCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        jun.rztKasPusatKeluar.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglkaskeluarpusatgridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztKasPusatKeluar;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Kas/Bank Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Kas/Bank Keluar',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'Delete Payment',
                //     ref: '../btnDel'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Print Kas/Bank Keluar',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },

                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglkaskeluarpusatgridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     ref: '../tgl',
                //     id: 'tglcashgridpusatout'
                // },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportKasKeluar",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "kas_id",
                            ref: "../../kas_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.KasGridPusatOut.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDel.on('Click', this.deleteRec, this);
        this.btnPrint.on('Click', this.btnprintClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.store.removeAll();
    },
    btnprintClick: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Kas Keluar");
            return;
        }
        // var idz = selectedz.json.breakdown_id;
        Ext.getCmp("form-ReportKasKeluar").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKasKeluar").getForm().url = "Report/PrintKas";
        this.kas_id.setValue(selectedz.json.kas_id);
        var form = Ext.getCmp('form-ReportKasKeluar').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KasWinPusatOut({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.kas_id;
        var form = new jun.KasWinPusatOut({modez: 1, idkas: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztKasDetail.baseParams = {
            kas_id: idz
        };
        jun.rztKasDetail.load();
        jun.rztKasDetail.baseParams = {};
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'Kas/delete',
            method: 'POST',
            params: {
                id: record.json.kas_id
            },
            success: function (f, a) {
                jun.rztKasPusatKeluar.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
