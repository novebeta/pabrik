jun.Bankstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Bankstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BankStoreId',
            url: 'Bank',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'bank_id'},
                {name: 'nama_bank'},
                {name: 'ket'},
                {name: 'account_code'},
                {name: 'visible'}
            ]
        }, cfg));
    }
});
jun.rztBank = new jun.Bankstore();
jun.rztBankLib = new jun.Bankstore();
jun.rztBankCmp = new jun.Bankstore({baseParams: {mode: "bank_cabang", f: "cmp"}});
jun.rztBankCmpPusat = new jun.Bankstore({baseParams: {mode: "bank_pusat", f: "cmp"}});
jun.rztBankTransCmpPusat = new jun.Bankstore({baseParams: {mode: "bank_trans_pusat", f: "cmp"}});
jun.rztBankTransCmp = new jun.Bankstore();
//jun.rztBankTransCmp = new jun.Bankstore({baseParams: {mode: "bank_trans_cabang", f: "cmp"}});
//jun.rztBankTransCmpPusat = new jun.Bankstore({baseParams: {mode: "bank_trans_pusat", f: "cmp"}});
//jun.rztBankCmp.load();
