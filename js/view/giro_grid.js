jun.GiroCair = Ext.extend(Ext.Window, {
    title: "Pencairan Giro",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-GiroCair",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tgl',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl_cair",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bank',
                        store: jun.rztBankTransCmp,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: "100%"
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'notep_',
                        ref: '../notep_',
                        anchor: "100%",
                        height: 52
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Update TGL",
                    ref: "../btnSave"
                }
            ]
        };
        jun.GiroCair.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        this.btnSave.setDisabled(true);
        var urlz = 'Giro/cair/';
        Ext.getCmp('form-GiroCair').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                id: this.id
            },
            success: function (f, a) {
                jun.rztGiro.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnSave.setDisabled(false);
            }
        });
    }
});
jun.GiroGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Giro",
    id: 'docs-jun.GiroGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Giro',
            sortable: true,
            resizable: true,
            dataIndex: 'no_giro',
            width: 100
        },
        {
            header: 'Tgl Klr',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_klr',
            width: 100
        },
        {
            header: 'Tgl Jt',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_jt',
            width: 100
        },
        {
            header: 'Bank Giro',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_giro',
            width: 100
        },
        {
            header: 'Saldo',
            sortable: true,
            resizable: true,
            dataIndex: 'saldo',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztGiro;
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Giro',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Giro',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Cair Giro',
                    ref: '../btnCair'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Tolak Giro',
                    ref: '../btnTolak'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del Giro',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_po: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_po: P0_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'USED',
                                status_po: PO_PARTIALLY_RECEIVED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'CAIR',
                                status_po: PO_RECEIVED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            },
                            {
                                text: 'TOLAK',
                                status_po: PO_INVOICED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseOrderGrid').setFilterData(m.status_po);
                                    }
                                }
                            }
                        ]
                    }
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.GiroGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnTolak.on('Click', this.tolakGiro, this);
        this.btnCair.on('Click', this.cairGiro, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.GiroWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Giro");
            return;
        }
        if (selectedz.json.used == '1') {
            Ext.MessageBox.alert("Warning", "Giro sudah terpakai. Tidak bisa diedit.");
            return;
        }
        var idz = selectedz.json.giro_id;
        var form = new jun.GiroWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    tolakGiro: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Giro");
            return;
        }
        if (selectedz.json.tolak != 0) {
            Ext.MessageBox.alert("Warning", "Giro sudah ditolak.");
            return;
        }
        if (selectedz.json.tgl_cair != null) {
            Ext.MessageBox.alert("Warning", "Giro tidak bisa ditolak karena sudah cair.");
            return;
        }
        Ext.MessageBox.prompt('Tolak Giro', 'Keterangan giro ditolak : ', function (btn, sInput) {
            if (btn == 'cancel') {
                return;
            }
            Ext.Ajax.request({
                url: 'Giro/tolak/id/' + selectedz.json.giro_id,
                method: 'POST',
                params: {
                    notep_: sInput
                },
                success: function (f, a) {
                    jun.rztGiro.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }, this, true);
    },
    cairGiro: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Giro");
            return;
        }
        if (selectedz.json.tolak != 0) {
            Ext.MessageBox.alert("Warning", "Giro sudah di tolak tidak bisa dicairkan.");
            return;
        }
        if (selectedz.json.tgl_cair != null) {
            Ext.MessageBox.alert("Warning", "Giro tidak bisa ditolak karena sudah cair.");
            return;
        }
        var idz = selectedz.json.giro_id;
        var form = new jun.GiroCair({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.data.used != 0) {
            Ext.MessageBox.alert("Warning", "Giro tidak bisa di hapus karena sudah dipakai.");
            return;
        }
        Ext.Ajax.request({
            url: 'Giro/delete/id/' + record.json.giro_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztGiro.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
