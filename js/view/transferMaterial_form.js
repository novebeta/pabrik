jun.TransferMaterialWin = Ext.extend(Ext.Window, {
    title: 'Transfer Material',
    modez: 1,
    width: 900,
    height: 480,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    transOut: true,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-TransferMaterial',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Nomor",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        readOnly: true,
                        width: 175,
                        x: 75,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        x: 75,
                        y: 32,
                        value: new Date(),
                        allowBlank: false,
                        editable: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Asal",
                        x: 290,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'loc_code',
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'dari_loc_code',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        width: 175,
                        x: 365,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tujuan",
                        x: 290,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'loc_code',
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'ke_loc_code',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        width: 175,
                        x: 365,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Note",
                        x: 5 + 285 + 285,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../note',
                        name: 'note',
                        width: 240,
                        height: 50,
                        x: 365 + 175 + 75 + 10,
                        y: 2
                    },
                    new jun.TransferMaterialDetailGrid({
                        x: 5,
                        y: 65,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetil",
                        anchor: '100% 100%',
                        store: this.storeDetail,
                        // storeComboMaterial: this.storeComboMaterial,
                        // tglExpiredStore: this.tglExpiredStore,
                        trans_id: this.trans_id,
                        // batch_id: this.batch_id,
                        // readOnly: ((this.modez < 2) ? false : true),
                        transOut: this.transOut
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Setuju',
                    ref: '../btnAccept',
                    iconCls: 'silk13-tick'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferMaterialWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnAccept.on('click', this.onbtnAcceptclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.penerima.on('select', this.onSelectPenerima, this);
        // this.pengirim.on('select', this.onSelectPengirim, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnAccept.setVisible(false);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnAccept.setVisible(false);
            this.btnSave.setVisible(false);
        } else { //view
            this.btnAccept.setVisible(this.accepting ? true : false);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
        // if (this.modez == 0 && this.batch_id) {
        //     this.btnSaveClose.setText("Proses");
        //     this.btnSaveClose.setIconClass('silk13-flag_green');
        // }
        // this.griddetil.pengirim = this.pengirim.getValue();
        // this.griddetil.penerima = this.penerima.getValue();
    },
    onWinClose: function () {
        this.griddetil.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnAccept.setDisabled(status);
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onSelectPenerima: function (c, r, i) {
        if (c.getValue() == this.pengirim.getValue()) c.reset();
        else this.griddetil.penerima = c.getValue();
    },
    onSelectPengirim: function (c, r, i) {
        if (c.getValue() == this.penerima.getValue()) c.reset();
        else this.griddetil.pengirim = c.getValue();
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-TransferMaterial').getForm().submit({
            url: 'TransferMaterial/Create/',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                trans_id: this.trans_id,
                batch_id: this.batch_id,
                trans_out: this.transOut ? 1 : 0,
                detil: Ext.encode(Ext.pluck(
                    this.storeDetail.data.items, "data"))
            },
            success: function (f, a) {
                //if (jun.rztBatch.getTotalCount() !== 0) jun.rztBatch.reload();
                //if (jun.rztTransferMaterial.getTotalCount() !== 0) jun.rztTransferMaterial.reload();
                if (Ext.getCmp('docs-jun.TransferMaterialGrid') !== undefined) Ext.getCmp('docs-jun.TransferMaterialGrid').reloadStore();
                if (Ext.getCmp('docs-jun.BatchGrid') !== undefined) Ext.getCmp('docs-jun.BatchGrid').botbar.doRefresh();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferMaterial').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnAcceptclick: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-TransferMaterial').getForm().submit({
            url: 'TransferMaterial/Accept/id/' + this.trans_id,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                // if (jun.rztBatch.getTotalCount() !== 0) jun.rztBatch.reload();
                if (jun.rztTransferMaterial.getTotalCount() !== 0) jun.rztTransferMaterial.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
