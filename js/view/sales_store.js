jun.Salesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Salesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesStoreId',
            url: 'Sales',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sales_id'},
                {name: 'doc_ref'},
                {name: 'tempo'},
                {name: 'sub_total', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'no_bg_cek'},
                {name: 'lunas'},
                {name: 'final'},
                {name: 'tgl'},
                {name: 'id_user'},
                {name: 'parent'},
                {name: 'bruto', type: 'float'},
                {name: 'tdate'},
                {name: 'arus'},
                {name: 'salesman_id'},
                {name: 'customer_id'},
                {name: 'totalpot', type: 'float'},
                {name: 'tot_pot1', type: 'float'},
                {name: 'tot_pot2', type: 'float'},
                {name: 'tot_pot3', type: 'float'},
                {name: 'salesspv_id'},
                {name: 'received'},
                {name: 'vatrp'}
            ]
        }, cfg));
    }
});
jun.rztSales = new jun.Salesstore();
jun.rztSalesKredit = new jun.Salesstore();
jun.rztReturSales = new jun.Salesstore({
    url: 'ReturSales'
});
//jun.rztSales.load();
