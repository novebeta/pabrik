jun.Kasstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Kasstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KasStoreId',
            url: 'Kas',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kas_id'},
                {name: 'doc_ref'},
                {name: 'no_kwitansi'},
                {name: 'keperluan'},
                {name: 'total'},
                {name: 'bank_id'},
                {name: 'tgl'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'type_'},
                {name: 'arus'}
            ]
        }, cfg));
    }
});
//jun.rztKas = new jun.Kasstore();
jun.rztKas = new jun.Kasstore({baseParams: {mode: "masuk"}});
jun.rztKasKeluar = new jun.Kasstore({baseParams: {mode: "keluar"}});
jun.rztKasPusat = new jun.Kasstore({baseParams: {mode: "masuk_pusat"}});
jun.rztKasPusatKeluar = new jun.Kasstore({baseParams: {mode: "keluar_pusat"}});
//jun.rztKas.load();
