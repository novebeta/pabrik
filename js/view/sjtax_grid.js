jun.SjtaxGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Surat Jalan Pajak",
    id: 'docs-jun.SjtaxGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. SJ Pajak',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        },
        {
            header: 'Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_id',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSjtax;
        jun.rztSjtax.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglsjtaxgridid').getValue()
                    }
                }
            }
        });
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglsjtaxgridid',
                    ref: '../tgl'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print SJ Pajak',
                    ref: '../btnPrintSJTax'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print INV Pajak',
                    ref: '../btnPrintINVTax'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportSuratJalan",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "type",
                            ref: "../../type_report"
                        },
                        {
                            xtype: "hidden",
                            name: "sjtax_id",
                            ref: "../../sjtax_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.SjtaxGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        //this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.btnPrintSJTax.on('Click', this.printSjtax, this);
        this.btnPrintINVTax.on('Click', this.printSjtax, this);
        this.tgl.on('select', function () {
            this.store.reload();
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    printSjtax: function (btn) {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Surat jalan");
            return;
        }
        Ext.getCmp("form-ReportSuratJalan").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSuratJalan").getForm().url = "Report/PrintSuratJalanTax";
        this.type_report.setValue(btn.refName);
        this.sjtax_id.setValue(selectedz.json.sjtax_id);
//        this.tglfrom.setValue(this.tgl.getValue().format('Y-m-d'));
        var form = Ext.getCmp('form-ReportSuratJalan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SjtaxWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sjtax_id;
        var form = new jun.SjtaxWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Sjtax/delete/id/' + record.json.sjtax_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSjtax.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
