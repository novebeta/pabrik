jun.StockMovesGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "StockMoves",
    id: 'docs-jun.StockMovesGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'stock_moves_id',
            sortable: true,
            resizable: true,
            dataIndex: 'stock_moves_id',
            width: 100
        },
        {
            header: 'type_no',
            sortable: true,
            resizable: true,
            dataIndex: 'type_no',
            width: 100
        },
        {
            header: 'trans_no',
            sortable: true,
            resizable: true,
            dataIndex: 'trans_no',
            width: 100
        },
        {
            header: 'tran_date',
            sortable: true,
            resizable: true,
            dataIndex: 'tran_date',
            width: 100
        },
        {
            header: 'price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100
        },
        {
            header: 'reference',
            sortable: true,
            resizable: true,
            dataIndex: 'reference',
            width: 100
        }
        /*
         {
         header:'qty',
         sortable:true,
         resizable:true,
         dataIndex:'qty',
         width:100
         },
         {
         header:'store',
         sortable:true,
         resizable:true,
         dataIndex:'store',
         width:100
         },
         {
         header:'tdate',
         sortable:true,
         resizable:true,
         dataIndex:'tdate',
         width:100
         },
         {
         header:'id_user',
         sortable:true,
         resizable:true,
         dataIndex:'id_user',
         width:100
         },
         {
         header:'batch',
         sortable:true,
         resizable:true,
         dataIndex:'batch',
         width:100
         },
         {
         header:'tgl_exp',
         sortable:true,
         resizable:true,
         dataIndex:'tgl_exp',
         width:100
         },
         {
         header:'barang_id',
         sortable:true,
         resizable:true,
         dataIndex:'barang_id',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztStockMoves;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.StockMovesGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.StockMovesWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.stock_moves_id;
        var form = new jun.StockMovesWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'StockMoves/delete/id/' + record.json.stock_moves_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztStockMoves.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.KelolaStokGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pengelolaan Persediaan",
    id: 'docs-jun.KelolaStokGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        // {
        //     header: 'Tanggal',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tran_date',
        //     width: 100
        // },
        {
            header: 'Ref. Dok',
            sortable: true,
            resizable: true,
            dataIndex: 'reference',
            width: 100
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        this.store = jun.rztKelolaStok;
        jun.rztKelolaStok.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglkelolagridid').getValue()
                    }
                }
            }
        });
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Pengelolaan',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Pengelolaan',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglkelolagridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.KelolaStokGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', function () {
            this.store.reload();
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KelolaStokWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.trans_no;
        var form = new jun.KelolaStokWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztKelolaStokDetil.baseParams = {
            trans_no: idz
        };
        jun.rztKelolaStokDetil.load();
        jun.rztKelolaStokDetil.baseParams = {};
    }
});
jun.KelolaStokDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Detil Pengelolaan Persediaan",
    id: 'docs-jun.KelolaStokDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Kode Material',
            resizable: true,
            dataIndex: 'material_id',
            width: 100,
            renderer: jun.renderKodeMaterial
        },
        {
            header: 'Nama Material',
            resizable: true,
            dataIndex: 'material_id',
            width: 250,
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Kode Gudang',
            sortable: true,
            resizable: true,
            dataIndex: 'storage_location',
            width: 50
        },
        {
            header: 'Qty',
            resizable: true,
            dataIndex: 'qty',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztKelolaStokDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Item',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Item',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.KelolaStokDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KelolaStokDetilWin({modez: 0});
        form.show();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus item ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});