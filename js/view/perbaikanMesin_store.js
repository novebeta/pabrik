jun.PerbaikanMesinstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PerbaikanMesinstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PerbaikanMesinStoreId',
            url: 'PerbaikanMesin',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'perbaikan_mesin_id'},
                {name: 'doc_ref'},
                {name: 'departemen'},
                {name: 'lokasi_kerja'},
                {name: 'tgl_lapor', type: 'date'},
                {name: 'pelapor'},
                {name: 'permintaan_perbaikan'},
                {name: 'tgl_terima', type: 'date'},
                {name: 'jam_terima'},
                {name: 'nomor_pp'},
                {name: 'laporan_pekerjaan'},
                {name: 'tgl_selesai', type: 'date'},
                {name: 'jam_selesai'},
                {name: 'adm_engineering'},
                {name: 'manager_spv'},
                {name: 'pelaksana'},
                {name: 'tgl_serahterima', type: 'date'},
                {name: 'mutu', type: 'int'},
                {name: 'tdate', type: 'date'},
                {name: 'id_user'},
                {name: 'status', type: 'int'}
            ]
        }, cfg));
    }
});
jun.rztPerbaikanMesin = new jun.PerbaikanMesinstore();