jun.TransferMaterialDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferMaterialDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferMaterialDetailStoreId',
            url: 'TransferMaterialDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_material_detail_id'},
                {name: 'transfer_material_id'},
                {name: 'material_id'},
                {name: 'qty'}
            ]
        }, cfg));
    }
});
jun.rztTransferMaterialDetail = new jun.TransferMaterialDetailstore();
//jun.rztTransferMaterialDetail.load();
