jun.TransferMaterialstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferMaterialstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferMaterialStoreId',
            url: 'TransferMaterial',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_material_id'},
                {name: 'doc_ref'},
                {name: 'tgl', type: 'date'},
                {name: 'dari_loc_code'},
                {name: 'ke_loc_code'},
                {name: 'note'},
                {name: 'accepted', type: 'int'},
                {name: 'tdate_accept', type: 'date'},
                {name: 'tdate', type: 'date'},
                {name: 'id_user'},
                {name: 'batch_id'},
                {name: 'trans_out', type: 'int'}
            ]
        }, cfg));
    }
});
jun.rztTransferMaterial = new jun.TransferMaterialstore();
// jun.rztStorageLocation = new Ext.data.ArrayStore({
//     id: 0,
//     fields: [
//         'id',
//         'name',
//         'havestock'
//     ],
//     data: storloc_arraystore
// });
jun.rztStorageLocationNoStock = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'id',
        'name',
        'havestock'
    ],
    data: storloc_nostock_arraystore
});
jun.rztStatusAcceptedTandaTerima = new Ext.data.ArrayStore({
    id: 0,
    fields: [
        'val',
        'displayText'
    ],
    data: [
        [0, 'Belum diterima'],
        [1, 'Telah diterima']
    ]
});