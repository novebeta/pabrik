jun.SalesKreditWin = Ext.extend(Ext.Window, {
    title: 'Penjualan',
    modez: 1,
    width: 955,
    height: 560,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Penjualan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Faktur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        value: DATE_NOW,
                        // readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Tempo:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tempo',
                        fieldLabel: 'tempo',
                        name: 'tempo',
                        id: 'tempoid',
                        format: 'd M Y',
                        width: 175,
//                        readOnly: true,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode_customer',
                            'nama_customer'
                        ],
                        mode: 'local',
                        store: jun.rztCustomersCmp,
                        valueField: 'customer_id',
                        hiddenName: 'customer_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Customer</th><th>Nama Customer</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_customer}</td><td>{nama_customer}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../customer',
                        displayField: 'kode_customer',
                        allowBlank: false,
                        width: 200,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Sales:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'salesman_code',
                            'salesman_name'
                        ],
                        mode: 'local',
                        store: jun.rztSalesmanCmp,
                        valueField: 'salesman_id',
                        hiddenName: 'salesman_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Salesman</th><th>Nama Salesman</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{salesman_code}</td><td>{salesman_name}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../sales',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        width: 200,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Sales SPV:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'salesman_code',
                            'salesman_name'
                        ],
                        mode: 'local',
                        store: jun.rztSalesmanCmp,
                        valueField: 'salesman_id',
                        hiddenName: 'salesspv_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Salesman</th><th>Nama Salesman</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{salesman_code}</td><td>{salesman_name}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../salesspv',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        width: 200,
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 585,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: "80",
                        width: "215",
                        readOnly: true,
                        // x: 375,
                        x: 665,
                        y: 2
                    },
                    new jun.SalesDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 315,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 425
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'sub_total',
//                        id: 'sub_totalid',
//                        ref: '../sub_total',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 422
//                    },
//                    {
//                        xtype: "label",
//                        text: "PPN:",
//                        x: 295,
//                        y: 455
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'ppn',
//                        id: 'ppnid',
//                        ref: '../ppn',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 452
//                    },
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 355,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'bruto',
                        id: 'brutosalesid',
                        ref: '../bruto',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 435,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Diskon:",
                        x: 355,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'totalpot',
                        id: 'totalpotsalesid',
                        ref: '../totalpot',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 435,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "VAT:",
                        x: 685,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'vatrp',
                        id: 'vatrpsalesid',
                        ref: '../vatrp',
                        maxLength: 30,
                        value: 0,
                        x: 765,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 685,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalsalesid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 765,
                        y: 452
                    },
                    {
                        xtype: "hidden",
                        id: 'tot_pot1salesid',
                        value: 0
                    },
                    {
                        xtype: "hidden",
                        id: 'tot_pot2salesid',
                        value: 0
                    },
                    {
                        xtype: "hidden",
                        id: 'tot_pot3salesid',
                        value: 0
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Retur',
                //     hidden: false,
                //     ref: '../btnRetur'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Botol',
                //     hidden: false,
                //     ref: '../btnBotol'
                // },
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                // {
                //     xtype: 'button',
                //     text: 'Final',
                //     ref: '../btnFinal'
                // },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalesKreditWin.superclass.initComponent.call(this);
        // this.btnRetur.on('click', this.onbtnReturclick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        // this.btnBotol.on('click', this.onbtnBotolclick, this);
        // this.btnFinal.on('click', this.onbtnFinalclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customer.on('select', this.onKonsumenChange, this);
        // this.uang_muka.on('change', this.updateTotal, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1) { //edit
            this.btnSave.setVisible(true);
            this.btnCancel.setText('Batal');
        } else if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnCancel.setText('Batal');
        } else { //after audit
            this.btnSave.setVisible(false);
            this.btnCancel.setText('Tutup');
        }
    },
    onKonsumenChange: function (c, r, i) {
        this.tempo.setValue(r.data.address);
        this.tempo.setValue(this.tgl.getValue().add(Date.DAY, r.data.term));
        this.salesspv.setValue(r.data.spv_id);
        this.sales.setValue(r.data.salesman_id);
        this.alamat.setValue(r.data.address);
    },
    onWinClose: function () {
        jun.rztSalesDetail.removeAll();
    },
    updateTotal: function () {
        jun.rztSalesDetail.refreshData();
    },
    btnDisabled: function (status) {
        // this.btnRetur.setDisabled(status);
        this.btnSave.setDisabled(status);
        // this.btnBotol.setDisabled(status);
        // this.btnFinal.setDisabled(status);
        this.btnCancel.setDisabled(status);
    },
    onbtnFinalclick: function () {
        this.modez = 2;
        this.saveForm(true);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Sales/create/';
        Ext.getCmp('form-Penjualan').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztSalesDetail.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztSalesKredit.reload();
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('form-Penjualan').getForm().reset();
                this.btnDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ReturSalesWin = Ext.extend(Ext.Window, {
    title: 'Retur Penjualan',
    modez: 1,
    width: 1005,
    height: 560,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ReturPenjualan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No Faktur:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        value: DATE_NOW,
                        // readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Tempo:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tempo',
                        fieldLabel: 'tempo',
                        name: 'tempo',
                        id: 'tempoid',
                        format: 'd M Y',
                        width: 175,
//                        readOnly: true,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode_customer',
                            'nama_customer'
                        ],
                        mode: 'local',
                        store: jun.rztCustomersCmp,
                        valueField: 'customer_id',
                        hiddenName: 'customer_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Customer</th><th>Nama Customer</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_customer}</td><td>{nama_customer}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../customer',
                        displayField: 'kode_customer',
                        allowBlank: false,
                        width: 200,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Sales:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'salesman_code',
                            'salesman_name'
                        ],
                        mode: 'local',
                        store: jun.rztSalesmanCmp,
                        valueField: 'salesman_id',
                        hiddenName: 'salesman_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Salesman</th><th>Nama Salesman</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{salesman_code}</td><td>{salesman_name}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../sales',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        width: 200,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Sales SPV:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'salesman_code',
                            'salesman_name'
                        ],
                        mode: 'local',
                        store: jun.rztSalesmanCmp,
                        valueField: 'salesman_id',
                        hiddenName: 'salesspv_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Salesman</th><th>Nama Salesman</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{salesman_code}</td><td>{salesman_name}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../salesspv',
                        displayField: 'salesman_name',
                        allowBlank: false,
                        width: 200,
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Alamat:",
                        x: 585,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        ref: '../alamat',
                        name: 'alamat',
                        id: 'alamatid',
                        height: "80",
                        width: "215",
                        readOnly: true,
                        // x: 375,
                        x: 665,
                        y: 2
                    },
                    new jun.ReturSalesDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 315,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 425
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'sub_total',
//                        id: 'sub_totalid',
//                        ref: '../sub_total',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 422
//                    },
//                    {
//                        xtype: "label",
//                        text: "PPN:",
//                        x: 295,
//                        y: 455
//                    },
//                    {
//                        xtype: 'numericfield',
//                        name: 'ppn',
//                        id: 'ppnid',
//                        ref: '../ppn',
//                        maxLength: 30,
//                        readOnly: true,
//                        value: 0,
//                        x: 375,
//                        y: 452
//                    },
                    {
                        xtype: "label",
                        text: "Bruto:",
                        x: 355,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'bruto',
                        id: 'brutosalesid',
                        ref: '../bruto',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 435,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Diskon:",
                        x: 355,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'totalpot',
                        id: 'totalpotsalesid',
                        ref: '../totalpot',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 435,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "VAT:",
                        x: 685,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        name: 'vatrp',
                        id: 'vatrpsalesid',
                        ref: '../vatrp',
                        maxLength: 30,
                        value: 0,
                        x: 765,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 685,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        name: 'total',
                        id: 'totalsalesid',
                        ref: '../total',
                        maxLength: 30,
                        readOnly: true,
                        value: 0,
                        x: 765,
                        y: 452
                    },
                    {
                        xtype: "hidden",
                        id: 'tot_pot1salesid',
                        value: 0
                    },
                    {
                        xtype: "hidden",
                        id: 'tot_pot2salesid',
                        value: 0
                    },
                    {
                        xtype: "hidden",
                        id: 'tot_pot3salesid',
                        value: 0
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturSalesWin.superclass.initComponent.call(this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customer.on('select', this.onKonsumenChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1) { //edit
            this.btnSave.setVisible(true);
            this.btnCancel.setText('Batal');
        } else if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnCancel.setText('Batal');
        } else { //after audit
            this.btnSave.setVisible(false);
            this.btnCancel.setText('Tutup');
        }
    },
    onKonsumenChange: function (c, r, i) {
        this.tempo.setValue(r.data.address);
        this.tempo.setValue(this.tgl.getValue().add(Date.DAY, r.data.term));
        this.salesspv.setValue(r.data.spv_id);
        this.sales.setValue(r.data.salesman_id);
        this.alamat.setValue(r.data.address);
    },
    onWinClose: function () {
        jun.rztReturSalesDetail.removeAll();
    },
    updateTotal: function () {
        jun.rztReturSalesDetail.refreshData();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnCancel.setDisabled(status);
    },
    onbtnFinalclick: function () {
        this.modez = 2;
        this.saveForm(true);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'ReturSales/create/';
        Ext.getCmp('form-ReturPenjualan').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztReturSalesDetail.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztReturSales.reload();
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: a.result.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp('form-ReturPenjualan').getForm().reset();
                this.btnDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});