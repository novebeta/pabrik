jun.BreakdownGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Breakdown",
    folder_id: 1,
    id: 'docs-jun.BreakdownGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'void',
            width: 50,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case 0 :
                        return '';
                    case 1 :
                        metaData.style += "background-color: #ff0000;";
                        return 'VOID';
                }
            }
            // ,
            // filter: {
            //     xtype: "combo",
            //     typeAhead: true,
            //     triggerAction: 'all',
            //     lazyRender: true,
            //     editable: false,
            //     mode: 'local',
            //     store: new Ext.data.ArrayStore({
            //         id: 0,
            //         fields: [
            //             'myId',
            //             'displayText'
            //         ],
            //         data: [['all', 'ALL'], [0, 'NOT POSTED'], [1, 'POSTED']]
            //     }),
            //     value: 'all',
            //     valueField: 'myId',
            //     displayField: 'displayText'
            // }
        }
    ],
    initComponent: function () {
        this.store = jun.rztBreakdown;
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        if (jun.rztWipCmp.getTotalCount() === 0) {
            jun.rztWipCmp.load();
        }
        if (jun.rztWipLib.getTotalCount() === 0) {
            jun.rztWipLib.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Breakdown',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Breakdown',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Void',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Breakdown',
                    ref: '../btnPrint'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportBreakdown",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "breakdown_id",
                            ref: "../../breakdown_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.BreakdownGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.btnprintClick, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BreakdownWin({
            modez: 0,
            folder_id: this.folder_id
        });
        form.folder.setValue(this.folder_id);
        form.show();
        console.log(form);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.breakdown_id;
        var form = new jun.BreakdownWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztBreakdownDetails.baseParams = {
            breakdown_id: idz
        };
        jun.rztBreakdownDetails.load();
        jun.rztBreakdownDetails.baseParams = {};
    },
    btnprintClick: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Breakdown");
            return;
        }
        // var idz = selectedz.json.breakdown_id;
        Ext.getCmp("form-ReportBreakdown").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBreakdown").getForm().url = "Report/PrintBreakDown";
        this.breakdown_id.setValue(selectedz.json.breakdown_id);
        var form = Ext.getCmp('form-ReportBreakdown').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin void data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih breakdown");
            return;
        }
        Ext.Ajax.request({
            url: 'Breakdown/delete/id/' + record.json.breakdown_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBreakdown.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
