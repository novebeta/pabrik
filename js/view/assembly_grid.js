jun.AssemblyGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Assembly",
    id: 'docs-jun.AssemblyGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 75
        },
        {
            header: 'Material',
            sortable: true,
            resizable: true,
            dataIndex: 'material_id',
            renderer: jun.renderNamaMaterial
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_'
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'void',
            width: 50,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case 0 :
                        return '';
                    case 1 :
                        metaData.style += "background-color: #ff0000;";
                        return 'VOID';
                }
            }
            // ,
            // filter: {
            //     xtype: "combo",
            //     typeAhead: true,
            //     triggerAction: 'all',
            //     lazyRender: true,
            //     editable: false,
            //     mode: 'local',
            //     store: new Ext.data.ArrayStore({
            //         id: 0,
            //         fields: [
            //             'myId',
            //             'displayText'
            //         ],
            //         data: [['all', 'ALL'], [0, 'NOT POSTED'], [1, 'POSTED']]
            //     }),
            //     value: 'all',
            //     valueField: 'myId',
            //     displayField: 'displayText'
            // }
        }
    ],
    initComponent: function () {
        this.store = jun.rztAssembly;
        if (jun.rztStorageLocationCmp.getTotalCount() === 0) {
            jun.rztStorageLocationCmp.load();
        }
        // if (jun.rztWipCmp.getTotalCount() === 0) {
        //     jun.rztWipCmp.load();
        // }
        // if (jun.rztWipLib.getTotalCount() === 0) {
        //     jun.rztWipLib.load();
        // }
        if (jun.rztMaterialCmp.getTotalCount() === 0) {
            jun.rztMaterialCmp.load();
        }
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Assembly',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat Assembly',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Void',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Assembly',
                    ref: '../btnPrint'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportAssembly",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "assembly_id",
                            ref: "../../assembly_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        jun.AssemblyGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.btnprintClick, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnprintClick: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Assembly");
            return;
        }
        // var idz = selectedz.json.breakdown_id;
        Ext.getCmp("form-ReportAssembly").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssembly").getForm().url = "Report/PrintAssembly";
        this.assembly_id.setValue(selectedz.json.assembly_id);
        var form = Ext.getCmp('form-ReportAssembly').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    loadForm: function () {
        var form = new jun.AssemblyWin({
            modez: 0,
            folder_id: this.folder_id
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.assembly_id;
        var form = new jun.AssemblyWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztAssemblyDetails.baseParams = {
            assembly_id: idz
        };
        jun.rztAssemblyDetails.load();
        jun.rztAssemblyDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin void data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.data.void != 0) {
            Ext.MessageBox.alert("Warning", "Assembly sudah divoid.");
            return;
        }
        Ext.Ajax.request({
            url: 'Assembly/delete/id/' + record.json.assembly_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztAssembly.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
