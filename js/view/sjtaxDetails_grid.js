jun.SjtaxDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SjtaxDetails",
    id: 'docs-jun.SjtaxDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'sjtax_details_id',
            sortable: true,
            resizable: true,
            dataIndex: 'sjtax_details_id',
            width: 100
        },
        {
            header: 'qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100
        },
        {
            header: 'price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100
        },
        {
            header: 'total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        },
        {
            header: 'barang_id',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100
        },
        {
            header: 'sjtax_id',
            sortable: true,
            resizable: true,
            dataIndex: 'sjtax_id',
            width: 100
        }
        /*
         {
         header:'batch',
         sortable:true,
         resizable:true,
         dataIndex:'batch',
         width:100
         },
         {
         header:'tgl_exp',
         sortable:true,
         resizable:true,
         dataIndex:'tgl_exp',
         width:100
         },
         {
         header:'ket',
         sortable:true,
         resizable:true,
         dataIndex:'ket',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztSjtaxDetails;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.SjtaxDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SjtaxDetailsWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sjtax_details_id;
        var form = new jun.SjtaxDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'SjtaxDetails/delete/id/' + record.json.sjtax_details_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSjtaxDetails.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
