jun.TransferBarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Barang",
    id: 'docs-jun.TransferBarangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: '',
            sortable: true,
            resizable: true,
            dataIndex: 'pengirim',
            width: 8,
            renderer: function (value, metaData, record, rowIndex) {
                if (record.get('pengirim') == UROLE) {
                    (metaData.style += "background-color: #FFD4FF;");
                    metaData.css += ' icon-red_arrow_right ';
                }
                if (record.get('penerima') == UROLE) {
                    (metaData.style += "background-color: #CCFFCC;");
                    metaData.css += ' icon-green_arrow_left ';
                }
                return "";
            }
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 45,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Nomor',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 60
        },
        {
            header: 'Pengirim',
            sortable: true,
            resizable: true,
            dataIndex: 'pengirim',
            width: 40,
            renderer: jun.renderStorageLocationName
        },
        {
            header: 'Penerima',
            sortable: true,
            resizable: true,
            dataIndex: 'penerima',
            width: 40,
            renderer: jun.renderStorageLocationName
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 120
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'accepted',
            width: 20,
            renderer: function (value, metaData, record, rowIndex) {
                value && (metaData.css += ' silk13-tick ');
                return '';
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actPrint, actAccept) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
        this.actAccept = actAccept;
    },
    initComponent: function () {
        this.store = jun.rztTransferBarang;
        if (jun.rztBarangCmp.getTotalCount() === 0) jun.rztBarangCmp.load();
        if (jun.rztTipeBarangCmp.getTotalCount() === 0) jun.rztTipeBarangCmp.load();
        if (jun.rztGrupCmp.getTotalCount() === 0) jun.rztGrupCmp.load();
        this.userPermission(1, 1, 1, 1);
        // switch(UROLE){
        //     case USER_ADMIN:        this.userPermission(0,0,1,1); break;
        //     case USER_GPJ:          this.userPermission(1,1,1,1); break;
        //     case USER_PPIC:         this.userPermission(0,0,0,0); break;
        //     case USER_GA:           this.userPermission(0,0,0,0); break;
        //     case USER_PURCHASING:   this.userPermission(0,0,0,0); break;
        //     case USER_PEMBELIAN:    this.userPermission(0,0,0,0); break;
        //     case USER_RND:          this.userPermission(0,0,0,0); break;
        //     case USER_PRODUKSI:     this.userPermission(1,1,1,1); break;
        //     default:                this.userPermission(0,0,0,0);
        // }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Kirim Barang',
                    ref: '../btnAdd',
                    iconCls: 'icon-red_arrow_right',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Terima',
                    ref: '../btnAccept',
                    iconCls: 'silk13-tick',
                    hidden: this.actAccept ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actAccept || this.actPrint) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>All</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TransferBarangGrid').setFilterData(m.status);
                                    }
                                }
                            },
                            {
                                text: 'BELUM DITERIMA',
                                status: 0,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TransferBarangGrid').setFilterData(m.status);
                                    }
                                }
                            },
                            {
                                text: 'DITERIMA',
                                status: 1,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TransferBarangGrid').setFilterData(m.status);
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-TransferBarangGrid",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "transfer_barang_id",
                            ref: "../../transfer_barang_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.TransferBarangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.btnPrintOnClick, this);
        this.btnAccept.on('Click', this.btnAcceptOnClick, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData('all');
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.actAccept && this.btnAccept.setDisabled(!(!r.get('accepted')));
        // this.actPrint && this.btnPrint.setDisabled(!(r.get('pengirim') === UROLE));
        // if (this.actEdit) {
        //     var filterEdit = (!parseInt(r.get('accepted')) || storloc[parseInt(r.get('penerima'))].havestock == 0) && this.actEdit && parseInt(r.get('pengirim')) == UROLE;
        //     this.btnEdit.setIconClass(filterEdit ? 'silk13-pencil' : 'silk13-eye');
        //     this.btnEdit.setText(filterEdit ? "Ubah" : "Lihat");
        // }
    },
    setFilterData: function (sts) {
        this.status = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        this.store.baseParams = {
            mode: "grid",
            status: this.status,
            urole: UROLE
        };
        this.store.reload();
    },
    loadForm: function () {
        var form = new jun.TransferBarangWin({
            modez: 0,
            title: 'Buat Transfer Barang',
            iconCls: 'icon-red_arrow_right',
            accepting: false
        });
        form.show(this);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        this.accepting = false;
        this.showWinEditForm();
    },
    showWinEditForm: function () {
        var selectedz = this.sm.getSelected().json;
        var idz = selectedz.transfer_barang_id;
        // var filterEdit = (!parseInt(selectedz.accepted) || storloc[parseInt(selectedz.penerima)].havestock == 0) && this.actEdit && parseInt(selectedz.pengirim) == UROLE;
        var form = new jun.TransferBarangWin({
            modez: this.accepting ? 2 : (filterEdit ? 1 : 2),
            trans_id: idz,
            title: (this.accepting ? "Terima" : (filterEdit ? "Ubah" : "View")) + " Transfer Barang",
            iconCls: (this.accepting ? 'silk13-tick' : (filterEdit ? 'silk13-pencil' : 'silk13-eye')),
            accepting: this.accepting
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.griddetils.store.load({
            params: {
                transfer_barang_id: idz
            }
        });
    },
    btnPrintOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        Ext.getCmp("form-TransferBarangGrid").getForm().standardSubmit = !0;
        Ext.getCmp("form-TransferBarangGrid").getForm().url = "Report/PrintTransferBarang";
        this.transfer_barang_id.setValue(selectedz.json.transfer_barang_id);
        var form = Ext.getCmp('form-TransferBarangGrid').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnAcceptOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        this.accepting = true;
        this.showWinEditForm();
    }
});
