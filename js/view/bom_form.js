jun.BomWin = Ext.extend(Ext.Window, {
    title: 'Bill Of Material',
    modez: 1,
    id: '',
    width: 900,
    height: 480,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Bom',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. BOM:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'no_bom',
                        ref: '../no_bom',
                        allowBlank: false,
                        width: 175 + 50,
                        x: 90,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        value: new Date(),
                        x: 90,
                        y: 32,
                        allowBlank: false,
                        width: 175 + 50
                    },
                    {
                        xtype: "label",
                        text: "Bentuk sediaan:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'bentuk_sediaan',
                        ref: '../bentuk_sediaan',
                        width: 175 + 50,
                        x: 90,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Kode Produk:",
                        x: 295 + 45,
                        y: 5
                    },
                    {
                        xtype: 'mfcombobox',
                        style: 'margin-bottom:2px',
                        searchFields: [
                            'kode_material',
                            'nama_material'
                        ],
                        mode: 'local',
                        store: jun.rztMaterialCmp,
                        hiddenName: 'material_id',
                        valueField: 'material_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Material</th><th>Nama Material</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_material}</td><td>{nama_material}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        width: 175 + 50,
                        x: 400 + 35,
                        y: 2,
                        allowBlank: false,
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../material',
                        displayField: 'kode_material'
                    },
                    {
                        xtype: "label",
                        text: "Kode Formula:",
                        x: 295 + 45,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'kode_formula',
                        ref: '../kode_formula',
                        width: 175 + 50,
                        x: 400 + 35,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Quantity :",
                        x: 295 + 45,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        allowBlank: false,
                        name: 'qty',
                        ref: '../qty',
                        width: 100,
                        minValue: 1,
                        value: 1,
                        x: 400 + 35,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "pcs",
                        ref: '../satuan',
                        x: 400 + 35 + 105,
                        y: 65
                    },
                    {
                        xtype: 'checkbox',
                        boxLabel: 'Aktif',
                        name: 'enable',
                        ref: '../enable',
                        x: 400 + 35 + 105 + 200,
                        y: 65
                    },
//                    {
//                        xtype: 'hidden',
//                        ref: '../sat'
//                    },
                    new jun.BomDetailGrid({
                        x: 5,
                        y: 95,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetil",
                        anchor: '100% 100%'
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BomWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.kode_barang_rnd.on('select', this.onSelectBarang, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    onSelectBarang: function (c, r, i) {
        this.satuan.setText(r.get('sat') + " @ " + r.get('qty_per_pot') + " gr");
        //this.sat.setValue(r.get('sat'));
    },
    onWinClose: function () {
        this.griddetil.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-Bom').getForm().submit({
            url: 'bom/create/',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(this.griddetil.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztBom.reload();
                if (jun.rztBomLib.getTotalCount() !== 0) jun.rztBomLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Bom').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});