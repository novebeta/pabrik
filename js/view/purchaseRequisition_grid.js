jun.PurchaseRequisitionGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Request",
    id: 'docs-jun.PurchaseRequisitionGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. PR',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 80
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50
        },
        {
            header: 'Pemohon',
            sortable: true,
            resizable: true,
            dataIndex: 'pemohon',
            width: 120
        },
        {
            header: 'Tgl Diperlukan',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_required',
            width: 50
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 60,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case P0_OPEN :
                        return 'OPEN';
                    case PR_PROCESS :
                        return 'PROCESS';
                    case PR_REJECTED :
                        return 'REJECTED [' + record.get('reject_no_ba') + ']';
                    case PR_CLOSED :
                        return 'CLOSED';
                }
            }
        },
        {
            header: 'Status PO',
            sortable: true,
            resizable: true,
            dataIndex: 'status_po',
            width: 40,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case P0_OPEN :
                        return 'OPEN';
                    case P0_RELEASED :
                        return 'RELEASED';
                    case PO_PARTIALLY_RECEIVED :
                        return 'PARTIALLY RECEIVED';
                    case PO_RECEIVED :
                        return 'RECEIVED';
                    //case PO_INVOICED : return 'INVOICED';
                    case PO_CANCELED :
                        return 'CANCELED';
                    case PO_CLOSED :
                        return 'CLOSED';
                    default :
                        return '';
                }
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actPrint, actCancel, actReject, actCreatePO) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actPrint = actPrint;
        this.actCancel = actCancel;
        this.actReject = actReject;
        this.actCreatePO = actCreatePO;
        //-- jika yang buka purchasing maka akan menampilkan semua PR
        //-- jika yang buka bukan purchasing maka hanya PR miliknya yang akan ditampilkan
        this.viewAllData = (this.actCreatePO || this.actReject);
    },
    initComponent: function () {
        if (jun.rztMaterialLib.getTotalCount() === 0) {
            jun.rztMaterialLib.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1, 0, 1, 1);
                break;
            case USER_PPIC:
                this.userPermission(1, 1, 1, 0, 0, 0);
                break;
            case USER_GA:
                this.userPermission(1, 1, 1, 0, 0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0, 0, 0, 1, 1);
                break;
            case USER_PEMBELIAN:
                this.userPermission(0, 0, 0, 0, 0, 0);
                break;
            default:
                this.userPermission(0, 0, 0, 0, 0, 0);
        }
        this.store = jun.rztPurchaseRequisition;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat PR',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true,
                    menu: {
                        xtype: 'menu',
                        ref: '../../menuAdd',
                        id: 'menuAddPRid',
                        items: []
                    }
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Print FPPB',
                    ref: '../btnPrintPR',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actPrint ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Batalkan PR',
                    ref: '../btnCancel',
                    hidden: this.actCancel ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actPrint || this.actCancel) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Reject PR',
                    ref: '../btnReject',
                    iconCls: 'silk13-cross',
                    hidden: this.actReject ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Buat PO',
                    ref: '../btnBuatPO',
                    iconCls: 'silk13-tick',
                    hidden: this.actCreatePO ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actReject || this.actCreatePO) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_pr: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseRequisitionGrid').setFilterData(m.status_pr);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_pr: PR_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseRequisitionGrid').setFilterData(m.status_pr);
                                    }
                                }
                            },
                            {
                                text: 'PROCESS',
                                status_pr: PR_PROCESS,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseRequisitionGrid').setFilterData(m.status_pr);
                                    }
                                }
                            },
                            {
                                text: 'REJECTED',
                                status_pr: PR_REJECTED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseRequisitionGrid').setFilterData(m.status_pr);
                                    }
                                }
                            },
                            {
                                text: 'CLOSED',
                                status_pr: PR_CLOSED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.PurchaseRequisitionGrid').setFilterData(m.status_pr);
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportPurchaseRequisition",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "pr_id",
                            ref: "../../pr_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tipeMaterialStore = new jun.TipeMaterialstore();
        this.tipeMaterialStore.load({
            params: {purchasable: 1},
            callback: this.addMenuPR,
            scope: this
        });
        jun.PurchaseRequisitionGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.btnEditOnClick, this);
        this.btnPrintPR.on('Click', this.btnPrintPROnClick, this);
        this.btnCancel.on('Click', this.btnCancelOnClick, this);
        this.btnReject.on('Click', this.btnRejectOnClick, this);
        this.btnBuatPO.on('Click', this.btnBuatPOonClick, this);
        this.on('rowdblclick', this.btnEditOnClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(PR_OPEN);
    },
    addMenuPR: function () {
        this.tipeMaterialStore.each(function (r) {
            Ext.getCmp('menuAddPRid').add({
                text: r.data.nama_tipe_material,
                nama_tipe_material: r.data.nama_tipe_material,
                tipe_material_id: r.data.tipe_material_id,
                listeners: {
                    click: function (m, e) {
                        Ext.getCmp('docs-jun.PurchaseRequisitionGrid').addPR(m.tipe_material_id, this.nama_tipe_material);
                    }
                }
            });
        });
    },
    setFilterData: function (sts) {
        //-- fungsi untuk filter grid PR
        this.status_pr = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        if (this.store.baseParams.status && this.store.baseParams.status == this.status_pr) {
            this.store.baseParams = {
                mode: "grid",
                status: this.status_pr,
                urole: (this.viewAllData ? 'all' : UROLE)
            };
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                mode: "grid",
                status: this.status_pr,
                urole: (this.viewAllData ? 'all' : UROLE)
            };
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (this.actEdit) {
            var filter = r.get('adt') == "0" && r.get('status') == PR_OPEN && r.get('id_user') == UID;
            this.btnEdit.setIconClass(filter ? 'silk13-pencil' : 'silk13-eye');
            this.btnEdit.setText(filter ? "Ubah" : "Lihat");
        }
    },
    addPR: function (tipe, nama) {
        var form = new jun.PurchaseRequisitionWin({
            modez: 0,
            tipe_material_id: tipe,
            title: "Buat Purchase Request - " + nama,
            iconCls: 'silk13-add'
        });
        form.show();
    },
    btnEditOnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data PR");
            return;
        }
        var filter = selectedz.json.adt == "0" && selectedz.json.status == PR_OPEN && selectedz.json.id_user == UID && this.actEdit;
        var ttl = "";
        switch (Number(selectedz.json.status)) {
            case PR_OPEN :
                ttl = "OPEN";
                break;
            case PR_PROCESS :
                ttl = "PROCESS";
                break;
            case PR_REJECTED :
                ttl = "REJECTED";
                break;
            case PR_CLOSED :
                ttl = "CLOSED";
                break;
        }
        var form = new jun.PurchaseRequisitionWin({
            modez: ( filter ? 1 : 2),
            pr_id: selectedz.json.pr_id,
            tipe_material_id: selectedz.json.tipe_material_id,
            title: ( filter ? "Ubah" : "Lihat") + " Purchase Request  [" + ttl + (Number(selectedz.json.status) == PR_REJECTED ? " - <i>No. BA : " + selectedz.json.reject_no_ba + "</i> " : "") + "]",
            iconCls: filter ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPurchaseRequisitionDetails.baseParams = {pr_id: selectedz.json.pr_id};
        jun.rztPurchaseRequisitionDetails.load();
        jun.rztPurchaseRequisitionDetails.baseParams = {};
    },
    btnPrintPROnClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data PR.");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status != PR_OPEN) {
            var msg = "";
            switch (status) {
                case PR_PROCESS :
                    msg = "PR sedang diproses.";
                    break;
                case PR_REJECTED :
                    msg = "PR ini telah ditolak.";
                    break;
                case PO_CLOSED :
                    msg = "PR ini telah ditutup.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "Tidak dapat mengunduh dokumen FPPB.<br>" + msg);
            return;
        }
        Ext.getCmp("form-ReportPurchaseRequisition").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchaseRequisition").getForm().url = "Report/PrintPurchaseRequisition";
        this.pr_id.setValue(selectedz.json.pr_id);
        var form = Ext.getCmp('form-ReportPurchaseRequisition').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnCancelOnClick: function () {
        //-- fungsi untuk membatalkan PR
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data PR");
            return;
        }
        if (this.sm.getSelected().json.id_user != UID) {
            //-- PR hanya dapat dibatalkan oleh user pembuatnya
            Ext.MessageBox.alert("Warning", "Anda tidak diperbolehkan untuk membatalkan PR ini.");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status != PR_OPEN) {
            //-- hanya PR berstatus OPEN yang dapat dibatalkan
            var msg = "";
            switch (status) {
                case PR_CLOSED :
                    msg = "Purchase Order untuk PR ini telah dibuat.";
                    break;
                case PR_PROCESS :
                    msg = "PR sedang diproses.";
                    break;
                case PR_REJECTED :
                    msg = "PR status REJECTED.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "PR tidak dapat dibatalkan.<br>" + msg);
            return;
        }
        Ext.MessageBox.confirm('Cancel PR', 'Apakah anda yakin ingin membatalkan PR ini?', this.cancelPR, this);
    },
    cancelPR: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'PurchaseRequisition/Cancel/id/' + record.json.pr_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPurchaseRequisition.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnRejectOnClick: function () {
        //-- fungsi untuk men-reject PR
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data PR");
            return;
        }
        if (!this.actReject) {
            //-- hanya user purchasing yang dapat menolak PR
            Ext.MessageBox.alert("Warning", "Anda tidak diperbolehkan untuk me-reject PR.");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status != PR_OPEN) {
            //-- hanya PR berstatus OPEN yang dapat di-reject
            var msg = "";
            switch (status) {
                case PR_CLOSED :
                    msg = "Purchase Order untuk PR ini telah dibuat.";
                    break;
                case PR_PROCESS :
                    msg = "PR sedang diproses.";
                    break;
                case PR_REJECTED :
                    msg = "PR status REJECTED.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "PR tidak dapat di-reject.<br>" + msg);
            return;
        }
        Ext.MessageBox.confirm('Reject PR', 'Apakah anda yakin ingin menolak PR ini?', this.rejectPR, this);
    },
    rejectPR: function (btn) {
        if (btn == 'no') {
            return;
        }
        var selectedz = this.sm.getSelected();
        var form = new jun.PurchaseRequisitionWin({
            modez: 1,
            pr_id: selectedz.json.pr_id,
            tipe_material_id: selectedz.json.tipe_material_id,
            title: "Reject Purchase Requisition",
            rejectPR: true,
            iconCls: 'silk13-cross'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPurchaseRequisitionDetails.baseParams = {pr_id: selectedz.json.pr_id};
        jun.rztPurchaseRequisitionDetails.load();
        jun.rztPurchaseRequisitionDetails.baseParams = {};
    },
    btnBuatPOonClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data PR");
            return;
        }
        if (!this.actCreatePO) {
            //-- hanya user purchasing yang dapat membuat PO
            Ext.MessageBox.alert("Warning", "Anda tidak dapat membuat PO.");
            return;
        }
        var status = Number(this.sm.getSelected().json.status);
        if (status != PR_OPEN && status != PR_PROCESS) {
            //-- hanya PR status OPEN yang dapat dibuat PO
            var msg = "";
            switch (status) {
                case PR_CLOSED :
                    msg = "Purchase Order untuk PR ini telah dibuat.";
                    break;
                case PR_REJECTED :
                    msg = "PR telah ditolak.";
                    break;
            }
            Ext.MessageBox.alert("Warning", "Tidak dapat membuat Purchase Order untuk PR ini.<br>" + msg);
            return;
        }
        this.createPO();
    },
    createPO: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data PR.");
            return;
        }
        var r = this.sm.getSelected().json;
        var form = new jun.PurchaseOrderWin({
            modez: 0,
            tipe_material_id: r.tipe_material_id,
            title: "Buat Purchase Order",
            storeDetail: jun.rztCreatePODetails,
            pr_id: r.pr_id,
            iconCls: 'silk13-tick'
        });
        form.show(this);
        //form.formz.getForm().loadRecord(this.record);
        form.pr_doc_ref.setValue(this.record.get('doc_ref'));
        form.attn_name.setValue(this.record.get('pemohon'));
        form.tgl_delivery.setValue(this.record.get('tgl_required'));
        //form.cmbSupplier.setRawValue(this.record.get('vendor_name'));
        //form.supplier_cp.setValue(this.record.get('vendor_contact'));
        form.griddetils.store.baseParams = {pr_id: r.pr_id};
        form.griddetils.store.load();
        form.griddetils.store.on({
            scope: this,
            load: {
                fn: function () {
                    jun.rztCreatePODetails.refreshData();
                }
            }
        });
        form.griddetils.store.baseParams = {};
    }
});
