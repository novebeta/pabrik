jun.PurchaseOrderDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchaseOrderDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchaseOrderDetailsStoreId',
            url: 'PurchaseOrderDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_detail_id'},
                {name: 'po_id'},
                {name: 'material_id'},
                {name: 'qty', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'disc_rp', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'note'},
                {name: 'sat'},
                {name: 'qty_to_purchase'} //tidak disimpan di database, data sementara untuk tanda
            ]
        }, cfg));
    },
    refreshData: function () {
        var total = this.sum("total");
        var disc_rp = this.sum("disc_rp");
        var bruto = this.sum("bruto");
        var vatrp = this.sum("vatrp");
        Ext.getCmp('form-PurchaseOrder-total').setValue(bruto);
        Ext.getCmp('form-PurchaseOrder-discount').setValue(disc_rp);
        // var disc = parseFloat(Ext.getCmp('form-PurchaseOrder-discount').getValue());
        // var persen_tax = parseFloat(Ext.getCmp('form-PurchaseOrder-persen_tax').getValue())/100;
        // var dpp = total - disc;
        Ext.getCmp('form-PurchaseOrder-dpp').setValue(bruto - disc_rp);
        Ext.getCmp('form-PurchaseOrder-tax').setValue(vatrp);
        Ext.getCmp('form-PurchaseOrder-grand_total').setValue(total);
    }
});
jun.rztPurchaseOrderDetails = new jun.PurchaseOrderDetailsstore();
jun.rztCreatePODetails = new jun.PurchaseOrderDetailsstore({url: 'PurchaseRequisitionDetails/GetItemsForPO'});
jun.ComboMaterialCreatePOstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchaseOrderDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ComboMaterialCreatePOstoreId',
            url: 'PurchaseRequisitionDetails/GetMaterialForPO',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'material_id'},
                {name: 'sat'},
                {name: 'dimension'},
                {name: 'qty', type: 'float'},
                {name: 'qty_purchased', type: 'float'},
                {name: 'kode_material'},
                {name: 'nama_material'}
            ]
        }, cfg));
    }
});
jun.rztComboMaterialCreatePO = new jun.ComboMaterialCreatePOstore();
