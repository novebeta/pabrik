jun.GiroWin = Ext.extend(Ext.Window, {
    title: 'Giro',
    modez: 1,
    width: 400,
    height: 250,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Giro',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'mfcombobox',
                        fieldLabel: 'Customers',
                        searchFields: [
                            'kode_customer',
                            'nama_customer'
                        ],
                        emptyText: 'All',
                        mode: 'local',
                        store: jun.rztCustomersCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate(
                            '<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode Customer</th><th>Nama Customer</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode_customer}</td><td>{nama_customer}</td>',
                            '</tr></tpl></tbody></table>'
                        ),
                        matchFieldWidth: !1,
                        allowBlank: true,
                        enableKeyEvents: true,
                        listWidth: 400,
                        ref: '../customer',
                        displayField: 'kode_customer',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Giro',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_giro',
                        id: 'no_giroid',
                        ref: '../no_giro',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_klr',
                        fieldLabel: 'Tgl Klr',
                        name: 'tgl_klr',
                        id: 'tgl_klrid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jt',
                        fieldLabel: 'Tgl Jt',
                        name: 'tgl_jt',
                        id: 'tgl_jtid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Bank Giro',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bank_giro',
                        id: 'bank_giroid',
                        ref: '../bank_giro',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Saldo',
                        hideLabel: false,
                        //hidden:true,
                        name: 'saldo',
                        id: 'saldoid',
                        ref: '../saldo',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GiroWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Giro/update/id/' + this.id;
        } else {
            urlz = 'Giro/create/';
        }
        Ext.getCmp('form-Giro').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztGiro.reload();
                jun.rztGiroCmp.reload();
                jun.rztGiroLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Giro').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});