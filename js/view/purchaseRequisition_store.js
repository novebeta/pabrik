jun.PurchaseRequisitionStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchaseRequisitionStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchaseRequisitionStoreId',
            url: 'PurchaseRequisition',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pr_id'},
                {name: 'doc_ref'},
                {name: 'pemohon'},
                {name: 'tdate'},
                {name: 'tgl_required'},
                {name: 'vendor_name'},
                {name: 'vendor_contact'},
                {name: 'status'},
                {name: 'status_po'},
                {name: 'total'},
                {name: 'note'},
                {name: 'tipe_material_id'},
                {name: 'id_user'},
                {name: 'tgl'},
                {name: 'reject_no_ba'},
                {name: 'reject_tgl'},
                {name: 'reject_id_user'},
                {name: 'reject_tdate'},
                {name: 'adt'}
            ]
        }, cfg));
    }
});
jun.rztPurchaseRequisition = new jun.PurchaseRequisitionStore();
