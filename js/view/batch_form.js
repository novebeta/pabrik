jun.BatchWin = Ext.extend(Ext.Window, {
    title: 'Production Order',
    modez: 1,
    width: 900,
    height: 480,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-Batch',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_produksi',
                        name: 'tgl_produksi',
                        format: 'd M Y',
                        x: 100,
                        y: 2,
                        allowBlank: false,
                        value: new Date(),
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Batch:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'no_batch',
                        ref: '../no_batch',
                        allowBlank: false,
                        width: 175,
                        x: 100,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Capacity :",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        allowBlank: false,
                        enableKeyEvents: true,
                        ref: '../capacity',
                        width: 110,
                        minValue: 1,
                        x: 100,
                        y: 62
                    },
                    {
                        xtype: "hidden",
                        name: 'qty',
                        ref: '../qty',
                        value: this.qty_capacity
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        editable: false,
                        allowBlank: false,
                        store: jun.rztUnitOfMeasureBerat,
                        hiddenName: 'sat',
                        valueField: 'code',
                        displayField: 'code',
                        value: 'KG',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{code}</b><br>{desc}</span>',
                            "</div></tpl>"),
                        ref: '../sat',
                        width: 60,
                        x: 140 + 105 - 30,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "No. BOM:",
                        x: 295 + 45,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: false,
                        //store: new jun.BomVsBarangstore({ baseParams: { enable: 1 } }),
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate(
                            '<tpl for="."><div class="search-item">',
                            '<span style=" font-weight: bold">{kode_barang} - {nama_barang}</span><br />Kode Formula : {kode_formula}<br />Nomor BOM : {no_bom}',
                            "</div></tpl>"
                        ),
                        listWidth: 400,
                        hiddenName: 'bom_id',
                        valueField: 'bom_id',
                        ref: '../bom_id',
                        displayField: 'no_bom',
                        width: 175,
                        x: 400 + 35,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Produk:",
                        x: 295 + 45,
                        y: 35
                    },
                    {
                        xtype: 'label',
                        ref: '../produk',
                        x: 400 + 35,
                        y: 35
                    },
                    {
                        xtype: "label",
                        text: "Kode Formula:",
                        x: 295 + 45,
                        y: 65
                    },
                    {
                        xtype: 'label',
                        ref: '../kode_formula',
                        x: 400 + 35,
                        y: 65
                    },
                    {
                        xtype: 'hidden',
                        name: 'sat_hasil',
                        ref: '../sat_hasil'
                    },
                    {
                        xtype: 'hidden',
                        name: 'barang_id',
                        ref: '../barang_id'
                    },
                    {
                        xtype: 'hidden',
                        name: 'qty_per_pot',
                        ref: '../qty_per_pot'
                    },
                    new jun.BatchDetailGrid({
                        x: 5,
                        y: 95,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetil",
                        anchor: '100% 100%',
                        //store: this.storeDetail,
                        qty_capacity: this.qty_capacity,
                        recordBOM: this.recordBOM,
                        readOnly: ((this.modez < 2) ? false : true)
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BatchWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.bom_id.on('select', this.onSelectBOM, this);
        this.capacity.on('keyup', this.onQtyChange, this);
        this.sat.on('select', this.onSelectSat, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
        this.bom_id.store = (this.modez < 2 ? jun.rztBomLibForCreateBatch : jun.rztBomLib);
        if (this.modez < 2) this.bom_id.store.load();
        //this.capacity.setValue(jun.konversiSatuanDitampilkan(this.qty_capacity, this.sat.getValue()));
    },
    onSelectBOM: function (c, r, i) {
        this.produk.setText(r.get('kode_barang') + " - " + r.get('nama_barang'));
        this.kode_formula.setText(r.get('kode_formula'));
        this.sat_hasil.setValue(r.get('sat'));
        this.barang_id.setValue(r.get('barang_id'));
        this.qty_per_pot.setValue(r.get('qty_per_pot'));
        this.griddetil.recordBOM = r;
        this.griddetil.store.load({
            params: {
                bom_id: r.get('bom_id')
            },
            callback: function () {
                this.griddetil.calculateQty(this.sat.getValue());
            },
            scope: this
        });
    },
    onQtyChange: function (t) {
        var qty_capacity = jun.konversiSatuanDisimpan(t.getValue(), this.sat.getValue());
        this.griddetil.qty_capacity = qty_capacity;
        this.qty.setValue(qty_capacity);
        if (this.griddetil.recordBOM !== null) this.griddetil.calculateQty(this.sat.getValue());
    },
    onSelectSat: function (c, r, i) {
        var qty_capacity = jun.konversiSatuanDisimpan(this.capacity.getValue(), this.sat.getValue());
        this.griddetil.qty_capacity = qty_capacity;
        this.qty.setValue(qty_capacity);
        if (this.griddetil.recordBOM !== null) this.griddetil.calculateQty(c.getValue());
    },
    onWinClose: function () {
        this.griddetil.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-Batch').getForm().submit({
            url: 'Batch/create/',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                batch_id: this.batch_id,
                detil: Ext.encode(Ext.pluck(this.griddetil.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztBatch.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Batch').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.BatchInputMixingWin = Ext.extend(Ext.Window, {
    title: 'Input Hasil Mixing',
    width: 500,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-BatchInputMixingWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label", text: "Kode Produk",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        ref: '../kode_produk',
                        x: 100,
                        y: 5
                    },
                    {
                        xtype: "label", text: "Nama Produk",
                        x: 5,
                        y: 22
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        ref: '../nama_produk',
                        x: 100,
                        y: 22
                    },
                    {
                        xtype: "label", text: "Kode Formula",
                        x: 5,
                        y: 39
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        ref: '../kode_formula',
                        x: 100,
                        y: 39
                    },
                    {
                        xtype: "label", text: "No. Batch",
                        x: 5,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        ref: '../no_batch',
                        x: 100,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Capacity",
                        x: 5,
                        y: 82
                    },
                    {
                        xtype: "label",
                        text: ": .....",
                        ref: '../qty',
                        x: 100,
                        y: 82
                    },
                    {
                        xtype: "label", text: "Tanggal Mixing",
                        x: 5,
                        y: 105 + 20
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_mixing',
                        name: 'tgl_mixing',
                        format: 'd M Y',
                        value: new Date(),
                        x: 100,
                        y: 102 + 20,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label", text: "Tanggal Expired",
                        x: 5,
                        y: 135 + 20
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_expired',
                        name: 'tgl_expired',
                        format: 'd M Y',
                        x: 100,
                        y: 132 + 20,
                        allowBlank: false,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Hasil Mixing",
                        x: 5,
                        y: 165 + 20
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        allowBlank: false,
                        ref: '../qty_mixing',
                        width: 175,
                        x: 100,
                        y: 162 + 20
                    },
                    {
                        xtype: "hidden",
                        name: 'qty_mixing',
                        ref: '../qty_mixing_hidden'
                    },
                    {
                        xtype: "label",
                        text: "gram",
                        ref: '../sat_mixing',
                        x: 277,
                        y: 165 + 20
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BatchInputMixingWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0 || this.modez == 1) {
            this.btnSaveClose.setVisible(true);
        } else { //view
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        this.qty_mixing_hidden.setValue(jun.konversiSatuanDisimpan(this.qty_mixing.getValue(), this.sat));
        Ext.getCmp('form-BatchInputMixingWin').getForm().submit({
            url: 'Batch/InputHasilMixing/id/' + this.batch_id,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                Ext.getCmp('docs-jun.BatchGrid').store.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.BatchInputHasilWin = Ext.extend(Ext.Window, {
    title: 'Hasil Produksi',
    modez: 1,
    width: 900,
    height: 480,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-BatchInputHasilWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_produksi',
                        name: 'tgl_produksi',
                        format: 'd M Y',
                        x: 100,
                        y: 2,
                        allowBlank: false,
                        value: new Date(),
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Batch:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'no_batch',
                        ref: '../no_batch',
                        allowBlank: false,
                        width: 175,
                        x: 100,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Capacity :",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        allowBlank: false,
                        enableKeyEvents: true,
                        name: 'qty',
                        ref: '../qty',
                        width: 140,
                        minValue: 1,
                        value: this.qty_capacity,
                        x: 100,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "gram",
                        ref: '../satuan',
                        x: 140 + 105,
                        y: 65
                    },
                    {
                        xtype: "label",
                        text: "No. BOM:",
                        x: 295 + 45,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: false,
                        store: jun.rztBomLib,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate(
                            '<tpl for="."><div class="search-item">',
                            '<span style=" font-weight: bold">{kode_barang} - {nama_barang}</span><br />Kode Formula : {kode_formula}<br />Nomor BOM : {no_bom}',
                            "</div></tpl>"
                        ),
                        listWidth: 400,
                        hiddenName: 'bom_id',
                        valueField: 'bom_id',
                        ref: '../bom_id',
                        displayField: 'no_bom',
                        width: 175,
                        x: 400 + 35,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Produk:",
                        x: 295 + 45,
                        y: 35
                    },
                    {
                        xtype: 'label',
                        ref: '../produk',
                        x: 400 + 35,
                        y: 35
                    },
                    {
                        xtype: "label",
                        text: "Kode Formula:",
                        x: 295 + 45,
                        y: 65
                    },
                    {
                        xtype: 'label',
                        ref: '../kode_formula',
                        x: 400 + 35,
                        y: 65
                    },
                    {
                        xtype: 'hidden',
                        name: 'sat',
                        ref: '../sat'
                    },
                    {
                        xtype: 'hidden',
                        name: 'qty_per_pot',
                        ref: '../qty_per_pot'
                    },
                    {
                        xtype: 'checkbox',
                        boxLabel: 'Final',
                        name: 'final',
                        ref: '../final',
                        x: 400 + 35 + 105 + 200,
                        y: 65
                    },
                    new jun.BatchHasilDetailGrid({
                        x: 5,
                        y: 95,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetil",
                        anchor: '100% 100%',
                        barang_id: this.barang_id,
                        recordBatch: this.recordBatch,
                        readOnly: ((this.modez < 2) ? false : true)
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BatchInputHasilWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        }
        this.formz.getForm().applyToFields({readOnly: true});
    },
    onWinClose: function () {
        this.griddetil.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-BatchInputHasilWin').getForm().submit({
            url: 'BatchHasil/create',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                batch_id: this.batch_id,
                barang_id: this.barang_id,
                detil: Ext.encode(Ext.pluck(this.griddetil.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztBatch.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BatchInputHasilWin').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        if (this.final.getValue()) {
            Ext.MessageBox.confirm(
                'Final Confirmation', "Apakah anda yakin ingin menutup Production Order ini",
                function (btn) {
                    if (btn == 'no') return;
                    this.saveForm();
                }
                , this);
        } else this.saveForm();
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        if (this.final.getValue()) {
            Ext.MessageBox.confirm(
                'Final Confirmation', "Apakah anda yakin ingin menutup Production Order ini",
                function (btn) {
                    if (btn == 'no') return;
                    this.saveForm();
                }
                , this);
        } else this.saveForm();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.BatchKonsumsiWin = Ext.extend(Ext.Window, {
    title: 'Pemakaian Bahan Baku',
    modez: 1,
    width: 900,
    height: 480,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-BatchKonsumsiWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_produksi',
                        name: 'tgl_produksi',
                        format: 'd M Y',
                        x: 100,
                        y: 2,
                        allowBlank: false,
                        value: new Date(),
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Batch:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'no_batch',
                        ref: '../no_batch',
                        allowBlank: false,
                        width: 175,
                        x: 100,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Capacity :",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        allowBlank: false,
                        enableKeyEvents: true,
                        name: 'qty',
                        ref: '../qty',
                        width: 140,
                        minValue: 1,
                        value: 1000,
                        x: 100,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "gram",
                        ref: '../sat',
                        x: 140 + 105,
                        y: 65
                    },
                    {
                        xtype: "label",
                        text: "No. BOM:",
                        x: 295 + 45,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBomLib,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate(
                            '<tpl for="."><div class="search-item">',
                            '<span style=" font-weight: bold">{kode_barang} - {nama_barang}</span><br />Kode Formula : {kode_formula}<br />Nomor BOM : {no_bom}',
                            "</div></tpl>"
                        ),
                        listWidth: 400,
                        hiddenName: 'bom_id',
                        valueField: 'bom_id',
                        ref: '../bom_id',
                        displayField: 'no_bom',
                        width: 175,
                        x: 400 + 35,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Produk:",
                        x: 295 + 45,
                        y: 35
                    },
                    {
                        xtype: 'label',
                        ref: '../produk',
                        x: 400 + 35,
                        y: 35
                    },
                    {
                        xtype: "label",
                        text: "Kode Formula:",
                        x: 295 + 45,
                        y: 65
                    },
                    {
                        xtype: 'label',
                        ref: '../kode_formula',
                        x: 400 + 35,
                        y: 65
                    },
                    new jun.BatchKonsumsiDetailGrid({
                        x: 5,
                        y: 95,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetil",
                        anchor: '100% 100%'
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tutup',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BatchKonsumsiWin.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.formz.getForm().applyToFields({readOnly: true});
    },
    onWinClose: function () {
        this.griddetil.store.removeAll();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});