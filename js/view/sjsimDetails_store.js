jun.SjsimDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SjsimDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SjsimDetailsStoreId',
            url: 'SjsimDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sjsim_details_id'},
                {name: 'qty'},
                {name: 'price'},
                {name: 'total'},
                {name: 'barang_id'},
                {name: 'sjsim_id'},
                {name: 'batch'},
                {name: 'tgl_exp'},
                {name: 'ket'}
            ]
        }, cfg));
    }
});
jun.rztSjsimDetails = new jun.SjsimDetailsstore();
//jun.rztSjsimDetails.load();
