jun.GlTransWin = Ext.extend(Ext.Window, {
    title: 'General Journal',
    modez: 1,
    width: 945,
    height: 555,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function () {
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8;padding: 10px',
                id: 'form-GlTrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: "xdatefield",
                        ref: "../tran_date",
                        name: "tran_date",
                        id: "tran_dateid",
                        format: "d M Y",
                        width: 175,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Total Debit:",
                        x: 290,
                        y: 5
                    },
                    {
                        xtype: "numericfield",
                        hideLabel: !1,
                        name: "tot_debit",
                        id: "tot_debit_id",
                        readOnly: !0,
                        ref: "../TotDebit",
                        value: 0,
                        width: 175,
                        x: 370,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Total Credit:",
                        x: 290,
                        y: 35
                    },
                    {
                        xtype: "numericfield",
                        hideLabel: !1,
                        name: "tot_kredit",
                        id: "tot_kredit_id",
                        readOnly: !0,
                        ref: "../TotKredit",
                        value: 0,
                        width: 175,
                        x: 370,
                        y: 32
                    },
                    new jun.GlTransGrid({
                        height: 405,
                        frameHeader: !1,
                        header: !1,
                        x: 5,
                        y: 65
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GlTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        //if (this.modez == 1 || this.modez == 2) {
        //    this.btnSave.setVisible(false);
        //} else {
        //    this.btnSave.setVisible(true);
        //}
    },
    btnDisabled: function (status) {
        //this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.tran_date.getValue() == "") {
            Ext.MessageBox.alert("Error", "Date cannot be blank.");
            this.btnDisabled(!1);
            return;
        }
        if (this.TotDebit.value != this.TotKredit.value) {
            Ext.MessageBox.alert("Error", "Total debit and credit must equal");
            this.btnDisabled(!1);
            return;
        }
        if (parseFloat(this.TotDebit.value) === 0 || parseFloat(this.TotKredit.value) === 0) {
            Ext.MessageBox.alert("Error", "Total debit or total credit must greater than 0.");
            this.btnDisabled(!1);
            return;
        }
        var urlz = 'GlTrans/create/';
        Ext.getCmp('form-GlTrans').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztGlTrans.data.items, "data")),
                id: this.idju,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztJurnalUmum.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-GlTrans').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});