jun.Customersstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Customersstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomersStoreId',
            url: 'Customers',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'customer_id'},
                {name: 'kode_customer'},
                {name: 'nama_customer'},
                {name: 'address'},
                {name: 'npwp'},
                {name: 'wil_int_id'},
                {name: 'wil_tax_id'},
                {name: 'nama_badan_hukum'},
                {name: 'alamat_badan_hukum'},
                {name: 'kota'},
                {name: 'term'},
                {name: 'limit_'},
                {name: 'spv_id'},
                {name: 'salesman_id'}
            ]
        }, cfg));
    }
});
jun.rztCustomers = new jun.Customersstore();
jun.rztCustomersLib = new jun.Customersstore();
jun.rztCustomersCmp = new jun.Customersstore();
jun.rztCustomersLib.load();
