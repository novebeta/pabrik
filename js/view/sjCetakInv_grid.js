jun.SjCetakInvGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Cetak Invoice",
    id: 'docs-jun.SjCetakInvGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_inv',
            width: 100
        },
        {
            header: 'No. Faktur Pajak',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur_pajak',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_inv',
            width: 100
        },
        {
            header: 'Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_id',
            width: 100,
            renderer: jun.renderCustomers
        }
    ],
    initComponent: function () {
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        if (jun.rztCustomersLib.getTotalCount() === 0) {
            jun.rztCustomersLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        jun.rztSjCetakInvoice.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglsalesinvoicegridid');
                    b.params.tgl = tgl.getValue();
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztSjCetakInvoice;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'label',
                //     style: 'margin:5px',
                //     text: 'Date :'
                // },
                // {
                //     xtype: 'xdatefield',
                //     id: 'tglsjcetakinvgridid',
                //     ref: '../tgl'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Print Invoice',
                    ref: '../btnPrintSJInt'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.DateField({
                    plugins: 'monthPickerPlugin',
                    format: 'm/Y',
                    id: 'tglsalesinvoicegridid',
                    ref: '../tgl',
                    noPastYears: false
                }),
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportSuratJalan2",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "type",
                            ref: "../../type_report"
                        },
                        {
                            xtype: "hidden",
                            name: "sj_id",
                            ref: "../../sj_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        jun.SjCetakInvGrid.superclass.initComponent.call(this);
        this.btnPrintSJInt.on('Click', this.printSj, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.selectTgl, this);
        // this.tgl.on('select', function () {
        //     this.store.reload();
        // }, this);
    },
    selectTgl: function (t, d) {
        this.store.reload();
    },
    printSj: function (btn) {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Surat jalan");
            return;
        }
        // var format_INV;
        // switch (parseInt(selectedz.json.tipe_barang)) {
        //     case BRG_PRODUK_LOKAL:
        //         format_INV = "INV_produk_lokal";
        //         break;
        //     case BRG_PRODUK_EXPORT:
        //         format_INV = "INV_produk_export";
        //         break;
        //     case BRG_EXPEDISI:
        //         format_INV = "INV_expedisi";
        //         break;
        //     case BRG_BARANGRONGSOKAN:
        //         format_INV = "INV_barang_rongsokan";
        //         break;
        //     default:
        //         Ext.MessageBox.alert("Warning", "Data yang dipilih tidak memiliki print out Invoice.");
        //         return;
        // }


        // Ext.getCmp("form-ReportSuratJalan2").getForm().standardSubmit = !0;
        // Ext.getCmp("form-ReportSuratJalan2").getForm().url = "Report/PrintInvCust";
        // this.type_report.setValue('INV_produk_lokal');
        // this.sj_id.setValue(selectedz.json.sj_id);
        // var form = Ext.getCmp('form-ReportSuratJalan2').getForm();
        // var el = form.getEl().dom;
        // var target = document.createAttribute("target");
        // target.nodeValue = "_blank";
        // el.setAttributeNode(target);
        // el.action = form.url;
        // el.submit();


        Ext.Ajax.request({
            url: 'Report/PrintInvCust',
            method: 'POST',
            scope: this,
            params: {
                sj_id: selectedz.json.sj_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (notReady()) {
                    return;
                }
                var msg = [{type: 'raw', data: Base64.decode(response.msg)}];
                print(PRINTER_RECEIPT, msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });


    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});
