jun.TerimaMaterialGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Surat Jalan Supplier",
    id: 'docs-jun.TerimaMaterialGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Surat Jalan',
            sortable: true,
            resizable: true,
            dataIndex: 'no_sj_supplier',
            width: 100
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_terima',
            width: 100
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 100,
            renderer: jun.renderNamaSuppliers
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 40,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case SJ_SUPPLIER_OPEN :
                        return 'OPEN';
                    case SJ_SUPPLIER_INVOICED :
                        return 'INVOICED';
                    case SJ_SUPPLIER_FPT :
                        return 'FPT';
                    case SJ_SUPPLIER_PAID :
                        return 'PAID';
                }
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actCreateReturn, actInputInvoice) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actView = true;
        this.actCreateReturn = actCreateReturn;
        this.actInputInvoice = actInputInvoice;
        //-- jika yang buka User Pembelian/Purchasing maka akan menampilkan semua return
        //-- jika yang buka bukan Pembelian/Purchasing maka hanya return miliknya yang akan ditampilkan
        this.viewAllData = (this.actInputInvoice || UROLE == USER_PURCHASING);
    },
    initComponent: function () {
        this.store = jun.rztTerimaMaterial;
        if (jun.rztSupplierLib.getTotalCount() === 0) jun.rztSupplierLib.load();
        if (jun.rztMaterialLib.getTotalCount() === 0) jun.rztMaterialLib.load();
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1, 1);
                break;
            case USER_PPIC:
                this.userPermission(1, 1, 1, 0);
                break;
            case USER_GA:
                this.userPermission(1, 1, 0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_PEMBELIAN:
                this.userPermission(0, 0, 0, 1);
                break;
            default:
                this.userPermission(0, 0, 0, 0);
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Penerimaan',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreate || this.actEdit || this.actView) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Return',
                    ref: '../btnReturn',
                    iconCls: 'silk13-lorry',
                    hidden: this.actCreateReturn ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Input Invoice',
                    ref: '../btnInvoiceSupplier',
                    iconCls: 'silk13-page_white_edit',
                    hidden: this.actInputInvoice ? false : true
                },
                {
                    xtype: 'tbseparator',
                    hidden: (this.actCreateReturn || this.actInputInvoice) ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>OPEN</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'All',
                                status_sj: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TerimaMaterialGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                status_sj: SJ_SUPPLIER_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TerimaMaterialGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'INVOICED',
                                status_sj: SJ_SUPPLIER_INVOICED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TerimaMaterialGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'FPT',
                                status_sj: SJ_SUPPLIER_FPT,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TerimaMaterialGrid').setFilterData(m.status_sj);
                                    }
                                }
                            },
                            {
                                text: 'PAID',
                                status_sj: SJ_SUPPLIER_PAID,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        Ext.getCmp('docs-jun.TerimaMaterialGrid').setFilterData(m.status_sj);
                                    }
                                }
                            }
                        ]
                    }
                }
            ]
        };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        jun.TerimaMaterialGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnReturn.on('Click', this.returnPembelian, this);
        this.btnInvoiceSupplier.on('Click', this.InputInvoiceSupplier, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.setFilterData(SJ_SUPPLIER_OPEN);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (this.actEdit) {
            this.btnEdit.setIconClass(r.get('status') == SJ_SUPPLIER_OPEN ? 'silk13-pencil' : 'silk13-eye');
            this.btnEdit.setText(r.get('status') == SJ_SUPPLIER_OPEN ? "Ubah" : "Lihat");
        }
    },
    setFilterData: function (sts) {
        this.status_sj = sts;
        this.reloadStore();
    },
    reloadStore: function () {
        if (this.store.baseParams.status && this.store.baseParams.status == this.status_sj) {
            this.store.baseParams = {
                mode: "grid",
                sj_only: true,
                status: this.status_sj,
                urole: (this.viewAllData ? 'all' : UROLE)
            };
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {
                mode: "grid",
                sj_only: true,
                status: this.status_sj,
                urole: (this.viewAllData ? 'all' : UROLE)
            };
            this.botbar.moveFirst();
        }
    },
    loadForm: function () {
        var form = new jun.TerimaMaterialWin({
            modez: 0,
            iconCls: 'silk13-add'
        });
        form.show(this);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Penerimaan.");
            return;
        }
        var ttl = "";
        switch (Number(selectedz.json.status)) {
            case SJ_SUPPLIER_OPEN :
                ttl = 'OPEN';
                break;
            case SJ_SUPPLIER_INVOICED :
                ttl = 'INVOICED';
                break;
            case SJ_SUPPLIER_FPT :
                ttl = 'FPT';
                break;
            case SJ_SUPPLIER_PAID :
                ttl = 'PAID';
                break;
        }
        var idz = selectedz.json.terima_material_id;
        var filterEdit = ((selectedz.json.status == SJ_SUPPLIER_OPEN || selectedz.json.status == SJ_SUPPLIER_INVOICED) && this.actEdit);
        var form = new jun.TerimaMaterialWin({
            modez: (filterEdit ? 1 : 2),
            restrictedEdit: selectedz.json.status == SJ_SUPPLIER_INVOICED ? true : false,
            id: idz,
            title: (filterEdit ? "Ubah" : "Lihat") + " Penerimaan barang [" + ttl + "]",
            iconCls: (filterEdit ? 'silk13-pencil' : 'silk13-eye')
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTerimaMaterialDetails.baseParams = {terima_material_id: idz};
        jun.rztTerimaMaterialDetails.load();
        jun.rztTerimaMaterialDetails.baseParams = {};
        if (filterEdit) {
            form.griddetils.storePO.reload({params: {supplier_id: selectedz.json.supplier_id}});
        }
    },
    returnPembelian: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Penerimaan.");
            return;
        }
        var r = this.sm.getSelected().json;
        var form = new jun.ReturnPembelianWin({
            modez: 0,
            terima_material_id: r.terima_material_id,
            title: "Buat Return Pembelian",
            storeDetail: jun.rztCreateReturnPembelianDetails,
            iconCls: 'silk13-lorry'
        });
        form.show(this);
        form.supplier_id.setValue(r.supplier_id);
        form.no_sj_supplier.setValue(r.no_sj_supplier);
        form.terima_material_id.setValue(r.terima_material_id);
        jun.rztCreateReturnPembelianDetails.baseParams = {terima_material_id: r.terima_material_id};
        jun.rztCreateReturnPembelianDetails.load();
        jun.rztCreateReturnPembelianDetails.baseParams = {};
        form.griddetils.material.store.baseParams = {terima_material_id: r.terima_material_id};
        form.griddetils.material.store.load();
        form.griddetils.material.store.baseParams = {};
    },
    InputInvoiceSupplier: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Penerimaan.");
            return;
        }
        if (selectedz.json.status != SJ_SUPPLIER_OPEN) {
            Ext.MessageBox.alert("Warning", "Invoice untuk data penerimaan ini telah dibuat.");
            return;
        }
        var form = new jun.InvoiceSupplierWin({
            modez: 1,
            id: selectedz.json.terima_material_id,
            iconCls: 'silk13-page_white_edit',
            title: "Invoice Supplier [ No. SJ : " + selectedz.json.no_sj_supplier + " ]",
            storeDetail: jun.rztInvoiceSupplierDetails,
            tipeInvoice: TIPE_INV_DEFAULT
        });
        form.show(this);
        //form.formz.getForm().loadRecord(this.record);
        form.cmbSupplier.setValue(this.record.get("supplier_id"));
        form.term_of_payment.setValue(this.record.get("term_of_payment"));
        form.currency.setValue(this.record.get("currency"));
        form.IDR_rate.setValue(this.record.get("IDR_rate"));
        form.griddetils.store.baseParams = {terima_material_id: selectedz.json.terima_material_id};
        form.griddetils.store.load({
            callback: function () {
                form.griddetils.store.refreshData();
            },
            scope: this
        });
        form.griddetils.store.baseParams = {};
    }
});
