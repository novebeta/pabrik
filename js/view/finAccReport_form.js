jun.FinAccReportWin = Ext.extend(Ext.Window, {
    title: 'Laporan FA',
    modez: 1,
    width: 460,
    height: 240,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-FinAccReport',
                labelWidth: 160,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../start_date',
                        fieldLabel: 'Mulai Tanggal',
                        name: 'start_date',
                        format: 'd M Y',
                        value: new Date(new Date().getFullYear(), new Date().getMonth() - 1, 1),
                        allowBlank: false,
                        hideLabel: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../end_date',
                        fieldLabel: 'Sampai Tanggal',
                        name: 'end_date',
                        format: 'd M Y',
                        value: new Date(new Date().getFullYear(), new Date().getMonth(), 0),
                        allowBlank: false,
                        hideLabel: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Tenaga kerja tidak langsung',
                        hideLabel: false,
                        name: 'biaya_tenagakerja_tidak_langsung',
                        ref: '../biaya_tenagakerja_tidak_langsung',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Listrik',
                        hideLabel: false,
                        name: 'biaya_listrik',
                        ref: '../biaya_listrik',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Penyusutan mesin',
                        hideLabel: false,
                        name: 'biaya_penyusutan_mesin',
                        ref: '../biaya_penyusutan_mesin',
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.FinAccReportWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
        } else { //view
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-FinAccReport').getForm().submit({
            url: 'FinAccReport/create',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id_fin_acc_report: this.fin_acc_report_id
            },
            success: function (f, a) {
                jun.rztFinAccReport.reload();
                if (this.modez == 0) {
                    Ext.getCmp('form-FinAccReport').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});