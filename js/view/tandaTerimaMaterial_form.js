jun.TandaTerimaWin = Ext.extend(Ext.Window, {
    title: 'Tanda Terima Material',
    modez: 1,
    width: 900,
    height: 435,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E8E8E8; padding: 10px',
                id: 'form-TandaTerimaMaterial',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref.:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        name: 'tgl',
                        allowBlank: false,
                        format: 'd M Y',
                        value: new Date(),
                        x: 85,
                        y: 32,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "No. Retur:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref_return',
                        ref: '../doc_ref_return',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 295 - 10,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        ref: '../cmbSupplier',
                        x: 400,
                        y: 2,
                        allowBlank: false,
                        readOnly: true,
                        width: 200
                    },
                    {
                        xtype: "label",
                        text: "Tanda Terima:",
                        x: 295 - 10,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'tanda_terima',
                        ref: '../tanda_terima',
                        maxLength: 100,
                        x: 400,
                        y: 32,
                        width: 200
                    },
                    {
                        xtype: "label",
                        text: "Note :",
                        x: 295 - 10,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        name: 'note',
                        ref: '../note',
                        maxLength: 100,
                        x: 400,
                        y: 62,
                        width: 200
                    },
                    {
                        xtype: "label",
                        text: "Gudang:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'loc_code',
                        id:'tanda_terima_loc_code_id',
                        store: jun.rztStorageLocationCmp,
                        hiddenName: 'loc_code',
                        valueField: 'loc_code',
                        displayField: 'loc_code',
                        x: 675,
                        y: 2
                    },
                    new jun.TandaTerimaMaterialDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 255,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        store: this.storeDetails//,
                        // readOnly: ((this.modez < 2) ? false : true)
                    }),
                    {
                        xtype: 'hidden',
                        name: 'return_pembelian_id',
                        ref: '../return_pembelian_id'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TandaTerimaWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 0) { //create
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else { //after audit
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            // this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    onWinClose: function () {
        this.storeDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-TandaTerimaMaterial').getForm().submit({
            url: 'TandaTerimaMaterial/create/',
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    this.storeDetails.data.items, "data"))
            },
            success: function (f, a) {
                if (Ext.getCmp('docs-jun.TandaTerimaMaterialGrid') !== undefined)
                    Ext.getCmp('docs-jun.TandaTerimaMaterialGrid').store.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TandaTerimaMaterial').getForm().reset();
                    this.btnDisabled(false);
                    this.onWinClose();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});