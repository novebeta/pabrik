jun.StorageLocationWin = Ext.extend(Ext.Window, {
    title: 'Gudang',
    modez: 1,
    width: 400,
    height: 175,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-StorageLocation',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode Gudang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'loc_code',
                        id: 'loc_codeid',
                        ref: '../loc_code',
                        maxLength: 60,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Gudang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'location_name',
                        id: 'location_nameid',
                        ref: '../location_name',
                        maxLength: 60,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Alamat',
                        hideLabel: false,
                        //hidden:true,
                        name: 'delivery_address',
                        id: 'delivery_addressid',
                        ref: '../delivery_address',
                        anchor: '100%'
                        //allowBlank:
                    }//,
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'phone',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'phone',
                    //     id: 'phoneid',
                    //     ref: '../phone',
                    //     maxLength: 30,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'phone2',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'phone2',
                    //     id: 'phone2id',
                    //     ref: '../phone2',
                    //     maxLength: 30,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'fax',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'fax',
                    //     id: 'faxid',
                    //     ref: '../fax',
                    //     maxLength: 30,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'email',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'email',
                    //     id: 'emailid',
                    //     ref: '../email',
                    //     maxLength: 100,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'contact',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'contact',
                    //     id: 'contactid',
                    //     ref: '../contact',
                    //     maxLength: 30,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'active',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'active',
                    //     id: 'activeid',
                    //     ref: '../active',
                    //     maxLength: 1,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.StorageLocationWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'StorageLocation/update/id/' + this.id;
        } else {
            urlz = 'StorageLocation/create/';
        }
        Ext.getCmp('form-StorageLocation').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztStorageLocation.reload();
                jun.rztStorageLocationCmp.reload();
                jun.rztStorageLocationLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-StorageLocation').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});