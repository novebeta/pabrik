jun.FinAccReportGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Laporan FA",
    id: 'docs-jun.FinAccReportGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Dari Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'start_date',
            width: 80,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Sampai Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'end_date',
            width: 80,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        }
    ],
    userPermission: function (actCreate, actEdit, actDelete, actDownload) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
        this.actDownload = actDownload;
    },
    initComponent: function () {
        this.store = jun.rztFinAccReport;
        switch (UROLE) {
            case USER_ADMIN:
                this.userPermission(1, 1, 1, 1);
                break;
            case USER_PPIC:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_GA:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_PURCHASING:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_PEMBELIAN:
                this.userPermission(0, 0, 0, 0);
                break;
            case USER_RND:
                this.userPermission(0, 0, 0, 0);
                break;
            default:
                this.userPermission(0, 0, 0, 0);
        }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: this.actDelete ? false : true
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Download',
                    ref: '../btnDownloadReport',
                    iconCls: 'silk13-page_white_excel',
                    hidden: this.actDownload ? false : true
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-FinAccReportGrid",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "fin_acc_report_id",
                            ref: "../../fin_acc_report_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel"
                        }
                    ]
                },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Periode</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'periode',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Periode',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'periode';
                                        Ext.getCmp('docs-jun.FinAccReportGrid').dateFind.focus().reset();
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'xdatefield',
                    ref: '../dateFind',
                    format: 'F Y',
                    width: 130
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        jun.FinAccReportGrid.superclass.initComponent.call(this);
        this.btnAdd.on('click', this.loadForm, this);
        this.btnEdit.on('click', this.loadEditForm, this);
        this.btnDelete.on('click', this.deleteRec, this);
        this.btnDownloadReport.on('click', this.downloadReport, this);
        this.dateFind.on('select', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    find: function (c, d) {
        this.store.baseParams = {fieldsearch: this.btnFind.fieldsearch, valuesearch: c.getValue()};
        this.botbar.moveFirst();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.FinAccReportWin({
            modez: 0,
            title: "Buat Laporan",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        var form = new jun.FinAccReportWin({
            modez: this.actEdit ? 1 : 2,
            fin_acc_report_id: selectedz.json.fin_acc_report_id,
            title: (this.actEdit ? "Ubah" : "Lihat") + " Laporan",
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data.");
            return;
        }
        Ext.MessageBox.confirm(
            'Hapus Data.',
            "Apakah anda yakin ingin menghapus data laporan ini?",
            this.deleteRecYes,
            this
        );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'FinAccReport/delete/id/' + record.json.fin_acc_report_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztFinAccReport.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    downloadReport: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Purchase Order");
            return;
        }
        Ext.getCmp("form-FinAccReportGrid").getForm().standardSubmit = !0;
        Ext.getCmp("form-FinAccReportGrid").getForm().url = "Report/PrintFinAccReport";
        this.fin_acc_report_id.setValue(selectedz.json.fin_acc_report_id);
        var form = Ext.getCmp('form-FinAccReportGrid').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
