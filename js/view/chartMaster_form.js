jun.ChartMasterWin = Ext.extend(Ext.Window, {
    title: 'Chart Of Accounts',
    modez: 1,
    width: 400,
    height: 280,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-ChartMaster',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Account Code',
                        hideLabel: false,
                        //hidden:true,
                        name: 'account_code',
                        id: 'account_codeid',
                        ref: '../account_code',
                        maxLength: 60,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Account Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'account_name',
                        id: 'account_nameid',
                        ref: '../account_name',
                        maxLength: 60,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    //{
                    //    xtype: 'checkbox',
                    //    fieldLabel: 'Header',
                    //    hideLabel: false,
                    //    value: 1,
                    //    inputValue: 1,
                    //    uncheckedValue: 0,
                    //    name: 'header',
                    //    id: 'headerid',
                    //    ref: '../h'
                    //},
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'Kategori',
                        store: jun.rztChartTypesCmp,
                        forceSelection: true,
                        hiddenName: 'kategori',
                        valueField: 'id',
                        ref: '../kategori',
                        displayField: 'name',
                        anchor: '100%'
                    },
                    new Ext.form.ComboBox({
                        fieldLabel: 'Saldo Normal',
                        store: ['D', 'K'],
                        mode: 'local',
                        ref: '../saldo_normal',
                        name: 'saldo_normal',
                        hiddenName: 'saldo_normal',
                        forceSelection: true,
                        triggerAction: 'all',
                        allowBlank: false
                    }),
                    {
                        xtype: 'hidden',
                        ref: '../tipe',
                        name: 'tipe'
                    },
                    // new Ext.form.ComboBox({
                    //     fieldLabel: 'Tipe',
                    //     store: ['L','N'],
                    //     mode: 'local',
                    //     ref: '../tipe',
                    //     name: 'tipe',
                    //     forceSelection: true,
                    //     triggerAction: 'all',
                    //     allowBlank: false
                    // })
                    //,
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel: false,
                        height: 80,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'description',
                        id: 'descriptionid',
                        ref: '../description',
                        anchor: '100%'
                        //allowBlank: 1
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ChartMasterWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'ChartMaster/update/id/' + this.id;
        } else {
            urlz = 'ChartMaster/create/';
        }
        Ext.getCmp('form-ChartMaster').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                jun.rztChartMaster.reload();
                jun.rztChartMasterHeader.reload();
                jun.rztChartMasterLib.reload();
                jun.rztChartMasterCmp.reload();
                jun.rztChartMasterCmpHutang.reload();
                jun.rztChartMasterCmpBank.reload();
                jun.rztChartMasterCmpBiaya.reload();
                jun.rztChartMasterCmpPendapatan.reload();
                Ext.getCmp('docs-jun.DesignLabaRugiGrid').getRootNode().reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ChartMaster').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});