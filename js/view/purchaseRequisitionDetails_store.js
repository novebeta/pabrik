jun.PurchaseRequisitionDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchaseRequisitionDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchaseRequisitionDetailsStoreId',
            url: 'PurchaseRequisitionDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pr_detail_id'},
                {name: 'qty'},
                {name: 'sat'},
                {name: 'price'},
                {name: 'material_id'},
                {name: 'pr_id'},
                {name: 'note'}
            ]
        }, cfg));
    },
    refreshData: function () {
        // var total = 0;
        // this.each(function(r){
        //     total += r.get('qty')*r.get('price');
        // });
        // Ext.getCmp('form-PurchaseRequisition-total').setValue(total);
    }
});
jun.rztPurchaseRequisitionDetails = new jun.PurchaseRequisitionDetailsstore();
