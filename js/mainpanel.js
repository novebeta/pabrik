jun.TabsUi = Ext.extend(Ext.TabPanel, {
    activeTab: 0,
    region: "center",
    frame: !0,
    id: "mainpanel",
    enableTabScroll: !0,
    style: "background-image:url(http://localhost:81/pabrik/images/logo-large.png) !important",
    initComponent: function () {
        this.items = [];
        jun.TabsUi.superclass.initComponent.call(this);
        this.on("load", this.onActivate, this);
    },
    onActivate: function (a) {
    },
    loadClass: function (href) {
        var id = "docs-" + href, tab = this.getComponent(id), obj = eval(href);
        if (tab)this.setActiveTab(tab); else {
            var object = new obj({id: id, closable: !0});
            if (object.iswin)object.show(); else {
                var p = this.add(object);
                this.setActiveTab(p)
            }
        }
    }
});